package com.opencheck.lixianalytics.location;

import android.location.Location;

public abstract class LixiLocationListener {

    public abstract void locationDetected(Location location);
}
