package com.opencheck.lixianalytics.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.opencheck.lixianalytics.log.ShowLog;

@SuppressWarnings({"WeakerAccess", "unused"})
public class LocationTracking extends LocationCallback implements Runnable {

    @Override
    public void onLocationResult(LocationResult locationResult) {
        ShowLog.LogInfo("Location detected");
        listener.locationDetected(locationResult.getLastLocation());
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability) {
        if (!locationAvailability.isLocationAvailable()) {
            handler.post(this);
            ShowLog.LogError("LocationAvailability not available");
        }
    }

    private Handler handler;
    private Context context;
    private long requestRetry = 10000;
    private long requestInterval = 60000;
    private LixiLocationListener listener;
    private FusedLocationProviderClient locationProviderClient;

    public LocationTracking(Context context,
                            Class<? extends LixiLocationListener> location, long interval) {
        init(context, location, interval);
    }

    private void init(Context context,
                      Class<? extends LixiLocationListener> location, long interval) {
        this.context = context;
        this.handler = new Handler();
        this.requestInterval = interval;
        locationProviderClient = new FusedLocationProviderClient(context);
        Exception exception = null;
        try {
            listener = location.newInstance();
        } catch (IllegalAccessException e) {
            exception = e;
        } catch (InstantiationException e) {
            exception = e;
        }
        if (exception != null) {
            ShowLog.LogError("Location - Cannot start tracking location - " +
                    exception.getLocalizedMessage());
            return;
        }
        startLocationTracking();
    }

    @Override
    public void run() {
        ShowLog.LogInfo("Retry detect location");
        startLocationTracking();
    }

    private void startLocationTracking() {
        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(requestInterval);
        locationRequest.setFastestInterval(requestInterval);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(context);
        com.google.android.gms.tasks.Task<LocationSettingsResponse> task =
                client.checkLocationSettings(builder.build());
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowLog.LogWarning("Location - gps off");
                handler.removeCallbacks(LocationTracking.this);
                handler.postDelayed(LocationTracking.this, requestRetry);
            }
        });
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if (ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ShowLog.LogWarning("Location - none permission");
                    handler.removeCallbacks(LocationTracking.this);
                    handler.postDelayed(LocationTracking.this, requestRetry);
                } else {
                    ShowLog.LogInfo("Location - Staring detect location");
                    startDetectLocation(locationRequest);
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void startDetectLocation(LocationRequest locationRequest) {
        locationProviderClient.removeLocationUpdates(this);
        locationProviderClient
                .requestLocationUpdates(locationRequest, this, null);
    }
}
