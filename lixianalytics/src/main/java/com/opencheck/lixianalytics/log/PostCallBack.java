package com.opencheck.lixianalytics.log;

public interface PostCallBack {

    void onResponse(Object obj);

    void onError(String error);
}
