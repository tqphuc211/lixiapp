package com.opencheck.lixianalytics.log;

import android.util.Log;

public class ShowLog {

    public static String LOG;

    public static void LogInfo(String message) {
        if (LOG == null || message == null) return;
        Log.i(LOG, message);
    }

    public static void LogError(String message) {
        if (LOG == null || message == null) return;
        Log.e(LOG, message);
    }

    public static void LogWarning(String message) {
        if (LOG == null || message == null) return;
        Log.w(LOG, message);
    }
}
