package com.opencheck.lixianalytics.model;


import android.support.annotation.Size;

import java.io.Serializable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Task implements Serializable {

    private long id = 0;
    private int retry = 0;
    private Object data;
    @Size(max = 2)
    private String[] key;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String[] getKey() {
        return key;
    }

    public void setEvent(String[] key) {
        this.key = key;
    }
}
