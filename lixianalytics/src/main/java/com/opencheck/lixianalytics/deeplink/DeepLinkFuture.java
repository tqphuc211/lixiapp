package com.opencheck.lixianalytics.deeplink;

public interface DeepLinkFuture {

    void onFuture(Object... arg);
}
