package com.opencheck.lixianalytics.deeplink;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.HashMap;

public abstract class LixiDeepLinkDetect {

    private HashMap<Class<? extends Activity>, Object[]> future = new HashMap<>();

    public HashMap<Class<? extends Activity>, Object[]> getFuture() {
        return future;
    }

    /**
     * @param aClass  Activity class that's implement [{@link DeepLinkFuture}] call when activity onResume [one times]
     * @param objects passing data
     */
    protected void applyFutureForActivity(Class<? extends Activity> aClass, Object... objects) {
        future.put(aClass, objects);
    }

    public abstract void onCreate(@Nullable Uri uri, Activity launcher);
}
