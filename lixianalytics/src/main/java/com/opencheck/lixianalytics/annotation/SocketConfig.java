package com.opencheck.lixianalytics.annotation;

import com.opencheck.lixianalytics.enums.EmmitFormat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.socket.engineio.client.transports.WebSocket;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SocketConfig {

    String url();

    String defaultEmmitFormat();

    boolean forceNew() default true;

    String[] transports() default {WebSocket.NAME};

    boolean reconnection() default true;

    long reconnectionDelay() default 1000L;

    int reconnectionAttempts() default 5;

    long reconnectionDelayMax() default 5000;
}
