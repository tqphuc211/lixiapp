package com.opencheck.lixianalytics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Name {

    //For custom model field you want to manual set value by hash map, contains with device key
    public static final String OPTIONS = "option";
    //Device key
    public static final String BATTERY = "battery";
    public static final String PLATFORM = "platform";
    public static final String DEVICE_ID = "device_id";
    public static final String TIME_STAMP = "time_stamp";
    public static final String OS_VERSION = "os_version";
    public static final String IP_ADDRESS = "ip_address";
    public static final String APP_VERSION = "app_version";
    public static final String MAC_ADDRESS = "mac_address";
    public static final String DEVICE_MODEL = "device_model";
    public static final String SCREEN_WIDTH = "screen_width";
    public static final String SCREEN_HEIGHT = "screen_height";
    public static final String NETWORK_CONNECTION = "network_connection";
    /**
     * @return the desired name of the field
     */
    String[] value();

    String format() default "";
}
