package com.opencheck.lixianalytics.socket;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.opencheck.lixianalytics.LixiAnalytics;
import com.opencheck.lixianalytics.annotation.SocketConfig;
import com.opencheck.lixianalytics.deeplink.DeepLinkFuture;
import com.opencheck.lixianalytics.deeplink.LixiDeepLinkActivity;
import com.opencheck.lixianalytics.deeplink.LixiDeepLinkDetect;
import com.opencheck.lixianalytics.log.ShowLog;
import com.opencheck.lixianalytics.model.Task;
import com.opencheck.lixianalytics.preferences.LixiPreferences;
import com.opencheck.lixianalytics.utils.Converters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

@SuppressWarnings({"WeakerAccess", "unused"})
public class LixiSocket implements Application.ActivityLifecycleCallbacks {

    private Socket socket = null;
    private long commitKey = 0;
    private boolean isEmmit = false;
    private SocketConfig socketConfig;
    private long currentActivityKey = 0;
    private static final String INSTANCE_KEY = "INSTANCE_KEY";
    private LinkedBlockingQueue<Task> tasks = new LinkedBlockingQueue<>();
    private LixiDeepLinkDetect deepLinkListener;
    private final Branch branch;
    private final Object mLock = new Object();

    private LixiSocketProvider socketListener;

    public LixiSocket(Class<? extends LixiSocketProvider> socket,
                      Class<? extends LixiDeepLinkDetect> deepLink, Branch branch) {
        Exception ex = null;
        this.branch = branch;
        try {
            this.socketConfig = socket.getAnnotation(SocketConfig.class);
            this.socketListener = socket.newInstance();
            this.deepLinkListener = deepLink.newInstance();

        } catch (Exception e) {
            ex = e;
        }
        if (ex != null) {
            ShowLog.LogError("Cannot create listener - " + ex.getLocalizedMessage());
        }
    }

    @Nullable
    private Uri createUriFromBranch(Uri uri, JSONObject referringParams) {
        if (referringParams == null) {
            return null;
        }
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(uri.getScheme())
                .authority(uri.getAuthority())
                .appendEncodedPath(uri.getPath());

        String query = null;
        Map<String, Object> objectHashMap = Converters.jsonObjectToMap(referringParams);
        for (Map.Entry<String, Object> item : objectHashMap.entrySet()
        ) {
            try {
                builder.appendQueryParameter(item.getKey(), String.valueOf(item.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return builder.build();
    }

    private final FragmentManager.FragmentLifecycleCallbacks fragmentLifecycleCallbacks =
            new FragmentManager.FragmentLifecycleCallbacks() {
                @Override
                public void onFragmentResumed(@NonNull FragmentManager fm, @NonNull Fragment f) {
                    super.onFragmentResumed(fm, f);
                    if (socketListener != null)
                        socketListener.onFragmentResumed(fm, f);
                }
            };

    @Override
    @SuppressWarnings("unchecked")
    public void onActivityCreated(final Activity activity, Bundle savedInstanceState) {
        if (deepLinkListener != null && activity instanceof LixiDeepLinkActivity) {
            ShowLog.LogInfo("DeepLink detected");
            currentActivityKey = System.currentTimeMillis();
            final Intent intent = activity.getIntent();
            if (branch != null && intent.getData() != null) {
                branch.initSession(new Branch.BranchReferralInitListener() {
                    @Override
                    public void onInitFinished(JSONObject referringParams, BranchError error) {
                        deepLinkListener.onCreate(
                                createUriFromBranch(intent.getData(), referringParams),
                                activity);
                    }
                }, intent.getData(), activity);
            } else {
                if (deepLinkListener != null)
                    deepLinkListener.onCreate(intent.getData(), activity);
            }
//            if (activity.isTaskRoot()) {
//                try {
//                    ActivityInfo ai = activity.getPackageManager()
//                            .getActivityInfo(activity.getComponentName(), PackageManager.GET_META_DATA);
//                    if (ai != null) {
//                        Bundle bundle = ai.metaData;
//                        if (bundle != null) {
//                            String pkg = bundle.getString("pkg");
//                            if (pkg != null) {
//                                Intent launchIntent = activity.getPackageManager()
//                                        .getLaunchIntentForPackage(pkg);
//                                if (launchIntent != null) {
//                                    launchIntent.setData(activity.getIntent().getData());
//                                    activity.startActivity(launchIntent);
//                                }
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    ShowLog.LogError("DeepLink : " + e.getLocalizedMessage());
//                }
//            }
//            activity.finish();
        } else if (activity.isTaskRoot() && currentActivityKey == 0) {
            ShowLog.LogInfo("on launcher Activity created");
            currentActivityKey = System.currentTimeMillis();
            if (deepLinkListener != null)
                deepLinkListener.onCreate(null, activity);
        }
    }

    @Override
    public void onActivityStarted(final Activity activity) {
        if (deepLinkListener != null && activity instanceof DeepLinkFuture) {
            Object[] objects = deepLinkListener.getFuture().get(activity.getClass());
            if (objects != null) {
                ((DeepLinkFuture) activity).onFuture(objects);
                deepLinkListener.getFuture().remove(activity.getClass());
            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        socketListener.onActivityResumed(activity);
        if (deepLinkListener != null && activity instanceof DeepLinkFuture) {
            Object[] objects = deepLinkListener.getFuture().get(activity.getClass());
            if (objects != null) {
                ((DeepLinkFuture) activity).onFuture(objects);
                deepLinkListener.getFuture().remove(activity.getClass());
            }
        }
        if (activity instanceof AppCompatActivity) {
            ((AppCompatActivity) activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(
                    fragmentLifecycleCallbacks, true);
        }
        if (activity.getIntent().getLongExtra(INSTANCE_KEY, 0L) == currentActivityKey) {
            startToForceGround();
        }
        activity.getIntent().putExtra(INSTANCE_KEY,
                (currentActivityKey = System.currentTimeMillis()));
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (activity instanceof AppCompatActivity) {
            ((AppCompatActivity) activity).getSupportFragmentManager().unregisterFragmentLifecycleCallbacks(
                    fragmentLifecycleCallbacks);
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        if (!activity.isChangingConfigurations() &&
                activity.getIntent().getLongExtra(INSTANCE_KEY,
                        0L) == currentActivityKey) {
            goToBackground();
        } else if (activity.isChangingConfigurations() &&
                activity.getIntent().getLongExtra(INSTANCE_KEY,
                        0L) == currentActivityKey) {
            //Activity rotate
            activity.getIntent().putExtra(INSTANCE_KEY, (System.currentTimeMillis()));
        }
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (!activity.isChangingConfigurations() && activity.getIntent().getLongExtra(
                INSTANCE_KEY, 0L) == currentActivityKey) {
            currentActivityKey = 0;
//            exit();
        }
    }

    public void startToForceGround() {
        ShowLog.LogInfo("Application ForceGround");
        socketListener.onForceGround();
    }

    public void goToBackground() {
        ShowLog.LogInfo("Application Background");
        socketListener.onBackground();
    }

    public SocketConfig getSocketConfig() {
        return socketConfig;
    }

    public void push(LixiAnalytics.SocketTrackBuilder socketTrackBuilder) {
        synchronized (mLock) {
            Task task = new Task();
            task.setId(++commitKey);
            task.setEvent(socketTrackBuilder.getEvent());
            task.setData(socketTrackBuilder.getObjectData());
            task.setRetry(socketTrackBuilder.getAutoRetry() == null ?
                    LixiPreferences.getInstance().getConfig()[0] :
                    socketTrackBuilder.getAutoRetry());
            tasks.add(task);
            autoEmmit();
        }
    }

    private synchronized ArrayList<Task> getPushTask() {
        synchronized (mLock) {
            ArrayList<Task> temp = new ArrayList<>(tasks);
            ArrayList<Task> pushTask = new ArrayList<>();
            Task lastTask = temp.get(temp.size() - 1);
            if (lastTask != null) {
                if (lastTask.getKey().length > 1) {
                    for (Task items : tasks
                    ) {
                        if (Arrays.equals(items.getKey(), lastTask.getKey())) {
                            items.setRetry(items.getRetry() - 1);
                            if (items.getRetry() >= 0) {
                                pushTask.add(items);
                            } else {
                                tasks.remove(items);
                            }
                            //Max queue
                            if (pushTask.size() >= LixiPreferences.getInstance().getConfig()[1]) {
                                break;
                            }
                        }
                    }
                    //Min queue
                    if (pushTask.size() < LixiPreferences.getInstance().getConfig()[2]) {
                        tasks.clear();
                        tasks.addAll(temp);
                        return null;
                    }
                } else {
                    lastTask.setRetry(lastTask.getRetry() - 1);
                    tasks.remove(lastTask);
                    pushTask.add(lastTask);
                }
            }
            tasks.removeAll(pushTask);
            return pushTask;
        }
    }

    private synchronized void autoEmmit() {
        synchronized (mLock) {
            if (isEmmit || socket == null || !socket.connected() || tasks.peek() == null) {
                return;
            }
            isEmmit = true;
            final ArrayList<Task> pushTask = getPushTask();
            //Min queue waiting
            if (pushTask == null) {
                isEmmit = false;
                return;
            }
            final ArrayList<Long> dataId = new ArrayList<>();
            ArrayList<Object> data = new ArrayList<Object>() {{
                for (Task item : pushTask
                ) {
                    add(item.getData());
                    dataId.add(item.getId());
                }
            }};
            switch (pushTask.size()) {
                case 0: {
                    isEmmit = false;
                    autoEmmit();
                    break;
                }
                case 1: {
                    ShowLog.LogInfo("[Key]: " + pushTask.get(0).getKey()[0] + " [Data]" + data.get(0));
                    if (socket != null)
                        socket.emit(pushTask.get(0).getKey()[0], data.get(0), new Ack() {
                            @Override
                            public void call(Object... args) {
                                try {
                                    //Push error
                                    switch ((int) args[0]) {
                                        case 0: {
                                            //Success
                                            ShowLog.LogInfo("Send tracking success - Task idKey(" +
                                                    pushTask.get(0).getId() + ")");
                                            break;
                                        }
                                        case 1: {
                                            //Server error should retry
                                            ShowLog.LogError(
                                                    "Server error should retry - Times: " +
                                                            pushTask.get(0).getRetry());
                                            tasks.addAll(pushTask);
                                            break;
                                        }
                                        case 2: {
                                            //Data client not match
                                            ShowLog.LogError(
                                                    "Send tracking error: " + args[1]);
                                            break;
                                        }
                                    }
                                } catch (Exception e) {
                                    ShowLog.LogError("Socket client error:" + e.getLocalizedMessage());
                                } finally {
                                    isEmmit = false;
                                    if (tasks.size() > 0) {
                                        autoEmmit();
                                    }
                                }
                            }
                        });
                    break;
                }
                default: {
                    ShowLog.LogInfo("[Key]: " + pushTask.get(0).getKey()[1] + " [Data]" + data);
                    if (socket != null)
                        socket.emit(pushTask.get(0).getKey()[1], new JSONArray(data), new Ack() {
                            @Override
                            public void call(Object... args) {
                                try {
                                    if (args[0] instanceof Integer) {
                                        //PushJsonData error
                                        switch ((int) args[0]) {
                                            case 0: {
                                                //Success
                                                ShowLog.LogInfo(
                                                        "Send tracking success: Task ids(" +
                                                                dataId + ")");
                                                break;
                                            }
                                            case 1: {
                                                //Server error should retry
                                                ShowLog.LogWarning(
                                                        "Server error should retry" + "Task ids(" +
                                                                dataId + ")");
                                                tasks.addAll(pushTask);
                                                break;
                                            }
                                            case 2: {
                                                //Data client not match
                                                ShowLog.LogError(
                                                        "Send tracking error: " + args[1] + "Remove all Task ids(" +
                                                                dataId + ")");
                                                break;
                                            }
                                        }
                                    }
                                    if (args[0] instanceof JSONArray) {
                                        JSONArray array = (JSONArray) args[0];
                                        List<Integer> argsList = new ArrayList<>();
                                        for (int i = 0; i < array.length(); i++) {
                                            try {
                                                argsList.add(array.getInt(i));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        for (int i = 0; i < argsList.size(); i++) {
                                            switch (argsList.get(i)) {
                                                case 0: {
                                                    //Success
                                                    ShowLog.LogInfo(
                                                            "Send tracking success - " +
                                                                    "Task idKey(" + pushTask.get(i).getId() + ")");
                                                    break;
                                                }
                                                case 1: {
                                                    //Server error should retry
                                                    ShowLog.LogInfo(
                                                            "Server error should retry - Times: "
                                                                    + pushTask.get(i).getRetry() + " Task idKey(" +
                                                                    pushTask.get(i).getId() + ")");
                                                    tasks.add(pushTask.get(i));
                                                    break;
                                                }
                                                case 2: {
                                                    //Data client not match
                                                    ShowLog.LogError(
                                                            "Send tracking error: " + args[1] + "Task idKey(" +
                                                                    pushTask.get(i).getId() + ")");
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    ShowLog.LogError("Socket client error:" + e.getLocalizedMessage());
                                } finally {
                                    isEmmit = false;
                                    if (tasks.size() > 0) {
                                        autoEmmit();
                                    }
                                }
                            }
                        });
                    break;
                }
            }
        }
    }

    public void connectSocket(LixiAnalytics.SocketQueryBuilder socketBuilder) {
        disconnectSocket();
        String socketUrl = socketConfig.url();
        final IO.Options opts = new IO.Options();
        opts.forceNew = socketConfig.forceNew();
        opts.transports = socketConfig.transports();
        opts.reconnection = socketConfig.reconnection();
        opts.reconnectionDelay = socketConfig.reconnectionDelay();
        opts.reconnectionAttempts = socketConfig.reconnectionAttempts();
        opts.reconnectionDelayMax = socketConfig.reconnectionDelayMax();
        //EmmitFormat socket
        socketUrl += socketBuilder.getQuery();
        socketUrl = socketUrl.trim().replace(" ", "%20");
        ShowLog.LogInfo("Starting connect: " + socketUrl);
        try {
            socket = IO.socket(socketUrl, opts);
            socket.connect();
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = false;
                    ShowLog.LogInfo("socket.io - EVENT_CONNECT");
                    if (socketListener != null)
                        socketListener.onConnect(socket, args);
                    autoEmmit();
                }
            }).on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogInfo("socket.io - EVENT_CONNECTING");
                    if (socketListener != null)
                        socketListener.onConnecting(args);
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogInfo("socket.io - EVENT_DISCONNECT");
                    if (socketListener != null)
                        socketListener.onDisconnect(args);
                }
            }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogError("socket.io - EVENT_ERROR");
                    if (socketListener != null)
                        socketListener.onError(args);
                }
            }).on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    ShowLog.LogInfo("socket.io - EVENT_MESSAGE");
                    if (socketListener != null)
                        socketListener.onMessage(args);
                }
            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogError("socket.io - EVENT_CONNECT_ERROR");
                    if (socketListener != null)
                        socketListener.onConnectError(args);
                }
            }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogError("socket.io - EVENT_CONNECT_TIMEOUT");
                    if (socketListener != null)
                        socketListener.onConnectTimeOut(args);
                }

            }).on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogWarning("socket.io - EVENT_RECONNECT");
                    if (socketListener != null)
                        socketListener.onReconnect(args);
                }
            }).on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogError("socket.io - EVENT_RECONNECT_ERROR");
                    if (socketListener != null)
                        socketListener.onReconnectError(args);
                }
            }).on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    ShowLog.LogError("socket.io - EVENT_RECONNECT_FAILED");
                    if (socketListener != null)
                        socketListener.onReconnectFailed(args);
                }
            }).on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogError("socket.io - EVENT_RECONNECT_ATTEMPT");
                    if (socketListener != null)
                        socketListener.onReconnectAttempt(args);
                }
            }).on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isEmmit = true;
                    ShowLog.LogWarning("socket.io - EVENT_RECONNECTING");
                    if (socketListener != null)
                        socketListener.onReconnecting(args);
                }
            }).on(Socket.EVENT_PING, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    ShowLog.LogInfo("socket.io - EVENT_PING");
                    if (socketListener != null)
                        socketListener.onPing(args);
                }
            }).on(Socket.EVENT_PONG, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    ShowLog.LogInfo("socket.io - EVENT_PONG");
                    if (socketListener != null)
                        socketListener.onPong(args);
                }
            });
        } catch (URISyntaxException e) {
            ShowLog.LogError("socket.io - URISyntaxException");
            String[] args = new String[1];
            args[0] = e.getLocalizedMessage();
            if (socketListener != null)
                socketListener.onWrongUri((Object) args);
        }
    }

    public void disconnectSocket() {
        if (socket != null && socket.connected()) {
            socket.disconnect();
            socket.close();
            socket.off();
            socket = null;
            ShowLog.LogInfo("disconnectSocket");
        }
    }
}