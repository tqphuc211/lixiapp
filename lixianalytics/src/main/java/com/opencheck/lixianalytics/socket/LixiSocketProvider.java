package com.opencheck.lixianalytics.socket;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.opencheck.lixianalytics.enums.EmmitState;

import io.socket.client.Socket;

public abstract class LixiSocketProvider {

    public abstract void onBackground();

    public abstract void onForceGround();

    public abstract void onActivityResumed(Activity activity);

    public abstract void onFragmentResumed(FragmentManager fragmentManager, Fragment fragment);

    public abstract EmmitState handleSocketEmmitReturn(Object... args);

    public void onConnect(@Nullable Socket socket, Object... args) {
    }

    public void onConnecting(Object... args) {
    }

    public void onDisconnect(Object... args) {
    }

    public void onError(Object... args) {
    }

    public void onMessage(Object... args) {
    }

    public void onConnectError(Object... args) {
    }

    public void onConnectTimeOut(Object... args) {
    }

    public void onReconnect(Object... args) {
    }

    public void onReconnectError(Object... args) {
    }

    public void onReconnectFailed(Object... args) {
    }

    public void onReconnectAttempt(Object... args) {
    }

    public void onReconnecting(Object... args) {
    }

    public void onPing(Object... args) {
    }

    public void onPong(Object... args) {
    }

    public void onWrongUri(Object... args) {
    }
}
