package com.opencheck.lixianalytics.enums;

public enum EmmitState {
    SUCCESS,
    RETRY,
    FAILED;
}
