package com.opencheck.lixianalytics.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Size;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.opencheck.lixianalytics.annotation.Name;
import com.opencheck.lixianalytics.utils.Converters;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public class LixiPreferences {

    private static final String DEVICES = "DEVICES";
    private static final String SOCKET = "SOCKET";

    private Context context;
    @SuppressLint("StaticFieldLeak")
    private static LixiPreferences mInstance;
    public SharedPreferences sharedPreferences;

    private LixiPreferences(Context... context) {
        this.context = context[0].getApplicationContext();
        sharedPreferences = context[0].getSharedPreferences(this.getClass().getName(),
                Context.MODE_PRIVATE);
    }

    public static LixiPreferences getInstance(Context... context) {
        if (mInstance == null && context.length > 0) {
            return (mInstance = new LixiPreferences(context[0]));
        } else {
            if (mInstance == null) {
                throw new NullPointerException("You must get get with context first");
            }
            return mInstance;
        }
    }

    @Size(value = 3)
    public int[] getConfig() {
        int retry = sharedPreferences.getInt("retry", 1);
        int maxQueue = sharedPreferences.getInt("maxQueue", 10);
        int minQueue = sharedPreferences.getInt("minQueue", 0);

        return new int[]{retry, maxQueue, minQueue};
    }

    public void saveConfig(int retry, int maxQueue, int minQueue) {
        sharedPreferences.edit().putInt("retry", retry < 0 ? 0 : retry).apply();
        sharedPreferences.edit().putInt("maxQueue", maxQueue < 1 ? 10 : maxQueue).apply();
        sharedPreferences.edit().putInt("minQueue", minQueue < 0 ? 0 : minQueue).apply();
    }

    public void saveSocketParam(HashMap<String, String> param) {
        String savedJson = sharedPreferences.getString(SOCKET, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.putAll(param);
        sharedPreferences.edit().putString(SOCKET, Converters.mapToJson(savedMap)).apply();
    }

    public HashMap<String, Object> getSocketParam() {
        String savedJson = sharedPreferences.getString(SOCKET, "{ }");
        return Converters.jsonToMap(savedJson);
    }

    public void clearSocketParam(String key) {
        String savedJson = sharedPreferences.getString(SOCKET, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.remove(key);
        sharedPreferences.edit().putString(SOCKET, Converters.mapToJson(savedMap)).apply();
    }

    public void clearAllSocketParam() {
        sharedPreferences.edit().putString(SOCKET, "{ }").apply();
    }

    public void clearCache() {
        sharedPreferences.edit().clear().apply();
    }

    /**
     * @return all saved field as map from  {@link SharedPreferences}
     */
    public HashMap<String, Object> getAllMap() {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.putAll(getDeviceInfo());
        return savedMap;
    }

    public void save(String key, Object object) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.put(key, object);
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }

    public void save(HashMap<String, Object> objectMap) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.putAll(objectMap);
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }


    public <T> void save(T model) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.putAll(Converters.objectToMap(model));
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }

    public void save(String json) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.putAll(Converters.jsonToMap(json));
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }

    public void clear(String key) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        savedMap.remove(key);
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }

    public <T> void clear(Class<T> clazz) {
        String savedJson = sharedPreferences.getString(DEVICES, "{ }");
        HashMap<String, Object> savedMap = Converters.jsonToMap(savedJson);
        for (java.lang.reflect.Field field : clazz.getDeclaredFields()
                ) {
            savedMap.remove(field.getName());
        }
        sharedPreferences.edit().putString(DEVICES, Converters.mapToJson(savedMap)).apply();
    }

    public String getNetworkConnection() {
        return "wifi";
    }

    public HashMap<String, Object> getDeviceInfo() {
        HashMap<String, Object> map = new HashMap<>();
        map.put(Name.BATTERY, getBatteryPercent());
        map.put(Name.APP_VERSION, getAppVersion());
        map.put(Name.DEVICE_ID, getDeviceId());
        map.put(Name.DEVICE_MODEL, getDeviceModel());
        map.put(Name.IP_ADDRESS, getIpAddress());
        map.put(Name.MAC_ADDRESS, getMacAddress());
        map.put(Name.NETWORK_CONNECTION, getNetworkConnection());
        map.put(Name.OS_VERSION, getOsVersion());
        map.put(Name.PLATFORM, getPlatform());
        map.put(Name.TIME_STAMP, System.currentTimeMillis());
        int[] deviceResolution = getDisplayResolution();
        map.put(Name.SCREEN_WIDTH, deviceResolution[0]);
        map.put(Name.SCREEN_HEIGHT, deviceResolution[1]);
        return map;
    }

    /**
     * @return displayResolution[0] as width, displayResolution[1] as height screen resolution
     */
    @Size(max = 2)
    public int[] getDisplayResolution() {
        int width = sharedPreferences.getInt("width", -1);
        int height = sharedPreferences.getInt("height", -1);
        if (width == -1 || height == -1) {
            WindowManager window = (WindowManager)
                    context.getSystemService(Context.WINDOW_SERVICE);
            if (window != null) {
                Display display = window.getDefaultDisplay();
                width = display.getWidth();
                height = display.getHeight();
            } else {
                DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                width = metrics.widthPixels;
                height = metrics.heightPixels;
            }
            sharedPreferences.edit().putInt("width", width).apply();
            sharedPreferences.edit().putInt("height", height).apply();
        }
        int[] displayResolution = new int[2];
        displayResolution[0] = width;
        displayResolution[1] = height;
        return displayResolution;
    }

    @SuppressLint("HardwareIds")
    public String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "02:00:00:00:00:00";
    }

    public String getIpAddress() {
        WifiManager manager = (WifiManager) context
                .getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (manager != null) {
            return Formatter.formatIpAddress(manager.getConnectionInfo().getIpAddress());
        }
        return "";
    }

    public String getDeviceModel() {
        String deviceModel = sharedPreferences.getString("deviceModel", "");
        if (deviceModel.equals("")) {
            deviceModel = (Build.MANUFACTURER.substring(0, 1).toUpperCase() +
                    Build.MANUFACTURER.substring(1)
                    + " " + Build.MODEL.substring(0, 1).toUpperCase() +
                    Build.MODEL.substring(1)).trim();
            sharedPreferences.edit().putString("deviceModel", deviceModel).apply();
        }
        return deviceModel;
    }

    public String getAppVersion() {
        try {
            PackageInfo pInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public Float getBatteryPercent() {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);
        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL,
                -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE,
                -1) : -1;
        if (level == -1 || scale == -1) {
            return 50.0f;
        }
        float batteryPct = level / (float) scale;
        return (batteryPct * 100);
    }

    public String getPlatform() {
        return "android";
    }

    @SuppressLint("HardwareIds")
    private String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
