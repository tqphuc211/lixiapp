package com.opencheck.lixianalytics.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.opencheck.lixianalytics.annotation.Name;
import com.opencheck.lixianalytics.log.ShowLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class Converters {

    /*
     *Convert JSON string to Map
     */
    @NonNull
    public static HashMap<String, Object> jsonToMap(String json) {
        return jsonStringToMap(json);
    }

    @NonNull
    public static String mapToJson(HashMap<String, Object> map) {
        return new JSONObject(map).toString();
    }

    @NonNull
    public static <T> String objectToJson(T object) {
        return new JSONObject(objectToMap(object)).toString();
    }

    @SuppressWarnings("unchecked")
    public static <T> HashMap<String, Object> objectToMap(T object) {
        HashMap<String, Object> hashMap = new HashMap<>();
        try {
            if (object == null || isWrapperType(object.getClass())) {
                hashMap.put("data", object);
                return hashMap;
            }
            for (java.lang.reflect.Field f : object.getClass().getDeclaredFields()
                    ) {
                if (f.isAnnotationPresent(Name.class)) {
                    String[] type = f.getAnnotation(Name.class).value();
                    boolean accessible = f.isAccessible();
                    f.setAccessible(true);
                    Object value = f.get(object);
                    if (value != null) {
                        if (value instanceof List) {
                            if (isWrapperType(((List) value).get(0).getClass())) {
                                HashMap<String, Object> objectHashMap = new HashMap<>();
                                objectHashMap.put(f.getName(), value);
                                hashMap.putAll(objectHashMap);
                            } else {
                                hashMap.putAll(listObjectToMap(f.getName(), (List) value));
                            }
                        } else if (isWrapperType(value.getClass())) {
                            hashMap.put(f.getName(), value);
                        } else {
                            hashMap.put(f.getName(), objectToMap(value));
                        }
                    }
                    f.setAccessible(accessible);
                }
            }
        } catch (Exception e) {
            ShowLog.LogError("Cannot cast object to map: " + e);
        }
        return hashMap;
    }

    public static <T> HashMap<String, Object> listObjectToMap(String key, List<T> object) {
        HashMap<String, Object> hashMap = new HashMap<>();
        List<HashMap<String, Object>> tArrayList = new ArrayList<>();
        for (T item : object
                ) {
            tArrayList.add(objectToMap(item));
        }
        hashMap.put(key, tArrayList);
        return hashMap;
    }

    public static Map<String, Object> jsonObjectToMap(JSONObject json) {
        Map<String, Object> retMap = new HashMap<>();
        try {
            if (json != JSONObject.NULL) {
                retMap = toMap(json);
            }
        } catch (JSONException e) {
            ShowLog.LogError("Cannot cast json to object: " + e);
        }
        return retMap;
    }

    public static HashMap<String, Object> jsonStringToMap(String json) {
        HashMap<String, Object> retMap = new HashMap<>();
        try {
            if (json != JSONObject.NULL) {
                retMap = toMap(new JSONObject(json));
            }
        } catch (JSONException e) {
            ShowLog.LogError("Cannot cast json to object: " + e);
        }
        return retMap;
    }


    @Nullable
    public static <T> T jsonToObject(Class<T> clazz, String json) {
        return mapToObject(clazz, jsonStringToMap(json));
    }

    @Nullable
    public static <T> T mapToObject(Class<T> clazz, HashMap<String, Object> map) {
        try {
            T t = clazz.newInstance();
            for (java.lang.reflect.Field field : t.getClass().getDeclaredFields()
                    ) {
                if (field.isAnnotationPresent(Name.class)) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    Object obj;
                    String[] value = field.getAnnotation(Name.class).value();
                    if (value.length == 1) {
                        if (value[0].equals(Name.OPTIONS)) {
                            if (isWrapperType(field.getType())) {
                                ShowLog.LogError("Field was a primitive value," +
                                        " please check your model Field.OPTIONS: Field name: " +
                                        field.getName() + " Model: " + clazz.getName());
                                continue;
                            }
                            field.set(t, mapToObject(field.getType(), map));
                        } else if ((obj = map.get(value[0])) != null) {
                            try {
                                //Fix double cannot convert to float
                                if (field.getType().equals(Float.TYPE) && obj instanceof Double) {
                                    field.setFloat(t, (float) (double) obj);
                                } else {
                                    field.set(t, obj);
                                }
                            } catch (Exception e) {
                                ShowLog.LogError("Cannot cast field: " + field.getName()
                                        + " - Please checking your field type: " + e);
                            }
                        }
                    } else {
                        Object[] a = new Object[value.length];
                        for (int i = 0; i < value.length; i++) {
                            a[i] = map.get(value[i]);
                        }
                        try {
                            String data = String.format(
                                    field.getAnnotation(Name.class).format(), a);
                            field.set(t, data);
                        } catch (Exception e) {
                            ShowLog.LogError( "Cannot cast field: " + field.getName() + " - Please " +
                                    "checking your field format");
                        }
                    }
                    field.setAccessible(accessible);
                }
            }
            return t;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            ShowLog.LogError( "Cannot create model: " + e);
        } catch (InstantiationException e) {
            e.printStackTrace();
            ShowLog.LogError("Cannot create model: " + e);
        }
        return null;
    }

    private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

    private static boolean isWrapperType(Class<?> clazz) {
        return WRAPPER_TYPES.contains(clazz);
    }

    private static Set<Class<?>> getWrapperTypes() {
        Set<Class<?>> ret = new HashSet<>();
        ret.add(Boolean.class);
        ret.add(String.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        return ret;
    }

    private static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
        HashMap<String, Object> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
