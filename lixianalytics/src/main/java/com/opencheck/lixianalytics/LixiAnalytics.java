package com.opencheck.lixianalytics;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.opencheck.lixianalytics.annotation.SocketConfig;
import com.opencheck.lixianalytics.deeplink.LixiDeepLinkDetect;
import com.opencheck.lixianalytics.enums.EmmitFormat;
import com.opencheck.lixianalytics.location.LixiLocationListener;
import com.opencheck.lixianalytics.location.LocationTracking;
import com.opencheck.lixianalytics.log.PostCallBack;
import com.opencheck.lixianalytics.log.ShowLog;
import com.opencheck.lixianalytics.preferences.LixiPreferences;
import com.opencheck.lixianalytics.socket.LixiSocket;
import com.opencheck.lixianalytics.socket.LixiSocketProvider;
import com.opencheck.lixianalytics.utils.Converters;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.branch.referral.Branch;

public class LixiAnalytics {

    private LixiSocket lixiSocket;
    private RequestQueue requestQueue;
    private static LixiAnalytics lixiAnalytics;

    private LixiAnalytics(Builder builder) {
        ShowLog.LOG = builder.log;
        requestQueue = Volley.newRequestQueue(builder.application);
        if (builder.location != null) {
            new LocationTracking(builder.application, builder.location, builder.locationInterval);
        }
        lixiSocket = new LixiSocket(builder.socketProvider, builder.deepLink, builder.branch);
        builder.application.unregisterActivityLifecycleCallbacks(lixiSocket);
        builder.application.registerActivityLifecycleCallbacks(lixiSocket);
    }

    public static synchronized LixiAnalytics get() {
        if (lixiAnalytics == null) {
            throw new NullPointerException("Build LixiAnalytics in Application");
        }
        return lixiAnalytics;
    }

    public SocketQueryBuilder socket() {
        return new SocketQueryBuilder(lixiSocket);
    }

    public <T> SocketTrackBuilder track(Class<T> clazz, HashMap<String, Object> object) {
        return new SocketTrackBuilder(clazz, object, lixiSocket);
    }

    public SocketTrackBuilder track(HashMap<String, Object> object) {
        return new SocketTrackBuilder(object, lixiSocket);
    }

    public SocketTrackBuilder track(Object object) {
        return new SocketTrackBuilder(object, lixiSocket);
    }

    public LixiPostBuilder createPost(String url) {
        return new LixiPostBuilder(url, requestQueue);
    }

    public static class LixiConfigBuilder {
        private int retryEmmit = 0;
        private final String baseUrl;

        public LixiConfigBuilder(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public LixiConfigBuilder addDefaultRetryEmmit(int retryEmmit) {
            this.retryEmmit = retryEmmit;
            return this;
        }

        public int getDefaultRetryEmmit() {
            return retryEmmit;
        }

        public String getBaseUrl() {
            return baseUrl;
        }
    }

    public static class LixiPostBuilder {
        private final String url;
        private final RequestQueue requestQueue;
        private HashMap<String, String> body;

        LixiPostBuilder(String url, RequestQueue requestQueue) {
            this.requestQueue = requestQueue;
            this.url = url;
        }

        public LixiPostBuilder setBody(HashMap<String, String> body) {
            if (this.body != null) {
                body.putAll(this.body);
                this.body = body;
            }
            return this;
        }

        public LixiPostBuilder addParam(String name, String value) {
            if (name != null && value != null && !name.trim().equals("") && !value.trim().equals("")) {
                if (body == null) {
                    body = new HashMap<>();
                }
                body.put(name, value);
            }
            return this;
        }

        public LixiPostBuilder clearBody() {
            body = null;
            return this;
        }

        public void post(@NonNull final PostCallBack postCallBack) {
            if (body == null) {
                postCallBack.onError("Empty body");
                return;
            }
            if (requestQueue != null ) {
                ShowLog.LogInfo("[Post]" + url + " [Body]" + new JSONObject(body).toString());
                // Instantiate the RequestQueue.
                StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                postCallBack.onResponse(response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        postCallBack.onError(error.getMessage());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        return body;
                    }

                };
                // Add the request to the RequestQueue.
                requestQueue.add(jsonObjectRequest);
            } else {
                postCallBack.onError("Add support RequestQueue in application for use post method");
            }
        }


    }

    public static class SocketTrackBuilder {
        private Integer autoRetry = null;
        private final LixiSocket lixiSocket;
        private String modelType;
        private @Size(min = 1, max = 2)
        String[] event;
        private Object objectData;

        SocketTrackBuilder(Object object, LixiSocket lixiSocket) {
            objectData = object;
            this.lixiSocket = lixiSocket;
        }

        SocketTrackBuilder(HashMap<String, Object> object, LixiSocket lixiSocket) {
            try {
                if (modelType == null) {
                    modelType = lixiSocket.getSocketConfig().defaultEmmitFormat();
                }
                switch (modelType) {
                    case EmmitFormat.JSON_OBJECT: {
                        objectData = new JSONObject(object);
                        break;
                    }
                    case EmmitFormat.JSON_STRING: {
                        objectData = new JSONObject(object).toString();
                        break;
                    }
                }
            } catch (Exception e) {
                ShowLog.LogError("Cannot create object to emmit " + e.getLocalizedMessage());
            }
            this.lixiSocket = lixiSocket;
        }

        SocketTrackBuilder(Class clazz,
                           HashMap<String, Object> object, LixiSocket lixiSocket) {
            try {
                if (modelType == null) {
                    modelType = lixiSocket.getSocketConfig().defaultEmmitFormat();
                }
                HashMap<String, Object> objectSummary = LixiPreferences.getInstance().getAllMap();
                objectSummary.putAll(object);
                switch (modelType) {
                    case EmmitFormat.JSON_OBJECT: {
                        objectData = new JSONObject(
                                Converters.objectToMap(Converters.mapToObject(clazz, objectSummary)));
                        break;
                    }
                    case EmmitFormat.JSON_STRING: {
                        objectData = Converters.objectToJson(Converters.mapToObject(clazz, objectSummary));
                        break;
                    }
                }
            } catch (Exception e) {
                ShowLog.LogError("Cannot create object to emmit " + e.getLocalizedMessage());
            }
            this.lixiSocket = lixiSocket;
        }

        public SocketTrackBuilder retry(int retryTimes) {
            this.autoRetry = retryTimes;
            return this;
        }

        public SocketTrackBuilder emmitType(String modelType) {
            this.modelType = modelType;
            return this;
        }

        @Nullable
        public Integer getAutoRetry() {
            return autoRetry;
        }

        public Object getObjectData() {
            return objectData;
        }

        public String[] getEvent() {
            return event;
        }

        public void emmit(@Size(min = 2, max = 2) String[] events) {
            if (objectData == null) {
                ShowLog.LogError("Object data emmit null");
                return;
            }
            this.event = events;
            this.lixiSocket.push(this);
        }

        public void emmit(String event) {
            if (objectData == null) {
                ShowLog.LogError("Object data emmit null");
                return;
            }
            this.event = new String[]{event};
            this.lixiSocket.push(this);
        }
    }

    public static class SocketQueryBuilder {

        private String query;
        private final LixiSocket lixiSocket;

        SocketQueryBuilder(LixiSocket lixiSocket) {
            this.lixiSocket = lixiSocket;
        }

        public SocketQueryBuilder saveConfig(int retry, int maxQueue, int minQueue) {
            LixiPreferences.getInstance().saveConfig(retry, maxQueue, minQueue);
            return this;
        }

        public SocketQueryBuilder addQuery(String name, String data) {
            addParam(name, data);
            return this;
        }

        public SocketQueryBuilder saveQuery(String name, String data) {
            if (name != null && !name.trim().equals("") && data != null && !data.trim().equals("")) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(name, data);
                LixiPreferences.getInstance().saveSocketParam(hashMap);
            }
            return this;
        }

        public SocketQueryBuilder removeSavedQuery(String key) {
            LixiPreferences.getInstance().clearSocketParam(key);
            return this;
        }

        public SocketQueryBuilder removeAllSavedQuery() {
            LixiPreferences.getInstance().clearAllSocketParam();
            return this;
        }

        private void addParam(String name, String data) {
            if (query == null || query.trim().equals("")) {
                query = "?";
            } else {
                query += "&";
            }
            if (name != null && !name.trim().equals("") && data != null && !data.trim().equals("")) {
                query += name.trim() + "=" + data.trim();
            } else {
                query = query.substring(0, query.length() - 1);
            }
        }

        public void connect() {
            lixiSocket.connectSocket(this);
        }

        public void disconnect() {
            lixiSocket.disconnectSocket();
        }

        public String getQuery() {
            HashMap<String, Object> paramSaved = LixiPreferences.getInstance().getSocketParam();
            for (Map.Entry<String, Object> entry : paramSaved.entrySet()
                    ) {
                addParam(entry.getKey(), String.valueOf(entry.getValue()));
            }
            return query == null ? "" : query;
        }
    }

    @SuppressWarnings("all")
    public static class Builder {

        private final Application application;
        private String log = null;
        private Long locationInterval;
        private SocketConfig socketConfig;
        private Class<? extends LixiSocketProvider> socketProvider;
        private Class<? extends LixiLocationListener> location;
        private Class<? extends LixiDeepLinkDetect> deepLink;
        private Class<? extends AppCompatActivity> laucherActivity;
        private Branch branch;

        public Builder(Application application) {
            this.application = application;
        }

        public Builder setLixiLog(String log) {
            this.log = log;
            return this;
        }

        public Builder addLocationListener(
                Class<? extends LixiLocationListener> location, long interval) {
            this.locationInterval = interval;
            this.location = location;
            return this;
        }

        public Builder addSupportDeepLink(Class<? extends LixiDeepLinkDetect> deepLink) {
            this.deepLink = deepLink;
            this.laucherActivity = laucherActivity;
            return this;
        }

        public Builder addSupportDeepLink(Class<? extends LixiDeepLinkDetect> deepLink, Branch branch) {
            this.branch = branch;
            this.deepLink = deepLink;
            this.laucherActivity = laucherActivity;
            return this;
        }

        public LixiAnalytics create(Class<? extends LixiSocketProvider> socketProvider) {
            this.socketProvider = socketProvider;
            if (!this.socketProvider.isAnnotationPresent(SocketConfig.class)) {
                throw new NullPointerException("" +
                        "Required annotation @SocketConfig in class" + socketProvider.getName());
            }
            LixiPreferences.getInstance(this.application);
            return lixiAnalytics = new LixiAnalytics(this);
        }
    }
}