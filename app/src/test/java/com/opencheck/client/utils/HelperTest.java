package com.opencheck.client.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelperTest {

    @Test
    void correctPhoneNumberSpace() {
        String incorrectNum = "0 123 123 123";
        String actualNum = Helper.correctPhoneNumber(incorrectNum);
        assertEquals("0123123123", actualNum);
    }


    @Test
    void correctPhoneNumberSpecialChar() {
        String incorrectNum = "0 sdfsdf 123 !@#$@#$#^%$&%^^&)(*^%^^$#@!#$^&$#$&*&%$#&(*(^%$#@#$&*((^%123 123";
        String actualNum = Helper.correctPhoneNumber(incorrectNum);
        assertEquals("0123123123", actualNum);
    }

    @Test
    void correctPhoneNumberVietnamPlus() {
        String incorrectNum = "(+84)123123123";
        String actualNum = Helper.correctPhoneNumber(incorrectNum);
        assertEquals("0123123123", actualNum);
    }


    @Test
    void correctSerialNumber() {
        String incorrectNum = "*100* 3243242sdfsdfsd3532523#";
        String actualNum = Helper.correctSerialNumber(incorrectNum);
        assertEquals("*100*3243242sdfsdfsd3532523#", actualNum);
    }

    @Test
    void getDaysBetweenTimestamp() {
    }

    @Test
    void getDifferentDay() {
    }

    @Test
    void getDifferentDay1() {
    }

    @Test
    void getFormatDateEngISO() {
    }

    @Test
    void getTimestamp() {
    }

    @Test
    void getTimeStampFromString() {
    }

    @Test
    void getVNCurrency() {
    }

    @Test
    void isValidPhone() {
        String num = "234309kfmldll";
        boolean isValid = Helper.isValidPhone(num);
        assertEquals(false, isValid);
    }

    @Test
    void isValidPhoneSpace() {
        String num = "234309325233";
        boolean isValid = Helper.isValidPhone(num);
        assertEquals(true, isValid);
    }

    @Test
    void isValidPhoneVietnam() {
        String num = "(+84)234309325233";
        boolean isValid = Helper.isValidPhone(num);
        assertEquals(true, isValid);
    }

    @Test
    void getFormatDateISO() {
    }
}