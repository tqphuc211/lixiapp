package com.opencheck.client.utils.load_image;

import android.content.Context;

public class ImageManager {
    Context context;
    String link = "";

    ImageManager(Context context) {
        this.context = context;
    }

    public ImageBuilder load(String link) {
        this.link = link;
        return new ImageBuilder(this);
    }
}
