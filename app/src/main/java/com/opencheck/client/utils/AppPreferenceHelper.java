package com.opencheck.client.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.flashsale.Model.RemindFlashSale;
import com.opencheck.client.home.flashsale.Service.OffsetTimer;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.models.user.UserModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class AppPreferenceHelper {
    private static volatile AppPreferenceHelper instance;

    public static AppPreferenceHelper getInstance() {
        synchronized (AppPreferenceHelper.class) {
            if (instance == null) {
                instance = new AppPreferenceHelper();
            }
        }
        return instance;
    }

    private SharedPreferences mAppSharedPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    @SuppressLint("CommitPrefEdits")
    public AppPreferenceHelper(Context context) {
        this.mAppSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.mPrefsEditor = mAppSharedPrefs.edit();
    }

    @SuppressLint("CommitPrefEdits")
    private AppPreferenceHelper() {
        this.mAppSharedPrefs = PreferenceManager.getDefaultSharedPreferences(LixiApplication.getInstance());
        this.mPrefsEditor = mAppSharedPrefs.edit();
    }

    //region store payment method
    public void setPaymentMethod(String s) {
        mPrefsEditor.putString("PaymentMethod", s);
        mPrefsEditor.commit();
    }

    public String getPaymentMethod() {
        return mAppSharedPrefs.getString("PaymentMethod", "");
    }

    public void setBankedSelected(String s) {
        mPrefsEditor.putString("BankSelected", s);
        mPrefsEditor.commit();
    }

    public String getBankedSelected() {
        return mAppSharedPrefs.getString("BankSelected", "");
    }
    //endregion

    public boolean isPinRequired() {
        return mAppSharedPrefs.getBoolean("PinRequired", false);
    }

    public void setPinRequired(boolean b) {
        mPrefsEditor.putBoolean("PinRequired", b);
        mPrefsEditor.commit();
    }

    public void setUserToken(String UserToken) {
        mPrefsEditor.putString("UserToken", UserToken);
        mPrefsEditor.commit();
    }

    public String getUserToken() {
//        App
        return "JWT " + mAppSharedPrefs.getString("UserToken", "");
    }

    public String getAccessToken() {
        return mAppSharedPrefs.getString("UserToken", "");
    }

    public void setIsSendPush(Boolean isRemember) {
        mPrefsEditor.putBoolean("IsSendPush", isRemember);
        mPrefsEditor.commit();
    }

    public Boolean getIsSendPush() {
        return mAppSharedPrefs.getBoolean("IsSendPush", false);
    }

    public void setIsPushSystem(Boolean isRemember) {
        mPrefsEditor.putBoolean("IsPushSystem", isRemember);
        mPrefsEditor.commit();
    }

    public Boolean getIsPushSystem() {
        return mAppSharedPrefs.getBoolean("IsPushSystem", false);
    }

    public String getPinCode() {
        return mAppSharedPrefs.getString("Pin_Code_App", "");
    }

    public void savePinCode(String Pin_Code_App) {
        mPrefsEditor.putString("Pin_Code_App", Pin_Code_App);
        mPrefsEditor.commit();
    }

    public String getPushToken() {
        return mAppSharedPrefs.getString("PushToken", "");
    }

    public void savePushToken(String PushToken) {
        mPrefsEditor.putString("PushToken", PushToken);
        mPrefsEditor.commit();
    }

    public void setReceivedReward(boolean received) {
        mPrefsEditor.putBoolean("ReceivedReward", received);
        mPrefsEditor.commit();
    }

    public boolean getIsReceivedReward() {
        return mAppSharedPrefs.getBoolean("ReceivedReward", false);
    }

    public void setIsNewUserTest() {
        mPrefsEditor.putBoolean("NewUser1", true);
        mPrefsEditor.putBoolean("NewUser2", true);
        mPrefsEditor.putBoolean("NewUser3", true);
        mPrefsEditor.putBoolean("NewUser4", true);
        mPrefsEditor.putBoolean("NewUser5", true);
        mPrefsEditor.commit();
    }

    public void setCampaignData(String cData) {
        mPrefsEditor.putString("CampaignData", cData);
        mPrefsEditor.commit();
    }

    public String getCampaignData() {
        return mAppSharedPrefs.getString("CampaignData", "");
    }

    public void setUpdateTokenInterval(Long timer) {
        mPrefsEditor.putLong("TimerInterval", timer);
        mPrefsEditor.commit();
    }

    public Long getUpdateTokenInterval() {
        return mAppSharedPrefs.getLong("TimerInterval", 0L);
    }


    public void setInstructionVisible(boolean isVisible) {
        mPrefsEditor.putBoolean("visible", isVisible);
        mPrefsEditor.commit();
    }

    public boolean getInstructionVisible() {
        return mAppSharedPrefs.getBoolean("visible", true);
    }

    public void setOnRefresh(boolean isRefresh) {
        mPrefsEditor.putBoolean(ConstantValue.ON_REFRESH, isRefresh);
        mPrefsEditor.commit();
    }

    public boolean getOnRefresh() {
        return mAppSharedPrefs.getBoolean(ConstantValue.ON_REFRESH, false);
    }

    // region Config
    public void setUserConfig(ConfigModel config) {
        Gson gson = new Gson();
        mPrefsEditor.putString("user_config", gson.toJson(config));
        mPrefsEditor.commit();
    }

    public ConfigModel getUserConfig() {
        Gson gson = new Gson();
        String data = mAppSharedPrefs.getString("user_config", "");
        if (data.isEmpty()) {
            return new ConfigModel();
        }
        return gson.fromJson(data, ConfigModel.class);
    }

    public void setTimeResetConfig(long time) {
        mPrefsEditor.putLong("time_reset_config", time);
        mPrefsEditor.commit();
    }

    public long getTimeResetConfig() {
        return mAppSharedPrefs.getLong("time_reset_config", 0);
    }

    public void setDeliConfig(ArrayList<DeliConfigModel> config) {
        Gson gson = new Gson();
        mPrefsEditor.putString("deli_config", gson.toJson(config));
        mPrefsEditor.commit();
    }

    public ArrayList<DeliConfigModel> getDeliConfig() {
        Gson gson = new Gson();
        String data = mAppSharedPrefs.getString("deli_config", "");
        if (data.isEmpty()) {
            return new ArrayList<DeliConfigModel>();
        }
        Type listType = new TypeToken<ArrayList<DeliConfigModel>>() {
        }.getType();
        ArrayList<DeliConfigModel> list = gson.fromJson(data, listType);
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setTimeResetDeliConfig(long time) {
        mPrefsEditor.putLong("time_reset_deli_config", time);
        mPrefsEditor.commit();
    }

    public long getTimeResetDeliConfig() {
        return mAppSharedPrefs.getLong("time_reset_deli_config", 0);
    }
    // endregion

    public void setHistorySearch(String cData) {
        mPrefsEditor.putString("HistorySearch", cData);
        mPrefsEditor.commit();
    }

    public String getHistorySearch() {
        return mAppSharedPrefs.getString("HistorySearch", "");
    }

    public void setHistorySearchDeli(String tag) {
        if (tag == null || tag.equals("")) {
            return;
        }

        List<String> listTag = getHistorySearchDeli();
        String savedTags = mAppSharedPrefs.getString("HistorySearchDeli", "");
        if (savedTags.equals("")) {
            mPrefsEditor.putString("HistorySearchDeli", tag);
        } else {
            if (listTag.contains(tag)) {
                savedTags = savedTags.replace(tag, "").replace("||", "|");
                if (savedTags.length() > 0) {
                    if (String.valueOf(savedTags.charAt(savedTags.length() - 1)).equals("|")) {
                        savedTags = savedTags.substring(0, savedTags.length() - 1);
                    }
                }
            } else {
                if (listTag.size() >= 10) {
                    savedTags = savedTags.substring(0, savedTags.lastIndexOf("|"));
                }
            }
            mPrefsEditor.putString("HistorySearchDeli", tag + "|" + savedTags);
        }
        mPrefsEditor.commit();
    }

    public List<String> getHistorySearchDeli() {
        return convertStrings(mAppSharedPrefs.getString("HistorySearchDeli", ""));
    }

    public List<String> convertStrings(String data) {
        List<String> result = new LinkedList<>();
        if (data == null || data.equals("")) {
            return result;
        } else {
            if (data.contains("|")) {
                return Arrays.asList(data.split("\\|"));
            } else {
                result.add(data);
                return result;
            }
        }
    }

    public void setIsFirstUseCODPayment(Boolean firstUseCod) {
        mPrefsEditor.putBoolean("FirstUseCODPayment", firstUseCod);
        mPrefsEditor.commit();
    }

    public Boolean getIsFirstUseCodPayment() {
        return mAppSharedPrefs.getBoolean("FirstUseCODPayment", true);
    }

    public void setUtm(SocketUtmModel socketUtmModel) {
        Gson gson = new Gson();
        String json = gson.toJson(socketUtmModel);
        mPrefsEditor.putString("UTM", json);
        mPrefsEditor.commit();
    }

    public SocketUtmModel getUtm() {
        Gson gson = new Gson();
        String json = mAppSharedPrefs.getString("UTM", "{}");
        if (json.equals("{}"))
            return null;
        return gson.fromJson(json, SocketUtmModel.class);
    }

    public void clear() {
        Analytics.trackLogout();
        mPrefsEditor.clear().commit();
    }

    //region FLASH SALE
    public long getOffsetTime() {
        return mAppSharedPrefs.getLong(OffsetTimer.OFFSET_TIME, -1);
    }

    public void setOffsetTime(long offsetTime) {
        mPrefsEditor.putLong(OffsetTimer.OFFSET_TIME, offsetTime);
        mPrefsEditor.commit();
    }

    public void clearRemind(long id) {
        ArrayList<RemindFlashSale> list = getRemind();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getIdFlashSale() * list.get(i).getIdProduct() == id) {
                list.remove(i);
                break;
            }
        }
        clearRemind();
        setRemind(list);
    }

    public void clearRemind() {
        mPrefsEditor.remove(RemindFlashSale.HEADER_REMIND);
        mPrefsEditor.commit();
    }

    public void setRemind(RemindFlashSale remindFlashSale) {
        Gson gson = new Gson();
        ArrayList<RemindFlashSale> listTemp = getRemind();
        listTemp.add(remindFlashSale);
        mPrefsEditor.putString(RemindFlashSale.HEADER_REMIND, gson.toJson(listTemp));
        mPrefsEditor.commit();
    }

    public void setRemind(ArrayList<RemindFlashSale> list) {
        Gson gson = new Gson();
        ArrayList<RemindFlashSale> listTemp = getRemind();
        listTemp.addAll(list);
        mPrefsEditor.putString(RemindFlashSale.HEADER_REMIND, gson.toJson(listTemp));
        mPrefsEditor.commit();
    }

    public ArrayList<RemindFlashSale> getRemind() {
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<RemindFlashSale>>() {
        }.getType();
        ArrayList<RemindFlashSale> listTemp = gson.fromJson(mAppSharedPrefs.getString(RemindFlashSale.HEADER_REMIND, ""), listType);
        if (listTemp == null) {
            listTemp = new ArrayList<>();
        }
        return listTemp;
    }
    //endregion

    //region DELI
    public void setDefaultPhoneDeli(String cData) {
        mPrefsEditor.putString("DefaultPhoneDeli", cData);
        mPrefsEditor.commit();
    }

    public void setDefaultLocation(DeliAddressModel location) {
        Gson gson = new Gson();
        String json = gson.toJson(location);
        mPrefsEditor.putString("DEFAULT_LOCATION", json);
        mPrefsEditor.commit();
    }

    public DeliAddressModel getDefaultLocation() {
        Gson gson = new Gson();
        String json = mAppSharedPrefs.getString("DEFAULT_LOCATION", "");
        return gson.fromJson(json, DeliAddressModel.class);
    }

    public String getShipReceiveTime() {
        return mAppSharedPrefs.getString("timestamp", "");
    }

    public void setShipReceiveTime(String time) {
        mPrefsEditor.putString("timestamp", time);
        mPrefsEditor.commit();
    }

    public String getDefaultPhoneDeli() {
        return mAppSharedPrefs.getString("DefaultPhoneDeli", "0901 863 639");
    }

    public void setOnLocationRefresh(boolean isRefresh) {
        mPrefsEditor.putBoolean(ConstantValue.ON_REFRESH, isRefresh);
        mPrefsEditor.commit();
    }

    public boolean getOnLocationRefresh() {
        return mAppSharedPrefs.getBoolean(ConstantValue.ON_REFRESH, false);
    }

    public boolean isStartActivityFromDeepLink() {
        return !mAppSharedPrefs.getBoolean("isDeepLink", false);
    }

    public void setStartActivityFromDeepLink(boolean isFromDeepLink) {
        mPrefsEditor.putBoolean("isDeepLink", isFromDeepLink);
        mPrefsEditor.commit();
    }

    public void setLastAnimationAddress() {
        mPrefsEditor.putLong("last_animation", System.currentTimeMillis());
        mPrefsEditor.commit();
    }

    public long getLastAnimationAddress() {
        return mAppSharedPrefs.getLong("last_animation", 0);
    }

    //endregion

    public void setRegion(int region) {
        mPrefsEditor.putInt("region", region);
        mPrefsEditor.commit();
    }

    public int getRegion() {
        return mAppSharedPrefs.getInt("region", 65);
    }

    private final String SAVED_BANK_CARD = "saved bank card";

    public void setTokenBankCard(TokenData tokenData) {
        ArrayList<TokenData> listTemp = getTokenBankCard();
        if (listTemp == null) {
            listTemp = new ArrayList<>();
        }

        listTemp.add(tokenData);
        String strTemp = (new Gson()).toJson(listTemp);
        mPrefsEditor.putString(SAVED_BANK_CARD, strTemp);
        mPrefsEditor.commit();
    }

    public void setTokenBankCard(ArrayList<TokenData> listTokenData) {
        if (listTokenData != null && listTokenData.size() != 0) {
            mPrefsEditor.putString(SAVED_BANK_CARD, (new Gson()).toJson(listTokenData));
        } else {
            mPrefsEditor.putString(SAVED_BANK_CARD, "");
        }
        mPrefsEditor.commit();
    }

    public ArrayList<TokenData> getTokenBankCard() {
        String temp = mAppSharedPrefs.getString(SAVED_BANK_CARD, "");
        if (!temp.equals("")) {
            Type listType = new TypeToken<ArrayList<TokenData>>() {
            }.getType();
            return (new Gson()).fromJson(temp, listType);
        } else {
            return null;
        }
    }

    public ArrayList<TokenData> getTokenBankCard(String key) {
        ArrayList<TokenData> listTokenData = null;
        String temp = mAppSharedPrefs.getString(SAVED_BANK_CARD, "");
        if (!temp.equals("")) {
            Type listType = new TypeToken<ArrayList<TokenData>>() {
            }.getType();
            listTokenData = (new Gson()).fromJson(temp, listType);
        } else {
            return null;
        }
        // Lọc theo key
        for (int i = listTokenData.size() - 1; i >= 0; i--) {
            if (!key.equals(listTokenData.get(i).getCard().getBank_code())) {
                listTokenData.remove(i);
            }
        }

        return listTokenData;
    }

    public void removeToken(TokenData token) {
        ArrayList<TokenData> listTemp = getTokenBankCard();
        if (listTemp != null) {
            for (int i = 0; i < listTemp.size(); i++) {
                if (listTemp.get(i).getCard().getNumber().equals(token.getCard().getNumber())) {
                    listTemp.remove(i);
                    break;
                }
            }
        }
        setTokenBankCard(listTemp);
    }

    public void setLanguage(String languageString) {
        try {
            mPrefsEditor.putString("Language", languageString);
            mPrefsEditor.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Map<String, String> getLanguage() {
        try {
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            return new Gson().fromJson(mAppSharedPrefs.getString("Language", "{ }"),
                    type);
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    public void setLanguageSource(String lang) {
        mPrefsEditor.putString("Language_Source", lang);
        mPrefsEditor.commit();
    }

    public String getLanguageSource() {
        return mAppSharedPrefs.getString("Language_Source", "");
    }

    public void setProvinceSource(String lang) {
        mPrefsEditor.putString("Province_Source", lang);
        mPrefsEditor.commit();
    }

    public String getProvinceSource() {
        return mAppSharedPrefs.getString("Province_Source", "HCM");
    }

    public void setRemindUpdateTime(long time) {
        mPrefsEditor.putLong("remind update time", time);
        mPrefsEditor.commit();
    }

    public long getRemindUpdateTime() {
        return mAppSharedPrefs.getLong("remind update time", -1);
    }

    public void setUpdateAddressTime(long time) {
        mPrefsEditor.putLong("update address time", time);
        mPrefsEditor.commit();
    }

    public long getUpdateAddressTime() {
        return mAppSharedPrefs.getLong("update address time", -1);
    }

    public void setUserModel(UserModel model) {
        Gson gson = new Gson();
        mPrefsEditor.putString("user_model", gson.toJson(model));
        mPrefsEditor.commit();
    }

    public UserModel getUserModel() {
        Gson gson = new Gson();
        String data = mAppSharedPrefs.getString("user_model", "");
        if (data.isEmpty()) {
            return null;
        }
        return gson.fromJson(data, UserModel.class);
    }

    public void clearUserModel() {
        mPrefsEditor.remove("user_model");
        mPrefsEditor.commit();
    }

    public void setLixiTrackerId(String tracker) {
        mPrefsEditor.putString("LixiTrackerId", tracker);
        mPrefsEditor.commit();
    }

    public String getLixiTrackerId() {
        return mAppSharedPrefs.getString("LixiTrackerId", "");
    }
}
