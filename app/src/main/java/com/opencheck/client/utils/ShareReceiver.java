package com.opencheck.client.utils;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.opencheck.client.utils.tracking.GlobalTracking;

public class ShareReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getBooleanExtra(ConstantValue.SHARE_TRACKING, false)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                ComponentName target = intent.getParcelableExtra(Intent.EXTRA_CHOSEN_COMPONENT);
                GlobalTracking.trackReferenceCodeShareMethodEvent(target.getPackageName());
            }
        }
    }
}
