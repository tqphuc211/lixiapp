package com.opencheck.client.utils.tracking;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public final class FirebaseTracker {

    private volatile static FirebaseTracker mInstance = null;
    private FirebaseAnalytics analytics;

    private FirebaseTracker(Context context) {
        analytics = FirebaseAnalytics.getInstance(context);
    }

    public static synchronized void initialize(Context context) {
        if (mInstance != null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }

        mInstance = new FirebaseTracker(context);
    }

    public static synchronized FirebaseTracker getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("Call initialize() before getInstance()");
        }

        return mInstance;
    }

    public void trackerBeginCheckoutEvent(String orderId, long totalMoney, String paymentMethod, String promotionCode) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.ITEM_ID, orderId);
        data.putString(FirebaseAnalytics.Param.CURRENCY, "VNĐ");
        data.putLong(FirebaseAnalytics.Param.PRICE, totalMoney);
        data.putString("payment_method", paymentMethod);
        if (promotionCode != null)
            data.putString(FirebaseAnalytics.Param.COUPON, promotionCode);
        analytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, data);
    }

    void trackLoginStartEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        analytics.logEvent("login_start", data);
    }

    void trackPhoneVerify(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        analytics.logEvent("login_phone_verified", data);
    }

    void trackSignUpComplete(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        analytics.logEvent("completed_registration", data);
    }

    void trackLoggedInEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        analytics.logEvent(FirebaseAnalytics.Event.LOGIN, data);
    }

    void trackStartUpdateInfo() {
        analytics.logEvent("update_information_start", null);
    }

    void trackSubmitUpdateInfoEvent() {
        analytics.logEvent("update_information_submit", null);
    }

    void trackUpdateInfoSuccess() {
        analytics.logEvent("update_information_success", null);
    }

    void trackLoggedOutEvent() {
        analytics.logEvent("logged_out", null);
    }

    void trackReferenceCodePopupImpressionsEvent() {
        analytics.logEvent("reference_code_popup_impressions", null);
    }

    void trackReferenceCodePopupAccepts() {
        analytics.logEvent("reference_code_popup_accepts", null);
    }

    void trackScreenReferenceCodeStartsEvent(boolean isLogin) {
        Bundle data = new Bundle();
        data.putBoolean(GlobalTracking.CHECK_LOGIN, isLogin);
        analytics.logEvent("reference_code_screen_starts", data);
    }

    void trackReferenceCodeShareEvent() {
        analytics.logEvent("reference_code_share_starts", null);
    }

    void trackReferenceCodeShareMethodEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        analytics.logEvent("reference_code_share_method", data);
    }

    void trackReferenceCodeSubmitEvent() {
        analytics.logEvent("reference_code_submit", null);
    }

    void trackReferenceCodeSuccessEvent() {
        analytics.logEvent("reference_code_success", null);
    }

    void trackEventSearch(String contentType, String search, String keyword, boolean result) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putString(FirebaseAnalytics.Param.SEARCH_TERM, search);
        data.putString(GlobalTracking.SEARCH, keyword);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        analytics.logEvent(FirebaseAnalytics.Event.SEARCH, data);
    }

    void trackShowDetail(String contentType, long id, String currency, double price, String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, data);
    }

    void trackStartCheckout(String contentType, long id, String source, double price) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString(GlobalTracking.SOURCE, source);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        analytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, data);
    }

    void trackUpdateCheckout(String contentType,
                             long id,
                             int numItems,
                             boolean availableInfo,
                             String currency,
                             String paymentMethod,
                             String coupon,
                             boolean result,
                             String trans_id,
                             double price,
                             String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean("payment_info_available", availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        data.putString(GlobalTracking.TRANS_ID, trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("update_checkout_order", data);
    }

    void trackCheckedOut(String contentType,
                         long id,
                         int numItems,
                         boolean availableInfo,
                         String currency,
                         String paymentMethod,
                         String coupon,
                         boolean result,
                         String trans_id,
                         double price,
                         String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("checked_out", data);
    }

    void trackPromotionCodeStart(long id) {
        Bundle data = new Bundle();
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        analytics.logEvent("promotion_code_starts", data);
    }

    void trackPromotionCodeSearch(long id, String search, boolean result) {
        Bundle data = new Bundle();
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString(FirebaseAnalytics.Param.SEARCH_TERM, search);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        analytics.logEvent(FirebaseAnalytics.Event.SEARCH, data);
    }

    void trackPromotionCodeUsed(long id, String promotion_code) {
        Bundle data = new Bundle();
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString(GlobalTracking.PROMOTION_CODE, promotion_code);
        analytics.logEvent("promotion_code_used", data);
    }

    void trackStoreImpression(long id, String type) {
        Bundle data = new Bundle();
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString("type", type);
        analytics.logEvent("store_popup_impression", data);
    }

    void trackPopupAccept(long id, String type, String action) {
        Bundle data = new Bundle();
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putString(GlobalTracking.TYPE, type);
        data.putString(GlobalTracking.ACTION, action);
        analytics.logEvent("store_popup_accepts", data);
    }

    void trackServiceStart(String type) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        analytics.logEvent("service_starts", data);
    }

    void trackServiceBegin(String type, double denominations, String currency) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        analytics.logEvent("service_begin_checkout", data);
    }

    void trackServiceCheckedOut(String type, double denominations, String currency, double price) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        analytics.logEvent("service_checked_out", data);
    }

    void trackServicePurchased(String type, double denominations, String currency, double price) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        analytics.logEvent("service_purchased", data);
    }

    void trackPurchaseComplete(String contentType,
                               long id,
                               int numItems,
                               boolean availableInfo,
                               String currency,
                               String paymentMethod,
                               String coupon,
                               boolean result,
                               String trans_id,
                               double price,
                               String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("ecommerce_purchase", data);
    }

    void trackOrderComplete(String contentType,
                            long id,
                            int numItems,
                            boolean availableInfo,
                            String currency,
                            String paymentMethod,
                            String coupon,
                            boolean result,
                            String trans_id,
                            double price,
                            String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, result);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("order_completed", data);
    }

    void trackCancelPurchase(String contentType,
                             long id,
                             int numItems,
                             boolean availableInfo,
                             String currency,
                             String paymentMethod,
                             String coupon,
                             String trans_id,
                             double price,
                             String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("purchase_canceled", data);
    }

    void trackRepayOrder(String contentType,
                         long id,
                         int numItems,
                         boolean availableInfo,
                         String currency,
                         String paymentMethod,
                         String coupon,
                         String trans_id,
                         double price,
                         String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("purchase_repay_start", data);
    }

    void trackRepaySuccess(String contentType,
                           long id,
                           int numItems,
                           boolean availableInfo,
                           String currency,
                           String paymentMethod,
                           String coupon,
                           String trans_id,
                           double price,
                           String source) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        data.putLong(FirebaseAnalytics.Param.ITEM_ID, id);
        data.putInt(FirebaseAnalytics.Param.QUANTITY, numItems);
        data.putBoolean(FirebaseAnalytics.Param.SUCCESS, availableInfo);
        data.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putDouble(FirebaseAnalytics.Param.VALUE, price);
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("purchase_repay_success", data);
    }

    void trackGameStart(String source) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SOURCE, source);
        analytics.logEvent("event_starts", data);
    }

    void trackGameShare(String contentType) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        analytics.logEvent(FirebaseAnalytics.Event.SHARE, data);
    }

    void trackGameShareSuccess(String contentType) {
        Bundle data = new Bundle();
        data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        analytics.logEvent("share_success", data);
    }

    void trackGameOpenTabbar(String source, int index) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SOURCE, source);
        data.putInt(GlobalTracking.INDEX, index);
        analytics.logEvent("open_tabbar", data);
    }
}