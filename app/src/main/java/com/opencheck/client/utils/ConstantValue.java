package com.opencheck.client.utils;

import android.os.CountDownTimer;

import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.models.user.ImageModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class ConstantValue {
    public static final String SENDER_ID = "963212786907";
    public static final String LOG_INFO_TAG = "mapi";
    public static final String PUSH_OC_RECEIVE = "PUSH_OC_RECEIVE";
    public static final String PUSH_EVOUCHER_ACTIVE = "PUSH_EVOUCHER_ACTIVE";
    public static final String PUSH_UPDATE_NOTIFICATION = "PUSH_UPDATE_NOTIFYCATION";
    public static final String NOTIFY_MODEL_FILE = "NOTIFY_MODEL_FILE";
    public static final String IS_SUCCESS = "IS_SUCCESS";
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String PUSH_SYSTEM_INTENT = "PUSH_SYSTEM_INTENT";
    public static final String PUSH_SURVEY = "PUSH_SURVEY";
    public static final String DEFAULT_LOG_TAG = "mInfo";
    public static final long AUTO_DISMISS_INTERVAL = 2000;
    public static final int LOGIN_REQUEST_CODE = 100;
    public static final int APP_REQUEST_CODE = 99;
    public static final int REQUEST_CODE_VERIFY_PHONE = 101;
    public static final int REQUEST_CODE_CALL = 102;
    public static final int REQUEST_CODE_PERMISSON = 103;
    public static final int PIN_REQUEST_CODE = 104;
    public static final int REQUEST_CODE_CAMERA_CHANGING_AVATAR = 105;
    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_CHANGING_AVATAR = 106;
    public static final int REQUEST_CODE_READ_CONTACTS = 107;
    public static final int REQUEST_CODE_READ_CONTACTS_PERMISSION = 108;
    public static final int REQUEST_CODE_CAMERA_QR = 109;
    public static final int REQUEST_CODE_COARSE_LOCATION = 110;
    public static final int REQUEST_CODE_FINE_LOCATION = 111;
    public static final int REQUEST_CODE_GPS = 112;
    public static final int REQUEST_CODE_SHARE_LINK = 114;
    public static final int GET_LAST_ORDER_DATA = 115;
    public static final int REQUEST_LIST_PROMOTION = 116;
    public static final int REQUEST_FAVORITE_STORE = 117;

    public static final String DEEP_LINK_INTENT = "DEEP_LINK_INTENT";
    public static final String FILTER_MODEL_MERCHANT = "FILTER_MODEL_MERCHANT";
    public static final String SERVICE_CATEGORY_CHILD_MODEL = "SERVICE_CATEGORY_CHILD_MODEL";
    public static final String MERCHANT_DETAIL_MODEL = "MERCHANT_DETAIL_MODEL";
    public static final String CURRENT_PRODUCT_ID = "current product id";


    public static final String ACTION_AFTER_LOGIN = "ACTION_AFTER_LOGIN";
    public static final String ACTION_TOPUP = "ACTION_TOPUP";
    public static final String ACTION_TAB_HISTORY = "ACTION_TAB_HISTORY";
    public static final String ACTION_TAB_SETTING = "ACTION_TAB_SETTING";
    public static final String ACTION_TAB_ACCOUNT = "ACTION_TAB_ACCOUNT";
    public static final String ACTION_BUY_CARD = "ACTION_BUY_CARD";
    public static final String ACTION_SHOW_REWARD_CODE = "ACTION_SHOW_REWARD_CODE";
    public static final String ACTION_EXCHANGE_CLASSIC_PRODUCT = "ACTION_EXCHANGE_CLASSIC_PRODUCT";
    public static final String ACTION_EXCHANGE_EVOUCHER = "ACTION_EXCHANGE_EVOUCHER";
    public static final String ACTION_BUY_EVOUCHER = "ACTION_BUY_EVOUCHER";
    public static final String ACTION_FAVORITE_PROMOTION = "ACTION_FAVORITE_PROMOTION";
    public static final String ACTION_FOR_PIN = "ACTION_FOR_PIN";
    public static final String ACTION_OPEN_APP = "ACTION_OPEN_APP";
    public static final String ACTION_CHANGE_PIN = "ACTION_CHANGE_PIN";
    public static final String ACTION_TURN_ON_PIN = "ACTION_TURN_ON_PIN";
    public static final String ACTION_ORDER_DELI = "ACTION_ORDER_DELI";
    public static final String ACTION_SAVE_LOCATION = "ACTION_SAVE_LOCATION";
    public static final String ACTION_NOTIFICATION = "ACTION_NOTIFICATION";
    public static final String ACTION_INTRO_CODE = "ACTION_INTRO_CODE";
    public static final String ACTION_PLAY_GAME = "ACTION_PLAY_GAME";
    public static final String ACTION_VIEW_ORDER = "ACTION_VIEW_ORDER";

    public static final String IS_COMBO_ITEM = "IS_COMBO_ITEM";
    public static final String IS_COMBO = "IS_COMBO";

    public static final String FORMAT_TIME_FULL = "EEE MMM dd HH:mm:ss zXXX yyyy";
    public static final String FORMAT_TIME = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_TIME_NO_SECONDS = "dd/MM/yyyy HH:mm";
    public static final String FORMAT_TIME_DATE = "dd-MM-yyyy";
    public static final String FORMAT_TIME_Y = "dd/MM/yyyy";
    public static final String STORE_LIST = "STORE_LIST";
    public static final String FORMAT_TIME_ORDERS = "dd/MM/yyyy - HH:mm:ss";
    public static final String FORMAT_TIME_ORDER = "dd-MM-yyyy HH:mm:ss";
    public static final String FORMAT_TIME_ORDER_WITHOUT_SECONDS = "HH:mm dd/MM/yyyy";
    public static final String FORMAT_TIME_MONTH_YEAR = "MM/yyyy";
    public static final String FORMAT_TIME_ORDER_OLD = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_TIME_REMAIN = "yyyy-MM-dd";
    public static final String FORMAT_TIME_TRACKING = "HH:mm";
    public static final String FORMAT_TIME_PROMOTION = "HH:mm:ss";

    public static final String SEARCH_ADDRESS_CALLBACK = "SEARCH_ADDRESS_CALLBACK";
    public static final String MAP_MARKER_CLICK = "MAP_MARKER_CLICK";
    public static final String STORE_ADDRESS_CLICK_DIALOG = "STORE_ADDRESS_CLICK_DIALOG";
    public static final String NOTIFY_ACTIVE = "activity";
    public static final String MERCHANT_MODEL = "MERCHANT_MODEL";

    public static final String ORDER_LIST_CODE = "ORDER_LIST_CODE";
    public static final String LIST_EVOUCHER_INFO = "list evoucher info";
    public static final String EVOUCHER_ID = "EVOUCHER_ID";
    public static final String TIMER_OUT = "TIMER_OUT";
    public static final String TIMER = "TIMER";
    public static final String ON_REFRESH = "ON_REFRESH";
    public static final String ON_MAP_CLICK = "ON_MAP_CLICK";
    public static final String ON_ITEM_STORE_CLICK = "ON_ITEM_STORE_CLICK";
    public static final String ON_ITEM_CLICK = "ON_ITEM_CLICK";
    public static final String ACTION_FINISH_FRAGMENTS = "com.opencheck.client.home.account.activecode.ACTION_FINISH_RECEIVER2";
    public static final String KEY_ALBUM = "album_name";
    public static final String KEY_PATH = "path";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_TIME = "date";
    public static final String KEY_COUNT = "date";
    public static final String ACTION_LOGIN_QR = "ACTION_LOGIN_QR";
    public static final String SOCKET_OUT_ACTION = "out";
    public static final String SOCKET_IN_ACTION = "in";
    public static final String ADD_CLICK = "ADD_CLICK";
    public static final String SUB_CLICK = "SUB_CLICK";
    public static final String CART_FILE = "CART_FILE";
    public static final String PRODUCT_FILE = "PRODUCT_FILE";
    public static final String STORE_FILE = "STORE_FILE";
    public static String PHOTO_NAME = "photo.jpg";
    public static Long ID = 0L;
    public static ArrayList<ImageModel> image = new ArrayList<>();
    public static HashMap<String, Boolean> map = new HashMap<>();
    public static HashMap<String, CountDownTimer> timer = new HashMap<>();

    // TODO: For New EVoucher
    public static final String CURRENT_POSITION = "current position";
    public static final String POSITION_IN_LIST = "position in list";
    public static final String TOTAL_CAN_ACTIVE = "total can active";
    public static final String CURRENT_PAGE = "current page";
    public static final String STATE_VOUCHER = "state voucher";

    public static final int VOICE_SEARCH_CODE = 1478;

    public static boolean USER_MULTI_ACTIVE = true;

    public static final String STATUS_WAITING = "waiting";
    public static final String STATUS_USED = "used";
    public static final String STATUS_EXPIRED = "expired";
    public static final String STATUS_CAN_ACTIVE = "can_active";

    public static final String LOGIN_PHONE = "login phone";
    public static final int LOGIN_RESULT_CODE = 1756;
    public static final String LOGIN_RESULT = "login resutl";
    public static final int RESULT_NO_PROFILE = 1005;

    public static final String LANG_CONFIG_DEF = "def";
    public static final String LANG_CONFIG_VN = "vi";
    public static final String LANG_CONFIG_EN = "en";

    public static final String[] DEFAULT_IMAGE = new String[]{"https://static.lixiapp.com/oc_gallery/2018/10/15/97cbbafccb39d54ad2ecdb3ac5ee554b1daa8d38.png"};

    public static SocketUtmModel SOCKET_UTM_MODEL = null;

    public static final String SHARE_TRACKING = "request_share";
    public static final String LAST_ORDER_TRANS_ID = "trans_id";
    public static final String LAST_ORDER_PAYMENT_METHOD = "payment_method";
    public static final String LAST_ORDER_PROMOTION = "promotion";
    public static final String LAST_ORDER_MONEY = "money";

    public static final long TIME_RESET_CONFIG = 30 * 60 * 1000;
}
