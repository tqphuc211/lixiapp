package com.opencheck.client.utils.load_image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.utils.load_image.interfaces.OnResultBitmap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ImageBuilder {
    private String link;
    private Context context;
    private DisplayImageOptions options = null;
    private ImageLoadingListener listener = null;

    ImageBuilder(ImageManager imageManager) {
        this.context = imageManager.context;
        this.link = imageManager.link;
    }

    /**
     * Bỏ qua là lấy ảnh gốc(DEFAULT)
     *
     * @param size {@link ImageSize}
     * @return This builder
     */
    @NonNull
    public ImageBuilder size(ImageSize size) {
        if (size != ImageSize.DEFAULT) {
            if (link.contains("lixiapp.com")) {
                if (!link.contains(ImageSize.MEDIUM.getValue()) && !link.contains(ImageSize.SMALL.getValue())) {
                    if (size == ImageSize.AUTO) {
                        size = displayResolution();
                    }
                    int extension = link.lastIndexOf('.');
                    this.link = link.substring(0, extension) + size.getValue() + link.substring(extension);
                }
            }
        }
        return this;
    }

    /**
     * option ảnh placeholder, load fail,...
     *
     * @param options
     * @return
     */
    public ImageBuilder options(DisplayImageOptions options) {
        this.options = options;
        return this;
    }

    public ImageBuilder listener(ImageLoadingListener listener) {
        this.listener = listener;
        return this;
    }

    /**
     * Bắt đầu load ảnh
     *
     * @param img ImageView
     */
    public void into(ImageView img) {
        if (options == null) {
            ImageLoader.getInstance().displayImage(link, img, LixiApplication.getInstance().optionsNomal, listener);
        } else {
            ImageLoader.getInstance().displayImage(link, img, options, listener);
        }
    }

    /**
     * Dành cho item của bottom navigationbar
     *
     * @param item
     */
    public void into(final MenuItem item) {
        if (link.equals("")) {
            return;
        }

        GetBitmapTask task = new GetBitmapTask(new OnResultBitmap() {
            @Override
            public void onResult(Bitmap bitmap) {
                if (bitmap != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap);
                    item.setIcon(bitmapDrawable);
                }
            }
        });
        task.execute(link);
    }

    public void getBitmap(OnResultBitmap onResult) {
        if (link.equals("")) {
            return;
        }

        GetBitmapTask task = new GetBitmapTask(onResult);
        task.execute(link);
    }

    // hdpi: 1.5, 2k: 3.5
    private ImageSize displayResolution() {
        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        if (displaymetrics.density <= 2.5) {
            return ImageSize.SMALL;
        } else {
            return ImageSize.MEDIUM;
        }
    }

    private class GetBitmapTask extends AsyncTask<String, String, Bitmap> {
        private OnResultBitmap onResultBitmap;

        public GetBitmapTask(OnResultBitmap onResultBitmap) {
            this.onResultBitmap = onResultBitmap;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                return BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            onResultBitmap.onResult(bitmap);
        }
    }
}
