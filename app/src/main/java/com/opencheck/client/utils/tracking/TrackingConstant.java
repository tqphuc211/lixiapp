package com.opencheck.client.utils.tracking;

public class TrackingConstant {
    public static class ContentType {
        public static final String STORE = "Store";
        public static final String EVOUCHER = "Evoucher";
        public static final String REWARD = "Reward";
        public static final String MERCHANT = "Merchant";
        public static final String FOOD = "Food";
        public static final String DELI_COLLECTION = "Delivery Collection";
        public static final String VOUCHER_COLLECTION = "Voucher Collection";
        public static final String DELI_CATEGORY = "Delivery Category";
        public static final String VOUCHER_CATEGORY = "Voucher Category";
        public static final String REFERENCE_CODE = "Reference Code";
        public static final String GAME = "Game";
    }

    public static class Currency {
        public static final String VND = "VND";
        public static final String USD = "USD";
    }

    public static class BaseAction {
        public static final String ACCEPT = "Accept";
        public static final String CANCEL = "Cancel";
        public static final String COUNTINUE = "Continue";
    }

    public static class CouponType {
        public static final String PROMOTION_CODE = "Promotion Code";
        public static final String LIXI_DISCOUNT = "Lixi Discount";
    }

    public static class PaymentMethod {
        public static final String COD = "cod";
        public static final String ONLINE = "Online";
        public static final String WALLET = "Wallet";
        public static final String AT_STORE = "AtStore";
    }

    public static class Login {
        public static final String FACEBOOK = "Facebook";
        public static final String ACCOUNT_KIT = "AccountKit";
    }

    public static class Service {
        public static final String TOPUP = "Topup";
        public static final String PHONE_CARD = "Phone Card";
    }

    public static class ContentSource {
        public static final String PUSH_NOTIFICATION = "Direct Notification";
        public static final String LIST_NOTIFICATION = "List Notification";
        public static final String DEEP_LINK = "Deeplink";
        public static final String DELI_COLLECTION = "Delivery Collection";
        public static final String DELI_CATEGORY = "Delivery Category";
        public static final String VOUCHER_COLLECTION = "Voucher Collection";
        public static final String VOUCHER_CATEGORY = "Voucher Category";
        public static final String SEARCH = "Search";
        public static final String HOME = "Home";
        public static final String REWARD_HOME = "Reward Home";
        public static final String ORDER_TRACKING = "Order Tracking";
        public static final String MERCHANT = "Merchant";
        public static final String TOPIC = "Delivery Topic";
    }

    public static class StoreCheck {
        public static final String LONG_DISTANCE = "Long Distance";
        public static final String STORE_PAUSE = "Store Paused";
        public static final String STORE_OUT_SERVICE = "Store Out Service";
        public static final String LIXI_PAUSE = "Lixi Paused";
        public static final String LIXI_OUT_SERVICE = "Lixi Out Service";
    }
}
