package com.opencheck.client.utils.load_image.interfaces;

import android.graphics.Bitmap;

public interface OnResultBitmap {
    void onResult(Bitmap bitmap);
}
