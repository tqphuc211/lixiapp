package com.opencheck.client.utils;

public class CrashModel extends Throwable {
    public CrashModel(String message) {
        super(message);
    }
}
