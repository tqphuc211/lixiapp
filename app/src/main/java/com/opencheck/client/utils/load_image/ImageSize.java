package com.opencheck.client.utils.load_image;

/**
 * AUTO: Density <= 2.5 là SMALL, ngược lại MEDIUM
 */
public enum ImageSize {
    DEFAULT, MEDIUM, SMALL, AUTO;

    public String getValue() {
        switch (this) {
            case SMALL:
                return "_small";
            case MEDIUM:
                return "_medium";
        }

        return "";
    }
}
