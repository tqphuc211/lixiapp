package com.opencheck.client.utils;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.databinding.ProgressDialogBinding;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class ProgressDialog extends LixiDialog {


    private ProgressDialogBinding mBinding;

    public ProgressDialog(@NonNull LixiActivity _activity) {
        super(_activity, true, true, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ProgressDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
    }

    @Override
    public void onBackPressed() {
        //DISALLOW BACKPRESS
    }

    @Override
    public void onClick(View v) {

    }
}
