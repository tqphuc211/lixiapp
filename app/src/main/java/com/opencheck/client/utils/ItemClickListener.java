package com.opencheck.client.utils;

/**
 * Created by vutha_000 on 3/6/2018.
 */

public interface ItemClickListener {
    public void onItemCLick(int pos, String action, Object object, Long count);
}
