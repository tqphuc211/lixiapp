package com.opencheck.client.utils.load_image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.opencheck.client.utils.load_image.interfaces.OnResultBitmap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class GetBitmapTask extends AsyncTask<String, String, Bitmap> {
    private OnResultBitmap onResultBitmap;

    public GetBitmapTask(OnResultBitmap onResultBitmap) {
        this.onResultBitmap = onResultBitmap;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        onResultBitmap.onResult(bitmap);
    }
}
