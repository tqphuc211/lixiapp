package com.opencheck.client.utils.tracking;

import android.support.annotation.NonNull;

import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;

public class GlobalTracking {
    public static final String SOURCE = "source";

    static final String EVENT_METHOD = "method";
    static final String CHECK_LOGIN = "is_login";
    static final String PAYMENT_METHOD = "payment_method";
    static final String COUPON_TYPE = "coupon";
    static final String TRANS_ID = "trans_id";
    static final String PROMOTION_CODE = "promotion_code";
    static final String TYPE = "type";
    static final String ACTION = "action";
    static final String SERVICE_TYPE = "service_type";
    static final String DENOMINATION = "denominations";
    static final String SEARCH = "search_string";
    static final String INDEX = "index";

    // region Login
    // nhấn nút login
    public static void trackLoginStartEvent(String method) {
        FacebookTracker.getInstance().trackLoginStartEvent(method);
        FirebaseTracker.getInstance().trackLoginStartEvent(method);
    }

    // xác thực sdt
    public static void trackPhoneVerify(String method) {
        FacebookTracker.getInstance().trackPhoneVerify(method);
        FirebaseTracker.getInstance().trackPhoneVerify(method);
    }

    // đăng ký
    public static void trackSignUpComplete(String method) {
        FacebookTracker.getInstance().trackSignUpComplete(method);
        FirebaseTracker.getInstance().trackSignUpComplete(method);
    }

    // đăng nhập thành công
    public static void trackLoggedInEvent(String method) {
        FacebookTracker.getInstance().trackLoggedInEvent(method);
        FirebaseTracker.getInstance().trackLoggedInEvent(method);
        if (UserModel.getInstance() != null) {
            if ((UserModel.getInstance().getDatecreated() + "").equals(System.currentTimeMillis() + "")) {
                trackSignUpComplete(method);
            }
        }
    }

    // mở trang update info
    public static void trackStartUpdateInfo() {
        FacebookTracker.getInstance().trackStartUpdateInfo();
        FirebaseTracker.getInstance().trackStartUpdateInfo();
    }

    // nhấn nút submit info
    public static void trackSubmitUpdateInfoEvent() {
        FacebookTracker.getInstance().trackSubmitUpdateInfoEvent();
        FirebaseTracker.getInstance().trackSubmitUpdateInfoEvent();
    }

    // cập nhật thành công
    public static void trackUpdateInfoSuccess() {
        FacebookTracker.getInstance().trackUpdateInfoSuccess();
        FirebaseTracker.getInstance().trackUpdateInfoSuccess();
    }

    // đăng xuất
    public static void trackLoggedOutEvent() {
        FacebookTracker.getInstance().trackLoggedOutEvent();
        FirebaseTracker.getInstance().trackLoggedOutEvent();
    }

    // hiện dialog nhắc nhở cập nhật
    public static void trackReferenceCodePopupImpressionsEvent() {
        FacebookTracker.getInstance().trackReferenceCodePopupImpressionsEvent();
        FirebaseTracker.getInstance().trackReferenceCodePopupImpressionsEvent();
    }

    // nhấn cập nhật ngay
    public static void trackReferenceCodePopupAccepts() {
        FacebookTracker.getInstance().trackReferenceCodePopupAccepts();
        FirebaseTracker.getInstance().trackReferenceCodePopupAccepts();
    }

    // mở màn hình mã giới thiệu
    public static void trackScreenReferenceCodeStartsEvent(boolean isLogin) {
        FacebookTracker.getInstance().trackScreenReferenceCodeStartsEvent(isLogin);
        FirebaseTracker.getInstance().trackScreenReferenceCodeStartsEvent(isLogin);
    }

    // nhấn nút chia sẻ trong mã giới thiệu
    public static void trackReferenceCodeShareEvent() {
        FacebookTracker.getInstance().trackReferenceCodeShareEvent();
        FirebaseTracker.getInstance().trackReferenceCodeShareEvent();
    }

    // chọn phương thức chia sẻ
    public static void trackReferenceCodeShareMethodEvent(String method) {
        FacebookTracker.getInstance().trackReferenceCodeShareMethodEvent(method);
        FirebaseTracker.getInstance().trackReferenceCodeShareMethodEvent(method);
    }

    // submit mã giới thiệu
    public static void trackReferenceCodeSubmitEvent() {
        FacebookTracker.getInstance().trackReferenceCodeSubmitEvent();
        FirebaseTracker.getInstance().trackReferenceCodeSubmitEvent();
    }

    // nhập mã giới thiệu thành công
    public static void trackReferenceCodeSuccessEvent() {
        FacebookTracker.getInstance().trackReferenceCodeSuccessEvent();
        FirebaseTracker.getInstance().trackReferenceCodeSuccessEvent();
    }
    //endregion

    // search
    public static void trackEventSearch(String contentType, String search, boolean result) {
        if (search == null || search.equals("")) {
            return;
        }
        String searchString = Helper.removeAccent(search).toLowerCase().trim();
        FacebookTracker.getInstance().trackEventSearch(contentType, searchString, search, result);
        FirebaseTracker.getInstance().trackEventSearch(contentType, searchString, search, result);
    }

    // mở trang chi tiết
    public static void trackShowDetail(String contentType, long id, String currency, double price, String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackShowDetail(contentType, id, currency, price, source);
        FirebaseTracker.getInstance().trackShowDetail(contentType, id, currency, price, source);
    }

    // bắt đầu checkout
    public static void trackStartCheckout(String contentType, long id, String source, double price) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackStartCheckout(contentType, id, source, price);
        FirebaseTracker.getInstance().trackStartCheckout(contentType, id, source, price);
    }

    //
    public static void trackUpdateCheckout(String contentType,
                                           long id,
                                           int numItems,
                                           boolean availableInfo,
                                           String currency,
                                           String paymentMethod,
                                           String coupon,
                                           boolean result,
                                           String trans_id,
                                           double price,
                                           String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackUpdateCheckout(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
        FirebaseTracker.getInstance().trackUpdateCheckout(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
    }

    // kết thúc checkout
    public static void trackCheckedOut(String contentType,
                                       long id,
                                       int numItems,
                                       boolean availableInfo,
                                       String currency,
                                       String paymentMethod,
                                       String coupon,
                                       boolean result,
                                       String trans_id,
                                       double price,
                                       String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackCheckedOut(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
        FirebaseTracker.getInstance().trackCheckedOut(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
    }

    // mở trang mãi khuyến mãi
    public static void trackPromotionCodeStart(long id) {
        FacebookTracker.getInstance().trackPromotionCodeStart(id);
        FirebaseTracker.getInstance().trackPromotionCodeStart(id);
    }

    // tìm kiếm mã khuyến mãi
    public static void trackPromotionCodeSearch(long id, String search, boolean result) {
        FacebookTracker.getInstance().trackPromotionCodeSearch(id, search, result);
        FirebaseTracker.getInstance().trackPromotionCodeSearch(id, search, result);
    }

    //
    public static void trackPromotionCodeUsed(long id, String promotion_code) {
        FacebookTracker.getInstance().trackPromotionCodeUsed(id, promotion_code);
        FirebaseTracker.getInstance().trackPromotionCodeUsed(id, promotion_code);
    }

    // chọn mãi khuyến mãi
    public static void trackStoreImpression(long id, String type) {
        FacebookTracker.getInstance().trackStoreImpression(id, type);
        FirebaseTracker.getInstance().trackStoreImpression(id, type);
    }

    // pause service
    public static void trackPopupAccept(long id, String type, String action) {
        FacebookTracker.getInstance().trackPopupAccept(id, type, action);
        FirebaseTracker.getInstance().trackPopupAccept(id, type, action);
    }

    // region service
    // chọn nạp tiền hoặc thẻ cào
    public static void trackServiceStart(String type) {
        FacebookTracker.getInstance().trackServiceStart(type);
        FirebaseTracker.getInstance().trackServiceStart(type);
    }

    // bắt đầu checkout
    public static void trackServiceBegin(String type, double denominations, String currency) {
        FacebookTracker.getInstance().trackServiceBegin(type, denominations, currency);
        FirebaseTracker.getInstance().trackServiceBegin(type, denominations, currency);
    }

    // check out
    public static void trackServiceCheckedOut(String type, double denominations, String currency, double price) {
        FacebookTracker.getInstance().trackServiceCheckedOut(type, denominations, currency, price);
        FirebaseTracker.getInstance().trackServiceCheckedOut(type, denominations, currency, price);
    }

    public static void trackServicePurchased(String type, double denominations, String currency, double price) {
        FacebookTracker.getInstance().trackServicePurchased(type, denominations, currency, price);
        FirebaseTracker.getInstance().trackServicePurchased(type, denominations, currency, price);
    }
    // endregion

    // region online payment
    // 10
    public static void trackPurchaseComplete(String contentType,
                                             long id,
                                             int numItems,
                                             boolean availableInfo,
                                             String currency,
                                             String paymentMethod,
                                             String coupon,
                                             boolean result,
                                             String trans_id,
                                             double price,
                                             String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackPurchaseComplete(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
        FirebaseTracker.getInstance().trackPurchaseComplete(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
    }

    // 11
    public static void trackOrderComplete(String contentType,
                                          long id,
                                          int numItems,
                                          boolean availableInfo,
                                          String currency,
                                          String paymentMethod,
                                          String coupon,
                                          boolean result,
                                          String trans_id,
                                          double price,
                                          String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackOrderComplete(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
        FirebaseTracker.getInstance().trackOrderComplete(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, result, trans_id, price, source);
    }

    // 12
    public static void trackCancelPurchase(String contentType,
                                           long id,
                                           int numItems,
                                           boolean availableInfo,
                                           String currency,
                                           String paymentMethod,
                                           String coupon,
                                           String trans_id,
                                           double price,
                                           String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackCancelPurchase(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
        FirebaseTracker.getInstance().trackCancelPurchase(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
    }

    // 13
    public static void trackRepayOrder(String contentType,
                                       long id,
                                       int numItems,
                                       boolean availableInfo,
                                       String currency,
                                       String paymentMethod,
                                       String coupon,
                                       String trans_id,
                                       double price,
                                       String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackRepayOrder(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
        FirebaseTracker.getInstance().trackRepayOrder(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
    }

    // 14
    public static void trackRepaySuccess(String contentType,
                                         long id,
                                         int numItems,
                                         boolean availableInfo,
                                         String currency,
                                         String paymentMethod,
                                         String coupon,
                                         String trans_id,
                                         double price,
                                         String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackRepaySuccess(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
        FirebaseTracker.getInstance().trackRepaySuccess(contentType, id, numItems, availableInfo, currency, paymentMethod, coupon, trans_id, price, source);
    }
    // endregion

    // region Game
    public static void trackGameStart(String source) {
        if (source == null) {
            source = "";
        }
        FacebookTracker.getInstance().trackGameStart(source);
        FirebaseTracker.getInstance().trackGameStart(source);
    }

    public static void trackGameShare(@NonNull String contentType) {
        FacebookTracker.getInstance().trackGameShare(contentType);
        FirebaseTracker.getInstance().trackGameShare(contentType);
    }

    public static void trackGameShareSuccess(@NonNull String contentType) {
        FacebookTracker.getInstance().trackGameShareSuccess(contentType);
        FirebaseTracker.getInstance().trackGameShareSuccess(contentType);
    }

    public static void trackGameOpenTabbar(@NonNull String contentType, int index) {
        FacebookTracker.getInstance().trackGameOpenTabbar(contentType, index);
        FirebaseTracker.getInstance().trackGameOpenTabbar(contentType, index);
    }
}
