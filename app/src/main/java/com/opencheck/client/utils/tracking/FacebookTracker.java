package com.opencheck.client.utils.tracking;

import android.content.Context;
import android.os.Bundle;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

import java.math.BigDecimal;
import java.util.Currency;

public class FacebookTracker {
    private volatile static FacebookTracker instance = null;
    private AppEventsLogger logger;

    private FacebookTracker(Context context) {
        logger = AppEventsLogger.newLogger(context);
    }

    public static synchronized void initialize(Context context) {
        if (instance != null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }

        instance = new FacebookTracker(context);
    }

    public static FacebookTracker getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }
        return instance;
    }

    void trackLoginStartEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        logger.logEvent("Login Start", data);
    }

    void trackLoggedInEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        logger.logEvent("Login", data);
    }

    void trackPhoneVerify(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        logger.logEvent("Login Phone Verified", data);
    }

    void trackSignUpComplete(String method) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, method);
        logger.logEvent(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, data);
    }

    void trackStartUpdateInfo() {
        logger.logEvent("Update Information Start");
    }

    void trackSubmitUpdateInfoEvent() {
        logger.logEvent("Update Information Submit");
    }

    void trackUpdateInfoSuccess() {
        logger.logEvent("Update Information Success");
    }

    void trackLoggedOutEvent() {
        logger.logEvent("Logged Out");
    }

    void trackReferenceCodePopupImpressionsEvent() {
        logger.logEvent("Reference Code Popup Impressions");
    }

    void trackReferenceCodePopupAccepts() {
        logger.logEvent("Reference Code Popup Accepts");
    }

    void trackScreenReferenceCodeStartsEvent(boolean isLogin) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.CHECK_LOGIN, isLogin ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        logger.logEvent("Reference Code Screen Starts", data);
    }

    void trackReferenceCodeShareEvent() {
        logger.logEvent("Reference Code Share Starts", null);
    }

    void trackReferenceCodeShareMethodEvent(String method) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.EVENT_METHOD, method);
        logger.logEvent("Reference Code Share Method", data);
    }

    void trackReferenceCodeSubmitEvent() {
        logger.logEvent("Reference Code Submit", null);
    }

    void trackReferenceCodeSuccessEvent() {
        logger.logEvent("Reference Code Success", null);
    }

    void trackEventSearch(String contentType, String search, String keyword, boolean result) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, search);
        data.putString(GlobalTracking.SEARCH, keyword);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, data);
    }

    void trackShowDetail(String contentType, long id, String currency, double price, String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, data);
    }

    void trackStartCheckout(String contentType, long id, String source, double price) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, price, data);
    }

    void trackUpdateCheckout(String contentType,
                             long id,
                             int numItems,
                             boolean availableInfo,
                             String currency,
                             String paymentMethod,
                             String coupon,
                             boolean result,
                             String trans_id,
                             double price,
                             String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(GlobalTracking.TRANS_ID, trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Update Checkout Order", price, data);
    }

    void trackCheckedOut(String contentType,
                         long id,
                         int numItems,
                         boolean availableInfo,
                         String currency,
                         String paymentMethod,
                         String coupon,
                         boolean result,
                         String trans_id,
                         double price,
                         String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Checked Out", price, data);
    }

    void trackPromotionCodeStart(long id) {
        Bundle data = new Bundle();
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        logger.logEvent("Promotion Code Starts", data);
    }

    void trackPromotionCodeSearch(long id, String search, boolean result) {
        Bundle data = new Bundle();
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, search);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        logger.logEvent("Promotion Code Searched", data);
    }

    void trackPromotionCodeUsed(long id, String promotion_code) {
        Bundle data = new Bundle();
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString(GlobalTracking.PROMOTION_CODE, promotion_code);
        logger.logEvent("Promotion Code Used", data);
    }

    void trackStoreImpression(long id, String type) {
        Bundle data = new Bundle();
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString("type", type);
        logger.logEvent("Store Popup Impression", data);
    }

    void trackPopupAccept(long id, String type, String action) {
        Bundle data = new Bundle();
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putString(GlobalTracking.TYPE, type);
        data.putString(GlobalTracking.ACTION, action);
        logger.logEvent("Store Popup Accepts", data);
    }

    void trackServiceStart(String type) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        logger.logEvent("Service Starts", data);
    }

    void trackServiceBegin(String type, double denominations, String currency) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent("Service Begin Checkout", data);
    }

    void trackServiceCheckedOut(String type, double denominations, String currency, double price) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent("Service Checked Out", price, data);
    }

    void trackServicePurchased(String type, double denominations, String currency, double price) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SERVICE_TYPE, type);
        data.putDouble(GlobalTracking.DENOMINATION, denominations);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent("Service Purchased", price, data);
    }

    void trackPurchaseComplete(String contentType,
                               long id,
                               int numItems,
                               boolean availableInfo,
                               String currency,
                               String paymentMethod,
                               String coupon,
                               boolean result,
                               String trans_id,
                               double price,
                               String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
//        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance(currency), data);
    }

    void trackOrderComplete(String contentType,
                            long id,
                            int numItems,
                            boolean availableInfo,
                            String currency,
                            String paymentMethod,
                            String coupon,
                            boolean result,
                            String trans_id,
                            double price,
                            String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, result ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Order Completed", price, data);
    }

    void trackCancelPurchase(String contentType,
                             long id,
                             int numItems,
                             boolean availableInfo,
                             String currency,
                             String paymentMethod,
                             String coupon,
                             String trans_id,
                             double price,
                             String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Purchase Canceled", price, data);
    }

    void trackRepayOrder(String contentType,
                         long id,
                         int numItems,
                         boolean availableInfo,
                         String currency,
                         String paymentMethod,
                         String coupon,
                         String trans_id,
                         double price,
                         String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Purchase Repay Start", price, data);
    }

    void trackRepaySuccess(String contentType,
                           long id,
                           int numItems,
                           boolean availableInfo,
                           String currency,
                           String paymentMethod,
                           String coupon,
                           String trans_id,
                           double price,
                           String source) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        data.putLong(AppEventsConstants.EVENT_PARAM_CONTENT_ID, id);
        data.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        data.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, availableInfo ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        data.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        data.putString(GlobalTracking.PAYMENT_METHOD, paymentMethod);
        data.putString(GlobalTracking.COUPON_TYPE, coupon == null ? "" : coupon);
        data.putString(GlobalTracking.TRANS_ID, trans_id == null ? "" : trans_id);
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Purchase Repay Success", price, data);
    }

    void trackGameStart(String source) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SOURCE, source);
        logger.logEvent("Event Start", data);
    }

    void trackGameShare(String contentType) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        logger.logEvent("Share", data);
    }

    void trackGameShareSuccess(String contentType) {
        Bundle data = new Bundle();
        data.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        logger.logEvent("Share Success", data);
    }

    void trackGameOpenTabbar(String source, int index) {
        Bundle data = new Bundle();
        data.putString(GlobalTracking.SOURCE, source);
        data.putInt(GlobalTracking.INDEX, index);
        logger.logEvent("Open Tabbar", data);
    }
}
