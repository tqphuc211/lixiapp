package com.opencheck.client.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.dialogs.AutoDismissNotifyDialog;
import com.opencheck.client.home.delivery.libs.gps.FallbackLocationTracker;
import com.opencheck.client.home.delivery.libs.gps.ProviderLocationTracker;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.merchant.map.GPSTracker;
import com.opencheck.client.home.merchant.map.LoadLocationAddress;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.NotificationModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class Helper {

    public static String getDeviceBrand() {
        return Build.BRAND;
    }

    public static String getDeviceID(Activity activity) {
        return Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public static String getMacAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) buf.append(String.format("%02X:", aMac));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static float getBatteryLevel(Activity activity) {
        Intent batteryIntent = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

    public static String correctPhoneNumber(String phone) {

        String rs = phone.replaceAll(" ", "");
        rs = rs.replace("+84", "0");
        rs = rs.replace("(84)", "0");

        char[] arr = rs.toCharArray();
        char f = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < '0' || arr[i] > '9')
                if (arr[i] != '+')
                    arr[i] = ' ';
        }

        rs = new String(arr);

        rs = rs.replaceAll(" ", "");

        return rs;
    }

    public static String correctSerialNumber(String phone) {

        String rs = phone.replaceAll(" ", "");

        return rs;
    }

    public static boolean checkGpsStatus(LixiActivity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean checkGPSStatus(LixiActivity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static int getDaysBetweenTimestamp(String timeCreate, String timeExpire, String formateDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formateDate, Locale.getDefault());

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(timeCreate);
            Expire_CovertedDate = dateFormat.parse(timeExpire);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int c_year = 0, c_month = 0, c_day = 0;

        if (Created_convertedDate.after(todayWithZeroTime)) {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(todayWithZeroTime);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar today_cal = Calendar.getInstance();
    int today_year = today_cal.get(Calendar.YEAR);
    int today = today_cal.get(Calendar.MONTH);
    int today_day = today_cal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(Expire_CovertedDate);

        int e_year = e_cal.get(Calendar.YEAR);
        int e_month = e_cal.get(Calendar.MONTH);
        int e_day = e_cal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(c_year, c_month, c_day);
        date2.clear();
        date2.set(e_year, e_month, e_day);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

//        return ("" + (int) dayCount + " Days");
        return (int) dayCount;
    }

    public static int daysBetween(Calendar startDate, Calendar endDate) {
        Calendar date = (Calendar) startDate.clone();
        int daysBetween = -1;
        while (date.before(endDate)) {
            daysBetween = 0;
            if (startDate.get(Calendar.DAY_OF_MONTH) == endDate.get(Calendar.DAY_OF_MONTH))
                break;
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static long dayDiff(String start, String end) {
        long diff = -1;
        try {
            // Converting date to Java8 Local date
            LocalDate startDate = LocalDate.parse(start);
            LocalDate endtDate = LocalDate.parse(end);

            // Range = End date - Start date
            diff = ChronoUnit.DAYS.between(startDate, endtDate);
        } catch (Exception e) {
            //handle the exception according to your own situation
        }

        return diff;

    }

    public static void vibratorClick(Activity activity, int time) {
        Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(time);
    }

    public static Boolean getBoolFromString(String sBool) {
        if (sBool.equalsIgnoreCase("false") || sBool.equalsIgnoreCase("FALSE") || sBool.equalsIgnoreCase("0") || sBool.equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\.\\+]+(\\.[_A-Za-z0-9-\\.\\+]+)*@[A-Za-z0-9-\\.\\+]+(\\.[A-Za-z0-9-\\.\\+]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //region GOOGLE MAP
    public static GPSTracker gps;
    private static double latitude;
    private static double longitude;

    public static void getGPSLocation(Context _con, LocationCallBack locationCallback) {
        gps = new GPSTracker(_con);
        LatLng mCurrentPoint;
        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            latitude = 0;
            longitude = 0;
            //gps.showSettingsAlert();
        }
        mCurrentPoint = new LatLng(latitude, longitude);
        LoadLocationAddress newTask = new LoadLocationAddress(locationCallback);
        newTask.execute(mCurrentPoint);
    }

    public static FallbackLocationTracker gpsTracker;

    public static LatLng getCurrentLocation(Context context) {
        gpsTracker = new FallbackLocationTracker(context, ProviderLocationTracker.ProviderType.GPS);
        if (gpsTracker.hasPossiblyStaleLocation()) {
            latitude = gpsTracker.getPossiblyStaleLocation().getLatitude();
            longitude = gpsTracker.getPossiblyStaleLocation().getLongitude();
        } else {
            latitude = 0;
            longitude = 0;
        }
        return new LatLng(latitude, longitude);

//        LoadLocationAddress newTask = new LoadLocationAddress(locationCallBack);
//        newTask.execute(mCurrentLocation);
    }

    public static LatLng getGPSAddress(Context context) {
        gps = new GPSTracker(context);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            latitude = 0;
            longitude = 0;
            gps.showSettingsAlert();
        }
        return new LatLng(latitude, longitude);

//        LoadLocationAddress newTask = new LoadLocationAddress(locationCallBack);
//        newTask.execute(mCurrentLocation);
    }
    //endregion

    public static String getVNCurrencyShort(long money) {
        if (money >= 1000000) {
            long r = money / 10000;
            NumberFormat formatter = new DecimalFormat("00");

            String tp = formatter.format(r % 100);

            if (tp.equals("00"))
                tp = "";
            else if (tp.charAt(1) == '0')
                tp = tp.charAt(0) + "";

            return (r / 100) + (tp.length() > 0 ? ("," + tp) : "") + "triệu";
        }
        if (money >= 100000)
            return (money / 1000) + "k";
        return getVNCurrency(money) + "đ";
    }

    public static String getPhoneSupport(LixiActivity activity) {
        try {
            String phone = ConfigModel.getInstance(activity).getDefaultSupportPhone();
            return phone;
        } catch (Exception e) {
            return activity.getResources().getString(R.string.DEFAULT_NUMBER);
        }
    }

    public static String getPhoneDeliSupport(LixiActivity activity) {
        try {
            String phone = ConfigModel.getInstance(activity).getDefaultSupportPhone();
            return phone;
        } catch (Exception e) {
            return activity.getResources().getString(R.string.DEFAULT_NUMBER);
        }
    }

    private static String number = "";

    public static void callPhoneSupport(LixiActivity activity) {
//        number = activity.getResources().getString(R.string.DEFAULT_NUMBER);
        number = getPhoneSupport(activity);
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            }
//        } else {
        call(activity);
//        }
    }

    public static void callPhoneDeliSupport(LixiActivity activity) {
//        number = activity.getResources().getString(R.string.DEFAULT_NUMBER);
        number = getPhoneDeliSupport(activity);
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            }
//        } else {
        call(activity);
//        }
    }

    public static void openDial(LixiActivity activity, String _number) {
        number = correctPhoneNumber(_number);
        if (number.equals(""))
            number = getPhoneSupport(activity);
        try {
            String _phone = getNumberFromString(number);
            Intent dialIntent = new Intent(Intent.ACTION_DIAL);
            dialIntent.setData(Uri.parse("tel:" + _phone));
            activity.startActivity(dialIntent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static void callPhone(LixiActivity activity, String _number) {
        number = correctPhoneNumber(_number);
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            }
//        } else {
        call(activity);
//        }
    }

    public static void useCardSerial(LixiActivity activity, String _number) {
        number = _number.replace(" ", "");
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, ConstantValue.REQUEST_CODE_CALL);
//            }
//        } else {
        callCard(activity);
//        }
    }


    public static void callCard(LixiActivity activity) {
        if (number.equals(""))
            number = getPhoneSupport(activity);
//            number = activity.getResources().getString(R.string.DEFAULT_NUMBER);
        try {
            String encodedHash = Uri.encode("#");
//            Intent callIntent = new Intent(Intent.ACTION_CALL);
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number + encodedHash));
            activity.startActivity(callIntent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }

    }

    //Dont use this, this is only used when you are sure that you have the permission
    public static void call(LixiActivity activity) {
        if (number.equals(""))
            number = getPhoneSupport(activity);
//            number = activity.getResources().getString(R.string.DEFAULT_NUMBER);
        try {
            String _number = getNumberFromString(number);
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + _number));
            activity.startActivity(callIntent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }

    }

    public static String getNumberFromString(String text) {
        String numberOnly = text.replaceAll("[^0-9]", "");
        if (numberOnly.equalsIgnoreCase("")) {
            return "0";
        }
        return numberOnly;
    }

    public static boolean getDifferentDay(Long time1, Long time2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(time1);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(time2);

        boolean same = (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) && (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) && (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH));
        if (same) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean getDifferentDay(Date c1, Date c2) {

        boolean same = (c1.getYear() == c2.getYear()) && (c1.getMonth() == c2.getMonth()) && (c1.getDay() == c2.getDay());
        if (same) {
            return true;
        } else {
            return false;
        }
    }

    public static String getFormatDateEngISO(String format, String timeStamp) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(Long.parseLong(timeStamp));
            Date d = c.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            return sdf.format(d);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return "";
    }

    public static String getFormatDateVN(String format, long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);
    }

    public static String getTimestamp() {
        Calendar calendar1 = Calendar.getInstance();
        java.util.Date now1 = calendar1.getTime();
        java.sql.Timestamp currentTimestamp1 = new java.sql.Timestamp(now1.getTime());
        return String.valueOf((currentTimestamp1.getTime() / 1000L));
    }

    public static Long getTimeStampFromString(String date, String format) {
        Long dTime = 0L;
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            Date convertedDate = dateFormat.parse(date);
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(convertedDate.getTime());
            dTime = currentTimestamp.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dTime;
    }

    public static void clearCart(Context ct) {
        HashMap<String, CartModel> mCart = getCartFromFile(ct, ConstantValue.CART_FILE);
        HashMap<String, ArrayList<String>> mMapping = getProductKeyListFromFile(ct, ConstantValue.PRODUCT_FILE);
        mCart.clear();
        mMapping.clear();
        saveProductToFile(mCart, ct, ConstantValue.CART_FILE);
        saveProductToFile(mMapping, ct, ConstantValue.PRODUCT_FILE);
    }

    public static void removeExpiredCart(Context ct) {
        HashMap<String, CartModel> mCart = getCartFromFile(ct, ConstantValue.CART_FILE);

        Set<String> keySet = mCart.keySet();
        Iterator<String> iterator = keySet.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Long createDate = mCart.get(key).getCreate_day();
            Calendar createdTime = Calendar.getInstance();
            Date mDate = new Date(createDate);
            createdTime.setTime(mDate);
            createdTime.add(Calendar.HOUR, 1);

            Calendar currentTime = Calendar.getInstance();
            currentTime.setTime(new Date());

            if (currentTime.after(createdTime)) {
                iterator.remove();
                mCart.remove(key);
            }
        }

        saveProductToFile(mCart, ct, ConstantValue.CART_FILE);
    }

    public static void saveObJectToFile(Object objectToWrite, Context ct, String _fileName) {
        String fileName = _fileName;
        try {
            FileOutputStream fos = ct.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os;
            os = new ObjectOutputStream(fos);
            os.writeObject(objectToWrite);
            os.close();
            fos.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static void saveProductToFile(Object objectToWrite, Context ct, String _fileName) {
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileOutputStream fos = new FileOutputStream(file_path);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(objectToWrite);
            fos.getFD().sync();
            os.close();
            fos.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, CartModel> getCartFromFile(Context ct, String _fileName) {
        HashMap cart = new HashMap<>();
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            cart = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }

        return cart;
    }

    public static HashMap<String, Integer> getRepayCount(Context ct, String _fileName) {
        HashMap map = new HashMap<>();
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }

        return map;
    }

    public static void saveObjectToFile(Object objectToWrite, Context ct, String _fileName) {
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileOutputStream fos = new FileOutputStream(file_path);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(objectToWrite);
            fos.getFD().sync();
            os.close();
            fos.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, String> getObjectFromFile(Context ct, String _fileName) {
        HashMap map = new HashMap<>();
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        return map;
    }

    public static String covertStringToURL(String str) {
        try {
            String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static HashMap<String, ArrayList<String>> getProductKeyListFromFile(Context ct, String _fileName) {
        HashMap mapping = new HashMap<>();
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            mapping = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        return mapping;
    }

    public static void showLog(String message) {
//         Log.d(ConstantValue.LOG_INFO_TAG, message);
    }

    public static NotificationModel readNotidfyFromFile(Activity activity, String _fileName) {
        NotificationModel simpleClass = null;
        String fileName = _fileName;
        try {
            FileInputStream fis = activity.openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            simpleClass = (NotificationModel) is.readObject();
            is.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return simpleClass;
    }

    public static void saveStoreToFile(Object object, Context ct, String _fileName) {
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            FileOutputStream fos = new FileOutputStream(file_path);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            fos.getFD().sync();
            os.close();
            fos.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, ArrayList<StoreOfCategoryModel>> getStoreFromFile(Context ct, String _fileName) {
        HashMap mapStore = new HashMap<>();
        try {
            String file_path = ct.getApplicationContext().getFilesDir().getAbsolutePath() + "/" + _fileName;
            File file = new File(file_path);
            if(!file.exists()){
                file.createNewFile();
            }
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            mapStore = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }

        return mapStore;
    }

    public static void showErrorDialog(LixiActivity activity, String message) {
        AutoDismissNotifyDialog dialog = new AutoDismissNotifyDialog(activity, false, true, false);
        dialog.show();
        dialog.setMessage(message);
    }

    public static void showErrorDialog(LixiActivity activity, String message, DialogInterface.OnCancelListener onCancelListener) {
        AutoDismissNotifyDialog dialog = new AutoDismissNotifyDialog(activity, false, true, false);
        dialog.show();
        dialog.setMessage(message);
        dialog.setOnCancelListener(onCancelListener);
    }

    public static void showSuccessDialog(LixiActivity activity, String message) {
        AutoDismissNotifyDialog dialog = new AutoDismissNotifyDialog(activity, false, true, false);
        dialog.show();
        dialog.setMessage(message);
    }

    public static String getVNCurrency(long money) {
        DecimalFormatSymbols format = new DecimalFormatSymbols();
        format.setDecimalSeparator(',');
        format.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("#,###,###,###", format);
        return df.format(money);
    }

    public static Boolean isValidPhone(String phone) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[0|+]{1}[0-9]{9,11}$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public static String getFormatDateISO(String format, String timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(Long.parseLong(timeStamp));
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);
    }

    public static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public static Uri getPhotoFileUri(LixiActivity activity, String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
//                    Log.d("tinhvc", "failed to create directory");
                }
            }

            // Return the file target for the photo based on filename
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return FileProvider.getUriForFile(activity,
                        BuildConfig.APPLICATION_ID + ".provider",
                        new File(mediaStorageDir.getPath() + File.separator + fileName));
            } else {
                return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
            }
        }
        return null;
    }

    public static Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
//                    Log.d("tinhvc", "failed to create directory");
                }
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    public static void showErrorToast(LixiActivity activity, String object) {
        Toast.makeText(activity, object, Toast.LENGTH_SHORT).show();
    }

    public static String getKCurrency(long money) {
        long k = money / 1000;
        return getVNCurrency(k) + "K";
    }

    public static void showSoftKeyBoard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception e) {

        }
    }

    public static void setScrollProrgess(int y, View background, TextView txtTitle, View... childs) {
        int color;
        if (y < 100) {
            txtTitle.setVisibility(View.GONE);
            background.setVisibility(View.GONE);
            color = Color.parseColor("#ffffff");
        } else {
            if (y < 130) {
                txtTitle.setTextColor(0x4B000000);
                background.setBackgroundColor(0x4BFFFFFF);
                color = Color.parseColor("#ffffff");
            } else {
                if (y < 160) {
                    txtTitle.setTextColor(0x6B000000);
                    background.setBackgroundColor(0x6BFFFFFF);
                }
                if (y >= 160 && y < 190) {
                    txtTitle.setTextColor(0x8B000000);
                    background.setBackgroundColor(0x8BFFFFFF);
                }
                if (y >= 191 && y < 210) {
                    txtTitle.setTextColor(0xAB000000);
                    background.setBackgroundColor(0xABFFFFFF);
                }
                if (y >= 211 && y < 230) {
                    txtTitle.setTextColor(0xDB000000);
                    background.setBackgroundColor(0xDDFFFFFF);
                }
                if (y > 245) {
                    txtTitle.setTextColor(0xFF000000);
                    background.setBackgroundColor(0xFFFFFFFF);
                }
                color = Color.parseColor("#AC1EA1");
            }
            txtTitle.setVisibility(View.VISIBLE);
            background.setVisibility(View.VISIBLE);
        }

        for (View view : childs) {
            if (view instanceof ImageView) {
                ((ImageView) view).setColorFilter(color);
            } else {
                if (view instanceof TextView) {
                    ((TextView) view).setTextColor(color);
                }
            }
        }
    }

    public static void shareLink(Activity activity, String content, boolean hasTracking) {
        if (content == null || content.equals("")) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.cash_evoucher_cannot_share, activity), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, content);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Intent receiver = new Intent(activity, ShareReceiver.class);
            receiver.putExtra(ConstantValue.SHARE_TRACKING, hasTracking);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(activity,
                    ConstantValue.REQUEST_CODE_SHARE_LINK,
                    receiver,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            activity.startActivity(Intent.createChooser(share, LanguageBinding.getString(R.string.deli_support_share_title, activity), pendingIntent.getIntentSender()));
        } else {
            activity.startActivity(Intent.createChooser(share, LanguageBinding.getString(R.string.deli_support_share_title, activity)));
        }
    }

    public static String formatPhone(String phone) {
        int length = phone.length();
        if (length < 6 || length > 11) {
            return phone;
        }
        String p3 = phone.substring(length - 3);
        String p2 = phone.substring(length - 6, length - 3);
        String p1 = phone.substring(0, length - 6);
        return p1 + " " + p2 + " " + p3;
    }

    public static String removeAccent(String s) {
        if (s == null) {
            return "";
        }
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static void setAllCapFilter(EditText edt) {
        InputFilter[] editFilters = edt.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
        newFilters[editFilters.length] = new InputFilter.AllCaps();
        edt.setFilters(newFilters);
    }

    public static DeliConfigModel getConfigByKey(LixiActivity activity, String key) {
        for (DeliConfigModel model : new AppPreferenceHelper(activity).getDeliConfig()) {
            if (model.getKey().equals(key)) {
                return model;
            }
        }
        return null;
    }

    public static void copyCode(String value, Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied", value);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            Toast.makeText(context, LanguageBinding.getString(R.string.copy_code, context), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, LanguageBinding.getString(R.string.try_again, context), Toast.LENGTH_SHORT).show();
        }
    }
}
