package com.opencheck.client.utils;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.models.socket.SocketDataModel;
import com.opencheck.client.models.socket.SocketDeviceModel;
import com.opencheck.client.models.socket.SocketMessageModel;
import com.opencheck.client.models.socket.SocketMetaModel;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.models.user.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;

public class LixiTrackingHelper {

    private LixiActivity activity;
    private Socket mSocket;
    private int count = 0;

    public LixiTrackingHelper(LixiActivity activity, Socket mSocket) {
        this.activity = activity;
        this.mSocket = mSocket;
    }

    private SocketMetaModel configMeta() {
        SocketMetaModel meta = new SocketMetaModel();
        meta.setPlatform("android");
        if (UserModel.getInstance() != null) {
            meta.setUser_id(UserModel.getInstance().getId());
            Log.d("id", UserModel.getInstance().getId());
        }

        Long tsLong = System.currentTimeMillis();
        meta.setTimestamp(tsLong);

        SocketDeviceModel deviceModel = new SocketDeviceModel();
        deviceModel.setDevice_id(Helper.getDeviceID(activity));
        deviceModel.setOs_version(Build.VERSION.CODENAME);
        deviceModel.setScreen_resolution(activity.heightScreen + "," + activity.widthScreen);
        deviceModel.setDevice_model(Helper.getDeviceModel());
        deviceModel.setNetwork_connection("wifi");
        deviceModel.setMac_address(Helper.getMacAddress("wlan0"));
        deviceModel.setIp_address(Helper.getIPAddress(true));
        deviceModel.setBattery_percent(Helper.getBatteryLevel(activity));
        deviceModel.setDevice_token(new AppPreferenceHelper(LixiApplication.getInstance()).getPushToken());
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            String version = pInfo.versionName;
            deviceModel.setApp_version(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        meta.setDevice(deviceModel);

        SocketUtmModel socketUtmModel = ConstantValue.SOCKET_UTM_MODEL;

        if (socketUtmModel != null) {
            Log.d("TTTT", "ok");
            meta.setUtm(socketUtmModel);
        }

        return meta;
    }

    private void retryEmit(String ms) {
        mSocket.emit("tracking-lzstring", ms, new Ack() {
            @Override
            public void call(Object... args) {
                Helper.showLog("Tracking sent again" + args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        mSocket.emit("");
                        count++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void createLoginTrackingSocket(boolean isFirstLogin) {
        SocketMessageModel message = new SocketMessageModel();
        SocketDataModel data = new SocketDataModel();
        data.setName("login");
        data.setFirst_login(isFirstLogin);
        SocketMetaModel meta = configMeta();

        message.setData(data);
        message.setMeta(meta);

        final String ms = LZString.compressToUTF16(new Gson().toJson(message));

        mSocket.emit("tracking-lzstring", ms, new Ack() {
            @Override
            public void call(Object... args) {
                Helper.showLog("Tracking-login sent" + args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status && count < 6)
                        retryEmit(ms);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void createNavigateTrackingSocket(final String mScreenName, Long mId) {
        SocketMessageModel message = new SocketMessageModel();
        SocketDataModel data = new SocketDataModel();
        data.setName("navigate");
        data.setScreen(mScreenName);
        if(mId != null)
            data.setId(mId);
        SocketMetaModel meta = configMeta();

        message.setData(data);
        message.setMeta(meta);

        final String ms = LZString.compressToUTF16(new Gson().toJson(message));

        mSocket.emit("tracking-lzstring", ms, new Ack() {
            @Override
            public void call(Object... args) {
                Helper.showLog("Tracking sent [" + mScreenName + "]" + args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status && count < 6)
                        retryEmit(ms);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void createOrderDeliTrackingSocket(String uuid) {
        SocketMessageModel message = new SocketMessageModel();
        SocketDataModel data = new SocketDataModel();
        data.setName("order_deli");
        data.setOrder_uuid(uuid);
        SocketMetaModel meta = configMeta();

        message.setData(data);
        message.setMeta(meta);

        final String ms = LZString.compressToUTF16(new Gson().toJson(message));

        mSocket.emit("tracking-lzstring", ms, new Ack() {
            @Override
            public void call(Object... args) {
                Helper.showLog("Tracking sent" + args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status && count < 6)
                        retryEmit(ms);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void createOrderLixiTrackingSocket(Long mProductId, String mTransactionId) {
        SocketMessageModel message = new SocketMessageModel();
        SocketDataModel data = new SocketDataModel();
        data.setName("order");
        data.setProduct_id(mProductId);
        data.setTransaction_id(mTransactionId);
        SocketMetaModel meta = configMeta();

        message.setData(data);
        message.setMeta(meta);

        final String ms = LZString.compressToUTF16(new Gson().toJson(message));

        mSocket.emit("tracking-lzstring", ms, new Ack() {
            @Override
            public void call(Object... args) {
                Helper.showLog("Tracking sent [order]" + args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status && count < 6)
                        retryEmit(ms);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
