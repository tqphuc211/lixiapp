package com.opencheck.client.utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Support set language
 */
public class LanguageBinding {

    private static final String TAG = "LanguageBinding";

    /**
     * Get string from resource or from language from server
     *
     * @param res     resource id
     * @param context context needed
     * @return String language
     */
    public static String getString(@StringRes int res, @Nullable Context context) {
        if (context != null) {
            String name = context.getResources().getResourceEntryName(res);
            if (new AppPreferenceHelper(context).getLanguage().containsKey(name)) {
                return new AppPreferenceHelper(context).getLanguage().get(name);
            } else {
                return context.getString(res);
            }
        } else return "";
    }

    @BindingAdapter("android:text")
    public static void setText(TextView textView, String key) {
        try {
            if (new AppPreferenceHelper(textView.getContext()).getLanguage().containsKey(key)) {
                textView.setText(new AppPreferenceHelper(textView.getContext())
                        .getLanguage().get(key));
            } else {
                int id = textView.getContext().getResources().getIdentifier(key, "string",
                        textView.getContext().getPackageName());
                String name = textView.getContext().getResources().getString(id);
                textView.setText(name);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
            textView.setText(key);
        }
    }

    @BindingAdapter("android:hint")
    public static void setTextHint(TextView textView, String key) {
        try {
            if (new AppPreferenceHelper(textView.getContext()).getLanguage().containsKey(key)) {
                textView.setText(new AppPreferenceHelper(textView.getContext())
                        .getLanguage().get(key));
            } else {
                int id = textView.getContext().getResources().getIdentifier(key, "string",
                        textView.getContext().getPackageName());
                String name = textView.getContext().getResources().getString(id);
                textView.setHint(name);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
            textView.setHint(key);
        }
    }

    @BindingAdapter("android:hint")
    public static void setEditTextHint(EditText editText, String key) {
        try {
            if (new AppPreferenceHelper(editText.getContext()).getLanguage().containsKey(key)) {
                editText.setText(new AppPreferenceHelper(editText.getContext())
                        .getLanguage().get(key));
            } else {
                int id = editText.getContext().getResources().getIdentifier(key, "string",
                        editText.getContext().getPackageName());
                String name = editText.getContext().getResources().getString(id);
                editText.setHint(name);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
            editText.setHint(key);
        }
    }
}
