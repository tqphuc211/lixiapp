package com.opencheck.client;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.deeplink.ApplicationLifecycle;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.new_layout.activity.IntroCodeActivity;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.activities.TopicActivity;
import com.opencheck.client.home.game.GameActivity;
import com.opencheck.client.home.history.detail.HistoryReceiveLixiDialog;
import com.opencheck.client.home.history.detail.OrderDetailDialog;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.home.lixishop.header.rewardcode.LixiCodeDetailDialog;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.parent.LixiParentActivity;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ProgressDialog;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.function.ToDoubleBiFunction;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public abstract class LixiActivity extends LixiParentActivity implements View.OnClickListener {

    protected LixiActivity activity;
    public int widthScreen;
    public int heightScreen;
    public Long id = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        getScreenSize();
        receiveNotificationData();
    }

    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        widthScreen = display.getWidth();
        heightScreen = display.getHeight();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    //private HomeWatcher mHomeWatcher;

    @Override
    protected void onResume() {
        super.onResume();
        setUpPushNotificationReceiver();
        handleGoogleAnalytics();
    }

    //Bởi vì kiểu nào xin xong cũng gọi nên override thẳng trong đây để dùng cho mọi acitivity
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Helper.call(activity);
                } else {
                    Helper.showErrorDialog(activity, "Xin Quyền Không Thành Công!");
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    //region Google Analytics
    private Tracker mTracker;

    public void handleGoogleAnalytics() {
        LixiApplication application = (LixiApplication) getApplication();
        mTracker = application.getDefaultTracker();
//
//        String uri = "";
//        if (ConstantValue.SOCKET_UTM_MODEL != null) {
//            SocketUtmModel utmModel = ConstantValue.SOCKET_UTM_MODEL;
//            StringBuilder builder = new StringBuilder();
//
//            if (utmModel.getSource() != null)
//                builder.append("utm_source=").append(utmModel.getSource());
//            if (utmModel.getCampaign() != null)
//                builder.append(builder.length() == 0 ? "utm_campaign=" : "&utm_campaign=").append(utmModel.getCampaign());
//            if (utmModel.getContent() != null)
//                builder.append(builder.length() == 0 ? "utm_content=" : "&utm_content=").append(utmModel.getContent());
//            if (utmModel.getMedium() != null)
//                builder.append(builder.length() == 0 ? "utm_medium=" : "&utm_medium=").append(utmModel.getMedium());
//            if (utmModel.getTerm() != null)
//                builder.append(builder.length() == 0 ? "utm_term=" : "&utm_term=").append(utmModel.getTerm());
//
//            uri = builder.toString();
//
//        }
        //String uri = (new AppPreferenceHelper(this)).getCampaignData();
        if (getScreenName() == null) {
            return;
        }
        mTracker.setScreenName(getScreenName() + " Screen");

        if (ApplicationLifecycle.UTM_QUERY == null ||
                ApplicationLifecycle.UTM_QUERY.length() == 0) {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    //.setCampaignParamsFromUrl(uri)
                    .build());
        } else {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCampaignParamsFromUrl(ApplicationLifecycle.UTM_QUERY)
                    .build());
            (new AppPreferenceHelper(this)).setCampaignData("");
        }
    }

    //endregion

    //region PUSH

    public void receiveNotificationData() {
        if (new AppPreferenceHelper(activity).getIsPushSystem()) {
            new AppPreferenceHelper(activity).setIsPushSystem(false);
            NotificationModel notificationModel = Helper.readNotidfyFromFile(activity, ConstantValue.NOTIFY_MODEL_FILE);
            if (notificationModel != null) {
                Log.d("mPush", ">goToActionPush>" + notificationModel.getType() + ">>" + notificationModel.getObject_id());
                handlePushNotification(notificationModel, true);
            }
        }
    }

    private void setUpPushNotificationReceiver() {
        final IntentFilter intentPush = new IntentFilter(ConstantValue.PUSH_SYSTEM_INTENT);
        registerReceiver(receiver, intentPush);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(ConstantValue.PUSH_SYSTEM_INTENT)) {
                NotificationModel notificationModel = Helper.readNotidfyFromFile(activity, ConstantValue.NOTIFY_MODEL_FILE);
                if (notificationModel != null) {
                    Log.d("mPush", ">goToActionPush>BCR" + notificationModel.getType() + ">>" + notificationModel.getObject_id());
                    handlePushNotification(notificationModel, false);
                }
            }
        }
    };

    private void handlePushNotification(NotificationModel notificationModel, boolean b) {
        Log.d("mPush", ">goToActionPush>" + notificationModel.getObject_id());
        DataLoader.setReadNotification(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                //do nothing
            }
        }, notificationModel.getPublic_id());
        final NotificationModel model = notificationModel;
//        new ReadedNotifyController(getCurrentActivity(), notifycationModel);
        Runnable notiRunnable = new Runnable() {
            @Override
            public void run() {
                handleNotificationAction(model);
            }
        };
        new Handler(Looper.getMainLooper()).postDelayed(notiRunnable, 1000);
    }

    private void handleNotificationAction(final NotificationModel notificationModel) {
        Log.d("mPush", ">setReadedNotify>>>type" + notificationModel.getType() + ">id>" + notificationModel.getObject_id());

        //set utm to send to tracking
        SocketUtmModel socketUtmModel = new SocketUtmModel();
        socketUtmModel.setSource("crm");
        socketUtmModel.setCampaign("title_pro");
        socketUtmModel.setMedium("noti");
        ConstantValue.SOCKET_UTM_MODEL = socketUtmModel;

        //config notification
        String modelType = notificationModel.getType();
        OrderDetailDialog detailDialog;
        if (Helper.readNotidfyFromFile(activity, ConstantValue.NOTIFY_MODEL_FILE) == null) {
            return;
        }
        activity.deleteFile(ConstantValue.NOTIFY_MODEL_FILE);
        try {
            switch (modelType) {

                case NotificationModel.POS_POINT_ADD_WAIT:
                case NotificationModel.POS_POINT_ADD_WAIT_OLD:
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(notificationModel.getDetail_id(), modelType);
                    break;
                case NotificationModel.POS_POINT_ADD_DONE:
                case NotificationModel.POS_POINT_ADD_DONE_OLD:
                    String idDt = notificationModel.getDetail_id();
                    if (idDt == null || idDt.length() == 0)
                        idDt = notificationModel.getObject_id();
                    if (idDt == null || idDt.length() == 0)
                        idDt = "";
                    if (idDt.indexOf("pointin-") < 0)
                        idDt = "pointin-" + idDt;
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(idDt, modelType);
                    break;
                case NotificationModel.POS_POINT_ADD_CANCEL:
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(notificationModel.getDetail_id(), modelType);
                    break;
                case NotificationModel.ORDER_CODE:
                    LixiCodeDetailDialog codeDetailDialog = new LixiCodeDetailDialog(activity, true, true, false);
                    codeDetailDialog.show();
                    codeDetailDialog.setData(Long.parseLong(notificationModel.getObject_id()));
                    break;
                case NotificationModel.SALE_ORDER_CANCEL:
                case NotificationModel.SALE_ORDER_CHANGE:
                case NotificationModel.ORDER_ADD:
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(notificationModel.getDetail_id(), modelType);
                    break;
                case NotificationModel.ADMIN_SURVEY:
                case NotificationModel.SURVEY:
                    getSurvey(notificationModel);
                    break;
                case NotificationModel.SURVEY_CODE:
                    getSurveyCode(notificationModel);
                    break;
                case NotificationModel.PAYMENT_ADD:
                case NotificationModel.PAYMENT_ADD_BUY_CARD:
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(notificationModel.getDetail_id(), modelType);
                    try {
                        copyPin(notificationModel);
                    } catch (Exception ex) {
                    }
                    break;
                case NotificationModel.PAYMENT_ADD_TOPUP:
                    detailDialog = new OrderDetailDialog(activity, false, true, false);
                    detailDialog.show();
                    detailDialog.setData(notificationModel.getDetail_id(), modelType);
                    break;
                case NotificationModel.PROMOTION_ADD:
                case NotificationModel.PROMOTION_EXPIRE:
//                CustomFragmentActivity.startPromotionDetailActivity(this, notifycationModel.getObject_id(), true);
                    break;
                case NotificationModel.ADMIN_MERCHANT_ADD:
                    MerchantModel mc = new MerchantModel();
                    mc.setId(notificationModel.getObject_id());
                    Intent merchant = new Intent(activity, MerchantDetailActivity.class);
                    Bundle data = new Bundle();
                    data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(mc));
                    data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.PUSH_NOTIFICATION);
                    merchant.putExtras(data);
                    activity.startActivity(merchant);
                    break;
                case NotificationModel.ADMIN_PROMOTION_ADD:
//                CustomFragmentActivity.startPromotionDetailActivity(this, notifycationModel.getObject_id(), true);
                    break;
                case NotificationModel.SHOP_BUY_FAILED:
                case NotificationModel.SHOP_BUY_SUCCESS:
                    OrderShopDetailDialog dialog = new OrderShopDetailDialog(activity, true, true, false);
                    dialog.show();
                    dialog.setData(notificationModel.getDetail_id());
                    break;
                case NotificationModel.ADMIN_LINK_ADD:
                    if (notificationModel.getUrl() != null && notificationModel.getUrl().length() > 0) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notificationModel.getUrl()));
                        startActivity(browserIntent);
                    }
                    break;
                case NotificationModel.ADMIN_ADD_PUSH:
                    //ve trang chu
//                isChangeTab = true;
//                tabHost.setCurrentTab(0);
                    break;
                case NotificationModel.VOUCHER_DETAIL:
                    String id = notificationModel.getObject_id();
                    openVoucherDetail(id);
                    break;
                case NotificationModel.USER_ORDER_CONFIRMED:
                case NotificationModel.USER_ORDER_PICKED:
                case NotificationModel.USER_ORDER_CANCEL:
                case NotificationModel.USER_PAYMENT_ORDER_COMPLETED:
                case NotificationModel.USER_PAYMENT_ORDER_CANCELED:
                    Bundle bundle = new Bundle();
                    bundle.putString("uuid", notificationModel.getObject_id());
                    Intent intent = new Intent(activity, OrderTrackingActivity.class);
                    intent.putExtras(bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                    break;
                case NotificationModel.DELI_STORE_DETAIL:
                    Bundle dataStore = new Bundle();
                    dataStore.putLong("ID", Long.parseLong(notificationModel.getObject_id()));
                    dataStore.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.PUSH_NOTIFICATION);
                    Intent intentStore = new Intent(activity, StoreDetailActivity.class);
                    intentStore.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentStore.putExtras(dataStore);
                    activity.startActivity(intentStore);
                    AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(true);
                    break;
                case NotificationModel.DELI_ARTICLE_DETAIL:
                    StoreListOfArticleActivity.startArticleActivity(activity, 0L,
                            Long.parseLong(notificationModel.getObject_id()), "", "",
                            TrackingConstant.ContentSource.PUSH_NOTIFICATION);
                    break;
                case NotificationModel.DELI_PROMOTION_DETAIL:
                    break;
                case NotificationModel.INTRO_CODE:
                    IntroCodeActivity.start(activity);
                    break;
                case NotificationModel.DELI_TOPIC_DETAIL:
                    Bundle dataTopic = new Bundle();
                    dataTopic.putLong("TOPIC_ID", Long.valueOf(notificationModel.getObject_id()));
                    Intent intentTopic = new Intent(activity, TopicActivity.class);
                    intentTopic.putExtras(dataTopic);
                    activity.startActivity(intentTopic);
                    break;
                case NotificationModel.WEB_GAME:
                    if (LoginHelper.isLoggedIn(activity)) {
                        Intent intentGame = new Intent(activity, GameActivity.class);
                        intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.PUSH_NOTIFICATION);
                        activity.startActivity(intentGame);
                    } else {
                        LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_PLAY_GAME);
                    }
                    break;
                case NotificationModel.HOME_LIXI_SHOP:
                    HomeActivity.getInstance().tab = Integer.parseInt(notificationModel.getObject_id()) + 1;
                    HomeActivity.getInstance().gotoTab();
                    Intent homeIntent = new Intent(activity, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(homeIntent);
                    break;
                case NotificationModel.DELI_USER_ORDER:
//                    HistoryReceiveLixiDialog dialogLixi = new HistoryReceiveLixiDialog(activity, false, true, false);
//                    dialogLixi.show();
                    // TODO: set data for dialog
                    // dialogLixi.setData("");
                    break;
                default:
                    //do (fucking) absolutely nothing
                    break;
            }
        } catch (Exception ex) {
        }

    }

    private void openVoucherDetail(String id) {
        long _id = Long.parseLong(id);
//        activity.startActivity(new Intent(activity, CashEvoucherDetailActivity.class).putExtra(CashEvoucherDetailActivity.PRODUCT_ID, _id));
        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, _id, TrackingConstant.ContentSource.PUSH_NOTIFICATION);
    }

    public void copyPin(Object object) {
        if (((NotificationModel) object).getType().equals("payment.add.buycard")) {
            String tt = ((NotificationModel) object).getBody();
            //Bạn vừa sử dụng OC Credit cho việc Mua thẻ cào [Viettel] serial: 57706827880, code:6552989031555
            int l = tt.length();
            int st = tt.indexOf("code:");
            if (st >= 0) {
                int e = st + 6;
                while (e < l && tt.charAt(e) >= '0' && tt.charAt(e) <= '9')
                    e++;
                tt = tt.substring(st + 5, e).trim();
                Helper.copyCode(tt, activity);
            } else {
                Toast.makeText(activity, "Tin nhắn không đúng, không thể copy mã pin!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void getSurvey(NotificationModel notifycationModel) {
        //TODO servey
//        new CallServiceTask().userGetSurvey(activity,
//                ApiEntity.GET_SURVEY + "/" + notifycationModel.getObject_id(),
//                true, new CallServiceCallbackObj() {
//                    @Override
//                    public void callBack(Boolean isSuccess, Object objects) {
//                        if (isSuccess) {
//                            SurveyModel jsonResords = (new Gson()).fromJson(objects.toString(), SurveyModel.class);
//
//                            dialogInviteSurvey = new PopupSurveyInv(activity, "", "", "", false, true, false);
//                            dialogInviteSurvey.setIDidalogEventListener(new PopupSurveyInv.IDidalogEvent() {
//                                @Override
//                                public void onOk(SurveyModel surveyModel) {
//                                    startSurvey(surveyModel);
//                                }
//                            });
//                            dialogInviteSurvey.setCancelable(false);
//                            dialogInviteSurvey.show();
//                            dialogInviteSurvey.setData(jsonResords);
//                        } else {
//                            PopupNotifySimple dialog = new PopupNotifySimple((CustomFragmentActivity) activity, objects.toString(), false, true, false);
//                            dialog.show();
//                        }
//                    }
//                });
    }

    public void getSurveyCode(NotificationModel notifycationModel) {
        //TODO API
//        new CallServiceTask().userGetSurveyCode(activity,
//                ApiEntity.SURVEY_CODE + "/" + notifycationModel.getObject_id(),
//                true, new CallServiceCallbackObj() {
//                    @Override
//                    public void callBack(Boolean isSuccess, Object objects) {
//                        if (isSuccess) {
//                            LixiCodeModel jsonResords = (new Gson()).fromJson(objects.toString(), LixiCodeModel.class);
//
//                            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
//                            ClipData clip = ClipData.newPlainText("reward code copyed", jsonResords.getCode());
//                            clipboard.setPrimaryClip(clip);
//
//                            Toast.makeText(activity, "Đã copy mã khuyến mãi: " + jsonResords.getCode(), Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            PopupNotifySimple dialog = new PopupNotifySimple(activity, objects.toString(), false, true, false);
//                            dialog.show();
//                        }
//                    }
//                });
    }

    //endregion

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();
        hideProgressBar();
    }

    @Override
    public String getScreenName() {
        return null;
    }

    private ProgressDialog dialog = null;

    public void showProgressBar() {
        //Sanity check
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = new ProgressDialog(activity);
        dialog.show();
    }

    public void hideProgressBar() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
