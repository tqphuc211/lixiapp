package com.opencheck.client.dataloaders;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.account.evoucher.Model.ActiveCodeApiBody;
import com.opencheck.client.home.account.evoucher.Model.EVoucherSummary;
import com.opencheck.client.home.account.evoucher.Model.EVoucherUnused;
import com.opencheck.client.home.account.evoucher.Model.GroupCode;
import com.opencheck.client.home.account.evoucher.Model.GroupCodeApiBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodModel;
import com.opencheck.client.home.account.new_layout.fragment.UserFragment;
import com.opencheck.client.home.account.new_layout.model.DetailTransaction;
import com.opencheck.client.home.account.new_layout.model.HistoryIntroModel;
import com.opencheck.client.home.account.new_layout.model.UserTransaction;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindRequest;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindResponse;
import com.opencheck.client.home.flashsale.Model.FlashSaleTime;
import com.opencheck.client.home.rating.model.MarkUsedResponse;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.home.rating.model.RatingOption;
import com.opencheck.client.home.rating.model.RatingResponse;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.ErrorBody;
import com.opencheck.client.models.FAQModel;
import com.opencheck.client.models.FacebookTokenModel;
import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperModel;
import com.opencheck.client.models.lixishop.ActiveCodeModel;
import com.opencheck.client.models.lixishop.CompletedVoucherModel;
import com.opencheck.client.models.lixishop.DetailHistoryCardModel;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.models.lixishop.LixiProductModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.LixiShopCategoryModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.models.lixishop.LixiShopTransactionResultModel;
import com.opencheck.client.models.lixishop.OrderCheckModel;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.lixishop.QuantityProductModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.models.merchant.BannerModel;
import com.opencheck.client.models.merchant.CardValueModel;
import com.opencheck.client.models.merchant.DiscountModel;
import com.opencheck.client.models.merchant.MenuModel;
import com.opencheck.client.models.merchant.MerchantHotBrandModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.models.merchant.PaymentPartnerModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.models.merchant.survey.SurveyAnswerResultModel;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.LixiCodeDetailModel;
import com.opencheck.client.models.user.LixiCodeModel;
import com.opencheck.client.models.user.OrderHistoryModel;
import com.opencheck.client.models.user.TokenModel;
import com.opencheck.client.models.user.UserLoginModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class DataLoader {
    public static String token = "";
    public static int region = -1;
    public static String lang = "";
    private static OkHttpClient authenClient = null;
    private static Retrofit.Builder retrofitBuilder = null;
    public static ApiCall apiService = null;
    public static final int record_per_page = 20;
    private static String api_error = "API CALL ERROR!";
    //    public static UserModel userModel = null;
    private static final int CONNECTION_TIMEOUT = 40;
    private static final int WRITE_TIMEOUT = 40;
    private static final int READ_TIMEOUT = 40;
    //region config api call

    private static void createAuthenInterceptors(final String token, final int region, final String lang) {
        authenClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();

                if (!token.equals("JWT ")) {
                    Request.Builder builder = originalRequest.newBuilder().addHeader("Authorization", token);
                    builder = builder.addHeader("platform", "android");
                    if (region >= 0) {
                        builder = builder.addHeader("region", String.valueOf(region));
                    }
                    builder.addHeader("lang", lang);
                    builder.addHeader("version", BuildConfig.VERSION_NAME);
                    originalRequest = builder.build();
                } else {
                    Request.Builder builder = originalRequest.newBuilder().addHeader("platform", "android");
                    if (region >= 0) {
                        builder = builder.addHeader("region", String.valueOf(region));
                    }
                    builder.addHeader("lang", lang);
                    builder.addHeader("version", BuildConfig.VERSION_NAME);
                    originalRequest = builder.build();
                }
                Helper.showLog(originalRequest.toString());
                if (originalRequest.body() != null) {
                    final Buffer buffer = new Buffer();
                    originalRequest.body().writeTo(buffer);
                    Helper.showLog("Body: " + buffer.readUtf8());
                    Helper.showLog("Header: " + originalRequest.headers().toString());

                }
                return chain.proceed(originalRequest);

            }
        }).connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor())
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    private static HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    private static String getErrorMessage(ResponseBody responseBody) {
        try {
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(responseBody.string()).getAsJsonObject();
            Type type = new TypeToken<ApiWrapperModel<ErrorBody>>() {
            }.getType();
            ApiWrapperModel<ErrorBody> wrapperModel = new Gson().fromJson(jsonObject, type);
            ErrorBody errorModel = wrapperModel.getRecords();
            return errorModel.getMessage_code();
        } catch (Exception ex) {
            return "error";
        }
    }

    //MUST call when user logged in
    public static ApiCall initAPICall(LixiActivity activity, String _token, final ApiCallBack callBack) {
        token = _token;
        region = (new AppPreferenceHelper(activity).getRegion());
        lang = (new AppPreferenceHelper(activity).getLanguageSource());
        if (lang.equals("")) {
            lang = ConstantValue.LANG_CONFIG_VN;
        }
        createAuthenInterceptors(token, region, lang);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(authenClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
        apiService = retrofitBuilder.build().create(ApiCall.class);
        if (!token.equals("JWT ")) {
            getUserProfile(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    //get User profile
                    if (callBack != null) {
                        if (isSuccess) {
                            ApiWrapperModel<UserModel> result = (ApiWrapperModel<UserModel>) object;
                            if (UserModel.getInstance() != null) {
                                callBack.handleCallback(true, UserModel.getInstance());
                            } else {
                                callBack.handleCallback(false, result.getRecords().getMessage_code());
                            }
                        } else {
                            if (object != null)
                                callBack.handleCallback(false, object);
                            else
                                callBack.handleCallback(false, "Lỗi không xác định");
                        }
                    }
                }
            }, "JustForInit");
        }
        return apiService;
    }

    public static void clearDeviceToken() {
        token = "";
    }

    public static void setupAPICall(LixiActivity activity, ApiCallBack _callback) {
        checkInternet(activity);
        if (token == null || token.length() == 0)
            try {
                token = (new AppPreferenceHelper(activity).getUserToken());
            } catch (Exception ex) {
            }
        if (apiService == null)
            initAPICall(activity, token, null);
    }

    public static void setupAPICallForPost(LixiActivity activity, ApiCallBack callBack) {
        setupAPICall(activity, callBack);
//        Helper.showProgressBar(activity);
        activity.showProgressBar();
    }

    //endregion

    //region POST


    public static void postUserAccessToken(final LixiActivity activity, String facebookToken, String facebookSource, final ApiCallBack _callBack) {

        setupAPICallForPost(activity, _callBack);

        FacebookTokenModel facebookTokenModel = new FacebookTokenModel();
        facebookTokenModel.setToken(facebookToken);
        facebookTokenModel.setSource(facebookSource);

        Call<TokenModel> call = apiService._postUserAccessToken(facebookTokenModel);

        call.enqueue(new Callback<TokenModel>() {

            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    TokenModel result = response.body();
                    _callBack.handleCallback(true, result);
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });


    }

    public static void logoutPushToken(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<JsonObject> call = apiService._logoutPushToken(Helper.getDeviceID(activity));

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.showLog(response.toString());
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, "");
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void sendPushTokenGuest(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICall(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._sendPushTokenGuest(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.showLog(response.toString());
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, "");
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void sendPushToken(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICall(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._sendPushToken(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.showLog(response.toString());
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, "");
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void createPaymentService(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._createPaymentService(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records").get("message").getAsString());
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void readAllNotication(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        JSONObject params = new JSONObject();
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._readAllNotification(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    Object result = response.body();
                    JSONObject jsonResult = objectToJSONObject(result);
                    String status = null;
                    try {
                        status = jsonResult.getJSONObject("_meta").getString("status");
                        if (status.equals(ConstantValue.SUCCESS)) {
                            _callBack.handleCallback(true, jsonResult.getJSONObject("records").getString("message"));
                        } else {
                            Helper.showErrorDialog(activity, jsonResult.getJSONObject("records").getString("message_code"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void useRewardCode(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._useRewardCode(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void checkPhone(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._checkPhone(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records").get("is_available").getAsBoolean());
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void verifyPhone(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._verifyPhone(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject jsonResult = response.body();

                    Helper.showLog(jsonResult.toString());

                    if (jsonResult != null) {
                        JsonObject jsonMeta = jsonResult.getAsJsonObject("_meta");
                        JsonObject jsonResords = jsonResult.getAsJsonObject("records");

                        String status = jsonMeta.get("status").getAsString();

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            String mess = jsonResords.get("message").getAsString();
                            _callBack.handleCallback(true, mess);
                        } else {
                            String mess = jsonResords.get("message_code").getAsString();
                            _callBack.handleCallback(false, mess);
                        }
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void exchangeLixiShopProduct(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<LixiShopTransactionResultModel>> call = apiService._buyProductLixiShop(requestBody);

        call.enqueue(new Callback<ApiWrapperModel<LixiShopTransactionResultModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiShopTransactionResultModel>> call,
                                   Response<ApiWrapperModel<LixiShopTransactionResultModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiShopTransactionResultModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiShopTransactionResultModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void exchangeLixiShopProductV2(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<LixiShopTransactionResultModel>> call = apiService._buyProductLixiShopV2(requestBody);

        call.enqueue(new Callback<ApiWrapperModel<LixiShopTransactionResultModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiShopTransactionResultModel>> call,
                                   Response<ApiWrapperModel<LixiShopTransactionResultModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiShopTransactionResultModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiShopTransactionResultModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void postLixiDiscount(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
//        setupAPICallForPost(activity, _callBack);
        setupAPICall(activity, _callBack); // chổ này ko cần loading

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<DiscountModel>> call = apiService._postDiscount(requestBody);

        call.enqueue(new Callback<ApiWrapperModel<DiscountModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<DiscountModel>> call, Response<ApiWrapperModel<DiscountModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<DiscountModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<DiscountModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void buyCashProduct(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params, String method, String... loading) {
        if (loading.length > 0)
            setupAPICallForPost(activity, _callBack);
        else
            setupAPICall(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._buyCashVoucher(requestBody, method);

        if (method.indexOf("napas") >= 0)
            call = apiService._buyCashVoucherNapas(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void buyCashProductV2(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params, String method) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._buyCashVoucherV2(requestBody, method);

        if (method.indexOf("napas") >= 0)
            call = apiService._buyCashVoucherNapasV2(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void buyProductNapas(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);

        Call<JsonObject> call = apiService._buyCashVoucherNapas(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void buyProductNapasV2(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);

        Call<JsonObject> call = apiService._buyCashVoucherNapasV2(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void addFavouriteMerchant(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._addFavouriteMerchant(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void updateProfile(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<UserModel>> call = apiService._updateProfile(requestBody);

        call.enqueue(new Callback<ApiWrapperModel<UserModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserModel>> call, Response<ApiWrapperModel<UserModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        UserModel.SaveProfile(result.getRecords());
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void updateAvatar(final LixiActivity activity, final ApiCallBack _callBack, String paths) {
        setupAPICallForPost(activity, _callBack);
        //  String strParams = params.toString();
        File file = new File(paths);
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), fileBody);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        Call<ApiWrapperModel<UserModel>> call = apiService._updateAvatar(body, name);

        call.enqueue(new Callback<ApiWrapperModel<UserModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserModel>> call, Response<ApiWrapperModel<UserModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        UserModel.SaveProfile(result.getRecords());
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void sendFeedback(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._sendFeedback(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitActiceCode(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<ActiveCodeModel>> call = apiService._submitActiveCode(requestBody);

        call.enqueue(new Callback<ApiWrapperModel<ActiveCodeModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<ActiveCodeModel>> call,
                                   Response<ApiWrapperModel<ActiveCodeModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<ActiveCodeModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<ActiveCodeModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitActiceCodeMultiple(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperForListModel<ActiveCodeModel>> call = apiService._submitMultipleActiveCode(requestBody);

        call.enqueue(new Callback<ApiWrapperForListModel<ActiveCodeModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<ActiveCodeModel>> call,
                                   Response<ApiWrapperForListModel<ActiveCodeModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<ActiveCodeModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<ActiveCodeModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void buyProductByMomo(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._buyProductByMomo(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call,
                                   Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitToMomo(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params, String url) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._submitToMomo(url, requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call,
                                   Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    _callBack.handleCallback(true, result);
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitTMomo(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params, String url) {
        setupAPICallForPost(activity, _callBack);

        OkHttpClient client = new OkHttpClient();

        try {
            Request request = new Request.Builder()
                    .url(url + params)
                    .addHeader("Authorization", token)
                    .addHeader("platform", "android")
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    handleUnexpectedCallErrorForPost(activity, e);
                }

                @Override
                public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
//                            Helper.hideProgressBar();
                            activity.hideProgressBar();
                            if (response.isSuccessful()) {
                                try {
                                    String strResult = response.body().string();
                                    JSONObject jsonResult = new JSONObject(strResult);
                                    _callBack.handleCallback(true, jsonResult);
//                                    String status = jsonResult.getJSONObject("_meta").getString("status");
//                                    if (status.equals(ConstantValue.SUCCESS)) {
//                                        String JsonItem = jsonResult.getJSONObject("records").toString();
//                                        LixiShopBankTransactionResult item = new Gson().fromJson(JsonItem, LixiShopBankTransactionResult.class);
//                                        _callBack.handleCallback(true, item);
//                                    } else {
//                                        Helper.showErrorDialog(activity, jsonResult.getJSONObject("records").getString("message_code"));
//                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                handleUnsuccessfullRespond(activity, _callBack, response.body(), response.message());
                            }
                        }
                    };
                    new Handler(Looper.getMainLooper()).post(runnable);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void buyProductByZaloWallet(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<JsonObject> call = apiService._buyProductByZaloWallet(requestBody);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitToZaloWallet(final LixiActivity activity, String url, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), params.toString());
        Call<JsonObject> call = apiService._submitToZaloWallet(url, requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    _callBack.handleCallback(true, result);
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    //endregion

    //region Delete
    public static void deletePendingOrder(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICallForPost(activity, _callBack);

        Call<JsonObject> call = apiService._deletePendingOrder(id);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records"));
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }
    //endregion

    //region GET

    public static void getUserConfig(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);
        Call<ApiWrapperModel<ConfigModel>> userConfig = apiService._getUserConfig();

        userConfig.enqueue(new Callback<ApiWrapperModel<ConfigModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<ConfigModel>> call, Response<ApiWrapperModel<ConfigModel>> response) {
                if (response.isSuccessful()) {
//                    Helper.hideProgressBar();
                    activity.hideProgressBar();
                    ApiWrapperModel<ConfigModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                        try {
                            ConfigModel.SaveConfig(activity, result.getRecords());
                        } catch (Exception e) {
                        }
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<ConfigModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserConfig(final LixiActivity activity) {
        setupAPICall(activity, null);
        Call<ApiWrapperModel<ConfigModel>> userConfig = apiService._getUserConfig();

        userConfig.enqueue(new Callback<ApiWrapperModel<ConfigModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<ConfigModel>> call, Response<ApiWrapperModel<ConfigModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<ConfigModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        try {
                            ConfigModel.SaveConfig(activity, result.getRecords());
                        } catch (Exception e) {
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<ConfigModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserProfile(final LixiActivity activity, final ApiCallBack _callBack, final String... forInit) {
        setupAPICallForPost(activity, _callBack);

        Call<ApiWrapperModel<UserModel>> call = apiService._getUserProfile();

        call.enqueue(new Callback<ApiWrapperModel<UserModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserModel>> call, Response<ApiWrapperModel<UserModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        UserModel.SaveProfile(result.getRecords());
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserModel>> call, Throwable t) {
                activity.hideProgressBar();
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getMerchantHotBrand(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<MerchantHotBrandModel>> call = apiService._getMerchantHotBranch(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantHotBrandModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantHotBrandModel>> call,
                                   Response<ApiWrapperForListModel<MerchantHotBrandModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantHotBrandModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantHotBrandModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getSpecialEvoucher(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = apiService._getSpecialEvoucher(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getBannerImage(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<BannerModel>> call = apiService._getBannerImage();

        call.enqueue(new Callback<ApiWrapperForListModel<BannerModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<BannerModel>> call, Response<ApiWrapperForListModel<BannerModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<BannerModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<BannerModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void searchListMerchant(final LixiActivity activity, final ApiCallBack _callBack, int page, String key, boolean isTag) {

        setupAPICall(activity, _callBack);

        String tag_name = null;
        String keyword = null;
        String u_id = null;

        if (isTag) {
            tag_name = key;
        } else {
            keyword = key;
        }

        if (LoginHelper.isLoggedIn(activity) && UserModel.getInstance() != null) {
            u_id = UserModel.getInstance().getId();
        }

        Call<ApiWrapperForListModel<MerchantModel>> call = apiService._searchMerchant(page,
                record_per_page, tag_name, keyword, u_id);

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantModel>> call,
                                   Response<ApiWrapperForListModel<MerchantModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void searchVoucherByLixi(final LixiActivity activity, final ApiCallBack _callBack, int page, String keyword) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiProductModel>> call = apiService._searchVoucherByLixi(page, record_per_page, keyword);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiProductModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiProductModel>> call,
                                   Response<ApiWrapperForListModel<LixiProductModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiProductModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiProductModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void searchVoucher(final LixiActivity activity, final ApiCallBack _callBack, int page, String keywords) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = apiService._searchVoucher(page, record_per_page, keywords);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void setReadNotification(final LixiActivity activity, final ApiCallBack _callBack, String public_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<LixiModel>> call = apiService._setReadNofication(public_id);

        call.enqueue(new Callback<ApiWrapperModel<LixiModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiModel>> call, Response<ApiWrapperModel<LixiModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getNotificationCount(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<Object> call = apiService._getNotificationCount();

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Object result = response.body();
                    JSONObject jsonResult = objectToJSONObject(result);
                    String status = null;
                    try {
                        status = jsonResult.getJSONObject("_meta").getString("status");
                        if (status.equals(ConstantValue.SUCCESS)) {
                            _callBack.handleCallback(true, jsonResult.getJSONObject("records").getInt("count"));
                        } else {
                            Helper.showErrorDialog(activity, jsonResult.getJSONObject("records").getString("message_code"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getNotificationCount(final ApiCallBack _callBack) {
        if (apiService == null) {
            return;
        }

        Call<Object> call = apiService._getNotificationCount();

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Object result = response.body();
                    JSONObject jsonResult = objectToJSONObject(result);
                    String status = null;
                    try {
                        status = jsonResult.getJSONObject("_meta").getString("status");
                        if (status.equals(ConstantValue.SUCCESS)) {
                            _callBack.handleCallback(true, jsonResult.getJSONObject("records").getInt("count"));
                        } else {
                            _callBack.handleCallback(false, 0);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    _callBack.handleCallback(false, 0);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                _callBack.handleCallback(false, 0);
            }
        });
    }

    public static void getNotificationList(final LixiActivity activity, final ApiCallBack _callBack, int page, String type) {
        setupAPICall(activity, _callBack);

        int region = 0;
        DeliAddressModel deliAddressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (deliAddressModel != null) {
            region = deliAddressModel.getRegion();
        }

        Call<ApiWrapperForListModel<NotificationModel>> call = apiService._getNotification(page, record_per_page, type, region);

        call.enqueue(new Callback<ApiWrapperForListModel<NotificationModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<NotificationModel>> call,
                                   Response<ApiWrapperForListModel<NotificationModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<NotificationModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<NotificationModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getCardValue(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<Object> call = apiService._getCardValue();

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    Object result = response.body();
                    JsonObject tmpRes = gson.toJsonTree(result).getAsJsonObject();

                    String status = null;
                    try {
                        JSONObject jsonResult = new JSONObject(tmpRes.toString());
                        status = jsonResult.getJSONObject("_meta").getString("status");
                        if (status.equals(ConstantValue.SUCCESS)) {
                            ArrayList<CardValueModel> listItem = new ArrayList<>();

                            Iterator<String> iter = jsonResult.getJSONObject("records").keys();
                            while (iter.hasNext()) {
                                String key = iter.next();
                                String value = jsonResult.getJSONObject("records").getString(key);

                                CardValueModel cardValueModel = new CardValueModel();
                                cardValueModel.setData(key, value);
                                listItem.add(cardValueModel);
                            }
                            _callBack.handleCallback(true, listItem);
                        } else {
                            Helper.showErrorDialog(activity, jsonResult.getJSONObject("records").getString("message_code"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getPaymentPartner(final LixiActivity activity, final ApiCallBack _callBack, String service_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<PaymentPartnerModel>> call = apiService._getPaymentPartner(service_id);

        call.enqueue(new Callback<ApiWrapperForListModel<PaymentPartnerModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<PaymentPartnerModel>> call,
                                   Response<ApiWrapperForListModel<PaymentPartnerModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<PaymentPartnerModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<PaymentPartnerModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopCategory(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopCategoryModel>> call = apiService._getLixiShopCategory(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopCategoryModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopCategoryModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopCategoryModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopCategoryModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopCategoryModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopServiceCategory(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<ServiceCategoryModel>> call = apiService._getLixiShopPaymentService(0);

        call.enqueue(new Callback<ApiWrapperForListModel<ServiceCategoryModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<ServiceCategoryModel>> call,
                                   Response<ApiWrapperForListModel<ServiceCategoryModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<ServiceCategoryModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<ServiceCategoryModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopHotProduct(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiHotProductModel>> call = apiService._getLixiShopHotProduct(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiHotProductModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiHotProductModel>> call,
                                   Response<ApiWrapperForListModel<LixiHotProductModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiHotProductModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiHotProductModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getNumLixiCode(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<JsonObject> call = apiService._getNumLixiCode();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject result = response.body();
                    String status = result.getAsJsonObject("_meta").get("status").getAsString();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getAsJsonObject("records").get("count").getAsInt());
                    } else {
                        Helper.showErrorDialog(activity, result.getAsJsonObject("records").get("message_code").getAsString());
                    }

                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopProductCategory(final LixiActivity activity, final ApiCallBack _callBack, int page, String category) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiProductModel>> call = apiService._getLixiShopProductCategory(page, record_per_page, category);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiProductModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiProductModel>> call,
                                   Response<ApiWrapperForListModel<LixiProductModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiProductModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiProductModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopRewardCode(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiCodeModel>> call = apiService._getLixiShopRewardCode(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiCodeModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiCodeModel>> call,
                                   Response<ApiWrapperForListModel<LixiCodeModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiCodeModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiCodeModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopRewardCodeBanner(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiCodeBarnerModel>> call = apiService._getLixiShopRewardCodeBanner();

        call.enqueue(new Callback<ApiWrapperForListModel<LixiCodeBarnerModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiCodeBarnerModel>> call,
                                   Response<ApiWrapperForListModel<LixiCodeBarnerModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiCodeBarnerModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiCodeBarnerModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getLixiShopRewardCodeDetail(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<LixiCodeDetailModel>> call = apiService._getRewardCode(id);

        call.enqueue(new Callback<ApiWrapperModel<LixiCodeDetailModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiCodeDetailModel>> call, Response<ApiWrapperModel<LixiCodeDetailModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiCodeDetailModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiCodeDetailModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getProductDetail(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<LixiShopProductDetailModel>> call = apiService._getLixiShopProductDetail(id);

        call.enqueue(new Callback<ApiWrapperModel<LixiShopProductDetailModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiShopProductDetailModel>> call, Response<ApiWrapperModel<LixiShopProductDetailModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiShopProductDetailModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiShopProductDetailModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getProductQuantity(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<QuantityProductModel>> call = apiService._getLixiShopProductQuantity(id);

        call.enqueue(new Callback<ApiWrapperModel<QuantityProductModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<QuantityProductModel>> call, Response<ApiWrapperModel<QuantityProductModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<QuantityProductModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<QuantityProductModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void checkProduct(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICallForPost(activity, _callBack);

        Call<ApiWrapperModel<OrderCheckModel>> call = apiService._checkProduct(id);

        call.enqueue(new Callback<ApiWrapperModel<OrderCheckModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<OrderCheckModel>> call, Response<ApiWrapperModel<OrderCheckModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<OrderCheckModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<OrderCheckModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void handleBuyAtStoreCallback(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICallForPost(activity, _callBack);

        Call<ApiWrapperModel<LixiShopBankTransactionResult>> call = apiService._getBuyAtStore(id);

        call.enqueue(new Callback<ApiWrapperModel<LixiShopBankTransactionResult>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiShopBankTransactionResult>> call,
                                   Response<ApiWrapperModel<LixiShopBankTransactionResult>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiShopBankTransactionResult> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiShopBankTransactionResult>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void checkShoppingStatus(final LixiActivity activity, final ApiCallBack _callBack, String params, String... napas) {
        setupAPICallForPost(activity, _callBack);

        OkHttpClient client = new OkHttpClient();

        try {
            Request request = new Request.Builder()
                    .url(BuildConfig.BASE_URL + "/" + ApiEntity.PAYMENT_CALLBACK + params)
                    .addHeader("Authorization", token)
                    .addHeader("platform", "android")
                    .build();

            if (napas.length > 0)
                request = new Request.Builder()
                        .url(BuildConfig.BASE_URL + "/" + ApiEntity.PAYMENT_CALLBACK_NAPAS + params)
                        .addHeader("Authorization", token)
                        .addHeader("platform", "android")
                        .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    handleUnexpectedCallErrorForPost(activity, e);
                }

                @Override
                public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
//                            Helper.hideProgressBar();
                            activity.hideProgressBar();
                            if (response.isSuccessful()) {
                                try {
                                    String strResult = response.body().string();

                                    JSONObject jsonResult = new JSONObject(strResult);
                                    String status = jsonResult.getJSONObject("_meta").getString("status");
                                    if (status.equals(ConstantValue.SUCCESS)) {
                                        String JsonItem = jsonResult.getJSONObject("records").toString();
                                        LixiShopBankTransactionResult item = new Gson().fromJson(JsonItem, LixiShopBankTransactionResult.class);
                                        _callBack.handleCallback(true, item);
                                    } else {
                                        Helper.showErrorDialog(activity, jsonResult.getJSONObject("records").getString("message_code"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                handleUnsuccessfullRespond(activity, _callBack, response.body(), response.message());
                            }
                        }
                    };
                    new Handler(Looper.getMainLooper()).post(runnable);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getHistory(final LixiActivity activity, final ApiCallBack _callBack, int page, String type,
                                  String store_name, String from_date, String to_date, String state) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<OrderHistoryModel>> call = apiService._getHistoryList(page, record_per_page, type,
                store_name, from_date, to_date, state);

        call.enqueue(new Callback<ApiWrapperForListModel<OrderHistoryModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<OrderHistoryModel>> call,
                                   Response<ApiWrapperForListModel<OrderHistoryModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<OrderHistoryModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<OrderHistoryModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getOrderDetail(final LixiActivity activity, final ApiCallBack _callBack, String id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<OrderDetailModel>> call = apiService._getHistoryDetail(id);

        call.enqueue(new Callback<ApiWrapperModel<OrderDetailModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<OrderDetailModel>> call,
                                   Response<ApiWrapperModel<OrderDetailModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<OrderDetailModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<OrderDetailModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantEvoucherNewList(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = apiService._getMerchantEVoucherNew(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantDetail(final LixiActivity activity, final ApiCallBack _callBack, String id) {
        setupAPICall(activity, _callBack);
        String u_id = null;

        if (LoginHelper.isLoggedIn(activity))
            u_id = UserModel.getInstance().getId();

        Call<ApiWrapperModel<MerchantModel>> call = apiService._getMerchantDetail(id, u_id);

        call.enqueue(new Callback<ApiWrapperModel<MerchantModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<MerchantModel>> call,
                                   Response<ApiWrapperModel<MerchantModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<MerchantModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<MerchantModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantEvoucherList(final LixiActivity activity, final ApiCallBack _callBack, int page, String merchant_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = apiService._getMerchantEVoucher(page, record_per_page, merchant_id);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantStore(final LixiActivity activity, final ApiCallBack _callBack, String merchant_id, LatLng position) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<StoreModel>> call = apiService._getMerchantStore(1, 1000, merchant_id, region, String.valueOf(position.latitude), String.valueOf(position.longitude));

        call.enqueue(new Callback<ApiWrapperForListModel<StoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<StoreModel>> call,
                                   Response<ApiWrapperForListModel<StoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<StoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<StoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantStore(final LixiActivity activity, final ApiCallBack _callBack, int page, String merchant_id, LatLng position, String keyword) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<StoreModel>> call = apiService._getMerchantStore(1, 1000, merchant_id, region, position != null ? String.valueOf(position.latitude) : "", position != null ? String.valueOf(position.longitude) : "", keyword);

        call.enqueue(new Callback<ApiWrapperForListModel<StoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<StoreModel>> call,
                                   Response<ApiWrapperForListModel<StoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<StoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<StoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getMerchantMenu(final LixiActivity activity, final ApiCallBack _callBack, int page, String merchant_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<MenuModel>> call = apiService._getMerchantMenu(merchant_id, page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<MenuModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MenuModel>> call,
                                   Response<ApiWrapperForListModel<MenuModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MenuModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MenuModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getFAQ(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<FAQModel>> call = apiService._getFAQ();

        call.enqueue(new Callback<ApiWrapperForListModel<FAQModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<FAQModel>> call,
                                   Response<ApiWrapperForListModel<FAQModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<FAQModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<FAQModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getBoughtEVoucher(final LixiActivity activity, final ApiCallBack _callBack, int page, String state) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call = apiService._getBoughEVoucher(page, record_per_page, state);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherBoughtModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getNearestStore(final LixiActivity activity, final ApiCallBack _callBack, int page, float lat, float lon) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<NearestStoreModel>> call = apiService._getNearsetStore(page, record_per_page, lat, lon);

        call.enqueue(new Callback<ApiWrapperForListModel<NearestStoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<NearestStoreModel>> call,
                                   Response<ApiWrapperForListModel<NearestStoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<NearestStoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<NearestStoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getCashBoughtEvoucherDetail(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);
//        String u_id = null;
//
//        if (LoginHelper.isLoggedIn(activity))
//            u_id = userModel.getId();

        Call<ApiWrapperModel<EvoucherDetailModel>> call = apiService._getCashBoughtEvoucherDetail(id);

        call.enqueue(new Callback<ApiWrapperModel<EvoucherDetailModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<EvoucherDetailModel>> call,
                                   Response<ApiWrapperModel<EvoucherDetailModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<EvoucherDetailModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<EvoucherDetailModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getCashBoughtEvoucherStoreApply(final LixiActivity activity, final ApiCallBack _callBack, long product_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<StoreApplyModel>> call = apiService._getBoughtEvoucherApplyStore(product_id);

        call.enqueue(new Callback<ApiWrapperForListModel<StoreApplyModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<StoreApplyModel>> call,
                                   Response<ApiWrapperForListModel<StoreApplyModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<StoreApplyModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<StoreApplyModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getStoreApplyCashCode(final LixiActivity activity, final ApiCallBack _callBack, String id) {
        setupAPICallForPost(activity, _callBack);

        Call<ApiWrapperModel<StoreApplyModel>> call = apiService._getStoreApplyCashCode(id);

        call.enqueue(new Callback<ApiWrapperModel<StoreApplyModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<StoreApplyModel>> call,
                                   Response<ApiWrapperModel<StoreApplyModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<StoreApplyModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<StoreApplyModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getCompleteVoucherCode(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICallForPost(activity, _callBack);

        Call<ApiWrapperModel<CompletedVoucherModel>> call = apiService._getCompleteVoucherCode(id);

        call.enqueue(new Callback<ApiWrapperModel<CompletedVoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<CompletedVoucherModel>> call,
                                   Response<ApiWrapperModel<CompletedVoucherModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<CompletedVoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<CompletedVoucherModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getStoreApplyCashCodeList(final LixiActivity activity, final ApiCallBack _callBack, long id, int page, String key) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<StoreApplyModel>> call = apiService._getStoreApplyCashCodeList(id, page, record_per_page, key);

        call.enqueue(new Callback<ApiWrapperForListModel<StoreApplyModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<StoreApplyModel>> call,
                                   Response<ApiWrapperForListModel<StoreApplyModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<StoreApplyModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<StoreApplyModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getListCard(final LixiActivity activity, final ApiCallBack _callBack, String id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<DetailHistoryCardModel>> call = apiService._getListCard(id);

        call.enqueue(new Callback<ApiWrapperForListModel<DetailHistoryCardModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<DetailHistoryCardModel>> call,
                                   Response<ApiWrapperForListModel<DetailHistoryCardModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<DetailHistoryCardModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<DetailHistoryCardModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getStoreBoughtEVoucher(final LixiActivity activity, final ApiCallBack _callBack, int page, long store_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call = apiService._getStoreBoughEVoucher(page, record_per_page, store_id);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherBoughtModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getStoreNearby(final LixiActivity activity, final ApiCallBack _callBack, int page, String search) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<NearestStoreModel>> call = apiService._getStoreNearby(page, record_per_page, search);

        call.enqueue(new Callback<ApiWrapperForListModel<NearestStoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<NearestStoreModel>> call,
                                   Response<ApiWrapperForListModel<NearestStoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<NearestStoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<NearestStoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getStoreCode(final LixiActivity activity, final ApiCallBack _callBack, String id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<NearestStoreModel>> call = apiService._getStoreCode(id);

        call.enqueue(new Callback<ApiWrapperModel<NearestStoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<NearestStoreModel>> call,
                                   Response<ApiWrapperModel<NearestStoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<NearestStoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<NearestStoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getStoreMapList(final LixiActivity activity, final ApiCallBack _callBack, String merchant_id, String lat, String lon, String ciry_id, String district_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<StoreModel>> call = apiService._getMapStoreList(merchant_id, lat, lon, ciry_id, district_id);

        call.enqueue(new Callback<ApiWrapperForListModel<StoreModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<StoreModel>> call,
                                   Response<ApiWrapperForListModel<StoreModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<StoreModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<StoreModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }


    public static void getVoucherTopHot(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = apiService._getSpecialEvoucher(page, record_per_page);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }


    public static void getUserSurvey(final LixiActivity activity, final ApiCallBack _callBack, String survey_id) {
        setupAPICall(activity, _callBack);
        Call<ApiWrapperModel<SurveyModel>> call = apiService._getUserSurvey(survey_id);
        call.enqueue(new Callback<ApiWrapperModel<SurveyModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<SurveyModel>> call, Response<ApiWrapperModel<SurveyModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<SurveyModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<SurveyModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void userAnswerSurvey(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICall(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<SurveyAnswerResultModel>> call = apiService._userAnswerSurvey(requestBody);
        call.enqueue(new Callback<ApiWrapperModel<SurveyAnswerResultModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<SurveyAnswerResultModel>> call, Response<ApiWrapperModel<SurveyAnswerResultModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<SurveyAnswerResultModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<SurveyAnswerResultModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void userDeclineSurvey(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<ApiWrapperModel<Object>> call = apiService._userDeclineSurvey(requestBody);
        call.enqueue(new Callback<ApiWrapperModel<Object>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<Object>> call, Response<ApiWrapperModel<Object>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<Object> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<Object>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }
    //endregion


    //region handle Api error
    private static void handleUnexpectedCallError(LixiActivity activity, Throwable t) {
        t.printStackTrace();
        if (t instanceof IOException) {
            Helper.showErrorDialog(activity, "No internet connection!");

        }
        if (t instanceof SocketTimeoutException) {
            Helper.showErrorDialog(activity, "Request taking too long!");
        }
    }


    private static void handleUnexpectedCallErrorForPost(LixiActivity activity, Throwable t) {
//        Helper.hideProgressBar();
        activity.hideProgressBar();
        t.printStackTrace();
        if (t instanceof IOException) {
            Helper.showErrorDialog(activity, "No internet connection!");

        }
        if (t instanceof SocketTimeoutException) {
            Helper.showErrorDialog(activity, "Request taking too long!");
        }
    }


    private static void handleUnsuccessfullRespond(LixiActivity activity, ApiCallBack callBack, ResponseBody responseBody, String defaultMessage) {
        try {
            JSONObject errorJson = new JSONObject(responseBody.string());
            if (errorJson.getJSONObject("records").getString("message_code").equals("Invalid token")) {
                LoginHelper.Logout(activity);
            }
            callBack.handleCallback(false, errorJson.getJSONObject("records").getString("message_code"));
        } catch (JSONException e) {
            callBack.handleCallback(false, defaultMessage);
            e.printStackTrace();
        } catch (IOException e) {
            callBack.handleCallback(false, defaultMessage);
            e.printStackTrace();
        }
    }


    private static void checkInternet(LixiActivity activity) {
        if (!isOnline(activity)) {
            askToTurnOnInternet(activity);
        }
    }

    private static ManualDismissDialog internetDialog = null;

    private static void askToTurnOnInternet(final LixiActivity activity) {
        if (internetDialog != null) {
            try {
                internetDialog.dismiss();
                internetDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        internetDialog = new ManualDismissDialog(activity, false, true, false);
        internetDialog.setDismissAble(true);
        internetDialog.setIDidalogEventListener(new ManualDismissDialog.IDialogEvent() {
            @Override
            public void onOk() {
                activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        internetDialog.show();
        internetDialog.setMessage(LanguageBinding.getString(R.string.app_need_internet, activity));
    }

    private static boolean isOnline(LixiActivity activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm != null ? cm.getAllNetworkInfo() : new NetworkInfo[0];
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    //endregion

    //region Object to JSON conversion

    public static JSONObject objectToJSONObject(Object object) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;

    }

    public static JSONArray objectToJSONArray(Object object) {
        Object json = null;
        JSONArray jsonArray = null;
        try {
            json = new JSONTokener(object.toString()).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (json instanceof JSONArray) {
            jsonArray = (JSONArray) json;
        }
        return jsonArray;
    }

    //endregion

    // For Evoucher Summary
    public static void getEVoucherSummary(final LixiActivity activity, final ApiCallBack _callBack, long merchant_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<EVoucherSummary>> call = apiService._getEvoucherSummary(merchant_id);

        call.enqueue(new Callback<ApiWrapperModel<EVoucherSummary>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<EVoucherSummary>> call,
                                   Response<ApiWrapperModel<EVoucherSummary>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<EVoucherSummary> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<EVoucherSummary>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getEvoucherUnused(final LixiActivity activity, final ApiCallBack _callBack, int page, long record_per_page, long merchant_id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<EVoucherUnused>> call = apiService._getEvoucherUnused(page, record_per_page, merchant_id);
        call.enqueue(new Callback<ApiWrapperForListModel<EVoucherUnused>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<EVoucherUnused>> call,
                                   Response<ApiWrapperForListModel<EVoucherUnused>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<EVoucherUnused> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<EVoucherUnused>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getGroupCode(final LixiActivity activity, final ApiCallBack _callBack, GroupCodeApiBody groupCodeApiBody) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<GroupCode>> call = apiService._getGroupCode(groupCodeApiBody);
        call.enqueue(new Callback<ApiWrapperModel<GroupCode>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<GroupCode>> call, Response<ApiWrapperModel<GroupCode>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<GroupCode> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<GroupCode>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getActiveCode(final LixiActivity activity, final ApiCallBack _callBack, long group_id, ActiveCodeApiBody activeCodeApiBody) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<GroupCode>> call = apiService._getGroupCode(group_id, activeCodeApiBody);
        call.enqueue(new Callback<ApiWrapperModel<GroupCode>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<GroupCode>> call, Response<ApiWrapperModel<GroupCode>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<GroupCode> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<GroupCode>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void postActiveEvoucher(final LixiActivity activity, final ApiCallBack _callBack, JsonObject jsonObject) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<MarkUsedResponse>> call = apiService._postActiveEvoucher(jsonObject);
        call.enqueue(new Callback<ApiWrapperModel<MarkUsedResponse>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<MarkUsedResponse>> call, Response<ApiWrapperModel<MarkUsedResponse>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<MarkUsedResponse> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<MarkUsedResponse>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    // For Flash sale
    public static void getListFlashSaleTime(final LixiActivity activity, final ApiCallBack _callBack, int page, long record_per_page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<FlashSaleTime>> call = apiService._getListFlashSaleTime(page, record_per_page);
        call.enqueue(new Callback<ApiWrapperForListModel<FlashSaleTime>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<FlashSaleTime>> call, Response<ApiWrapperForListModel<FlashSaleTime>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<FlashSaleTime> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<FlashSaleTime>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getListFlashSaleDetail(final LixiActivity activity, final ApiCallBack _callBack, int page, long record_per_page, int id, String keyword) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<FlashSaleNow.ProductsBean>> call = apiService._getListFlashSaleDetail(id, page, record_per_page, keyword);
        call.enqueue(new Callback<ApiWrapperForListModel<FlashSaleNow.ProductsBean>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<FlashSaleNow.ProductsBean>> call, Response<ApiWrapperForListModel<FlashSaleNow.ProductsBean>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<FlashSaleNow.ProductsBean> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<FlashSaleNow.ProductsBean>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getListFlashSaleNow(final LixiActivity activity, final ApiCallBack _callBack, int page, final long record_per_page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<FlashSaleNow>> call = apiService._getListFlashSaleNow(page, record_per_page);
        call.enqueue(new Callback<ApiWrapperModel<FlashSaleNow>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<FlashSaleNow>> call, Response<ApiWrapperModel<FlashSaleNow>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<FlashSaleNow> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<FlashSaleNow>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void paymentCod(final LixiActivity activity, final ApiCallBack _callBack, PaymentCodBody paymentCodBody) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<PaymentCodModel>> call = apiService._paymentCod(paymentCodBody);
        call.enqueue(new Callback<ApiWrapperModel<PaymentCodModel>>() {
            @Override
            public void onResponse(@NonNull Call<ApiWrapperModel<PaymentCodModel>> call,
                                   @NonNull Response<ApiWrapperModel<PaymentCodModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<PaymentCodModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiWrapperModel<PaymentCodModel>> call, @NonNull Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getFlashSaleShop(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<LixiShopProductDetailModel>> call = apiService._getLixiShopProductDetail(id);

        call.enqueue(new Callback<ApiWrapperModel<LixiShopProductDetailModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<LixiShopProductDetailModel>> call, Response<ApiWrapperModel<LixiShopProductDetailModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<LixiShopProductDetailModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result);
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<LixiShopProductDetailModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void requestFlashSaleRemind(final LixiActivity activity, final ApiCallBack _callBack, FlashSaleRemindRequest flashSaleRemindRequest) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<FlashSaleRemindResponse>> call = apiService._requestFlashSaleRemind(flashSaleRemindRequest);
        call.enqueue(new Callback<ApiWrapperModel<FlashSaleRemindResponse>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<FlashSaleRemindResponse>> call, Response<ApiWrapperModel<FlashSaleRemindResponse>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<FlashSaleRemindResponse> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<FlashSaleRemindResponse>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void deleteFlashSaleRemind(final LixiActivity activity, final ApiCallBack _callBack, long id) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<FlashSaleRemindResponse>> call = apiService._deleteFlashSaleRemind(id);
        call.enqueue(new Callback<ApiWrapperModel<FlashSaleRemindResponse>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<FlashSaleRemindResponse>> call, Response<ApiWrapperModel<FlashSaleRemindResponse>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<FlashSaleRemindResponse> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<FlashSaleRemindResponse>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getListRating(final LixiActivity activity, final ApiCallBack _callBack, int product_id, int page, int record_per_page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<RatingModel>> call = apiService._getListRating(product_id, page, record_per_page);
        call.enqueue(new Callback<ApiWrapperForListModel<RatingModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<RatingModel>> call, Response<ApiWrapperForListModel<RatingModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<RatingModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<RatingModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getRatingOption(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperForListModel<RatingOption>> call = apiService._getRatingOption(page, record_per_page);
        call.enqueue(new Callback<ApiWrapperForListModel<RatingOption>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<RatingOption>> call, Response<ApiWrapperForListModel<RatingOption>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<RatingOption> result = response.body();

                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<RatingOption>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void postRating(final LixiActivity activity, final ApiCallBack _callBack, JsonObject jsonObject) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<RatingResponse>> call = apiService._postRating(jsonObject);
        call.enqueue(new Callback<ApiWrapperModel<RatingResponse>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<RatingResponse>> call, Response<ApiWrapperModel<RatingResponse>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<RatingResponse> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<RatingResponse>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void createNewCard(final LixiActivity activity, final ApiCallBack _callBack, JsonObject newCardResquest) {
        setupAPICall(activity, _callBack);
        Call<ApiWrapperModel<JsonObject>> call = apiService._createNewCard(newCardResquest);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<JsonObject> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        Helper.showErrorDialog(activity, result.getRecords().getAsString());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getUserTransaction(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);
        Call<ApiWrapperForListModel<UserTransaction>> call = apiService._getUserTransaction(page, record_per_page);
        call.enqueue(new Callback<ApiWrapperForListModel<UserTransaction>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<UserTransaction>> call, Response<ApiWrapperForListModel<UserTransaction>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<UserTransaction> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<UserTransaction>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void getDetailTransaction(final LixiActivity activity, final ApiCallBack _callBack, int id, String index) {
        setupAPICall(activity, _callBack);

        Call<ApiWrapperModel<DetailTransaction>> call = apiService._getDetailTransaction(id, index);
        call.enqueue(new Callback<ApiWrapperModel<DetailTransaction>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<DetailTransaction>> call, Response<ApiWrapperModel<DetailTransaction>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<DetailTransaction> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<DetailTransaction>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    // region Login

    public static void postCheckPhone(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postCheckPhone(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        _callBack.handleCallback(false, result.getRecords().getMessage_code());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void putUpdateUserInfo(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<JsonObject>> call = apiService._putUpdateUserInfo(params);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<JsonObject> result = response.body();
                    if (UserModel.getInstance() != null) {
                        UserModel.getInstance().setNeed_update(false);
                    }
                    _callBack.handleCallback(true, result.get_meta().getStatus());
                } else {
                    if (response.code() == 500) {
                        _callBack.handleCallback(false, getErrorMessage(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void postGetUserToken(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postGetUserToken(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        UserLoginModel userModel = result.getRecords();
                        String accessToken = userModel.getAccess_token();
                        if (accessToken != null && !accessToken.equals("")) {
                            (new AppPreferenceHelper(activity)).setUserToken(accessToken);
                            Analytics.trackLogin(result.getRecords().getUser_id());
                            initAPICall(activity, "JWT " + accessToken, _callBack);
                            DeliDataLoader.initAPICall(activity, "JWT " + accessToken);
                        } else {
                            _callBack.handleCallback(false, "Không đủ thông tin");
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    _callBack.handleCallback(false, response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void postLoginPhone(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postLoginPhone(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        UserLoginModel userModel = result.getRecords();
                        String accessToken = userModel.getAccess_token();
                        if (accessToken != null && !accessToken.equals("")) {
                            (new AppPreferenceHelper(activity)).setUserToken(accessToken);
                            Analytics.trackLogin(result.getRecords().getUser_id());
                            initAPICall(activity, "JWT " + accessToken, _callBack);
                            DeliDataLoader.initAPICall(activity, "JWT " + accessToken);
                        } else {
                            _callBack.handleCallback(false, "Không đủ thông tin");
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    if (response.code() == 500) {
                        _callBack.handleCallback(false, getErrorMessage(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
                _callBack.handleCallback(false, "fail");
            }
        });
    }

    public static void putLoginPhone(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), params.toString());

        Call<ApiWrapperModel<JsonObject>> call = apiService._putLoginPhone(requestBody);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    final ApiWrapperModel<JsonObject> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        getUserProfile(activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                if (isSuccess) {
                                    _callBack.handleCallback(true, result.getRecords().get("status").getAsBoolean());
                                } else {
                                    ApiWrapperModel<UserModel> result = (ApiWrapperModel<UserModel>) object;
                                    _callBack.handleCallback(false, result.getRecords().getMessage_code());
                                }
                            }
                        });

                    } else {
                        _callBack.handleCallback(false, response.body().get_meta().getStatus());
                    }
                } else {
                    if (response.code() == 500) {
                        _callBack.handleCallback(false, getErrorMessage(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
                _callBack.handleCallback(false, "fail");
            }
        });
    }

    public static void postLoginFacebook(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postLoginFacebook(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    String temp = result.get_meta().getStatus() + "";
                    if (temp.equals(ConstantValue.SUCCESS)) {
                        UserLoginModel userModel = result.getRecords();
                        String accessToken = userModel.getAccess_token();
                        if (accessToken != null && !accessToken.equals("")) {
                            (new AppPreferenceHelper(activity)).setUserToken(accessToken);
                            Analytics.trackLogin(result.getRecords().getUser_id());
                            initAPICall(activity, "JWT " + accessToken, _callBack);
                            DeliDataLoader.initAPICall(activity, accessToken);
                        } else {
                            _callBack.handleCallback(false, "Không đủ thông tin");
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    _callBack.handleCallback(false, response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
                _callBack.handleCallback(false, "fail");
            }
        });
    }

    public static void postCheckFacebook(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postCheckFacebook(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        UserLoginModel userModel = result.getRecords();
                        String accessToken = userModel.getAccess_token();
                        if (accessToken != null && !accessToken.equals("")) {
                            (new AppPreferenceHelper(activity)).setUserToken(accessToken);
                            Analytics.trackLogin(result.getRecords().getUser_id());
                            initAPICall(activity, "JWT " + accessToken, _callBack);
                            DeliDataLoader.initAPICall(activity, "JWT " + accessToken);
                        } else {
                            _callBack.handleCallback(false, "Không đủ thông tin");
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    _callBack.handleCallback(false, response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void postForgotPass(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<UserLoginModel>> call = apiService._postForgotPass(params);
        call.enqueue(new Callback<ApiWrapperModel<UserLoginModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<UserLoginModel>> call, Response<ApiWrapperModel<UserLoginModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<UserLoginModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        UserLoginModel userModel = result.getRecords();
                        String accessToken = userModel.getAccess_token();
                        if (accessToken != null && !accessToken.equals("")) {
                            (new AppPreferenceHelper(activity)).setUserToken(accessToken);
                            Analytics.trackLogin(result.getRecords().getUser_id());
                            initAPICall(activity, "JWT " + accessToken, _callBack);
                            DeliDataLoader.initAPICall(activity, "JWT " + accessToken);
                        } else {
                            _callBack.handleCallback(false, "Không đủ thông tin");
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    _callBack.handleCallback(false, response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<UserLoginModel>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void putResetPassword(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<JsonObject>> call = apiService._putResetPassword(params);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperModel<JsonObject> result = response.body();
                    _callBack.handleCallback(result.getRecords().get("status").getAsBoolean(), null);
                } else {
                    _callBack.handleCallback(false, response.body().getRecords().get("message_code").getAsBoolean());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                handleUnexpectedCallError(activity, t);
            }
        });
    }
    // endregion

    public static void getHistoryIntro(final LixiActivity activity, final ApiCallBack _callBack, int page) {
        setupAPICall(activity, _callBack);
        Call<ApiWrapperForListModel<HistoryIntroModel>> call = apiService._getHistoryIntro(page, record_per_page);
        call.enqueue(new Callback<ApiWrapperForListModel<HistoryIntroModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<HistoryIntroModel>> call, Response<ApiWrapperForListModel<HistoryIntroModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<HistoryIntroModel> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        _callBack.handleCallback(true, result.getRecords());
                    } else {
                        _callBack.handleCallback(false, result.getMessage_code());
                    }
                } else {
                    if (response.code() == 500) {
                        _callBack.handleCallback(false, getErrorMessage(response.errorBody()));
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<HistoryIntroModel>> call, Throwable t) {
                handleUnexpectedCallError(activity, t);
            }
        });
    }

    public static void putUpdateProfile(final LixiActivity activity, final ApiCallBack _callBack, JsonObject param) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<JsonObject>> call = apiService._putUpdateProfile(param);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<JsonObject> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        if (result.getRecords().get("status").getAsBoolean()) {
                            UserFragment.UPDATE_USER_INFO = true;
                            getUserProfile(activity, _callBack);
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    _callBack.handleCallback(false, response.message());
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
                _callBack.handleCallback(false, null);
            }
        });
    }

    public static void putUpdatePass(final LixiActivity activity, final ApiCallBack _callBack, JsonObject param) {
        setupAPICallForPost(activity, _callBack);
        Call<ApiWrapperModel<JsonObject>> call = apiService._putUpdatePass(param);
        call.enqueue(new Callback<ApiWrapperModel<JsonObject>>() {
            @Override
            public void onResponse(Call<ApiWrapperModel<JsonObject>> call, Response<ApiWrapperModel<JsonObject>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    ApiWrapperModel<JsonObject> result = response.body();
                    if (result.get_meta().getStatus().equals(ConstantValue.SUCCESS)) {
                        if (result.getRecords().get("status").getAsBoolean()) {
                            UserFragment.UPDATE_USER_INFO = true;
                            getUserProfile(activity, _callBack);
                        }
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                } else {
                    if (response.code() == 500) {
                        _callBack.handleCallback(false, getErrorMessage(response.errorBody()));
                    } else {
                        _callBack.handleCallback(false, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperModel<JsonObject>> call, Throwable t) {
                _callBack.handleCallback(false, null);
            }
        });
    }
}
