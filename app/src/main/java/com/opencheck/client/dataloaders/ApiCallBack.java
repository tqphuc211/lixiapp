package com.opencheck.client.dataloaders;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public interface ApiCallBack  {
    void handleCallback(boolean isSuccess, Object object);
}
