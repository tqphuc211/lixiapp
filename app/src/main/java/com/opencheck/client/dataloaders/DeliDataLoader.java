package com.opencheck.client.dataloaders;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.BankModel;
import com.opencheck.client.home.delivery.model.ClaimReasonModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliApiWrapperForListModel;
import com.opencheck.client.home.delivery.model.DeliApiWrapperModel;
import com.opencheck.client.home.delivery.model.DeliBannerModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.DeliOrderResponseModel;
import com.opencheck.client.home.delivery.model.DetailTopicModel;
import com.opencheck.client.home.delivery.model.FeedbackItemModel;
import com.opencheck.client.home.delivery.model.MenuModel;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.ResponsePromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.model.StoreTopicModel;
import com.opencheck.client.home.delivery.model.SubCategoryModel;
import com.opencheck.client.home.delivery.model.UserFeedbackModel;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeliDataLoader {
    public static String token = "";
    private static OkHttpClient authenClient = null;
    private static ApiCall apiService = null;
    public static final int record_per_page = 20;
    private static String api_error = "API CALL ERROR!";
    private static final int CONNECTION_TIMEOUT = 40;
    private static final int WRITE_TIMEOUT = 40;
    private static final int READ_TIMEOUT = 40;
    public static String shipping_receive_address = "Chung cư H2, 96 Hoàng Diệu, phường 8, quận 4, Hồ Chí Minh";
    public static double shipping_receive_lat = 10.7622597;
    public static double shipping_receive_lng = 106.7019736;
    public static String lang = "vi";
    private static String province = "HCM";

    //region config api call

    private static void createAuthenInterceptors(final String token) {
        authenClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder = originalRequest.newBuilder()
                        .addHeader("user_authorization", token)
                        .addHeader("lang", lang)
                        .addHeader("province", province)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("platform", "android")
                        .addHeader("version", BuildConfig.VERSION_NAME);
                originalRequest = builder.build();
                Helper.showLog("lang: " + originalRequest.header("lang"));
                Helper.showLog("province: " + originalRequest.header("province"));
                Helper.showLog("platform: " + originalRequest.header("platform"));
                Helper.showLog(originalRequest.toString());
                if (originalRequest.body() != null) {
                    final Buffer buffer = new Buffer();
                    originalRequest.body().writeTo(buffer);
                    Helper.showLog("Body-Deli: " + buffer.readUtf8());
                    Helper.showLog("Header-Deli: " + originalRequest.headers().toString());

                }
                return chain.proceed(originalRequest);

            }
        }).connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    public static void checkChangeProvinceHeader(LixiActivity activity) {
        if (apiService == null) {
            initAPICall(activity, new AppPreferenceHelper(activity).getUserToken());
            return;
        }
        String pv = "";
        try {
            AppPreferenceHelper appPreferenceHelper = AppPreferenceHelper.getInstance();
            DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
            if (deliAddressModel != null) {
                pv = deliAddressModel.getProvince();
            } else
                pv = DeliAddressModel.PROVINCE_ALL;
        } catch (Exception ex) {
            pv = DeliAddressModel.PROVINCE_ALL;
        }
        try {
            if (province != null && !province.equals(pv))
                DeliDataLoader.initAPICall(activity, new AppPreferenceHelper(activity).getUserToken());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //MUST call when user logged in
    public static void initAPICall(LixiActivity activity, String _token) {
        token = _token;
        lang = new AppPreferenceHelper(activity).getLanguageSource();
        if (lang.equals("")) {
            lang = ConstantValue.LANG_CONFIG_VN;
        }
//        province = new AppPreferenceHelper(activity).getProvinceSource();
        try {
            AppPreferenceHelper appPreferenceHelper = AppPreferenceHelper.getInstance();
            DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
            if (deliAddressModel != null) {
                province = deliAddressModel.getProvince();
            } else
                province = DeliAddressModel.PROVINCE_ALL;
        } catch (Exception ex) {
            province = DeliAddressModel.PROVINCE_ALL;
        }
        createAuthenInterceptors(token);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.DELIVERY)
                .client(authenClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
        apiService = retrofitBuilder.build().create(ApiCall.class);
        if (!token.equals("JWT ")) {
            DataLoader.getUserProfile(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    //get User profile
                }
            });
        }
    }

    private static void setupAPICall(LixiActivity activity, ApiCallBack _callback) {
        checkInternet(activity);
        if (token == null || token.length() == 0)
            token = new AppPreferenceHelper(activity).getUserToken();

        if (apiService == null)
            initAPICall(activity, token);
    }

    private static void setupAPICallForPost(LixiActivity activity, ApiCallBack callBack) {
        setupAPICall(activity, callBack);
//        Helper.showProgressBar(activity);
        activity.showProgressBar();
    }

    //endregion

    //region GET
    public static void getArticle(final LixiActivity activity, final ApiCallBack _callBack, int page, String province) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<ArticleModel>> call = apiService._getArticle(page, record_per_page, province);
        call.enqueue(new Callback<DeliApiWrapperForListModel<ArticleModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<ArticleModel>> call, @NonNull Response<DeliApiWrapperForListModel<ArticleModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<ArticleModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        if (result != null) {
                            Helper.showLog(result.getError().getMessage());
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<ArticleModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getBanner(final LixiActivity activity, final ApiCallBack _callBack, String province) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<DeliBannerModel>> call = apiService._getBanner(1, record_per_page, province);
        call.enqueue(new Callback<DeliApiWrapperForListModel<DeliBannerModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<DeliBannerModel>> call, @NonNull Response<DeliApiWrapperForListModel<DeliBannerModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<DeliBannerModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        if (result != null) {
                            Helper.showLog(result.getError().getMessage());
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<DeliBannerModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getListTopic(final LixiActivity activity, final ApiCallBack _callBack, long topic_id, String address, double lat, double lng) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperForListModel<StoreTopicModel>> call = apiService._getListTopic(topic_id, address, lat, lng, 1, record_per_page);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreTopicModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<StoreTopicModel>> call, Response<DeliApiWrapperForListModel<StoreTopicModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreTopicModel> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<StoreTopicModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getDetailTopic(final LixiActivity activity, final ApiCallBack _callBack, long topic_id) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<DetailTopicModel>> call = apiService._getDetailTopic(topic_id);
        call.enqueue(new Callback<DeliApiWrapperModel<DetailTopicModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<DetailTopicModel>> call, Response<DeliApiWrapperModel<DetailTopicModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<DetailTopicModel> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<DetailTopicModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getStoreOfArticle(final LixiActivity activity, final ApiCallBack _callBack, int page, long article_id, String shipping_receive_address,
                                         double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getStoreOfArticle(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng, article_id);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        if (result != null) {
                            Helper.showLog(result.getError().getMessage());
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getDetailArticle(final LixiActivity activity, final ApiCallBack _callBack, int page, long article_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperModel<ArticleModel>> call = apiService._getDetailArticle(page, record_per_page, article_id);
        call.enqueue(new Callback<DeliApiWrapperModel<ArticleModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<ArticleModel>> call, @NonNull Response<DeliApiWrapperModel<ArticleModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<ArticleModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<ArticleModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getSubCategory(final LixiActivity activity, final ApiCallBack _callBack, int page, long article_id, long subcategory_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<SubCategoryModel>> call = apiService._getSubCategory(page, record_per_page, article_id, subcategory_id);
        call.enqueue(new Callback<DeliApiWrapperForListModel<SubCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<SubCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<SubCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<SubCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<SubCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getBestPrice(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                    double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getBestPrice(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getListHot(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                  double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getListHot(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getStoreFastest(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                       double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getStoreFastest(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> getStoreListByKey(final LixiActivity activity, final ApiCallBack _callBack, String key, Long tag_id, int page, String shipping_receive_address,
                                                                                           double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getStoreList(key, tag_id, page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                if (call.isCanceled())
                    return;
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });

        return call;
    }

    public static void getStoreRecommend(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                         double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getStoreRecommend(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getProductMerchantHot(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                             double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<ProductMerchantHot>> call = apiService._getProductMerchantHot(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<ProductMerchantHot>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<ProductMerchantHot>> call, @NonNull Response<DeliApiWrapperForListModel<ProductMerchantHot>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<ProductMerchantHot> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<ProductMerchantHot>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }


    public static void getStoreDetail(final LixiActivity activity, final ApiCallBack _callBack, long store_id, String shipping_receive_address,
                                      Double shipping_receive_lat, Double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperModel<StoreOfCategoryModel>> call;
        if (UserModel.getInstance() == null) {
            call = apiService._getStoreDetail(shipping_receive_address, shipping_receive_lat, shipping_receive_lng, store_id);
        } else {
            call = apiService._getStoreDetail(shipping_receive_address, shipping_receive_lat, shipping_receive_lng, store_id, Long.parseLong(UserModel.getInstance().getId()));
        }
        call.enqueue(new Callback<DeliApiWrapperModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<StoreOfCategoryModel>> call, Response<DeliApiWrapperModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<StoreOfCategoryModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getProductDetail(final LixiActivity activity, final ApiCallBack _callBack, long product_id) {
        setupAPICallForPost(activity, _callBack);

        Call<DeliApiWrapperModel<ProductModel>> call = apiService._getProductDetail(product_id);
        call.enqueue(new Callback<DeliApiWrapperModel<ProductModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<ProductModel>> call, Response<DeliApiWrapperModel<ProductModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<ProductModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<ProductModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getMenu(final LixiActivity activity, final ApiCallBack _callBack, long store_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<MenuModel>> call = apiService._getMenu(store_id);
        call.enqueue(new Callback<DeliApiWrapperForListModel<MenuModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<MenuModel>> call, Response<DeliApiWrapperForListModel<MenuModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<MenuModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<MenuModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserOrderDetail(final LixiActivity activity, final ApiCallBack _callBack, String uuid, String id) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<DeliOrderModel>> call = apiService._getUserOrderDetail(uuid, id);
        call.enqueue(new Callback<DeliApiWrapperModel<DeliOrderModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<DeliOrderModel>> call, Response<DeliApiWrapperModel<DeliOrderModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<DeliOrderModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<DeliOrderModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserOrderHistory(final LixiActivity activity, final ApiCallBack _callBack, int page, int record, String state, Long create_date_from, Long create_date_to) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperForListModel<DeliOrderModel>> call = apiService._getUserOrderHistory(page, record, state, create_date_from, create_date_to);
        call.enqueue(new Callback<DeliApiWrapperForListModel<DeliOrderModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<DeliOrderModel>> call, Response<DeliApiWrapperForListModel<DeliOrderModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<DeliOrderModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<DeliOrderModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getStoreTagList(final LixiActivity activity, final ApiCallBack _callBack, int page, String category_code, boolean show_home_only, String province) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<ArticleModel>> call = apiService._getStoreTagList(page, record_per_page, category_code, show_home_only, province);
        call.enqueue(new Callback<DeliApiWrapperForListModel<ArticleModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<ArticleModel>> call, Response<DeliApiWrapperForListModel<ArticleModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<ArticleModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<ArticleModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPromotionList(final LixiActivity activity, final ApiCallBack _callBack, String search, long store_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<PromotionModel>> call = apiService._getPromotionList(search, store_id, UserModel.getInstance() == null ? 0 : Long.parseLong(UserModel.getInstance().getId()));
        call.enqueue(new Callback<DeliApiWrapperForListModel<PromotionModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<PromotionModel>> call, Response<DeliApiWrapperForListModel<PromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<PromotionModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<PromotionModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPromotionListUserGroup(final LixiActivity activity, final ApiCallBack _callBack, String search, String shipping_receive_address,
                                                 double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<PromotionModel>> call = apiService._getPromotionListUserGroup(search, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<PromotionModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<PromotionModel>> call, Response<DeliApiWrapperForListModel<PromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<PromotionModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<PromotionModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPromotionByCode(final LixiActivity activity, final ApiCallBack _callBack, String code) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperModel<PromotionModel>> call = apiService._getPromotionByCode(code);
        call.enqueue(new Callback<DeliApiWrapperModel<PromotionModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<PromotionModel>> call, Response<DeliApiWrapperModel<PromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<PromotionModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<PromotionModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPromotionByValue(final LixiActivity activity, final ApiCallBack _callBack, String user_order_uuid, String list_code) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperModel<ResponsePromotionModel>> call = apiService._getPromotionByValue(user_order_uuid, list_code);
        call.enqueue(new Callback<DeliApiWrapperModel<ResponsePromotionModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<ResponsePromotionModel>> call, Response<DeliApiWrapperModel<ResponsePromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<ResponsePromotionModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<ResponsePromotionModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPromotionDetail(final LixiActivity activity, final ApiCallBack _callBack, long promotion_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperModel<PromotionModel>> call = apiService._getPromotionDetail(promotion_id);
        call.enqueue(new Callback<DeliApiWrapperModel<PromotionModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<PromotionModel>> call, Response<DeliApiWrapperModel<PromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<PromotionModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<PromotionModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getPaymentMethod(final LixiActivity activity, final ApiCallBack _callBack, long store_id, String uuid) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<PaymentModel>> call = apiService._getPaymentMethod(store_id, uuid);
        call.enqueue(new Callback<DeliApiWrapperForListModel<PaymentModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<PaymentModel>> call, Response<DeliApiWrapperForListModel<PaymentModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<PaymentModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<PaymentModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getBankCode(final LixiActivity activity, final ApiCallBack _callBack, String payment_method_code) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<BankModel>> call = apiService._getBankCode(payment_method_code);
        call.enqueue(new Callback<DeliApiWrapperForListModel<BankModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<BankModel>> call, Response<DeliApiWrapperForListModel<BankModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<BankModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<BankModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getFeedbackItem(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<FeedbackItemModel>> call = apiService._getFeedbackItem();
        call.enqueue(new Callback<DeliApiWrapperForListModel<FeedbackItemModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<FeedbackItemModel>> call, Response<DeliApiWrapperForListModel<FeedbackItemModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<FeedbackItemModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<FeedbackItemModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserConfig(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<DeliConfigModel>> call = apiService._getUserDeliConfig();
        call.enqueue(new Callback<DeliApiWrapperForListModel<DeliConfigModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<DeliConfigModel>> call, @NonNull Response<DeliApiWrapperForListModel<DeliConfigModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<DeliConfigModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                        DeliConfigModel.SaveConfig(activity, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<DeliConfigModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserConfig(final LixiActivity activity) {
        setupAPICall(activity, null);
        Call<DeliApiWrapperForListModel<DeliConfigModel>> call = apiService._getUserDeliConfig();
        call.enqueue(new Callback<DeliApiWrapperForListModel<DeliConfigModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<DeliConfigModel>> call, Response<DeliApiWrapperForListModel<DeliConfigModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<DeliConfigModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        DeliConfigModel.SaveConfig(activity, result.getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<DeliConfigModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserDistance(final LixiActivity activity, final ApiCallBack _callBack, long store_id, double lat, double lng) {
        Call<DeliApiWrapperModel<Double>> call = apiService._getUserDistance(store_id, lat, lng);
        call.enqueue(new Callback<DeliApiWrapperModel<Double>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Double>> call, Response<DeliApiWrapperModel<Double>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Double> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Double>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getReasons(final LixiActivity activity, final ApiCallBack _callBack) {
        Call<DeliApiWrapperForListModel<ClaimReasonModel>> call = apiService._getReasons();
        call.enqueue(new Callback<DeliApiWrapperForListModel<ClaimReasonModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<ClaimReasonModel>> call, @NonNull Response<DeliApiWrapperForListModel<ClaimReasonModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<ClaimReasonModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<ClaimReasonModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getCalculatePrice(final LixiActivity activity, final ApiCallBack _callBack, String uuid, String payment_method_code, String promotion_code, boolean is_use_lixi_money) {
        Call<DeliApiWrapperModel<ResponsePromotionModel>> call = apiService._getCalculatePrice(uuid, payment_method_code, promotion_code, is_use_lixi_money);
        call.enqueue(new Callback<DeliApiWrapperModel<ResponsePromotionModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<ResponsePromotionModel>> call, @NonNull Response<DeliApiWrapperModel<ResponsePromotionModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<ResponsePromotionModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<ResponsePromotionModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getFavoriteStore(final LixiActivity activity, final ApiCallBack _callBack, int page, String shipping_receive_address,
                                        double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getFavoriteStore(page, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        if (result != null) {
                            Helper.showLog(result.getError().getMessage());
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getTotalFavoriteStore(final LixiActivity activity, final ApiCallBack _callBack, String shipping_receive_address,
                                             double shipping_receive_lat, double shipping_receive_lng) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call = apiService._getFavoriteStore(1, record_per_page, shipping_receive_address, shipping_receive_lat, shipping_receive_lng);
        call.enqueue(new Callback<DeliApiWrapperForListModel<StoreOfCategoryModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Response<DeliApiWrapperForListModel<StoreOfCategoryModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<StoreOfCategoryModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.get_meta().getTotal());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void addFavoriteStore(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICall(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<Object>> call = apiService._addFavoriteStore(requestBody);
        call.enqueue(new Callback<DeliApiWrapperModel<Object>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Object>> call, Response<DeliApiWrapperModel<Object>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Object> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Object>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void deleteFavoriteStore(final LixiActivity activity, final ApiCallBack _callBack, long store_id) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<Integer>> call = apiService._deleteFavoriteStore(store_id);
        call.enqueue(new Callback<DeliApiWrapperModel<Integer>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Integer>> call, Response<DeliApiWrapperModel<Integer>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Integer> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Integer>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserFeedback(final LixiActivity activity, final ApiCallBack _callBack, int page, long store_id) {
        setupAPICall(activity, _callBack);

        Call<DeliApiWrapperForListModel<UserFeedbackModel>> call = apiService._getUserFeedback(page, record_per_page, store_id);

        call.enqueue(new Callback<DeliApiWrapperForListModel<UserFeedbackModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<UserFeedbackModel>> call, Response<DeliApiWrapperForListModel<UserFeedbackModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<UserFeedbackModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showLog(api_error);
                    }
                } else {
                    if (response.errorBody() != null) {
                        handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<UserFeedbackModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getCalculatePrice(final LixiActivity activity, final ApiCallBack _callBack, String uuid, String payment_method_code, String promotion_code) {
        getCalculatePrice(activity, _callBack, uuid, payment_method_code, promotion_code, false);
    }

    //endregion

    //region POST
    public static void createOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), strParams);
        Call<DeliApiWrapperModel<String>> call = apiService._createOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<String>> call, @NonNull Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<String>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void addPromotionToOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<ResponsePromotionModel>> call = apiService._addPromotionToOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<ResponsePromotionModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<ResponsePromotionModel>> call, @NonNull Response<DeliApiWrapperModel<ResponsePromotionModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<ResponsePromotionModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<ResponsePromotionModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void deletePromotionOfOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<String>> call = apiService._deletePromotionOfOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<String>> call, Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<String>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void updateOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<String>> call = apiService._updateOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<String>> call, @NonNull Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<String>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });


    }

    public static void userUpdateOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);
        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<String>> call = apiService._userUpdateOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<String>> call, Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<String>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void submitOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<DeliOrderResponseModel>> call = apiService._submitOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<DeliOrderResponseModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, @NonNull Response<DeliApiWrapperModel<DeliOrderResponseModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<DeliOrderResponseModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void payOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<DeliOrderResponseModel>> call = apiService._payOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<DeliOrderResponseModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, Response<DeliApiWrapperModel<DeliOrderResponseModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<DeliOrderResponseModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void repayOrder(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<DeliOrderResponseModel>> call = apiService._repayOrder(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<DeliOrderResponseModel>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, @NonNull Response<DeliApiWrapperModel<DeliOrderResponseModel>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<DeliOrderResponseModel> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result != null ? result.getError().getMessage() : null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<DeliOrderResponseModel>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void deleteOrder(final LixiActivity activity, final ApiCallBack _callBack, String user_order_uuid) {
        setupAPICallForPost(activity, _callBack);

        Call<DeliApiWrapperModel<String>> call = apiService._deleteOrder(user_order_uuid);
        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<String>> call, Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<String>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void sendFeedback(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<String>> call = apiService._createFeedback(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<String>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<String>> call, Response<DeliApiWrapperModel<String>> response) {
//                Helper.hideProgressBar();
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<String> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<String>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void getUserLocation(final LixiActivity activity, final ApiCallBack _callBack) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperForListModel<DeliAddressModel>> call = apiService._getUserLocation();
        call.enqueue(new Callback<DeliApiWrapperForListModel<DeliAddressModel>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperForListModel<DeliAddressModel>> call, Response<DeliApiWrapperForListModel<DeliAddressModel>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperForListModel<DeliAddressModel> result = response.body();
                    boolean status = result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, null);
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperForListModel<DeliAddressModel>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void createUserLocation(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<Integer>> call = apiService._createLocation(params);
        call.enqueue(new Callback<DeliApiWrapperModel<Integer>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Integer>> call, Response<DeliApiWrapperModel<Integer>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Integer> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Integer>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void editLocation(final LixiActivity activity, final ApiCallBack _callBack, JsonObject params) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<Integer>> call = apiService._editLocation(params);
        call.enqueue(new Callback<DeliApiWrapperModel<Integer>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Integer>> call, Response<DeliApiWrapperModel<Integer>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Integer> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Integer>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void deleteLocation(final LixiActivity activity, final ApiCallBack _callBack, int id) {
        setupAPICall(activity, _callBack);
        Call<DeliApiWrapperModel<Integer>> call = apiService._deleteLocation(id);
        call.enqueue(new Callback<DeliApiWrapperModel<Integer>>() {
            @Override
            public void onResponse(Call<DeliApiWrapperModel<Integer>> call, Response<DeliApiWrapperModel<Integer>> response) {
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Integer> result = response.body();
                    if (result.get_meta().isSuccess()) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        _callBack.handleCallback(false, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliApiWrapperModel<Integer>> call, Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }

    public static void sendReasonSupport(final LixiActivity activity, final ApiCallBack _callBack, JSONObject params) {
        setupAPICallForPost(activity, _callBack);

        String strParams = params.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), strParams);
        Call<DeliApiWrapperModel<Integer>> call = apiService._postReason(requestBody);

        call.enqueue(new Callback<DeliApiWrapperModel<Integer>>() {
            @Override
            public void onResponse(@NonNull Call<DeliApiWrapperModel<Integer>> call, @NonNull Response<DeliApiWrapperModel<Integer>> response) {
                activity.hideProgressBar();
                if (response.isSuccessful()) {
                    DeliApiWrapperModel<Integer> result = response.body();
                    boolean status = result != null && result.get_meta().isSuccess();
                    if (status) {
                        _callBack.handleCallback(true, result.getData());
                    } else {
                        Helper.showErrorDialog(activity, result.getError().getMessage());
                    }
                } else {
                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliApiWrapperModel<Integer>> call, @NonNull Throwable t) {
                handleUnexpectedCallErrorForPost(activity, t);
            }
        });
    }
    //endregion

    //region handle Api error
    private static void handleUnexpectedCallError(LixiActivity activity, Throwable t) {
        t.printStackTrace();
        if (t instanceof IOException) {
            Helper.showErrorDialog(activity, "No internet connection!");

        }
        if (t instanceof SocketTimeoutException) {
            Helper.showErrorDialog(activity, "Request taking too long!");
        }
    }


    private static void handleUnexpectedCallErrorForPost(LixiActivity activity, Throwable t) {
//        Helper.hideProgressBar();
        activity.hideProgressBar();
        t.printStackTrace();
        if (t instanceof IOException) {
            Helper.showErrorDialog(activity, "No internet connection!");

        }
        if (t instanceof SocketTimeoutException) {
            Helper.showErrorDialog(activity, "Request taking too long!");
        }
    }


    private static void handleUnsuccessfullRespond(LixiActivity activity, ApiCallBack callBack, ResponseBody responseBody, String defaultMessage) {
        try {
            JSONObject errorJson = new JSONObject(responseBody.string());
            if (errorJson.getJSONObject("error").getString("message").equals("Invalid token")) {
                LoginHelper.Logout(activity);
            }
            callBack.handleCallback(false, errorJson.getJSONObject("error").getString("message"));
        } catch (JSONException | IOException e) {
            callBack.handleCallback(false, defaultMessage);
            e.printStackTrace();
        }
    }


    private static void checkInternet(LixiActivity activity) {
        if (!isOnline(activity)) {
            askToTurnOnInternet(activity);
        }
    }

    private static ManualDismissDialog internetDialog = null;

    private static void askToTurnOnInternet(final LixiActivity activity) {
        if (internetDialog != null) {
            try {
                internetDialog.dismiss();
                internetDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        internetDialog = new ManualDismissDialog(activity, false, true, false);
        internetDialog.setDismissAble(true);
        internetDialog.setIDidalogEventListener(new ManualDismissDialog.IDialogEvent() {
            @Override
            public void onOk() {
                activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        internetDialog.show();
        internetDialog.setMessage(LanguageBinding.getString(R.string.app_need_internet, activity));
    }

    public static boolean isOnline(Context activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm != null ? cm.getAllNetworkInfo() : new NetworkInfo[0];
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    //endregion

}
