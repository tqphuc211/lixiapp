package com.opencheck.client.dataloaders;

import com.opencheck.client.BuildConfig;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class ApiEntity {
    public static final int STATUS_OK = 200;

    //real
//    final public static String BASE_URL = "https://api.opencheck.co";
//    public static final String SOCKET = "http://tracking.lixiapp.com/";
//    public static final String DELIVERY = "https://deliapi.lixiapp.com/api/";
//    public static final String DELI_SOCKET = "https://deliapi.lixiapp.com";

    //dev
//    public static final String BASE_URL = "http://45.118.151.83:8000";
//    public static final String SOCKET = "http://tracking-dev.lixiapp.com";
//    public static final String DELIVERY = "https://deliapi-stg.lixiapp.com/api/";
//    public static final String DELI_SOCKET = "https://deliapi-stg.lixiapp.com";

    public static final String USER_AUTHEN = "user/register";
    public static final String USER_PROFILE = "profile";
    public static final String UPDATE_AVATAR = "profile/avatar";

    public static final String USER_CONFIG = "mobile/config";
    public static final String GET_MERCHANT_HOT_BRAND = "merchant/hot_brand";
    public static final String SHOP_EVOUCHER_SPECIAL = "shop/evoucher/special";
    public static final String GET_BANNER_IMAGE = "banner";
    public static final String GET_MERCHANT = "merchant";
    public static final String SHOP_EVOUCHER_SEARCH = "shop/evoucher/search";
    public static final String READ_NOTIFICATION = "notification/track/{public_id}";
    public static final String GET_NOTIFICATION_COUNT = "notification/count";
    public static final String GET_NOTIFICATION = "notification/v1";
    public static final String GET_DENOMINATION = "payment/denomination";
    public static final String GET_PAYMENT_PARTNER_CATEGORY = "paymentpartner";
    public static final String CREATE_PAYMENT_CATEGORY = "payment";
    public static final String SHOP_CATEGORY = "shop/category";
    public static final String GET_PAYMENT_SERVICE_CATEGORY = "service";
    public static final String SHOP_LIST_HOT = "shop/hot";
    public static final String REWARD_CODE_COUNT = "reward_code/count";
    public static final String USER_READ_ALL_NOTIFICATION = "notification/all";
    public static final String SHOP_LIST_PRODUCT = "shop/list";
    public static final String REWARD_CODE = "reward_code";
    public static final String REWARD_CODE_BANNER = "reward_code/banner";
    public static final String SHOP_PRODUCT_DETAIL = "shop";
    public static final String CHECK_PHONE = "check_phone";
    public static final String USER_VERIFY_PHONE = "profile/verified_phone";
    public static final String CHECK_QUANTITY_PRODUCT = "shop/buy_info";
    public static final String LIXI_SHOP_BUY_PRODUCT = "shop/buy";
    public static final String LIXI_SHOP_BUY_PRODUCT_V2 = "shop/v1/exchange";
    public static final String SHOP_ORDER_CHECK = "shop/order/check";
    public static final String SHOP_DISCOUNT = "shop/discount";
    public static final String PAYOO_ORDER_NUMER = "shop/order_payoo/number";
    public static final String SHOP_PAY_CASH = "shop/pay/cash";
    public static final String SHOP_PAY_CASH_NAPAS = "shop/napas/pay/cash";
    public static final String SHOP_PAY_CASH_V2 = "shop/v1/pay/cash";
    public static final String SHOP_PAY_CASH_NAPAS_V2 = "shop/v1/napas/pay/cash";
    public static final String SHOP_ORDER_DELETE = "shop/order/delete";
    public static final String PAYMENT_CALLBACK = "payment/callback";
    public static final String PAYMENT_CALLBACK_NAPAS = "payment/result";
    public static final String GET_HISTORY = "transaction";
    public static final String SHOP_EVOUCHER_NEW = "shop/evoucher/new";
    public static final String SHOP_EVOUCHER_MERCHANT = "shop/merchant";
    public static final String ADD_FAVORITE_MERCHANT = "merchant/favorite";
    public static final String MERCHANT_STORE = "store";
    public static final String MERCHANT_MENU = "merchant/menu";
    public static final String GET_FAQ = "faq";
    public static final String FEEDBACK = "feedback";
    public static final String USER_EVOUCHER_BOUGHT = "user/evoucher/bought";
    public static final String SHOP_EVOUCHER = "shop/evoucher";
    public static final String STORE_APPLY_CODE = "store/apply_code";
    public static final String USER_ACTIVE_CODE = "user/active_code";
    public static final String STORE_CODEE = "store/code";
    public static final String VOUCHER_COMPLETE = "shop/is_voucher_complete";
    public static final String GET_LIST_CARD = "card";
    public static final String USER_ACTIVE_CODE_MULTIPLE = "user/active_code_multiple";
    public static final String STORE_NEAR_BY = "merchant/store_near_by";
    public static final String STORE_CODE = "store/code";
    public static final String EVOUCHER_UNUSED_SUMMARY = "user/evoucher/unused/summary";
    public static final String EVOUCHER_UNUSED = "user/evoucher/unused";
    public static final String EVOUCHER_ACTIVE = "user/evoucher/active";
    public static final String GROUP_CODE = "group_code";
    public static final String RATING = "rating";
    public static final String RATING_OPTION = "rating/option";
    public static final String SHOP_PAY_MOMO = "shop/pay/cash/momo";
    public static final String SHOP_PAY_ZALO_WALLET = "shop/pay/cash/zalo/wallet";

    // region Login
    public static final String LOGIN_CHECK_PHONE = "login/check/phone";
    public static final String LOGIN_UPDATE_USER_INFO = "login/info";
    public static final String LOGIN_ACCOUNT_KIT = "login/accountkit";
    public static final String LOGIN_PHONE = "login/phone";
    public static final String LOGIN_FACEBOOK = "login/facebook";
    public static final String LOGIN_CHECK_FACEBOOK = "login/check/facebook";
    public static final String LOGIN_FORGOT_PASS = "aktoken/check/phone";
    public static final String LOGIN_PASSWORD = "login/password";
    // endregion

    //region DeviceToken
    public static final String MOBILE_INIT = "mobile";
    public static final String MOBILE_INIT_GUEST = "mobile/guest";
    public static final String MOBILE_LOGOUT = "mobile/logout";
    //endregion

    //region HOMEPAGE NEW 2.5
    public static final String USER_EVOUCHER_SPECIAL = "/user/evoucher/special";
    public static final String USER_EVOUCHER_HOT = "/user/evoucher/hot";
    public static final String USER_EVOUCHER_NEARBY = "/user/evoucher/near";
    public static final String USER_EVOUCHER_GOODPRICE = "/user/evoucher/price";
    public static final String PRODUCT_CATEGORY = "/category";

    public static final String COLLECTION = "/collection";
    public static final String COLLECTION_EVOUCHER = "/collection/evoucher";

    public static final String CATEGORY_MERCHANT = "/category/merchant";
    public static final String CATEGORY_VOUCHER = "/category/evoucher";
    public static final String KEYWORD_HOT = "/keyword/hot";

    public static final String FLASH_SALE = "/flash_sale";
    public static final String FLASH_SALE_NOW = "/flash_sale/now";
    public static final String FLASH_SALE_SHOP = "/shop";
    public static final String FLASH_SALE_REMIND = "/flash_sale/remind";
    //endregion

    public static final String PAYMENT_CALLBACK_PATH = "/account/shoppaymentcallback";
    public static final String PAYMENT_CALLBACK_PATH_NAPAS = "/payment/result";
    public static final String CREATE_NEW_CARD = "/create/token/napas";
    public static final String USER_TRANSACTION = "user/transaction";

    //Payment cod
    public static final String PAYMENT_COD = "/shop/pay/cash/cod";
    public static final String GET_SURVEY = "survey";
    public static final String DECLINE_SURVEY = "survey/refuse";
    public static final String ANSWER_SURVEY = "survey/answer";
    public static final String SURVEY_CODE = "survey/code";

    //delivery
    public static final String ARTICLE = "user/article";
    public static final String ARTICLE_DETAIL = "user/article/detail";
    public static final String STORE_OF_SUBCATEGORY = "user/article/detail/store_of_subcategory";
    public static final String STORE_OF_ARTICLE = "user/store/article";
    public static final String STORE_RECOMMEND = "user/store/recommend";
    public static final String PRODUCT_MERCHANT_HOT = "user/product/merchant_hot";
    public static final String STORE_TAG = "user/store_tag";

    public static final String STORE_BEST_PRICE = "user/store/best_price";
    public static final String STORE_LIST = "user/store";
    public static final String STORE_DETAIL = "user/store/detail";
    public static final String STORE_LIST_HOT = "user/store/list_hot";
    public static final String STORE_DELI_FASTEST = "user/store/fastest";
    public static final String BANNER = "user/banner";
    public static final String TOPIC = "user/store/topic";
    public static final String TOPIC_DETAIL = "user/store_topic/detail";

    public static final String PRODUCT_DETAIL = "user/product/detail";
    public static final String PRODUCT_MENU = "user/product/menu";

    public static final String USER_ORDER = "user/user_order";
    public static final String USER_ORDER_DETAIL = "user/user_order/detail";
    public static final String USER_ORDER_SUBMIT = "user/user_order/submit";
    public static final String USER_PROMOTION = "user/promotion_code";
    public static final String USER_PROMOTION_FOR_YOU = "user/promotion_code/for_you";
    public static final String USER_PROMOTION_DETAIL = "user/promotion/detail";
    public static final String USER_PROMOTION_BY_CODE = "user/promotion/by_code";
    public static final String USER_PROMOTION_BY_VALUE = "user/promotion_code/value";
    public static final String USER_PROMOTION_APPLY = "user/promotion_code/calculate_money";
    public static final String USER_PROMOTION_CANCEL = "user/promotion_code/cancel";
    public static final String USER_PAYMENT = "user/payment/method_list";
    public static final String USER_BANK_CODE = "user/payment/bank_code";
    public static final String USER_PAY = "user/user_order/pay";
    public static final String USER_UPDATE_ORDER = "user/user_order_request";
    public static final String USER_DELI_CONFIG = "user/config";
    public static final String USER_ORDER_FEEDBACK = "user/user_order_feedback";
    public static final String USER_ORDER_FEEDBACK_ITEM = "user/user_order_feedback_item";
    public static final String USER_CALCULATE_DISTANCE = "user/store/calculate_distance";
    public static final String USER_ORDER_REPAY = "user/user_order/repay";
    public static final String USER_ORDER_CALCULATE_PRICE = "user/user_order/calculate_price";
    public static final String USER_FAVORITE_STORE = "user/user_favorite_store";

    public static final String USER_REFERENCE = "user/reference";

    public static final String UPDATE_PROFILE = "update/profile";
    public static final String UPDATE_PASS = "update/pass";

    //region
    public static final String USER_CLAIM_REASON = "user/claim/reason";
    public static final String USER_CLAIM_URGENT = "user/claim/urgent";
    //endregion

    // User location
    public static final String USER_GET_LOCATION = "user/user_address";
    public static final String USER_CREATE_LOCATION = "user/user_address";
    public static final String USER_EDIT_LOCATION = "user/user_address";
    public static final String USER_DELETE_LOCATION = "user/user_address";

    public static String getPaymentLink() {
        if (BuildConfig.BASE_URL.equals("http://45.118.151.83:8000"))
            return "http://dev.lixiapp.com/account/shoppaymentcallback";
        else if (BuildConfig.BASE_URL.equals("https://api.opencheck.co")) {
            return "https://m.lixiapp.com/account/shoppaymentcallback";
        }
        return "account/shoppaymentcallback";
    }

    public static String getPaymentLinkSafe() {
        if (BuildConfig.BASE_URL.equals("http://45.118.151.83:8000"))
            return "https://dev.lixiapp.com/account/shoppaymentcallback";
        else if (BuildConfig.BASE_URL.equals("https://api.opencheck.co")) {
            return "https://m.lixiapp.com/account/shoppaymentcallback";
        }
        return "";
    }


}
