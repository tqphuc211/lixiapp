package com.opencheck.client.dataloaders;

import com.google.gson.JsonObject;
import com.opencheck.client.home.account.evoucher.Model.ActiveCodeApiBody;
import com.opencheck.client.home.account.evoucher.Model.EVoucherSummary;
import com.opencheck.client.home.account.evoucher.Model.EVoucherUnused;
import com.opencheck.client.home.account.evoucher.Model.GroupCode;
import com.opencheck.client.home.account.evoucher.Model.GroupCodeApiBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodModel;
import com.opencheck.client.home.account.new_layout.model.DetailTransaction;
import com.opencheck.client.home.account.new_layout.model.HistoryIntroModel;
import com.opencheck.client.home.account.new_layout.model.UserTransaction;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.BankModel;
import com.opencheck.client.home.delivery.model.ClaimReasonModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliApiWrapperForListModel;
import com.opencheck.client.home.delivery.model.DeliApiWrapperModel;
import com.opencheck.client.home.delivery.model.DeliBannerModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.DeliOrderResponseModel;
import com.opencheck.client.home.delivery.model.DetailTopicModel;
import com.opencheck.client.home.delivery.model.FeedbackItemModel;
import com.opencheck.client.home.delivery.model.FeedbackModel;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.ResponsePromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.model.StoreTopicModel;
import com.opencheck.client.home.delivery.model.SubCategoryModel;
import com.opencheck.client.home.delivery.model.UserFeedbackModel;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindRequest;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindResponse;
import com.opencheck.client.home.flashsale.Model.FlashSaleTime;
import com.opencheck.client.home.rating.model.MarkUsedResponse;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.home.rating.model.RatingOption;
import com.opencheck.client.home.rating.model.RatingResponse;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.FAQModel;
import com.opencheck.client.models.FacebookTokenModel;
import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.ProductCategoryModel;
import com.opencheck.client.models.ProductCollectionModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperModel;
import com.opencheck.client.models.lixishop.ActiveCodeModel;
import com.opencheck.client.models.lixishop.CompletedVoucherModel;
import com.opencheck.client.models.lixishop.DetailHistoryCardModel;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.models.lixishop.LixiProductModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.LixiShopCategoryModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.models.lixishop.LixiShopTransactionResultModel;
import com.opencheck.client.models.lixishop.OrderCheckModel;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.lixishop.QuantityProductModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.models.user.UserLoginModel;
import com.opencheck.client.models.merchant.BannerModel;
import com.opencheck.client.models.merchant.DiscountModel;
import com.opencheck.client.models.merchant.MenuModel;
import com.opencheck.client.models.merchant.MerchantHotBrandModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.models.merchant.PaymentPartnerModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.models.merchant.survey.SurveyAnswerResultModel;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.LixiCodeDetailModel;
import com.opencheck.client.models.user.LixiCodeModel;
import com.opencheck.client.models.user.OrderHistoryModel;
import com.opencheck.client.models.user.TokenModel;
import com.opencheck.client.models.user.UserModel;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public interface ApiCall {

    //region implement API call
    @POST(ApiEntity.USER_AUTHEN)
    Call<TokenModel> _postUserAccessToken(@Body FacebookTokenModel userInfo);

    @POST(ApiEntity.MOBILE_INIT)
    Call<JsonObject> _sendPushToken(@Body RequestBody params);

    @POST(ApiEntity.MOBILE_INIT_GUEST)
    Call<JsonObject> _sendPushTokenGuest(@Body RequestBody params);

    @GET(ApiEntity.MOBILE_LOGOUT + "/{deviceId}")
    Call<JsonObject> _logoutPushToken(@Path("deviceId") String deviceId);

    @GET(ApiEntity.USER_PROFILE)
    Call<ApiWrapperModel<UserModel>> _getUserProfile();

    @GET(ApiEntity.USER_CONFIG)
    Call<ApiWrapperModel<ConfigModel>> _getUserConfig();

    @GET(ApiEntity.GET_MERCHANT_HOT_BRAND)
    Call<ApiWrapperForListModel<MerchantHotBrandModel>> _getMerchantHotBranch(@Query("page") int page,
                                                                              @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.SHOP_EVOUCHER_SPECIAL)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getSpecialEvoucher(@Query("page") int page,
                                                                            @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.GET_BANNER_IMAGE)
    Call<ApiWrapperForListModel<BannerModel>> _getBannerImage();

    @GET(ApiEntity.GET_MERCHANT)
    Call<ApiWrapperForListModel<MerchantModel>> _searchMerchant(@Query("page") int page,
                                                                @Query("record_per_page") int record_per_page,
                                                                @Query("tag_name") String tag_name,
                                                                @Query("keyword") String keyword,
                                                                @Query("u_id") String u_id);

    @GET(ApiEntity.SHOP_EVOUCHER_SEARCH)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _searchVoucher(@Query("page") int page,
                                                                       @Query("record_per_page") int record_per_page,
                                                                       @Query("search") String search);

    @GET(ApiEntity.SHOP_LIST_PRODUCT)
    Call<ApiWrapperForListModel<LixiProductModel>> _searchVoucherByLixi(@Query("page") int page,
                                                                        @Query("record_per_page") int record_per_page,
                                                                        @Query("keyword") String keyword);

    @GET(ApiEntity.READ_NOTIFICATION)
    Call<ApiWrapperModel<LixiModel>> _setReadNofication(@Path("public_id") String public_id);

    @GET(ApiEntity.GET_NOTIFICATION_COUNT)
    Call<Object> _getNotificationCount();

    @POST(ApiEntity.CREATE_PAYMENT_CATEGORY)
    Call<JsonObject> _createPaymentService(@Body RequestBody params);

    @GET(ApiEntity.GET_DENOMINATION)
    Call<Object> _getCardValue();

    @GET(ApiEntity.GET_PAYMENT_PARTNER_CATEGORY)
    Call<ApiWrapperForListModel<PaymentPartnerModel>> _getPaymentPartner(@Query("service_id") String id);

    @GET(ApiEntity.GET_NOTIFICATION)
    Call<ApiWrapperForListModel<NotificationModel>> _getNotification(@Query("page") int page,
                                                                     @Query("record_per_page") int record_per_page,
                                                                     @Query("type") String type,
                                                                     @Query("region_param") int region);

    @GET(ApiEntity.SHOP_CATEGORY)
    Call<ApiWrapperForListModel<LixiShopCategoryModel>> _getLixiShopCategory(@Query("page") int page,
                                                                             @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.GET_PAYMENT_SERVICE_CATEGORY)
    Call<ApiWrapperForListModel<ServiceCategoryModel>> _getLixiShopPaymentService(@Query("parent_id") int parent_id);

    @GET(ApiEntity.SHOP_LIST_HOT)
    Call<ApiWrapperForListModel<LixiHotProductModel>> _getLixiShopHotProduct(@Query("page") int page,
                                                                             @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.REWARD_CODE_COUNT)
    Call<JsonObject> _getNumLixiCode();

    @POST(ApiEntity.USER_READ_ALL_NOTIFICATION)
    Call<JsonObject> _readAllNotification(@Body RequestBody params);

    @GET(ApiEntity.SHOP_LIST_PRODUCT)
    Call<ApiWrapperForListModel<LixiProductModel>> _getLixiShopProductCategory(@Query("page") int page,
                                                                               @Query("record_per_page") int record_per_page,
                                                                               @Query("category") String category);

    @GET(ApiEntity.REWARD_CODE)
    Call<ApiWrapperForListModel<LixiCodeModel>> _getLixiShopRewardCode(@Query("page") int page,
                                                                       @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.REWARD_CODE_BANNER)
    Call<ApiWrapperForListModel<LixiCodeBarnerModel>> _getLixiShopRewardCodeBanner();

    @POST(ApiEntity.REWARD_CODE)
    Call<JsonObject> _useRewardCode(@Body RequestBody params);

    @POST(ApiEntity.REWARD_CODE + "/{id}")
    Call<ApiWrapperModel<LixiCodeDetailModel>> _getRewardCode(@Path("id") long id);

    @GET(ApiEntity.SHOP_PRODUCT_DETAIL + "/{id}")
    Call<ApiWrapperModel<LixiShopProductDetailModel>> _getLixiShopProductDetail(@Path("id") long id);

    @GET(ApiEntity.SHOP_PRODUCT_DETAIL + "/{id}")
    Call<ApiWrapperModel<ProductCashVoucherDetailModel>> _getLixiShopProductDetailNew(@Path("id") long id);

    @GET(ApiEntity.SHOP_PRODUCT_DETAIL + "/{id}")
    Call<ApiWrapperModel<ProductCashVoucherDetailModel>> _getLixiShopProductDetailNew(@Path("id") long id,
                                                                                      @Query("city_id_sort") int city_id_sort,
                                                                                      @Query("lat") String lat,
                                                                                      @Query("lon") String lng);

    @POST(ApiEntity.CHECK_PHONE)
    Call<JsonObject> _checkPhone(@Body RequestBody params);

    @POST(ApiEntity.USER_VERIFY_PHONE)
    Call<JsonObject> _verifyPhone(@Body RequestBody params);

    @GET(ApiEntity.CHECK_QUANTITY_PRODUCT + "/{id}")
    Call<ApiWrapperModel<QuantityProductModel>> _getLixiShopProductQuantity(@Path("id") long id);

    @POST(ApiEntity.LIXI_SHOP_BUY_PRODUCT)
    Call<ApiWrapperModel<LixiShopTransactionResultModel>> _buyProductLixiShop(@Body RequestBody params);

    @POST(ApiEntity.LIXI_SHOP_BUY_PRODUCT_V2)
    Call<ApiWrapperModel<LixiShopTransactionResultModel>> _buyProductLixiShopV2(@Body RequestBody params);

    @GET(ApiEntity.SHOP_ORDER_CHECK + "/{id}")
    Call<ApiWrapperModel<OrderCheckModel>> _checkProduct(@Path("id") long id);

    @POST(ApiEntity.SHOP_DISCOUNT)
    Call<ApiWrapperModel<DiscountModel>> _postDiscount(@Body RequestBody params);

    @GET(ApiEntity.PAYOO_ORDER_NUMER)
    Call<ApiWrapperModel<LixiShopBankTransactionResult>> _getBuyAtStore(@Query("order_id") long page);

    @POST(ApiEntity.SHOP_PAY_CASH + "{method}")
    Call<JsonObject> _buyCashVoucher(@Body RequestBody params, @Path("method") String method);

    @POST(ApiEntity.SHOP_PAY_CASH_NAPAS)
    Call<JsonObject> _buyCashVoucherNapas(@Body RequestBody params);

    @POST(ApiEntity.SHOP_PAY_CASH_V2 + "{method}")
    Call<JsonObject> _buyCashVoucherV2(@Body RequestBody params, @Path("method") String method);

    @POST(ApiEntity.SHOP_PAY_CASH_NAPAS_V2)
    Call<JsonObject> _buyCashVoucherNapasV2(@Body RequestBody params);

    @DELETE(ApiEntity.SHOP_ORDER_DELETE + "/{id}")
    Call<JsonObject> _deletePendingOrder(@Path("id") long id);

    @GET(ApiEntity.GET_HISTORY)
    Call<ApiWrapperForListModel<OrderHistoryModel>> _getHistoryList(@Query("page") int page,
                                                                    @Query("record_per_page") int record_per_page,
                                                                    @Query("type") String type,
                                                                    @Query("store_name") String store_name,
                                                                    @Query("from_date") String from_date,
                                                                    @Query("to_date") String to_date,
                                                                    @Query("state") String state);

    @GET(ApiEntity.GET_HISTORY + "/{id}")
    Call<ApiWrapperModel<OrderDetailModel>> _getHistoryDetail(@Path("id") String id);

    @GET(ApiEntity.SHOP_EVOUCHER_NEW)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getMerchantEVoucherNew(@Query("page") int page,
                                                                                @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.GET_MERCHANT + "/{id}")
    Call<ApiWrapperModel<MerchantModel>> _getMerchantDetail(@Path("id") String merchant_id, @Query("u_id") String u_id);

    @GET(ApiEntity.SHOP_EVOUCHER_MERCHANT)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getMerchantEVoucher(@Query("page") int page,
                                                                             @Query("record_per_page") int record_per_page,
                                                                             @Query("merchant_id") String merchant_id);

    @POST(ApiEntity.ADD_FAVORITE_MERCHANT)
    Call<JsonObject> _addFavouriteMerchant(@Body RequestBody params);

    @GET(ApiEntity.MERCHANT_STORE)
    Call<ApiWrapperForListModel<StoreModel>> _getMerchantStore(@Query("page") int page,
                                                               @Query("record_per_page") int record_per_page,
                                                               @Query("merchant_id") String merchant_id,
                                                               @Query("city_id_sort") int city_id,
                                                               @Query("lat") String lat,
                                                               @Query("lng") String lng);

    @GET(ApiEntity.MERCHANT_STORE)
    Call<ApiWrapperForListModel<StoreModel>> _getMerchantStore(@Query("page") int page,
                                                               @Query("record_per_page") int record_per_page,
                                                               @Query("merchant_id") String merchant_id,
                                                               @Query("city_id_sort") int city_id,
                                                               @Query("lat") String lat,
                                                               @Query("lng") String lng,
                                                               @Query("keyword") String keyword);

    @GET(ApiEntity.MERCHANT_STORE)
    Call<ApiWrapperForListModel<StoreModel>> _getMapStoreList(@Query("merchant_id") String merchant_id,
                                                              @Query("lat") String lat,
                                                              @Query("lng") String lng,
                                                              @Query("city_id") String city_id,
                                                              @Query("district_id") String district_id);

    @GET(ApiEntity.MERCHANT_MENU + "/{id}")
    Call<ApiWrapperForListModel<MenuModel>> _getMerchantMenu(@Path("id") String merchant_id,
                                                             @Query("page") int page,
                                                             @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.GET_FAQ)
    Call<ApiWrapperForListModel<FAQModel>> _getFAQ();

    @Multipart
    @POST(ApiEntity.UPDATE_AVATAR)
    Call<ApiWrapperModel<UserModel>> _updateAvatar(@Part MultipartBody.Part file, @Part("name") RequestBody name);

    @PUT(ApiEntity.USER_PROFILE)
    Call<ApiWrapperModel<UserModel>> _updateProfile(@Body RequestBody params);

    @POST(ApiEntity.FEEDBACK)
    Call<JsonObject> _sendFeedback(@Body RequestBody params);

    @GET(ApiEntity.USER_EVOUCHER_BOUGHT)
    Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> _getBoughEVoucher(@Query("page") int page,
                                                                                @Query("record_per_page") int record_per_page,
                                                                                @Query("state") String state);

    @GET(ApiEntity.SHOP_EVOUCHER + "/{id}")
    Call<ApiWrapperModel<EvoucherDetailModel>> _getCashBoughtEvoucherDetail(@Path("id") long id);

    @GET(ApiEntity.STORE_APPLY_CODE + "/{product_id}")
    Call<ApiWrapperForListModel<StoreApplyModel>> _getBoughtEvoucherApplyStore(@Path("product_id") long product_id);

    @POST(ApiEntity.USER_ACTIVE_CODE)
    Call<ApiWrapperModel<ActiveCodeModel>> _submitActiveCode(@Body RequestBody params);

    @GET(ApiEntity.STORE_CODEE + "/{id}")
    Call<ApiWrapperModel<StoreApplyModel>> _getStoreApplyCashCode(@Path("id") String id);

    @GET(ApiEntity.VOUCHER_COMPLETE + "/{id}")
    Call<ApiWrapperModel<CompletedVoucherModel>> _getCompleteVoucherCode(@Path("id") long id);

    @GET(ApiEntity.STORE_APPLY_CODE + "/{id}")
    Call<ApiWrapperForListModel<StoreApplyModel>> _getStoreApplyCashCodeList(@Path("id") long id,
                                                                             @Query("page") int page,
                                                                             @Query("record_per_page") int record_per_page,
                                                                             @Query("search") String key);

    @GET(ApiEntity.GET_LIST_CARD + "/{id}")
    Call<ApiWrapperForListModel<DetailHistoryCardModel>> _getListCard(@Path("id") String id);

    @POST(ApiEntity.USER_ACTIVE_CODE_MULTIPLE)
    Call<ApiWrapperForListModel<ActiveCodeModel>> _submitMultipleActiveCode(@Body RequestBody params);

    @POST(ApiEntity.SHOP_PAY_MOMO)
    Call<JsonObject> _buyProductByMomo(@Body RequestBody params);

    @POST(ApiEntity.SHOP_PAY_ZALO_WALLET)
    Call<JsonObject> _buyProductByZaloWallet(@Body RequestBody params);

    @POST
    Call<JsonObject> _submitToMomo(@Url String url, @Body RequestBody params);

    @POST
    Call<JsonObject> _submitToZaloWallet(@Url String url, @Body RequestBody params);

    @GET(ApiEntity.USER_EVOUCHER_BOUGHT)
    Call<ApiWrapperForListModel<LixiShopEvoucherBoughtModel>> _getStoreBoughEVoucher(@Query("page") int page,
                                                                                     @Query("record_per_page") int record_per_page,
                                                                                     @Query("store_id") long store_id);

    @GET(ApiEntity.STORE_NEAR_BY)
    Call<ApiWrapperForListModel<NearestStoreModel>> _getStoreNearby(@Query("page") int page,
                                                                    @Query("record_per_page") int record_per_page,
                                                                    @Query("search") String store_id);

    @GET(ApiEntity.STORE_CODE + "/{id}")
    Call<ApiWrapperModel<NearestStoreModel>> _getStoreCode(@Path("id") String id);

    @GET(ApiEntity.STORE_NEAR_BY)
    Call<ApiWrapperForListModel<NearestStoreModel>> _getNearsetStore(@Query("page") int page,
                                                                     @Query("record_per_page") int record_per_page,
                                                                     @Query("current_lat") float current_lat,
                                                                     @Query("current_long") float current_long);

    @GET(ApiEntity.GET_SURVEY + "/{id}")
    Call<ApiWrapperModel<SurveyModel>> _getUserSurvey(@Path("id") String survey_id);

    @POST(ApiEntity.ANSWER_SURVEY)
    Call<ApiWrapperModel<SurveyAnswerResultModel>> _userAnswerSurvey(@Body RequestBody params);

    @POST(ApiEntity.DECLINE_SURVEY)
    Call<ApiWrapperModel<Object>> _userDeclineSurvey(@Body RequestBody params);

    //article
    @GET(ApiEntity.ARTICLE)
    Call<DeliApiWrapperForListModel<ArticleModel>> _getArticle(@Query("page") int page,
                                                               @Query("page_size") int page_size,
                                                               @Query("province") String province);

    @GET(ApiEntity.STORE_OF_ARTICLE)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getStoreOfArticle(@Query("page") int page,
                                                                              @Query("page_size") int page_size,
                                                                              @Query("shipping_receive_address") String shipping_receive_address,
                                                                              @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                              @Query("shipping_receive_lng") double shipping_receive_lng,
                                                                              @Query("article_id") long article_id);

    @GET(ApiEntity.ARTICLE_DETAIL)
    Call<DeliApiWrapperModel<ArticleModel>> _getDetailArticle(@Query("page") int page,
                                                              @Query("page_size") int page_size,
                                                              @Query("article_id") long article_id);

    @GET(ApiEntity.STORE_OF_SUBCATEGORY)
    Call<DeliApiWrapperForListModel<SubCategoryModel>> _getSubCategory(@Query("page") int page,
                                                                       @Query("page_size") int page_size,
                                                                       @Query("article_id") long article_id,
                                                                       @Query("subcategory_id") long subcategory_id);

    @GET(ApiEntity.STORE_LIST)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getStoreList(@Query("search") String key,
                                                                         @Query("tag_id") Long tag_id,
                                                                         @Query("page") int page,
                                                                         @Query("page_size") int page_size,
                                                                         @Query("shipping_receive_address") String shipping_receive_address,
                                                                         @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                         @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.STORE_BEST_PRICE)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getBestPrice(@Query("page") int page,
                                                                         @Query("page_size") int page_size,
                                                                         @Query("shipping_receive_address") String shipping_receive_address,
                                                                         @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                         @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.STORE_RECOMMEND)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getStoreRecommend(@Query("page") int page,
                                                                              @Query("page_size") int page_size,
                                                                              @Query("shipping_receive_address") String shipping_receive_address,
                                                                              @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                              @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.PRODUCT_MERCHANT_HOT)
    Call<DeliApiWrapperForListModel<ProductMerchantHot>> _getProductMerchantHot(@Query("page") int page,
                                                                                @Query("page_size") int page_size,
                                                                                @Query("shipping_receive_address") String shipping_receive_address,
                                                                                @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                                @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.STORE_TAG)
    Call<DeliApiWrapperForListModel<ArticleModel>> _getStoreTagList(@Query("page") int page,
                                                                    @Query("page_size") int page_size,
                                                                    @Query("category_code") String category_code,
                                                                    @Query("show_home_only") boolean show_home_only,
                                                                    @Query("province") String province);

    @GET(ApiEntity.STORE_DETAIL)
    Call<DeliApiWrapperModel<StoreOfCategoryModel>> _getStoreDetail(@Query("shipping_receive_address") String shipping_receive_address,
                                                                    @Query("shipping_receive_lat") Double shipping_receive_lat,
                                                                    @Query("shipping_receive_lng") Double shipping_receive_lng,
                                                                    @Query("store_id") long store_id);

    @GET(ApiEntity.STORE_DETAIL)
    Call<DeliApiWrapperModel<StoreOfCategoryModel>> _getStoreDetail(@Query("shipping_receive_address") String shipping_receive_address,
                                                                    @Query("shipping_receive_lat") Double shipping_receive_lat,
                                                                    @Query("shipping_receive_lng") Double shipping_receive_lng,
                                                                    @Query("store_id") long store_id,
                                                                    @Query("user_id") long user_id);

    @GET(ApiEntity.STORE_LIST_HOT)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getListHot(@Query("page") int page,
                                                                       @Query("page_size") int page_size,
                                                                       @Query("shipping_receive_address") String shipping_receive_address,
                                                                       @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                       @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.STORE_DELI_FASTEST)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getStoreFastest(@Query("page") int page,
                                                                            @Query("page_size") int page_size,
                                                                            @Query("shipping_receive_address") String shipping_receive_address,
                                                                            @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                            @Query("shipping_receive_lng") double shipping_receive_lng);


    @GET(ApiEntity.PRODUCT_DETAIL)
    Call<DeliApiWrapperModel<ProductModel>> _getProductDetail(@Query("product_id") long product_id);

    @GET(ApiEntity.BANNER)
    Call<DeliApiWrapperForListModel<DeliBannerModel>> _getBanner(@Query("page") int page,
                                                                 @Query("page_size") int page_size,
                                                                 @Query("province") String province);

    @GET(ApiEntity.TOPIC)
    Call<DeliApiWrapperForListModel<StoreTopicModel>> _getListTopic(@Query("topic_id") long topic_id,
                                                                    @Query("shipping_receive_address") String shipping_receive_address,
                                                                    @Query("shipping_receive_lat") double lat,
                                                                    @Query("shipping_receive_lng") double lng,
                                                                    @Query("page") int page,
                                                                    @Query("page_size") int page_size);

    @GET(ApiEntity.TOPIC_DETAIL)
    Call<DeliApiWrapperModel<DetailTopicModel>> _getDetailTopic(@Query("id") long topic_id);


    @GET(ApiEntity.PRODUCT_MENU)
    Call<DeliApiWrapperForListModel<com.opencheck.client.home.delivery.model.MenuModel>> _getMenu(@Query("store_id") long store_id);

    @GET(ApiEntity.USER_ORDER)
    Call<DeliApiWrapperForListModel<DeliOrderModel>> _getUserOrderHistory(@Query("page") int page,
                                                                          @Query("page_size") int page_size,
                                                                          @Query("state") String state,
                                                                          @Query("create_date_from") Long create_date_from,
                                                                          @Query("create_date_to") Long create_date_to);

    @GET(ApiEntity.USER_PROMOTION)
    Call<DeliApiWrapperForListModel<PromotionModel>> _getPromotionList(@Query("search") String search, @Query("store_id") long store_id, @Query("user_id") Long user_id);

    @GET(ApiEntity.USER_PROMOTION_FOR_YOU)
    Call<DeliApiWrapperForListModel<PromotionModel>> _getPromotionListUserGroup(@Query("search") String search,
                                                                                @Query("shipping_receive_address") String shipping_receive_address,
                                                                                @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                                @Query("shipping_receive_lng") double shipping_receive_lng);

    @GET(ApiEntity.USER_PAYMENT)
    Call<DeliApiWrapperForListModel<PaymentModel>> _getPaymentMethod(@Query("store_id") long store_id,
                                                                     @Query("user_order_uuid") String uuid);

    @GET(ApiEntity.USER_BANK_CODE)
    Call<DeliApiWrapperForListModel<BankModel>> _getBankCode(@Query("payment_method_code") String payment_method_code);

    @GET(ApiEntity.USER_PROMOTION_BY_CODE)
    Call<DeliApiWrapperModel<PromotionModel>> _getPromotionByCode(@Query("code") String code);

    @GET(ApiEntity.USER_PROMOTION_BY_VALUE)
    Call<DeliApiWrapperModel<ResponsePromotionModel>> _getPromotionByValue(@Query("user_order_uuid") String user_order_uuid,
                                                                           @Query("list_code") String list_code);

    @GET(ApiEntity.USER_PROMOTION_DETAIL)
    Call<DeliApiWrapperModel<PromotionModel>> _getPromotionDetail(@Query("promotion_id") long promotion_id);

    @GET(ApiEntity.USER_ORDER_DETAIL)
    Call<DeliApiWrapperModel<DeliOrderModel>> _getUserOrderDetail(@Query("user_order_uuid") String uuid, @Query("user_order_id") String user_order_id);

    @POST(ApiEntity.USER_PROMOTION_APPLY)
    Call<DeliApiWrapperModel<ResponsePromotionModel>> _addPromotionToOrder(@Body RequestBody params);

    @POST(ApiEntity.USER_UPDATE_ORDER)
    Call<DeliApiWrapperModel<String>> _userUpdateOrder(@Body RequestBody params);

    @PUT(ApiEntity.USER_PROMOTION_CANCEL)
    Call<DeliApiWrapperModel<String>> _deletePromotionOfOrder(@Body RequestBody params);

    @POST(ApiEntity.USER_ORDER)
    Call<DeliApiWrapperModel<String>> _createOrder(@Body RequestBody params);

    @PUT(ApiEntity.USER_ORDER)
    Call<DeliApiWrapperModel<String>> _updateOrder(@Body RequestBody params);

    @PUT(ApiEntity.USER_ORDER_REPAY)
    Call<DeliApiWrapperModel<DeliOrderResponseModel>> _repayOrder(@Body RequestBody params);

    @DELETE(ApiEntity.USER_ORDER)
    Call<DeliApiWrapperModel<String>> _deleteOrder(@Query("user_order_uuid") String user_order_uuid);

    @PUT(ApiEntity.USER_ORDER_SUBMIT)
    Call<DeliApiWrapperModel<DeliOrderResponseModel>> _submitOrder(@Body RequestBody params);

    @PUT(ApiEntity.USER_PAY)
    Call<DeliApiWrapperModel<DeliOrderResponseModel>> _payOrder(@Body RequestBody params);

    @POST(ApiEntity.USER_ORDER_FEEDBACK)
    Call<DeliApiWrapperModel<String>> _createFeedback(@Body RequestBody params);

    @GET(ApiEntity.USER_ORDER_FEEDBACK)
    Call<DeliApiWrapperForListModel<FeedbackModel>> _getFeedback();

    @GET(ApiEntity.USER_ORDER_FEEDBACK)
    Call<DeliApiWrapperForListModel<UserFeedbackModel>> _getUserFeedback(@Query("page") int page,
                                                                         @Query("page_size") int page_size,
                                                                         @Query("store_id") long store_id);

    @GET(ApiEntity.USER_DELI_CONFIG)
    Call<DeliApiWrapperForListModel<DeliConfigModel>> _getUserDeliConfig();

    @GET(ApiEntity.USER_ORDER_FEEDBACK_ITEM)
    Call<DeliApiWrapperForListModel<FeedbackItemModel>> _getFeedbackItem();

    @GET(ApiEntity.USER_GET_LOCATION)
    Call<DeliApiWrapperForListModel<DeliAddressModel>> _getUserLocation();

    @GET(ApiEntity.USER_CALCULATE_DISTANCE)
    Call<DeliApiWrapperModel<Double>> _getUserDistance(@Query("store_id") long store_id,
                                                       @Query("lat") double lat,
                                                       @Query("lng") double lng);

    @GET(ApiEntity.USER_ORDER_CALCULATE_PRICE)
    Call<DeliApiWrapperModel<ResponsePromotionModel>> _getCalculatePrice(@Query("uuid") String uuid,
                                                                         @Query("payment_method_code") String payment_method_code,
                                                                         @Query("promotion_code") String promotion_code,
                                                                         @Query("is_use_lixi_money") boolean is_use_lixi_money);

    @GET(ApiEntity.USER_FAVORITE_STORE)
    Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> _getFavoriteStore(@Query("page") int page,
                                                                             @Query("page_size") int page_size,
                                                                             @Query("shipping_receive_address") String shipping_receive_address,
                                                                             @Query("shipping_receive_lat") double shipping_receive_lat,
                                                                             @Query("shipping_receive_lng") double shipping_receive_lng);

    @POST(ApiEntity.USER_FAVORITE_STORE)
    Call<DeliApiWrapperModel<Object>> _addFavoriteStore(@Body RequestBody params);

    @DELETE(ApiEntity.USER_FAVORITE_STORE)
    Call<DeliApiWrapperModel<Integer>> _deleteFavoriteStore(@Query("store_id") long store_id);

    @POST(ApiEntity.USER_CREATE_LOCATION)
    Call<DeliApiWrapperModel<Integer>> _createLocation(@Body JsonObject params);

    @PUT(ApiEntity.USER_EDIT_LOCATION)
    Call<DeliApiWrapperModel<Integer>> _editLocation(@Body JsonObject params);

    @DELETE(ApiEntity.USER_DELETE_LOCATION)
    Call<DeliApiWrapperModel<Integer>> _deleteLocation(@Query("id") int id);

    //endregion

    //region HOMEPAGE 2.5
    @GET(ApiEntity.PRODUCT_CATEGORY)
    Call<ApiWrapperForListModel<ProductCategoryModel>> _getCategory(@Query("page") int page,
                                                                    @Query("record_per_page") int record_per_page,
                                                                    @Query("parent_id") int parent_id);

    @GET(ApiEntity.COLLECTION)
    Call<ApiWrapperForListModel<ProductCollectionModel>> _getCollection(@Query("page") int page,
                                                                        @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.COLLECTION_EVOUCHER)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getCollectionEvoucher(@Query("page") int page,
                                                                               @Query("record_per_page") int record_per_page,
                                                                               @Query("collection_id") int collection_id);

    @GET(ApiEntity.USER_EVOUCHER_HOT)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getEvoucherHot(@Query("page") int page,
                                                                        @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.USER_EVOUCHER_SPECIAL)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getEvoucherSpecial(@Query("page") int page,
                                                                            @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.USER_EVOUCHER_NEARBY)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getEvoucherNearby(@Query("page") int page,
                                                                           @Query("record_per_page") int record_per_page,
                                                                           @Query("lat") float lat,
                                                                           @Query("lon") float lng);

    @GET(ApiEntity.USER_EVOUCHER_GOODPRICE)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getEvoucherGoodprice(@Query("page") int page,
                                                                              @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.CATEGORY_MERCHANT)
    Call<ApiWrapperForListModel<MerchantModel>> _getCategoryMerchant(@Query("page") int page,
                                                                     @Query("record_per_page") int record_per_page,
                                                                     @Query("category_id") int category_id,
                                                                     @Query("keyword") String keyword);

    @GET(ApiEntity.CATEGORY_VOUCHER)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getCategoryVoucher(@Query("page") int page,
                                                                            @Query("record_per_page") int record_per_page,
                                                                            @Query("category_id") int category_id,
                                                                            @Query("keyword") String keyword);

    @GET(ApiEntity.CATEGORY_MERCHANT)
    Call<ApiWrapperForListModel<MerchantModel>> _getSearchCategoryMerchant(@Query("page") int page,
                                                                           @Query("record_per_page") int record_per_page,
                                                                           @Query("keyword") String keyword);

    @GET(ApiEntity.CATEGORY_VOUCHER)
    Call<ApiWrapperForListModel<LixiShopEvoucherModel>> _getSearchCategoryVoucher(@Query("page") int page,
                                                                                  @Query("record_per_page") int record_per_page,
                                                                                  @Query("keyword") String keyword);

    @GET(ApiEntity.KEYWORD_HOT)
    Call<ApiWrapperForListModel<String>> _getKeywordhot();
    //endregion

    //region Claim
    @GET(ApiEntity.USER_CLAIM_REASON)
    Call<DeliApiWrapperForListModel<ClaimReasonModel>> _getReasons();

    @POST(ApiEntity.USER_CLAIM_URGENT)
    Call<DeliApiWrapperModel<Integer>> _postReason(@Body RequestBody params);
    //endregion

    // For EVoucherSummary
    @GET(ApiEntity.EVOUCHER_UNUSED_SUMMARY)
    Call<ApiWrapperModel<EVoucherSummary>> _getEvoucherSummary(@Query("merchant") long merchant_id);

    @GET(ApiEntity.EVOUCHER_UNUSED)
    Call<ApiWrapperForListModel<EVoucherUnused>> _getEvoucherUnused(@Query("page") int page,
                                                                    @Query("record_per_page") long record_per_page,
                                                                    @Query("merchant") long merchant_id);

    @POST(ApiEntity.EVOUCHER_ACTIVE)
    Call<ApiWrapperModel<MarkUsedResponse>> _postActiveEvoucher(@Body JsonObject jsonObject);

    @POST(ApiEntity.GROUP_CODE)
    Call<ApiWrapperModel<GroupCode>> _getGroupCode(@Body GroupCodeApiBody groupCodeApiBody);

    @PUT(ApiEntity.GROUP_CODE + "/{group_id}")
    Call<ApiWrapperModel<GroupCode>> _getGroupCode(@Path("group_id") long group_id,
                                                   @Body ActiveCodeApiBody activeCodeApiBody);

    @POST(ApiEntity.PAYMENT_COD)
    Call<ApiWrapperModel<PaymentCodModel>> _paymentCod(@Body PaymentCodBody paymentCodBody);

    // For Flash sale
    @GET(ApiEntity.FLASH_SALE)
    Call<ApiWrapperForListModel<FlashSaleTime>> _getListFlashSaleTime(@Query("page") int page,
                                                                      @Query("record_per_page") long record_per_page);

    @GET(ApiEntity.FLASH_SALE + "/{id}")
    Call<ApiWrapperForListModel<FlashSaleNow.ProductsBean>> _getListFlashSaleDetail(@Path("id") int id,
                                                                                    @Query("page") int page,
                                                                                    @Query("record_per_page") long record_per_page,
                                                                                    @Query("keyword") String keyword);

    @GET(ApiEntity.FLASH_SALE_NOW)
    Call<ApiWrapperModel<FlashSaleNow>> _getListFlashSaleNow(@Query("page") int page,
                                                             @Query("record_per_page") long record_per_page);

    @POST(ApiEntity.FLASH_SALE_REMIND)
    Call<ApiWrapperModel<FlashSaleRemindResponse>> _requestFlashSaleRemind(@Body FlashSaleRemindRequest flashSaleRemindRequest);

    @DELETE(ApiEntity.FLASH_SALE_REMIND + "/{id}")
    Call<ApiWrapperModel<FlashSaleRemindResponse>> _deleteFlashSaleRemind(@Path("id") long id);

    @POST(ApiEntity.CREATE_NEW_CARD)
    Call<ApiWrapperModel<JsonObject>> _createNewCard(@Body JsonObject newCardResquest);

    @GET(ApiEntity.USER_TRANSACTION)
    Call<ApiWrapperForListModel<UserTransaction>> _getUserTransaction(@Query("page") int page,
                                                                      @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.USER_TRANSACTION + "/{id}")
    Call<ApiWrapperModel<DetailTransaction>> _getDetailTransaction(@Path("id") int id,
                                                                   @Query("index") String index);

    @GET(ApiEntity.RATING + "/{product_id}")
    Call<ApiWrapperForListModel<RatingModel>> _getListRating(@Path("product_id") int product_id,
                                                             @Query("page") int page,
                                                             @Query("record_per_page") int record_per_page);

    @GET(ApiEntity.RATING_OPTION)
    Call<ApiWrapperForListModel<RatingOption>> _getRatingOption(@Query("page") int page,
                                                                @Query("record_per_page") int record_per_page);

    @POST(ApiEntity.RATING)
    Call<ApiWrapperModel<RatingResponse>> _postRating(@Body JsonObject jsonObject);

    // region Login
    @POST(ApiEntity.LOGIN_CHECK_PHONE)
    Call<ApiWrapperModel<UserLoginModel>> _postCheckPhone(@Body JsonObject params);

    @PUT(ApiEntity.LOGIN_UPDATE_USER_INFO)
    Call<ApiWrapperModel<JsonObject>> _putUpdateUserInfo(@Body JsonObject params);

    @POST(ApiEntity.LOGIN_ACCOUNT_KIT)
    Call<ApiWrapperModel<UserLoginModel>> _postGetUserToken(@Body JsonObject params);

    @POST(ApiEntity.LOGIN_PHONE)
    Call<ApiWrapperModel<UserLoginModel>> _postLoginPhone(@Body JsonObject params);

    @PUT(ApiEntity.LOGIN_PHONE)
    Call<ApiWrapperModel<JsonObject>> _putLoginPhone(@Body RequestBody params);

    @POST(ApiEntity.LOGIN_FACEBOOK)
    Call<ApiWrapperModel<UserLoginModel>> _postLoginFacebook(@Body JsonObject param);

    @POST(ApiEntity.LOGIN_CHECK_FACEBOOK)
    Call<ApiWrapperModel<UserLoginModel>> _postCheckFacebook(@Body JsonObject param);

    @POST(ApiEntity.LOGIN_FORGOT_PASS)
    Call<ApiWrapperModel<UserLoginModel>> _postForgotPass(@Body JsonObject param);

    @PUT(ApiEntity.LOGIN_PASSWORD)
    Call<ApiWrapperModel<JsonObject>> _putResetPassword(@Body JsonObject param);
    // endregion

    @GET(ApiEntity.USER_REFERENCE)
    Call<ApiWrapperForListModel<HistoryIntroModel>> _getHistoryIntro(@Query("page") int page,
                                                                     @Query("record_per_page") int record);

    @PUT(ApiEntity.UPDATE_PROFILE)
    Call<ApiWrapperModel<JsonObject>> _putUpdateProfile(@Body JsonObject param);

    @PUT(ApiEntity.UPDATE_PASS)
    Call<ApiWrapperModel<JsonObject>> _putUpdatePass(@Body JsonObject param);
}
