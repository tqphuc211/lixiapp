package com.opencheck.client;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.opencheck.client.deeplink.ApplicationLifecycle;
import com.opencheck.client.parent.LixiParentFragment;
import com.opencheck.client.utils.AppPreferenceHelper;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public abstract class LixiFragment extends LixiParentFragment implements View.OnClickListener {

    protected View rootview = null;
    protected LixiActivity activity;
    protected boolean isResume = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (LixiActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        isResume = false;
        handleAnalytics();
    }

    @Override
    public void onStop() {
        super.onStop();
        isResume = true;
    }

    @Override
    public void onDestroyView() {

        if (rootview != null) {
            ViewGroup parentViewGroup = (ViewGroup) rootview.getParent();

            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }

        super.onDestroyView();
    }

    //region Gooogle Analytics
    private Tracker mTracker;

    private void handleAnalytics() {
        LixiApplication application = (LixiApplication) activity.getApplication();
        mTracker = application.getDefaultTracker();
        if (getScreenName() == null) {
            return;
        }
        mTracker.setScreenName(getScreenName() + "Screen");
        if (ApplicationLifecycle.UTM_QUERY == null ||
                ApplicationLifecycle.UTM_QUERY.length() == 0) {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    //.setCampaignParamsFromUrl(uri)
                    .build());
        } else {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCampaignParamsFromUrl(ApplicationLifecycle.UTM_QUERY)
                    .build());
            (new AppPreferenceHelper(activity)).setCampaignData("");
        }
    }

    //endregion


    @Override
    public String getScreenName() {
        return null;
    }
}
