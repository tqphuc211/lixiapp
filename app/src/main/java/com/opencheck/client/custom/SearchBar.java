package com.opencheck.client.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewSearchBarBinding;
import com.opencheck.client.utils.Helper;

public class SearchBar extends LinearLayout implements View.OnClickListener, TextWatcher {
    private Context context;
    private OnSearchEventListener onSearchEventListener;

    // var
    private boolean focusEdittext = true;
    private boolean isAllCap = false;

    public SearchBar(Context context) {
        super(context);
        initView(context);
    }

    public SearchBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.SearchBar, 0, 0);
        focusEdittext = typeArray.getBoolean(R.styleable.SearchBar_focusEdittext, true);
        isAllCap = typeArray.getBoolean(R.styleable.SearchBar_allCap, false);
        typeArray.recycle();
        initView(context);
    }

    public SearchBar(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.SearchBar, defStyle, defStyle);
        focusEdittext = typeArray.getBoolean(R.styleable.SearchBar_focusEdittext, true);
        typeArray.recycle();
        initView(context);
    }

    private EditText edtSearch;
    private ImageView imgClear;
    private LinearLayout linearParent;
    private ViewSearchBarBinding mBinding;

    private void initView(Context context) {
        this.context = context;
        mBinding = ViewSearchBarBinding.inflate(LayoutInflater.from(getContext()), this, true);

        edtSearch = (EditText) findViewById(R.id.edt_search);
        imgClear = (ImageView) findViewById(R.id.imgClear);
        linearParent = (LinearLayout) findViewById(R.id.linearParent);

        edtSearch.setFocusable(focusEdittext);
        if (isAllCap) {
            Helper.setAllCapFilter(edtSearch);
        }
        edtSearch.setOnClickListener(this);

        imgClear.setOnClickListener(this);
        edtSearch.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            // search
                            if (onSearchEventListener != null) {
                                onSearchEventListener.onComplete(getText());
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        edtSearch.addTextChangedListener(this);

        if (focusEdittext) {
            Helper.showSoftKeyBoard(context);
        }
    }

    public void clear() {
        edtSearch.getText().clear();
    }

    @NonNull
    public String getText() {
        return edtSearch.getText().toString();
    }

    public void setText(String text) {
        edtSearch.setText(text);
        edtSearch.setSelection(text.length());
    }

    public void setAllCapFilter() {
        Helper.setAllCapFilter(edtSearch);
    }

    public void setEnableEditText(boolean isEnable) {
        edtSearch.setEnabled(isEnable);
    }

    public void focusEdittext() {
        edtSearch.requestFocus();
        edtSearch.setSelection(edtSearch.getText().length());
        Helper.showSoftKeyBoard(context);
    }

    public void setOnSearchEventListener(OnSearchEventListener onSearchEventListener) {
        this.onSearchEventListener = onSearchEventListener;
    }

    private OnClickListener onClickListener;

    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        linearParent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgClear:
                setText("");
                break;
            case R.id.edt_search:
                if (focusEdittext) {
                    Helper.showSoftKeyBoard(context);
                    break;
                }
            case R.id.linearParent:
                if (onClickListener != null) {
                    onClickListener.onClick(SearchBar.this);
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.toString().equals("")) {
            imgClear.setVisibility(GONE);
        } else {
            imgClear.setVisibility(VISIBLE);
        }

        if (onSearchEventListener != null) {
            onSearchEventListener.onTextChange(s.toString());
        }
    }

    public interface OnSearchEventListener {
        void onTextChange(@NonNull String text);

        void onComplete(@NonNull String text);
    }
}
