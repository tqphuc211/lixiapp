package com.opencheck.client.custom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class MerchantMenuRecyclerView extends RecyclerView {

    Context context;

    public MerchantMenuRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public MerchantMenuRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MerchantMenuRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean fling(int velocityX, int velocityY) {

        velocityX *= 0.1;
        velocityY *= 0.1;
        // velocityX *= 0.7; for Horizontal recycler view. comment velocityY line not require for Horizontal Mode.

        return super.fling(velocityX, velocityY);
    }

}