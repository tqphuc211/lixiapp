package com.opencheck.client.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.opencheck.client.R;
import com.opencheck.client.utils.load_image.LixiImage;

public class LixiNavigationBar extends BottomNavigationView implements ImageLoadingListener {
    private Context context;

    public LixiNavigationBar(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public LixiNavigationBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public LixiNavigationBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private ImageView imgIcon;
    private String link;
    private BottomNavigationMenuView menuView;
    private Bitmap bitmap = null;

    private void initView() {
        menuView = (BottomNavigationMenuView) getChildAt(0);
    }

    public void setImage(int index, String link) {
        this.link = link;
        View view = menuView.getChildAt(index);
        BottomNavigationItemView itemView = (BottomNavigationItemView) view;
        imgIcon = itemView.findViewById(android.support.design.R.id.icon);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.shake_button_game_anim);
        imgIcon.setAnimation(animation);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (imgIcon != null) {
            if (link != null && !link.equals("")) {
                LixiImage.with(context)
                        .load(link)
                        .listener(this)
                        .into(imgIcon);
            } else {
                if (bitmap != null) {
                    imgIcon.setImageBitmap(bitmap);
                }
            }
        }
    }

    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        this.bitmap = loadedImage;
        this.link = null;
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {

    }
}
