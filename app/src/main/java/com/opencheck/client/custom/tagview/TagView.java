package com.opencheck.client.custom.tagview;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.R;

import java.util.ArrayList;

public class TagView extends RelativeLayout {

    // Constant
    private final float DEFAULT_LINE_MARGIN = 8;
    private final float DEFAULT_TAG_MARGIN = 15;
    private final float DEFAULT_TAG_TEXT_PADDING_LEFT = 8;
    private final float DEFAULT_TAG_TEXT_PADDING_TOP = 5;
    private final float DEFAULT_TAG_TEXT_PADDING_RIGHT = 8;
    private final float DEFAULT_TAG_TEXT_PADDING_BOTTOM = 5;

    private final float LAYOUT_WIDTH_OFFSET = 2;

    //----------------- separator Tag Item-----------------//
    private final float DEFAULT_TAG_TEXT_SIZE = 14f;
    private final float DEFAULT_TAG_RADIUS = 2;
    private final int DEFAULT_TAG_LAYOUT_COLOR = Color.parseColor("#e6e6e6");
    private final int DEFAULT_TAG_LAYOUT_COLOR_PRESS = Color.parseColor("#88363636");
    private final int DEFAULT_TAG_TEXT_COLOR = Color.parseColor("#000000");

    private int dipToPx(Context c, float dipValue) {
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    /**
     * tag list
     */
    private ArrayList<String> mTags = new ArrayList<>();

    /**
     * System Service
     */
    private LayoutInflater mInflater;
    private ViewTreeObserver mViewTreeObserber;

    /**
     * listeners
     */
    private OnTagClickListener mClickListener;
    private OnTagLongClickListener mTagLongClickListener;

    /**
     * view size param
     */
    private int mWidth;

    /**
     * layout initialize flag
     */
    private boolean mInitialized = false;

    /**
     * custom layout param
     */
    private int lineMargin;
    private int tagMargin;
    private int textPaddingLeft;
    private int textPaddingRight;
    private int textPaddingTop;
    private int textPaddingBottom;
    private float textTagSize = DEFAULT_TAG_TEXT_SIZE;
    private int textColor = DEFAULT_TAG_TEXT_COLOR;
    private int tagBackground = DEFAULT_TAG_LAYOUT_COLOR;


    /**
     * constructor
     *
     * @param ctx
     */
    public TagView(Context ctx) {
        super(ctx, null);
        initialize(ctx, null, 0);
    }

    /**
     * constructor
     *
     * @param ctx
     * @param attrs
     */
    public TagView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        initialize(ctx, attrs, 0);
    }

    /**
     * constructor
     *
     * @param ctx
     * @param attrs
     * @param defStyle
     */
    public TagView(Context ctx, AttributeSet attrs, int defStyle) {
        super(ctx, attrs, defStyle);
        initialize(ctx, attrs, defStyle);
    }

    /**
     * initalize instance
     *
     * @param ctx
     * @param attrs
     * @param defStyle
     */
    private void initialize(Context ctx, AttributeSet attrs, int defStyle) {
        mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewTreeObserber = getViewTreeObserver();
        mViewTreeObserber.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!mInitialized) {
                    mInitialized = true;
                    drawTags();
                }
            }
        });

        // get AttributeSet
        TypedArray typeArray = ctx.obtainStyledAttributes(attrs, R.styleable.TagView, defStyle, defStyle);
        this.lineMargin = (int) typeArray.getDimension(R.styleable.TagView_lineMargin, dipToPx(this.getContext(), DEFAULT_LINE_MARGIN));
        this.tagMargin = (int) typeArray.getDimension(R.styleable.TagView_tagMargin, dipToPx(this.getContext(), DEFAULT_TAG_MARGIN));
        this.textPaddingLeft = (int) typeArray.getDimension(R.styleable.TagView_textPaddingLeft, dipToPx(this.getContext(), DEFAULT_TAG_TEXT_PADDING_LEFT));
        this.textPaddingRight = (int) typeArray.getDimension(R.styleable.TagView_textPaddingRight, dipToPx(this.getContext(), DEFAULT_TAG_TEXT_PADDING_RIGHT));
        this.textPaddingTop = (int) typeArray.getDimension(R.styleable.TagView_textPaddingTop, dipToPx(this.getContext(), DEFAULT_TAG_TEXT_PADDING_TOP));
        this.textPaddingBottom = (int) typeArray.getDimension(R.styleable.TagView_textPaddingBottom, dipToPx(this.getContext(), DEFAULT_TAG_TEXT_PADDING_BOTTOM));
        this.textTagSize = typeArray.getFloat(R.styleable.TagView_textTagSize, DEFAULT_TAG_TEXT_SIZE);
        this.textColor = typeArray.getColor(R.styleable.TagView_textColor, DEFAULT_TAG_TEXT_COLOR);
        this.tagBackground = typeArray.getColor(R.styleable.TagView_tagBackground, DEFAULT_TAG_LAYOUT_COLOR);
        typeArray.recycle();
    }

    /**
     * onSizeChanged
     *
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        if (width <= 0)
            return;
        mWidth = getMeasuredWidth();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawTags();
    }

    /**
     * tag draw
     */
    private void drawTags() {

        if (!mInitialized) {
            return;
        }

        // clear all tag
        removeAllViews();

        // layout padding left & layout padding right
        float total = getPaddingLeft() + getPaddingRight();

        int listIndex = 1;// List Index
        int indexBottom = 1;// The Tag to add below
        int indexHeader = 1;// The header tag of this line
        String tagPre = null;
        for (String item : mTags) {
            final int position = listIndex - 1;
            final String tag = item;
            if (tag == null || tag.trim().equals("")){
                continue;
            }

            // inflate tag layout
            View tagLayout = mInflater.inflate(R.layout.view_tag, null);
            tagLayout.setId(listIndex);

            tagLayout.setBackground(getSelector());

            // tag text
            TextView tagView = (TextView) tagLayout.findViewById(R.id.tv_tag_item_contain);
            tagView.setText(tag);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tagView.getLayoutParams();
            params.setMargins(textPaddingLeft, textPaddingTop, textPaddingRight, textPaddingBottom);
            tagView.setLayoutParams(params);
            tagView.setTextColor(textColor);
            tagView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textTagSize);

            tagLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mClickListener != null) {
                        mClickListener.onTagClick(tag, position);
                    }
                }
            });

            tagLayout.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mTagLongClickListener != null) {
                        mTagLongClickListener.onTagLongClick(tag, position);
                    }
                    return true;
                }
            });

            // tagView padding (left & right)
            LayoutParams tagParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            //add margin of each line
            tagParams.bottomMargin = lineMargin;

            // calculate　of tag layout width
            float tagWidth = tagView.getPaint().measureText(tag) + textPaddingLeft + textPaddingRight + 9;
            if (mWidth <= total + tagWidth + dipToPx(this.getContext(), LAYOUT_WIDTH_OFFSET)) {
                //need to add in new line
                if (tagPre != null) tagParams.addRule(RelativeLayout.BELOW, indexBottom);
                // initialize total param (layout padding left & layout padding right)
                total = getPaddingLeft() + getPaddingRight();
                indexBottom = listIndex;
                indexHeader = listIndex;
            } else {
                //no need to new line
                tagParams.addRule(RelativeLayout.ALIGN_TOP, indexHeader);
                //not header of the line
                if (listIndex != indexHeader) {
                    tagParams.addRule(RelativeLayout.RIGHT_OF, listIndex - 1);
                    tagParams.leftMargin = tagMargin;
                    total += tagMargin;
                }
            }
            total += tagWidth;
            addView(tagLayout, tagParams);
            tagPre = tag;
            listIndex++;
        }

    }

    private Drawable getSelector() {

        StateListDrawable states = new StateListDrawable();
        GradientDrawable gdNormal = new GradientDrawable();
        gdNormal.setColor(tagBackground);
        gdNormal.setCornerRadius(DEFAULT_TAG_RADIUS);
        GradientDrawable gdPress = new GradientDrawable();
        gdPress.setColor(DEFAULT_TAG_LAYOUT_COLOR_PRESS);
        gdPress.setCornerRadius(DEFAULT_TAG_RADIUS);
        states.addState(new int[]{android.R.attr.state_pressed}, gdPress);
        //must add state_pressed first，or state_pressed will not take effect
        states.addState(new int[]{}, gdNormal);
        return states;
    }


    //public methods
    //----------------- separator  -----------------//

    /**
     * @param tag
     */
    public void addTag(String tag) {

        mTags.add(tag);
        drawTags();
    }

    public void addTags(ArrayList<String> tags) {
        if (tags == null) return;
        if (tags.isEmpty())
            drawTags();

        mTags.addAll(tags);
        drawTags();
    }


    /**
     * get tag list
     *
     * @return mTags TagObject List
     */
    public ArrayList<String> getTags() {
        return mTags;
    }

    /**
     * remove tag
     *
     * @param position
     */
    public void remove(int position) {
        if (position < mTags.size()) {
            mTags.remove(position);
            drawTags();
        }
    }

    /**
     * remove all views
     */
    public void removeAll() {
        mTags.clear(); //clear all of tags
        removeAllViews();
    }

    public int getLineMargin() {
        return lineMargin;
    }

    public void setLineMargin(float lineMargin) {
        this.lineMargin = dipToPx(getContext(), lineMargin);
    }

    public int getTagMargin() {
        return tagMargin;
    }

    public void setTagMargin(float tagMargin) {
        this.tagMargin = dipToPx(getContext(), tagMargin);
    }

    public int getTextPaddingLeft() {
        return textPaddingLeft;
    }

    public void setTextPaddingLeft(float textPaddingLeft) {
        this.textPaddingLeft = dipToPx(getContext(), textPaddingLeft);
    }

    public int getTextPaddingRight() {
        return textPaddingRight;
    }

    public void setTextPaddingRight(float textPaddingRight) {
        this.textPaddingRight = dipToPx(getContext(), textPaddingRight);
    }

    public int getTextPaddingTop() {
        return textPaddingTop;
    }

    public void setTextPaddingTop(float textPaddingTop) {
        this.textPaddingTop = dipToPx(getContext(), textPaddingTop);
    }

    public int gettextPaddingBottom() {
        return textPaddingBottom;
    }

    public void settextPaddingBottom(float textPaddingBottom) {
        this.textPaddingBottom = dipToPx(getContext(), textPaddingBottom);
    }


    /**
     * setter for OnTagLongClickListener
     *
     * @param longClickListener
     */
    public void setOnTagLongClickListener(OnTagLongClickListener longClickListener) {
        mTagLongClickListener = longClickListener;
    }

    /**
     * setter for OnTagSelectListener
     *
     * @param clickListener
     */
    public void setOnTagClickListener(OnTagClickListener clickListener) {
        mClickListener = clickListener;
    }

    public interface OnTagClickListener {
        void onTagClick(String tag, int position);
    }

    public interface OnTagLongClickListener {
        void onTagLongClick(String tag, int position);
    }
}
