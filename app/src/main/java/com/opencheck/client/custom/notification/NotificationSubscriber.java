package com.opencheck.client.custom.notification;

public interface NotificationSubscriber {
    void subscribe(OnCountNotification onCountNotification);
    void unSubscribe(OnCountNotification onCountNotification);
    void notifyObserve();
}
