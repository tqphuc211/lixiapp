package com.opencheck.client.custom;

import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;

import java.util.ArrayList;

public class MyTagView implements View.OnClickListener {

    LixiActivity activity;
    LinearLayout parentView;
    ArrayList<String> listTag;
    ArrayList<TextView> listTextView;
    ArrayList<LayoutListener> listListener;
    int exactHeight = 0;
    LinearLayout line;
    int crbreak = 100;
    int lastNeedBreak = 0;

    public MyTagView(LixiActivity activity, LinearLayout parent, ArrayList<String> listTag) {
        this.activity = activity;
        this.parentView = parent;
        this.listTag = listTag;

        listTextView = new ArrayList<>();
        listListener = new ArrayList<>();

        line = addLine();

        for (int i = 0; i < listTag.size(); i++) {
            final TextView textView = new TextView(activity);
            textView.setText(listTag.get(i));
            int margin = (int) activity.getResources().getDimension(R.dimen.value_8);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(margin, 0, 0, 0);
            textView.setLayoutParams(layoutParams);
            int paddingv = (int) activity.getResources().getDimension(R.dimen.value_5);
            int paddingh = (int) activity.getResources().getDimension(R.dimen.value_8);
            textView.setPaddingRelative(paddingh, paddingv, paddingh, paddingv);
            textView.setBackgroundColor(0x26868686);

            listListener.add(new LayoutListener(textView, i));
            listTextView.add(textView);
            textView.setOnClickListener(this);
            line.addView(textView);
        }
    }

    public LinearLayout addLine() {
        LinearLayout line = new LinearLayout(activity);
        line.setBackgroundColor(0xffffffff);
        line.setOrientation(LinearLayout.HORIZONTAL);
        int margin = (int) activity.getResources().getDimension(R.dimen.value_6);
        LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LLParams.setMargins(0, margin, 0, 0);
        line.setLayoutParams(LLParams);

        parentView.addView(line);
        return line;
    }

    public void handleOutRange(TextView tv, int pos) {
        crbreak = pos;
        for (int i = pos; i < listTextView.size(); i++) {
            line.removeView(listTextView.get(i));
            listListener.get(pos).removeListener();
        }
        line = addLine();
        for (int i = pos; i < listTextView.size(); i++) {
            line.addView(listTextView.get(i));
            listListener.get(pos).addlistener();
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof TextView)
            if (eventListener != null)
                eventListener.onClick((TextView) v);

    }

    public class LayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        public TextView tv;
        private int pos;
        private ViewTreeObserver vto;

        public LayoutListener(TextView tv, int pos) {
            this.tv = tv;
            this.pos = pos;
            vto = tv.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(this);
        }

        @Override
        public void onGlobalLayout() {
            if (tv.getMeasuredHeight() > 0) {
                if (exactHeight == 0)
                    exactHeight = tv.getMeasuredHeight();

//                if (vto.isAlive())
//                    vto.removeOnGlobalLayoutListener(this);
                if (tv.getMeasuredHeight() > exactHeight) {
//                    Log.d("mview", ">break>" + pos + "   >>crBreak>" + crbreak);
                    if (crbreak >= pos || lastNeedBreak > pos || (pos == listTag.size() - 1 && pos == lastNeedBreak)) {
                        handleOutRange(tv, pos);
                    }
                    lastNeedBreak = pos;
                }
            }
        }

        public void removeListener() {
            if (vto.isAlive())
                vto.removeOnGlobalLayoutListener(this);
        }

        public void addlistener() {
            vto = tv.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(this);
        }
    }

    public interface IMytagViewListener {
        void onClick(TextView tv);
    }

    public void setEventListener(IMytagViewListener eventListener) {
        this.eventListener = eventListener;
    }

    private IMytagViewListener eventListener;

}
