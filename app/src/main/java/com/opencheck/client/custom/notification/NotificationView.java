package com.opencheck.client.custom.notification;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.ViewNotificationBinding;
import com.opencheck.client.home.notification.NotificationListingActivity;

public class NotificationView extends RelativeLayout implements OnCountNotification {
    private Context context;
    ViewNotificationBinding mBinding;

    public NotificationView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public NotificationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public NotificationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    public NotificationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        initView();
    }

    private CircleImageView imgCount;
    private TextView txtCount;

    private void initView(){
        mBinding = ViewNotificationBinding.inflate(LayoutInflater.from(getContext()), this, true);
        imgCount = (CircleImageView) findViewById(R.id.img_notify_count);
        txtCount = (TextView) findViewById(R.id.tv_notify_count);

        NotiViewManager.getInstance().subscribe(this);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotificationListingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                context.startActivity(intent);
            }
        });
    }

    public void setNotifyCount(int countNotRead) {
        if (countNotRead <= 0) {
            imgCount.setVisibility(View.GONE);
            txtCount.setVisibility(View.GONE);
        } else {
            imgCount.setVisibility(View.VISIBLE);
            txtCount.setVisibility(View.VISIBLE);
            txtCount.setText(String.valueOf(countNotRead));
        }
    }

    @Override
    public void onCount(int total) {
        setNotifyCount(total);
    }

    @Override
    protected void onDetachedFromWindow() {
        NotiViewManager.getInstance().unSubscribe(this);
        super.onDetachedFromWindow();
    }
}
