package com.opencheck.client.custom.notification;

public interface OnCountNotification {
    void onCount(int total);
}
