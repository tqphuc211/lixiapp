package com.opencheck.client.custom;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewMessengerButtonBinding;
import com.opencheck.client.utils.LanguageBinding;

public class MessengerButton extends RelativeLayout {
    private Context context;

    public MessengerButton(Context context) {
        super(context);
        initView(context);
    }

    public MessengerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MessengerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public MessengerButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private ViewMessengerButtonBinding mBinding;

    private void initView(final Context context) {
        this.context = context;
        mBinding = ViewMessengerButtonBinding.inflate(LayoutInflater.from(getContext()), this, true);

        mBinding.rlMessenger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                } catch (Exception e) {
                    Toast.makeText(context, LanguageBinding.getString(R.string.profile_setting_notify_error, context), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
