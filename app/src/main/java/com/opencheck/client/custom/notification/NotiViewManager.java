package com.opencheck.client.custom.notification;

import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;

import java.util.LinkedList;
import java.util.List;

public class NotiViewManager implements NotificationSubscriber {
    private static volatile NotiViewManager instantce;

    public static NotiViewManager getInstance() {
        synchronized (NotiViewManager.class) {
            if (instantce == null) {
                instantce = new NotiViewManager();
            }
        }
        return instantce;
    }

    private NotiViewManager() {
        listObserver = new LinkedList<>();
    }

    private List<OnCountNotification> listObserver;
    private int totalNoti = 0;

    @Override
    public void subscribe(OnCountNotification onCountNotification) {
        if (!listObserver.contains(onCountNotification)) {
            listObserver.add(onCountNotification);
            onCountNotification.onCount(totalNoti);
        }
    }

    @Override
    public void unSubscribe(OnCountNotification onCountNotification) {
        if (!listObserver.contains(onCountNotification)) {
            listObserver.remove(onCountNotification);
        }
    }

    @Override
    public void notifyObserve() {
        if (listObserver != null) {
            for (OnCountNotification onCount : listObserver) {
                if (onCount != null) {
                    onCount.onCount(totalNoti);
                }
            }
        }
    }

    public void hasNotify() {
        DataLoader.getNotificationCount(new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    try {
                        totalNoti = (int) object;
                        notifyObserve();
                    } catch (Exception e) {
                    }
                } else {
                    totalNoti = 0;
                    notifyObserve();
                }
            }
        });
    }
}
