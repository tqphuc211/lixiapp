package com.opencheck.client.firebase;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class FCMInstantIDService extends FirebaseInstanceIdService {
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        getToken();
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
//        Intent intent = new Intent(this, RegistrationIntentService.class);
//        startService(intent);
    }

    public void getToken() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Helper.showLog("Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        Helper.showLog("Token push: " + token);
        // Add custom implementation, as needed.
        new AppPreferenceHelper(LixiApplication.getInstance()).savePushToken(token);
    }

    // [END refresh_token]
}
