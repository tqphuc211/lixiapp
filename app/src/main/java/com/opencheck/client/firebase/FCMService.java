package com.opencheck.client.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.notification.NotiViewManager;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.models.MerchantActive;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class FCMService extends FirebaseMessagingService {
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    public static MerchantActive merchantActive;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        String from = remoteMessage.getFrom();
        Log.d(ConstantValue.LOG_INFO_TAG, "Push from firebase: " + remoteMessage.getData());

        try {
            Map<String, String> dataMap = remoteMessage.getData();

            if (dataMap != null) {
                JSONObject jsonData = new JSONObject();
                for (Map.Entry<String, String> entry : dataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    jsonData.put(key, value);
                }
                generateNotificationSystem(getApplicationContext(), jsonData);

                if (LixiApplication.isAppRunning){
                    NotiViewManager.getInstance().hasNotify();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void generateNotificationSystem(Context context, JSONObject jsonData) {

        Intent notificationIntent;
        Log.d(ConstantValue.LOG_INFO_TAG, "Json_Data: " + jsonData);

        NotificationModel notificationModel = (new Gson()).fromJson(jsonData.toString(), NotificationModel.class);
        notificationModel.setTotal("0");

        if (notificationModel.getType().equals(NotificationModel.VOUCHER_DETAIL_ACTIVE)) {
            //if(!new AppPreferencesLib(context).getIsBoughtFragment()) {
            if (merchantActive != null) {
                merchantActive.getActive().postValue(true);
            }
            Log.d("mapi", "success");
            if (ConstantValue.USER_MULTI_ACTIVE)
                notificationIntent = new Intent(context, EVoucherAccountActivity.class);
            else
                notificationIntent = new Intent(context, ActiveCodeActivity.class);
            Bundle data = new Bundle();
            data.putLong(ConstantValue.EVOUCHER_ID, Long.parseLong(notificationModel.getObject_id()));
            notificationIntent.putExtras(data);
        }

        notificationIntent = new Intent(context, PendingPushActivity.class);
        notificationIntent.putExtra("Json_Data", jsonData.toString());

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, (int) (Math.random() * 100),
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_ONE_SHOT);

        initNotification(context, resultPendingIntent, notificationModel);
    }

    private static void initNotification(Context context, PendingIntent resultPendingIntent, NotificationModel notificationModel) {
        int mNotificationId = Integer.parseInt(Helper.getTimestamp());
        int icon = R.drawable.icon_app_trans;
        String CHANNEL_ID = "my_channel_01";

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID);
        mBuilder.setSmallIcon(icon).setTicker(notificationModel.getTitle()).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(notificationModel.getTitle())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationModel.getBody()))
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(notificationModel.getBody());

        Notification notification;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // The id of the channel.
            CharSequence name = "ANDROID_O";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder.setChannelId(CHANNEL_ID);
        }

        notification = mBuilder.build();
        notificationManager.notify(mNotificationId, notification);

        if (LixiApplication.getInstance() != null && LixiApplication.isAppRunning) {
//            Helper.saveObJectToFile(notificationModel, LixiApplication.getInstance(), ConstantValue.NOTIFY_MODEL_FILE);

            if (notificationModel.getType().indexOf("order") >= 0 || notificationModel.getType().indexOf("payment") >= 0) {
                Intent intent = new Intent(ConstantValue.PUSH_OC_RECEIVE);
                context.sendBroadcast(intent);

                Intent intent2 = new Intent(ConstantValue.PUSH_UPDATE_NOTIFICATION);
                context.sendBroadcast(intent2);
            } else if (notificationModel.getType().equals(NotificationModel.VOUCHER_DETAIL_ACTIVE)) {
                Log.d("mapi", "broadcast push");
                Intent intent = new Intent(ConstantValue.PUSH_EVOUCHER_ACTIVE);
                intent.putExtra("ev_id", notificationModel.getObject_id());
                context.sendBroadcast(intent);
            }
        }
    }
}
