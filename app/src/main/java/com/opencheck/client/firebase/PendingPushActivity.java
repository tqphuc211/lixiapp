package com.opencheck.client.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class PendingPushActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("mPush", "pending activity");
//        Utilities.showLog("Action: " + getIntent().getStringExtra("click_action"));
//        Utilities.showLog("Data: " + getIntent().getStringExtra("body"));
//        Utilities.showLog("Data String: "+getIntent().getExtras().toString());

        try {
            NotificationModel notificationModel = new NotificationModel();
            Helper.showLog(getIntent().getExtras().toString());
            if (getIntent().getExtras().getString("Json_Data") != null) {
                String jsonData = getIntent().getExtras().getString("Json_Data");
                Helper.showLog("Json data: " + jsonData);
                notificationModel = (new Gson()).fromJson(jsonData, NotificationModel.class);
                notificationModel.setTotal("0");
            } else {
                Helper.showLog("Json from intent: " + getIntent().getStringExtra("body"));
                notificationModel = createModelFromBundle(getIntent());
            }

            Helper.saveObJectToFile(notificationModel, this, ConstantValue.NOTIFY_MODEL_FILE);
            Helper.showLog("OK");
            if (HomeActivity.getInstance() != null && LixiApplication.isAppRunning) {
                Helper.showLog("OK BEDE");
                if (notificationModel.getType().equals(NotificationModel.ADMIN_SURVEY)
                        || notificationModel.getType().equals(NotificationModel.SURVEY))
                    sendMessage(this, ConstantValue.PUSH_SURVEY, true);
                else
                    sendMessage(this, ConstantValue.PUSH_SYSTEM_INTENT, true);
            } else {
                Helper.showLog("OK CONDE");
                if (!notificationModel.getType().equals(NotificationModel.ADMIN_SURVEY)
                        && !notificationModel.getType().equals(NotificationModel.SURVEY))
                    new AppPreferenceHelper(this).setIsPushSystem(true);
                Intent intentPeding = new Intent(this, FirstLaunchActivity.class);
                intentPeding.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentPeding);
                Helper.showLog("STARTO!");
            }
            finish();
        } catch (Exception e) {
            Helper.showLog("SHIT");
            e.printStackTrace();
            Helper.showLog("Error notify click: " + e.getLocalizedMessage());
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void sendMessage(Context cont, String Action, Boolean isSuccess) {
        Intent intent = new Intent(Action);
        intent.putExtra(ConstantValue.IS_SUCCESS, isSuccess);
        cont.sendBroadcast(intent);
    }

    private NotificationModel createModelFromBundle(Intent intent) {
        NotificationModel notificationModel = new NotificationModel();

        notificationModel.setId(intent.getStringExtra("id"));
        notificationModel.setBody(intent.getStringExtra("body"));
        notificationModel.setUrl(intent.getStringExtra("url"));
        notificationModel.setDate_created(intent.getStringExtra("date_created"));
        notificationModel.setDate_read(intent.getStringExtra("date_read"));
        notificationModel.setIs_read(intent.getStringExtra("is_read"));
        notificationModel.setObject_id(intent.getStringExtra("object_id"));
        notificationModel.setDetail_id(intent.getStringExtra("detail_id"));
        notificationModel.setTitle(intent.getStringExtra("title"));
        notificationModel.setTo_partner_id(intent.getStringExtra("to_partner_id"));
        notificationModel.setClick_action(intent.getStringExtra("click_action"));
        notificationModel.setImage(intent.getStringExtra("image"));
        notificationModel.setType(intent.getStringExtra("type"));
//        Log.d("mapi", (new Gson()).toJson(notificationModel));
        return notificationModel;

    }
}
