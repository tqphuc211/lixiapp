package com.opencheck.client.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.opencheck.client.utils.AppPreferenceHelper;

/**
 * Created by qtp on 6/24/17.
 */

public class RefererReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // Pass the intent to other receivers.
        // When you're done, pass the intent to the Google Analytics receiver.
        String cData = intent.getStringExtra("referrer");
        if (cData != null) {
            Log.d("GAv4", cData);
            (new AppPreferenceHelper(context)).setCampaignData(cData);
        }
    }
}