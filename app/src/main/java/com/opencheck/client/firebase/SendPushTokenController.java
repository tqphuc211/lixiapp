package com.opencheck.client.firebase;

import android.os.Build;
import android.os.Handler;
import android.view.Display;

import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class SendPushTokenController {

    private LixiActivity activity;
    private String tokenFCM = "";
    private Boolean isLogout = false;

    public SendPushTokenController(LixiActivity _activity, String _tokenFCM, Boolean _isLogout) {
        this.activity = _activity;
        this.tokenFCM = _tokenFCM;
        this.isLogout = _isLogout;
        Helper.showLog(_tokenFCM);

        if (isLogout) {
            logOutToken();
            return;
        }
        if (LoginHelper.isLoggedIn(activity)) {
            if (!new AppPreferenceHelper(activity).getPushToken().equalsIgnoreCase("")) {
                sendToken(1);
                return;
            }
        }

        sendToken(0);
    }

    private void logOutToken() {
        DataLoader.logoutPushToken(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    int i = 0;
                    i++;
                } else {
                    int i = 0;
                    i++;
                }
            }
        });
    }

    private int tryAgain = 0;

    private void sendToken(final long userId) {
        try {
            Display display = activity.getWindowManager().getDefaultDisplay();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("device_id", Helper.getDeviceID(activity));
            jsonObject.put("pushtracker_id", tokenFCM);
            jsonObject.put("screen_width", display.getWidth());
            jsonObject.put("screen_height", display.getHeight());

            if (LoginHelper.isLoggedIn(activity) && UserModel.getInstance() != null) {
                jsonObject.put("name", UserModel.getInstance().getFullname());
            } else {
                jsonObject.put("name", "");
            }

            String str = Helper.getDeviceBrand() + "_" + Build.DEVICE + "_" + Build.MODEL + "_" + Build.PRODUCT;
            try {
                str = BuildConfig.VERSION_CODE + "__" + str;
            } catch (Exception ex) {
            }
            jsonObject.put("brand", str);
//           jsonObject.put("device_os", "ANDROID " + activity.getResources().getString(R.string.version_name));
            jsonObject.put("device_os", "ANDROID_" + Build.VERSION.SDK_INT);

            try {
                AppPreferenceHelper appPreferenceHelper = AppPreferenceHelper.getInstance();
                DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
                if (deliAddressModel != null) {
                    jsonObject.put("lat", deliAddressModel.getLat());
                    jsonObject.put("long", deliAddressModel.getLng());
                    jsonObject.put("region", deliAddressModel.getRegion());
                }
            } catch (Exception ex) {
                jsonObject.put("lat", 0);
                jsonObject.put("long", 0);
                jsonObject.put("region", 0);
            }

            new AppPreferenceHelper(activity).setUpdateTokenInterval(System.currentTimeMillis());

            if (userId > 0) {
                DataLoader.sendPushToken(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        Helper.showLog(object.toString());
                        if (isSuccess) {
                            new AppPreferenceHelper(activity).setIsSendPush(true);
                            //OK
                        } else {
                            new AppPreferenceHelper(activity).setIsSendPush(false);
                            if (tokenFCM.length() > 0 && tryAgain < 6) {
                                tryAgain++;
                                Handler handler = new Handler();
                                Runnable retry = new Runnable() {
                                    @Override
                                    public void run() {
                                        sendToken(userId);
                                    }
                                };
                                handler.postDelayed(retry, 1000);
                            }
                        }
                    }
                }, jsonObject);
            } else {
                DataLoader.sendPushTokenGuest(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        Helper.showLog(object.toString());
                        if (isSuccess) {
                            new AppPreferenceHelper(activity).setIsSendPush(true);
                            //OK
                        } else {
                            new AppPreferenceHelper(activity).setIsSendPush(false);
                            if (tokenFCM.length() > 0 && tryAgain < 6) {
                                tryAgain++;
                                Handler handler = new Handler();
                                Runnable retry = new Runnable() {
                                    @Override
                                    public void run() {
                                        sendToken(userId);
                                    }
                                };
                                handler.postDelayed(retry, 1000);
                            }
                        }
                    }
                }, jsonObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
