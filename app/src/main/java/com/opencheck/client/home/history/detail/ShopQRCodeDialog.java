package com.opencheck.client.home.history.detail;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.BoughtVoucherQrDialogBinding;
import com.opencheck.client.home.account.GeneralQRCodeTask;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class ShopQRCodeDialog extends LixiDialog {
    public ShopQRCodeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ImageView iv_qr;
    private TextView tv_code;
    private TextView tv_title;
    private RelativeLayout rl_dismiss;

    private String title = "";
    private String code;
    private BoughtVoucherQrDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = BoughtVoucherQrDialogBinding.inflate(LayoutInflater.from(getContext()),
                null, false);
        setContentView(mBinding.getRoot());
        title = LanguageBinding.getString(R.string.transaction_code, activity);
        findViews();
        initDialogEvent();
    }


    private void findViews() {
        iv_qr = (ImageView) findViewById(R.id.iv_qr);
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_title = (TextView) findViewById(R.id.tv_title);
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);

        rl_dismiss.setOnClickListener(this);
    }

    public void initDialogEvent() {

    }

    public void setData(String _title, String code) {
        title = _title;
        this.code = code;
        setViewData();
    }

    public void setViewData() {
        new GeneralQRCodeTask(activity, code, iv_qr, 0xFF667991, new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (pos == 1) {
                    iv_qr.setImageBitmap((Bitmap) object);
                }
            }
        });
        String substr = code;
        tv_code.setText(substr);
        tv_title.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_dismiss:
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
