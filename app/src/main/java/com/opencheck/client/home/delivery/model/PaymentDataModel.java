package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class PaymentDataModel implements Serializable {
    private String pay_url;
    private String redirect_pattern;
    private Object sdk_param;
    private String submit_url;
    private String offline_code;
    private String payment_uuid;
    private String order_uuid;
    private String state;
    private String checksum;

    public String getPay_url() {
        return pay_url;
    }

    public void setPay_url(String pay_url) {
        this.pay_url = pay_url;
    }

    public String getRedirect_pattern() {
        return redirect_pattern;
    }

    public void setRedirect_pattern(String redirect_pattern) {
        this.redirect_pattern = redirect_pattern;
    }

    public Object getSdk_param() {
        return sdk_param;
    }

    public String getSubmit_url() {
        return submit_url;
    }

    public void setSubmit_url(String submit_url) {
        this.submit_url = submit_url;
    }

    public String getOffline_code() {
        return offline_code;
    }

    public void setOffline_code(String offline_code) {
        this.offline_code = offline_code;
    }

    public String getPayment_uuid() {
        return payment_uuid;
    }

    public void setPayment_uuid(String payment_uuid) {
        this.payment_uuid = payment_uuid;
    }

    public String getOrder_uuid() {
        return order_uuid;
    }

    public void setOrder_uuid(String order_uuid) {
        this.order_uuid = order_uuid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
}
