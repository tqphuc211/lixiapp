package com.opencheck.client.home.setting.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebViewClient;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogLearnMoreBinding;

public class LearnMoreDialog extends LixiDialog {

    public LearnMoreDialog(@NonNull LixiActivity _activity) {
        super(_activity, true, true, false);
    }

    private DialogLearnMoreBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogLearnMoreBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        mBinding.imgBack.setOnClickListener(this);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void setData(String link){
        mBinding.webView.setWebViewClient(new WebViewClient());
        mBinding.webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        mBinding.webView.getSettings().setLoadsImagesAutomatically(true);
        mBinding.webView.loadUrl(link);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                cancel();
                break;
        }
    }
}
