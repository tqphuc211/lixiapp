package com.opencheck.client.home.merchant;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.MerchantVoucherFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class MerchantVoucherFragment extends LixiFragment {

    //region Layout Variable
    private RecyclerView rv_new_voucher;
    private SwipeRefreshLayout swipeRefreshLayout;
    //endregion

    //region Logic Variable
    private ArrayList<LixiShopEvoucherModel> list_voucher = new ArrayList<>();
    private final int RECORD_PER_PAGE = DataLoader.record_per_page;
    private int page = 1;
    private Boolean isLoading = false;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager layoutManager;
    private boolean isOutOfVoucherPage = false;
    //endregion

    //region Adapter, Controller
    private AdapterVoucherDiscoveryFragment adapterVoucher;
    //endregion

    private MerchantVoucherFragmentBinding mBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = MerchantVoucherFragmentBinding.inflate(inflater, container, false);
        initView(mBinding.getRoot());

        return mBinding.getRoot();
    }

    private void initView(View view) {
        rv_new_voucher = (RecyclerView) view.findViewById(R.id.rv_new_voucher);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!list_voucher.isEmpty())
                {
                    list_voucher.clear();
                    adapterVoucher.notifyDataSetChanged();
                }
                isLoading = false;
                page = 1;
                list_voucher.clear();
                isOutOfVoucherPage = false;
                startLoadData();
            }
        });

        rv_new_voucher.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);if(rv_new_voucher.computeVerticalScrollOffset() > 0){

                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if(!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2){
                        Helper.showLog("load more");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startLoadData();
                            }
                        }, 1000);

                        isLoading = true;
                    }
                }
            }
        });

        startLoadData();
    }

    private void startLoadData() {
        DataLoader.getMerchantEvoucherNewList(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    handleLoadDataSuccess(object);
                } else {
                    isLoading = true;
                }
            }
        }, page);
    }

    private void setOutOfVoucherPage(List<LixiShopEvoucherModel> list) {
        if (list.size() < RECORD_PER_PAGE) {
            isOutOfVoucherPage = true;
        }
    }

    private void handleLoadDataSuccess(Object objects) {
        if (page == 1){
            handleIsFirstPage(objects);
        }else{
            handleLoadMore(objects);
        }
        isLoading = false;
        page++;
    }

    private void handleLoadMore(Object objects) {
        ArrayList<LixiShopEvoucherModel> list = (ArrayList<LixiShopEvoucherModel>) objects;
        if(list == null || list_voucher == null || list.size() == 0)
            return;
        setOutOfVoucherPage(list);
        list_voucher.addAll(list);
        adapterVoucher.notifyDataSetChanged();
    }

    private void handleIsFirstPage(Object objects) {
        list_voucher = (ArrayList<LixiShopEvoucherModel>) objects;
        if(list_voucher == null || list_voucher.size() == 0)
            return;
        setOutOfVoucherPage(list_voucher);
        showData();
    }

    private void showData() {
        adapterVoucher = new AdapterVoucherDiscoveryFragment(activity,this, list_voucher, false, activity.widthScreen);
        rv_new_voucher.setAdapter(adapterVoucher);
        layoutManager = new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false);
        rv_new_voucher.setLayoutManager(layoutManager);
    }

    @Override
    public void onClick(View view) {

    }
}
