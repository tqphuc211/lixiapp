package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PauseServiceDialogBinding;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.OpenTimeModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PauseServiceDialog extends LixiDialog {
    public PauseServiceDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    public final static String LIXI_OFF = "LIXI_OFF";
    public final static String STORE_STOP_SERVICE = "STORE_STOP_SERVICE";
    public final static String STORE_OUT_OF_TIME = "STORE_OUT_OF_TIME";
    public final static String LIXI_OUT_OF_TIME = "LIXI_OUT_OF_TIME";
    public final static String FAR_AWAY = "FAR_AWAY";

    private Long orderTime = null, openTime = null;
    private PauseServiceDialogBinding mBinding;
    private boolean isAfterTomorrow = false;

    private String typeStore;
    private StoreOfCategoryModel mStore;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.pause_service_dialog, null, false);
        setContentView(mBinding.getRoot());

        mBinding.tvSchedule.setOnClickListener(this);
        mBinding.tvLocation.setOnClickListener(this);
    }

    public void setData(StoreOfCategoryModel store, String type) {
        this.typeStore = type;
        this.id = store.getId();
        this.mStore = store;
        orderTime = getSuitableOrderTime(store);

        switch (type) {
            case LIXI_OFF:
                typeStore = TrackingConstant.StoreCheck.LIXI_PAUSE;
                mBinding.tvTitle.setText(LanguageBinding.getString(R.string.pause_dialog_maintaining_system, activity));
                mBinding.tvMessage.setText(LanguageBinding.getString(R.string.pause_dialog_maintaining_system_message, activity));
                mBinding.tvSchedule.setText(LanguageBinding.getString(R.string.exit, activity));
                mBinding.tvDefaultSchedule.setVisibility(View.GONE);
                mBinding.vDownLine.setVisibility(View.GONE);
                mBinding.tvLocation.setVisibility(View.GONE);
                break;
            case STORE_STOP_SERVICE:
                typeStore = TrackingConstant.StoreCheck.STORE_PAUSE;
                mBinding.tvTitle.setText(LanguageBinding.getString(R.string.pause_dialog_stop, activity));
                mBinding.tvLocation.setText(LanguageBinding.getString(R.string.pause_dialog_other_place, activity));
                mBinding.tvMessage.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_reason, activity) +
                        " " + store.getPause_reason_message()));

                if (store.isAllow_order_in_pause_time() && orderTime != null) {
                    mBinding.tvSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_deliver, activity) + " "
                            + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(store.getDelay_after_open() * 60 * 1000 + orderTime))));
                } else {
                    mBinding.tvSchedule.setText(LanguageBinding.getString(R.string.pause_dialog_discover_food, activity));
                }

                if (store.getEnd_pause_time() != null) {
                    mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_again_service_time, activity) +
                            "<br><b>" + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(store.getEnd_pause_time())) + "</b>"));
                } else {
                    mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) + "<br><b>----</b>"));
                }
                break;
            case STORE_OUT_OF_TIME:
                typeStore = TrackingConstant.StoreCheck.STORE_OUT_SERVICE;
                mBinding.tvTitle.setText(LanguageBinding.getString(R.string.pause_dialog_out_of_store_time, activity));
                mBinding.tvLocation.setText(LanguageBinding.getString(R.string.pause_dialog_other_place, activity));
                mBinding.tvSchedule.setText(LanguageBinding.getString(R.string.pause_dialog_discover_food, activity));

                if (isAfterTomorrow) {
                    mBinding.tvDefaultSchedule.setVisibility(View.GONE);
                    Long guessTime = getTheDayAfterTomorrowActiveTime(store);
                    if (guessTime != null)
                        mBinding.tvMessage.setText(String.format(LanguageBinding.getString(R.string.pause_dialog_out_of_store_time_message_2, activity),
                                Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(guessTime))));
                    else {
                        try {
                            Crashlytics.getInstance().crash();
                        } catch (Exception e) {
                            Crashlytics.logException(new Throwable("Open time error:" + store.getId()));
                        }
                    }
                } else {
                    mBinding.tvMessage.setText(LanguageBinding.getString(R.string.pause_dialog_out_of_store_time_message, activity));

//                    if (getSuitableOpenTime(store) != null) {
//                        mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) +
//                                "<br><b>" + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(getSuitableOpenTime(store))) + "</b>"));
//                    } else {
//                        mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) +
//                                "<br><b>----</b>"));
//                    }

                    if (openTime != null) {
                        mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) +
                                "<br><b>" + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(openTime)) + "</b>"));
                    } else {
                        mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) +
                                "<br><b>----</b>"));
                        try {
                            Crashlytics.getInstance().crash();
                        } catch (Exception e) {
                            Crashlytics.logException(new Throwable("Open time error:" + store.getId()));
                        }
                    }

                    if (orderTime != null) {
//                        mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_open_time, activity) +
//                                "<br><b>" + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(orderTime)) + "</b>"));

                        mBinding.tvSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_deliver, activity) + " "
                                + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(store.getDelay_after_open() * 60 * 1000 + orderTime))));
                    }
                }
                break;
            case LIXI_OUT_OF_TIME:
                typeStore = TrackingConstant.StoreCheck.LIXI_OUT_SERVICE;
                String deli_working_time = DeliConfigModel.DELI_WORKING_TIME_DEFAULT;
                DeliConfigModel deliWorkingTime = Helper.getConfigByKey(activity, DeliConfigModel.DELI_WORKING_TIME_KEY);
                if (deliWorkingTime != null) {
                    deli_working_time = deliWorkingTime.getValue();
                }
                mBinding.tvTitle.setText(LanguageBinding.getString(R.string.pause_dialog_out_of_lixi_time, activity));
                mBinding.tvMessage.setText(LanguageBinding.getString(R.string.pause_dialog_out_of_lixi_time_message, activity));
                mBinding.tvLocation.setText(LanguageBinding.getString(R.string.pause_dialog_other_place, activity));
                mBinding.tvDefaultSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_default_activity_time, activity)
                        + "<br><b>" + deli_working_time.replace("-", " đến") + "</b>"));
                if (orderTime != null) {
                    mBinding.tvSchedule.setText(Html.fromHtml(LanguageBinding.getString(R.string.pause_dialog_deliver, activity) + " "
                            + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(store.getDelay_after_open() * 60 * 1000 + orderTime))));
                } else
                    mBinding.tvSchedule.setText(LanguageBinding.getString(R.string.pause_dialog_discover_food, activity));
                break;
            case FAR_AWAY:
                typeStore = TrackingConstant.StoreCheck.LONG_DISTANCE;
                DeliConfigModel distanceModel = Helper.getConfigByKey(activity, DeliConfigModel.DELI_LIMIT_DISTANCE);
                String distance = "8";
                if (distanceModel != null)
                    distance = String.valueOf(Long.parseLong(distanceModel.getValue()) / 1000);
                mBinding.tvTitle.setText(LanguageBinding.getString(R.string.pause_dialog_far_away, activity));
                mBinding.tvMessage.setText(String.format(LanguageBinding.getString(R.string.pause_dialog_far_away_message, activity), distance));
                mBinding.tvSchedule.setText(LanguageBinding.getString(R.string.pause_dialog_discover_food, activity));
                mBinding.tvLocation.setText(LanguageBinding.getString(R.string.pause_dialog_other_place, activity));
                mBinding.tvDefaultSchedule.setText(Html.fromHtml("<b>" + LanguageBinding.getString(R.string.info_update_address, activity) + ":</b> " + store.getAddress()));
                break;
        }

        GlobalTracking.trackStoreImpression(store.getId(), typeStore);
    }

    private OpenTimeModel getDeliWorkingTime() {
        String deli_working_time = DeliConfigModel.DELI_WORKING_TIME_DEFAULT;
        DeliConfigModel deliWorkingTime = Helper.getConfigByKey(activity, DeliConfigModel.DELI_WORKING_TIME_KEY);
        if (deliWorkingTime != null) {
            deli_working_time = deliWorkingTime.getValue();
        }

        Calendar deliOpenTime = Calendar.getInstance();
        deliOpenTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[0]));
        deliOpenTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[1]));

        Calendar deliCloseTime = Calendar.getInstance();
        deliCloseTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[0]));
        deliCloseTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[1]));
        return new OpenTimeModel(deliOpenTime.get(Calendar.DAY_OF_WEEK), deliOpenTime.getTimeInMillis(), deliCloseTime.getTimeInMillis());
    }

    private Long getTheBestOpenTime(StoreOfCategoryModel store) {
        Calendar realDateDeliveryCal = Calendar.getInstance();
        Calendar currentDateCal = Calendar.getInstance();
        ArrayList<OpenTimeModel> listOpenTimeToday = new ArrayList<>();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);

        //Lấy danh sách mở cửa ngày hôm nay
        for (int i = 0; i < store.getList_open_time().size(); i++) {
            if (store.getList_open_time().get(i).getWeekday() == mDayOfWeek) {
                listOpenTimeToday.add(store.getList_open_time().get(i));
            }
        }

        boolean isToday = true;
        Calendar tempDateDeliveryCal = Calendar.getInstance();

        if (listOpenTimeToday.size() == 0) {
            isToday = false;
        } else {
            // Lấy khung giờ phù hợp
            for (int i = 0; i < listOpenTimeToday.size(); i++) {
                tempDateDeliveryCal.setTimeInMillis(listOpenTimeToday.get(i).getOpen_time());
                tempDateDeliveryCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                tempDateDeliveryCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                tempDateDeliveryCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

                Calendar tempCloseDeliveryCal = Calendar.getInstance();
                tempCloseDeliveryCal.setTimeInMillis(listOpenTimeToday.get(i).getClose_time());
                tempCloseDeliveryCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                tempCloseDeliveryCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                tempCloseDeliveryCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

                // Chưa đến khung giờ cuối
                if (i != listOpenTimeToday.size() - 1) {
                    if (store.getState().equals("paused")) {
                        Calendar endPausedTimeCal = Calendar.getInstance();
                        if (store.getEnd_pause_time() != null)
                            endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                        //Nếu giờ hoạt động lại nằm trong giới hạn mở cửa sớm nhất và đóng cửa lớn nhất
                        if (endPausedTimeCal.compareTo(tempCloseDeliveryCal) < 0)
                            break;
                    } else {
                        if (currentDateCal.compareTo(tempDateDeliveryCal) < 0)
                            break;
                    }
                } else { // So sánh đến khung giờ cuối
                    if (store.getState().equals("paused")) {
                        Calendar endPausedTimeCal = Calendar.getInstance();
                        if (store.getEnd_pause_time() != null)
                            endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                        //Nằm sau tất cả các khung giờ
                        if (TimeUnit.MILLISECONDS.toDays(endPausedTimeCal.getTimeInMillis() - currentDateCal.getTimeInMillis()) == 1
                                || endPausedTimeCal.compareTo(tempCloseDeliveryCal) > 0)
                            isToday = false;
                    } else {
                        if (currentDateCal.compareTo(tempCloseDeliveryCal) > 0)
                            isToday = false;
                    }
                }
            }
        }

        if (isToday) {
            realDateDeliveryCal.set(Calendar.HOUR_OF_DAY, tempDateDeliveryCal.get(Calendar.HOUR_OF_DAY));
            realDateDeliveryCal.set(Calendar.MINUTE, tempDateDeliveryCal.get(Calendar.MINUTE));
        } else {
            //Lấy khung giờ của ngày khả dụng gần nhất
            ArrayList<OpenTimeModel> listTimeNext = new ArrayList<>();
            for (int i = 0; i < store.getList_open_time().size(); i++) {
                if (mDayOfWeek == 7) { // Ngày mai là chủ nhật
                    if (store.getList_open_time().get(i).getWeekday() == 1) {
                        listTimeNext.add(store.getList_open_time().get(i));
                    }
                } else {
                    if (store.getList_open_time().get(i).getWeekday() == mDayOfWeek + 1) {
                        listTimeNext.add(store.getList_open_time().get(i));
                    }
                }
            }

            if (listTimeNext.size() > 0) { // Tìm khung giờ mở cửa vào ngày mai
                if (store.getState().equals("paused")) {
                    Calendar endPausedTimeCal = Calendar.getInstance();
                    if (store.getEnd_pause_time() != null)
                        endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                    for (int i = 0; i < listTimeNext.size(); i++) {
                        tempDateDeliveryCal.setTimeInMillis(listTimeNext.get(i).getOpen_time());
                        tempDateDeliveryCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                        tempDateDeliveryCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                        tempDateDeliveryCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                        Calendar tempCloseDeliveryCal = Calendar.getInstance();
                        tempCloseDeliveryCal.setTimeInMillis(listTimeNext.get(i).getClose_time());
                        tempCloseDeliveryCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                        tempCloseDeliveryCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                        tempCloseDeliveryCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                        if (i != listTimeNext.size() - 1) {
                            if (endPausedTimeCal.compareTo(tempCloseDeliveryCal) < 0)
                                break;
                        } else {
                            if (endPausedTimeCal.compareTo(tempCloseDeliveryCal) > 0)
                                return null;
                        }
                    }
                } else {
                    tempDateDeliveryCal.setTimeInMillis(listTimeNext.get(0).getOpen_time());
                }

                realDateDeliveryCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                realDateDeliveryCal.set(Calendar.HOUR_OF_DAY, tempDateDeliveryCal.get(Calendar.HOUR_OF_DAY));
                realDateDeliveryCal.set(Calendar.MINUTE, tempDateDeliveryCal.get(Calendar.MINUTE));

            } else {
                //Tìm khung giờ mở cửa của ngày gần nhất

                return null;
            }
        }

        return realDateDeliveryCal.getTimeInMillis();
    }

    private Long getOrderTime(StoreOfCategoryModel store) {
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        Calendar storeOpenTimeCal = Calendar.getInstance();
        Long storeOpenTime = getSuitableOpenTime(store);

        if (storeOpenTime != null)
            storeOpenTimeCal.setTimeInMillis(storeOpenTime);
        else return null;

        if (store.getState().equals("paused")) {
            Calendar endPauseTimeCal = Calendar.getInstance();
            if (store.getEnd_pause_time() != null) {
                endPauseTimeCal.setTimeInMillis(store.getEnd_pause_time());
            } else endPauseTimeCal.setTimeInMillis(System.currentTimeMillis());

            if (TimeUnit.MILLISECONDS.toDays(storeOpenTimeCal.getTimeInMillis() - lixiOpenTimeCal.getTimeInMillis()) > 1)
                return null;

            if (storeOpenTimeCal.compareTo(endPauseTimeCal) < 0)
                storeOpenTimeCal.setTimeInMillis(endPauseTimeCal.getTimeInMillis());
        }

        lixiOpenTimeCal.set(Calendar.DAY_OF_MONTH, storeOpenTimeCal.get(Calendar.DAY_OF_MONTH));
        lixiCloseTimeCal.set(Calendar.DAY_OF_MONTH, storeOpenTimeCal.get(Calendar.DAY_OF_MONTH));
        if (lixiCloseTimeCal.compareTo(storeOpenTimeCal) <= 0)
            return null;

        if (lixiOpenTimeCal.compareTo(storeOpenTimeCal) < 0)
            lixiOpenTimeCal.setTimeInMillis(storeOpenTimeCal.getTimeInMillis());
        lixiOpenTimeCal.add(Calendar.MINUTE, store.getDelay_after_open());

        return lixiOpenTimeCal.getTimeInMillis();
    }

    private Long getSuitableOpenTime(StoreOfCategoryModel store) {
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        Long todayOpenTime = getTodayOpenTime(store);
        if (todayOpenTime != null) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(todayOpenTime);

            if (lixiCloseTimeCal.compareTo(openTimeCal) < 0) {
                Long afterOpenTime = getOpenTimeAfterToday(store);
                if (afterOpenTime != null)
                    return afterOpenTime;
            }
            return todayOpenTime;
        } else {
            Long afterOpenTime = getOpenTimeAfterToday(store);
            if (afterOpenTime != null)
                return afterOpenTime;
        }
        return null;
    }

    private Long getTodayOpenTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);
        ArrayList<OpenTimeModel> listOpenTimeToday = getOpenTimeListByDay(store, mDayOfWeek);

        if (listOpenTimeToday.size() == 0)
            return null;

        for (int i = 0; i < listOpenTimeToday.size(); i++) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(listOpenTimeToday.get(i).getOpen_time());
            openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            Calendar closeTimeCal = Calendar.getInstance();
            closeTimeCal.setTimeInMillis(listOpenTimeToday.get(i).getClose_time());
            closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            if (store.getState().equals("paused")) {
                Calendar endPausedTimeCal = Calendar.getInstance();
                if (store.getEnd_pause_time() != null)
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                if (endPausedTimeCal.compareTo(closeTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
            } else {
                if (currentDateCal.compareTo(openTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
                else if (currentDateCal.compareTo(openTimeCal) >= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                    return currentDateCal.getTimeInMillis();
            }
        }

        return null;
    }

    private Long getOpenTimeAfterToday(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);

        ArrayList<OpenTimeModel> listOpenTimeTomorrow = getOpenTimeListByDay(store, mDayOfWeek == 7 ? 1 : mDayOfWeek + 1);

        if (store.getState().equals("paused")) {
            for (int i = 0; i < listOpenTimeTomorrow.size(); i++) {
                Calendar openTimeCal = Calendar.getInstance();
                openTimeCal.setTimeInMillis(listOpenTimeTomorrow.get(i).getOpen_time());
                openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar closeTimeCal = Calendar.getInstance();
                closeTimeCal.setTimeInMillis(listOpenTimeTomorrow.get(i).getClose_time());
                closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar endPausedTimeCal = Calendar.getInstance();
                if (store.getEnd_pause_time() != null)
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                if (endPausedTimeCal.compareTo(closeTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
            }
        } else {
            if (listOpenTimeTomorrow.size() > 0) {
                Calendar resCal = Calendar.getInstance();
                resCal.setTimeInMillis(listOpenTimeTomorrow.get(0).getOpen_time());
                resCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                resCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                resCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                return resCal.getTimeInMillis();
            } else {

            }
        }

        return null;
    }

    private Long getSuitableOrderTime(StoreOfCategoryModel store) {
        Long res = getTodayActiveTime(store);
        if (res != null) {
            return res;
        }
        return getTomorrowActiveTime(store);
    }

    private Long getTodayActiveTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);
        ArrayList<OpenTimeModel> listOpenTimeToday = getOpenTimeListByDay(store, mDayOfWeek);

        if (listOpenTimeToday.size() == 0)
            return null;

        //get lixi time
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        for (int i = 0; i < listOpenTimeToday.size(); i++) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(listOpenTimeToday.get(i).getOpen_time());
            openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            Calendar closeTimeCal = Calendar.getInstance();
            closeTimeCal.setTimeInMillis(listOpenTimeToday.get(i).getClose_time());
            closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            if (store.getState().equals("paused")) {
                if (store.getEnd_pause_time() != null) {
                    Calendar endPausedTimeCal = Calendar.getInstance();
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());
                    if (endPausedTimeCal.compareTo(closeTimeCal) < 0) {
                        if (endPausedTimeCal.compareTo(openTimeCal) >= 0) {
                            openTimeCal.setTimeInMillis(endPausedTimeCal.getTimeInMillis());
                        }

                        if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                            if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                                return lixiOpenTimeCal.getTimeInMillis();
                        } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                            return openTimeCal.getTimeInMillis();
                        }
                    }

                } else {
                    if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                        if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                            return lixiOpenTimeCal.getTimeInMillis();
                    } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                        return openTimeCal.getTimeInMillis();
                    }
                }

            } else {
                if (currentDateCal.compareTo(openTimeCal) >= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                    openTimeCal.setTimeInMillis(currentDateCal.getTimeInMillis());

                openTime = openTimeCal.getTimeInMillis();
                if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                    if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                        return lixiOpenTimeCal.getTimeInMillis();
                } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0 && openTimeCal.compareTo(currentDateCal) >= 0) {
                    return openTimeCal.getTimeInMillis();
                }
            }
        }

        return null;
    }

    private Long getTomorrowActiveTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);
        ArrayList<OpenTimeModel> listOpenTimeTomorrow = getOpenTimeListByDay(store, mDayOfWeek == 7 ? 1 : mDayOfWeek + 1);

        //get lixi time
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        if (listOpenTimeTomorrow != null && listOpenTimeTomorrow.size() > 0) {
            for (int i = 0; i < listOpenTimeTomorrow.size(); i++) {
                Calendar openTimeCal = Calendar.getInstance();
                openTimeCal.setTimeInMillis(listOpenTimeTomorrow.get(i).getOpen_time());
                openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar closeTimeCal = Calendar.getInstance();
                closeTimeCal.setTimeInMillis(listOpenTimeTomorrow.get(i).getClose_time());
                closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                closeTimeCal.set(Calendar.MINUTE, -8);

                lixiOpenTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                lixiCloseTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                if (store.getState().equals("paused")) {
                    if (store.getEnd_pause_time() != null) {
                        Calendar endPausedTimeCal = Calendar.getInstance();
                        endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                        if (endPausedTimeCal.compareTo(closeTimeCal) < 0) {
                            if (endPausedTimeCal.compareTo(openTimeCal) >= 0) {
                                openTimeCal.setTimeInMillis(endPausedTimeCal.getTimeInMillis());
                            }

                            if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                                if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                                    return lixiOpenTimeCal.getTimeInMillis();
                            } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                                return openTimeCal.getTimeInMillis();
                            }
                        }
                    }
                } else {
                    openTime = openTimeCal.getTimeInMillis();
                    if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                        if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                            return lixiOpenTimeCal.getTimeInMillis();
                    } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                        return openTimeCal.getTimeInMillis();
                    }
                }
            }
        }
        isAfterTomorrow = true;
        return null;
    }

    private Long getTheDayAfterTomorrowActiveTime(StoreOfCategoryModel store) {

        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);

        // Nếu ngày mai không có khung giờ, tìm khung giờ gần nhất
        int i = 0, indexLeft = -1, indexRight = -1;
        int min = 15, max = -1;
        while (i < store.getList_open_time().size()) {
            int temp = store.getList_open_time().get(i).getWeekday();

            if (temp <= mDayOfWeek) {
                if (temp == 1) {
                    indexRight = i;
                    min = 0;
                } else if (max <= mDayOfWeek - temp) {
                    indexLeft = i;
                    max = mDayOfWeek - temp;
                }
            } else if (min > temp - mDayOfWeek) {
                indexRight = i;
                min = temp - mDayOfWeek;
            }

            i++;
        }

        if (indexLeft == -1 && indexRight == -1)
            return null;

        Calendar openTimeCal = Calendar.getInstance();
        openTimeCal.setTimeInMillis(store.getList_open_time().get(indexRight != -1 ? indexRight : indexLeft).getOpen_time());
        openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
        openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));

        if (indexLeft != -1 && store.getList_open_time().get(indexLeft).getWeekday() == mDayOfWeek)
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 7);
        else
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + ((store.getList_open_time().get(indexRight != -1 ? indexRight : indexLeft).getWeekday() - mDayOfWeek + 7) % 7));


        return openTimeCal.getTimeInMillis();
    }

    private ArrayList<OpenTimeModel> getOpenTimeListByDay(StoreOfCategoryModel store, int day) {
        ArrayList<OpenTimeModel> res = new ArrayList<>();
        for (OpenTimeModel item : store.getList_open_time()) {
            if (item.getWeekday() == day)
                res.add(item);
        }

        // Sắp xếp lại danh sách
        Collections.sort(res, new Comparator<OpenTimeModel>() {
            @Override
            public int compare(OpenTimeModel o1, OpenTimeModel o2) {
                Calendar o1Cal = Calendar.getInstance();
                o1Cal.setTimeInMillis(o1.getOpen_time());
                Calendar o2Cal = Calendar.getInstance();
                o2Cal.setTimeInMillis(o2.getOpen_time());
                return o1Cal.compareTo(o2Cal);
            }
        });

        return res;
    }

    @Override
    public void onClick(View view) {
        String action = TrackingConstant.BaseAction.CANCEL;
        if (view == mBinding.tvLocation) {
            GlobalTracking.trackPopupAccept(id, typeStore, action);
            eventListener.onExit();
            dismiss();
        } else if (view == mBinding.tvSchedule) {
            if (mBinding.tvSchedule.getText().toString().equals(LanguageBinding.getString(R.string.pause_dialog_discover_food, activity))) {
                eventListener.onWatch();
                action = TrackingConstant.BaseAction.COUNTINUE;
            } else if (mBinding.tvSchedule.getText().toString().contains(LanguageBinding.getString(R.string.pause_dialog_deliver, activity))) {
                eventListener.onOrder(mStore.getDelay_after_open() * 60 * 1000 + orderTime);
                action = TrackingConstant.BaseAction.COUNTINUE;
            } else {
                eventListener.onExit();
            }
            GlobalTracking.trackPopupAccept(id, typeStore, action);
            dismiss();
        }
    }

    @Override
    public void onBackPressed() {

    }

    public interface IDialogEvent {
        void onOrder(Long time);

        void onWatch();

        void onExit();
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
