package com.opencheck.client.home.lixishop.header;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentCardValueItemBinding;
import com.opencheck.client.models.merchant.CardValueModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class PaymentCardValueAdapter extends RecyclerView.Adapter<PaymentCardValueAdapter.ViewHolder> {

    private LixiActivity activity;
    private List<CardValueModel> arrData;
    private int currentSelected = 0;

    public PaymentCardValueAdapter(List<CardValueModel> data, LixiActivity activity) {
        this.activity = activity;
        arrData = data;
    }

    public int getCurrentSelected() {
        return currentSelected;
    }

    public void setCurrentSelected(int currentSelected) {
        this.currentSelected = currentSelected;
    }

    //private List<UserDTO>
    public class ViewHolder extends RecyclerView.ViewHolder {

        public int pos;
        public TextView tv_value;
        public View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_value = (TextView) itemView.findViewById(R.id.tv_value);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCurrentSelected(pos);
                    notifyDataSetChanged();
                    if (listener != null)
                        listener.onClick(pos);
                }
            });
        }
    }

    @Override
    public PaymentCardValueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaymentCardValueItemBinding paymentCardValueItemBinding =
                PaymentCardValueItemBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(paymentCardValueItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(PaymentCardValueAdapter.ViewHolder viewHolder, int position) {
        viewHolder.pos = position;
        viewHolder.tv_value.setText(arrData.get(position).getValue());

        if (position == getCurrentSelected()) {
            viewHolder.rootView.setBackgroundColor(activity.getResources().getColor(R.color.app_violet));
            viewHolder.tv_value.setTextColor(0xffffffff);
        } else {
            viewHolder.rootView.setBackgroundResource(R.drawable.bg_tran_border_gray);
            viewHolder.tv_value.setTextColor(0xff666666);
        }
//        ImageLoader.getInstance().displayImage(arrData.get(position).getUser().getAvatarPhoto(),
//                viewHolder.iv_avatar, ImageLoaderHelper.option_circle);
    }

    @Override
    public int getItemCount() {
        if (arrData == null)
            return 0;
        return arrData.size();
    }

//    public void addData(List<UserDetailDTO> listUser){
//        if (arrData==null)
//            arrData= new ArrayList<UserDetailDTO>();
//
//        arrData.addAll(listUser);
//    }

    public interface OnItemClickListener {
        public void onClick(int position);
    }

    public OnItemClickListener listener;

    public void setOnClickListener(OnItemClickListener lsn) {
        listener = lsn;
    }

}