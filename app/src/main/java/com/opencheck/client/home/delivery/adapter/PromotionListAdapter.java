package com.opencheck.client.home.delivery.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PromotionItemLayoutBinding;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PromotionListAdapter extends RecyclerView.Adapter<PromotionListAdapter.ViewHolder> implements Filterable {
    private LixiActivity activity;
    private ArrayList<PromotionModel> promotionList;
    private ArrayList<PromotionModel> filterList;
    private ItemClickListener itemClickListener;
    private int lastPos = -1;
    private long total = 0L;
    private OnAvailableData onAvailableData;

    public PromotionListAdapter(LixiActivity activity, ArrayList<PromotionModel> promotionList, long total) {
        this.activity = activity;
        this.promotionList = promotionList;
        this.filterList = promotionList;
        this.total = total;
    }

    public void setPromotionList(ArrayList<PromotionModel> promotionList) {
        this.promotionList = promotionList;
        notifyDataSetChanged();
    }

    public void setCheckedItem(int pos) {
        if(lastPos != -1) {
            filterList.get(lastPos).setChecked(false);
        }
        filterList.get(pos).setChecked(true);
        lastPos = pos;
        notifyDataSetChanged();
    }

    public void setCheckedByCode(String code) {
        if(code == null || code.equalsIgnoreCase(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity)))
            return;
        for(int i = 0; i < filterList.size(); i++) {
            if(code.equalsIgnoreCase(filterList.get(i).getCode())) {
                filterList.get(i).setChecked(true);
                lastPos = i;
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setOnAvailableData(OnAvailableData onAvailableData) {
        this.onAvailableData = onAvailableData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PromotionItemLayoutBinding itemLayoutBinding =
                PromotionItemLayoutBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);
        return new ViewHolder(itemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PromotionModel model = filterList.get(position);
        holder.tv_code.setText(model.getCode());
        holder.tv_copy_code.setText(LanguageBinding.getString(R.string.use, activity));
        if (model.getDescription() != null && model.getDescription().length() > 0) {
            holder.tv_description.setVisibility(View.VISIBLE);
            holder.tv_description.setText(model.getDescription());
        } else
            holder.tv_description.setVisibility(View.GONE);

        holder.tv_timer.setText("Hạn sử dụng: " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(model.getEnd_date())));
        holder.tv_type.setText(model.getName());

        if(total < model.getMin_bill_money()) {
            holder.rl_copy.setBackground(ContextCompat.getDrawable(activity, R.drawable.bg_promotion_gray_corner_right));
        } else {
            holder.rl_copy.setBackground(ContextCompat.getDrawable(activity, R.drawable.bg_promotion_red_corner));
        }

        if (model.isChecked()) {
            holder.rl_copy.setVisibility(View.GONE);
            holder.iv_check.setVisibility(View.VISIBLE);
        } else {
            holder.rl_copy.setVisibility(View.VISIBLE);
            holder.iv_check.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String key = charSequence.toString();
                if (key.isEmpty()) {
                    filterList = promotionList;
                } else {
                    List<PromotionModel> filteredList = new ArrayList<>();
                    for (PromotionModel productModel : promotionList) {
                        String temp = productModel.getCode().toLowerCase();
                        if (formatString(temp).contains(formatString(key.toLowerCase()))) {
                            filteredList.add(productModel);
                        }
                    }

                    filterList = (ArrayList<PromotionModel>) filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<PromotionModel>) filterResults.values;
                if (filterList != null && filterList.size() > 0) {
                    onAvailableData.onHaveData(true);
                } else {
                    onAvailableData.onHaveData(false);
                }
                notifyDataSetChanged();
            }
        };
    }

    private String formatString(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_type, tv_description, tv_timer, tv_code, tv_copy_code;
        private RelativeLayout rl_copy;
        private View v_line;
        private ImageView iv_check;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_type = itemView.findViewById(R.id.tv_type);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_timer = itemView.findViewById(R.id.tv_timer);
            tv_code = itemView.findViewById(R.id.tv_code);
            tv_copy_code = itemView.findViewById(R.id.tv_copy_code);
            v_line = itemView.findViewById(R.id.v_line);
            iv_check = itemView.findViewById(R.id.iv_check);
            rl_copy = itemView.findViewById(R.id.rl_copy);

            rl_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemCLick(getAdapterPosition(), "USE_CODE", filterList.get(getAdapterPosition()), 0L);
                }
            });
        }

    }

    public interface OnAvailableData {
        void onHaveData(boolean isHave);
    }
}