package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopBankItemBinding;
import com.opencheck.client.home.delivery.model.BankModel;

import java.util.ArrayList;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<BankModel> mList;
    private TextView tv_bank;
    private int lastPost = -1;
    private BankModel currentUserBank = null;

    public BankAdapter(LixiActivity activity, ArrayList<BankModel> mList, TextView tv_bank) {
        this.activity = activity;
        this.mList = mList;
        this.tv_bank = tv_bank;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LixiShopBankItemBinding lixiShopBankItemBinding = LixiShopBankItemBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(lixiShopBankItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BankModel dto = mList.get(position);
        holder.rootItemView.setTag(position);
        holder.tv_name.setText(dto.getName());
        ImageLoader.getInstance().displayImage(dto.getLogo_link(), holder.iv_logo, LixiApplication.getInstance().optionsNomal);
        if (dto.isSelected()) {
            lastPost = position;
            currentUserBank = dto;
            tv_bank.setText(dto.getName());
            tv_bank.setTextColor(activity.getResources().getColor(R.color.my_black));
            holder.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_cab_done_mtrl_alpha, 0);
        } else {
            holder.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        private TextView tv_name;
        private ImageView iv_logo;

        public ViewHolder(View itemView) {
            super(itemView);

            rootItemView = itemView;
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
            rootItemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView) {
                BankModel currentBank = mList.get((int) v.getTag());
                if (lastPost == -1) {
                    mList.get((int) v.getTag()).setSelected(!currentBank.isSelected());
                    notifyDataSetChanged();
                } else {
                    mList.get((int) v.getTag()).setSelected(!currentBank.isSelected());
                    if (lastPost != (int) v.getTag()) {
                        mList.get(lastPost).setSelected(false);
                    }
                    notifyDataSetChanged();
                }
                lastPost = (int) v.getTag();
                if (mList.get(lastPost).isSelected()) {
                    currentBank = mList.get(lastPost);
                    tv_bank.setText(currentBank.getName());
                    tv_bank.setTextColor(activity.getResources().getColor(R.color.my_black));
                } else {
                    currentUserBank = null;
                    tv_bank.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
                    tv_bank.setText("Chưa chọn ngân hàng");
                }
            }
        }
    }

    public BankModel getCurrentUserBank() {
        return currentUserBank;
    }
}
