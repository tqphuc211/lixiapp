package com.opencheck.client.home.LixiHomeV25.MerchantList;

import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityListMerchantBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityListMerchant extends LixiActivity {

    public static String EXTRA_NAME = "EXTRA_NAME";
    public static String EXTRA_CATEGORY_ID = "EXTRA_CATEGORY_ID";

    private RelativeLayout rl_top;
    private ImageView iv_back;
    private TextView tv_name;
    private SwipeRefreshLayout swr;
    private RecyclerView rv;

    private int categoryId;
    private String categoryName;
    private AdapterMerchant adapter;

    private ActivityListMerchantBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_list_merchant);
        categoryId = getIntent().getIntExtra(EXTRA_CATEGORY_ID, -1);
        categoryName = getIntent().getStringExtra(EXTRA_NAME);

        findViews();
        initView();
        startLoadData();
    }

    private void findViews() {
        rl_top = (RelativeLayout) findViewById(R.id.rl_top);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_name = (TextView) findViewById(R.id.tv_name);
        swr = (SwipeRefreshLayout) findViewById(R.id.swr);
        rv = (RecyclerView) findViewById(R.id.rv);

        tv_name.setText(categoryName);
        iv_back.setOnClickListener(this);
    }

    public void initView() {
        rv.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new AdapterMerchant(activity, null);
        rv.setAdapter(adapter);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if (!isLoading && !noRemain && isRvBottom()) {
                    if (adapter == null)
                        return;
                    getMerchant();
                }
            }
        });
    }

    int page = 1;
    int pageSize = 20;
    boolean isLoading;
    boolean noRemain = false;

    public void resetList() {
        if (adapter == null || adapter.getListMerchant() == null) return;
        adapter.getListMerchant().clear();
        adapter.notifyDataSetChanged();
    }

    private boolean isRvBottom() {
        if (adapter.getListMerchant() == null || adapter.getListMerchant().size() == 0)
            return false;
        int totalItemCount = rv.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && adapter.getListMerchant().size() >= pageSize);
//                && totalItemCount >= lixiHomeAdapter.getList_voucher().size());
    }


    public void startLoadData() {
        getMerchant();
    }

    private void getMerchant() {
        DataLoader.setupAPICall(activity, null);

        isLoading = true;
        Call<ApiWrapperForListModel<MerchantModel>> call = DataLoader.apiService._getCategoryMerchant(1, 20, categoryId, "");

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantModel>> call,
                                   Response<ApiWrapperForListModel<MerchantModel>> response) {
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<MerchantModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        adapter.setListMerchant(result.getRecords());
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == iv_back)
            onBackPressed();
    }
}
