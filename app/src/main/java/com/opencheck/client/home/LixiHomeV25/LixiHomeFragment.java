package com.opencheck.client.home.LixiHomeV25;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.FragmentLixiMainBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.ChooseRegionDialog;
import com.opencheck.client.dialogs.UnableConfigRegionDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.LixiHomeV25.Search.SearchingActivity;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.Service.OffsetTimer;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.ProductCategoryModel;
import com.opencheck.client.models.ProductCollectionModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.BannerModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.LocationHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LixiHomeFragment extends LixiFragment implements SwipeRefreshLayout.OnRefreshListener, ItemClickListener {

    private LixiActivity activity;

    private View rootView;

    private LinearLayout ll_stick_header;
    private LinearLayout ll_high_light;
    private TextView tv_high_light;
    private View v_high_light;
    private LinearLayout ll_nearby;
    private TextView tv_nearby;
    private View v_nearby;
    private LinearLayout ll_good_price;
    private TextView tv_good_price;
    private View v_good_price;

    private ProgressBar pb;

    private RelativeLayout rl_region;
    private TextView txtAddress;

    private SwipeRefreshLayout swr;
    private RecyclerView rv;
    public LixiHomeAdapter lixiHomeAdapter;
    private LocationHelper mLocationHelper;

    //region implement parent

    private AppPreferenceHelper appPreferenceHelper;

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_HOME;
    }
    //endregion

    private FragmentLixiMainBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentLixiMainBinding.inflate(inflater, container, false);
        rootView = mBinding.getRoot();
        activity = (LixiActivity) getActivity();
        appPreferenceHelper = new AppPreferenceHelper(activity);
        mLocationHelper = new LocationHelper(activity);
        findViews();
        initView();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoadData();
            }
        }, 500);

        if (appPreferenceHelper.getRegion() < 0) {
            showChooseRegion();
        } else {
            if (appPreferenceHelper.getRegion() == 65) {
                txtAddress.setText(LanguageBinding.getString(R.string.region_hcm, activity));
            }

            if (appPreferenceHelper.getRegion() == 66) {
                txtAddress.setText(LanguageBinding.getString(R.string.region_hanoi, activity));
            }
        }

        return rootView;
    }

    private void findViews() {
        swr = rootView.findViewById(R.id.swr);
        rv = rootView.findViewById(R.id.rv);

        ll_stick_header = rootView.findViewById(R.id.ll_stick_header);
        ll_high_light = rootView.findViewById(R.id.ll_high_light);
        tv_high_light = rootView.findViewById(R.id.tv_high_light);
        v_high_light = rootView.findViewById(R.id.v_high_light);
        ll_nearby = rootView.findViewById(R.id.ll_nearby);
        tv_nearby = rootView.findViewById(R.id.tv_nearby);
        v_nearby = rootView.findViewById(R.id.v_nearby);
        ll_good_price = rootView.findViewById(R.id.ll_good_price);
        tv_good_price = rootView.findViewById(R.id.tv_good_price);
        v_good_price = rootView.findViewById(R.id.v_good_price);
        pb = rootView.findViewById(R.id.pb);
        rl_region = rootView.findViewById(R.id.rl_address);
        txtAddress = rootView.findViewById(R.id.tv_address);

        mBinding.searchBar.setOnClickListener(this);
        ll_high_light.setOnClickListener(this);
        ll_nearby.setOnClickListener(this);
        ll_good_price.setOnClickListener(this);
        rl_region.setOnClickListener(this);

//        tab_demo = rootView.findViewById(R.id.tab_demo);
        ArrayList<String> abc = new ArrayList<String>();
        abc.add("tab 1");
        abc.add("tab tab 2");
        abc.add("tab 3");
        abc.add("tab 4");
        abc.add("tab 5");
//        tab_demo.setListTab(abc);
//        tab_demo.setPadding(0,0,0,0);
    }

//    PagerSlidingTabStrip2 tab_demo;

    public void initView() {

        swr.setOnRefreshListener(this);

        lixiHomeAdapter = new LixiHomeAdapter(activity, null, this);
        lixiHomeAdapter.setEventListener(new LixiHomeAdapter.ILixiHomeAdapterListener() {
            @Override
            public void onSelectHighlightVoucher() {
                selectTab(0);
            }

            @Override
            public void onSelectNearbyVoucher() {
                selectTab(1);
            }

            @Override
            public void onSelectGoodpriceVoucher() {
                selectTab(2);
            }
        });

        rv.setLayoutManager(new LinearLayoutManager(activity));
        rv.setAdapter(lixiHomeAdapter);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                if (pos > LixiHomeAdapter.VOUCHER_INDICATOR)
                    ll_stick_header.setVisibility(View.VISIBLE);
                else ll_stick_header.setVisibility(View.GONE);

                if (!isLoading && !noRemain && isRvBottom()) {
                    if (lixiHomeAdapter == null)
                        return;
                    switch (lixiHomeAdapter.getCurrentTab()) {
                        case 0:
                            Log.d("mapi", "onscroll get voucher");
                            getVoucherHighLight();
                            break;
                        case 1:
                            getVoucherNearby();
                            break;
                        case 2:
                            getVoucherGoodprice();
                            break;
                    }
                }
//                tab_demo.selectTab(pos);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (lixiHomeAdapter != null && lixiHomeAdapter.getCurrentTab() == 1) {
            tryTime = 6;
            getLocation();
        }
        getListFlashSale();

        if (HomeActivity.getInstance().navigation.getSelectedItemId() == R.id.navigation_home && !HomeActivity.getInstance().isFirstLauch) {

        } else {
            HomeActivity.getInstance().isFirstLauch = false;
        }
    }

    //region HANDLE ACTION
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.edt_search:
//            case R.id.iv_search:
            case R.id.searchBar:
                activity.hideKeyboard();
                Intent intent = new Intent(activity, SearchingActivity.class);
                startActivity(intent, null);
                break;
            case R.id.ll_high_light:
                selectTab(0);
                break;
            case R.id.ll_nearby:
                selectTab(1);
                break;
            case R.id.ll_good_price:
                selectTab(2);
                break;
            case R.id.rl_address:
                if (ConfigModel.getInstance(activity).getLocation_change_enable() == 1) {
                    showChooseRegion();
                } else {
                    showUnableConfigRegion();
                }
        }
    }

    ChooseRegionDialog chooseRegionDialog;

    private void showChooseRegion() {
        if (chooseRegionDialog == null) {
            chooseRegionDialog = new ChooseRegionDialog(activity, false, true, false);
        }

        chooseRegionDialog.show();
        chooseRegionDialog.addOnItemClick(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (!action.equals(txtAddress.getText().toString())) {
                    txtAddress.setText(action);
                    DataLoader.initAPICall(activity, appPreferenceHelper.getUserToken(), null);
                    startLoadData();
                }
            }
        });
    }

    UnableConfigRegionDialog unableConfigRegionDialog;

    private void showUnableConfigRegion() {
        if (unableConfigRegionDialog == null) {
            unableConfigRegionDialog = new UnableConfigRegionDialog(activity, false, true, true);
        }

        unableConfigRegionDialog.show();
    }

    int preTab = 0;

    public void selectPreTab() {
        Log.d("mapi", "selectPreTab get voucher");
        selectTab(preTab);
    }

    public void selectTab(int tab) {

        if (lixiHomeAdapter != null)
            lixiHomeAdapter.setCurrentTab(tab);

        tv_high_light.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
        tv_nearby.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
        tv_good_price.setTextColor(activity.getResources().getColor(R.color.color_gray_price));

        v_high_light.setVisibility(View.GONE);
        v_nearby.setVisibility(View.GONE);
        v_good_price.setVisibility(View.GONE);

        Log.d("mapi", "selectPreTab get voucher pre:" + preTab + "  tab: " + tab);
        switch (tab) {
            case 0:
//                if (0 != lixiHomeAdapter.getCurrentTab()) {
                if (0 != preTab) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }
                lixiHomeAdapter.setCurrentTab(0);
                Log.d("mapi", "select tab get voucher");
                getVoucherHighLight();
                tv_high_light.setTextColor(activity.getResources().getColor(R.color.timmongmo));
                v_high_light.setVisibility(View.VISIBLE);
                preTab = 0;
                break;
            case 1:
//                if (1 != lixiHomeAdapter.getCurrentTab()) {
                if (1 != preTab) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }
                lixiHomeAdapter.setCurrentTab(1);
                getLocation();
                tv_nearby.setTextColor(activity.getResources().getColor(R.color.timmongmo));
                v_nearby.setVisibility(View.VISIBLE);
                break;
            case 2:
//                if (2 != lixiHomeAdapter.getCurrentTab()) {
                if (2 != preTab) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }
                lixiHomeAdapter.setCurrentTab(2);
                getVoucherGoodprice();

                tv_good_price.setTextColor(activity.getResources().getColor(R.color.timmongmo));
                v_good_price.setVisibility(View.VISIBLE);
                preTab = 2;
                break;
        }
    }

    @Override
    public void onRefresh() {
        startLoadData();
    }
    //endregion

    //region LOAD DATA

    public void showLoading() {
        pb.setVisibility(View.VISIBLE);
        float pos = activity.getResources().getDimension(R.dimen.value_126);
        if (lixiHomeAdapter != null) {
            float i = Math.max(0, lixiHomeAdapter.getIndicatorPos());
            pb.setY(i + pos);
        }
    }

    public void hideLoading() {
        pb.setVisibility(View.GONE);
    }

    int page = 1;
    int pageSize = 20;
    boolean isLoading;
    boolean noRemain = false;

    private boolean isRvBottom() {
        if (lixiHomeAdapter.getList_voucher() == null || lixiHomeAdapter.getList_voucher().size() == 0)
            return false;
        int totalItemCount = rv.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && lixiHomeAdapter.getList_voucher().size() >= pageSize);
//                && totalItemCount >= lixiHomeAdapter.getList_voucher().size());
    }

    public void resetList() {
        if (lixiHomeAdapter == null || lixiHomeAdapter.getList_voucher() == null) return;
        lixiHomeAdapter.getList_voucher().clear();
        lixiHomeAdapter.notifyDataSetChanged();
    }

    public void startLoadData() {
        getBannerImage();
        getCategory();
        getListFlashSale();
        getVoucherTopHot();
        getCollection();

        isLoading = false;
        noRemain = false;
        page = 1;
        Log.d("mapi", "startloaddata get voucher");
//        getVoucherHighLight(); select tab đã load rồi
        if (lixiHomeAdapter != null)
            lixiHomeAdapter.setCurrentTab(0);
        Log.d("mapi", "start loaddata selectPreTab get voucher");
        selectTab(0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 10000);
    }

    private void getBannerImage() {
        DataLoader.getBannerImage(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    lixiHomeAdapter.setList_banner((ArrayList<BannerModel>) object);
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    private void getCategory() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<ProductCategoryModel>> call = DataLoader.apiService._getCategory(1, 20, 0);

        call.enqueue(new Callback<ApiWrapperForListModel<ProductCategoryModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<ProductCategoryModel>> call,
                                   Response<ApiWrapperForListModel<ProductCategoryModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<ProductCategoryModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        lixiHomeAdapter.setList_category(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<ProductCategoryModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCollection() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<ProductCollectionModel>> call = DataLoader.apiService._getCollection(1, 20);

        call.enqueue(new Callback<ApiWrapperForListModel<ProductCollectionModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<ProductCollectionModel>> call,
                                   Response<ApiWrapperForListModel<ProductCollectionModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<ProductCollectionModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        lixiHomeAdapter.setList_collection(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                        lixiHomeAdapter.setList_collection(null);
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    lixiHomeAdapter.setList_collection(null);
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<ProductCollectionModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getListFlashSale() {
        DataLoader.getListFlashSaleNow(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ApiWrapperModel<FlashSaleNow> result = (ApiWrapperModel<FlashSaleNow>) object;
                    FlashSaleNow flashSaleNow = result.getRecords();
                    long currentTime = result.get_meta().getCurrentTime();
                    OffsetTimer.initOffset(currentTime, activity);
                    long displayTime = 0;
                    if (flashSaleNow.getStart_time() > currentTime) {
                        displayTime = flashSaleNow.getStart_time() - currentTime;
                        lixiHomeAdapter.setList_flash_sale(flashSaleNow.getProducts(), displayTime * 1000, false);
                    } else {
                        displayTime = flashSaleNow.getEnd_time() - currentTime;
                        lixiHomeAdapter.setList_flash_sale(flashSaleNow.getProducts(), displayTime * 1000, true);
                    }

//                    lixiHomeAdapter.notifyDataSetChanged();
                }
            }
        }, 1, 10);
    }

    private void getVoucherTopHot() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getEvoucherHot(1, 20);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
//                        _callBack.handleCallback(true, result.getRecords());
                        lixiHomeAdapter.setList_voucher_tophot(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getVoucherHighLight() {
        DataLoader.setupAPICall(activity, null);

        showLoading();
        isLoading = true;
        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getEvoucherSpecial(page, pageSize);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                swr.setRefreshing(false);
                hideLoading();
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        lixiHomeAdapter.setList_voucher(result.getRecords());
                        if (page == 1 && ll_stick_header.getVisibility() == View.VISIBLE)
                            rv.scrollToPosition(LixiHomeAdapter.VOUCHER_INDICATOR);
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getVoucherNearby() {
        DataLoader.setupAPICall(activity, null);

        showLoading();
        isLoading = true;
        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getEvoucherNearby(page, pageSize, curLat, curLong);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                swr.setRefreshing(false);
                hideLoading();
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        lixiHomeAdapter.setList_voucher(result.getRecords());
                        if (page == 1 && ll_stick_header.getVisibility() == View.VISIBLE)
                            rv.scrollToPosition(LixiHomeAdapter.VOUCHER_INDICATOR);
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getVoucherGoodprice() {
        DataLoader.setupAPICall(activity, null);

        showLoading();
        isLoading = true;
        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getEvoucherGoodprice(page, pageSize);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                swr.setRefreshing(false);
                hideLoading();
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        lixiHomeAdapter.setList_voucher(result.getRecords());
                        if (page == 1 && ll_stick_header.getVisibility() == View.VISIBLE)
                            rv.scrollToPosition(LixiHomeAdapter.VOUCHER_INDICATOR);
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }
    //endregion

    //region PERMISSION

    private boolean checkLocationPermission() {
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
//            }
//            return false;
//        }
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
//            }
//            return false;
//        }
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationHelper.onRequestPermissionsResult(requestCode, grantResults);
//        switch (requestCode) {
//            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
//            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    getLocation();
//                } else {
//                    Toast.makeText(activity, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
//                    selectTab(preTab);
//                }
//                break;
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLocationHelper.onActivityResult(requestCode, resultCode);
    }

    private boolean checkGpsStatus() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private float curLat;
    private float curLong;
    private int tryTime = 0;

    public void getLocation() {
        showLoading();
        mLocationHelper.setTimeOut(Integer.MAX_VALUE)
                .requestLocationGoogle(new LocationHelper.LocationCallBack() {
                    @Override
                    public void onResponse(LocationHelper.Task task) {
                        if (task.isSuccess()) {
                            curLat = (float) task.getLocation().getLatitude();
                            curLong = (float) task.getLocation().getLongitude();
                            getVoucherNearby();
                            preTab = 1;
                        } else {
                            Log.d("TTTT", task.getException().getMessage());
                            selectTab(preTab);
                        }
                    }
                });

//        if (checkGpsStatus()) {
//            Helper.getGPSLocation(activity, new LocationCallBack() {
//                @Override
//                public void callback(LatLng position, ArrayList<String> addressInfo) {
//                    curLat = (float) position.latitude;
//                    curLong = (float) position.longitude;
////                    Log.d("mapi", "Location: " + curLat + " " + curLong + "");
//                    if (curLat == 0 || curLong == 0) {
//                        if (tryTime > 0) {
//                            (new Handler()).postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    getLocation();
//                                }
//                            }, 500);
//                            tryTime--;
//                        } else {
//                            selectPreTab();
//                            Toast.makeText(activity, "Không thể xác định vị trí của bạn. Vui lòng thử lại", Toast.LENGTH_SHORT).show();
//                        }
//                        return;
//                    }
//                    getVoucherNearby();
//                    preTab = 1;
//                }
//            });
//        } else {
//            turnGPSOn();
//        }
    }
    //endregion

    private void turnGPSOn() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Cài đặt GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS đã tắt. Bạn có muốn đi đến menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton(LanguageBinding.getString(R.string.setting, activity), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectPreTab();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onItemCLick(int pos, String action, Object object, Long count) {
//        lixiHomeAdapter.setList_flash_sale(null, 0, false);
//        initView();startLoadData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoadData();
            }
        }, 1000);
    }
}