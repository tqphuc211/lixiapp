package com.opencheck.client.home.product.cart_old;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopBankSelectionDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.product.gateway.DialogTransactions;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.BankModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.lixishop.SingleMethodPaymentModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class BankSelectionDialog extends LixiTrackingDialog {
    public BankSelectionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private static final int MAX_MONEY_ALLOWED = 500000000;

    public static final String PRODUCT_MODEL = "PRODUCT_MODEL";

    private RelativeLayout rl_header;
    private TextView tv_title;
    private ImageView iv_back;

    private TextView tv_buy;
    private TextView tv_cash;
    private TextView tv_bank_name;
    private AdapterBank bankAdapter;
    private RecyclerView recyclerView;


    private ProductCashVoucherDetailModel productModel;
    private UserModel userInfo;
    int sl = -1;
    private List<BankModel> bankList;
    long id = -1;

    long paymentPrice = 0;
    boolean isBank = false;
    long cashBack = 0;
    long lixi_discount = 0;

    //region Initial setting


    public void setData(long lixi_discount, long id, ProductCashVoucherDetailModel productModel, int sl, UserModel userInfo, List<BankModel> bankModelList, CheckBox cb_lixi_point_use, PaymentMethodModel paymentMethodModel, long PaymentPrice, long cashback, String... bank) {
        this.id = id;
        this.productModel = productModel;
        this.sl = sl;
        this.userInfo = userInfo;
        this.bankList = bankModelList;
        this.paymentMethodModel = paymentMethodModel;
        this.cb_lixi_point_use = cb_lixi_point_use;
        this.paymentPrice = PaymentPrice;
        this.cashBack = cashback;
        this.lixi_discount = lixi_discount;
        initDialogEvent();
        setViewData();
        if (bank.length > 0)
            isBank = true;

        trackScreen();
    }

    public void setPaymentPrice(long price) {
        tv_cash.setText(Helper.getVNCurrency(price) + activity.getString(R.string.p));
    }

    private LixiShopBankSelectionDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopBankSelectionDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
    }

    private void findViews() {
        rl_header = (RelativeLayout) findViewById(R.id.rl_header);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_cash = (TextView) findViewById(R.id.tv_cash);
        tv_buy = (TextView) findViewById(R.id.tv_buy);
        tv_bank_name = (TextView) findViewById(R.id.tv_bank_name);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

    }

    public void initDialogEvent() {
        iv_back.setOnClickListener(this);
        tv_buy.setOnClickListener(this);
    }


    public void setViewData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        bankAdapter = new AdapterBank(activity, bankList, tv_bank_name);
        bankAdapter.setData(paymentPrice);
        recyclerView.setAdapter(bankAdapter);
        bankAdapter.notifyDataSetChanged();
//        tv_cash.setText(Utilities.getVNCurrency(productModel.getPayment_discount_price() * sl) + "VNĐ");
        tv_cash.setText(Helper.getVNCurrency(paymentPrice) + activity.getString(R.string.p));
        if (paymentMethodModel.getKey().equals("bank"))
            tv_title.setText(activity.getString(R.string.popup_select_bank_title));
        else
            tv_title.setText(activity.getString(R.string.popup_select_card_type_title));
    }

    //endregion

    private boolean isClicked = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                Intent intent = new Intent().setAction("reLoadLoadQuantityAllow");
                activity.sendBroadcast(intent);
                onBackPressed();
                break;
            case R.id.tv_buy:
                if (bankAdapter.getCurrentUserBank() != null) {
                    if (!isClicked) {
                        isClicked = true;
                        if (productModel.getOrder_method().equals("v2")
                                && paymentMethodModel.getKey().indexOf("payoo") >= 0)
                            checkoutV2();
                        else
                            checkout();
                    } else
                        Helper.showErrorDialog(activity, "Đang xử lý, vui lòng chờ");
                } else {
                    Toast.makeText(activity, "Chưa chon ngân hàng", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private PaymentMethodModel paymentMethodModel;
    private CheckBox cb_lixi_point_use;

    private void checkout() {

        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        obj.addProperty("bank_code", bankAdapter.getCurrentUserBank().getCode());
        obj.addProperty("phone", userInfo.getPhone());
        obj.addProperty("payment_method", paymentMethodModel.getKey());

        if (cb_lixi_point_use.isChecked()) {
            obj.addProperty("user_discount", true);
        }

        if (ConfirmBuyCashEvouherDialog.isReorder) {
            ConfirmBuyCashEvouherDialog.isReorder = false;
            obj.addProperty("order_id", ConfirmBuyCashEvouherDialog.order_id);
            ConfirmBuyCashEvouherDialog.order_id = -1;
        }

        String method = "";

        if (paymentMethodModel.getKey().contains("zalo"))
            method = "/zalo";
        if (paymentMethodModel.getKey().contains("payoo"))
            method = "/payoo";

        DataLoader.buyCashProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(final boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = null;
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    Long productId = jsonObject.get("product").getAsLong();
                    isClicked = false;
                    DialogTransactions popupTransaction = new DialogTransactions(activity, false, false, false);
                    popupTransaction.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            dismiss();
                        }
                    });
                    popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
                        @Override
                        public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                            dismiss();
                            if (eventListener != null)
                                eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
                        }
                    });
                    popupTransaction.show();
                    //TODO: fix name
                    popupTransaction.setData(payment_link, false, orderId);
                } else {
                    Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }, obj, method, "showloading");


    }


    private void checkoutV2() {

        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", productModel.getId());
            jsonObject.put("quantity", sl);
            jsonArray.put(jsonObject);

            JSONObject objs = new JSONObject();
//            objs.put("bank_code", paymentMethodAdapter.getSingleMethodPaymentModel().getPayment_method().getBankSelected().getCode());
            objs.put("phone", userInfo.getPhone());
            objs.put("payment_method", paymentMethodModel.getKey());
            objs.put("user_discount", cb_lixi_point_use.isChecked());
            objs.put("list_product", jsonArray);

            DataLoader.buyCashProductV2(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        JsonObject jsonObject = (JsonObject) object;
                        String payment_link = "";
                        payment_link = jsonObject.get("payment_link").getAsString();
                        Long orderId = jsonObject.get("id").getAsLong();
                        Long productId = jsonObject.get("product").getAsLong();
                        DialogTransactions popupTransaction = new DialogTransactions(activity, false, false, false);
                        popupTransaction.setOnDismissListener(new OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //  dismiss();
                            }
                        });
                        popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
                            @Override
                            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                                dismiss();
                                if (eventListener != null)
                                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
                            }
                        });
                        SingleMethodPaymentModel singleMethodPaymentModel = new SingleMethodPaymentModel();
                        singleMethodPaymentModel.setPayment_method(paymentMethodModel);
                        popupTransaction.show();
                        //TODO: fix name
                        popupTransaction.setData(payment_link, false, orderId);
                    } else {
                        Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }, objs, "/payoo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //region Define dialog's callback

    public interface IDidalogEvent {
        void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    //endregion
}
