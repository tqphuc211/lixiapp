package com.opencheck.client.home.delivery.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.databinding.FragmentChooseLocationBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.activities.NewSearchLocationActivity;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.game.GameActivity;
import com.opencheck.client.home.merchant.map.LoadLocationAddress;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.LocationHelper;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseLocationFragment extends LixiFragment {

    //Initialize views
    private TextView tv_require_address, tv_location, tv_message, tv_detail_message, tv_turn_on_gps, tv_search_location, tv_message_feedback;
    private TextView tv_inactive_message, tv_active_address;
    private LinearLayout ll_type, ll_gps, ll_search, ll_turn_on_gps;
    private EditText et_type;
    private View rootView;
    private RelativeLayout rl_loading, rl_inactive_deli, rlChooseAddress;
    private ArrayList<StoreOfCategoryModel> list;
    public static boolean deliHomeShowing = false;

    //Location Variable
    private LocationHelper mLocationHelper;
    private FragmentChooseLocationBinding mBinding;
    private AppPreferenceHelper appPreferenceHelper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_choose_location, container, false);
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }

        initViews(mBinding.getRoot());
        rootview = mBinding.getRoot();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appPreferenceHelper = AppPreferenceHelper.getInstance();
        mLocationHelper = new LocationHelper(getActivity());
    }

    private void initViews(View v) {
        tv_require_address = v.findViewById(R.id.tv_require_address);
        ll_search = v.findViewById(R.id.ll_search);
        ll_turn_on_gps = v.findViewById(R.id.ll_turn_on_gps);
        tv_message = v.findViewById(R.id.tv_message);
        tv_detail_message = v.findViewById(R.id.tv_detail_message);
        tv_turn_on_gps = v.findViewById(R.id.tv_turn_on_gps);
        tv_inactive_message = v.findViewById(R.id.tv_inactive_message);
        tv_message_feedback = v.findViewById(R.id.tv_message_feedback);
        tv_search_location = v.findViewById(R.id.tv_search_location);
        tv_active_address = v.findViewById(R.id.tv_active_address);
        ll_gps = v.findViewById(R.id.ll_gps);
        rl_loading = v.findViewById(R.id.rl_loading);
        rl_inactive_deli = v.findViewById(R.id.rl_inactive_deli);
        rlChooseAddress = v.findViewById(R.id.rlChooseAddress);

        tv_require_address.setOnClickListener(this);
        mBinding.searchBar.setOnClickListener(this);
        ll_turn_on_gps.setOnClickListener(this);
        ll_search.setOnClickListener(this);
        rlChooseAddress.setOnClickListener(this);
        rl_loading.setOnClickListener(this);
        mBinding.tvChooseOtherLocation.setOnClickListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getConfig();
            }
        }, 2000);

        try {
            mBinding.imgGame.setOnClickListener(this);

            if (ConfigModel.getInstance(activity).getGame_home_button_enable() == 0) {
                mBinding.imgGame.setVisibility(View.GONE);
            } else {
                mBinding.imgGame.setVisibility(View.VISIBLE);
            }

            LixiImage.with(activity)
                    .load(ConfigModel.getInstance(activity).getGame_home_button_icon())
                    .into(mBinding.imgGame);
        } catch (Exception ex) {
            Crashlytics.logException(ex);
        }
    }

    private boolean checkGpsStatus() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void getLocation() {
        rl_loading.setVisibility(View.VISIBLE);
        if (appPreferenceHelper.getDefaultLocation() != null) {
            DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
            tv_active_address.setText(deliAddressModel.getFullAddress());
            //tv_location.setText(deliAddressModel.getFullAddress());
            mBinding.tvChooseLocation.setText(deliAddressModel.getFullAddress());
            getStoreAtLocation(deliAddressModel.getFullAddress(), deliAddressModel.getLat(), deliAddressModel.getLng());
            return;
        }
        if (checkGpsStatus()) {
            if (new AppPreferenceHelper(activity).isStartActivityFromDeepLink()) {
                requestLocation();
            } else {
                countDeepLink++;
            }

        } else {
            showViewNoGPS();
        }
    }

    private void showViewNoGPS() {
        isRequest = true;
        rl_loading.setVisibility(View.GONE);
        rl_inactive_deli.setVisibility(View.GONE);
        if (mBinding.tvChooseLocation.getText().toString().equals("Chọn địa chỉ giao hàng")) {
            ll_gps.setVisibility(View.VISIBLE);
            tv_require_address.setVisibility(View.GONE);
            tv_detail_message.setVisibility(View.GONE);
            tv_message.setText(LanguageBinding.getString(R.string.delivery_choose_location_message_1, getContext()));
        } else {
            ll_gps.setVisibility(View.GONE);
            tv_require_address.setVisibility(View.VISIBLE);
            tv_detail_message.setVisibility(View.VISIBLE);
            tv_message.setText(LanguageBinding.getString(R.string.delivery_choose_location_message_2, getContext()));
        }

        if (appPreferenceHelper.isStartActivityFromDeepLink()) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("LAST_LOCATION", true);
            Intent intent = new Intent(activity, NewSearchLocationActivity.class);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    private boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private String getAddressFromLocation(double lat, double lng) {
        String res = null;
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            if (Geocoder.isPresent()) {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                if (addresses != null && addresses.size() > 0) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i));
                        if (i != returnedAddress.getMaxAddressLineIndex())
                            strReturnedAddress.append(", ");
                    }

                    res = strReturnedAddress.toString();
                } else {
                    Log.w("GPS", "No Address returned!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public DeliveryHomeFragment deliHomeFragment;
    private boolean isEnable = false;
    private boolean isRequest = false;
    private int countHandleResult = 0;
    private int countDeepLink = -1;

    private void getStoreAtLocation(final String address, final double lat, final double lng) {
        DeliDataLoader.getListHot(activity, new ApiCallBack() {
            @Override
            public void handleCallback(final boolean isSuccess, Object object) {
                if (isSuccess) {
                    rl_loading.setVisibility(View.GONE);
                    list = (ArrayList<StoreOfCategoryModel>) object;
                    if (list == null || list.size() == 0) {
                        setUnsupportLocation();
                    } else {
                        deliHomeShowing = true;
                        try {
                            deliHomeFragment = new DeliveryHomeFragment();
                            Analytics.trackScreenView(deliHomeFragment.getScreenName(), null);
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.child_fragment_container, deliHomeFragment).commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                        } catch (IllegalStateException e) {
                            getStoreAtLocation(address, lat, lng);
                        }
                    }
                } else {
                    rl_loading.setVisibility(View.GONE);
                    if (mBinding.tvChooseLocation.getText().toString().equals("Chọn địa chỉ giao hàng"))
                        return;
                    setUnsupportLocation();
                }
            }
        }, 1, address, lat, lng);
    }

    private void setUnsupportLocation() {
        if (appPreferenceHelper.getDefaultLocation() != null) {
            tv_detail_message.setVisibility(View.VISIBLE);
            mBinding.tvChooseOtherLocation.setVisibility(View.VISIBLE);
            tv_require_address.setVisibility(View.VISIBLE);
            mBinding.tvUnsupportLocation.setVisibility(View.VISIBLE);
            mBinding.tvUnsupportDeli.setVisibility(View.VISIBLE);
            ll_gps.setVisibility(View.GONE);
            tv_message.setVisibility(View.GONE);
            tv_message_feedback.setVisibility(View.GONE);
            mBinding.tvUnsupportLocation.setText(appPreferenceHelper.getDefaultLocation().getFullAddress());
            mBinding.tvChooseLocation.setText(appPreferenceHelper.getDefaultLocation().getFullAddress());
        }
    }

    public void getConfig() {
        rl_loading.setVisibility(View.VISIBLE);
        DeliDataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    appPreferenceHelper.setTimeResetDeliConfig(System.currentTimeMillis());
                    ArrayList<DeliConfigModel> mListConfig = (ArrayList<DeliConfigModel>) object;
                    String openTime = "", closeTime = "", disableMessage = "";
                    for (DeliConfigModel model : mListConfig) {
                        if (model.getKey().equals("deli_disable_message"))
                            disableMessage = model.getValue();
                        else if (model.getKey().equals("deli_open_time")) {
                            openTime = model.getValue();
                        } else if (model.getKey().equals("deli_close_time")) {
                            closeTime = model.getValue();
                        } else if (model.getKey().equals("deli_enable") && model.getValue().equals("true")) {
                            isEnable = true;
                        } else if (model.getKey().equals("lixi_phone"))
                            (new AppPreferenceHelper(activity)).setDefaultPhoneDeli(model.getValue());
                    }

                    if (isEnable) {
                        rl_inactive_deli.setVisibility(View.GONE);
                        mBinding.searchBar.setVisibility(View.VISIBLE);
                        if (appPreferenceHelper.getDefaultLocation() != null) {
                            DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
                            tv_active_address.setText(deliAddressModel.getFullAddress());
                            //tv_location.setText(deliAddressModel.getFullAddress());
                            mBinding.tvChooseLocation.setText(deliAddressModel.getFullAddress());
                            getStoreAtLocation(deliAddressModel.getFullAddress(), deliAddressModel.getLat(), deliAddressModel.getLng());
                        } else {
                            getLocation();//
                        }

                    } else {
                        rl_loading.setVisibility(View.GONE);
                        rl_inactive_deli.setVisibility(View.VISIBLE);
                        tv_inactive_message.setText(disableMessage);
                        mBinding.searchBar.setVisibility(View.GONE);
                        mBinding.rlChooseAddress.setVisibility(View.GONE);
                    }
                } else
                    rl_loading.setVisibility(View.GONE);
            }
        });
    }

    public void recheckConfig() {
        DeliDataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<DeliConfigModel> mListConfig = (ArrayList<DeliConfigModel>) object;
                    boolean isEnableCheck = false;

                    String openTime = "", closeTime = "", disableMessage = "";
                    for (DeliConfigModel model : mListConfig) {
                        if (model.getKey().equals("deli_disable_message"))
                            disableMessage = model.getValue();
                        else if (model.getKey().equals("deli_open_time")) {
                            openTime = model.getValue();
                        } else if (model.getKey().equals("deli_close_time")) {
                            closeTime = model.getValue();
                        } else if (model.getKey().equals("deli_enable") && model.getValue().equals("true")) {
                            isEnableCheck = true;
                        }
                    }

                    if (isEnableCheck == isEnable)
                        return;

                    isEnable = isEnableCheck;
                    if (isEnableCheck) {
                        rl_inactive_deli.setVisibility(View.GONE);
                        mBinding.searchBar.setVisibility(View.VISIBLE);
                        if (appPreferenceHelper.getDefaultLocation() != null) {
                            DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
                            tv_active_address.setText(deliAddressModel.getFullAddress());
                            //tv_location.setText(deliAddressModel.getFullAddress());
                            mBinding.tvChooseLocation.setText(deliAddressModel.getFullAddress());
                            getStoreAtLocation(deliAddressModel.getFullAddress(), deliAddressModel.getLat(), deliAddressModel.getLng());
                        } else {
                            getLocation();
                        }
                    } else {
                        if (deliHomeShowing) {
                            getFragmentManager().beginTransaction().detach(deliHomeFragment).commit();
                            deliHomeShowing = false;
                        }
                        rl_loading.setVisibility(View.GONE);
                        rl_inactive_deli.setVisibility(View.VISIBLE);
                        tv_inactive_message.setText(disableMessage);
                        mBinding.searchBar.setVisibility(View.GONE);
                        mBinding.rlChooseAddress.setVisibility(View.GONE);
                    }
                } else
                    rl_loading.setVisibility(View.GONE);
            }
        });
    }

    private void handleOnGPSResponse(String address, double lat, double lng) {
        if (address.contains("Vietnam"))
            address = address.replace("Vietnam", "Việt Nam");
        rl_inactive_deli.setVisibility(View.GONE);
        tv_active_address.setText(address);
        mBinding.tvChooseLocation.setText(address);
        String placeName = address.substring(0, address.contains(",") ? address.indexOf(",") : 0);
        DeliAddressModel addressModel = new DeliAddressModel(address, placeName, lat, lng, true);
        appPreferenceHelper.setDefaultLocation(addressModel);
        DeliDataLoader.checkChangeProvinceHeader(activity);
        getStoreAtLocation(address, lat, lng);
    }

    private void requestLocation() {
        mLocationHelper.setTimeOut(Integer.MAX_VALUE).requestLocationGoogle(new LocationHelper.LocationCallBack() {
            @Override
            public void onResponse(LocationHelper.Task task) {
                isRequest = true;
                if (task.isSuccess()) {
                    rl_loading.setVisibility(View.VISIBLE);
                    String fullAddress = getAddressFromLocation(task.getLocation().getLatitude(), task.getLocation().getLongitude());
                    if (fullAddress != null && fullAddress.length() > 0) {
                        handleOnGPSResponse(fullAddress, task.getLocation().getLatitude(), task.getLocation().getLongitude());
                    } else {
                        LatLng mCurrentPoint = new LatLng(task.getLocation().getLatitude(), task.getLocation().getLongitude());
                        LoadLocationAddress newTask = new LoadLocationAddress(new LocationCallBack() {
                            @Override
                            public void callback(LatLng position, ArrayList<String> addressInfo) {
                                if (addressInfo != null && addressInfo.size() > 0
                                        && addressInfo.get(0) != null && addressInfo.get(0).length() > 0) {
                                    String fullAddress = addressInfo.get(0);
                                    handleOnGPSResponse(fullAddress, position.latitude, position.longitude);
                                }
                            }
                        });
                        newTask.execute(mCurrentPoint);
                    }
                } else {
                    showViewNoGPS();
                }
            }
        });
    }

    private void requestLocationChange() {
        if (DeliveryHomeFragment.isRefreshLocation) {
            rl_loading.setVisibility(View.VISIBLE);
            DeliAddressModel mLocation = appPreferenceHelper.getDefaultLocation();
            if (mLocation == null) {
                return;
            }
            mBinding.tvChooseLocation.setText(mLocation.getFullAddress());
            getStoreAtLocation(mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
            return;
        }

        if (checkLocationPermission() && isRequest) {
            DeliAddressModel addressModel = appPreferenceHelper.getDefaultLocation();
            rl_loading.setVisibility(View.VISIBLE);
            if (addressModel == null) {
                if (appPreferenceHelper.isStartActivityFromDeepLink() && countHandleResult != 1) {
                    requestLocation();
                }
            } else {
                mBinding.tvChooseLocation.setText(addressModel.getFullAddress());
                getStoreAtLocation(addressModel.getFullAddress(), addressModel.getLat(), addressModel.getLng());
            }
        } else {
            if (countDeepLink == 0) {
                requestLocation();
                countDeepLink = -1;
            }
        }
    }

    //region BASE EVENT
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mLocationHelper != null)
            mLocationHelper.onRequestPermissionsResult(requestCode, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mLocationHelper != null)
            mLocationHelper.onActivityResult(requestCode, resultCode);
        countHandleResult++;
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        if (view == tv_require_address) {
            tv_message_feedback.setVisibility(View.VISIBLE);
            tv_require_address.setVisibility(View.GONE);
            if (list != null && list.size() > 0) {
                if (appPreferenceHelper.getDefaultLocation() != null) {
                    deliHomeFragment = new DeliveryHomeFragment();
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.replace(R.id.child_fragment_container, deliHomeFragment).commitAllowingStateLoss();
                }
            } else {
                Toast.makeText(activity, LanguageBinding.getString(R.string.delivery_choose_location_message_location_not_support_toast, getContext()), Toast.LENGTH_SHORT).show();
            }
        } else if (view == mBinding.searchBar || view == ll_search || view == rlChooseAddress || view == mBinding.tvChooseOtherLocation) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("LAST_LOCATION", true);
            try {
                appPreferenceHelper.setStartActivityFromDeepLink(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(activity, NewSearchLocationActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (view == ll_turn_on_gps) {
            rl_loading.setVisibility(View.VISIBLE);
            requestLocation();
        } else if (view == mBinding.imgGame) {
            if ((new AppPreferenceHelper(activity)).getAccessToken().equals("")) {
                LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_PLAY_GAME);
            } else {
                Intent intentGame = new Intent(activity, GameActivity.class);
                intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.HOME);
                startActivity(intentGame);
            }
        }
    }

    @Override
    public void onResume() {
        ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);
//        ((HomeActivity) activity).navigation.setVisibility(View.VISIBLE);

        if (!deliHomeShowing) {
            Log.d("ddd", "3");
            requestLocationChange();
        } else {
            Log.d("ddd", "2");
            setUnsupportLocation();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (countHandleResult == 1)
            countHandleResult = 0;
    }
    //endregion
}
