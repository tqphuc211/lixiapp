package com.opencheck.client.home.history.detail.controller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopGiftCodeItemBinding;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.lixishop.LixiShopVoucherCodeModel;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class ControllerLixiShopCode implements View.OnClickListener {

    LixiActivity activity;
    LinearLayout parentView;
    View rootView;
    LixiShopVoucherCodeModel voucher;
    OrderDetailModel orderDTO;
    ArrayList<LixiShopEvoucherBoughtModel> eVoucherList;

    private TextView tv_serial;
    private TextView tv_pin;
    private TextView tv_date_expire;
    private ImageView iv_qrcode;
    private View v_viewLine;
    private boolean isLastItem = false;

    private LixiShopGiftCodeItemBinding mBinding;

    public ControllerLixiShopCode(LixiActivity activity, LixiShopVoucherCodeModel voucher, OrderDetailModel orderDTO, LinearLayout parentView, boolean isLastItem) {
        this.activity = activity;
        this.parentView = parentView;
        this.voucher = voucher;
        this.orderDTO = orderDTO;
        this.isLastItem = isLastItem;

        rootView = activity.getLayoutInflater().inflate(R.layout.lixi_shop_gift_code_item, parentView, false);
        parentView.addView(rootView);

        eVoucherList = new ArrayList<>();

        findView();
    }

    public void findView() {
        tv_serial = (TextView) rootView.findViewById(R.id.tv_serial);
        tv_pin = (TextView) rootView.findViewById(R.id.tv_pin);
        tv_date_expire = (TextView) rootView.findViewById(R.id.tv_date_expire);
        iv_qrcode = (ImageView) rootView.findViewById(R.id.iv_qrcode);
        v_viewLine = (View) rootView.findViewById(R.id.v_view_line);

        rootView.setOnClickListener(this);

        if (voucher.isSend_pin()) {
            tv_pin.setText(voucher.getPin());
            tv_pin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        } else {
            tv_pin.setText("(Nhấn vào để xem mã)");
            tv_pin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        }
        tv_date_expire.setText(voucher.getExpired_time());
        tv_serial.setText(voucher.getSerial());
        if (isLastItem) {
            v_viewLine.setVisibility(View.GONE);
        } else {
            v_viewLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        long position = orderDTO.getCode_list().indexOf(voucher);
        Bundle data = new Bundle();
        data.putLong(ConstantValue.EVOUCHER_ID, voucher.getId());
        data.putParcelableArrayList(ConstantValue.ORDER_LIST_CODE, (ArrayList<? extends Parcelable>) orderDTO.getCode_list());
        data.putInt(ConstantValue.CURRENT_POSITION, (int) position);
        Intent intent;
        if (ConstantValue.USER_MULTI_ACTIVE)
            intent = new Intent(activity, EVoucherAccountActivity.class);
        else
            intent = new Intent(activity, ActiveCodeActivity.class);
        intent.putExtras(data);
        activity.startActivity(intent);
    }
}