package com.opencheck.client.home.history.detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemOrderDetailLogLayoutBinding;
import com.opencheck.client.models.merchant.OrderDetailLogModel;
import com.opencheck.client.utils.Helper;

import java.util.List;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderLogAdapter extends RecyclerView.Adapter<OrderLogAdapter.ViewHolder> {

    private LixiActivity activity;
    private List<OrderDetailLogModel> arrData;
    private int currentSelected = -1;

    public OrderLogAdapter(List<OrderDetailLogModel> data, LixiActivity activity) {
        this.activity = activity;
        setArrData(data);
    }

    public int getCurrentSelected() {
        return currentSelected;
    }

    public void setCurrentSelected(int currentSelected) {
        this.currentSelected = currentSelected;
    }

    public List<OrderDetailLogModel> getArrData() {
        return arrData;
    }

    public void setArrData(List<OrderDetailLogModel> arrData) {
        this.arrData = arrData;
    }

    //private List<UserDTO>
    public class ViewHolder extends RecyclerView.ViewHolder {

        public int pos;
        private RelativeLayout rlItem;
        private TextView tv_action;
        private TextView tv_time;
        private TextView tv_point;
        private RelativeLayout rlLine;

        public ViewHolder(View itemView) {
            super(itemView);

            rlItem = itemView.findViewById(R.id.rlItem);
            tv_action = itemView.findViewById(R.id.tv_action);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_point = itemView.findViewById(R.id.tv_point);
            rlLine = itemView.findViewById(R.id.rlLine);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCurrentSelected(pos);
                    notifyDataSetChanged();
                    if (listener != null)
                        listener.onClick(pos);
                }
            });
        }
    }

    @Override
    public OrderLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderDetailLogLayoutBinding detailLogLayoutBinding =
                ItemOrderDetailLogLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(detailLogLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(OrderLogAdapter.ViewHolder viewHolder, int position) {
        viewHolder.pos = position;

        if (position == getItemCount() - 1)
            viewHolder.rlLine.setVisibility(View.GONE);
        else
            viewHolder.rlLine.setVisibility(View.VISIBLE);

        viewHolder.tv_action.setText(getArrData().get(position).getAction());
        viewHolder.tv_time.setText(getArrData().get(position).getDate());
        viewHolder.tv_point.setText(Helper.getVNCurrency(getArrData().get(position).getPoint()) + "đ");
    }

    @Override
    public int getItemCount() {
        if (getArrData() == null)
            return 0;
        return getArrData().size();
    }


    public interface OnItemClickListener {
        void onClick(int position);
    }

    public OnItemClickListener listener;

    public void setOnClickListener(OnItemClickListener lsn) {
        listener = lsn;
    }

}