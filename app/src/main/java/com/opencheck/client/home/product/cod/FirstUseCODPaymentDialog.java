package com.opencheck.client.home.product.cod;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FirstUseCodDialogBinding;
import com.opencheck.client.utils.AppPreferenceHelper;

public class FirstUseCODPaymentDialog extends LixiDialog {

    FirstUseCODPaymentDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private FirstUseCodDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = FirstUseCodDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
    }

    @Override
    public void show() {
        super.show();
        if (getWindow() != null) {
            getWindow().setLayout((int) (Resources.getSystem().getDisplayMetrics().widthPixels * 0.9),
                    (int) (Resources.getSystem().getDisplayMetrics().heightPixels * 0.9));
        }
    }

    void setData(String phoneNumber) {
        mBinding.btnClose.setOnClickListener(this);
        String step1 = String.format(activity.getString(R.string.step_1_content), phoneNumber);
        String step3 = String.format(activity.getString(R.string.step_3_content), phoneNumber);
        mBinding.tvStep1Info.setText(Html.fromHtml(step1));
        mBinding.tvStep3Info.setText(Html.fromHtml(step3));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_close) {
            dismiss();
        }
    }
}
