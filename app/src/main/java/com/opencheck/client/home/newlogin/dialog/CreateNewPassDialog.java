package com.opencheck.client.home.newlogin.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogCreateNewPassBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.newlogin.communicate.OnResetPassResult;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class CreateNewPassDialog extends LixiDialog {
    private String accountkit_token;

    public CreateNewPassDialog(@NonNull LixiActivity _activity, String accountkit_token) {
        super(_activity, false, false, false);
        this.accountkit_token = accountkit_token;
    }

    private DialogCreateNewPassBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogCreateNewPassBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        mBinding.btnBack.setOnClickListener(this);
        mBinding.txtConfirm.setOnClickListener(this);

        Helper.showSoftKeyBoard(activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBack:
                cancel();
                break;
            case R.id.txtConfirm:
                // check input full
                String pass = mBinding.edtPass.getText().toString() + "";
                String con_pass = mBinding.edtConfirm.getText().toString() + "";

                // check null
                if (pass.equals("") || con_pass.equals("")){
                    Toast.makeText(activity, LanguageBinding.getString(R.string.login_error_miss_input_info, activity), Toast.LENGTH_SHORT).show();
                    return;
                }

                // check pass
                if (!pass.equals(con_pass)){
                    Toast.makeText(activity, LanguageBinding.getString(R.string.login_error_pass_not_match, activity), Toast.LENGTH_SHORT).show();
                    return;
                }

                resetPass();
                break;
        }
    }

    private void resetPass(){
        String pass = mBinding.edtPass.getText().toString();
        JsonObject param = new JsonObject();
        param.addProperty("accountkit_token", accountkit_token);
        param.addProperty("password", pass);
        DataLoader.putResetPassword(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (onResetPassResult != null){
                    onResetPassResult.onResetResult(isSuccess);
                }else{
                    Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, param);
    }

    private OnResetPassResult onResetPassResult;
    public void setOnResetPassResult(OnResetPassResult onResetPassResult){
        this.onResetPassResult = onResetPassResult;
    }
}
