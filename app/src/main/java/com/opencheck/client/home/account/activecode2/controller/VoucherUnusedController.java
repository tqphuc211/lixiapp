package com.opencheck.client.home.account.activecode2.controller;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by Tony Tuan on 04/02/2018.
 */

public class VoucherUnusedController implements View.OnClickListener{

    private LixiActivity activity;
    private LinearLayout lnRoot;
    private View viewChild;
    private int position;
    private int size;
    private LixiShopEvoucherBoughtModel evoucherBoughtModel;
    private ItemClickListener itemClickListener;
    //var
    private ImageView iv_hinh, iv_tick;
    private TextView tv_name, tv_code, tv_expire_date, tv_price, tv_quantity_day, tv_timer;
    private RelativeLayout rlRoot;

    public VoucherUnusedController(LixiActivity _activity, LixiShopEvoucherBoughtModel _evoucherBoughtModel, int _position, int _size, LinearLayout _lnRoot, ItemClickListener itemClickListener){
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.size = _size;
        this.evoucherBoughtModel = _evoucherBoughtModel;
        this.itemClickListener = itemClickListener;
        this.viewChild = View.inflate(activity, R.layout.e_voucher_item_layout, null);
        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initData() {
        if(evoucherBoughtModel.isSelected())
            iv_tick.setVisibility(View.VISIBLE);
        else
            iv_tick.setVisibility(View.GONE);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private void initView() {
        iv_hinh = (ImageView) viewChild.findViewById(R.id.iv_hinh);
        iv_tick = (ImageView) viewChild.findViewById(R.id.iv_tick);
        tv_name = (TextView) viewChild.findViewById(R.id.tv_name);
        tv_code = (TextView) viewChild.findViewById(R.id.tv_code);
        tv_expire_date = (TextView) viewChild.findViewById(R.id.tv_expire_date);
        tv_price = (TextView) viewChild.findViewById(R.id.tv_price);
        tv_quantity_day = (TextView) viewChild.findViewById(R.id.tv_quantity_day);
        tv_timer = (TextView) viewChild.findViewById(R.id.tv_timer);
        rlRoot = (RelativeLayout) viewChild.findViewById(R.id.rlRoot);

        rlRoot.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v == rlRoot)
            itemClickListener.onItemCLick(position, ConstantValue.ON_ITEM_CLICK, evoucherBoughtModel, 0L);
    }
}
