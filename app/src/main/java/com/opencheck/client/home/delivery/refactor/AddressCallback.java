package com.opencheck.client.home.delivery.refactor;

import com.google.android.gms.maps.model.LatLng;

public interface AddressCallback {
    void callback(LatLng position, String addressInfo);
}
