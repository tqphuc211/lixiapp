package com.opencheck.client.home.delivery.model;

import com.opencheck.client.utils.tracking.TrackingConstant;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentModel implements Serializable {

    public final static String PRE_PAY = "pre_pay";
    public final static String POST_PAY = "post_pay";

    private String code;
    private String name;
    private String type;
    private String logo_link;
    private String state;
    private ArrayList<GateWayModel> gateway_data;
    private boolean isSelected = false;
    private StorePolicyModel store_policy;

    public PaymentModel(String code, String name, String type, boolean isSelected) {
        this.code = code;
        this.name = name;
        this.type = type;
        this.isSelected = isSelected;
    }

    public StorePolicyModel getStore_policy() {
        return store_policy;
    }

    public void setStore_policy(StorePolicyModel store_policy) {
        this.store_policy = store_policy;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<GateWayModel> getGateway_data() {
        return gateway_data;
    }

    public void setGateway_data(ArrayList<GateWayModel> gateway_data) {
        this.gateway_data = gateway_data;
    }

    public String getLogo_link() {
        return logo_link;
    }

    public void setLogo_link(String logo_link) {
        this.logo_link = logo_link;
    }

    public String getMethodTracking() {
        switch (code) {
            case DeliConfigModel.PAYMENT_BY_COD:
                return TrackingConstant.PaymentMethod.COD;
            case DeliConfigModel.PAYMENT_BY_MOMO:
            case DeliConfigModel.PAYMENT_BY_ZALO:
                return TrackingConstant.PaymentMethod.WALLET;
            case DeliConfigModel.PAYMENT_AT_STORE:
                return TrackingConstant.PaymentMethod.AT_STORE;
            default:
                return TrackingConstant.PaymentMethod.ONLINE;
        }
    }
}
