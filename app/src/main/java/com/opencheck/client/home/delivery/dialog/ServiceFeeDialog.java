package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ServiceFeeDialogBinding;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.QuotationModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.Helper;

public class ServiceFeeDialog extends LixiDialog {

    private ServiceFeeDialogBinding mBinding;

    public ServiceFeeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.service_fee_dialog, null, false);
        setContentView(mBinding.getRoot());
        mBinding.rlDismiss.setOnClickListener(this);
    }

    public void showData(DeliOrderModel orderModel) {
        long totalMoney = 0L, serviceFee = 0L;
        for (QuotationModel model : orderModel.getList_quotation()) {
            if (model.getCode().equalsIgnoreCase("product_money")) {
                totalMoney = model.getMoney();
            } else if (model.getCode().equalsIgnoreCase("service_money")) {
                serviceFee = model.getMoney();
            }
        }

        mBinding.tvTotalValue.setText(Helper.getVNCurrency(totalMoney) + "đ");
        mBinding.tvServiceFeeValue.setText(Helper.getVNCurrency(serviceFee) + "đ");
        if (orderModel.getMin_money_to_free_service_price() != null) {
            mBinding.tvMinTotalValue.setVisibility(View.VISIBLE);
            mBinding.tvMinTotalTitle.setVisibility(View.VISIBLE);
            mBinding.tvMinTotalValue.setText(Helper.getVNCurrency(orderModel.getMin_money_to_free_service_price()) + "đ");
        } else {
            mBinding.tvMinTotalValue.setVisibility(View.GONE);
            mBinding.tvMinTotalTitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.rlDismiss) {
            dismiss();
        }
    }
}
