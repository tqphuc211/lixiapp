package com.opencheck.client.home.product.Control;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.home.product.bank_card.PopupNoSecurity;
import com.opencheck.client.home.product.communicate.OnCheckFingerPrintListener;
import com.opencheck.client.home.product.communicate.OnSettingListener;

import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class CheckSecurity extends LixiActivity {
    public static final int CHECK_SECURITY_CODE = 3698;
    public static final String ACTION_CODE = "action security code";
    private final int LOCK_REQUEST_CODE = 221;
    private final int SECURITY_SETTING_REQUEST_CODE = 233;
    private final int PERMISSION_REQUEST_CODE = 1596;

    private static final String KEY_NAME = "fingerprint key";
    private FingerprintManagerCompat.CryptoObject cryptoObject;
    private FingerprintManagerCompat fingerprintManager;

    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private KeyguardManager keyguardManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkFingerPrint();
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(
                    KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void checkFingerPrint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            // kiểm tra hardware
            fingerprintManager = FingerprintManagerCompat.from(this);
            if (fingerprintManager.isHardwareDetected()) {
                // nếu thiết bị có hỗ trợ

                // xin quyền
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.USE_FINGERPRINT}, PERMISSION_REQUEST_CODE);
                    return;
                }

                if (fingerprintManager.hasEnrolledFingerprints()) {
                    // nếu người dùng có đặt vân tay
                    if (!keyguardManager.isKeyguardSecure()) {
                        // nếu use không đặt bảo mật là pin, pass, pattern
                        try {
                            generateKey();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (initCipher()) {
                            cryptoObject = new FingerprintManagerCompat.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(this);
                            helper.startAuth(fingerprintManager, cryptoObject, new OnCheckFingerPrintListener() {
                                @Override
                                public void onFailed() {
                                    checkSecurity();
                                }

                                @Override
                                public void onSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
                                    setResult();
                                }

                                @Override
                                public void onHelp(int helpMsgId, CharSequence helpString) {
                                    checkSecurity();
                                }

                                @Override
                                public void onError(int errMsgId, CharSequence errString) {
                                    checkSecurity();
                                }
                            });
                            return;
                        }
                    }
                }
            }
        }
        checkSecurity();
    }

    private void checkSecurity() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        Intent i = keyguardManager.createConfirmDeviceCredentialIntent(
                getResources().getString(R.string.confirm),
                getResources().getString(R.string.security_use_card_content));
        try {
            //Start activity for result
            startActivityForResult(i, LOCK_REQUEST_CODE);
        } catch (Exception e) {
            //TODO chổ này yêu cầu là cho qua luôn ko cần check hoặc có thể thông báo đt ko đủ bảo mật rồi mới mở setting lên
            PopupNoSecurity popupNoSecurity = new PopupNoSecurity(activity, false, false, false);
            popupNoSecurity.show();
            popupNoSecurity.setSettingEventListener(new OnSettingListener() {
                @Override
                public void onSetting(int option) {
                    switch (option) {
                        case PopupNoSecurity.SETTING:
                            Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                            try {
                                startActivityForResult(intent, SECURITY_SETTING_REQUEST_CODE);
                            } catch (Exception ex) {

                            }
                            break;
                        case PopupNoSecurity.SKIP:
                            setResult();
                            break;
                        case PopupNoSecurity.CANCEL:
                            finish();
                            break;
                    }
                }
            });
        }
    }

    /**
     * method to return whether device has screen lock enabled or not
     **/
    private boolean isDeviceSecure() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        return keyguardManager.isKeyguardSecure();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkFingerPrint();
            } else {
                checkSecurity();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            finish();
            return;
        }
        switch (requestCode) {
            case LOCK_REQUEST_CODE:
                setResult();
                break;
            case SECURITY_SETTING_REQUEST_CODE:
                if (isDeviceSecure()) {
                    checkSecurity();
                }
                break;
        }
    }

    private void setResult() {
        Intent intent = getIntent();
        if (intent == null) {
            intent = new Intent();
        }
        this.setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {

    }
}
