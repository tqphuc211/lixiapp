package com.opencheck.client.home.setting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LoginPinCodeActivityBinding;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

import static android.view.View.VISIBLE;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class LoginPinCodeActivity extends LixiActivity {

    private ScrollView scrollview;
    private TextView tv2;
    private View viewLine;
    private TextView tvUserName;
    private TextView tvPhone;
    private RelativeLayout rlBack;
    private RelativeLayout rlIconBack;
    private TextView tv1;
    private RelativeLayout rlInPutPin;
    private LinearLayout lnPinCodeHint;
    private View viewPinHint0;
    private View viewPinHint1;
    private View viewPinHint2;
    private View viewPinHint3;
    private View viewPinHint4;
    private View viewPinHint5;
    private LinearLayout lnPinCode;
    private View viewPin0;
    private View viewPin1;
    private View viewPin2;
    private View viewPin3;
    private View viewPin4;
    private View viewPin5;
    private RelativeLayout rlClearText;
    private LinearLayout lnKeyboard;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnNone;
    private Button btn0;
    private RelativeLayout rlDel;
    private RelativeLayout rlForLogin;

    private String textPass = "";
    private String textPassConfirm = "";
    private Boolean isConfirmMode = false;

    private ArrayList<View> listViewPin = new ArrayList<View>();

    private UserModel userModel;

    private String actionAfterLogin = "";

    private Boolean hasLoginPass = false;

    private LoginPinCodeActivityBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.login_pin_code_activity);
        actionAfterLogin = getIntent().getExtras().getString(ConstantValue.ACTION_FOR_PIN, "");
        this.initView();
        this.initData();
    }


    private void initView() {

        scrollview = findViewById(R.id.scrollview);
        tv2 = findViewById(R.id.tv2);
        viewLine = findViewById(R.id.viewLine);
        tvUserName = findViewById(R.id.tvUserName);
        tvPhone = findViewById(R.id.tvPhone);
        rlBack = findViewById(R.id.rlBack);
        rlIconBack = findViewById(R.id.rlIconBack);
        tv1 = findViewById(R.id.tv1);
        rlInPutPin = findViewById(R.id.rlInPutPin);
        lnPinCodeHint = findViewById(R.id.lnPinCodeHint);
        viewPinHint0 = findViewById(R.id.viewPinHint0);
        viewPinHint1 = findViewById(R.id.viewPinHint1);
        viewPinHint2 = findViewById(R.id.viewPinHint2);
        viewPinHint3 = findViewById(R.id.viewPinHint3);
        viewPinHint4 = findViewById(R.id.viewPinHint4);
        viewPinHint5 = findViewById(R.id.viewPinHint5);
        lnPinCode = findViewById(R.id.lnPinCode);
        viewPin0 = findViewById(R.id.viewPin0);
        viewPin1 = findViewById(R.id.viewPin1);
        viewPin2 = findViewById(R.id.viewPin2);
        viewPin3 = findViewById(R.id.viewPin3);
        viewPin4 = findViewById(R.id.viewPin4);
        viewPin5 = findViewById(R.id.viewPin5);
        rlClearText = findViewById(R.id.rlClearText);
        lnKeyboard = findViewById(R.id.lnKeyboard);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btnNone = findViewById(R.id.btnNone);
        btn0 = findViewById(R.id.btn0);
        rlDel = findViewById(R.id.rlDel);
        rlForLogin = findViewById(R.id.rlForLogin);
    }

    private void initData() {
        userModel = UserModel.getInstance();
        rlBack.setOnClickListener(this);
        rlClearText.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnNone.setOnClickListener(this);
        btn0.setOnClickListener(this);
        rlDel.setOnClickListener(this);

        listViewPin.add(viewPin0);
        listViewPin.add(viewPin1);
        listViewPin.add(viewPin2);
        listViewPin.add(viewPin3);
        listViewPin.add(viewPin4);
        listViewPin.add(viewPin5);

        setViewDefault();
        setOvalPin();
    }

    private void setViewDefault() {
        tvUserName.setText(userModel.getFullname());
        tvPhone.setText(userModel.getPhone());

        if (!hasLoginPass) {
            if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_OPEN_APP)) {
                tv2.setText(LanguageBinding.getString(R.string.pin_setting_enter_app, activity));
            } else if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_CHANGE_PIN)) {
                tv2.setText(LanguageBinding.getString(R.string.pin_setting_change_pin, activity));
                rlBack.setVisibility(VISIBLE);
            } else if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_TURN_ON_PIN)) {
                tv2.setText(LanguageBinding.getString(R.string.pin_setting_require, activity));
                hasLoginPass = true;
            }
        }
    }

    private void appenText(String text) {
        Helper.vibratorClick(activity, 50);
        if (!hasLoginPass) {
            textPass = textPass + text;
            if (textPass.length() == 6) {
                if (textPass.equalsIgnoreCase(new AppPreferenceHelper(activity).getPinCode())) {
                    if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_OPEN_APP)) {
                        Bundle data = new Bundle();
                        data.putString(ConstantValue.ACTION_FOR_PIN, actionAfterLogin);
                        Intent intent = new Intent();
                        intent.putExtras(data);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_CHANGE_PIN)) {
                        hasLoginPass = true;
                        resetInput();
                    } else if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_TURN_ON_PIN)) {
                        Bundle data = new Bundle();
                        data.putString(ConstantValue.ACTION_FOR_PIN, actionAfterLogin);
                        Intent intent = new Intent();
                        intent.putExtras(data);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    Helper.vibratorClick(activity, 200);
                    resetInput();
                }
            }
            setOvalPin();
        } else {
            if (!isConfirmMode) {
                textPass = textPass + text;

                if (textPass.length() == 6) {
                    isConfirmMode = true;
                    textPassConfirm = "";
                }
                setOvalPin();
            } else {
                textPassConfirm = textPassConfirm + text;
                if (textPassConfirm.length() == 6) {
                    if (textPassConfirm.equalsIgnoreCase(textPass)) {
                        new AppPreferenceHelper(activity).savePinCode(textPassConfirm);
                        Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.success, activity));
                        (new AppPreferenceHelper(activity)).setPinRequired(true);
                        setResult(RESULT_OK);
                        finish();
                        return;
                    } else {
                        Helper.vibratorClick(activity, 200);
                        Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.pin_setting_pin_mismatch, activity));
                        resetInput();
                    }
                }
                setOvalPin();
            }
        }
    }

    private void setOvalPin() {

        for (int i = 0; i < listViewPin.size(); i++) {
            listViewPin.get(i).setAlpha(0);
        }

        if (!isConfirmMode) {
            for (int i = 0; i < textPass.length(); i++) {
                listViewPin.get(i).setAlpha(1);
            }
            if (!hasLoginPass) {
                tv1.setText(LanguageBinding.getString(R.string.pin_setting_input, activity));
                if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_CHANGE_PIN)) {
                    tv1.setText(LanguageBinding.getString(R.string.pin_setting_input_old_pin, activity));
                }
            } else {
                tv1.setText(LanguageBinding.getString(R.string.pin_setting_title, activity));
                if (actionAfterLogin.equalsIgnoreCase(ConstantValue.ACTION_CHANGE_PIN)) {
                    tv1.setText(LanguageBinding.getString(R.string.pin_setting_input_new_pin, activity));
                }
            }
        } else {
            for (int i = 0; i < textPassConfirm.length(); i++) {
                listViewPin.get(i).setAlpha(1);
            }
            tv1.setText(LanguageBinding.getString(R.string.pin_setting_confirm_pin, activity));
        }
    }

    private void clearText() {
        Helper.vibratorClick(activity, 50);
        if (!isConfirmMode) {
            if (textPass.length() > 0) {
                textPass = textPass.substring(0, textPass.length() - 1);
            }
        } else {
            if (textPassConfirm.length() > 0) {
                textPassConfirm = textPassConfirm.substring(0, textPassConfirm.length() - 1);
            }
        }
        setOvalPin();
    }

    private void resetInput() {
        Helper.vibratorClick(activity, 50);
        textPassConfirm = "";
        textPass = "";
        isConfirmMode = false;
        setOvalPin();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            activity.finish();
        } else if (v == rlClearText) {
            resetInput();
        } else if (v == btn1) {
            appenText("1");
        } else if (v == btn2) {
            appenText("2");
        } else if (v == btn3) {
            appenText("3");
        } else if (v == btn4) {
            appenText("4");
        } else if (v == btn5) {
            appenText("5");
        } else if (v == btn6) {
            appenText("6");
        } else if (v == btn7) {
            appenText("7");
        } else if (v == btn8) {
            appenText("8");
        } else if (v == btn9) {
            appenText("9");
        } else if (v == btn0) {
            appenText("0");
        } else if (v == rlDel) {
            clearText();
        }
    }

}
