package com.opencheck.client.home.survey;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class MyEditText extends EditText {
    Context context;

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            // User has pressed Back key. So hide the keyboard
//            InputMethodManager mgr = (InputMethodManager)
//                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
//            mgr.hideSoftInputFromWindow(this.getWindowToken(), 0);
            if (eventListener != null)
                eventListener.onbackPress();
        }
        return super.onKeyPreIme(keyCode, event);
    }

//    @Override
//    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
//        EmojiconHandler.addEmojis(getContext(), getText(), 50);
//    }

    private onHardBackPress eventListener;

    public interface onHardBackPress {
        public void onbackPress();
    }

    public void setHardbackPressListener(onHardBackPress listener) {
        eventListener = listener;
    }
}
