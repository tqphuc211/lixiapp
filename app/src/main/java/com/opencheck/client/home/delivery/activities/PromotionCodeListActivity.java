package com.opencheck.client.home.delivery.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.databinding.ActivityPromotionCodeListBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.PromotionListAdapter;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.ResponsePromotionModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;

import java.util.ArrayList;

public class PromotionCodeListActivity extends LixiActivity {

    public static final String ID = "ID";
    public static final String UUID = "UUID";
    public static final String PROMOTION_CODE = "PROMOTION CODE";
    public static final String MONEY_TOTAL = "MONEY TOTAL";
    public static final String PAYMENT_METHOD_CODE = "PAYMENT METHOD CODE";
    public static final String PROMOTION_RESPONSE = "PROMOTION RESPONSE";
    public static final int REQUEST_CODE_PROMOTION_CODE = 114;

    private PromotionListAdapter mAdapter;
    private long storeId = 0L;
    private String uuid = "";
    private String code = "";
    private long total = 0L;
    private String paymentMethodCode;
    private ArrayList<PromotionModel> list = new ArrayList<>();
    private ActivityPromotionCodeListBinding mBinding;

    public static void startPromotionCodeActivity(LixiActivity context, long _storeId, long _total, String _uuid, String _code, String payment_method_code) {
        Intent intent = new Intent(context, PromotionCodeListActivity.class);
        intent.putExtra(ID, _storeId);
        intent.putExtra(UUID, _uuid);
        intent.putExtra(PROMOTION_CODE, _code);
        intent.putExtra(MONEY_TOTAL, _total);
        intent.putExtra(PAYMENT_METHOD_CODE, payment_method_code);
        context.startActivityForResult(intent, REQUEST_CODE_PROMOTION_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_promotion_code_list);
        storeId = getIntent().getLongExtra(ID, 0L);
        total = getIntent().getLongExtra(MONEY_TOTAL, 0L);
        uuid = getIntent().getStringExtra(UUID);
        code = getIntent().getStringExtra(PROMOTION_CODE);
        paymentMethodCode = getIntent().getStringExtra(PAYMENT_METHOD_CODE);
        initView();
    }

    private void initView() {
        mBinding.ivBack.setOnClickListener(this);
        mBinding.txtSearch.setOnClickListener(this);

        mBinding.searchBar.setOnSearchEventListener(new SearchBar.OnSearchEventListener() {
            @Override
            public void onTextChange(@NonNull String text) {
                if (text.equals("")) {
                    search(text);
                }
            }

            @Override
            public void onComplete(@NonNull String text) {
                search(text);
            }
        });

        getListPromotionCode(null);
    }

    private String operateTextSearch(String text) {
        if (text.equals("")) {
            return null;
        }
        return text.trim().toUpperCase();
    }

    private void search(String text) {
        list.clear();
        getListPromotionCode(operateTextSearch(text));
        hideKeyboard();
    }

    private void setAdapter() {
        mBinding.rvPromotionCode.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mAdapter = new PromotionListAdapter(activity, list, total);
        mBinding.rvPromotionCode.setAdapter(mAdapter);
        mAdapter.setCheckedByCode(code);

        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                PromotionModel promotionModel = (PromotionModel) object;
                if (promotionModel.getMin_bill_money() <= total)
                    getUserCalculatePrice(promotionModel.getCode(), pos, promotionModel);
                else
                    Helper.showErrorDialog(activity, String.format(LanguageBinding.getString(R.string.deli_promotion_condition_apply, activity),
                            Helper.getVNCurrency(promotionModel.getMin_bill_money()) + "đ"));

                GlobalTracking.trackPromotionCodeUsed(storeId, promotionModel.getCode());
            }
        });

        mAdapter.setOnAvailableData(new PromotionListAdapter.OnAvailableData() {
            @Override
            public void onHaveData(boolean isHave) {
                if (!isHave) {
                    mBinding.lnNoResult.setVisibility(View.VISIBLE);
                    mBinding.lnResult.setVisibility(View.GONE);
                } else {
                    mBinding.lnNoResult.setVisibility(View.GONE);
                    mBinding.lnResult.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void getPromotionCodeByValue(String listCode, final int pos, final PromotionModel promotionModel) {
        DeliDataLoader.getPromotionByValue(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ResponsePromotionModel model = (ResponsePromotionModel) object;
                    if (model.getPromotions() == null || model.getPromotions().size() == 0) {
                        return;
                    }
                    mAdapter.setCheckedItem(pos);
                    Intent resultIntent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(PROMOTION_RESPONSE, model);
                    resultIntent.putExtras(bundle);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, uuid, listCode);
    }

    private void getUserCalculatePrice(final String listCode, final int pos, final PromotionModel promotionModel) {
        DeliDataLoader.getCalculatePrice(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ResponsePromotionModel model = (ResponsePromotionModel) object;
                    if (model == null)
                        return;
                    mAdapter.setCheckedItem(pos);
                    Intent resultIntent = new Intent();
                    Bundle bundle = new Bundle();
                    model.setPromotion_code(listCode);
                    bundle.putSerializable(PROMOTION_RESPONSE, model);
                    resultIntent.putExtras(bundle);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, uuid, paymentMethodCode, listCode);
    }

    private void getListPromotionCode(final String search) {
        DeliDataLoader.getPromotionList(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    list = (ArrayList<PromotionModel>) object;
                    if (list == null || list.size() == 0) {
                        mBinding.lnNoResult.setVisibility(View.VISIBLE);
                        mBinding.lnResult.setVisibility(View.GONE);
                        mBinding.tv.setText(LanguageBinding.getString(R.string.deli_promotion_waiting_code, activity));
                        mBinding.iv.setImageResource(R.drawable.ic_wait_promotion);
                        GlobalTracking.trackPromotionCodeSearch(storeId, search, false);
                    } else {
                        mBinding.lnNoResult.setVisibility(View.GONE);
                        mBinding.lnResult.setVisibility(View.VISIBLE);
                        setAdapter();
                        GlobalTracking.trackPromotionCodeSearch(storeId, search, true);
                    }
                } else {
                    GlobalTracking.trackPromotionCodeSearch(storeId, search, false);
                }
            }
        }, search, storeId); // id store demo - 1
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                hideKeyboard();
                finish();
                break;
            case R.id.txtSearch:
                search(mBinding.searchBar.getText());
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }
}
