package com.opencheck.client.home.merchant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.custom.MerchantMenuRecyclerView;
import com.opencheck.client.databinding.MerchantDetailMenuDialogBinding;
import com.opencheck.client.models.merchant.MenuModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class MerchantDetailMenuDialog extends LixiDialog {

    public MerchantDetailMenuDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;
    private LinearLayout ll_root_popup_menu;
    private TextView tv_pos;
    private ImageView iv_call;
    private RelativeLayout rl_back;
    private MerchantMenuRecyclerView rv;
    private int crPost;
    private int total;

    private MerchantDetailMenuDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = MerchantDetailMenuDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }


    private android.view.GestureDetector gestureDetector;

    private void findViews() {
        ll_root_popup_menu = findViewById(R.id.ll_root_popup_menu);
        tv_pos = findViewById(R.id.tv_pos);
        iv_call = findViewById(R.id.iv_call);
        rv = findViewById(R.id.rv);
        rl_back = findViewById(R.id.rl_back);
//
        rv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
//        initRecyclerView();
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(rv);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int first = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                setCrPost(first);
            }
        });

        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        gestureDetector = new android.view.GestureDetector(activity, new NewGestureDetector());
        ll_root_popup_menu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean event = gestureDetector.onTouchEvent(motionEvent);
                return event;
            }
        });

    }

    public void initDialogEvent() {
        iv_call.setOnClickListener(this);
    }

    private List<MenuModel> listMenu;
    private MerchantDetailMenuSlideAdapter adapterMenu;

    public void setData(List<MenuModel> listMenu, int pos, int total) {
        this.listMenu = listMenu;
        this.total = total;
        setCrPost(pos);

        adapterMenu = new MerchantDetailMenuSlideAdapter(activity, listMenu);
        rv.setAdapter(adapterMenu);

        rv.scrollToPosition(pos);
    }

    private Window.Callback winCallback = new Window.Callback() {
        @Override
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                dismiss();
            }
            return false;
        }

        @Override
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return false;
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            if (gestureDetector != null) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
            return false;
        }

        @Override
        public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            return false;
        }

        @Nullable
        @Override
        public View onCreatePanelView(int i) {
            return null;
        }

        @Override
        public boolean onCreatePanelMenu(int i, Menu menu) {
            return false;
        }

        @Override
        public boolean onPreparePanel(int i, View view, Menu menu) {
            return false;
        }

        @Override
        public boolean onMenuOpened(int i, Menu menu) {
            return false;
        }

        @Override
        public boolean onMenuItemSelected(int i, MenuItem menuItem) {
            return false;
        }

        @Override
        public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {

        }

        @Override
        public void onContentChanged() {

        }

        @Override
        public void onWindowFocusChanged(boolean b) {

        }

        @Override
        public void onAttachedToWindow() {

        }

        @Override
        public void onDetachedFromWindow() {

        }

        @Override
        public void onPanelClosed(int i, Menu menu) {

        }

        @Override
        public boolean onSearchRequested() {
            return false;
        }

        @Override
        public boolean onSearchRequested(SearchEvent searchEvent) {
            return false;
        }

        @Nullable
        @Override
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return null;
        }

        @Nullable
        @Override
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            return null;
        }

        @Override
        public void onActionModeStarted(ActionMode actionMode) {

        }

        @Override
        public void onActionModeFinished(ActionMode actionMode) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_call:
                if (eventListener != null)
                    eventListener.onCall();
                break;
            default:
                break;
        }
    }


    public void upDateData() {
        if (adapterMenu != null)
            adapterMenu.notifyDataSetChanged();
    }

    public int getCrPost() {
        return crPost;
    }

    public void setCrPost(int crPost) {
        if (crPost < 0)
            return;
        this.crPost = crPost;
        if (tv_pos != null)
            tv_pos.setText((crPost + 1) + "/" + total);

        if (crPost >= listMenu.size() - 5)
            if (eventListener != null)
                eventListener.onLoadmore();
    }


    public interface IDidalogEvent {
        void onLoadmore();

        void onCall();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    public class NewGestureDetector extends android.view.GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 120;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                dismiss();
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                dismiss();
            } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                dismiss();
            } else if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                dismiss();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
