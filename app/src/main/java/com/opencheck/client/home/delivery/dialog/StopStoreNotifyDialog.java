package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StoreNotifyDialogBinding;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

public class StopStoreNotifyDialog extends LixiDialog {

    public StopStoreNotifyDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private TextView tv_message, tv_ok;

    private StoreNotifyDialogBinding mBinding;
    private StoreOfCategoryModel mStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.store_notify_dialog, null, false);
        setContentView(mBinding.getRoot());
        tv_message = findViewById(R.id.tv_message);
        tv_ok = findViewById(R.id.tv_ok);
        tv_ok.setOnClickListener(this);
    }

    public void setData(StoreOfCategoryModel store) {
        this.mStore = store;
        tv_message.setText("Lý do: " + mStore.getPause_reason_message());
        if (mStore.getEnd_pause_time() != null)
            mBinding.tvOpenTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(mStore.getEnd_pause_time())));
        else mBinding.tvOpenTime.setText("----");
    }

    @Override
    public void onClick(View view) {
        if (view == tv_ok)
            eventListener.onStop(true);
        dismiss();
    }

    @Override
    public void onBackPressed() {

    }

    public interface IDidalogEvent {
        public void onStop(boolean isStopped);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
