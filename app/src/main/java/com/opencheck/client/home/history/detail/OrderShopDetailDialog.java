package com.opencheck.client.home.history.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.custom.SelectableRoundedImageView;
import com.opencheck.client.databinding.LixiHistoryOrderShopClassicDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.history.detail.controller.ControllerLixiShopCode;
import com.opencheck.client.home.history.detail.controller.ControllerLixiShopOrderDetailInfo;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.lixishop.LixiShopVoucherCodeModel;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.merchant.OrderInfoModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderShopDetailDialog extends LixiDialog {
    public OrderShopDetailDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, false, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu;
    private RelativeLayout rlTop;
    private RelativeLayout rl_back;
    private TextView tvTitle;
    private TextView tv_cancel;
    private RelativeLayout rlLine;
    private SelectableRoundedImageView iv_icon;
    private TextView tv_name;
    private TextView tv_point;
    private TextView tv_time;
    private TextView tv_state;
    private TextView tv_message;
    private LinearLayout ll_detail;
    private RelativeLayout rl_call;
    private LinearLayout ll_support;
    private SwipeRefreshLayout swiperefresh;
    TextView tv_support, tv_sdt;

    private LinearLayout ll_code;
    private LinearLayout ll_list_code;

    private String orderId;
    OrderDetailModel orderDTO;

    private LixiHistoryOrderShopClassicDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiHistoryOrderShopClassicDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findView();
    }


    public void findView() {
        tv_support = (TextView) findViewById(R.id.tv_support);
        tv_sdt = (TextView) findViewById(R.id.tv_sdt);
        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        rlTop = (RelativeLayout) findViewById(R.id.rlTop);
        rlTop = (RelativeLayout) findViewById(R.id.rlTop);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        rlLine = (RelativeLayout) findViewById(R.id.rlLine);
        iv_icon = (SelectableRoundedImageView) findViewById(R.id.iv_icon);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_point = (TextView) findViewById(R.id.tv_point);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_state = (TextView) findViewById(R.id.tv_state);
        tv_message = (TextView) findViewById(R.id.tv_message);
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        rl_call = (RelativeLayout) findViewById(R.id.rl_call);
        ll_support = (LinearLayout) findViewById(R.id.ll_support);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        ll_code = (LinearLayout) findViewById(R.id.ll_code);
        ll_list_code = (LinearLayout) findViewById(R.id.ll_list_code);

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ll_detail.removeAllViews();
                ll_list_code.removeAllViews();
                startLoadData();
            }
        });

        ll_support.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        tvTitle.setText(R.string.trans_detail_title);
        tv_support.setText(R.string.trans_detail_help);
        tv_sdt.setText(R.string.trans_detail_call);
    }

    public void setData(String orderId) {
        this.orderId = orderId;
        startLoadData();
    }

    public void startLoadData() {
        String url = "";

        DataLoader.getOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swiperefresh.setRefreshing(false);
                if (isSuccess) {
                    orderDTO = (OrderDetailModel) object;
                    showData();
                } else {
                    tv_name.setText("Không tìm thấy giao dịch");
                    dismiss();
                    Toast.makeText(activity, "Không tìm thấy giao dịch", Toast.LENGTH_LONG).show();
                }
            }
        }, orderId);
    }

    public void showData() {


        if (orderDTO.getCode_list() == null || orderDTO.getCode_list().size() == 0) {
            tv_message.setVisibility(View.VISIBLE);
            tv_message.setText(orderDTO.getShipping_info());
            ll_code.setVisibility(View.GONE);
            tv_cancel.setVisibility(View.VISIBLE);
            if (orderDTO.getState().equals("cancel") || orderDTO.getState().equals("done") ||
                    orderDTO.getState().equals("payment_expired")) {
                tv_cancel.setVisibility(View.GONE);
            } else {
                tv_cancel.setVisibility(View.VISIBLE);
            }
        } else {
            tv_message.setVisibility(View.GONE);
            ll_code.setVisibility(View.VISIBLE);
            tv_cancel.setVisibility(View.GONE);
            ll_code.setOnClickListener(this);
            for (int i = 0; i < orderDTO.getCode_list().size(); i++) {
                LixiShopVoucherCodeModel code = orderDTO.getCode_list().get(i);
                boolean isLastItem = false;
                if (i == orderDTO.getCode_list().size() - 1) {
                    isLastItem = true;
                }
                ControllerLixiShopCode item = new ControllerLixiShopCode(activity, code, orderDTO, ll_list_code, isLastItem);
            }
        }


        ImageLoader.getInstance().displayImage(orderDTO.getImage(), iv_icon, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(orderDTO.getName());
        tv_point.setText(Helper.getVNCurrency(orderDTO.getPoint()) + "đ");
        tv_time.setText(orderDTO.getDate());
        tv_state.setText(orderDTO.getStateVN());
        tv_state.setTextColor(orderDTO.getStateColor());
        for (int i = 0; i < orderDTO.getInfo().size(); i++) {
//            boolean isVoucher = orderDTO.getCode_list() != null && orderDTO.getCode_list().size() > 0;
            boolean isVoucher = orderDTO.getProduct_type().equals("evoucher") || orderDTO.getProduct_type().contains("voucher");
            OrderInfoModel dto = orderDTO.getInfo().get(i);

            if (isVoucher) {
                tv_cancel.setVisibility(View.GONE);
            }

            ControllerLixiShopOrderDetailInfo orderDtailItem = new ControllerLixiShopOrderDetailInfo(activity, 0, orderDTO.getState(), dto.getName(), dto.getValue(), "#" + dto.getColor(), dto.isBold(), (i == 0 && !isVoucher) ? R.drawable.ic_qr_l : 0, ll_detail);
            if (i == 0 && !isVoucher) {
                orderDtailItem.setIOrderDetailControllerEventListener(new ControllerLixiShopOrderDetailInfo.IOrderDetailControllerEvent() {
                    @Override
                    public void onClick() {
                        ShopQRCodeDialog dialog = new ShopQRCodeDialog(activity, false, true, false);
                        dialog.setCancelable(true);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                        //doi orderID thanh OrderDetailOutModel, hien tai dang thong nhat la getOrderId(khong get id)
                        dialog.setData(LanguageBinding.getString(R.string.transaction_code, activity), orderDTO.getOrder_id());
                    }
                });
            }
        }
        if (orderDTO.getProduct_type().equals("evoucher") || orderDTO.getProduct_type().equals("combo")) {
            tv_cancel.setVisibility(View.GONE);
        } else tv_cancel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                dismiss();
                break;
            case R.id.ll_code:
                break;
            case R.id.tv_cancel:
                ManualDismissDialog dialog = new ManualDismissDialog(activity, false, true, false);
                dialog.show();
                dialog.setTitle(LanguageBinding.getString(R.string.trans_detail_cancel, activity));
                dialog.setMessage(String.format(LanguageBinding.getString(R.string.trans_hotline_cancel, activity), ConfigModel.getInstance(activity).getDefaultSupportPhone()));
                dialog.setIDidalogEventListener(new ManualDismissDialog.IDialogEvent() {
                    @Override
                    public void onOk() {
                        Helper.callPhoneSupport(activity);
                    }
                });
                break;
            case R.id.ll_support:
                Helper.callPhoneSupport(activity);
                break;
            default:
                break;
        }
    }
}
