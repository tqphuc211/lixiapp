package com.opencheck.client.home.flashsale.Model;

public class FlashSaleTime {

    /**
     * end_time : 0
     * id : 0
     * name : string
     * start_time : 0
     * state : string
     */

    private long end_time;
    private int id;
    private String name;
    private long start_time;
    private String state;

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
