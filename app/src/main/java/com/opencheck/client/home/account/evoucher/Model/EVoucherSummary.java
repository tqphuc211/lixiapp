package com.opencheck.client.home.account.evoucher.Model;

import com.opencheck.client.models.LixiModel;

public class EVoucherSummary extends LixiModel {
//    count:	integer
//    default: 0
//    total_cashback_price:	integer
//    default: 0
//    total_payment_discount:	integer
//    default: 0
//    total_payment_discount_price:	integer
//    default: 0
//    total_payment_price:	integer
//    default: 0
//    total_price:	integer
//    default: 0

    private long count;
    private long total_cashback_price;
    private long total_payment_discount;
    private long total_payment_discount_price;
    private long total_payment_price;
    private long total_price;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getTotal_cashback_price() {
        return total_cashback_price;
    }

    public void setTotal_cashback_price(long total_cashback_price) {
        this.total_cashback_price = total_cashback_price;
    }

    public long getTotal_payment_discount() {
        return total_payment_discount;
    }

    public void setTotal_payment_discount(long total_payment_discount) {
        this.total_payment_discount = total_payment_discount;
    }

    public long getTotal_payment_discount_price() {
        return total_payment_discount_price;
    }

    public void setTotal_payment_discount_price(long total_payment_discount_price) {
        this.total_payment_discount_price = total_payment_discount_price;
    }

    public long getTotal_payment_price() {
        return total_payment_price;
    }

    public void setTotal_payment_price(long total_payment_price) {
        this.total_payment_price = total_payment_price;
    }

    public long getTotal_price() {
        return total_price;
    }

    public void setTotal_price(long total_price) {
        this.total_price = total_price;
    }
}
