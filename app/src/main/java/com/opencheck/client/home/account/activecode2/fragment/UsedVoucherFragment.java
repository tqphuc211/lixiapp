package com.opencheck.client.home.account.activecode2.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.UsedVoucherFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.activecode2.adapter.VoucherAtStoreAdapter;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.NearestStoreModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class UsedVoucherFragment extends LixiFragment {

    private RecyclerView rv_list;
    private RelativeLayout rlBack, rl_buy;
    private LinearLayoutManager mLayoutManager;
    private VoucherAtStoreAdapter adapter;
    private SwipeRefreshLayout srf_refresh;
    private LinearLayout ll_buy;
    private ArrayList<LixiShopEvoucherBoughtModel> list_voucher = new ArrayList<>();
    private ArrayList<LixiShopEvoucherBoughtModel> usedList = new ArrayList<>();
    private ArrayList<LixiShopEvoucherBoughtModel> unusedList = new ArrayList<>();
    private int page = 1;
    private int record_per_page = DataLoader.record_per_page;
    private String store_id = "";
    private boolean isLoading = false;
    private NearestStoreModel storeModel;
    private int countSelected = 0;
    private boolean isOutOfPage = false;
    private int total;

    public UsedVoucherFragment() {
        // Required empty public constructor
    }

    private UsedVoucherFragmentBinding mBinding;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = UsedVoucherFragmentBinding.inflate(inflater, container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        storeModel = new Gson().fromJson(getArguments().getString("store"), NearestStoreModel.class);
        this.initView(mBinding.getRoot());
        rootview = mBinding.getRoot();
        return mBinding.getRoot();
    }

    private void initView(View v){
        rlBack = v.findViewById(R.id.rlBack);
        rv_list = v.findViewById(R.id.rv_list);
        ll_buy = (LinearLayout) v.findViewById(R.id.ll_buy);
        rl_buy = (RelativeLayout) v.findViewById(R.id.rl_buy);
        srf_refresh = v.findViewById(R.id.srf_refresh);

        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rlBack.setOnClickListener(this);
        rl_buy.setOnClickListener(this);

        srf_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                isOutOfPage = false;
                if(adapter != null) {
                    list_voucher.clear();
                    unusedList.clear();
                    adapter.notifyDataSetChanged();
                }
                ll_buy.setVisibility(View.GONE);

                startLoadData();
            }
        });

        rv_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(recyclerView.computeVerticalScrollOffset() > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                        startLoadData();
                        isLoading = true;
                    }
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoadData();
            }
        }, 100);
    }

    private void setAdapter(){
        adapter = new VoucherAtStoreAdapter(activity, unusedList, list_voucher, storeModel, ll_buy, total);
        rv_list.setHasFixedSize(true);
        rv_list.setLayoutManager(mLayoutManager);
        rv_list.setAdapter(adapter);
    }

    int index = 0;

    private void filterVoucher(ArrayList<LixiShopEvoucherBoughtModel> arr, int start){
        int length = 0;
        if(record_per_page*page < index)
            length = record_per_page * page;
        else
            length = index;
        for(int i = start; i < length; i++){
            unusedList.add(arr.get(i));
        }
    }

    private void startLoadData(){
        if(!isLoading) {
            if(!isOutOfPage) {
                srf_refresh.setRefreshing(false);
                DataLoader.getStoreBoughtEVoucher(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            ApiWrapperForListModel<LixiShopEvoucherBoughtModel> res = (ApiWrapperForListModel<LixiShopEvoucherBoughtModel>) object;
                            total = res.get_meta().getTotal();
                            if (page == 1) {
                                list_voucher = res.getRecords();
                                if(list_voucher != null)
                                    index = list_voucher.get(0).getEvoucher_selled_count();
                                filterVoucher(list_voucher, 0);
                                setAdapter();
                                setOutOfPage(list_voucher);
                            } else {
                                ArrayList<LixiShopEvoucherBoughtModel> list = res.getRecords();
                                if (list != null && list.size() > 0 && list_voucher != null) {
                                    setOutOfPage(list);
                                    list_voucher.addAll(list);
                                    filterVoucher(list_voucher, unusedList.size());
                                    adapter.setUnusedList(unusedList);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            isLoading = false;
                            page++;
                        } else {
                            isLoading = true;
                        }
                    }
                }, page, storeModel.getId());
            }
        }
    }

    private void setOutOfPage(ArrayList<LixiShopEvoucherBoughtModel> arrayList){
        if(arrayList.size() < record_per_page){
            isOutOfPage = true;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == rl_buy){
            Bundle data = new Bundle();
            data.putString("arr_ev", new Gson().toJson(adapter.getChosenVoucherList()));
            data.putString("store", new Gson().toJson(storeModel));
            ConfirmActiveCodeFragment fragment = new ConfirmActiveCodeFragment();
            fragment.setArguments(data);
            ((ActiveCode2Activity) activity).replaceFragment(fragment);
        }
        else if(view == rlBack)
            activity.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActiveCode2Activity) activity).toolbar.setVisibility(View.GONE);
    }

}
