package com.opencheck.client.home.delivery.controller;

import android.databinding.DataBindingUtil;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StorePolicyCheckoutItemLayoutBinding;
import com.opencheck.client.home.delivery.model.StorePolicyModel;

public class StorePolicyCheckoutController {
    private LixiActivity activity;
    private LinearLayout lnRoot;
    private StorePolicyCheckoutItemLayoutBinding mBinding;
    private StorePolicyModel storePolicyModel;

    public StorePolicyCheckoutController(LixiActivity activity, LinearLayout _lnRoot, StorePolicyModel storePolicyModel) {
        this.activity = activity;
        this.lnRoot = _lnRoot;
        this.storePolicyModel = storePolicyModel;

        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.store_policy_checkout_item_layout, null, false);
        this.initData();
        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(mBinding.getRoot());
            }
        });
    }

    private void initData() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < storePolicyModel.getList_payment_method_name().size(); i++) {
            builder.append(storePolicyModel.getList_payment_method_name().get(i));
            if (i != storePolicyModel.getList_payment_method_name().size() - 1)
                builder.append(",");
        }
        mBinding.tvPolicy.setText(Html.fromHtml(storePolicyModel.getName() + " (áp dụng với " + builder.toString() + ")"));
    }
}