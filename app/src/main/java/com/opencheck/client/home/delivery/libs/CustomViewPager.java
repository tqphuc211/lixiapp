package com.opencheck.client.home.delivery.libs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {
    private boolean enabled;
    private OnSwipeOutListener listener;

    public CustomViewPager(@NonNull Context context) {
        super(context);
        this.enabled = true;
    }

    public CustomViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    public void setListener(OnSwipeOutListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return enabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if(listener != null) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    listener.onSwipeOut();
                    break;
            }
        }
        return enabled && super.onInterceptTouchEvent(event);
    }

    public void setPagingEnabled(boolean enable){
        this.enabled = enable;
    }

    public boolean isPagingEnabled(){
        return enabled;
    }

    public interface OnSwipeOutListener {
        void onSwipeOut();
    }
}
