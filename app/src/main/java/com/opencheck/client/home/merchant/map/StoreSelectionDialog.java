package com.opencheck.client.home.merchant.map;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StoreSelectionDialogBinding;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class StoreSelectionDialog extends LixiDialog {

    public static String ITEM_SELECTED = "ITEM_SELECTED";
    private ItemClickListener itemClickListener;
    private String action = "";

    private String title = "";
    private ArrayList<String> listItem = new ArrayList<String>();

    private RelativeLayout rlRoot;
    private TextView tvTitle;
    private ListView lvItem;


    public StoreSelectionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    @Override
    public void onClick(View view) {

    }

    public void setData(ArrayList<String> _listItem, String title, ItemClickListener _itemClickListener, String _action) {
        this.itemClickListener = _itemClickListener;
        this.listItem = _listItem;
        this.title = title;
        this.action = _action;
        initViews();
    }

    private StoreSelectionDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = StoreSelectionDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    private void findViews() {
        rlRoot = findViewById(R.id.rlRoot);
        tvTitle = findViewById(R.id.tvTitle);
        lvItem = findViewById(R.id.lvItem);
    }

    private void initViews() {
        lvItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();
                itemClickListener.onItemCLick(position, action, listItem.get(position), 0L);
            }
        });

        tvTitle.setText(title);

        StringAdapter stringAdapter = new StringAdapter(activity, R.layout.item_select_city_layout);
        lvItem.setAdapter(stringAdapter);
        stringAdapter.addListItems(listItem);
    }


    public class StringAdapter extends ArrayAdapter<String> {


        private Activity activity;
        private int layoutItem;

        class ViewHolder {
            private TextView tvItem;

        }

        public StringAdapter(Activity _activity, int _layoutItem) {
            super(_activity, _layoutItem);
            this.activity = _activity;
            this.layoutItem = _layoutItem;
        }

        public void addListItems(List<String> popups) {
            for (String obj : popups) {
                add(obj);
            }
            notifyDataSetChanged();
        }

        public ArrayList<String> getListItems() {
            ArrayList<String> listItems = new ArrayList<String>();
            for (int i = 0; i < getCount(); i++) {
                listItems.add(getItem(i));
            }
            return listItems;
        }


        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(activity);
                convertView = inflater.inflate(layoutItem, null);

                holder.tvItem = (TextView) convertView.findViewById(R.id.tvItem);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final String item = getItem(position);
            holder.tvItem.setText(item);


            return convertView;
        }
    }
}
