package com.opencheck.client.home.delivery.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FavoriteItemLayoutBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class RecommendStoreAdapter extends RecyclerView.Adapter<RecommendStoreAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<StoreOfCategoryModel> listRecommendStore;

    public RecommendStoreAdapter(LixiActivity activity, ArrayList<StoreOfCategoryModel> storeList) {
        this.activity = activity;
        this.listRecommendStore = storeList;
    }

    public void setListRecommendStore(ArrayList<StoreOfCategoryModel> listRecommendStore) {
        this.listRecommendStore = listRecommendStore;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FavoriteItemLayoutBinding favoriteItemLayoutBinding =
                FavoriteItemLayoutBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(favoriteItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StoreOfCategoryModel store = listRecommendStore.get(position);
//        ImageLoader.getInstance().displayImage(store.getCover_link(), holder.iv_avatar, LixiApplication.getInstance().optionsNomal);

        LixiImage.with(activity).load(store.getCover_link()).size(ImageSize.AUTO).into(holder.iv_avatar);

        holder.tv_name.setText(store.getName());
        holder.tv_time.setText(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
        if (store.getShipping_distance() >= 100) {
            double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
            holder.tv_distance.setText(String.valueOf(distance) + " km");
        } else
            holder.tv_distance.setText(store.getShipping_distance() + " m");

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < listRecommendStore.get(position).getList_tag().size(); i++) {
            builder.append("#").append(listRecommendStore.get(position).getList_tag().get(i).getName()).append(" ");
        }
        holder.tv_tags.setText(builder.toString());


        if (store.getMin_money_to_free_ship_price() == null) {
            holder.tv_shipping_fee.setVisibility(View.GONE);
            // holder.tv_shipping_fee.setText(Helper.getVNCurrency(store.getShip_price()) + "đ");
        } else {
            holder.tv_shipping_fee.setVisibility(View.VISIBLE);
            holder.tv_shipping_fee.setText(LanguageBinding.getString(R.string.store_detail_free_ship, activity));
        }

        if (store.getPromotion_text() != null && store.getPromotion_text().length() > 0) {
            holder.ll_promotion.setVisibility(View.VISIBLE);
            holder.tv_promotion.setText(store.getPromotion_text());
            holder.tv_name.setMaxLines(1);
        } else {
            holder.ll_promotion.setVisibility(View.GONE);
            holder.tv_name.setMaxLines(2);
        }

        if (store.isIs_opening()) {
            holder.tv_open_time.setVisibility(View.GONE);
            holder.iv_store_close.setVisibility(View.GONE);
            holder.iv_avatar.setColorFilter(null);

        } else {
            holder.tv_open_time.setText(String.format(LanguageBinding.getString(R.string.open_close_time, activity), store.getOpen_time_text()));
            holder.tv_open_time.setVisibility(View.VISIBLE);
            holder.iv_store_close.setVisibility(View.VISIBLE);
            holder.iv_avatar.setColorFilter(Color.parseColor("#CC000000"));
        }

        int width = activity.widthScreen * 5 / 9;
        ViewGroup.LayoutParams params = holder.iv_avatar.getLayoutParams();
        params.height = 3 * width / 4;
        params.width = width;
        holder.iv_avatar.setLayoutParams(params);

        if (position == 0 || position == listRecommendStore.size() - 1) {
            float density = activity.getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (activity.getResources().getDimension(R.dimen.value_3) * density);
            holder.root_view.setPadding(paddingPixel, 0, paddingPixel, paddingPixel);
        } else {
            float density = activity.getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (activity.getResources().getDimension(R.dimen.value_3) * density);
            holder.root_view.setPadding(0, 0, paddingPixel, paddingPixel);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(params.width, ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.root_view.setLayoutParams(layoutParams);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreDetailActivity.startStoreDetailActivity(activity, store.getId(), TrackingConstant.ContentSource.HOME);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listRecommendStore == null ? 0 : listRecommendStore.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_avatar, iv_store_close;
        private TextView tv_name, tv_time, tv_distance, tv_tags, tv_shipping_fee, tv_promotion, tv_open_time;
        private View rootView;
        private LinearLayout ll_promotion;
        private RelativeLayout root_view;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            iv_avatar = itemView.findViewById(R.id.iv_avatar);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_promotion = itemView.findViewById(R.id.tv_promotion);
            tv_tags = itemView.findViewById(R.id.tv_tags);
            tv_shipping_fee = itemView.findViewById(R.id.tv_shipping_fee);
            root_view = itemView.findViewById(R.id.root_view);
            ll_promotion = itemView.findViewById(R.id.ll_promotion);
            tv_open_time = itemView.findViewById(R.id.tv_open_time);
            iv_store_close = itemView.findViewById(R.id.iv_store_close);
        }
    }
}
