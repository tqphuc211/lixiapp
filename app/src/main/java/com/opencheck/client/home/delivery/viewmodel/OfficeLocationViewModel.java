package com.opencheck.client.home.delivery.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.opencheck.client.home.delivery.model.DeliAddressModel;

public class OfficeLocationViewModel extends ViewModel {
    private MutableLiveData<DeliAddressModel> officeLocation;

    public MutableLiveData<DeliAddressModel> getOfficeLocation() {
        if (officeLocation == null){
            officeLocation = new MutableLiveData<>();
        }
        return officeLocation;
    }
}
