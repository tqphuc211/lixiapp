package com.opencheck.client.home.product.cart_old;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopBuySuccessCashDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.history.detail.controller.OrderDetailInfoController;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.merchant.OrderInfoModel;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class SuccessCashDialog extends LixiTrackingDialog {
    public SuccessCashDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu, ll_detail;
    private Button btnClose;
    private TextView tv_message, tv_view_code, tv_change_gift;

    private LixiShopBankTransactionResult transactionResult;

    SpannableStringBuilder spanMessage;
    String mss = "";
    String title = "";
    String okText = "";
    OrderDetailModel orderDTO;


    public void setData(String title, String okText, String ms, LixiShopBankTransactionResult transactionResult) {
        this.mss = ms;
        this.title = title;
        this.okText = okText;
        this.transactionResult = transactionResult;
        initData();
    }

    private void initData() {
//        if (spanMessage != null)
//            tv_message.setText(spanMessage);
//        else if (mss.length() > 0)
//            tv_message.setText(Html.fromHtml(mss));

        if (okText.length() > 0) {
            tv_message.setText(Html.fromHtml(okText));
        }

        tv_view_code.setOnClickListener(this);
        tv_change_gift.setOnClickListener(this);
        btnClose.setOnClickListener(this);

        getOrderdetail();
    }

    private LixiShopBuySuccessCashDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopBuySuccessCashDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }

    private void findViews() {
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        tv_change_gift = (TextView) findViewById(R.id.tv_change_gift);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_view_code = (TextView) findViewById(R.id.tv_view_code);
        btnClose = (Button) findViewById(R.id.btnClose);
    }

    private void showOrderDetailInfo() {
        for (OrderInfoModel dto : orderDTO.getInfo())
            new OrderDetailInfoController(activity, 0, dto.getName(), dto.getValue(), Long.parseLong("FF" + dto.getColor(), 16), !dto.isBold(), ll_detail);
    }

    private void getOrderdetail() {
        DataLoader.getOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    orderDTO = (OrderDetailModel) object;
                    showOrderDetailInfo();
                }
            }
        }, transactionResult.getOrder_id());
    }

    public void initDialogEvent() {
        // rl_dismiss.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
            case R.id.tv_view_code:
//                dismiss();
//                HomeActivity.getInstance().tab = 4;
//                LoginHelper.startHomeActivity(activity);
//                activity.finish();
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(HomeActivity.RESUME_NAVIGATE, 4);
                activity.startActivity(intent);
                dismiss();
                break;
            case R.id.tv_change_gift:
//                dismiss();
//                HomeActivity.getInstance().tab = 2;
//                LoginHelper.startHomeActivity(activity);
//                activity.finish();
                Intent intent1 = new Intent(activity, HomeActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent1.putExtra(HomeActivity.RESUME_NAVIGATE, 3);
                activity.startActivity(intent1);
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {
        public void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
