package com.opencheck.client.home.product.gateway.result;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentAtBankFailLayoutBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.utils.Helper;

/**
 * Created by I'm Sugar on 5/24/2018.
 */

public class PaymentAtBankFailDialog extends LixiTrackingDialog {

    private ImageView img;
    private NestedScrollView scollView;
    private TextView tvPhone;
    private TextView tvProductName;
    private TextView tvProductPrice;
    private TextView tvTotalProduct;
    private TextView tvDetailTrans;
    private TextView tv_tam_tinh;
    private TextView full_price;
    private TextView tv_lixi_use;
    private TextView tv_tong_tien;
    private TextView tvTotalPrice;

    private LinearLayout ll_payment_method;
    private TextView tv_payment_method;

    private LinearLayout lnBottom;
    private TextView tvRebuy;
    private TextView tvCancel;

    private String phone = "";
    private ProductCashVoucherDetailModel productModel;
    private int totalProduct = 0;
    private long totalPrice = 0;
    private long totalLixiBonus = 0;
    private String orderID = "";
    private String paymentMethod = "";
    private long lixi_discount = 0;


    public PaymentAtBankFailDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private PaymentAtBankFailLayoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PaymentAtBankFailLayoutBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    private void findViews() {
        img = (ImageView) findViewById(R.id.img);
        scollView = (NestedScrollView) findViewById(R.id.scollView);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvProductPrice = (TextView) findViewById(R.id.tvProductPrice);
        tvTotalProduct = (TextView) findViewById(R.id.totalProduct);
        tvDetailTrans = (TextView) findViewById(R.id.tvDetailTrans);
        tv_tam_tinh = (TextView) findViewById(R.id.tv_tam_tinh);
        full_price = (TextView) findViewById(R.id.full_price);
        tv_lixi_use = (TextView) findViewById(R.id.tv_lixi_use);
        tv_tong_tien = (TextView) findViewById(R.id.tv_tong_tien);
        tvTotalPrice = (TextView) findViewById(R.id.totalPrice);
        ll_payment_method = (LinearLayout) findViewById(R.id.ll_payment_method);
        tv_payment_method = (TextView) findViewById(R.id.tv_payment_method);
        lnBottom = (LinearLayout) findViewById(R.id.lnBottom);
        tvRebuy = (TextView) findViewById(R.id.tvRebuy);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvRebuy.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvDetailTrans.setOnClickListener(this);
    }

    public void loadData(ProductCashVoucherDetailModel productModel,
                         String phone,
                         int totalProduct,
                         long totalPrice,
                         long totalLixiBonus,
                         String orderID,
                         String paymentMethod,
                         long lixi_discount) {
        this.productModel = productModel;
        this.phone = phone;
        this.totalProduct = totalProduct;
        this.totalPrice = totalPrice;
        this.totalLixiBonus = totalLixiBonus;
        this.orderID = orderID;
        this.paymentMethod = paymentMethod;
        this.lixi_discount = lixi_discount;
        initData();

    }

    private void initData() {
        String text = "Rất tiếc giao dịch bị thất bại." + "<br>" + "</br>" + " Xin thử lại hoặc liên hệ " + "<b>" + Helper.getPhoneSupport(activity) + "</b>" + "\n" + " để Lixi hỗ trợ bạn.";
        tvPhone.setText(Html.fromHtml(text));
        tvProductName.setText(productModel.getName());
        long productPrice = productModel.getFlash_sale_id() > 0 ? productModel.getFlash_sale_info().getPayment_discount_price() : productModel.getPayment_discount_price();
        tvProductPrice.setText(Helper.getVNCurrency(productPrice) + "đ");
        tvTotalProduct.setText("x" + totalProduct);

        full_price.setText(Helper.getVNCurrency(totalPrice) + "đ");
        if (lixi_discount == 0){
            tv_lixi_use.setText((Helper.getVNCurrency(lixi_discount)) + "đ");
        }else {
            tv_lixi_use.setText("-" + (Helper.getVNCurrency(lixi_discount)) + "đ");
        }

        tvTotalPrice.setText(Helper.getVNCurrency(totalPrice - lixi_discount) + "đ");

        if (paymentMethod != null && paymentMethod.length() > 0) {
            tv_payment_method.setText(paymentMethod);
        } else ll_payment_method.setVisibility(View.GONE);

        id = Long.valueOf(orderID.substring(orderID.indexOf("-") + 1, orderID.length()));
        trackScreen();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvCancel) {
            onEventOK(true);
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            dismiss();
        }
        if (view.getId() == R.id.tvRebuy) {
            onEventOK(false);
            dismiss();
        }
        if (view.getId() == R.id.tvDetailTrans) {
            OrderShopDetailDialog orderShopDetailDialog = new OrderShopDetailDialog(activity, false, false, false);
            orderShopDetailDialog.show();
            orderShopDetailDialog.setData(orderID);
        }

    }

    private void onEventOK(boolean ok) {
        if (eventListener != null) {
            eventListener.onOk(ok);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onOk(boolean success);
    }

    protected PaymentAtBankFailDialog.IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            PaymentAtBankFailDialog.IDidalogEvent listener) {
        eventListener = listener;
    }

}