package com.opencheck.client.home.LixiHomeV25.MerchantList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class AdapterMerchant extends RecyclerView.Adapter<AdapterMerchant.ViewHolder> {

    private LixiActivity activity;

    public void setListMerchant(ArrayList<MerchantModel> listMerchant) {
        this.listMerchant = listMerchant;
        notifyDataSetChanged();
    }

    public ArrayList<MerchantModel> getListMerchant() {
        return listMerchant;
    }

    private ArrayList<MerchantModel> listMerchant;

    public AdapterMerchant(LixiActivity activity, ArrayList<MerchantModel> listCategory) {
        this.activity = activity;
        this.listMerchant = listCategory;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lixi_home_merchant_item, null, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (position == 0)
            holder.v_space.setVisibility(View.GONE);

        MerchantModel merchantModel = listMerchant.get(position);

        ImageLoader.getInstance().displayImage(merchantModel.getLogo(), holder.iv_logo, LixiApplication.getInstance().optionsNomal);
        holder.tv_name.setText(merchantModel.getName());
        holder.tv_voucher_count.setText(String.format(LanguageBinding.getString(R.string.promotion, activity), merchantModel.getTotal_evoucher_count()));
        holder.tv_lixi_count.setText("+" + Helper.getVNCurrency(merchantModel.getTotal_cashback_price()) + " lixi");
        holder.tv_store_count.setText(merchantModel.getCount_store() + " cửa hàng");

        holder.rootView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if (listMerchant == null)
            return 0;
        return listMerchant.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        private View v_space;
        private ImageView iv_logo;
        private TextView tv_name;
        private TextView tv_voucher_count;
        private TextView tv_store_count;
        private TextView tv_lixi_count;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            v_space = rootView.findViewById(R.id.v_space);
            iv_logo = (ImageView) rootView.findViewById(R.id.iv_logo);
            tv_name = (TextView) rootView.findViewById(R.id.tv_name);
            tv_voucher_count = (TextView) rootView.findViewById(R.id.tv_voucher_count);
            tv_store_count = (TextView) rootView.findViewById(R.id.tv_store_count);
            tv_lixi_count = (TextView) rootView.findViewById(R.id.tv_lixi_count);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MerchantDetailActivity.startMerchantDetailActivity(activity, listMerchant.get((int) v.getTag()).getId(), null);
                }
            });
        }
    }
}
