package com.opencheck.client.home.setting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.OnlyTextLayoutDialogBinding;

import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class InstructiondDetailDialog extends LixiDialog implements AdapterView.OnItemClickListener {
    public InstructiondDetailDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    //region Layout Variable
    private View view;
    private TextView tv_answer;
    private WebView wv_answer;

    private RelativeLayout rlBack;
    private TextView tvTitle;
    //endregion

    //region Logic Variable
    private String answers;
    private String question;

    public static final String EXTRA_ANSWERS = "EXTRA_ANSWERS";
    public static final String EXTRA_QUESTION = "EXTRA_QUESTION";

    //endregion


    //region Processing UI
    private OnlyTextLayoutDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = OnlyTextLayoutDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findView();
    }

    public void findView() {

        tv_answer = (TextView) findViewById(R.id.tv_answer);
        wv_answer = (WebView) findViewById(R.id.wv_answer);

        rlBack = (RelativeLayout) findViewById(R.id.rlBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        rlBack.setOnClickListener(this);

    }

    //endregion

    //region Handle UI Logic

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }


    //endregion

    //region Handle background processing
    public void setData(String question, String answers) {
        this.question = question;
        this.answers = answers;
        tvTitle.setText(question);

        tv_answer.setText(answers);
        tv_answer.setVisibility(View.GONE);


        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        wv_answer.loadDataWithBaseURL("", answers, mimeType, encoding, "");
    }
    //endregion

}
