package com.opencheck.client.home.product.gateway;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.CheckOutCancelTransLayoutBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;

/**
 * Created by I'm Sugar on 5/23/2018.
 */

public class DialogCancelTransaction extends LixiTrackingDialog {

    RelativeLayout rlScreen;
    TextView tvCont, tvCancel;
    DialogTransactions transactionsDialog;

    public DialogCancelTransaction(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    public void setData(DialogTransactions transactionsDialog) {
        this.transactionsDialog = transactionsDialog;
    }

    private CheckOutCancelTransLayoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = CheckOutCancelTransLayoutBinding
                .inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        rlScreen = findViewById(R.id.rlScreen);
        tvCont = findViewById(R.id.tvCont);
        tvCancel = findViewById(R.id.tvCancel);
        rlScreen.setOnClickListener(this);
        tvCont.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rlScreen)
            dismiss();
        if (view.getId() == R.id.tvCancel) {
            dismiss();
            if (transactionsDialog != null) {
                transactionsDialog.dismiss();
            }
        }
        if (view.getId() == R.id.tvCont)
            dismiss();
    }
}
