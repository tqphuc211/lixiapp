package com.opencheck.client.home.history.detail.controller;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemOrderInfoLayoutBinding;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class ControllerLixiShopOrderDetailInfo implements View.OnClickListener {

    private Activity activity;
    private View viewChild;
    private LinearLayout lnRoot;

    private LinearLayout ll_content;
    private TextView tv_name;
    private TextView tv_value;
    private int drawable;
    private View viewLine;

    private int type = 0;
    private String name = "";
    private String value = "";
    private long textColor = 0xff000000;
    private String strTextColor = "#ff000000";
    private String state = "";
    private boolean isBolt = false;
    private int color;


    public ControllerLixiShopOrderDetailInfo(Activity _activity, int type, String _name, String _value, long drawabletextColor, boolean isBolt, int drawable, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.type = type;
        this.name = _name;
        this.value = _value;
        this.textColor = textColor;
        this.drawable = drawable;
        this.isBolt = isBolt;
        ItemOrderInfoLayoutBinding itemOrderInfoLayoutBinding =
                ItemOrderInfoLayoutBinding.inflate(LayoutInflater.from(activity),
                        null, false);
        this.viewChild = itemOrderInfoLayoutBinding.getRoot();

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    public ControllerLixiShopOrderDetailInfo(Activity _activity, int type, String state, String _name, String _value, String drawabletextColor, boolean isBolt, int drawable, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.type = type;
        this.name = _name;
        this.value = _value;
        this.strTextColor = drawabletextColor;
        this.drawable = drawable;
        this.isBolt = isBolt;
        this.state = state;

        this.viewChild = View.inflate(activity, R.layout.item_order_info_layout, null);

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initView() {
        ll_content = (LinearLayout) viewChild.findViewById(R.id.ll_content);
        tv_name = (TextView) viewChild.findViewById(R.id.tv_name);
        tv_value = (TextView) viewChild.findViewById(R.id.tv_value);
        viewLine = (View) viewChild.findViewById(R.id.viewLine);
    }

    private void initData() {
        if (type == 0) {
            tv_name.setText(name);
            tv_value.setText(value);
//            tv_value.setTextColor((int) textColor);
            tv_value.setTextColor(Color.parseColor(strTextColor));
            tv_value.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
            tv_value.setCompoundDrawablePadding((int) activity.getResources().getDimension(R.dimen.value_6));
            if (isBolt)
                tv_value.setTypeface(null, Typeface.BOLD);

            if (name.equals(LanguageBinding.getString(R.string.lixi_cash_back, activity))) {
                tv_value.setTextColor(getColor());
            }

            if(name.equals("Lixi sử dụng"))
                tv_value.setTextColor(getColor());

            if(name.equals("Giá tiền"))
                tv_value.setTextColor(0xFF64B408);

            ll_content.setVisibility(View.VISIBLE);
            viewLine.setVisibility(View.GONE);
            ll_content.setOnClickListener(this);
        } else {
            viewLine.setVisibility(View.VISIBLE);
            ll_content.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == ll_content)
            if (eventListener != null) {
                eventListener.onClick();
            }
    }

    public int getColor() {
        if (state.equals("wait"))
            return 0xFFEB8509;
        else if (state.equals("done"))
            return 0xffac1ea1;
        else if (state.equals("cancel"))
            return 0xffac1ea1;
        else if (state.equals("payment_expired"))
            return 0xffac1ea1;
        return 0xFF000000;
    }

    public interface IOrderDetailControllerEvent {
        void onClick();
    }

    IOrderDetailControllerEvent eventListener;

    public void setIOrderDetailControllerEventListener(IOrderDetailControllerEvent listener) {
        eventListener = listener;
    }
}