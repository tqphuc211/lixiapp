package com.opencheck.client.home.survey;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyQuestInputBinding;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;
import com.opencheck.client.models.merchant.survey.SurveyUserAnswerModel;

public class QuestionFreeController extends QuestionAnswerController {
    private TextView tv_question;
    private MyEditText et_asw;
    SurveyQuestionModel quest;
    private SurveyDialog surveyDialog;

    public QuestionFreeController(LixiActivity activity, ViewGroup parent, SurveyQuestionModel quest, SurveyDialog pu) {
        this.activity = activity;
        this.parent = parent;
        this.quest = quest;
        surveyDialog = pu;
        SurveyQuestInputBinding surveyQuestInputBinding =
                SurveyQuestInputBinding.inflate(
                        LayoutInflater.from(activity), parent, false);
        rootView = surveyQuestInputBinding.getRoot();
        parent.addView(rootView);

        findView();
    }

    public void findView() {
        tv_question = (TextView) rootView.findViewById(R.id.tv_question);
        et_asw = (MyEditText) rootView.findViewById(R.id.et_asw);

        tv_question.setText(quest.getQuestion_name());
        et_asw.setHardbackPressListener(new MyEditText.onHardBackPress() {
            @Override
            public void onbackPress() {
                surveyDialog.onHideKeyBoard();
            }
        });

        et_asw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surveyDialog.onShowKeyBoard();
            }
        });

//        et_asw.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP)
//                    popuSurvey.onShowKeyBoard();
//                return false;
//            }
//        });
    }

    @Override
    SurveyUserAnswerModel getAnswer() {
        if (et_asw.getText().toString().trim().length() == 0)
            return null;
        SurveyUserAnswerModel rs = new SurveyUserAnswerModel();
        rs.setQuestion_id(quest.getId());
        rs.setAnswer_value(getEt_asw().getText().toString());
        return rs;
    }

    public EditText getEt_asw() {
        return et_asw;
    }
}
