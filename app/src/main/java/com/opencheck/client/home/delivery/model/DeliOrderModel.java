package com.opencheck.client.home.delivery.model;

import android.content.Context;

import com.opencheck.client.R;
import com.opencheck.client.utils.LanguageBinding;

import java.io.Serializable;
import java.util.ArrayList;

public class DeliOrderModel implements Serializable {

    public final static String DRAFT = "draft";
    public final static String PAYMENT = "payment";
    public final static String SUBMITTED = "submitted";
    public final static String CONFIRMED = "confirmed";
    public final static String ASSIGNED = "assigned";
    public final static String PICKED = "picked";
    public final static String COMPLETED = "completed";
    public final static String CANCELED = "canceled";
    public final static String REJECTED = "rejected";
    public final static String EXPIRED = "expired";

    private String uuid;
    private String ref_code;
    private long create_date;
    private long complete_date;
    private long merchant_id;
    private long store_id;
    private long user_id;
    private long money;
    private long cashback;
    private long product_count;
    private long estimate_time;
    private long shipping_distance;
    private long shipping_duration;
    private long ship_price;
    private long min_ship_money;
    private long service_price;
    private String ship_price_type;
    private String service_price_type;
    private String shipping_driver_id;
    private String shipping_driver_name;
    private String shipping_driver_phone;
    private String merchant_name;
    private String store_name;
    private String store_address;
    private String user_name;
    private String user_phone;
    private String receive_name;
    private String receive_phone;
    private String receive_address;
    private Long receive_date;
    private String note;
    private String state;
    private String store_logo_link;
    private String store_cover_link;
    private ArrayList<LogModel> list_log;
    private ArrayList<QuotationModel> list_quotation;
    private ArrayList<ProductGetModel> list_product;
    private ArrayList<PromotionModel> list_promotion_code_apply;
    private DeliTrackingModel current_tracking;
    private FeedbackOrderModel feedback;
    private PaymentModel payment_method;
    private String type;
    private double receive_lat, receive_lng;
    private double lat, lng;
    private int typeItem;
    private boolean isViewCreated = false;
    private String close_reason_type;
    private String close_reason_message;
    private Long min_money_to_free_service_price;
    private boolean apply_service_price;
    private ClaimReasonModel claim;
    private long point_lixi_money;

    public long getPoint_lixi_money() {
        return point_lixi_money;
    }

    public void setPoint_lixi_money(long point_lixi_money) {
        this.point_lixi_money = point_lixi_money;
    }


    public int getStateColor(){
        switch (state){
            case DRAFT:
            case PAYMENT:
            case SUBMITTED:
            case CONFIRMED:
            case ASSIGNED:
            case PICKED:
                return 0xFFEB8509;
            case COMPLETED:
                return 0xFF64B408;
            case CANCELED:
                return 0xffac1ea1;
            case REJECTED:
            case EXPIRED:
                return 0xffac1ea1;
                default: return 0xFF000000;
        }
    }

    public String getStateString(Context context){
        switch (state){
            case DRAFT:
                return "Khởi tạo";
            case PAYMENT:
                return "Chờ thanh toán";
            case SUBMITTED:
                return "Đã gửi yêu cầu";
            case CONFIRMED:
                return "Đã xác nhận";
            case ASSIGNED:
                return "Chuẩn bị";
            case PICKED:
                return "Đang giao hàng";
            case COMPLETED:
                return "Hoàn thành";
            case CANCELED:
                return "Đã huỷ";
            case REJECTED:
                return "Từ chối";
            case EXPIRED:
                return LanguageBinding.getString(R.string.expired, context);
            default: return LanguageBinding.getString(R.string.unknown, context);
        }
    }

    public ClaimReasonModel getClaim() {
        return claim;
    }

    public ArrayList<PromotionModel> getList_promotion_code_apply() {
        return list_promotion_code_apply;
    }

    public void setList_promotion_code_apply(ArrayList<PromotionModel> list_promotion_code_apply) {
        this.list_promotion_code_apply = list_promotion_code_apply;
    }

    public Long getMin_money_to_free_service_price() {
        return min_money_to_free_service_price;
    }

    public void setMin_money_to_free_service_price(Long min_money_to_free_service_price) {
        this.min_money_to_free_service_price = min_money_to_free_service_price;
    }

    public boolean isApply_service_price() {
        return apply_service_price;
    }

    public void setApply_service_price(boolean apply_service_price) {
        this.apply_service_price = apply_service_price;
    }

    public DeliTrackingModel getCurrent_tracking() {
        return current_tracking;
    }

    public void setCurrent_tracking(DeliTrackingModel current_tracking) {
        this.current_tracking = current_tracking;
    }

    public String getRef_code() {
        return ref_code;
    }

    public void setRef_code(String ref_code) {
        this.ref_code = ref_code;
    }

    public long getShip_price() {
        return ship_price;
    }

    public void setShip_price(long ship_price) {
        this.ship_price = ship_price;
    }

    public long getMin_ship_money() {
        return min_ship_money;
    }

    public void setMin_ship_money(long min_ship_money) {
        this.min_ship_money = min_ship_money;
    }

    public long getService_price() {
        return service_price;
    }

    public void setService_price(long service_price) {
        this.service_price = service_price;
    }

    public String getShip_price_type() {
        return ship_price_type;
    }

    public void setShip_price_type(String ship_price_type) {
        this.ship_price_type = ship_price_type;
    }

    public String getService_price_type() {
        return service_price_type;
    }

    public void setService_price_type(String service_price_type) {
        this.service_price_type = service_price_type;
    }

    public Long getReceive_date() {
        return receive_date;
    }

    public String getStore_logo_link() {
        return store_logo_link;
    }

    public void setStore_logo_link(String store_logo_link) {
        this.store_logo_link = store_logo_link;
    }

    public String getStore_cover_link() {
        return store_cover_link;
    }

    public void setStore_cover_link(String store_cover_link) {
        this.store_cover_link = store_cover_link;
    }

    public double getReceive_lat() {
        return receive_lat;
    }

    public void setReceive_lat(double receive_lat) {
        this.receive_lat = receive_lat;
    }

    public double getReceive_lng() {
        return receive_lng;
    }

    public void setReceive_lng(double receive_lng) {
        this.receive_lng = receive_lng;
    }

    public void setReceive_date(Long receive_date) {
        this.receive_date = receive_date;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getShipping_distance() {
        return shipping_distance;
    }

    public void setShipping_distance(long shipping_distance) {
        this.shipping_distance = shipping_distance;
    }

    public long getShipping_duration() {
        return shipping_duration;
    }

    public void setShipping_duration(long shipping_duration) {
        this.shipping_duration = shipping_duration;
    }

    public String getShipping_driver_id() {
        return shipping_driver_id;
    }

    public void setShipping_driver_id(String shipping_driver_id) {
        this.shipping_driver_id = shipping_driver_id;
    }

    public String getShipping_driver_name() {
        return shipping_driver_name;
    }

    public void setShipping_driver_name(String shipping_driver_name) {
        this.shipping_driver_name = shipping_driver_name;
    }

    public String getShipping_driver_phone() {
        return shipping_driver_phone;
    }

    public void setShipping_driver_phone(String shipping_driver_phone) {
        this.shipping_driver_phone = shipping_driver_phone;
    }

    public long getCashback() {
        return cashback;
    }

    public void setCashback(long cashback) {
        this.cashback = cashback;
    }

    public int getTypeItem() {
        return typeItem;
    }

    public void setTypeItem(int typeItem) {
        this.typeItem = typeItem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<QuotationModel> getList_quotation() {
        return list_quotation;
    }

    public void setList_quotation(ArrayList<QuotationModel> list_quotation) {
        this.list_quotation = list_quotation;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ArrayList<ProductGetModel> getList_product() {
        return list_product;
    }

    public void setList_product(ArrayList<ProductGetModel> list_product) {
        this.list_product = list_product;
    }

    public FeedbackOrderModel getFeedback() {
        return feedback;
    }

    public void setFeedback(FeedbackOrderModel feedback) {
        this.feedback = feedback;
    }

    public PaymentModel getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(PaymentModel payment_method) {
        this.payment_method = payment_method;
    }

    public long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(long create_date) {
        this.create_date = create_date;
    }

    public long getComplete_date() {
        return complete_date;
    }

    public void setComplete_date(long complete_date) {
        this.complete_date = complete_date;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public long getStore_id() {
        return store_id;
    }

    public void setStore_id(long store_id) {
        this.store_id = store_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public long getProduct_count() {
        return product_count;
    }

    public void setProduct_count(long product_count) {
        this.product_count = product_count;
    }

    public long getEstimate_time() {
        return estimate_time;
    }

    public void setEstimate_time(long estimate_time) {
        this.estimate_time = estimate_time;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_address() {
        return store_address;
    }

    public void setStore_address(String store_address) {
        this.store_address = store_address;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getReceive_name() {
        return receive_name;
    }

    public void setReceive_name(String receive_name) {
        this.receive_name = receive_name;
    }

    public String getReceive_phone() {
        return receive_phone;
    }

    public void setReceive_phone(String receive_phone) {
        this.receive_phone = receive_phone;
    }

    public String getReceive_address() {
        return receive_address;
    }

    public void setReceive_address(String receive_address) {
        this.receive_address = receive_address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<LogModel> getList_log() {
        return list_log;
    }

    public void setList_log(ArrayList<LogModel> list_log) {
        this.list_log = list_log;
    }

    public boolean isViewCreated() {
        return isViewCreated;
    }

    public void setViewCreated(boolean viewCreated) {
        isViewCreated = viewCreated;
    }

    public String getClose_reason_type() {
        return close_reason_type;
    }

    public void setClose_reason_type(String close_reason_type) {
        this.close_reason_type = close_reason_type;
    }

    public String getClose_reason_message() {
        return close_reason_message;
    }

    public void setClose_reason_message(String close_reason_message) {
        this.close_reason_message = close_reason_message;
    }
}
