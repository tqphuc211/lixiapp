package com.opencheck.client.home.lixishop.header.buycard;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.BuyCardLayoutBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.lixishop.header.ConfirmPaymentDialog;
import com.opencheck.client.home.lixishop.header.PaymentCardValueAdapter;
import com.opencheck.client.models.lixishop.PaymentModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.models.merchant.CardValueModel;
import com.opencheck.client.models.merchant.PaymentPartnerModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class LixiBuyCardFragment extends LixiFragment {

    private ServiceCategoryModel serviceCategorySelected = new ServiceCategoryModel();
    private PaymentPartnerModel paymentPartnerSelected = new PaymentPartnerModel();
    private ArrayList<PaymentPartnerModel> listPaymentPartner = new ArrayList<PaymentPartnerModel>();
    private CardValueModel cardValuSelected = new CardValueModel();

    private UserModel userModel = new UserModel();
    private int crSelectSuplier = 0;

    private ArrayList<CardValueModel> listCardValue = new ArrayList<CardValueModel>();
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private RelativeLayout rlSelectCard;
    private ImageView iv_icon;
    private TextView tv_name;
    private TextView tv_link;
    private TextView tv_change, tvSelectCard, tv_values;
    private RecyclerView rl_card_value;
    private Button btnContinute;

    private PaymentCardValueAdapter adapterCardValue;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                activity.onBackPressed();
                break;
            case R.id.btnContinute:
                if (checkInput()) {
                    PaymentModel paymentModel = new PaymentModel();
                    paymentModel.setContract_code(userModel.getCode());
                    paymentModel.setFullname(userModel.getFullname());
                    paymentModel.setMoney_payment(cardValuSelected.getKey());
                    paymentModel.setName(serviceCategorySelected.getName());
                    paymentModel.setPaymentPartnerModel(paymentPartnerSelected);
                    paymentModel.setPhone(userModel.getPhone());
                    paymentModel.setServiceCategoryModel(serviceCategorySelected);
                    paymentModel.setQuantity("1");
                    ConfirmPaymentDialog dialog = new ConfirmPaymentDialog(activity, false, true, false);
                    dialog.show();
                    dialog.setData(serviceCategorySelected, paymentModel, false, false);
                }
                break;
            case R.id.tv_change:
                showPopupPaymentPartner();
                break;
        }
    }

    private BuyCardLayoutBinding mBingding;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mBingding = BuyCardLayoutBinding.inflate(inflater, container, false);
        getVariableStatic();
        GlobalTracking.trackServiceStart(TrackingConstant.Service.PHONE_CARD);

        this.initView(mBingding.getRoot());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initData();

                getPaymentPartner();
            }
        }, 300);

        return mBingding.getRoot();
    }

    private void initView(View view) {
        tvSelectCard = view.findViewById(R.id.tvSelectCard);
        rlBack = view.findViewById(R.id.rlBack);
        tvTitle = view.findViewById(R.id.tvTitle);
        rlSelectCard = view.findViewById(R.id.rlSelectCard);
        iv_icon = view.findViewById(R.id.iv_icon);
        tv_name = view.findViewById(R.id.tv_name);
        tv_link = view.findViewById(R.id.tv_link);
        tv_change = view.findViewById(R.id.tv_change);
        rl_card_value = view.findViewById(R.id.rl_card_value);
        btnContinute = view.findViewById(R.id.btnContinute);
        tv_values = view.findViewById(R.id.tv_values);

        GridLayoutManager mLayoutManager = new GridLayoutManager(activity, 3);
        rl_card_value.setLayoutManager(mLayoutManager);

        tv_change.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        btnContinute.setOnClickListener(this);
    }

    private void initData() {
        userModel = UserModel.getInstance();
        getCardValue();
    }


    private void getVariableStatic() {
        String jsonServiceCategory;
        Bundle extras = getArguments();
        if (extras != null) {
            jsonServiceCategory = extras.getString(ConstantValue.SERVICE_CATEGORY_CHILD_MODEL);
            serviceCategorySelected = new Gson().fromJson(jsonServiceCategory, ServiceCategoryModel.class);
        } else
            serviceCategorySelected = new ServiceCategoryModel();
    }

    private void getPaymentPartner() {
        DataLoader.getPaymentPartner(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listPaymentPartner = (ArrayList<PaymentPartnerModel>) object;

                    PaymentPartnerModel pn = detectPaymentPartner(userModel.getPhone());
                    if (pn != null) {
                        paymentPartnerSelected = pn;

                        tv_link.setText(paymentPartnerSelected.getWebsite());
                        tv_name.setText(paymentPartnerSelected.getName());
                        ImageLoader.getInstance().displayImage(paymentPartnerSelected.getAvatar(), iv_icon, LixiApplication.getInstance().optionsNomal);
                    }

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, serviceCategorySelected.getId());
    }

    public void showPopupPaymentPartner() {
        SelectSupplierDialog dialog = new SelectSupplierDialog(activity, false, true, false);
        dialog.show();
        dialog.setData(listPaymentPartner, crSelectSuplier);
        dialog.setIDidalogEventListener(new SelectSupplierDialog.IDidalogEvent() {
            @Override
            public void onSelectItem(int pos) {
                crSelectSuplier = pos;
                paymentPartnerSelected = listPaymentPartner.get(pos);

                tv_link.setText(paymentPartnerSelected.getWebsite());
                tv_name.setText(paymentPartnerSelected.getName());
                ImageLoader.getInstance().displayImage(paymentPartnerSelected.getAvatar(), iv_icon, LixiApplication.getInstance().optionsNomal);

            }
        });
    }

    private Boolean checkInput() {
        if (paymentPartnerSelected.getId().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.buy_card_require_suplier));
            return false;
        }

        if (cardValuSelected.getValue().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.buy_card_require_value));
            return false;
        }

        float remnant = Float.parseFloat(UserModel.getInstance().getAvailable_point()) - Float.parseFloat(cardValuSelected.getKey());

        if (remnant < 0) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.buy_card_require_available));
            return false;
        }

        return true;
    }

    private void getCardValue() {
        DataLoader.getCardValue(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listCardValue = (ArrayList<CardValueModel>) object;
                    showCardValue();

                    cardValuSelected = listCardValue.get(0);

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    public void showCardValue() {
        adapterCardValue = new PaymentCardValueAdapter(listCardValue, activity);
        rl_card_value.setAdapter(adapterCardValue);

        adapterCardValue.setOnClickListener(new PaymentCardValueAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                cardValuSelected = listCardValue.get(position);
            }
        });
    }

    private PaymentPartnerModel detectPaymentPartner(String sdt) {
        boolean found = false;
        PaymentPartnerModel rs = null;
        for (int i = 0; i < listPaymentPartner.size(); i++) {
            rs = listPaymentPartner.get(i);

            for (int j = 0; j < rs.getSuplier_prefix_phone().size(); j++) {
                if (sdt.indexOf(rs.getSuplier_prefix_phone().get(j)) == 0) {
                    if (sdt.length() - rs.getSuplier_prefix_phone().get(j).length() == 7) {
                        crSelectSuplier = i;
                        found = true;
                        break;
                    }
                }
            }
            if (found)
                break;
        }
        if (found)
            return rs;
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.GONE);
//            ((HomeActivity) activity).navigation.setVisibility(View.GONE);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.SERVICE_CARD;
    }
}
