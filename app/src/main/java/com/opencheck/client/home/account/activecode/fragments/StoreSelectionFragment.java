package com.opencheck.client.home.account.activecode.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SelectStoreFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.activecode.adapters.StoreAdapter;
import com.opencheck.client.home.account.activecode.dialogs.NoApplyDialog;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.merchant.map.StoreMapActivity;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

//import com.opencheck.client.home.account.activecode.ActiveCodeActivity;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class StoreSelectionFragment extends LixiFragment {

    private RelativeLayout rlBack, rl_buy;
    private TextView tv_buy, tv_title;
    private LinearLayout ll_no_result, ll_list_store;
    private SwipeRefreshLayout srf_refresh;
    private RecyclerView rv_store;
    private StoreAdapter storeAdapter;
    private ArrayList<StoreApplyModel> listStore = new ArrayList<>();

    private EvoucherDetailModel evoucherModel;
    private boolean isLoading = false;
    private int page_size = 1;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private boolean isOutOfPage = false;
    private LinearLayoutManager mLayoutManager;
    private BroadcastReceiver receiver;
    private IntentFilter intentFilter;
    private boolean is_only_seen = false;

    public StoreSelectionFragment() {
        // Required empty public constructor
    }

    private SelectStoreFragmentBinding mBinding;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (activity instanceof EVoucherAccountActivity)
            ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.GONE);
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = SelectStoreFragmentBinding.inflate(inflater, container, false);
        rootview = mBinding.getRoot();
        super.onCreateView(inflater, container, savedInstanceState);
        evoucherModel = new Gson().fromJson(getArguments().getString("ev"), EvoucherDetailModel.class);
        is_only_seen = getArguments().getBoolean("is_only_seen", false);
        findViews(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void findViews(View v) {
        rlBack = v.findViewById(R.id.rlBack);
        rl_buy = v.findViewById(R.id.rl_buy);
        ll_no_result = v.findViewById(R.id.ll_no_result);
        ll_list_store = v.findViewById(R.id.ll_list_store);
        rv_store = v.findViewById(R.id.rv_store);
        tv_buy = v.findViewById(R.id.tv_buy);
        tv_title = v.findViewById(R.id.tvTitle);
        srf_refresh = v.findViewById(R.id.srf_refresh);

        rlBack.setOnClickListener(this);
        rl_buy.setOnClickListener(this);
        srf_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!is_only_seen) {
                    isLoading = false;
                    page_size = 1;
                    isOutOfPage = false;
                    listStore.clear();
                    if (storeAdapter != null) {
                        storeAdapter.notifyDataSetChanged();
                    } else {
                        setAdapter();
                    }
                    startLoadData();
                } else {
                    srf_refresh.setRefreshing(false);
                }
            }
        });

        rv_store.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startLoadData();
                            }
                        }, 300);

                        isLoading = true;
                    }
                }
            }
        });

        if (is_only_seen) {
            rl_buy.setVisibility(View.GONE);
            tv_title.setText(LanguageBinding.getString(R.string.store_list, activity));
        } else {
            rl_buy.setVisibility(View.VISIBLE);
            tv_title.setText(LanguageBinding.getString(R.string.choose_store, activity));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!is_only_seen) {
                    startLoadData();
                } else {
                    listStore = evoucherModel.getStore_apply();
                    setAdapter();
                }
            }
        }, 100);

    }

    @Override
    public void onPause() {
        super.onPause();
        activity.hideKeyboard();
    }

    private int StorePos = -1;

    private void setAdapter() {
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rv_store.setLayoutManager(mLayoutManager);
        storeAdapter = new StoreAdapter(activity, listStore);
        rv_store.setAdapter(storeAdapter);
        storeAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (action.equalsIgnoreCase(ConstantValue.ON_MAP_CLICK)) {
                    StorePos = -1;
                    turnOnMapActivity(pos);
                } else if (action.equalsIgnoreCase(ConstantValue.ON_ITEM_STORE_CLICK)) {
                    if (!is_only_seen) {
                        StoreApplyModel model = (StoreApplyModel) object;
                        if (model.isAllow_user_send_request_active()) {
                            ConfirmActiveCodeFragment fragment = new ConfirmActiveCodeFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("ev", new Gson().toJson(evoucherModel));
                            bundle.putString("sa", new Gson().toJson(listStore.get(pos)));
                            bundle.putInt(ConstantValue.CURRENT_POSITION, getArguments().getInt(ConstantValue.CURRENT_POSITION));
                            fragment.setArguments(bundle);
                            if (activity instanceof ActiveCodeActivity)
                                ((ActiveCodeActivity) activity).replaceFragment(fragment);
                            else
                                ((EVoucherAccountActivity) activity).replaceFragment(fragment);
                        } else {
                            NoApplyDialog dialog = new NoApplyDialog(activity, false, true, false);
                            dialog.show();
                            dialog.setData(model, evoucherModel);
                        }
                    }
                }
            }
        });
    }

    public void turnOnMapActivity(int pos) {
        if (pos == -1)
            return;
        ArrayList<StoreModel> arrTemp = new ArrayList<>();
        for (int i = 0; i < listStore.size(); i++) {
            StoreModel storeModel = listStore.get(i).convertToStoreModel();
            arrTemp.add(storeModel);
        }
        StoreModel tempStore = arrTemp.get(pos);
        arrTemp.remove(arrTemp.get(pos));
        arrTemp.add(0, tempStore);

        if (checkLocationPermission()) {
            MerchantModel merchantModel = new MerchantModel();
            Bundle bundle = new Bundle();
            merchantModel.setListStore(arrTemp);
            merchantModel.setId(String.valueOf(evoucherModel.getMerchant_info().getId()));
            Intent intent = new Intent(activity, StoreMapActivity.class);
            bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
                turnOnMapActivity(StorePos);
                break;
            case ConstantValue.REQUEST_CODE_CAMERA_QR:
                Bundle bundle = new Bundle();
                bundle.putString("ev", new Gson().toJson(evoucherModel));
                ScannerQRCodeFragment fragment = new ScannerQRCodeFragment();
                fragment.setArguments(bundle);
                if (activity instanceof ActiveCodeActivity)
                    ((ActiveCodeActivity) activity).replaceFragment(fragment);
                else
                    ((EVoucherAccountActivity) activity).replaceFragment(fragment);
                break;

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
            }
            return false;
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
            }
            return false;
        }
        return true;
    }

    private void startLoadData() {
        if (!isLoading) {
            if (!isOutOfPage) {
                srf_refresh.setRefreshing(false);
                DataLoader.getStoreApplyCashCodeList(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, final Object object) {
                        if (isSuccess) {
                            if (page_size == 1) {
                                listStore = (ArrayList<StoreApplyModel>) object;
                                if (listStore != null && listStore.size() > 0) {
                                    setOutOfPage(listStore);
                                    setAdapter();
                                }
                            } else {
                                ArrayList<StoreApplyModel> list = (ArrayList<StoreApplyModel>) object;
                                if (list != null && list.size() > 0 && listStore != null) {
                                    setOutOfPage(list);
                                    listStore.addAll(list);
                                    storeAdapter.notifyDataSetChanged();
                                }
                            }

                            if (listStore != null && listStore.size() > 0) {
                                rv_store.setVisibility(View.VISIBLE);
                                ll_no_result.setVisibility(View.GONE);
                            } else {
                                rv_store.setVisibility(View.GONE);
                                listStore.clear();
                                ll_no_result.setVisibility(View.VISIBLE);
                            }

                            isLoading = false;
                            page_size++;

                        } else {
                            isLoading = true;
                        }
                    }
                }, evoucherModel.getProduct(), page_size, "");
            }
        }

    }

    private void setOutOfPage(ArrayList<StoreApplyModel> arrayList) {
        if (arrayList.size() < RECORD_PER_PAGE) {
            isOutOfPage = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        ((ActiveCodeActivity) activity).toolbar.setVisibility(View.GONE);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.VISIBLE);
        try {
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (view.getId()) {
            case R.id.rl_buy:
                Toast.makeText(activity, "Quét mã", Toast.LENGTH_SHORT).show();
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                    }
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("ev", new Gson().toJson(evoucherModel));
                    ScannerQRCodeFragment fragment = new ScannerQRCodeFragment();
                    fragment.setArguments(bundle);
                    if (activity instanceof ActiveCodeActivity)
                        ((ActiveCodeActivity) activity).replaceFragment(fragment);
                    else
                        ((EVoucherAccountActivity) activity).replaceFragment(fragment);
                }
                break;
            case R.id.rlBack:
                activity.hideKeyboard();
                activity.onBackPressed();
                break;
        }
    }

    public EvoucherDetailModel getEvoucherModel() {
        return evoucherModel;
    }
}
