package com.opencheck.client.home.product.bank_card;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PopupSavedBankCardBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.flashsale.Global.FlashSaleTimerController;
import com.opencheck.client.home.product.communicate.OnCreateNewCard;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;
import com.opencheck.client.home.product.communicate.OnUseSavedCard;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;

import java.util.ArrayList;

public class PopupChooseSavedCard extends LixiTrackingDialog {

    public PopupChooseSavedCard(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    RelativeLayout relBack, relFacebookMessenger;
    RecyclerView recSaveCard;
    TextView txtNewCard;
    CheckBox cbSaveCard;

    LinearLayout linearFlashSale;
    FlashSaleTimerController fsControler;

    SavedBankCardAdapter adapter;
    LinearLayoutManager layoutManager;

    ArrayList<TokenData> listTokenBankCard;
    ProductCashVoucherDetailModel productModel;

    private PopupSavedBankCardBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PopupSavedBankCardBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        initView();

        txtNewCard.setOnClickListener(this);
        relBack.setOnClickListener(this);
        relFacebookMessenger.setOnClickListener(this);
    }

    private void initView() {
        relBack = (RelativeLayout) findViewById(R.id.rlBack);
        relFacebookMessenger = (RelativeLayout) findViewById(R.id.rlFacebookMessenger);
        recSaveCard = (RecyclerView) findViewById(R.id.recSaveCard);
        txtNewCard = (TextView) findViewById(R.id.txtNewCard);
        cbSaveCard = (CheckBox) findViewById(R.id.cbSaveCard);
        linearFlashSale = (LinearLayout) findViewById(R.id.ll_flashsale);

        cbSaveCard.setChecked(true);
    }

    public void setData(ArrayList<TokenData> list, ProductCashVoucherDetailModel productModel) {
        listTokenBankCard = list;
        this.productModel = productModel;
        adapter = new SavedBankCardAdapter(activity, listTokenBankCard);
        layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recSaveCard.setLayoutManager(layoutManager);
        recSaveCard.setAdapter(adapter);

        adapter.addOnDeleteListener(new OnDeleteBankCard() {
            @Override
            public void onDelete(int position) {
                if (onDeleteBankCard != null) {
                    onDeleteBankCard.onDelete(position);
                }
            }
        });

        adapter.addOnUseSavedCard(new OnUseSavedCard() {
            @Override
            public void onUse(TokenData bankCard) {
                if (onUseSavedCard != null) {
                    onUseSavedCard.onUse(bankCard);
                }
            }
        });

        if (productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {
            fsControler = new FlashSaleTimerController(activity, linearFlashSale);
        }

        id = productModel.getId();
        trackScreen();
    }

    public void updateFsTimer(int[] arrTime) {
        if (fsControler != null)
            fsControler.showTime(arrTime);
    }

    OnCreateNewCard onCreateNewCard;
    OnUseSavedCard onUseSavedCard;
    OnDeleteBankCard onDeleteBankCard;

    public void addOnCreateNewCard(OnCreateNewCard onCreateNewCard) {
        this.onCreateNewCard = onCreateNewCard;
    }

    public void addOnUseSavedCard(OnUseSavedCard onUseSavedCard) {
        this.onUseSavedCard = onUseSavedCard;
    }

    public void addOnDeleteBankCard(OnDeleteBankCard onDeleteBankCard) {
        this.onDeleteBankCard = onDeleteBankCard;
    }

    public void notifyDataSetChange(ArrayList<TokenData> listTokenBankCard) {
        this.listTokenBankCard = listTokenBankCard;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtNewCard:
                if (onCreateNewCard != null) {
                    onCreateNewCard.onNew(cbSaveCard.isChecked());
                }
                break;
            case R.id.rlBack:
                cancel();
                break;
            case R.id.rlFacebookMessenger:
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                break;
        }
    }
}
