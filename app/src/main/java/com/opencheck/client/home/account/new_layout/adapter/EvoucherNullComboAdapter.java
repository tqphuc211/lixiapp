package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.home.account.new_layout.model.EvoucherInfo;
import com.opencheck.client.home.account.new_layout.model.ProductInfo;

import java.util.ArrayList;

public class EvoucherNullComboAdapter extends RecyclerView.Adapter<EvoucherNullComboAdapter.NullComboHolder>{
    private LixiActivity activity;
    private ArrayList<EvoucherInfo> listEvoucherInfo;
    private ProductInfo productInfo;

    public EvoucherNullComboAdapter(LixiActivity activity, ArrayList<EvoucherInfo> listEvoucherInfo, ProductInfo productInfo) {
        this.activity = activity;
        this.listEvoucherInfo = listEvoucherInfo;
        this.productInfo = productInfo;
    }

    @Override
    public NullComboHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_evoucher, parent, false);
        return new NullComboHolder(view);
    }

    @Override
    public void onBindViewHolder(NullComboHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listEvoucherInfo != null ? 1 : 0;
    }

    public class NullComboHolder extends RecyclerView.ViewHolder {
        TextView txtEvoucherName, txtEvoucherQuant;
        RecyclerView recEvoucherInfo;

        public NullComboHolder(View itemView) {
            super(itemView);
            txtEvoucherName = (TextView) itemView.findViewById(R.id.txtEvoucherName);
            txtEvoucherQuant = (TextView) itemView.findViewById(R.id.txtEvoucherQuant);
            recEvoucherInfo = (RecyclerView) itemView.findViewById(R.id.recEvoucherInfo);
        }

        public void bind(){
            txtEvoucherName.setText(productInfo.getName());
            txtEvoucherQuant.setText("(" + productInfo.getQuantity() + " voucher)");

            EvoucherItemAdapter adapter = new EvoucherItemAdapter(activity, listEvoucherInfo);
            LinearLayoutManager manager = new LinearLayoutManager(activity);
            manager.setOrientation(LinearLayoutManager.VERTICAL);

            recEvoucherInfo.setAdapter(adapter);
            recEvoucherInfo.setLayoutManager(manager);
        }
    }
}
