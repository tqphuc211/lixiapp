package com.opencheck.client.home.delivery.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class EdittingViewModel extends ViewModel {
    private MutableLiveData<Boolean> isEditing;

    public MutableLiveData<Boolean> getIsEditing() {
        if (isEditing == null){
            isEditing = new MutableLiveData<>();
        }
        return isEditing;
    }
}
