package com.opencheck.client.home.merchant.map;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SelectStoreMapLayoutDialogBinding;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class StoreLocationControllerDialog implements View.OnClickListener, AdapterView.OnItemClickListener {


    private Dialog dialog;
    private Activity activity;
    private ItemClickListener itemClickListener;


    private LinearLayout lnPopup;
    private RelativeLayout rlRoot;
    private ListView lvItem;

    private ArrayList<StoreModel> listItem = new ArrayList<StoreModel>();
    private StoreLocationAdapter storeLocationAdapter;

    public StoreLocationControllerDialog(Activity _activity, ArrayList<StoreModel> _listItem, ItemClickListener _itemClickListener) {
        this.activity = _activity;
        this.listItem = _listItem;
        this.itemClickListener = _itemClickListener;
        this.setupPopup();
    }


    private void setupPopup() {

        this.dialog = new Dialog(activity);
        this.dialog.getWindow();
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        SelectStoreMapLayoutDialogBinding selectStoreMapLayoutDialogBinding =
                SelectStoreMapLayoutDialogBinding.inflate(dialog.getLayoutInflater(),
                        null, false);
        this.dialog.setContentView(selectStoreMapLayoutDialogBinding.getRoot());

        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.dialog.setCanceledOnTouchOutside(false);
        this.dialog.setCancelable(true);

        this.initView();
    }

    private void initView() {
        lnPopup = (LinearLayout) dialog.findViewById(R.id.lnPopup);
        rlRoot = (RelativeLayout) dialog.findViewById(R.id.rlRoot);
        lvItem = (ListView) dialog.findViewById(R.id.lvItem);

        initData();
    }

    private void initData() {
        lvItem.setOnItemClickListener(this);
        lnPopup.getLayoutParams().width = ((LixiActivity) activity).widthScreen - activity.getResources().getDimensionPixelSize(R.dimen.value_10);

        setCateAdapter();

    }

    private void setCateAdapter() {
        storeLocationAdapter = new StoreLocationAdapter(activity, R.layout.item_filter_layout, itemClickListener);
        lvItem.setAdapter(storeLocationAdapter);
        storeLocationAdapter.addListItems(listItem);
        storeLocationAdapter.setPosSelected(0);
        storeLocationAdapter.notifyDataSetChanged();
    }


    public void showPopup() {
        this.dialog.show();
    }

    public void hidePopup() {
        this.dialog.hide();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        itemClickListener.onItemCLick(position, ConstantValue.STORE_ADDRESS_CLICK_DIALOG, listItem.get(position), 0L);
        dialog.dismiss();
    }
}