package com.opencheck.client.home.setting.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentChangePassBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.newlogin.dialog.UpdateSuccessDialog;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.LanguageBinding;

public class ChangePassFragment extends LixiFragment {

    private FragmentChangePassBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentChangePassBinding.inflate(inflater, container, false);
        initEvent();
        return mBinding.getRoot();
    }

    private void initEvent() {
        mBinding.rlBack.setOnClickListener(this);
        mBinding.btnComplete.setOnClickListener(this);
        mBinding.linearParent.setOnClickListener(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                if (getFragmentManager() != null) {
                    activity.hideKeyboard();
                    getFragmentManager().beginTransaction().detach(this).commit();
                }
                break;
            case R.id.btnComplete:
                handlePassword();
                break;
        }
    }

    private void handlePassword() {
        String newPass = mBinding.edtNewPass.getText().toString();
        String oldPass = mBinding.edtOldPass.getText().toString();
        String confirm = mBinding.edtConfirm.getText().toString();
        // check full input
        if (newPass.isEmpty() || oldPass.isEmpty() || confirm.isEmpty()) {
            toast(LanguageBinding.getString(R.string.login_error_miss_input_info, activity));
            return;
        }

        // check length
        if (newPass.length() < 6) {
            toast(LanguageBinding.getString(R.string.change_pass_min, activity));
            mBinding.edtNewPass.setText("");
            mBinding.edtConfirm.setText("");
            mBinding.edtNewPass.requestFocus();
            return;
        }

        // check equal pass
        if (!newPass.equals(confirm)) {
            toast(LanguageBinding.getString(R.string.login_error_pass_not_match, activity));
            mBinding.edtNewPass.setText("");
            mBinding.edtConfirm.setText("");
            mBinding.edtNewPass.requestFocus();
            return;
        }

        //  update pass
        JsonObject param = new JsonObject();
        param.addProperty("current_password", oldPass);
        param.addProperty("new_password", newPass);

        DataLoader.putUpdatePass(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    activity.hideKeyboard();
                    UpdateSuccessDialog dialog = new UpdateSuccessDialog(activity, UserModel.getInstance().getPhone());
                    dialog.show();
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            if (getFragmentManager() != null) {
                                getFragmentManager().beginTransaction().detach(ChangePassFragment.this).commit();
                            }else {
                                activity.finish();
                            }
                        }
                    });
                }else {
                    toast((String) object);
                }
            }
        }, param);
    }

    private void toast(String content) {
        Toast.makeText(activity, content, Toast.LENGTH_SHORT).show();
    }
}
