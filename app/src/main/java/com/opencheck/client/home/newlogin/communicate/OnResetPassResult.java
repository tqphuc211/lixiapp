package com.opencheck.client.home.newlogin.communicate;

public interface OnResetPassResult {
    void onResetResult(boolean result);
}
