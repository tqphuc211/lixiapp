package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class ImageModel implements Serializable {
    private long id;
    private String image_link;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }
}
