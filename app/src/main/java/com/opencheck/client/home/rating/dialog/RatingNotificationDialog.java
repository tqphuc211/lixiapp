package com.opencheck.client.home.rating.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.DialogRatingNotificationBinding;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;

public class RatingNotificationDialog extends LixiDialog {

    public RatingNotificationDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private CircleImageView imgLogo;
    private TextView txtVoucherName, txtAddress, txtContent, txtRating, txtNo;

    private DialogRatingNotificationBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogRatingNotificationBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView(){
        imgLogo = (CircleImageView) findViewById(R.id.imgLogo);
        txtVoucherName = (TextView) findViewById(R.id.txtVoucherName);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtContent = (TextView) findViewById(R.id.txtContent);
        txtRating = (TextView) findViewById(R.id.txtRating);
        txtNo = (TextView) findViewById(R.id.txtNo);
    }

    public void setData(EvoucherDetailModel evoucher){
        ImageLoader.getInstance().displayImage(evoucher.getMerchant_info().getLogo(), imgLogo);
        txtVoucherName.setText(evoucher.getName());
        txtAddress.setText(evoucher.getStore_apply().get(0).getAddress());
        txtContent.setText("Bạn vừa sử dụng voucher "
                + evoucher.getMerchant_info().getName()
                + " tại " + evoucher.getName()
                + ". Bạn có muốn cho mọi người biết về sản phẩm này");

        txtRating.setOnClickListener(this);
        txtNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtRating:
                break;
            case R.id.txtNo:
                cancel();
                break;
        }
    }
}
