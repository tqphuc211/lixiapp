package com.opencheck.client.home.product.communicate;

public interface OnDeleteBankCard {
    void onDelete(int position);
}
