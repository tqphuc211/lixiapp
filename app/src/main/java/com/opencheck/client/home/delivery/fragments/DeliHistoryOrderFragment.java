package com.opencheck.client.home.delivery.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.custom.PinnedSectionListView;
import com.opencheck.client.databinding.FragmentDeliHistoryOrderBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.delivery.adapter.DeliHistoryOrderAdapter;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.history.HistoryPromotionStickyAdapter;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeliHistoryOrderFragment extends LixiFragment implements AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {

    private ViewGroup viewGroupHeader;

    private SwipeRefreshLayout swipeLayout;
    private PinnedSectionListView lvItem;
    private LinearLayout lnNoResult;
    private TextView tvNoResult;

    private DeliHistoryOrderAdapter historyOrderAdapter;

    private int record_per_page = DeliDataLoader.record_per_page;
    private int indexPage = 1;
    private Boolean isLoading = false;
    private Socket mSocket;

    private FragmentDeliHistoryOrderBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.viewGroupHeader = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_deli_history_order, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (this.isAdded())
            initView(view);
        else LoginHelper.Logout(activity);
    }

    private void initView(View view) {
        swipeLayout = view.findViewById(R.id.swipeLayout);
        lvItem = view.findViewById(R.id.lvItem);
        lnNoResult = view.findViewById(R.id.lnNoResult);
        tvNoResult = view.findViewById(R.id.tvNoResult);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initData();
            }
        }, 300);
    }

    private void initData() {
        lvItem.setOnScrollListener(this);
        swipeLayout.setOnRefreshListener(this);

        setAdapter();
        getListResult();
    }

    private void setAdapter() {
        this.historyOrderAdapter = new DeliHistoryOrderAdapter(activity, R.layout.history_order_item, itemClickListener);
        this.lvItem.setAdapter(historyOrderAdapter);
    }

    private void addListItem(ArrayList<DeliOrderModel> listItem) {
        historyOrderAdapter.addListItems(listItem);
    }

    private void getListResult() {
        swipeLayout.setRefreshing(true);
        if (!isLoading) {
            isLoading = true;
            DeliDataLoader.getUserOrderHistory(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ArrayList<DeliOrderModel> listItem = (ArrayList<DeliOrderModel>) object;
                        Helper.showLog("History size: " + listItem.size());
                        addListItem(listItem);
                        isLoading = false;
                        indexPage++;
                    } else {
                        isLoading = true;
                    }
                    swipeLayout.setRefreshing(false);
                    if (historyOrderAdapter.getListItems().size() <= 0) {
                        lnNoResult.setVisibility(View.VISIBLE);
                        tvNoResult.setText(getResources().getString(R.string.trans_no_result));
                    } else {
                        lnNoResult.setVisibility(View.GONE);
                    }
                }
            }, indexPage, 20, null, null, null);
        }

    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemCLick(int pos, String action, Object object, Long count) {
            if (action.equalsIgnoreCase(HistoryPromotionStickyAdapter.ITEM_CLICK_ACTION)) {
                DeliOrderModel dto = (DeliOrderModel) object;
                OrderTrackingActivity.startOrderTrackingActivity(activity, dto.getUuid().substring(dto.getUuid().indexOf('O') + 1, dto.getUuid().length()), null, true);
            }
        }
    };

    @Override
    public void onRefresh() {
        indexPage = 1;
        isLoading = false;
        setAdapter();
        getListResult();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!isLoading && totalItemCount > 0) {
            if (firstVisibleItem >= totalItemCount - visibleItemCount - 4) {
                getListResult();
            }
        }
    }

    @Override
    public void onClick(View v) {

    }
}
