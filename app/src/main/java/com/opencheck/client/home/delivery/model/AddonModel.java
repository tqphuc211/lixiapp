package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class AddonModel implements Serializable {
    private long id;
    private String name;
    private String state;
    private String pause_reason_message;
    private String description;
    private long price_modifier;
    private String data_type;
    private boolean isSelected = false;
    private Object default_value;
    private Integer min_number_value;
    private Integer max_number_value;
    private int quantity;
    private String category;

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public Object getDefault_value() {
        return default_value;
    }

    public void setDefault_value(Object default_value) {
        this.default_value = default_value;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPause_reason_message() {
        return pause_reason_message;
    }

    public void setPause_reason_message(String pause_reason_message) {
        this.pause_reason_message = pause_reason_message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPrice_modifier() {
        return price_modifier;
    }

    public void setPrice_modifier(long price_modifier) {
        this.price_modifier = price_modifier;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getMin_number_value() {
        return min_number_value;
    }

    public void setMin_number_value(Integer min_number_value) {
        this.min_number_value = min_number_value;
    }

    public Integer getMax_number_value() {
        return max_number_value;
    }

    public void setMax_number_value(Integer max_number_value) {
        this.max_number_value = max_number_value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    AddonGetModel convert() {
        AddonGetModel model = new AddonGetModel();
        model.setAddon_id(id);
        model.setAddon_name(name);
        model.setDate_type(data_type);
        if (data_type != null && data_type.equals("boolean"))
            model.setValue("true");
        else if (default_value != null) model.setValue(String.valueOf(((Double)default_value).intValue()));
        return model;
    }
}
