package com.opencheck.client.home.product.cart_new;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowNewPaymentMethodBinding;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterPaymentMethod extends RecyclerView.Adapter<AdapterPaymentMethod.PaymentHolder> {

    private LixiActivity activity;
    private ArrayList<PaymentMethodModel> paymentMethodModelArrayList;

    private int checkedItem = -1;
    private boolean isSaveCard = false;
    private AppPreferenceHelper appPreferenceHelper;

    public AdapterPaymentMethod(LixiActivity activity, ArrayList<PaymentMethodModel> paymentMethodModelArrayList) {
        this.activity = activity;
        this.paymentMethodModelArrayList = paymentMethodModelArrayList;
        appPreferenceHelper = new AppPreferenceHelper(activity);
    }

    @Override
    public PaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowNewPaymentMethodBinding rowNewPaymentMethodBinding =
                RowNewPaymentMethodBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new PaymentHolder(rowNewPaymentMethodBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(PaymentHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        if (paymentMethodModelArrayList == null || paymentMethodModelArrayList.size() == 0)
            return 0;
        return paymentMethodModelArrayList.size();
    }

    public PaymentMethodModel getPaymentMethod() {
        if (checkedItem >= 0) {
            return paymentMethodModelArrayList.get(checkedItem);
        }

        return null;
    }

    public boolean isSaveCard() {
        return isSaveCard;
    }

    public class PaymentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgRadio;
        TextView txtPaymentMethod, txtSaveCard, txtPaymentValue;
        CheckBox cbSaveCard;

        public PaymentHolder(View itemView) {
            super(itemView);
            imgRadio = (ImageView) itemView.findViewById(R.id.imgRadio);
            txtPaymentMethod = (TextView) itemView.findViewById(R.id.txtPaymentMethod);
            txtSaveCard = (TextView) itemView.findViewById(R.id.txtSaveCard);
            txtPaymentValue = (TextView) itemView.findViewById(R.id.txtPaymentValue);
            cbSaveCard = (CheckBox) itemView.findViewById(R.id.cbSaveCard);

            itemView.setOnClickListener(this);
            cbSaveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSaveCard = cbSaveCard.isChecked();
                }
            });
        }

        public void bind() {
            cbSaveCard.setChecked(false);
            cbSaveCard.setVisibility(View.GONE);
            txtSaveCard.setVisibility(View.GONE);
            PaymentMethodModel paymentMethod = paymentMethodModelArrayList.get(getAdapterPosition());
            if (checkedItem == getAdapterPosition()) {
                imgRadio.setBackground(activity.getResources().getDrawable(R.drawable.icon_radio_checked));

                if (!paymentMethod.getKey().equals("cod")){
                    txtPaymentValue.setVisibility(View.GONE);
                }else {
                    txtPaymentValue.setVisibility(View.VISIBLE);
                    txtPaymentValue.setText(getCodFeeInfo(paymentMethodModelArrayList.get(getAdapterPosition()).getPaymentCodFee()));
                }

                if (paymentMethod.getKey().contains("napas")) {
                    if (appPreferenceHelper.getTokenBankCard(paymentMethod.getKey()) == null || appPreferenceHelper.getTokenBankCard(paymentMethod.getKey()).size() == 0) {
                        cbSaveCard.setVisibility(View.VISIBLE);
                        txtSaveCard.setVisibility(View.GONE);
                    } else {
                        cbSaveCard.setVisibility(View.GONE);
                        txtSaveCard.setVisibility(View.VISIBLE);
                        String savedBankCard = String.format(LanguageBinding.getString(R.string.cash_evoucher_bank_card, activity),
                                appPreferenceHelper.getTokenBankCard(paymentMethod.getKey()).size());
                        txtSaveCard.setText(savedBankCard);
                    }
                } else {
                    cbSaveCard.setVisibility(View.GONE);
                    txtSaveCard.setVisibility(View.GONE);
                }
            } else {
                imgRadio.setBackground(activity.getResources().getDrawable(R.drawable.icon_radio_uncheck));
                cbSaveCard.setVisibility(View.GONE);
                txtPaymentValue.setVisibility(View.GONE);
            }

            txtPaymentMethod.setText(paymentMethodModelArrayList.get(getAdapterPosition()).getValue());
        }

        @Override
        public void onClick(View view) {
            checkedItem = getAdapterPosition();
            notifyDataSetChanged();
        }

        private Spanned getCodFeeInfo(String codInfo) {
            try {
                JSONObject jsonObject = new JSONObject(codInfo);
                String minMaxInfo = String.format(LanguageBinding.getString(R.string.cod_min_max_info, activity),
                        Helper.getVNCurrency(jsonObject.getLong("min")),
                        Helper.getVNCurrency(jsonObject.getLong("max")));
                return Html.fromHtml(minMaxInfo);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return Html.fromHtml(LanguageBinding.getString(R.string.cant_find_cod_payment_fee, activity));
        }
    }
}
