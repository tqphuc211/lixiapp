package com.opencheck.client.home.delivery.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;

public class UpdateLocationDialog extends LixiDialog {

    public UpdateLocationDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackground, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackground, _titleBar);
    }

    private TextView tvAddress, tvUpdate, tvCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_location);

        tvAddress = findViewById(R.id.tvAddress);
        tvUpdate = findViewById(R.id.tvUpdate);
        tvCancel = findViewById(R.id.tvCancel);

        tvUpdate.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    public void setData(String address) {
        tvAddress.setText(address);
    }

    @Override
    public void onClick(View view) {
        if (view == tvCancel) {
            dismiss();
        } else if (view == tvUpdate) {
            eventListener.onUpdate();
            dismiss();
        }
    }

    public interface IDialogEvent {
         void onUpdate();
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(IDialogEvent listener) {
        eventListener = listener;
    }
}
