package com.opencheck.client.home.flashsale.Adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowFlashSaleItemHomeBinding;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;


public class FlashSaleHomeAdapter extends RecyclerView.Adapter<FlashSaleHomeAdapter.FlashSaleHomeHolder> {
    private LixiActivity activity;
    private ArrayList<FlashSaleNow.ProductsBean> listProductsBean;
    private boolean isHappening;

    public static final String FLASH_SALE_VOUCHER_ID = "flash sale voucher id";

    private ItemClickListener itemClickListener;

    public FlashSaleHomeAdapter(LixiActivity activity, ArrayList<FlashSaleNow.ProductsBean> listProductsBean, boolean isHappening) {
        this.activity = activity;
        this.listProductsBean = listProductsBean;
        this.isHappening = isHappening;
    }


    @Override
    public FlashSaleHomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowFlashSaleItemHomeBinding rowFlashSaleItemHomeBinding =
                RowFlashSaleItemHomeBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new FlashSaleHomeHolder(rowFlashSaleItemHomeBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(FlashSaleHomeHolder holder, int position) {
        FlashSaleNow.ProductsBean productsBean = listProductsBean.get(position);
        if (holder != null) {
            long orgPrice = productsBean.getPayment_price();
            long salePrice = productsBean.getPayment_discount_price();
            long disPrice = (long) (((double) (orgPrice - salePrice) / orgPrice) * 100);

            ImageLoader.getInstance().displayImage(productsBean.getProduct_cover_image(), holder.imgBanner, LixiApplication.getInstance().optionsNomal);
            holder.txtName.setText(productsBean.getName());
            holder.txtSalePrice.setText(Helper.getVNCurrency(salePrice) + Html.fromHtml(activity.getString(R.string.p_under)));

            // Happening
            if (isHappening) {
                holder.txtSaling.setVisibility(View.GONE);
                // progress
                if (productsBean.getQuantity_order_done() == productsBean.getQuantity_limit()) {
                    holder.txtSoldOut.setVisibility(View.VISIBLE);
                    holder.progSold.setVisibility(View.GONE);
                    holder.txtSold.setVisibility(View.GONE);
                } else {
                    holder.progSold.setVisibility(View.VISIBLE);
                    holder.txtSold.setVisibility(View.VISIBLE);
                    holder.txtSoldOut.setVisibility(View.GONE);
                    holder.progSold.setMax(productsBean.getQuantity_limit());
                    holder.progSold.setProgress(productsBean.getQuantity_order_done());
                    holder.txtSold.setText("Đã bán " + productsBean.getQuantity_order_done());
                }
            } else {
                holder.txtSaling.setVisibility(View.VISIBLE);
                holder.progSold.setVisibility(View.GONE);
                holder.txtSold.setVisibility(View.GONE);
                holder.txtSoldOut.setVisibility(View.GONE);
            }

            if (disPrice != 0) {
                // Sale off
                holder.frameSaleOff.setVisibility(View.VISIBLE);
                holder.frameLixi.setVisibility(View.INVISIBLE);
                holder.txtSaleOff.setText(disPrice + "%");
            } else {
                // Lixi
                holder.frameSaleOff.setVisibility(View.INVISIBLE);
                holder.frameLixi.setVisibility(View.VISIBLE);
                holder.txtLixi.setText("+" + Helper.getVNCurrency(productsBean.getCashback_price()));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listProductsBean == null) {
            return 0;
        }
        return listProductsBean.size();
    }

    public void onItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class FlashSaleHomeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName, txtSalePrice, txtSold, txtSoldOut, txtSaleOff, txtLixi, txtSaling;
        ImageView imgBanner;
        RelativeLayout frameSaleOff, frameLixi;
        ProgressBar progSold;

        public FlashSaleHomeHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtSalePrice = (TextView) itemView.findViewById(R.id.txtSalePrice);
            txtSold = (TextView) itemView.findViewById(R.id.txtSold);
            txtSoldOut = (TextView) itemView.findViewById(R.id.txtSoldOut);
            txtSaleOff = (TextView) itemView.findViewById(R.id.txtSaleOff);
            txtLixi = (TextView) itemView.findViewById(R.id.txtLixi);
            txtSaling = (TextView) itemView.findViewById(R.id.txtSaling);

            imgBanner = (ImageView) itemView.findViewById(R.id.imgBanner);
            frameSaleOff = (RelativeLayout) itemView.findViewById(R.id.frameSaleOff);
            frameLixi = (RelativeLayout) itemView.findViewById(R.id.frameLixi);

            progSold = (ProgressBar) itemView.findViewById(R.id.progSold);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemCLick(getAdapterPosition(), "click", (long) listProductsBean.get(getAdapterPosition()).getId(), 0L);
            }
        }
    }
}
