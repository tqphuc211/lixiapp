package com.opencheck.client.home.account.new_layout.communicate;

public interface OnCreateCardResultListener {
    void onSuccess();
    void onFail();
    void onCardExisted();
}
