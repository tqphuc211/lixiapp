package com.opencheck.client.home.merchant;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.MerchantDetailMenuSlideItemBinding;
import com.opencheck.client.models.merchant.MenuModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */
public class MerchantDetailMenuSlideAdapter extends RecyclerView.Adapter {

    LixiActivity activity;
    List<MenuModel> listMenu;

    public MerchantDetailMenuSlideAdapter(LixiActivity activity, List<MenuModel> listCode) {
        this.activity = activity;
        this.listMenu = listCode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == TYPE_HEADER) {
//            itemView = inflater.inflate(R.layout.reward_code_item_header, parent, false);
//            return new HolderHeader(itemView);
//        }
        if (viewType == TYPE_ITEM) {
            MerchantDetailMenuSlideItemBinding merchantDetailMenuSlideItemBinding =
                    MerchantDetailMenuSlideItemBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);

            return new HolderItemCode(merchantDetailMenuSlideItemBinding.getRoot());
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        if (holder.getItemViewType() == TYPE_HEADER) {
//            HolderHeader viewHolder = (HolderHeader) holder;
//        }
        if (holder.getItemViewType() == TYPE_ITEM) {
            HolderItemCode viewHolder = (HolderItemCode) holder;
            MenuModel dto = listMenu.get(position);

            viewHolder.rootItemView.setTag(position);

//            if (position == 0)
//                viewHolder.v_left.setVisibility(View.VISIBLE);
//            else
//                viewHolder.v_left.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage(dto.getLink(), viewHolder.iv, LixiApplication.getInstance().optionsNomal);

//            viewHolder.tv_code.setText("Mã Lixi: " + dto.getCode());
//            viewHolder.tv_description.setText(getSpanable(dto.getDesc(), dto.getDesc_data()));
//            viewHolder.tv_date.setText(dto.getStart_date() + "-" + dto.getEnd_date());
//
        }
    }
//
//    public class HolderHeader extends RecyclerView.ViewHolder {
//        public HolderHeader(View itemView) {
//            super(itemView);
//        }
//    }

    public class HolderItemCode extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        //        private View v_left;
        private ImageView iv;

        public HolderItemCode(View itemView) {
            super(itemView);
            rootItemView = itemView;
//            v_left = itemView.findViewById(R.id.v_left);
            iv = (ImageView) itemView.findViewById(R.id.iv);
            rootItemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView) {
//                dialogDetail = new PopupLixiCodeDetail(activity, listMenu.get((int) v.getTag()).getId(), false, false, false);
//                dialogDetail.show();
//                if (barner != null && barner.size() > 1)
//                    dialogDetail.setbaner(barner.get(1));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listMenu == null)
            return 0;
        return listMenu.size();
    }

    final int TYPE_HEADER = 0;
    final int TYPE_ITEM = 1;

    @Override
    public int getItemViewType(int position) {
        return 1;//Math.min(position, 1);
    }
}