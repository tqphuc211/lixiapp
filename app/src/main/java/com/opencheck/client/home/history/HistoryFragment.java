package com.opencheck.client.home.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.PagerSlidingTabStrip;
import com.opencheck.client.databinding.HistoryFragmentBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.fragments.DeliHistoryOrderFragment;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class HistoryFragment extends LixiFragment {
    private ViewGroup viewGroupHeader;

    private RelativeLayout rlBack;
    private TextView tvTitle;

    private HistoryFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = HistoryFragmentBinding.inflate(inflater, container, false);
        this.viewGroupHeader = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        rootview = mBinding.getRoot();

        return  mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initView(view);
    }

    private void initView(View view) {
        rlBack = view.findViewById(R.id.rlBack);
        tvTitle = view.findViewById(R.id.tvTitle);

        initPager();

        initData();
    }

    private void initData() {
        rlBack.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        tvTitle.setText(R.string.trans_title);
        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);
//            ((HomeActivity) activity).navigation.setVisibility(View.VISIBLE);
    }
    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            activity.onBackPressed();
        }
    }

    //region New

    public void initPager() {
        ViewPager viewPager = rootview.findViewById(R.id.viewpager);
        viewPager.setAdapter(new FragmentOrderAdapter(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(2);

        // Give the PagerSlidingTabStrip the ViewPager
        final PagerSlidingTabStrip tabsStrip = rootview.findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setTextColorResource(R.drawable.selector_text_color_lixi);

//        tabsStrip.setTypeface(Typeface.DEFAULT,R.style.AccountKit_InputText);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LinearLayout tab = (LinearLayout) tabsStrip.getChildAt(0);
                for (int i = 0; i < tab.getChildCount(); i++) {
                    TextView tv = (TextView) tab.getChildAt(i);
                    if (i != position) {
                        tv.setTextColor(0xAAAAAAAA);
                    } else {
                        tv.setTextColor(0xffac1ea1);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(1);
        viewPager.setCurrentItem(0);
        tabsStrip.setPadding(0,0,0,0);
    }

    public class FragmentOrderAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;
        String tabTitles[] = new String[]{LanguageBinding.getString(R.string.home_menu_title_deli, activity), LanguageBinding.getString(R.string.other, activity)};

        FragmentOrderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fm = null;
            switch (position) {
                case 0:
                    fm = new DeliHistoryOrderFragment();
                    break;
                case 1:
                    fm = new HistoryOrderFragment();
                    ((HistoryOrderFragment) fm).setTypeHistory(HistoryOrderFragment.ALL);
                    break;
            }
            return fm;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }
    //endregion


    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.HISTORY_HOME;
    }
}
