package com.opencheck.client.home.merchant.map;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class LoadLocationAddress extends AsyncTask<LatLng, Object, Object> {

    private LocationCallBack mCallBack;
    private LatLng mPosition;

    public LoadLocationAddress(LocationCallBack callback) {
        mCallBack = callback;
    }

    @Override
    protected Object doInBackground(LatLng... params) {

        mPosition = params[0];
        return getLocationName(mPosition);
    }

    @Override
    protected void onPostExecute(Object result) {

        super.onPostExecute(result);

//        mCallBack.callback(mPosition, updateLocation(String.valueOf(result)));
        ArrayList<String> callback = (ArrayList<String>) result;
        mCallBack.callback(mPosition, callback);
    }


    public ArrayList<String> getLocationName(LatLng key) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(makeUrl(key))
                    .build();

            Response response = client.newCall(request).execute();

            String respone = response.body().string();
//            Log.d("tinhvc", "GPS: "+respone);
            JSONObject jsonObject = new JSONObject(respone);
            JSONArray routeArray = jsonObject.getJSONArray("results");
            JSONObject note = routeArray.getJSONObject(0);
            Helper.showLog("Address: " + note.toString());
            ArrayList<String> res = new ArrayList<>();
            if(note.getString("formatted_address") == null ||note.getString("formatted_address").length() == 0){
                getLocationName(key);
            } else {
                res.add(note.getString("formatted_address"));
                res.add(note.getString("place_id"));
            }
            return res;
        } catch (Exception e) {
            return null;
        }
    }

    public String updateLocation(String location) {
        try {
            location = new String(location.getBytes("ISO-8859-1"), "UTF-8");
        } catch (Throwable e) {
            location = "";
            e.printStackTrace();
        }
        return location;
    }

    public String makeUrl(LatLng point) {
        StringBuilder urlString = new StringBuilder();

        urlString.append("https://maps.googleapis.com/maps/api/geocode/json?latlng=");
        urlString.append(point.latitude + "," + point.longitude);
        urlString.append("&sensor=false&key=AIzaSyAfxluMz7e0E1jdulRlUuABzFP3PX0h2Lk");
//        Log.d("tinhvc", "URL=" + urlString.toString());
        return urlString.toString().trim().replace(" ", "%20");
    }
}