package com.opencheck.client.home.newlogin.dialog;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.DialogInputPasswordBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.newlogin.communicate.OnForgotPassListener;
import com.opencheck.client.home.newlogin.communicate.OnLoginResult;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

public class InputPasswordDialog extends LixiDialog {
    private String phone;

    public InputPasswordDialog(@NonNull LixiActivity _activity, String phone) {
        super(_activity, false, true, false);
        this.phone = phone;
    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.LOGIN_PHONE_PASSWORD;
    }

    private DialogInputPasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogInputPasswordBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        mBinding.txtPhone.setText(phone);

        mBinding.txtLogin.setOnClickListener(this);
        mBinding.txtForgotPass.setOnClickListener(this);
        mBinding.btnBack.setOnClickListener(this);

        mBinding.txtForgotPass.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        Helper.showSoftKeyBoard(activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                cancel();
                break;
            case R.id.txtLogin:
                // login
                login();
                break;
            case R.id.txtForgotPass:
                // forgot pass
                if (onForgotPassListener != null) {
                    onForgotPassListener.onForgotPass(phone);
                }
                break;
        }
    }

    private OnForgotPassListener onForgotPassListener;

    public void setOnForgotPassListener(OnForgotPassListener onForgotPassListener) {
        this.onForgotPassListener = onForgotPassListener;
    }

    private OnLoginResult onLoginResult;

    public void setOnLoginResult(OnLoginResult onLoginResult) {
        this.onLoginResult = onLoginResult;
    }

    private void login() {

        String phone = mBinding.txtPhone.getText().toString().trim();
        String pass = mBinding.edtPhoneInput.getText().toString();

        if (pass.equals("")) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_pass_null, activity), Toast.LENGTH_SHORT).show();
            return;
        }

        JsonObject params = new JsonObject();
        params.addProperty("password", pass);
        params.addProperty("phone", phone);

        DataLoader.postLoginPhone(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (onLoginResult != null) {
                    if (isSuccess) {
                        GlobalTracking.trackLoggedInEvent(TrackingConstant.Login.ACCOUNT_KIT);
                        onLoginResult.onLogin(isSuccess, object);
                        cancel();
                    } else {
                        Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, params);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
