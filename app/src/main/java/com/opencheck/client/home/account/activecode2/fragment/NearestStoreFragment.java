package com.opencheck.client.home.account.activecode2.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.NearestStoreFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.activecode2.adapter.StoreByAdapter;
import com.opencheck.client.home.account.activecode2.dialog.NoApplyDialog;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.home.merchant.map.StoreMapActivity;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NearestStoreFragment extends LixiFragment implements SwipeRefreshLayout.OnRefreshListener {

    private RelativeLayout rlBack, rlSearch, rl_buy;
    private LinearLayout ll_gps;
    private TextView tv_turn_gps, tv_message;
    private RecyclerView rv_store;
    private SwipeRefreshLayout srf_refresh;
    private float curLat = -1.0f, curLong = -1.0f;
    private int page = 1;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private boolean isLoading = false;
    private ArrayList<NearestStoreModel> list_store = new ArrayList<>();
    private StoreByAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private LocationManager locationManager;
    private boolean isOutOfPage = false;

    private NearestStoreFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = NearestStoreFragmentBinding.inflate(inflater, container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        if (checkLocationPermission()) {
            initView(mBinding.getRoot());
        }
        rootview = mBinding.getRoot();
        return mBinding.getRoot();
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_COARSE_LOCATION);
            }
            return false;
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_FINE_LOCATION);
            }
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
                initView(rootview);
                break;
        }
    }

    private void initView(View v) {
        rlBack = v.findViewById(R.id.rlBack);
        rlSearch = v.findViewById(R.id.rlSearch);
        rl_buy = v.findViewById(R.id.rl_buy);
        rv_store = v.findViewById(R.id.rv_store);
        srf_refresh = v.findViewById(R.id.srf_refresh);
        ll_gps = v.findViewById(R.id.ll_gps);
        tv_turn_gps = v.findViewById(R.id.tv_turn_gps);
        tv_message = v.findViewById(R.id.tv_message);

        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rlBack.setOnClickListener(this);
        rlSearch.setOnClickListener(this);
        rl_buy.setOnClickListener(this);
        tv_turn_gps.setOnClickListener(this);
        srf_refresh.setOnRefreshListener(this);

        rv_store.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                        startLoadData();
                        isLoading = true;
                    }
                }
            }
        });

        if (checkGpsStatus()) {
            Helper.getGPSLocation(activity, new LocationCallBack() {
                @Override
                public void callback(LatLng position, ArrayList<String> address) {
                    curLat = (float) position.latitude;
                    curLong = (float) position.longitude;
                    Log.d("mapi", "Location: " + curLat + " " + curLong + "");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startLoadData();
                        }
                    }, 500);
                }
            });
            ll_gps.setVisibility(View.GONE);
        } else {
            ll_gps.setVisibility(View.VISIBLE);
        }

        if (new AppPreferenceHelper(activity).getInstructionVisible()) {
            tv_message.setVisibility(View.VISIBLE);
        } else
            tv_message.setVisibility(View.GONE);
    }

    private void turnGPSOn() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Cài đặt GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS đã tắt. Bạn có muốn đi đến menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton(LanguageBinding.getString(R.string.setting, activity), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
                ll_gps.setVisibility(View.GONE);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private boolean checkGpsStatus() {
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void setAdapter() {
        adapter = new StoreByAdapter(activity, list_store);
        rv_store.setLayoutManager(mLayoutManager);
        rv_store.setAdapter(adapter);
        adapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (action.equalsIgnoreCase(ConstantValue.ON_ITEM_STORE_CLICK)) {
//                    TestFragment fragment = new TestFragment();
//                    ((ActiveCode2Activity) activity).replaceFragment(fragment);
                    NearestStoreModel model = (NearestStoreModel) object;
                    if (model.isAllow_user_send_request_active()) {
                        Bundle bundle = new Bundle();
                        bundle.putString("store", new Gson().toJson(list_store.get(pos)));
                        UsedVoucherFragment fragment = new UsedVoucherFragment();
                        fragment.setArguments(bundle);
                        ((ActiveCode2Activity) activity).replaceFragment(fragment);
                    } else {
                        NoApplyDialog dialog = new NoApplyDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData((NearestStoreModel) object);
                    }
                } else if (action.equalsIgnoreCase(ConstantValue.ON_MAP_CLICK)) {
                    ArrayList<StoreModel> arrTemp = new ArrayList<StoreModel>();
                    for (int i = 0; i < list_store.size(); i++) {
                        StoreModel storeModel = list_store.get(i).convertToStoreModel();
                        arrTemp.add(storeModel);
                    }
                    StoreModel tempStore = arrTemp.get(pos);
                    arrTemp.remove(arrTemp.get(pos));
                    arrTemp.add(0, tempStore);

                    MerchantModel merchantModel = new MerchantModel();
                    Bundle bundle = new Bundle();
                    merchantModel.setListStore(arrTemp);
                    merchantModel.setId(String.valueOf(list_store.get(pos).getMerchant_info().getId()));
                    Intent intent = new Intent(activity, StoreMapActivity.class);
                    bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            }
        });
    }

    private void startLoadData() {
        if (!isLoading) {
            if (!isOutOfPage) {
                srf_refresh.setRefreshing(false);
                DataLoader.getNearestStore(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            if (page == 1) {
                                list_store = (ArrayList<NearestStoreModel>) object;
                                if (list_store != null && list_store.size() > 0) {
                                    setAdapter();
                                    setOutOfPage(list_store);
                                }
                            } else {
                                ArrayList<NearestStoreModel> list = (ArrayList<NearestStoreModel>) object;
                                if (list != null && list.size() > 0 && list_store != null) {
                                    setOutOfPage(list);
                                    list_store.addAll(list);

                                }
                            }
                            adapter.notifyDataSetChanged();
                            page++;
                            isLoading = false;
                        } else {
                            Helper.showErrorDialog(activity, object.toString());
                            isLoading = true;
                        }
                    }
                }, page, curLat, curLong);
            }
        }
    }

    private void setOutOfPage(ArrayList<NearestStoreModel> arrayList) {
        if (arrayList.size() < RECORD_PER_PAGE) {
            isOutOfPage = true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                activity.onBackPressed();
                break;
            case R.id.rlSearch:
                Bundle bundle = new Bundle();
                bundle.putFloat("lat", curLat);
                bundle.putFloat("long", curLong);
                SearchStoreFragment fragment = new SearchStoreFragment();
                ((ActiveCode2Activity) activity).replaceFragment(fragment);
                break;
            case R.id.rl_buy:
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                    }
                } else {
                    ScannerQRFragment fragment1 = new ScannerQRFragment();
                    ((ActiveCode2Activity) activity).replaceFragment(fragment1);
                }
                break;
            case R.id.tv_turn_gps:
                turnGPSOn();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActiveCode2Activity) activity).toolbar.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        page = 1;
        isLoading = false;
        isOutOfPage = false;
        list_store.clear();
        if (adapter != null)
            adapter.notifyDataSetChanged();

        if (checkGpsStatus()) {
            Helper.getGPSLocation(activity, new LocationCallBack() {
                @Override
                public void callback(LatLng position, ArrayList<String> address) {
                    curLat = (float) position.latitude;
                    curLong = (float) position.longitude;
                    Log.d("mapi", "Location: " + curLat + " " + curLong + "");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startLoadData();
                        }
                    }, 500);
                }
            });
            ll_gps.setVisibility(View.GONE);
        } else {
            srf_refresh.setRefreshing(false);
            ll_gps.setVisibility(View.VISIBLE);
        }
    }
}
