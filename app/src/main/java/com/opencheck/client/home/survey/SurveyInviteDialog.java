package com.opencheck.client.home.survey;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyInviteDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.merchant.survey.SurveyAnswerResultModel;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.utils.Helper;

import org.json.JSONObject;

import java.util.List;

public class SurveyInviteDialog extends LixiDialog {

    public SurveyInviteDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected LixiActivity activity;
    private boolean fullScreen;
    private boolean dimBackgroud;
    private boolean titleBar;

    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu;
    private ImageView iv_logo;
    private ImageView iv_close;
    private TextView tv_title;
    private TextView tv_message;
    private LinearLayout ll_code;
    private TextView tv_code;
    private Button bt_ok;

    private SurveyModel surveyModel;
    private SurveyAnswerResultModel surveyAnswerModel;

    SpannableStringBuilder spanMessage;
    String mss = "";
    String title = "";
    String okText = "";

    private SurveyInviteDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = SurveyInviteDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);

        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_message = (TextView) findViewById(R.id.tv_message);
        ll_code = (LinearLayout) findViewById(R.id.ll_code);
        tv_code = (TextView) findViewById(R.id.tv_code);
        bt_ok = (Button) findViewById(R.id.bt_ok);

        tv_title.setText(title);
        if (spanMessage != null)
            tv_message.setText(spanMessage);
        else
            tv_message.setText(mss);

        if (okText.length() > 0)
            bt_ok.setText(okText);

        rl_dismiss.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
        iv_close.setOnClickListener(this);
        ll_code.setOnClickListener(this);
    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    public void setData(SurveyModel surveyModel) {
        this.surveyModel = surveyModel;

        String mess = surveyModel.getWelcome_message();
        List<String> data = null;
        SpannableStringBuilder messageBuilder = new SpannableStringBuilder();
        data = surveyModel.getWelcome_data();
        if (data != null) {
            int st = 0;
            for (int i = 0; i < data.size(); i++) {
                int pos = mess.indexOf("{" + i + "}");
                if (pos > 0) {
                    messageBuilder.append(mess.substring(st, pos));
                    st = pos + 2 + (i > 9 ? 2 : 1);
                    SpannableString redSpannable = new SpannableString(data.get(i));
                    redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.app_violet)), 0, data.get(i).length(), 0);
                    redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, data.get(i).length(), 0);
                    messageBuilder.append(redSpannable);
                }
            }
            if (st < mess.length() - 1)
                messageBuilder.append(mess.substring(st, mess.length()));
//                                            mess.replace("{" + i + "}", data.getString(i));
        } else
            messageBuilder.append(mess);

        tv_title.setText("Khảo sát từ " + surveyModel.getMerchant_name());
        tv_message.setText(messageBuilder);
        ImageLoader.getInstance().displayImage(surveyModel.getMerchant_logo(), iv_logo, LixiApplication.getInstance().optionsNomal);
    }

    public void setData(SurveyAnswerResultModel surveyAnswerModel) {
        this.surveyAnswerModel = surveyAnswerModel;

        String mess = surveyAnswerModel.getResponse_message();
        List<String> data = null;
        SpannableStringBuilder messageBuilder = new SpannableStringBuilder();
        data = surveyAnswerModel.getResponse_data();
        if (data != null) {
            int st = 0;
            for (int i = 0; i < data.size(); i++) {
                int pos = mess.indexOf("{" + i + "}");
                if (pos > 0) {
                    messageBuilder.append(mess.substring(st, pos));
                    st = pos + 2 + (i > 9 ? 2 : 1);
                    SpannableString redSpannable = new SpannableString(data.get(i));
                    redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.app_violet)), 0, data.get(i).length(), 0);
                    redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, data.get(i).length(), 0);
                    messageBuilder.append(redSpannable);
                }
            }
            if (st < mess.length() - 1)
                messageBuilder.append(mess.substring(st, mess.length()));
//                                            mess.replace("{" + i + "}", data.getString(i));
        } else
            messageBuilder.append(mess);

        if (surveyAnswerModel.getReward_code() != null && surveyAnswerModel.getReward_code().length() > 0)
            ll_code.setVisibility(View.VISIBLE);
        else
            ll_code.setVisibility(View.GONE);

        iv_close.setVisibility(View.GONE);
        tv_code.setText("Code: " + surveyAnswerModel.getReward_code());
        bt_ok.setText("Đóng");
        tv_title.setText("Hoàn thành khảo sát");//surveyAnswerModel.getMerchant_name());
        tv_message.setText(messageBuilder);
        ImageLoader.getInstance().displayImage(surveyAnswerModel.getMerchant_logo(), iv_logo, LixiApplication.getInstance().optionsNomal);
    }

    private void declineSurvey() {
        if (getSurveyModel() == null)
            return;

        try {
//            Log.d("mapi", ">huy ks>" + getSurveyModel().getId());
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("survey_id", getSurveyModel().getId());
//            Log.d("mapi", ">huy ks>" + getSurveyModel().getId());

            DataLoader.userDeclineSurvey(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        dismiss();
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                        dismiss();
                    }
                }
            }, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_close:
                declineSurvey();
//                new AppPreferencesLib(activity).setCrSurvey(surveyModel.getId());
                dismiss();
                break;
            case R.id.bt_ok:
                if (getSurveyModel() != null) {
                    if (eventListener != null) {
                        eventListener.onOk(getSurveyModel());
                    }
                }
                dismiss();
                break;
            case R.id.ll_code:
                if (getSurveyModel() == null) {
                    Helper.copyCode(surveyAnswerModel.getReward_code(),  activity);
                }
                break;
            default:
                break;
        }
    }

    public SurveyModel getSurveyModel() {
        return surveyModel;
    }

    public interface IDidalogEvent {
        public void onOk(SurveyModel surveyModel);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
