package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.custom.tagview.TagView;
import com.opencheck.client.databinding.ActivitySearchStoreBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.DeliStoreAdapter;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliApiWrapperForListModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class SearchStoreActivity extends LixiActivity implements SwipeRefreshLayout.OnRefreshListener, TagView.OnTagClickListener {
    private int page = 1;
    private int pageSize = DeliDataLoader.record_per_page;
    private DeliStoreAdapter adapter;
    private DeliStoreAdapter recentStoreAdapter;
    private boolean isLoading = false;
    private DeliAddressModel mLocation;
    private AppPreferenceHelper appPreferenceHelper;

    private ActivitySearchStoreBinding mBinding;

    private Call<DeliApiWrapperForListModel<StoreOfCategoryModel>> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_store);
        appPreferenceHelper = new AppPreferenceHelper(activity);
        initViews();
    }

    private void initViews() {
        mBinding.ivBack.setOnClickListener(this);
        mBinding.tvSearch.setOnClickListener(this);

        mBinding.tagViewHotKeyword.setOnTagClickListener(this);
        mBinding.tagViewHistory.setOnTagClickListener(this);
        setAdapter();

        mBinding.searchBar.setOnSearchEventListener(new SearchBar.OnSearchEventListener() {
            @Override
            public void onTextChange(@NonNull String text) {
                if (text.equals("")) {
                    if (call != null){
                        call.cancel();
                    }
                    setDefault();
                    if (searchHandler != null && searchRunnable != null) {
                        searchHandler.removeCallbacks(searchRunnable);
                    }
                    return;
                }

                handleDelaySearch(text);
            }

            @Override
            public void onComplete(@NonNull String text) {
                search(text);
                hideKeyboard();
            }
        });

        mBinding.swr.setOnRefreshListener(this);
        setDefault();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                hideKeyboard();
                finish();
                break;
            case R.id.tv_search:
                mBinding.searchBar.focusEdittext();
                break;
        }
    }

    private void search(String keyword) {
        isLoading = false;
        page = 1;
        mBinding.swr.setRefreshing(false);
        keyword = keyword.trim();
        if (keyword.equals("")) {
            // về mặc định
            setDefault();
            return;
        }

        if (mLocation != null)
            getStore(keyword);
        else finish();
    }

    private void setDefault() {
        mBinding.recStore.setVisibility(View.GONE);
        mBinding.linearMore.setVisibility(View.VISIBLE);
        isLoading = false;
        page = 1;
        mBinding.swr.setRefreshing(false);
        mBinding.tagViewHistory.removeAll();
        mBinding.tagViewHotKeyword.removeAll();
        getHistoryTag();
        getHotKeyword();
    }

    private void setAdapter() {
        adapter = new DeliStoreAdapter(activity, null);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        mBinding.rcvStore.setLayoutManager(linearLayoutManager);
        mBinding.rcvStore.setAdapter(adapter);

        mBinding.rcvStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                hideKeyboard();
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mBinding.rcvStore.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisibleItems) >= totalItemCount - 5) {
                        getStore(mBinding.searchBar.getText());
                        isLoading = true;
                    }
                }
            }
        });

        adapter.setOnStoreClickListener(new DeliStoreAdapter.OnStoreClickListener() {
            @Override
            public void onClick() {
                appPreferenceHelper.setHistorySearchDeli(mBinding.searchBar.getText());
            }
        });

        recentStoreAdapter = new DeliStoreAdapter(activity, null);
        mBinding.rvRecentStore.setLayoutManager(new LinearLayoutManager(activity));
        mBinding.rvRecentStore.setAdapter(recentStoreAdapter);
        mBinding.rvRecentStore.setHasFixedSize(true);
        mBinding.rvRecentStore.setNestedScrollingEnabled(false);
    }

    private void loadRecentStore() {
        if(UserModel.getInstance() != null) {
            HashMap<String, ArrayList<StoreOfCategoryModel>> mapStore = Helper.getStoreFromFile(activity, ConstantValue.STORE_FILE);
            ArrayList<StoreOfCategoryModel> stores = mapStore.get(UserModel.getInstance().getId());
            if(stores != null && stores.size() > 0) {
                ArrayList<StoreOfCategoryModel> cloneStores = new ArrayList<>(stores);
                Collections.reverse(cloneStores);
                recentStoreAdapter.setListStore(cloneStores);
            } else {
                mBinding.llRecentStore.setVisibility(View.GONE);
            }
        }
    }

    private void resetList() {
        if (adapter == null || adapter.getListStore() == null) return;
        adapter.getListStore().clear();
        adapter.notifyDataSetChanged();
    }

    private void getStore(final String key) {
        if (!isLoading) {
            mBinding.swr.setRefreshing(true);
            if (call != null){
                call.cancel();
            }

            call = DeliDataLoader.getStoreListByKey(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    mBinding.swr.setRefreshing(false);
                    if (isSuccess) {
                        ArrayList<StoreOfCategoryModel> arrayList = (ArrayList<StoreOfCategoryModel>) object;
                        if (page == 1) {
                            mBinding.recStore.setVisibility(View.VISIBLE);
                            mBinding.linearMore.setVisibility(View.GONE);
                            resetList();
                            if (arrayList == null || arrayList.size() == 0) {
                                mBinding.lnNoResult.setVisibility(View.VISIBLE);
                                GlobalTracking.trackEventSearch(TrackingConstant.ContentType.STORE, key, false);
                                return;
                            }
                            mBinding.lnNoResult.setVisibility(View.GONE);
                            GlobalTracking.trackEventSearch(TrackingConstant.ContentType.STORE, key, true);
                        } else {
                            if (arrayList == null || arrayList.size() == 0 || arrayList.size() < pageSize) {
                                return;
                            }
                        }
                        adapter.setListStore(arrayList);
                        isLoading = false;
                        page++;
                    } else {
                        isLoading = true;
                        Helper.showErrorDialog(activity, (String) object);
                        GlobalTracking.trackEventSearch(TrackingConstant.ContentType.STORE, key, false);
                    }
                }
            }, key, null, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        }
    }

    private void getHistoryTag() {
        List<String> history = appPreferenceHelper.getHistorySearchDeli();
        if (history.size() == 0) {
            mBinding.linearHistory.setVisibility(View.GONE);
        } else {
            mBinding.linearHistory.setVisibility(View.VISIBLE);
            for (String tag : history) {
                mBinding.tagViewHistory.addTag(tag);
            }
        }
    }

    private void getHotKeyword() {
        ArrayList<DeliConfigModel> configModels = DeliConfigModel.getData(this);
        for (DeliConfigModel model : configModels) {
            if (model.getKey().equals("hot_keyword")) {
                if (model.getValue().equals("")) {
                    mBinding.linearHotKeyword.setVisibility(View.GONE);
                } else {
                    mBinding.linearHotKeyword.setVisibility(View.VISIBLE);
                    for (String tag : appPreferenceHelper.convertStrings(model.getValue().replace(",", "|"))) {
                        mBinding.tagViewHotKeyword.addTag(tag);
                    }
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        search(mBinding.searchBar.getText());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(recentStoreAdapter != null) {
            if(recentStoreAdapter.getListStore() != null) {
                recentStoreAdapter.getListStore().clear();
            }
        }
        loadRecentStore();
    }

    @Override
    public void onTagClick(String tag, int position) {
        mBinding.searchBar.setText(tag);
        search(tag);
    }

    private Handler searchHandler;
    private Runnable searchRunnable;

    private void handleDelaySearch(final String text) {
        final int SEARCH_DELAY = 500;
        if (searchHandler != null && searchRunnable != null) {
            searchHandler.removeCallbacks(searchRunnable);
        }

        searchHandler = new Handler();
        searchRunnable = new Runnable() {
            @Override
            public void run() {
                search(text);
                searchHandler.removeCallbacks(this);
            }
        };


        searchHandler.postDelayed(searchRunnable, SEARCH_DELAY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_SEARCH;
    }
}
