package com.opencheck.client.home.lixishop.detail;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowDetailComboBinding;
import com.opencheck.client.models.lixishop.ComboItemModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

class AdapterDetailCombo extends RecyclerView.Adapter {
    EvoucherDetail activityLixiEvoucherDetail;
    ArrayList<ComboItemModel> listCombo = new ArrayList<>();

    public AdapterDetailCombo(EvoucherDetail activityLixiEvoucherDetail, ArrayList<ComboItemModel> listCombo) {
        this.activityLixiEvoucherDetail = activityLixiEvoucherDetail;
        this.listCombo = listCombo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowDetailComboBinding rowDetailComboBinding =
                RowDetailComboBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new HolderItem(rowDetailComboBinding.getRoot());
    }

    public void SetRatioContent(HolderItem holder) {
        int height = activityLixiEvoucherDetail.getWindowManager().getDefaultDisplay().getHeight();
        int with = activityLixiEvoucherDetail.getWindowManager().getDefaultDisplay().getWidth();

        ViewGroup.LayoutParams layoutParams = holder.ivItem.getLayoutParams();
        layoutParams.width = with / 3;
        int withOftvName = layoutParams.width / 5 * 4;
        layoutParams.height = layoutParams.width * 3 / 4;
        holder.ivItem.setLayoutParams(layoutParams);
        holder.tvName.setWidth(withOftvName);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_3), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5));
        lp.width = with / 3 + activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_15);
        lp.height = with / 3 * 16 / 9 - activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_40);
        holder.lnItem.setLayoutParams(lp);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder _holder, final int position) {
        HolderItem holder = (HolderItem) _holder;
        try {
            SetRatioContent(holder);

            ImageLoader.getInstance().displayImage(listCombo.get(position).getProduct_image_medium().get(0), holder.ivItem, LixiApplication.getInstance().optionsNomal);
            holder.tvName.setText(listCombo.get(position).getName());
            holder.tvPrice.setText(Helper.getVNCurrency(listCombo.get(position).getDiscount_price()) + " Lixi");
            holder.tvPrice.setTextColor(activityLixiEvoucherDetail.getResources().getColor(R.color.app_violet));
            holder.tvQuantity.setText(listCombo.get(position).getQuantity() + " voucher");

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.lnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activityLixiEvoucherDetail, EvoucherDetail.class);
                intent.putExtra(EvoucherDetail.PRODUCT_ID, listCombo.get(position).getId());
                intent.putExtra(ConstantValue.IS_COMBO_ITEM, true);
                activityLixiEvoucherDetail.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCombo.size();
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        ImageView ivItem;
        TextView tvName, tvPrice, tvQuantity;
        LinearLayout lnItem;

        public HolderItem(final View itemView) {
            super(itemView);
            lnItem = itemView.findViewById(R.id.lnItem);
            ivItem = itemView.findViewById(R.id.ivItem);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
        }
    }
}