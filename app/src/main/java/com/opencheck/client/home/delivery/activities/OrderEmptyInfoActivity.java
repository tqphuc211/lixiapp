package com.opencheck.client.home.delivery.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityOrderEmptyInfoBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.controller.FoodController;
import com.opencheck.client.home.delivery.dialog.InfoOrderDialog;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.ProductGetModel;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class OrderEmptyInfoActivity extends LixiActivity {

    private ActivityOrderEmptyInfoBinding mBinding;
    private long storeId = 0L;
    private String source;
    private String mReceiveTimeChanged = null;
    private boolean isRegister = true;
    private HashMap<String, CartModel> cart;
    private IntentFilter intentFilter = new IntentFilter("LOCATION_CHANGE");
    public final static int EMPTY_ORDER_RESUME_REQUEST_CODE = 197;

    public static void startEmptyOrderCheckoutActivity(Activity context, long storeId, String source, String receiveTime) {
        Intent intent = new Intent(context, OrderEmptyInfoActivity.class);
        intent.putExtra("ID", storeId);
        intent.putExtra("RECEIVE_TIME", receiveTime);
        intent.putExtra(GlobalTracking.SOURCE, source);
        context.startActivityForResult(intent, EMPTY_ORDER_RESUME_REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_empty_info);
        storeId = getIntent().getLongExtra("ID", 0L);
        source = getIntent().getStringExtra(GlobalTracking.SOURCE);
        mReceiveTimeChanged = getIntent().getStringExtra("RECEIVE_TIME");
        initEvents();
        showData();
    }

    private void initEvents() {
        mBinding.tvGetAddress.setPaintFlags(mBinding.tvGetAddress.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.lnInfo.setOnClickListener(this);
        mBinding.rlGetAddress.setOnClickListener(this);
        mBinding.tvEditInfo.setOnClickListener(this);
        mBinding.tvOrder.setOnClickListener(this);
        activity.registerReceiver(mReceiver, intentFilter);
        getStoreDetail();
    }

    private void showData() {
        cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        if (cart.containsKey("" + storeId)) {
            if (cart.get("" + storeId) != null && cart.get("" + storeId).getProduct_list() != null) {
                ArrayList<ProductGetModel> productList = new ArrayList<>();
                Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
                for (String key : keySet) {
                    ArrayList<ProductModel> temp = cart.get("" + storeId).getProduct_list().get(key);
                    for (int j = 0; j < temp.size(); j++) {
                        productList.add(temp.get(j).convert());
                    }
                }

                long total = 0L;
                int quantity = 0;
                for (int i = 0; i < productList.size(); i++) {
                    total += productList.get(i).getPrice() * productList.get(i).getQuantity();
                    quantity += productList.get(i).getQuantity();
                    new FoodController(activity, productList.get(i), mBinding.llFoodList, true, i, productList.size());
                }

                if (mReceiveTimeChanged != null) {
                    Calendar currentDate = Calendar.getInstance();
                    Calendar receiveDate = Calendar.getInstance();
                    receiveDate.setTimeInMillis(Long.parseLong(mReceiveTimeChanged));
                    int diff = Helper.daysBetween(currentDate, receiveDate);
                    if (diff == 0)
                        mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, mReceiveTimeChanged)
                                + LanguageBinding.getString(R.string.deli_tracking_today, activity));
                    else if (diff == 1)
                        mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, mReceiveTimeChanged)
                                + LanguageBinding.getString(R.string.deli_tracking_tomorrow, activity));
                    else if (diff > 1)
                        mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, mReceiveTimeChanged));
                } else
                    mBinding.tvTime.setText(LanguageBinding.getString(R.string.deli_tracking_earliest, activity));
                mBinding.tvQuantityOrder.setText(String.format(LanguageBinding.getString(R.string.order_confrim_temp_value, activity), quantity));
                mBinding.tvMoneyTotal.setText(Helper.getVNCurrency(total) + "đ");
                mBinding.tvPriceTotal.setText(Helper.getVNCurrency(total) + "đ");
                mBinding.tvUsername.setText(UserModel.getInstance().getFullname() + " - ");
                mBinding.tvPhone.setText(UserModel.getInstance().getPhone());
            } else onBackPressed();
        }
    }

    private void getStoreDetail() {
        DeliDataLoader.getStoreDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    StoreOfCategoryModel mStore = (StoreOfCategoryModel) object;
                    mBinding.tvNameMerchant.setText(mStore.getName());
                    mBinding.tvTimeDistance.setText(mStore.getShipping_duration_min() + "-" + mStore.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
                    if (mStore.getLogo_link() != null)
                        ImageLoader.getInstance().displayImage(mStore.getLogo_link(), mBinding.ivMerchant, LixiApplication.getInstance().optionsNomal);
                    if (mStore.isTrusted_store())
                        mBinding.llTrueStore.setVisibility(View.VISIBLE);

                }
            }
        }, storeId, null, null, null);
    }

    private void createOrder(DeliAddressModel location) {
        JSONObject params = new JSONObject();
        try {
            params.put("store_id", storeId);
            String userName = mBinding.tvUsername.getText().toString().substring(0, mBinding.tvUsername.getText().toString().length() - 3);
            params.put("receive_name", userName);
            params.put("receive_phone", mBinding.tvPhone.getText().toString());
            params.put("note", "");

            ArrayList<ProductModel> temp = new ArrayList<>();
            Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
            for (String key : keySet) {
                temp.addAll(cart.get("" + storeId).getProduct_list().get(key));
            }

            JSONArray productArr = new JSONArray();
            for (int i = 0; i < temp.size(); i++) {
                JSONObject productParams = new JSONObject();
                productParams.put("product_id", temp.get(i).getId());
                productParams.put("quantity", temp.get(i).getQuantity());
                productParams.put("note", "");

                if (temp.get(i).getList_addon() != null) {
                    JSONArray arrAddon = new JSONArray();
                    for (int j = 0; j < temp.get(i).getList_addon().size(); j++) {
                        JSONObject addParams = new JSONObject();
                        addParams.put("addon_id", temp.get(i).getList_addon().get(j).getId());
                        if (temp.get(i).getList_addon().get(j).isSelected())
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).isSelected()));
                        else
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).getDefault_value()));
                        arrAddon.put(addParams);
                    }
                    productParams.put("list_addon", arrAddon);
                }
                productArr.put(productParams);
            }

            params.put("list_product", productArr);
            params.put("shipping_receive_address", location.getFullAddress());
            params.put("shipping_receive_lat", location.getLat());
            params.put("shipping_receive_lng", location.getLng());
            if (mReceiveTimeChanged != null)
                params.put("shipping_receive_time", Long.valueOf(mReceiveTimeChanged));
            else params.put("shipping_receive_time", JSONObject.NULL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.createOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    try {
                        cart.get("" + storeId).setUuid((String) object);
                        cart.get("" + storeId).setState("draft");
                        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                    } catch (Exception e) {
                        Crashlytics.logException(e);
                        finish();
                        return;
                    }
                    OrderInfoActivity.startOrderCheckoutActivity(activity, (String) object, storeId, source);
                    finish();
                } else {
                    mBinding.rlLoading.setVisibility(View.GONE);
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.ivBack) {
            finish();
        } else if (view == mBinding.rlGetAddress) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("LAST_LOCATION", false);
            Intent intent = new Intent(activity, NewSearchLocationActivity.class);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        } else if (view == mBinding.tvEditInfo) {
            activity.unregisterReceiver(mReceiver);
            isRegister = false;
            InfoOrderDialog dialog = new InfoOrderDialog(activity, false, false, true);
            dialog.show();
            String userName = mBinding.tvUsername.getText().toString().substring(0, mBinding.tvUsername.getText().toString().length() - 3);

            dialog.setDataWithoutLocation(storeId, userName, mBinding.tvPhone.getText().toString(), mReceiveTimeChanged);

            dialog.setIDialogEventListener(new InfoOrderDialog.IDialogEvent() {
                @Override
                public void onOk(DeliAddressModel deliAddressModel, String name, String phone, String time) {
                    mBinding.tvUsername.setText(name + " - ");
                    mBinding.tvPhone.setText(phone);

                    if (time == null) {
                        mBinding.tvTime.setText(LanguageBinding.getString(R.string.deli_tracking_earliest, activity));
                        mReceiveTimeChanged = null;
                        cart.get("" + storeId).setUpdated_ship_receive_time(null);
                    } else {
                        Calendar currentDate = Calendar.getInstance();
                        Calendar receiveDate = Calendar.getInstance();
                        receiveDate.setTimeInMillis(Long.parseLong(time));
                        int diff = Helper.daysBetween(currentDate, receiveDate);

                        if (diff == 0)
                            mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, time) + " - " + LanguageBinding.getString(R.string.today, activity));
                        else if (diff == 1)
                            mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, time) + " - " + LanguageBinding.getString(R.string.tomorrow, activity));
                        else if (diff > 1)
                            mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, time));

                        cart.get("" + storeId).setUpdated_ship_receive_time(Long.parseLong(time));
                        mReceiveTimeChanged = time;
                    }

                    Helper.saveObJectToFile(cart, activity, ConstantValue.CART_FILE);

                    if (deliAddressModel != null) {
                        mBinding.rlLoading.setVisibility(View.VISIBLE);
                        createOrder(deliAddressModel);
                    } else {
                        isRegister = true;
                        activity.registerReceiver(mReceiver, intentFilter);
                    }
                }
            });

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    isRegister = true;
                    activity.registerReceiver(mReceiver, intentFilter);
                }
            });

        } else if (view == mBinding.tvOrder) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.order_confirm_choose_location, activity), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        if (isRegister)
            activity.unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DeliAddressModel location = (DeliAddressModel) intent.getSerializableExtra("LOCATION");
            createOrder(location);
        }
    };
}
