package com.opencheck.client.home.account.evoucher.Enum;

public enum EvoucherStatus {
    USED, WAITING, EXPIRED, CAN_ACTIVE
}
