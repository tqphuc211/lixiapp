package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class CartModel implements Serializable {
    private long id;
    private String uuid;
    private String state;
    private Long create_day = System.currentTimeMillis();
    private Long ship_receive_time;
    private Long updated_ship_receive_time;
    private HashMap<String, ArrayList<ProductModel>> product_list;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public HashMap<String, ArrayList<ProductModel>> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(HashMap<String, ArrayList<ProductModel>> product_list) {
        this.product_list = product_list;
    }

    public Long getCreate_day() {
        return create_day;
    }

    public Long getShip_receive_time() {
        return ship_receive_time;
    }

    public void setShip_receive_time(Long ship_receive_time) {
        this.ship_receive_time = ship_receive_time;
    }

    public Long getUpdated_ship_receive_time() {
        return updated_ship_receive_time;
    }

    public void setUpdated_ship_receive_time(Long updated_ship_receive_time) {
        this.updated_ship_receive_time = updated_ship_receive_time;
    }
}
