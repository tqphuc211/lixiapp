package com.opencheck.client.home.newlogin.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.databinding.DialogDatePickerBinding;
import com.opencheck.client.home.newlogin.communicate.OnPickDateListener;

import java.util.Calendar;

public class DatePickerDialog extends LixiDialog {
    public DatePickerDialog(@NonNull LixiActivity _activity) {
        super(_activity, false, true, false);
    }

    private DialogDatePickerBinding mBinding;
    private boolean pick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogDatePickerBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        Calendar canlendar = Calendar.getInstance();

        mBinding.timePicker.init(canlendar.get(Calendar.YEAR), canlendar.get(Calendar.MONTH), canlendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                if (!pick){
                    pick = true;
                    return;
                }
                if (onPickDateListener != null) {
                    onPickDateListener.onPicked(dateFormat(datePicker));
                }
                cancel();
            }
        });
        mBinding.timePicker.getTouchables().get(0).performClick();
        mBinding.timePicker.getTouchables().get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pick = false;
            }
        });
    }

    private OnPickDateListener onPickDateListener;
    public void setOnPickListener(OnPickDateListener onPickDateListener){
        this.onPickDateListener = onPickDateListener;
    }

    private String dateFormat(DatePicker datePicker){
        int date = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        return formatNumb(date) + "-" + formatNumb(month + 1) + "-" + year;
    }

    private String formatNumb(int numb){
        String res = "";
        if (numb < 10){
            res = "0" + numb;
        }else {
            res = numb + "";
        }
        return res;
    }

    @Override
    public void onClick(View view) {

    }
}
