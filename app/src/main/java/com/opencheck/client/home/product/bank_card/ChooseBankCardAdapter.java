package com.opencheck.client.home.product.bank_card;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowChooseBankBinding;
import com.opencheck.client.home.product.FilterBankCardAdapter;
import com.opencheck.client.models.lixishop.BankModel;

import java.util.ArrayList;

public class ChooseBankCardAdapter extends FilterBankCardAdapter {
    // Constructor
    private LixiActivity activity;

    // Var
    private int checkedItem = -1;
    private int lastChecked = -1;
    private BankModel bankModel = null;

    public ChooseBankCardAdapter(LixiActivity activity, ArrayList<BankModel> listBankCard) {
        this.activity = activity;
        this.list = listBankCard;
        this.originalList = listBankCard;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowChooseBankBinding rowChooseBankBinding =
                RowChooseBankBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(rowChooseBankBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BankModel bankModel = list.get(position);
        if (bankModel != null) {
            ImageLoader.getInstance().displayImage(bankModel.getBank_logo(), holder.imgBankLogo);
            holder.txtBankName.setText(bankModel.getName());

            if (checkedItem == position) {
                holder.imgChecked.setVisibility(View.VISIBLE);
            } else {
                holder.imgChecked.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public BankModel getBankModel() {
        return bankModel;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBankLogo, imgChecked;
        TextView txtBankName;

        ConstraintLayout constraintParent;

        public ViewHolder(View itemView) {
            super(itemView);
            imgBankLogo = (ImageView) itemView.findViewById(R.id.imgBankLogo);
            imgChecked = (ImageView) itemView.findViewById(R.id.imgChecked);

            txtBankName = (TextView) itemView.findViewById(R.id.txtBankName);

            constraintParent = (ConstraintLayout) itemView.findViewById(R.id.constraintParent);

            constraintParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // check item
                    bankModel = list.get(getAdapterPosition());
                    if (checkedItem == getAdapterPosition()) {
                        return;
                    }
                    checkedItem = getAdapterPosition();
                    imgChecked.setVisibility(View.VISIBLE);
//                    bankModel = list.get(getAdapterPosition());
                    if (lastChecked >= 0) {
                        notifyItemChanged(lastChecked);
                    }
                    lastChecked = checkedItem;
                }
            });
        }
    }
}
