package com.opencheck.client.home.rating.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogLixiGiftBinding;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class LixiGiftDialog extends LixiDialog {
    public LixiGiftDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private TextView txtLixi;
    private ImageView imgClose;

    private DialogLixiGiftBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_lixi_gift, null, false);
        setContentView(mBinding.getRoot());
        txtLixi = (TextView) findViewById(R.id.txtLixi);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);
    }

    public void setData(int lixi) {
        String s = String.format(LanguageBinding.getString(R.string.cash_evoucher_offer_lixi, activity), Helper.getVNCurrency(lixi));
        txtLixi.setText(Html.fromHtml(s));
    }

    @Override
    public void onClick(View view) {
        cancel();
    }
}
