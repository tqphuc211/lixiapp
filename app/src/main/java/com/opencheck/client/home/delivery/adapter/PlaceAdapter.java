package com.opencheck.client.home.delivery.adapter;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PlaceItemLayoutBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.activities.CategoryDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<StoreOfCategoryModel> arrStore;
    private String source = "";

    public PlaceAdapter(LixiActivity activity, ArrayList<StoreOfCategoryModel> arrStore) {
        this.activity = activity;
        this.arrStore = arrStore;

        if (activity instanceof HomeActivity) {
            source = TrackingConstant.ContentSource.HOME;
        } else if (activity instanceof CategoryDetailActivity) {
            source = TrackingConstant.ContentSource.DELI_CATEGORY;
        } else if (activity instanceof StoreListOfArticleActivity) {
            source = TrackingConstant.ContentSource.DELI_COLLECTION;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PlaceItemLayoutBinding placeItemLayoutBinding =
                PlaceItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(placeItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StoreOfCategoryModel store = arrStore.get(position);
        LixiImage.with(activity)
                .load(store.getCover_link())
                .size(ImageSize.AUTO)
                .into(holder.iv_avatar);

        holder.tv_name.setText(store.getName());
        holder.tv_time.setText(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
        if (store.getShipping_distance() >= 100) {
            double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
            holder.tv_distance.setText(String.valueOf(distance) + " km");
        } else
            holder.tv_distance.setText(String.valueOf(store.getShipping_distance()) + " m");

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < store.getList_tag().size(); i++) {
            builder.append("#").append(store.getList_tag().get(i).getName());
            if (i < store.getList_tag().size() - 1)
                builder.append(".");
        }
        holder.tv_tags.setText(builder.toString());
        if(store.getList_tag().size() > 0) {
            holder.tv_tags.setVisibility(View.VISIBLE);
        } else {
            holder.tv_tags.setVisibility(View.GONE);
        }

        if (store.isIs_opening()) {
            holder.tv_open_time.setVisibility(View.GONE);
            holder.iv_store_close.setVisibility(View.GONE);
            holder.iv_avatar.setColorFilter(null);
            holder.tv_shipping_fee.setVisibility(View.VISIBLE);
        } else {
            holder.tv_open_time.setText(String.format(LanguageBinding.getString(R.string.open_close_time, activity), store.getOpen_time_text()));
            holder.tv_open_time.setVisibility(View.VISIBLE);
            holder.iv_store_close.setVisibility(View.VISIBLE);
            holder.iv_avatar.setColorFilter(Color.parseColor("#CC000000"));
            holder.tv_shipping_fee.setVisibility(View.GONE);
        }

        if (store.getMin_money_to_free_ship_price() == null)
            holder.tv_shipping_fee.setVisibility(View.GONE);
        else {
            holder.tv_shipping_fee.setVisibility(View.VISIBLE);
            holder.tv_shipping_fee.setText(LanguageBinding.getString(R.string.store_detail_free_ship, activity));
        }

        if (store.getPromotion_text() != null && store.getPromotion_text().length() > 0) {
            holder.ll_promotion.setVisibility(View.VISIBLE);
            holder.tv_promotion.setText(store.getPromotion_text());
        } else {
            holder.ll_promotion.setVisibility(View.GONE);
        }

//        holder.v_top.setVisibility(View.GONE);
//        holder.v_left.setVisibility(View.GONE);
//        holder.v_right.setVisibility(View.GONE);

        int width = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_1)) / 25 * 10;
        ViewGroup.LayoutParams params = holder.iv_avatar.getLayoutParams();
        params.width = width;
        params.height = 3 * width / 4;
        holder.iv_avatar.setLayoutParams(params);
        holder.tv_open_time.setMaxWidth(width);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreDetailActivity.startStoreDetailActivity(activity, store.getId(), source);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrStore == null ? 0 : arrStore.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_avatar, iv_store_close;
        private TextView tv_name, tv_time, tv_distance, tv_tags, tv_shipping_fee, tv_promotion, tv_open_time;
        private View rootView, v_top, v_left, v_right;
        private LinearLayout ll_promotion;
        private RelativeLayout root_view;
        private CardView card_view;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            iv_avatar = itemView.findViewById(R.id.iv_avatar);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_tags = itemView.findViewById(R.id.tv_tags);
            tv_shipping_fee = itemView.findViewById(R.id.tv_shipping_fee);
            tv_promotion = itemView.findViewById(R.id.tv_promotion);
            root_view = itemView.findViewById(R.id.root_view);
            v_right = itemView.findViewById(R.id.v_right);
            v_left = itemView.findViewById(R.id.v_left);
            v_top = itemView.findViewById(R.id.v_top);
            ll_promotion = itemView.findViewById(R.id.ll_promotion);
            tv_open_time = itemView.findViewById(R.id.tv_open_time);
            iv_store_close = itemView.findViewById(R.id.iv_store_close);
            card_view = itemView.findViewById(R.id.card_view);
        }
    }
}
