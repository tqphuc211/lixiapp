package com.opencheck.client.home.delivery.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.FragmentDeliveryHomeBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.activities.NewSearchLocationActivity;
import com.opencheck.client.home.delivery.activities.SearchStoreActivity;
import com.opencheck.client.home.delivery.adapter.DeliveryHomeAdapter;
import com.opencheck.client.home.delivery.adapter.TrackingOrderAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliBannerModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.refactor.LocationSelectionFragment;
import com.opencheck.client.home.game.GameActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryHomeFragment extends LixiFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static boolean isRefreshLocation = false;
    //Initialize views
    private RecyclerView rvItem, rv_tracking;
    private LinearLayout ll_sticky_header, llTitle, ll;
    private LinearLayout llHotStore, llFastestStore, llBestPriceStore;
    private RelativeLayout rootView;
    private LinearLayout rl_address;
    private TextView tvHotStore, tvFastestStore, tvBestPriceStore;
    private View vHotStore, vFastestStore, vBestPriceStore;
    private SwipeRefreshLayout swr;
    private TextView tv_address, tv_active_address;
    private RelativeLayout rl_active_location, rlData;

    private DeliveryHomeAdapter mAdapter;
    private SpacesItemDecoration spacesItemDecoration;
    private GridLayoutManager mLayoutManager;

    //Initialize arrays, variables
    private ArrayList<StoreOfCategoryModel> listItem = new ArrayList<>();
    private ArrayList<DeliOrderModel> listTracking = new ArrayList<>();
    private TrackingOrderAdapter orderAdapter;

    private int page = 1, page_tracking = 1;
    private int pageSize = DeliDataLoader.record_per_page;
    private boolean isLoading = false, isTrackingLoading = false;
    private boolean noRemain = false;
    private String tab = "";
    private int preTab = 0;
    private DeliAddressModel mLocation, lastLocation = new DeliAddressModel();

    //Hide/show location view
    private final static float HIDE_THRESHOLD = 10;
    private final static float SHOW_THRESHOLD = 70;
    private int mLocationViewOffset = 0;
    private boolean mControlsVisible = true;
    private int mLocationViewHeight;
    private int mTotalScrollDistance;
    private String screenName = "delivery-home";
    private boolean isResume = false;
    private String province = "NW";

    private FragmentDeliveryHomeBinding mBinding;

    //region LIFECYCLE
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_delivery_home, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        lastLocation = mLocation;
        initViews(view);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        }, 500);
    }

    @Override
    public void onClick(View v) {
        if (v == llHotStore) {
            selectTab(0);
        } else if (v == llFastestStore) {
            selectTab(1);
        } else if (v == llBestPriceStore) {
            selectTab(2);
        } else if (v == mBinding.searchBar) {
            startActivity(new Intent(activity, SearchStoreActivity.class));
        } else if (v == rl_address) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("LAST_LOCATION", true);
            try {
                AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(activity, NewSearchLocationActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v == mBinding.imgGame) {
            if ((new AppPreferenceHelper(activity)).getAccessToken().equals("")) {
                LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_PLAY_GAME);
            } else {
                Intent intentGame = new Intent(activity, GameActivity.class);
                intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.HOME);
                startActivity(intentGame);
            }
        }
    }

    @Override
    public void onRefresh() {
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (mLocation != null && mLocation.getFullAddress() != null) {
            if (!lastLocation.getFullAddress().equalsIgnoreCase(mLocation.getFullAddress())) {
                tv_address.setText(mLocation.getFullAddress());
                lastLocation = mLocation;
                isRefreshLocation = false;
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadData();
                }
            }, 500);
        }
    }

    @Override
    public void onResume() {
        if (isRefreshLocation) {
            onRefresh();
        }

        if (LoginHelper.isLoggedIn(activity)) {
            page_tracking = 1;
            isTrackingLoading = false;
            getTrackingOrder();
        } else {
            setButtonGameConstraint(false);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        isRefreshLocation = false;
        super.onPause();
    }
    //endregion

    //region UI
    private void initViews(View v) {
        rvItem = v.findViewById(R.id.rvItem);
        rv_tracking = v.findViewById(R.id.rv_tracking);
        rl_address = v.findViewById(R.id.rl_address);
        swr = v.findViewById(R.id.swr);
        tv_address = v.findViewById(R.id.tv_address);
        ll_sticky_header = v.findViewById(R.id.ll_sticky_header);
        llTitle = v.findViewById(R.id.ll_title);
        llHotStore = v.findViewById(R.id.llHotStore);
        llFastestStore = v.findViewById(R.id.llFastestStore);
        llBestPriceStore = v.findViewById(R.id.llBestPriceStore);
        tvHotStore = v.findViewById(R.id.tvHotStore);
        tvFastestStore = v.findViewById(R.id.tvFastestStore);
        tvBestPriceStore = v.findViewById(R.id.tvBestPriceStore);
        vHotStore = v.findViewById(R.id.vHotStore);
        vFastestStore = v.findViewById(R.id.vFastestStore);
        vBestPriceStore = v.findViewById(R.id.vBestPriceStore);
        tv_active_address = v.findViewById(R.id.tv_active_address);
        rl_active_location = v.findViewById(R.id.rl_active_location);
        rootView = v.findViewById(R.id.rootView);
        ll = v.findViewById(R.id.ll);
        rlData = v.findViewById(R.id.rlData);

        llHotStore.setOnClickListener(this);
        llFastestStore.setOnClickListener(this);
        llBestPriceStore.setOnClickListener(this);
        mBinding.searchBar.setOnClickListener(this);
        rl_address.setOnClickListener(this);
        mBinding.imgGame.setOnClickListener(this);

        if (ConfigModel.getInstance(activity).getGame_home_button_enable() == 0) {
            mBinding.imgGame.setVisibility(View.GONE);
        } else {
            mBinding.imgGame.setVisibility(View.VISIBLE);
        }

        LixiImage.with(activity)
                .load(ConfigModel.getInstance(activity).getGame_home_button_icon())
                .into(mBinding.imgGame);

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv_tracking);

        swr.setOnRefreshListener(this);
        mAdapter = new DeliveryHomeAdapter(activity, null);
        mAdapter.setOnTabClickListener(new DeliveryHomeAdapter.OnTabClickListener() {
            @Override
            public void onHotTabClick() {
                selectTab(0);
            }

            @Override
            public void onFastestTabClick() {
                selectTab(1);
            }

            @Override
            public void onBestPriceTabClick() {
                selectTab(2);
            }
        });
        mLayoutManager = new GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mAdapter.getItemViewType(position)) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return 2;
//                    case 6:
//                        return 1;
                    default:
                        return -1;
                }

            }
        });

        final int paddingTop = (int) activity.getResources().getDimension(R.dimen.value_81);
        mLocationViewHeight = (int) activity.getResources().getDimension(R.dimen.value_30);
        rvItem.setPadding(rvItem.getPaddingLeft(), paddingTop, rvItem.getPaddingRight(), rvItem.getPaddingBottom());
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.setAdapter(mAdapter);
        rvItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    if (mTotalScrollDistance < mLocationViewHeight) {
//                        setVisible();
//                    } else {
//                        if (mControlsVisible) {
//                            if (mLocationViewOffset > HIDE_THRESHOLD) {
//                                setInvisible();
//                            } else {
//                                setVisible();
//                            }
//                        } else {
//                            if ((mLocationViewHeight - mLocationViewOffset) > SHOW_THRESHOLD) {
//                                setVisible();
//                            } else {
//                                setInvisible();
//                            }
//                        }
//                    }
//                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d("dy", dy + "");
                int pos = ((LinearLayoutManager) rvItem.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
//                if (pos > 3 && dy > 0)
//                    rvItem.setPadding(rvItem.getPaddingLeft(), paddingTop - mLocationViewHeight, rvItem.getPaddingRight(), rvItem.getPaddingBottom());
//                else {
//                    rvItem.setPadding(rvItem.getPaddingLeft(), paddingTop, rvItem.getPaddingRight(), rvItem.getPaddingBottom());
//                }
                if (pos > 5) {
                    ll_sticky_header.setVisibility(View.VISIBLE);
                } else {
                    ll_sticky_header.setVisibility(View.GONE);
                }

//                clipLocationViewOffset();
//                llTitle.setTranslationY(-mLocationViewOffset);

//                if ((mLocationViewOffset < mLocationViewHeight && dy > 0) || (mLocationViewOffset > 0 && dy < 0)) {
//                    mLocationViewOffset += dy;
//                }
//                if (mTotalScrollDistance < 0) {
//                    mTotalScrollDistance = 0;
//                } else {
//                    mTotalScrollDistance += dy;
//                }

                if (!isLoading && !noRemain && isRvBottom()) {
                    Log.d("mapi", "load more");
                    if (mAdapter == null)
                        return;
                    switch (mAdapter.getCurrentTab()) {
                        case 0:
                            getHotStoreList();
                            break;
                        case 1:
                            getFastestStoreList();
                            break;
                        case 2:
                            getBestPriceStoreList();
                            break;
                    }
                    isLoading = true;
                }
            }
        });

        if (mLocation != null && mLocation.getFullAddress().length() > 0) {
            tv_address.setText(mLocation.getFullAddress());
            tv_active_address.setText(mLocation.getFullAddress());
            //moveViewAnimation();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tv_address.setTransitionName(tv_address.getTransitionName());
            }
        }

        setTrackingAdapter();
    }

    private void setTrackingAdapter() {
        orderAdapter = new TrackingOrderAdapter(activity, listTracking);
        rv_tracking.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        rv_tracking.setAdapter(orderAdapter);
    }

    private void resetList() {
        if (mAdapter == null || mAdapter.getListItem() == null) return;
        mAdapter.getListItem().clear();
        mAdapter.notifyDataSetChanged();
    }

    private boolean isRvBottom() {
        if (mAdapter.getListItem() == null || mAdapter.getListItem().size() == 0)
            return false;
        int totalItemCount = rvItem.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) rvItem.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && mAdapter.getListItem().size() >= pageSize);
    }

    private void selectTab(int tab) {
        if (mAdapter != null)
            mAdapter.setCurrentTab(tab);
        tvHotStore.setTextColor(activity.getResources().getColor(R.color.color_gray_tab));
        tvFastestStore.setTextColor(activity.getResources().getColor(R.color.color_gray_tab));
        tvBestPriceStore.setTextColor(activity.getResources().getColor(R.color.color_gray_tab));

        vHotStore.setVisibility(View.GONE);
        vFastestStore.setVisibility(View.GONE);
        vBestPriceStore.setVisibility(View.GONE);

        switch (tab) {
            case 0:
                if (preTab != 0) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }

                mAdapter.setCurrentTab(0);
                getHotStoreList();
                tvHotStore.setTextColor(activity.getResources().getColor(R.color.app_violet));
                vHotStore.setVisibility(View.VISIBLE);
                preTab = 0;
                break;
            case 1:
                if (preTab != 1) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }
                mAdapter.setCurrentTab(1);
                getFastestStoreList();
                tvFastestStore.setTextColor(activity.getResources().getColor(R.color.app_violet));
                vFastestStore.setVisibility(View.VISIBLE);
                preTab = 1;
                break;
            case 2:
                if (preTab != 2) {
                    page = 1;
                    isLoading = false;
                    noRemain = false;
                }
                mAdapter.setCurrentTab(2);
                getBestPriceStoreList();
                tvBestPriceStore.setTextColor(activity.getResources().getColor(R.color.app_violet));
                vBestPriceStore.setVisibility(View.VISIBLE);
                preTab = 2;
                break;
        }
    }

    public void moveViewAnimation() {
        tv_active_address.post(new Runnable() {
            @Override
            public void run() {
                try {
                    int[] fromLoc = new int[2];
                    tv_active_address.getLocationOnScreen(fromLoc);
                    float startX = fromLoc[0];
                    float startY = fromLoc[1];

                    int[] toLoc = new int[2];
                    tv_address.getLocationOnScreen(toLoc);
                    float endX = toLoc[0];
                    float endY = Math.max(0, toLoc[1] - 86);

                    final TextView textView = new TextView(activity);
                    textView.setText(mLocation.getFullAddress());
                    textView.setTextSize(14);
                    textView.setMaxLines(1);
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    textView.setTypeface(null, Typeface.BOLD);
                    textView.setTextColor(activity.getResources().getColor(R.color.black));

                    final Path path = new Path();
                    path.moveTo(startX, 580);
                    path.lineTo(endX, endY);

                    final ValueAnimator pathAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
                    pathAnimator.setStartDelay(1000);
                    pathAnimator.setDuration(380);
                    pathAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                        float[] point = new float[2];

                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            try {
                                float val = animation.getAnimatedFraction();
                                PathMeasure pathMeasure = new PathMeasure(path, true);
                                pathMeasure.getPosTan(pathMeasure.getLength() / 2 * val, point, null);

                                textView.setTranslationX(point[0]);
                                textView.setTranslationY(point[1]);
                            } catch (Exception ex) {
                                rl_active_location.setVisibility(View.GONE);
                            }
                        }
                    });

                    pathAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationCancel(Animator animation) {
                            super.onAnimationCancel(animation);
                        }

                        @Override
                        public void onAnimationStart(Animator animation) {
                            try {
                                rootView.addView(textView);
                                super.onAnimationStart(animation);
                                rl_active_location.setVisibility(View.VISIBLE);
                                ll.setVisibility(View.GONE);
                                tv_address.setTextColor(activity.getResources().getColor(R.color.white));
                            } catch (Exception ex) {
                                rl_active_location.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            try {
                                rl_active_location.setVisibility(View.GONE);
                                rootView.removeView(textView);
                                tv_address.setTextColor(activity.getResources().getColor(R.color.black));

                            } catch (Exception ex) {
                                rl_active_location.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onAnimationPause(Animator animation) {
                            super.onAnimationPause(animation);
                            Log.d("mapi", "pause animation");
                        }

                        @Override
                        public void onAnimationResume(Animator animation) {
                            super.onAnimationResume(animation);
                            Log.d("mapi", "resume animation");
                        }
                    });

                    Animation a;
                    a = new AlphaAnimation(1, 0);
                    a.setDuration(400);
                    ll.setVisibility(View.VISIBLE);
                    rl_active_location.setVisibility(View.VISIBLE);
                    pathAnimator.start();
                    textView.startAnimation(a);
                } catch (Exception ex) {
                    rl_active_location.setVisibility(View.GONE);
                }
            }
        });
    }
    //endregion

    //region Show/hide address
    private void clipLocationViewOffset() {
        if (mLocationViewOffset > mLocationViewHeight) {
            mLocationViewOffset = mLocationViewHeight;
        } else if (mLocationViewOffset < 0) {
            mLocationViewOffset = 0;
        }
    }

    private void setVisible() {
        if (mLocationViewOffset > 0) {
            llTitle.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            mLocationViewOffset = 0;
        }
        mControlsVisible = true;
    }

    private void setInvisible() {
        if (mLocationViewOffset < mLocationViewHeight) {
            llTitle.animate().translationY(-mLocationViewHeight).setInterpolator(new AccelerateInterpolator(2)).start();
            mLocationViewOffset = mLocationViewHeight;
        }
        mControlsVisible = false;
    }
    //endregion

    //region API
    private void getBanner() {
        DeliDataLoader.getBanner(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mAdapter.setListBanner((ArrayList<DeliBannerModel>) object);
                }
            }
        }, province);
    }

    private void getCategory() {
        DeliDataLoader.getStoreTagList(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mAdapter.setListCategory((ArrayList<ArticleModel>) object);
                }
            }
        }, 1, "category", true, province);
    }

    private void getArticle() {
        DeliDataLoader.getArticle(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mAdapter.setListArticle((ArrayList<ArticleModel>) object);
                }
            }
        }, 1, province);
    }

    private void loadData() {
        isLoading = false;
        noRemain = false;
        isTrackingLoading = false;
        page = 1;
        page_tracking = 1;

        if (mLocation != null) {
            String temp = mLocation.getFullAddress();
            if (Helper.covertStringToURL(temp).contains("ho chi minh"))
                province = "HCM";
            else if (Helper.covertStringToURL(temp).contains("ha noi"))
                province = "HaNoi";
        }

        getBanner();
        getCategory();
        getArticle();
        getRecommendStoreList();
        getProductMerchantHot();

        if (mAdapter != null)
            mAdapter.setCurrentTab(0);
        selectTab(0);
    }

    private void getBestPriceStoreList() {
        swr.setRefreshing(true);
        DeliDataLoader.getBestPrice(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swr.setRefreshing(false);
                if (isSuccess) {
                    listItem = (ArrayList<StoreOfCategoryModel>) object;
                    if (page == 1) {
                        resetList();
                        if (listItem == null || listItem.size() == 0)
                            return;
                        if (listItem.size() < pageSize)
                            noRemain = true;
                    } else {
                        if (listItem == null || listItem.size() == 0 || listItem.size() < pageSize) {
                            noRemain = true;
                            return;
                        }
                    }
                    mAdapter.setListMainStore(listItem);
                    if (page == 1 && ll_sticky_header.getVisibility() == View.VISIBLE)
                        rvItem.scrollToPosition(6);
                    page++;
                    isLoading = false;
                } else {
                    isLoading = true;
                    //Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());

    }

    private void getFastestStoreList() {
        swr.setRefreshing(true);
        DeliDataLoader.getStoreFastest(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swr.setRefreshing(false);
                if (isSuccess) {
                    listItem = (ArrayList<StoreOfCategoryModel>) object;
                    if (page == 1) {
                        resetList();
                        if (listItem == null || listItem.size() == 0)
                            return;
                        if (listItem.size() < pageSize)
                            noRemain = true;
                    } else {
                        if (listItem == null || listItem.size() == 0 || listItem.size() < pageSize) {
                            noRemain = true;
                            return;
                        }
                    }
                    mAdapter.setListMainStore(listItem);
                    isLoading = false;
                    if (page == 1 && ll_sticky_header.getVisibility() == View.VISIBLE)
                        rvItem.scrollToPosition(6);
                    page++;
                } else {
                    isLoading = true;
                    //Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());

    }

    private void getHotStoreList() {
        if (!isLoading) {
            swr.setRefreshing(true);
            DeliDataLoader.getListHot(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    swr.setRefreshing(false);
                    if (isSuccess) {
                        ArrayList<StoreOfCategoryModel> listItem = (ArrayList<StoreOfCategoryModel>) object;
                        if (page == 1) {
                            resetList();
                            if (listItem == null || listItem.size() == 0) {
                                LocationSelectionFragment.deliHomeShowing = false;
                                getChildFragmentManager().beginTransaction().detach(DeliveryHomeFragment.this).commitAllowingStateLoss();
                                return;
                            }
                            if (listItem.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (listItem == null || listItem.size() == 0 || listItem.size() < pageSize) {
                                noRemain = true;
                                return;
                            }
                        }
                        mAdapter.setListMainStore(listItem);
                        isLoading = false;
                        if (page == 1 && ll_sticky_header.getVisibility() == View.VISIBLE)
                            rvItem.scrollToPosition(6);
                        page++;
                        LocationSelectionFragment.deliHomeShowing = true;
                    } else {
                        isLoading = true;
                        LocationSelectionFragment.deliHomeShowing = false;
                        getChildFragmentManager().beginTransaction().detach(DeliveryHomeFragment.this).commitAllowingStateLoss();
                    }
                }
            }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        }
    }

    private void getRecommendStoreList() {
        DeliDataLoader.getStoreRecommend(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mAdapter.setListRecommend((ArrayList<StoreOfCategoryModel>) object);
                }
            }
        }, 1, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
    }

    private void getProductMerchantHot() {
        DeliDataLoader.getProductMerchantHot(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mAdapter.setListProductMerchantHot((ArrayList<ProductMerchantHot>) object);
                }
            }
        }, 1, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
    }

    private void getTrackingOrder() {
        if (!isTrackingLoading) {
            isTrackingLoading = true;
            DeliDataLoader.getUserOrderHistory(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ArrayList<DeliOrderModel> list = (ArrayList<DeliOrderModel>) object;
                        if (page_tracking == 1) {
                            orderAdapter.clearOrderData();
                        }

                        if (list == null || list.size() == 0)
                            return;
                        orderAdapter.setOrderData(list);
                        page_tracking++;
                        isTrackingLoading = false;
                    }

                    setButtonGameConstraint(true);
                }
            }, page_tracking, 20, "submitted, confirmed, assigned, picked", null, null);
        }
    }
    //endregion

    private void setButtonGameConstraint(boolean hasTracking) {
        if (ConfigModel.getInstance(activity).getGame_home_button_enable() == 1) {
            mBinding.imgGame.setVisibility(View.VISIBLE);
            if (!hasTracking) {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(mBinding.constraintParent);
                constraintSet.connect(R.id.imgGame, ConstraintSet.BOTTOM, R.id.constraintParent, ConstraintSet.BOTTOM, 20);
                constraintSet.applyTo(mBinding.constraintParent);
            } else {
                rv_tracking.post(new Runnable() {
                    @Override
                    public void run() {
                        if (rv_tracking.getHeight() != 0) {
                            ConstraintSet constraintSet = new ConstraintSet();
                            constraintSet.clone(mBinding.constraintParent);
                            constraintSet.connect(R.id.imgGame, ConstraintSet.BOTTOM, R.id.constraintParent, ConstraintSet.BOTTOM, rv_tracking.getHeight() + 20);
                            constraintSet.applyTo(mBinding.constraintParent);
                        } else {
                            setButtonGameConstraint(true);
                        }
                    }
                });
            }
        } else {
            mBinding.imgGame.setVisibility(View.GONE);
        }
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_HOME;
    }
}
