package com.opencheck.client.home.rating.view;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewDragQrBinding;
import com.opencheck.client.home.account.evoucher.Enum.DragAction;
import com.opencheck.client.home.history.detail.GeneralBarCodeAndCropTask;
import com.opencheck.client.home.history.detail.GeneralQRCodeAndCropTask;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

public class DragQRView extends ConstraintLayout implements View.OnClickListener, Animator.AnimatorListener {
    // Constant
    public static final String QRCODE = "qr_code";
    public static final String BAR_CODE = "bar_code";
    public static final String DEFAULT = "default";

    private Context context;

    public DragQRView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public DragQRView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public DragQRView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    // Var
    boolean canSwipe = true;
    private float x, cx;
    private float changeX;
    private float privotXLeft, privotXRight, privotXCenter;
    boolean swipeNext = false, swipePrev = false;
    private int COLOR_BLACK, COLOR_GRAY;
    private DragAction action = DragAction.CENTER;

    private ViewDragQrBinding mBinding;

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mBinding = ViewDragQrBinding.inflate(LayoutInflater.from(getContext()),
                this, true);
        mBinding.imgNext.setOnClickListener(this);
        mBinding.imgPrev.setOnClickListener(this);

        COLOR_BLACK = context.getResources().getColor(R.color.black);
        COLOR_GRAY = Color.parseColor("#9B9B9B");

        mBinding.linearDrag.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (canSwipe) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            cx = getWidth() / 2 - mBinding.linearDrag.getWidth() / 2;
                            privotXLeft = getWidth() / 4 - mBinding.linearDrag.getWidth() / 2;
                            privotXRight = (getWidth() / 4) * 3 - mBinding.linearDrag.getWidth() / 2;
                            privotXCenter = mBinding.linearDrag.getX();
                            x = motionEvent.getX();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            changeX = motionEvent.getX();
                            mBinding.linearDrag.setX(mBinding.linearDrag.getX() + changeX - x);
                            break;
                        default:
                            // next
                            if (mBinding.linearDrag.getX() <= privotXLeft) {
                                if (swipeNext) {
                                    setViewTranslate(-1 * getWidth(), DragAction.NEXT);
                                    return true;
                                }
                            }

                            // prev
                            if (mBinding.linearDrag.getX() >= privotXRight) {
                                if (swipePrev) {
                                    setViewTranslate(getWidth(), DragAction.PREV);
                                    return true;
                                }
                            }

                            // back
                            setViewTranslate(cx, DragAction.CENTER);
                            break;
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    private void setViewTranslate(float x, DragAction action){
        this.action = action;
        mBinding.linearDrag.animate()
                .translationX(x)
                .setDuration(200)
                .setListener(this);
    }

    @Override
    public void onClick(View view) {
        if (canSwipe) {
            switch (view.getId()) {
                case R.id.imgNext:
                    if (canSwipe) {
                        if (onActionChangedListener != null) {
                            onActionChangedListener.onChanged(DragAction.NEXT);
                        }
                    }
                    break;
                case R.id.imgPrev:
                    if (canSwipe) {
                        if (onActionChangedListener != null) {
                            onActionChangedListener.onChanged(DragAction.PREV);
                        }
                    }
                    break;
            }
        }
    }

    public void setCanSwipe(boolean canSwipe) {
        this.canSwipe = canSwipe;
    }

    public void setStateVoucher(String state) {
        mBinding.stateVoucher.setState(state);
    }

    public void setData(EvoucherDetailModel evoucherModel) {
        mBinding.linearDrag.setX(0);
        mBinding.txtTitle.setText(evoucherModel.getName());
        mBinding.txtPrice.setText(Helper.getVNCurrency(evoucherModel.getPrice()) + context.getString(R.string.p_under));
        if (evoucherModel.isSend_pin()) {
            mBinding.txtCode.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            mBinding.txtCode.setTextColor(COLOR_BLACK);
            mBinding.txtCode.setText(evoucherModel.getPin());
        } else {
            mBinding.txtCode.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
            mBinding.txtCode.setTextColor(COLOR_GRAY);
            mBinding.txtCode.setText("("  + LanguageBinding.getString(R.string.scan_code, context) + ")");
        }
        mBinding.txtExpired.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_expired, context),
                Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y, String.valueOf(evoucherModel.getExpired_time() * 1000))));
        if (evoucherModel.isIs_show_serial()) {
            mBinding.txtSerial.setText(evoucherModel.getSerial());
        } else {
            mBinding.txtSerial.setText("");
        }
    }


    public void setImageQR(LixiActivity activity, String pin, String transform) {
        if (transform.equals(BAR_CODE)){
            new GeneralBarCodeAndCropTask(activity, pin, mBinding.imgQR, new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    if (pos == 1) {
                        mBinding.imgQR.setImageBitmap((Bitmap) object);
                    }
                }
            });
        }else {
            new GeneralQRCodeAndCropTask(activity, pin, mBinding.imgQR, new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    if (pos == 1) {
                        mBinding.imgQR.setImageBitmap((Bitmap) object);
                    }
                }
            });
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (onActionChangedListener != null){
            onActionChangedListener.onChanged(action);
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public interface OnActionChangedListener {
        void onChanged(DragAction action);
    }

    public void resetView() {
        if (cx != 0) {
            mBinding.linearDrag.setX(cx);
        }
    }

    public void setVisibleArrow(long total, long pos) {
        if (total <= 1) {
            setPositionStatus(false, false);
        } else {
            if (pos == total - 1) {
                setPositionStatus(false, true);
            }

            if (pos == 0) {
                setPositionStatus(true, false);
            }

            if (pos > 0 && pos < total - 1) {
                setPositionStatus(true, true);
            }
        }

        if (pos < 0) {
            setPositionStatus(false, false);
        }
    }

    private void setPositionStatus(boolean hasNext, boolean hasPrev){
        mBinding.imgNext.setVisibility(hasNext ? VISIBLE : INVISIBLE);
        mBinding.imgNext.setEnabled(hasNext);
        swipeNext = hasNext;

        mBinding.imgPrev.setVisibility(hasPrev ? VISIBLE : INVISIBLE);
        mBinding.imgPrev.setEnabled(hasPrev);
        swipePrev = hasPrev;
    }

    private OnActionChangedListener onActionChangedListener;

    public void addOnActionChangedListener(OnActionChangedListener onActionChangedListener) {
        this.onActionChangedListener = onActionChangedListener;
    }
}
