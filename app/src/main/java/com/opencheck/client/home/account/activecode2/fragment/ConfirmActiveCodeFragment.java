package com.opencheck.client.home.account.activecode2.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.ConfirmActiveCodeFragment2Binding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.activecode2.controller.VoucherController;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmActiveCodeFragment extends LixiFragment {

    private Button btnClose;
    private TextView tv_name, tv_address, tv_agree, tv_note_message, tv_support_message, tv_total_price, tv_total_voucher;
    private LinearLayout ll_container;
    private CircleImageView iv_merchant;
    private NearestStoreModel storeModel;
    private ArrayList<LixiShopEvoucherBoughtModel> listVoucher;

    public ConfirmActiveCodeFragment() {
        // Required empty public constructor
    }

    private ConfirmActiveCodeFragment2Binding mBinding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = ConfirmActiveCodeFragment2Binding.inflate(inflater, container, false);

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LixiShopEvoucherBoughtModel>>() {
        }.getType();
        String JsonListVoucher = getArguments().getString("arr_ev");
        ArrayList<LixiShopEvoucherBoughtModel> voucher_list = gson.fromJson(JsonListVoucher, type);
        listVoucher = voucher_list;
        storeModel = new Gson().fromJson(getArguments().getString("store"), NearestStoreModel.class);
        initView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initView(View v) {
        btnClose = v.findViewById(R.id.btnClose);
        tv_address = v.findViewById(R.id.tv_address);
        tv_agree = v.findViewById(R.id.tv_agree);
        tv_name = v.findViewById(R.id.tv_name);
        iv_merchant = v.findViewById(R.id.iv_merchant);
        tv_note_message = v.findViewById(R.id.tv_note_message);
        tv_support_message = v.findViewById(R.id.tv_support_message);
        tv_total_price = v.findViewById(R.id.tv_total_price);
        tv_total_voucher = v.findViewById(R.id.tv_total_voucher);
        ll_container = v.findViewById(R.id.ll_container);

        btnClose.setOnClickListener(this);
        tv_agree.setOnClickListener(this);

        initData();
    }

    private void initData() {
        ImageLoader.getInstance().displayImage(storeModel.getMerchant_info().getLogo(),
                iv_merchant, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(storeModel.getName());
        tv_address.setText(storeModel.getAddress());
        tv_note_message.setText(Html.fromHtml(activity.getResources().getString(R.string.note_message)));
        tv_support_message.setText(Html.fromHtml(activity.getResources().getString(R.string.support_message)));
        tv_total_voucher.setText(LanguageBinding.getString(R.string.list_voucher_use, activity) + "(" + listVoucher.size() + ")");
        long price = 0L;
        for (int i = 0; i < listVoucher.size(); i++) {
            new VoucherController(activity, listVoucher.get(i), i, listVoucher.size(), false, ll_container);
            price += listVoucher.get(i).getPrice();
        }
        tv_total_price.setText(Helper.getVNCurrency(price) + "đ");
    }

    private String getListIdVoucher() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < listVoucher.size(); i++) {
            String temp = listVoucher.get(i).getId() + "";
            if (i != listVoucher.size() - 1)
                temp += ",";
            res.append(temp);
        }

        return res.toString();
    }

    private void submitActiveCode() {
        if (storeModel.getStore_code() != null) {
            final JsonObject obj = new JsonObject();
            obj.addProperty("store_code", storeModel.getStore_code());
            obj.addProperty("list_voucher_id", getListIdVoucher());
            obj.addProperty("page", 1);
            obj.addProperty("record_per_page", 10);
            DataLoader.submitActiceCodeMultiple(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        activity.onBackPressed();
                        Bundle data = new Bundle();
                        data.putString("store", new Gson().toJson(storeModel));
                        data.putString("arr_ev", new Gson().toJson(listVoucher));
                        new AppPreferenceHelper(activity).setInstructionVisible(false);
                        activity.onBackPressed();
                        HomeActivity.getInstance().tab = 5;
                        LoginHelper.startHomeActivity(activity);
                        new AppPreferenceHelper(activity).setOnRefresh(true);
                        activity.finish();
//                        ConfirmSuccessFragment fragment = new ConfirmSuccessFragment();
//                        fragment.setArguments(data);
//                        ((ActiveCode2Activity) activity).replaceFragment(fragment);

                    } else {
                        Helper.showErrorDialog(activity, object.toString());
                    }
                }
            }, obj);
        } else
            Toast.makeText(activity, "Không thể yêu cầu kích hoạt", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                activity.onBackPressed();
                break;
            case R.id.tv_agree:
                submitActiveCode();
                break;
        }
    }

}
