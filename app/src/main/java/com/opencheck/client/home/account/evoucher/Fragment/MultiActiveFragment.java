package com.opencheck.client.home.account.evoucher.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentMultiActiveBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.evoucher.Adapter.MultiActiveAdapter;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.account.evoucher.Model.EVoucherUnused;
import com.opencheck.client.home.account.evoucher.Model.GroupCode;
import com.opencheck.client.home.account.evoucher.Model.GroupCodeApiBody;
import com.opencheck.client.home.account.new_layout.activity.BoughtEvoucherActivity;
import com.opencheck.client.home.account.new_layout.control.OperateListCode;
import com.opencheck.client.home.account.new_layout.control.OperateObject;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class MultiActiveFragment extends LixiFragment {

    // View
    private View view;
    private RecyclerView recLayout;
    private ImageView imgBack;
    private Button btnUncheck;

    // Var
    private int page;
    private long record_per_page;
    boolean NEW_ACTIVE = false;
    private ArrayList<EVoucherUnused> listEvoucher;
    private EvoucherDetailModel evoucherModel;
    private GroupCodeApiBody groupCodeApiBody;

    MultiActiveAdapter multiActiveAdapter;

    private void initView() {
        recLayout = (RecyclerView) view.findViewById(R.id.recyclerlayout);
        imgBack = (ImageView) view.findViewById(R.id.iv_back);
        btnUncheck = (Button) view.findViewById(R.id.btn_active);

        imgBack.setOnClickListener(this);
        btnUncheck.setOnClickListener(this);
    }

    private FragmentMultiActiveBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentMultiActiveBinding.inflate(inflater, container, false);
        view = mBinding.getRoot();
        initView();
        page = 1;
        Bundle bundle = getArguments();
        if (bundle != null) {
            record_per_page = bundle.getLong("total", 0);
            evoucherModel = new Gson().fromJson(bundle.getString("ev"), EvoucherDetailModel.class);
            NEW_ACTIVE = bundle.getBoolean("layout active", false);
        }

        if (evoucherModel != null) {
            loadListVoucher(evoucherModel.getMerchant_info().getId());
        } else {
            ArrayList<LixiShopEvoucherBoughtModel> listTemp = OperateListCode.getInstance().getListEvoucher();
            listEvoucher = new ArrayList<>();
            for (int i = 0; i < listTemp.size(); i++) {
                EVoucherUnused e = OperateObject.getInstance().parseData(listTemp.get(i), EVoucherUnused.class);
                e.setName(listTemp.get(i).getProduct_name());
                listEvoucher.add(e);
            }

            groupCodeApiBody = new GroupCodeApiBody();
            groupCodeApiBody.setEvoucher_ids(OperateListCode.getInstance().getEvoucherIds());
            groupCodeApiBody.setMerchant_id(OperateListCode.getInstance().getMAU().getId());
            evoucherModel = new EvoucherDetailModel();
            evoucherModel.setMerchant_info(OperateObject.getInstance().parseData(OperateListCode.getInstance().getMAU(), MerchantInfoModel.class));
            loadGroupCode(groupCodeApiBody);
        }

        if (NEW_ACTIVE) {
            btnUncheck.setText(LanguageBinding.getString(R.string.done, activity));
        } else {
            btnUncheck.setText("Bỏ chọn tất cả");
        }

        return view;
    }

    private void loadListVoucher(long merchant_id) {
        DataLoader.getEvoucherUnused(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listEvoucher = (ArrayList<EVoucherUnused>) object;
                    if (listEvoucher != null) {
                        groupCodeApiBody = new GroupCodeApiBody();
                        String ids = "";
                        for (EVoucherUnused unused : listEvoucher){
                            ids += unused.getId() + " ";
                        }

                        groupCodeApiBody.setEvoucher_ids(ids.trim().replace(" ", ","));
                        groupCodeApiBody.setMerchant_id(evoucherModel.getMerchant_info().getId());

                        loadGroupCode(groupCodeApiBody);
                    }
                }
            }
        }, page, record_per_page, merchant_id);
    }

    private void loadGroupCode(GroupCodeApiBody groupCodeApiBody) {
        DataLoader.getGroupCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    GroupCode groupCode = (GroupCode) object;
                    initAdapter(listEvoucher, groupCode, evoucherModel);
                } else {
                    Toast.makeText(getContext(), object.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, groupCodeApiBody);
    }

    private void initAdapter(ArrayList<EVoucherUnused> listEvoucher, GroupCode groupCode, EvoucherDetailModel evoucherModel) {
        multiActiveAdapter = new MultiActiveAdapter(activity, listEvoucher, groupCode, evoucherModel);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recLayout.setLayoutManager(layoutManager);
        recLayout.setAdapter(multiActiveAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (activity instanceof EVoucherAccountActivity){
            ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                activity.onBackPressed();
                break;
            case R.id.btn_active:
                if (NEW_ACTIVE) {
                    ((BoughtEvoucherActivity) activity).reset();
                    activity.onBackPressed();
                } else {
                    if (multiActiveAdapter != null) {
                        multiActiveAdapter.unCheckAll();
                    }
                }
                break;
        }
    }
}
