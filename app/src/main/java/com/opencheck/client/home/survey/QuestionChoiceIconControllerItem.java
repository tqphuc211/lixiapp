package com.opencheck.client.home.survey;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyQuestChoiceIconItemBinding;
import com.opencheck.client.models.merchant.survey.SurveyAnswerModel;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;

public class QuestionChoiceIconControllerItem {
    LixiActivity activity;
    View parent;
    View rootView;
    SurveyQuestionModel quest;
    private int pos;
    QuestionChoiceIconController parentController;

    private View v_space;
    private ImageView iv;
    private TextView tv_name;

    public QuestionChoiceIconControllerItem(LixiActivity activity, ViewGroup parent, SurveyQuestionModel quest, int pos, QuestionChoiceIconController parentController) {
        this.activity = activity;
        this.parent = parent;
        this.quest = quest;
        this.pos = pos;
        this.parentController = parentController;
        SurveyQuestChoiceIconItemBinding surveyQuestChoiceIconItemBinding =
                SurveyQuestChoiceIconItemBinding.inflate(LayoutInflater.from(activity),
                        parent, false);
        rootView = surveyQuestChoiceIconItemBinding.getRoot();
        parent.addView(rootView);

        findView();
        setData();
    }

    public void findView() {
        v_space = rootView.findViewById(R.id.v_space);
        iv = (ImageView) rootView.findViewById(R.id.iv);
        tv_name = (TextView) rootView.findViewById(R.id.tv_name);


        if (getPos() == 0)
            v_space.setVisibility(View.GONE);
    }

    public void setData() {
        SurveyAnswerModel asw = quest.getAnswers().get(getPos());
        tv_name.setText(asw.getAnswer_name());
        ImageLoader.getInstance().displayImage(asw.getUncheck_icon(), iv, LixiApplication.getInstance().optionsNomal);


        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentController.selectAnswer(QuestionChoiceIconControllerItem.this);
            }
        });
    }

    public void setSelected(boolean selected) {
        SurveyAnswerModel asw = quest.getAnswers().get(getPos());
        if (selected)
            ImageLoader.getInstance().displayImage(asw.getCheck_icon(), iv, LixiApplication.getInstance().optionsNomal);
        else
            ImageLoader.getInstance().displayImage(asw.getUncheck_icon(), iv, LixiApplication.getInstance().optionsNomal);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
