package com.opencheck.client.home.merchant.map;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class StoreLocationAdapter extends ArrayAdapter<StoreModel> {


    private Activity activity;
    private int layoutItem;
    private ItemClickListener itemClickListener;

    class ViewHolder {
        private RelativeLayout rlRoot;
        private TextView tvName;
    }

    public StoreLocationAdapter(Activity _activity, int _layoutItem, ItemClickListener _itemClickListener) {
        super(_activity, _layoutItem);
        this.activity = _activity;
        this.layoutItem = _layoutItem;
        this.itemClickListener = _itemClickListener;
    }

    public void addListItems(List<StoreModel> popups) {
        for (StoreModel obj : popups) {
            add(obj);
        }
        notifyDataSetChanged();
    }

    public ArrayList<StoreModel> getListItems() {
        ArrayList<StoreModel> listItems = new ArrayList<StoreModel>();
        for (int i = 0; i < getCount(); i++) {
            listItems.add(getItem(i));
        }
        return listItems;
    }


    private int posSelected = -1;

    public void setPosSelected(int posSelected) {
        this.posSelected = posSelected;
    }

    public int getPosSelected() {
        return posSelected;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(layoutItem, null);

            holder.rlRoot = (RelativeLayout) convertView.findViewById(R.id.rlRoot);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final StoreModel item = getItem(position);

        holder.tvName.setText(item.getAddress());
        holder.tvName.setGravity(Gravity.CENTER_VERTICAL);

        if (position == posSelected) {
            holder.rlRoot.setBackgroundResource(R.drawable.bg_btn_launch_login);
            holder.tvName.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.rlRoot.setBackgroundResource(R.drawable.bg_border_btn_login_signup);
            holder.tvName.setTextColor(Color.parseColor("@color/app_violet"));
        }

        return convertView;
    }
}
