package com.opencheck.client.home.notification;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.custom.PinnedSectionListView;
import com.opencheck.client.custom.notification.NotiViewManager;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.activity.IntroCodeActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.history.detail.OrderDetailDialog;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.home.lixishop.header.rewardcode.LixiCodeDetailDialog;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.models.user.OrderModel;
import com.opencheck.client.models.user.PurchaseModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

public class NotificationV1Adapter extends ArrayAdapter<NotificationModel> implements PinnedSectionListView.PinnedSectionListAdapter {

    private ArrayList<NotificationModel> listNoti;
    private LixiActivity activity;
    private int layout;
    private final String ONLY_DAY = "dd-MM-yyyy";
    private final String DAY_TIME = "dd-MM-yyyy HH:mm:ss";

    public NotificationV1Adapter(@NonNull LixiActivity activity, int layout, ArrayList<NotificationModel> listNoti) {
        super(activity, layout);
        this.listNoti = listNoti;
        this.activity = activity;
        this.layout = layout;
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == OrderModel.SECTION;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getTypeItem();
    }

    private class NotiHolder{
        private TextView txtHeader;
        private RelativeLayout rlItem;
        private CircleImageView imgItemCircle;
        private TextView tvTitle;
        private TextView tvItem;
        private TextView tvTime;
        private View viewLine;
    }

    public void addListItems(List<NotificationModel> popups) {
        if (popups.size() > 0) {
            if (getCount() == 0) {
                NotificationModel orders = new NotificationModel();
                orders.setTypeItem(PurchaseModel.SECTION);
                orders.setDate_created_timestamp(popups.get(0).getDate_created_timestamp());
                add(orders);
            }else {
                String time = Helper.getFormatDateVN(ONLY_DAY, popups.get(0).getDate_created_timestamp());
                String timeP = Helper.getFormatDateVN(ONLY_DAY, getListItems().get(getCount() - 1).getDate_created_timestamp());
                if (!time.equals(timeP)){
                    NotificationModel orders = new NotificationModel();
                    orders.setTypeItem(OrderModel.SECTION);
                    orders.setDate_created_timestamp(popups.get(0).getDate_created_timestamp());
                    add(orders);
                }
            }

            add(popups.get(0));
            for (int i = 1; i < popups.size(); i++) {

                String time = Helper.getFormatDateVN(ONLY_DAY, popups.get(i).getDate_created_timestamp());
                String timeP = Helper.getFormatDateVN(ONLY_DAY, popups.get(i - 1).getDate_created_timestamp());

                if (!time.equals(timeP)) {
                    NotificationModel orders = new NotificationModel();
                    orders.setTypeItem(OrderModel.SECTION);
                    orders.setDate_created_timestamp(popups.get(i).getDate_created_timestamp());
                    add(orders);
                }
                popups.get(i).setTypeItem(OrderModel.ITEM);
                add(popups.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public ArrayList<NotificationModel> getListItems() {
        ArrayList<NotificationModel> listItems = new ArrayList<NotificationModel>();
        for (int i = 0; i < getCount(); i++) {

            listItems.add(getItem(i));
        }
        return listItems;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        NotiHolder holder;
        if (convertView == null){
            holder = new NotiHolder();

            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(layout, null);

            holder.imgItemCircle = convertView.findViewById(R.id.imgItemCircle);
            holder.rlItem = convertView.findViewById(R.id.rlItem);
            holder.viewLine = convertView.findViewById(R.id.viewLine);
            holder.tvItem = convertView.findViewById(R.id.tvItem);
            holder.tvTitle = convertView.findViewById(R.id.tvTitle);
            holder.tvTime = convertView.findViewById(R.id.tvTime);
            holder.txtHeader = convertView.findViewById(R.id.txtHeader);
            convertView.setTag(holder);
        }else {
            holder = (NotiHolder) convertView.getTag();
        }

        final NotificationModel item = getItem(position);
        if (item != null) {
            if (item.getTypeItem() == PurchaseModel.ITEM) {
                // item
                holder.txtHeader.setVisibility(View.GONE);
                holder.rlItem.setVisibility(View.VISIBLE);

                ImageLoader.getInstance().displayImage(item.getImage(), holder.imgItemCircle, LixiApplication.getInstance().optionsNomal);
                holder.tvTitle.setText(item.getTitle());
                holder.tvItem.setText(item.getBody());
                holder.tvTime.setText(Helper.getFormatDateVN(DAY_TIME, item.getDate_created_timestamp()));
            } else {
                // header
                holder.txtHeader.setVisibility(View.VISIBLE);
                holder.rlItem.setVisibility(View.GONE);

                String time = Helper.getFormatDateVN(ONLY_DAY, item.getDate_created_timestamp());
                String currentTime = Helper.getFormatDateVN(ONLY_DAY, System.currentTimeMillis());
                if (time.equals(currentTime)) {
                    holder.txtHeader.setText(LanguageBinding.getString(R.string.today, activity));
                } else {
                    holder.txtHeader.setText(time);
                }
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getTypeItem() == OrderModel.ITEM) {
                    handleNotiClick(position);
                }
            }
        });
        return convertView;
    }

    private void handleNotiClick(int pos) {
        NotificationModel dto = getItem(pos);
        dto.setIs_read("1");
        notifyDataSetChanged();
        handleNotificationAction(dto);
    }

    private void handleNotificationAction(NotificationModel notificationModel) {
        DataLoader.setReadNotification(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    NotiViewManager.getInstance().hasNotify();
                }
            }
        }, notificationModel.getPublic_id());

        String modelType = notificationModel.getType();
        //set utm to send to tracking
        SocketUtmModel socketUtmModel = new SocketUtmModel();
        socketUtmModel.setSource("crm");
        socketUtmModel.setCampaign("title_pro");
        socketUtmModel.setMedium("noti");
        ConstantValue.SOCKET_UTM_MODEL = socketUtmModel;

        OrderDetailDialog detailDialog;
        switch (modelType) {

            case NotificationModel.POS_POINT_ADD_WAIT:
            case NotificationModel.POS_POINT_ADD_WAIT_OLD:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                break;
            case NotificationModel.POS_POINT_ADD_DONE:
            case NotificationModel.POS_POINT_ADD_DONE_OLD:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                break;
            case NotificationModel.POS_POINT_ADD_CANCEL:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                break;
            case NotificationModel.ORDER_CODE:
                LixiCodeDetailDialog codeDetailDialog = new LixiCodeDetailDialog(activity, false, true, false);
                codeDetailDialog.show();
                codeDetailDialog.setData(Long.parseLong(notificationModel.getObject_id()));
                break;
            case NotificationModel.SALE_ORDER_CANCEL:
            case NotificationModel.SALE_ORDER_CHANGE:
            case NotificationModel.ORDER_ADD:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                break;
            case NotificationModel.ADMIN_SURVEY:
            case NotificationModel.SURVEY:
                getSurvey(notificationModel);
                break;
            case NotificationModel.SURVEY_CODE:
                getSurveyCode(notificationModel);
                break;
            case NotificationModel.PAYMENT_ADD:
            case NotificationModel.PAYMENT_ADD_BUY_CARD:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                try {
                    copyPin(notificationModel);
                } catch (Exception ex) {
                }
                break;
            case NotificationModel.PAYMENT_ADD_TOPUP:
                detailDialog = new OrderDetailDialog(activity, false, true, false);
                detailDialog.show();
                detailDialog.setData(notificationModel.getDetail_id(), modelType);
                break;
            case NotificationModel.PROMOTION_ADD:
            case NotificationModel.PROMOTION_EXPIRE:
//                CustomFragmentActivity.startPromotionDetailActivity(this, notifycationModel.getObject_id(), true);
                break;
            case NotificationModel.ADMIN_MERCHANT_ADD:
                MerchantModel mc = new MerchantModel();
                mc.setId(notificationModel.getObject_id());
                Intent merchant = new Intent(activity, MerchantDetailActivity.class);
                Bundle data = new Bundle();
                data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(mc));
                data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.LIST_NOTIFICATION);
                merchant.putExtras(data);
                activity.startActivity(merchant);
                break;
            case NotificationModel.ADMIN_PROMOTION_ADD:
//                CustomFragmentActivity.startPromotionDetailActivity(this, notifycationModel.getObject_id(), true);
                break;
            case NotificationModel.SHOP_BUY_FAILED:
            case NotificationModel.SHOP_BUY_SUCCESS:
                OrderShopDetailDialog dialog = new OrderShopDetailDialog(activity, false, true, false);
                dialog.show();
                dialog.setData(notificationModel.getDetail_id());
                break;
            case NotificationModel.ADMIN_LINK_ADD:
                if (notificationModel.getUrl() != null && notificationModel.getUrl().length() > 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notificationModel.getUrl()));
                    activity.startActivity(browserIntent);
                }
                break;
            case NotificationModel.ADMIN_ADD_PUSH:
                //ve trang chu
//                isChangeTab = true;
//                tabHost.setCurrentTab(0);
                break;
            case NotificationModel.VOUCHER_DETAIL:
                String id = notificationModel.getObject_id();
                openVoucherDetail(id);
                break;
            case NotificationModel.DELI_ARTICLE_DETAIL:
//                Bundle dataArticle = new Bundle();
//                dataArticle.putLong("ID", Long.parseLong(notificationModel.getObject_id()));
//                dataArticle.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.LIST_NOTIFICATION);
//                Intent intentArticle = new Intent(activity, StoreListOfArticleActivity.class);
//                intentArticle.putExtras(dataArticle);
//                activity.startActivity(intentArticle);
                StoreListOfArticleActivity.startArticleActivity(activity, 0L,
                        Long.parseLong(notificationModel.getObject_id()), "", "",
                        TrackingConstant.ContentSource.LIST_NOTIFICATION);
                break;
            case NotificationModel.DELI_STORE_DETAIL:
                StoreDetailActivity.startStoreDetailActivity(activity, Long.parseLong(notificationModel.getObject_id()), TrackingConstant.ContentSource.LIST_NOTIFICATION);
                break;
            case NotificationModel.USER_ORDER_CANCEL:
            case NotificationModel.USER_ORDER_CONFIRMED:
            case NotificationModel.USER_ORDER_PICKED:
                Bundle dataOrder = new Bundle();
                dataOrder.putString("uuid", notificationModel.getObject_id());
                Intent intentOrder = new Intent(activity, OrderTrackingActivity.class);
                intentOrder.putExtras(dataOrder);
                activity.startActivity(intentOrder);
                break;
            case NotificationModel.INTRO_CODE:
                IntroCodeActivity.start(activity);
                break;
            default:
                break;
        }
    }

    private void openVoucherDetail(String id) {
        LixiShopEvoucherModel lixiShopEvoucherModel = new LixiShopEvoucherModel();
        lixiShopEvoucherModel.setId(Long.parseLong(id));
//        activity.startActivity(new Intent(activity, CashEvoucherDetailActivity.class).putExtra(CashEvoucherDetailActivity.PRODUCT_ID, (lixiShopEvoucherModel.getId())));
        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, lixiShopEvoucherModel.getId(), TrackingConstant.ContentSource.LIST_NOTIFICATION);
    }

    public void copyPin(Object object) {
        if (((NotificationModel) object).getType().equals("payment.add.buycard")) {
            String tt = ((NotificationModel) object).getBody();
            //Bạn vừa sử dụng OC Credit cho việc Mua thẻ cào [Viettel] serial: 57706827880, code:6552989031555
            int l = tt.length();
            int st = tt.indexOf("code:");
            if (st >= 0) {
                int e = st + 6;
                while (e < l && tt.charAt(e) >= '0' && tt.charAt(e) <= '9')
                    e++;
                tt = tt.substring(st + 5, e).trim();
                Helper.copyCode(tt, activity);
            } else {
                Toast.makeText(activity, "Tin nhắn không đúng, không thể copy mã pin!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void getSurvey(NotificationModel notifycationModel) {
        //TODO servey
//        new CallServiceTask().userGetSurvey(activity,
//                ApiEntity.GET_SURVEY + "/" + notifycationModel.getObject_id(),
//                true, new CallServiceCallbackObj() {
//                    @Override
//                    public void callBack(Boolean isSuccess, Object objects) {
//                        if (isSuccess) {
//                            SurveyModel jsonResords = (new Gson()).fromJson(objects.toString(), SurveyModel.class);
//
//                            dialogInviteSurvey = new PopupSurveyInv(activity, "", "", "", false, true, false);
//                            dialogInviteSurvey.setIDidalogEventListener(new PopupSurveyInv.IDidalogEvent() {
//                                @Override
//                                public void onOk(SurveyModel surveyModel) {
//                                    startSurvey(surveyModel);
//                                }
//                            });
//                            dialogInviteSurvey.setCancelable(false);
//                            dialogInviteSurvey.show();
//                            dialogInviteSurvey.setData(jsonResords);
//                        } else {
//                            PopupNotifySimple dialog = new PopupNotifySimple((CustomFragmentActivity) activity, objects.toString(), false, true, false);
//                            dialog.show();
//                        }
//                    }
//                });

        DataLoader.getUserSurvey(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    SurveyModel surveyModel = (SurveyModel) object;

                }
            }
        }, notifycationModel.getObject_id());
    }

    public void getSurveyCode(NotificationModel notifycationModel) {
        //TODO API
//        new CallServiceTask().userGetSurveyCode(activity,
//                ApiEntity.SURVEY_CODE + "/" + notifycationModel.getObject_id(),
//                true, new CallServiceCallbackObj() {
//                    @Override
//                    public void callBack(Boolean isSuccess, Object objects) {
//                        if (isSuccess) {
//                            LixiCodeModel jsonResords = (new Gson()).fromJson(objects.toString(), LixiCodeModel.class);
//
//                            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
//                            ClipData clip = ClipData.newPlainText("reward code copyed", jsonResords.getCode());
//                            clipboard.setPrimaryClip(clip);
//
//                            Toast.makeText(activity, "Đã copy mã khuyến mãi: " + jsonResords.getCode(), Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            PopupNotifySimple dialog = new PopupNotifySimple(activity, objects.toString(), false, true, false);
//                            dialog.show();
//                        }
//                    }
//                });
    }
}
