package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class StorePolicyModel implements Serializable {
    private String type;
    private long id;
    private String state;
    private long merchant_id;
    private boolean is_all_store;
    private String name;
    private long start_date;
    private long end_date;
    private String description;
    private Long min_money_to_commission;
    private Long commission_percent;
    private Long min_money_to_go_shipping;
    private Long max_free_ship_money;
    private Long min_money_to_free_ship_price;
    private Long fixed_ship_price;
    private Long fixed_ship_price_range;
    private boolean apply_commission;
    private int priority;
    private String promotion_text;
    private int lixi_commission_percent;
    private ArrayList<Long> list_promotion_code_id;
    private ArrayList<String> list_promotion_code;
    private ArrayList<String> list_payment_method_name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public boolean isIs_all_store() {
        return is_all_store;
    }

    public void setIs_all_store(boolean is_all_store) {
        this.is_all_store = is_all_store;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMin_money_to_commission() {
        return min_money_to_commission;
    }

    public void setMin_money_to_commission(Long min_money_to_commission) {
        this.min_money_to_commission = min_money_to_commission;
    }

    public Long getCommission_percent() {
        return commission_percent;
    }

    public void setCommission_percent(Long commission_percent) {
        this.commission_percent = commission_percent;
    }

    public Long getMin_money_to_go_shipping() {
        return min_money_to_go_shipping;
    }

    public void setMin_money_to_go_shipping(Long min_money_to_go_shipping) {
        this.min_money_to_go_shipping = min_money_to_go_shipping;
    }

    public Long getMax_free_ship_money() {
        return max_free_ship_money;
    }

    public void setMax_free_ship_money(Long max_free_ship_money) {
        this.max_free_ship_money = max_free_ship_money;
    }

    public Long getMin_money_to_free_ship_price() {
        return min_money_to_free_ship_price;
    }

    public void setMin_money_to_free_ship_price(Long min_money_to_free_ship_price) {
        this.min_money_to_free_ship_price = min_money_to_free_ship_price;
    }

    public Long getFixed_ship_price() {
        return fixed_ship_price;
    }

    public void setFixed_ship_price(Long fixed_ship_price) {
        this.fixed_ship_price = fixed_ship_price;
    }

    public Long getFixed_ship_price_range() {
        return fixed_ship_price_range;
    }

    public void setFixed_ship_price_range(Long fixed_ship_price_range) {
        this.fixed_ship_price_range = fixed_ship_price_range;
    }

    public boolean isApply_commission() {
        return apply_commission;
    }

    public void setApply_commission(boolean apply_commission) {
        this.apply_commission = apply_commission;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getPromotion_text() {
        return promotion_text;
    }

    public void setPromotion_text(String promotion_text) {
        this.promotion_text = promotion_text;
    }

    public int getLixi_commission_percent() {
        return lixi_commission_percent;
    }

    public void setLixi_commission_percent(int lixi_commission_percent) {
        this.lixi_commission_percent = lixi_commission_percent;
    }

    public ArrayList<Long> getList_promotion_code_id() {
        return list_promotion_code_id;
    }

    public void setList_promotion_code_id(ArrayList<Long> list_promotion_code_id) {
        this.list_promotion_code_id = list_promotion_code_id;
    }

    public ArrayList<String> getList_promotion_code() {
        return list_promotion_code;
    }

    public void setList_promotion_code(ArrayList<String> list_promotion_code) {
        this.list_promotion_code = list_promotion_code;
    }

    public ArrayList<String> getList_payment_method_name() {
        return list_payment_method_name;
    }

    public void setList_payment_method_name(ArrayList<String> list_payment_method_name) {
        this.list_payment_method_name = list_payment_method_name;
    }
}
