package com.opencheck.client.home.history;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.PinnedSectionListView;
import com.opencheck.client.custom.SelectableRoundedImageView;
import com.opencheck.client.models.user.OrderModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class HistoryPromotionStickyAdapter extends ArrayAdapter<OrderModel> implements PinnedSectionListView.PinnedSectionListAdapter {
    public static String ITEM_CLICK_ACTION = "ITEM_CLICK_ACTION";

    private Activity activity;
    private int layoutItem;
    private int posSelection = 1;
    private Boolean isPayment = false;


    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type;
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == OrderModel.SECTION;
    }

    private class ViewHolder {
        private TextView tvHeader;
        private RelativeLayout rlItem;
        private SelectableRoundedImageView imgItem;
        private TextView tvName;
        private TextView tvTime;
        private TextView tvPrice;
        private RelativeLayout rlLine;
    }

    public HistoryPromotionStickyAdapter(Activity _activity, int layoutItem) {
        super(_activity, layoutItem);
        this.activity = _activity;
        this.layoutItem = layoutItem;
    }

    public void addListItems(List<OrderModel> popups) {

        if (popups.size() > 0) {
            if (getCount() == 0) {
                OrderModel orders = new OrderModel();
                orders.type = OrderModel.SECTION;

                orders.setDate(popups.get(0).getDate());
                orders.setTime(popups.get(0).getTime());
                orders.setTimeStamp(popups.get(0).getTimeStamp());

                add(orders);
            }

            add(popups.get(0));
            for (int i = 1; i < popups.size(); i++) {

                Long timeCurrent = Long.parseLong(popups.get(i).getTimeStamp());
                Long timePre = Long.parseLong(popups.get(i - 1).getTimeStamp());

                if (!Helper.getDifferentDay(timeCurrent, timePre)) {
                    OrderModel orders = new OrderModel();
                    orders.type = OrderModel.SECTION;
                    orders.setDate(popups.get(i).getDate());
                    orders.setTime(popups.get(i).getTime());
                    orders.setTimeStamp(popups.get(i).getTimeStamp());
                    add(orders);
                }
                popups.get(i).type = OrderModel.ITEM;
                add(popups.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public ArrayList<OrderModel> getListItems() {
        ArrayList<OrderModel> listItems = new ArrayList<OrderModel>();
        for (int i = 0; i < getCount(); i++) {

            listItems.add(getItem(i));
        }
        return listItems;
    }

    public void setPosSelection(int posSelection) {
        this.posSelection = posSelection;
    }

    public int posShowMore = -1;

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(layoutItem, null);
            holder.tvHeader = convertView.findViewById(R.id.tvHeader);
            holder.rlItem = convertView.findViewById(R.id.rlItem);
            holder.imgItem = convertView.findViewById(R.id.imgItem);
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvTime = convertView.findViewById(R.id.tvTime);
            holder.tvPrice = convertView.findViewById(R.id.tvPrice);
            holder.rlLine = convertView.findViewById(R.id.rlLine);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final OrderModel item = getItem(position);


        if (item.type == OrderModel.ITEM) {
            holder.tvHeader.setVisibility(View.GONE);
            holder.rlItem.setVisibility(View.VISIBLE);
            holder.tvName.setText(item.getStore().getName());
            holder.tvTime.setText(item.getDate() + " - " + item.getTime());

            if (item.getState().equalsIgnoreCase(OrderModel.STATE_DRAFT) || item.getState().equalsIgnoreCase(OrderModel.STATE_SENT)) {
                holder.tvPrice.setText("");
            } else if (item.getState().equalsIgnoreCase(OrderModel.STATE_DONE) || item.getState().equalsIgnoreCase(OrderModel.STATE_SALE)) {
                holder.tvPrice.setText("+" + Helper.getVNCurrency((long) Float.parseFloat(item.getReal_point())) + "đ");
            }

            if (item.getStore().getListImage().size() > 0) {
                if (!item.getStore().getListImage().get(0).getImage_medium().equalsIgnoreCase("")) {
                    ImageLoader.getInstance().displayImage(item.getStore().getListImage().get(0).getImage_medium(), holder.imgItem, LixiApplication.getInstance().optionsNomal);
                } else {
                    ImageLoader.getInstance().displayImage(item.getStore().getListImage().get(0).getImage(), holder.imgItem, LixiApplication.getInstance().optionsNomal);
                }
            } else {
                holder.imgItem.setImageDrawable(activity.getResources().getDrawable(R.drawable.image_placeholder));
            }

            if (posShowMore == position) {
                holder.rlLine.setVisibility(View.GONE);

            } else {
                holder.rlLine.setVisibility(View.VISIBLE);
            }

        } else {
            holder.rlItem.setVisibility(View.GONE);
            holder.tvHeader.setVisibility(View.VISIBLE);
            long time = Long.parseLong(item.getTimeStamp()) * 1000;
            holder.tvHeader.setText(LanguageBinding.getString(R.string.Month, activity) + " " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_MONTH_YEAR, String.valueOf(time)));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getType() == OrderModel.ITEM) {
                    if (item.getState().equalsIgnoreCase(OrderModel.STATE_DRAFT) || item.getState().equalsIgnoreCase(OrderModel.STATE_SENT)) {
                    } else if (item.getState().equalsIgnoreCase(OrderModel.STATE_DONE) || item.getState().equalsIgnoreCase(OrderModel.STATE_SALE)) {
                        if (posShowMore == position) {
                            posShowMore = -1;
                        } else {
                            posShowMore = position;
                        }
                        notifyDataSetChanged();
                    }
                }
            }
        });

        return convertView;
    }
}
