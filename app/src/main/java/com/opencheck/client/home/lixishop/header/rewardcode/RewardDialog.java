package com.opencheck.client.home.lixishop.header.rewardcode;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RewardDialogBinding;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class RewardDialog extends LixiDialog {

    public RewardDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu;
    private TextView tv_title;
    private TextView tv_message;
    private Button bt_ok;

    SpannableStringBuilder spanMessage;
    String mss = "";
    String title = "";
    String okText = "";


    public void setData( String _title, String _okText, SpannableStringBuilder _spanMess) {
        spanMessage = _spanMess;
        this.title = _title;
        this.okText = _okText;
        pourData();
    }

    public void setData(  String title, String ms, String okText) {
        mss = ms;
        this.title = title;
        this.okText = okText;
        pourData();
    }

    private void pourData() {
        tv_title.setText(title);
        if (spanMessage != null)
            tv_message.setText(spanMessage);
        else
            tv_message.setText(mss);

        if (okText.length() > 0)
            bt_ok.setText(okText);

        rl_dismiss.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
    }

    private RewardDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mBinding = RewardDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }

    private void findViews() {
        rl_dismiss = findViewById(R.id.rl_dismiss);

        ll_root_popup_menu = findViewById(R.id.ll_root_popup_menu);
        tv_title = findViewById(R.id.tv_title);
        tv_message = findViewById(R.id.tv_message);
        bt_ok = findViewById(R.id.bt_ok);

    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rl_dismiss:
            case R.id.bt_ok:
                if (eventListener != null) {
                    eventListener.onOk();
                }
                dismiss();

            default:
                break;
        }
    }

    public interface IDidalogEvent {
        void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
