package com.opencheck.client.home.account.activecode2.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.ConfirmSuccessFragmentBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.activecode2.controller.VoucherController;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmSuccessFragment extends LixiFragment{

    private LinearLayout ll_container, ll_done;
    private CircleImageView iv_merchant;
    private TextView tv_name, tv_address, tv_total_price, tv_note_message, tv_support_message;
    private NearestStoreModel storeModel;
    private ArrayList<LixiShopEvoucherBoughtModel> listVoucher = new ArrayList<>();

    public ConfirmSuccessFragment() {
        // Required empty public constructor
    }

    private ConfirmSuccessFragmentBinding mBinding;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = ConfirmSuccessFragmentBinding.inflate(inflater, container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LixiShopEvoucherBoughtModel>>(){}.getType();
        String JsonListVoucher = getArguments().getString("arr_ev");
        ArrayList<LixiShopEvoucherBoughtModel> voucher_list = gson.fromJson(JsonListVoucher, type);
        listVoucher = voucher_list;
        storeModel = new Gson().fromJson(getArguments().getString("store"), NearestStoreModel.class);
        initView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initView(View v){
        tv_address = (TextView) v.findViewById(R.id.tv_address);
        tv_name = (TextView) v.findViewById(R.id.tv_name);
        iv_merchant = v.findViewById(R.id.iv_merchant);
        ll_container = (LinearLayout) v.findViewById(R.id.ll_container);
        ll_done = (LinearLayout) v.findViewById(R.id.ll_done);
        tv_total_price = (TextView) v.findViewById(R.id.tv_total_price);
        tv_note_message = v.findViewById(R.id.tv_note_message);
        tv_support_message = v.findViewById(R.id.tv_support_message);

        ll_done.setOnClickListener(this);
        initData();
    }

    private void initData(){
        tv_name.setText(storeModel.getName());
        tv_address.setText(storeModel.getAddress());
        tv_note_message.setText(Html.fromHtml(activity.getResources().getString(R.string.note_message)));
        tv_support_message.setText(Html.fromHtml(activity.getResources().getString(R.string.support_message)));
        ImageLoader.getInstance().displayImage(storeModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
        long price = 0L;
        for(int i = 0; i < listVoucher.size(); i++) {
            new VoucherController(activity, listVoucher.get(i), i, listVoucher.size(), true, ll_container);
            price += listVoucher.get(i).getPrice();
        }

        tv_total_price.setText(Helper.getVNCurrency(price) + "đ");
    }

    @Override
    public void onClick(View view) {
        if(view == ll_done){
            activity.onBackPressed();
            HomeActivity.getInstance().tab = 4;
            LoginHelper.startHomeActivity(activity);
            new AppPreferenceHelper(activity).setOnRefresh(true);
            activity.finish();
        }
    }

}
