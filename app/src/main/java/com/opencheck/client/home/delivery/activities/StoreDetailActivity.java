package com.opencheck.client.home.delivery.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.custom.PagerSlidingTabStrip;
import com.opencheck.client.databinding.ActivityStoreDeliDetailBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.CartAdapter;
import com.opencheck.client.home.delivery.adapter.MenuAdapter;
import com.opencheck.client.home.delivery.adapter.MenuTitleAdapter;
import com.opencheck.client.home.delivery.dialog.AddonCategoryDialog;
import com.opencheck.client.home.delivery.dialog.FreeShipDialog;
import com.opencheck.client.home.delivery.dialog.PauseServiceDialog;
import com.opencheck.client.home.delivery.fragments.DetailInfoFragment;
import com.opencheck.client.home.delivery.fragments.MenuFragment;
import com.opencheck.client.home.delivery.interfaces.OnGetMenuToActivityListener;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.delivery.libs.CustomViewPager;
import com.opencheck.client.home.delivery.libs.MyCustomLayoutManager;
import com.opencheck.client.home.delivery.libs.MyScrollView;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.MenuModel;
import com.opencheck.client.home.delivery.model.MenuTitleModel;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class StoreDetailActivity extends LixiActivity implements OnGetMenuToActivityListener {

    //variable main view
    private ImageView iv_back, iv_merchant, iv_banner, iv_share, iv_search, iv_like, iv_back_clone, iv_search_clone, iv_share_clone, iv_like_clone;
    public AppBarLayout appBarLayout;
    private TextView tv_address, tv_time, tv_order, tv_title, tv_total, tv_count, tv_distance, tv_tags, tv_shipping_fee, tv_title_clone;
    public RelativeLayout rl_cart, rl_header_clone, rootView, rl_cart_background, rl_order;
    public ImageView iv_cart;
    private View v_line;
    private CustomViewPager viewPager;
    private CircleImageView img_count;
    private CollapsingToolbarLayout collapsing;
    private StoreOfCategoryModel store;
    private FragmentOrderAdapter orderAdapter;
    private RecyclerView rv_menu_title;
    private CardView card;

    //variable for cart dialog
    private RelativeLayout rl_cart_dialog, rl_root_popup_menu;
    private TextView tv_cart_total, tv_product_title;
    private LinearLayout ll_delete_cart, lnCartNoResult;
    private RecyclerView rv_food_dialog;
    private CartAdapter mCartAdapter;

    //variable for search dialog
    private RelativeLayout rl_search_menu;
    private LinearLayout lnSearchNoResult, ll_true_store;
    private EditText edt_search;
    private ImageView iv_delete_search;
    private RecyclerView rv_menu_search;
    private MenuAdapter mSearchAdapter;
    private ArrayList<ProductModel> listProduct = new ArrayList<>();

    //
    private MenuTitleAdapter menuTitleAdapter;
    private int height;
    private boolean isLoaded = false;
    private int last = -1;
    private boolean isCollapsed = false;
    public static int heightAppbar = 0;
    private long storeId = 0L;
    private String source = "";
    private ArrayList<MenuTitleModel> mList = new ArrayList<>();
    public static boolean isWatch;

    //cart data
    //private HashMap<String, CartModel> cart = new HashMap<>();
    private DeliAddressModel mLocation;

    private ActivityStoreDeliDetailBinding mBinding;
    public final static String ID = "ID";
    public final static String PRODUCT_ID = "PRODUCT_ID";
    private boolean isUpdate = false;

    private String paymentMethodDefault = "";

    public static void startStoreDetailActivity(Activity activity, long storeId, String source) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(activity, StoreDetailActivity.class);
        bundle.putLong(ID, storeId);
        bundle.putString(GlobalTracking.SOURCE, source);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void startStoreDetailActivity(Activity activity, long storeId, String source, long productId) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(activity, StoreDetailActivity.class);
        bundle.putLong(ID, storeId);
        bundle.putLong(PRODUCT_ID, productId);
        bundle.putString(GlobalTracking.SOURCE, source);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static Intent getCallingIntent(Context context, long storeId, String source) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(context, StoreDetailActivity.class);
        bundle.putLong(ID, storeId);
        bundle.putString(GlobalTracking.SOURCE, source);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_store_deli_detail);
        storeId = getIntent().getExtras().getLong(ID, 0L);
        source = getIntent().getExtras().getString(GlobalTracking.SOURCE);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        isWatch = false;
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.STORE,
                storeId,
                TrackingConstant.Currency.VND,
                0,
                source
        );

        initViewsMain();
        initCartDialogViews();
        initViewSearchMenuViews();
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    //region CREATE VIEW
    private void initViewsMain() {
        iv_back = findViewById(R.id.iv_back);
        appBarLayout = findViewById(R.id.appbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        iv_merchant = findViewById(R.id.iv_merchant);
        iv_cart = findViewById(R.id.iv_cart);
        tv_address = findViewById(R.id.tv_address);
        tv_time = findViewById(R.id.tv_time);
        tv_distance = findViewById(R.id.tv_distance);
        tv_order = findViewById(R.id.tv_order);
        tv_tags = findViewById(R.id.tv_tags);
        tv_shipping_fee = findViewById(R.id.tv_shipping_fee);
        tv_total = findViewById(R.id.tv_total);
        rl_cart = findViewById(R.id.rl_cart);
        tv_count = findViewById(R.id.tv_count);
        img_count = findViewById(R.id.img_count);
        iv_banner = findViewById(R.id.iv_banner);
        iv_share = findViewById(R.id.iv_share);
        iv_search = findViewById(R.id.iv_search);
        iv_like = findViewById(R.id.iv_like);
        tv_title = findViewById(R.id.tv_title);
        collapsing = findViewById(R.id.collapsing);
        rl_header_clone = findViewById(R.id.rl_header_clone);
        rootView = findViewById(R.id.rootView);
        tv_title_clone = findViewById(R.id.tv_title_clone);
        rv_menu_title = findViewById(R.id.rv_menu_title);
        v_line = findViewById(R.id.v_line);
        card = findViewById(R.id.card);
        ll_true_store = findViewById(R.id.ll_true_store);
        rl_cart_background = findViewById(R.id.rl_cart_background);
        rl_order = findViewById(R.id.rl_order);
        iv_back_clone = findViewById(R.id.iv_back_clone);
        iv_search_clone = findViewById(R.id.iv_search_clone);
        iv_share_clone = findViewById(R.id.iv_share_clone);
        iv_like_clone = findViewById(R.id.iv_like_clone);

        height = toolbar.getHeight();
        iv_back.setOnClickListener(this);
        rl_cart.setOnClickListener(this);
        iv_merchant.setOnClickListener(this);
        iv_search.setOnClickListener(this);
        iv_share.setOnClickListener(this);
        iv_like.setOnClickListener(this);
        rl_order.setOnClickListener(this);
        tv_shipping_fee.setOnClickListener(this);
        iv_back_clone.setOnClickListener(this);
        iv_search_clone.setOnClickListener(this);
        iv_share_clone.setOnClickListener(this);
        iv_like_clone.setOnClickListener(this);
        rl_header_clone.setOnClickListener(this);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());
                heightAppbar = appBarLayout.getHeight() - Math.abs(verticalOffset);
                //onChangeOffsetListener.onChangeOffsetListener(appBarLayout.getHeight() - Math.abs(verticalOffset));
                //iv_banner.setAlpha(1 - (offsetAlpha * -1));
                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    isCollapsed = true;
                    if (store != null) {
                        if (store.getName() != null && store.getName().length() > 0)
                            tv_title.setText(store.getName());
                        tv_title.setVisibility(View.VISIBLE);
                        iv_back.setColorFilter(Color.argb(255, 172, 30, 161));
                        iv_share.setColorFilter(Color.argb(255, 172, 30, 161));
                        iv_search.setColorFilter(Color.argb(255, 172, 30, 161));
                        collapsing.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));

                        if (viewPager.getCurrentItem() == 0 && !isLoaded) {
                            isLoaded = true;
                            viewPager.setPagingEnabled(false);
                            int heightCard = 0;
                            if (card.getHeight() == 0) {
                                heightCard = (int) activity.getResources().getDimension(R.dimen.value_100);
                            } else
                                heightCard = card.getHeight();
                            if (menuTitleAdapter != null && menuTitleAdapter.getItemCount() > 0) {
                                translateDownAnimation(card, heightCard);
                                card.setVisibility(View.VISIBLE);
                                if (store != null)
                                    tv_title_clone.setText(store.getName());
                            }

                        }
                    }

                } else {
                    //expands
                    tv_title.setVisibility(View.GONE);
                    iv_back.setColorFilter(Color.argb(255, 255, 255, 255));
                    iv_share.setColorFilter(Color.argb(255, 255, 255, 255));
                    iv_search.setColorFilter(Color.argb(255, 255, 255, 255));
                    collapsing.setScrimVisibleHeightTrigger(5);
                    card.setVisibility(View.GONE);
                    viewPager.setPagingEnabled(true);
                    isLoaded = false;
                    isCollapsed = false;
                }
            }
        });

        initPager();
        setMenuTitleAdapter();
//        getStoreDetail();
    }

    private void initCartDialogViews() {
        rl_cart_dialog = findViewById(R.id.rl_cart_dialog);
        rl_root_popup_menu = findViewById(R.id.rl_root_popup_menu);
        tv_cart_total = findViewById(R.id.tv_cart_total);
        tv_product_title = findViewById(R.id.tv_product_title);
        ll_delete_cart = findViewById(R.id.ll_delete_cart);
        lnCartNoResult = findViewById(R.id.lnCartNoResult);
        rv_food_dialog = findViewById(R.id.rcv_food);
        View v_cart_dialog = findViewById(R.id.v_cart_dialog);

        ViewGroup.LayoutParams params = rl_root_popup_menu.getLayoutParams();
        params.height = 2 * activity.heightScreen / 3;
        rl_root_popup_menu.setLayoutParams(params);

        ll_delete_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                deleteCart(cart);
                setCartData();
            }
        });

        v_cart_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_cart_dialog.setVisibility(View.GONE);
            }
        });

        rl_root_popup_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        setCartDialogAdapter();
    }

    private void initViewSearchMenuViews() {
        rl_search_menu = findViewById(R.id.rl_search_menu);
        TextView tv_exit = findViewById(R.id.tv_exit);
        lnSearchNoResult = findViewById(R.id.lnSearchNoResult);
        edt_search = findViewById(R.id.edt_search);
        iv_delete_search = findViewById(R.id.iv_delete);
        rv_menu_search = findViewById(R.id.rv_menu_search);
        edt_search = findViewById(R.id.edt_search);

        tv_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_search_menu.setVisibility(View.GONE);
                card.setVisibility(View.VISIBLE);
                edt_search.setInputType(InputType.TYPE_NULL);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                }
            }
        });

        iv_delete_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_search.setText("");
            }
        });

        rl_search_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().equals(""))
                    iv_delete_search.setVisibility(View.GONE);
                else
                    iv_delete_search.setVisibility(View.VISIBLE);
                mSearchAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setSearchAdapter();

    }
    //endregion

    //region SET DATA
    private void addRecentlyStore(final StoreOfCategoryModel store) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (UserModel.getInstance() != null) {
                    HashMap<String, ArrayList<StoreOfCategoryModel>> mapStore = Helper.getStoreFromFile(activity, ConstantValue.STORE_FILE);
                    if (!mapStore.containsKey(UserModel.getInstance().getId())) {
                        mapStore.put(UserModel.getInstance().getId(), new ArrayList<StoreOfCategoryModel>());
                    }
                    ArrayList<StoreOfCategoryModel> stores = mapStore.get(UserModel.getInstance().getId());
                    int currentPos = -1;
                    for (int i = 0; i < stores.size(); i++) {
                        if (stores.get(i).getId() == store.getId()) {
                            currentPos = i;
                            break;
                        }
                    }
                    if (stores.size() >= 10) {
                        if (currentPos != -1) {
                            stores.remove(currentPos);
                        } else {
                            stores.remove(0);
                        }
                    } else {
                        if (currentPos != -1) {
                            stores.remove(currentPos);
                        }
                    }
                    stores.add(store);
                    Helper.saveStoreToFile(mapStore, activity, ConstantValue.STORE_FILE);

                    for (int i = 0; i < stores.size(); i++) {
                        Helper.showLog("store in file:" + i + " - " + stores.get(i).getName());
                    }
                }
            }
        });
    }

    private void setCartDialogData() {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        ArrayList<ProductModel> productList = new ArrayList<>();
        if (cart.containsKey("" + storeId)) {
            Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
            for (String key : keySet) {
                Helper.showLog("for key: " + key + "=>" + new Gson().toJson(cart.get("" + storeId).getProduct_list().get(key)));
                productList.addAll(cart.get("" + storeId).getProduct_list().get(key));
            }

            mCartAdapter.setProductList(productList);
            lnCartNoResult.setVisibility(View.GONE);
        } else {
            lnCartNoResult.setVisibility(View.VISIBLE);
        }

        rl_cart_dialog.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.popup_from_bottom);
        rl_root_popup_menu.setAnimation(animation);
        rl_root_popup_menu.startAnimation(animation);
        tv_product_title.setVisibility(View.GONE);
        tv_cart_total.setVisibility(View.VISIBLE);
        ll_delete_cart.setVisibility(View.VISIBLE);

        mCartAdapter.setItemClickListener(totalDialogItemClickListener);
        tv_cart_total.setText(Html.fromHtml("Tổng cộng: " + tv_count.getText() + " món"));
    }

    private void setSearchAdapter() {
        mSearchAdapter = new MenuAdapter(activity, null);
        rv_menu_search.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        rv_menu_search.setHasFixedSize(true);
        rv_menu_search.setAdapter(mSearchAdapter);

        mSearchAdapter.setOnDataChangeListener(new MenuAdapter.OnDataChangeListener() {
            @Override
            public void onDataChange(boolean isEmpty) {
                if (isEmpty)
                    lnSearchNoResult.setVisibility(View.VISIBLE);
                else lnSearchNoResult.setVisibility(View.GONE);
            }
        });

        mSearchAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                ProductModel product = (ProductModel) object;
                if (action.equals(ConstantValue.ADD_CLICK)) {
                    if (isWatch) {
                        Toast.makeText(activity, "Xin lỗi bạn không thể thêm vào giỏ hàng sản phẩm này. Vui lòng quay lại sau", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (product.isHave_addon()) {
                        getProductDetail(product, pos);
                    } else {
                        ((MenuFragment) orderAdapter.getItem(0)).getAdapter().addProductOnClickEvent(product.getId());
                        int posInMenu = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getPosOfItem(product);
                        ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(posInMenu, count.intValue());
                        addProductToCart(product, count.intValue());
                        quadToAnimation(view, count.intValue());
                        setCartData();
                    }

                } else if (action.equals(ConstantValue.SUB_CLICK)) {
                    if (product.isHave_addon()) {
                        setSpecifyProductData(product);
                    } else {
                        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                        if (count == 0) {
                            ((MenuFragment) orderAdapter.getItem(0)).getAdapter().removeProductInAddedList(product.getId());
                            cart.get("" + storeId).getProduct_list().remove("" + product.getId());
                            if (cart.get("" + storeId).getProduct_list().size() == 0) {
                                cart.remove("" + storeId);
                            }
                        } else
                            cart.get("" + storeId).getProduct_list().get("" + product.getId()).get(0).setQuantity(count.intValue());

                        int posInMenu = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getPosOfItem(product);
                        ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(posInMenu, count.intValue());
                        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                        setCartData();

                    }
                }
            }
        });
    }

    private void setCartDialogAdapter() {
        mCartAdapter = new CartAdapter(activity, null);
        rv_food_dialog.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        rv_food_dialog.setAdapter(mCartAdapter);
    }

    public void setSpecifyProductData(ProductModel product) {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        if (cart.containsKey("" + storeId)) {
            if (cart.get("" + storeId) != null && cart.get("" + storeId).getProduct_list() != null) {
                ArrayList<ProductModel> productList = new ArrayList<>(cart.get("" + storeId).getProduct_list().get("" + product.getId()));
                if (product.getIs_hot() == 1) {
                    for (ProductModel productModel : productList
                    ) {
                        productModel.setIs_hot(1);
                    }
                }
                mCartAdapter.setProductList(productList);
                lnCartNoResult.setVisibility(View.GONE);
            }
        } else {
            lnCartNoResult.setVisibility(View.VISIBLE);
        }

        rl_cart_dialog.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.popup_from_bottom);
        rl_root_popup_menu.setAnimation(animation);
        rl_root_popup_menu.startAnimation(animation);
        tv_product_title.setVisibility(View.VISIBLE);
        tv_cart_total.setVisibility(View.GONE);
        ll_delete_cart.setVisibility(View.GONE);
        tv_product_title.setText(Html.fromHtml(product.getName() + " (" + product.getQuantity() + ")"));
        mCartAdapter.setItemClickListener(specifyDialogItemClickListener);
    }

    private void setMenuTitleAdapter() {
        menuTitleAdapter = new MenuTitleAdapter(activity, null);
        MyCustomLayoutManager mLayoutManager = new MyCustomLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        rv_menu_title.setLayoutManager(mLayoutManager);
        rv_menu_title.setAdapter(menuTitleAdapter);
        menuTitleAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(final int pos, String action, Object object, Long count) {
                toggleTitle(pos);
                if (menuFragment != null && menuFragment.isAdded() && menuFragment.getScroll() != null) {
                    menuFragment.getScroll().post(new Runnable() {
                        @Override
                        public void run() {
                            rv_menu_title.smoothScrollToPosition(pos);
                            int positionTemp = pos;
                            if (positionTemp >= menuFragment.getHeaderList().size()) {
                                positionTemp = menuFragment.getHeaderList().size() - 1;

                            }
                            menuFragment.getScroll()
                                    .scrollTo(0, menuFragment.getHeaderList().get(positionTemp));
                        }
                    });
                }
            }
        });
    }

    public void toggleTitle(int pos) {
        if (pos >= menuTitleAdapter.getItemCount())
            return;
        if (last != pos) {
            mList.get(pos).setSelected(!mList.get(pos).isSelected());
            if (last != -1)
                mList.get(last).setSelected(false);

            if (menuTitleAdapter != null) {
                last = pos;
                menuTitleAdapter.notifyDataSetChanged();
            }
        }
    }

    private void showData() {
        tv_address.setText(store.getName());
        tv_time.setText(Html.fromHtml(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity)));
        if (store.getShipping_distance() >= 500) {
            double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
            tv_distance.setText(String.valueOf(distance) + " km");
        } else
            tv_distance.setText(Html.fromHtml(String.valueOf(Math.round(store.getShipping_distance())) + " m"));

        if (store.getLogo_link() != null)
            ImageLoader.getInstance().displayImage(store.getLogo_link(), iv_merchant, LixiApplication.getInstance().optionsNomal);

        ImageLoader.getInstance().displayImage(store.getCover_link(), iv_banner, LixiApplication.getInstance().optionsNomal);

        if (store.getList_tag().size() > 0) {
            tv_tags.setVisibility(View.VISIBLE);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < store.getList_tag().size(); i++) {
                builder.append("#").append(store.getList_tag().get(i).getName()).append("  ");
            }
            tv_tags.setText(builder.toString());
        } else tv_tags.setVisibility(View.GONE);

        if (store.isTrusted_store())
            ll_true_store.setVisibility(View.VISIBLE);

        updateViewLike(store.isIs_favorite());
        addRecentlyStore(store);
//        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
    }

    private void updateViewLike(boolean isLiked) {
        if (isLiked) {
            iv_like.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_heart));
            iv_like.setColorFilter(ContextCompat.getColor(activity, R.color.app_violet));
            iv_like_clone.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_heart));
            iv_like_clone.setColorFilter(ContextCompat.getColor(activity, R.color.app_violet));
        } else {
            iv_like.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_heart_outline));
            iv_like.setColorFilter(ContextCompat.getColor(activity, R.color.white));
            iv_like_clone.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_heart_outline));
            iv_like_clone.setColorFilter(ContextCompat.getColor(activity, R.color.app_violet));
        }

    }

    public void getStoreDetail() {
        DeliDataLoader.getStoreDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    store = (StoreOfCategoryModel) object;
                    checkConditions(store);

                    if (mFirstTabEvent != null && mSecondTabEvent != null) {
                        mFirstTabEvent.onDataReceivedOnFirstTab(store);
                        mSecondTabEvent.onDataReceivedOnSecondTab(store);
                    }

                    showData();
                    getPaymentMethod();

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                    finish();
                }
            }
        }, storeId, mLocation == null ? null : mLocation.getFullAddress(), mLocation == null ? null : mLocation.getLat(), mLocation == null ? null : mLocation.getLng());
    }

    private void checkConditions(StoreOfCategoryModel store) {
        DeliConfigModel deliEnable = Helper.getConfigByKey(activity, DeliConfigModel.DELI_ENABLE_KEY);
        boolean isDeliEnable = deliEnable == null ? true : Boolean.valueOf(deliEnable.getValue());
        String deli_working_time = DeliConfigModel.DELI_WORKING_TIME_DEFAULT;
        DeliConfigModel deliWorkingTime = Helper.getConfigByKey(activity, DeliConfigModel.DELI_WORKING_TIME_KEY);
        if (deliWorkingTime != null) {
            deli_working_time = deliWorkingTime.getValue();
        }

        Calendar currentTime = Calendar.getInstance();
        Calendar openTime = Calendar.getInstance();
        openTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[0]));
        openTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[1]));

        Calendar closeTime = Calendar.getInstance();
        closeTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[0]));
        closeTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[1]));

        if (!isDeliEnable) {
            showPauseDialog(store, PauseServiceDialog.LIXI_OFF);
        } else if (store.getState().equals("paused")) {
            showPauseDialog(store, PauseServiceDialog.STORE_STOP_SERVICE);
        } else if (!store.isIs_opening()) {
            showPauseDialog(store, PauseServiceDialog.STORE_OUT_OF_TIME);
        } else if (currentTime.compareTo(openTime) < 0 || currentTime.compareTo(closeTime) > 0) {
            showPauseDialog(store, PauseServiceDialog.LIXI_OUT_OF_TIME);
        } else if (AppPreferenceHelper.getInstance().getDefaultLocation() != null) {
            getUserDistance();
        }
    }

    private void getUserDistance() {
        DeliDataLoader.getUserDistance(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    double distance = (double) object;
                    double limitDistance = 8000;
                    DeliConfigModel deliConfigModel = Helper.getConfigByKey(activity, DeliConfigModel.DELI_LIMIT_DISTANCE);
                    if (deliConfigModel != null) {
                        limitDistance = Double.valueOf(deliConfigModel.getValue());
                    }
                    if (distance > limitDistance)
                        showPauseDialog(store, PauseServiceDialog.FAR_AWAY);

                }
            }
        }, store.getId(), AppPreferenceHelper.getInstance().getDefaultLocation().getLat(), AppPreferenceHelper.getInstance().getDefaultLocation().getLng());
    }

    private void showPauseDialog(final StoreOfCategoryModel store, String type) {
        PauseServiceDialog dialog = new PauseServiceDialog(activity, false, true, false);
        dialog.show();
        dialog.setData(store, type);
        dialog.setIDialogEventListener(new PauseServiceDialog.IDialogEvent() {
            @Override
            public void onOrder(Long time) {
                isWatch = false;
                if (mLocation == null) {
                    HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                    if (cart.containsKey("" + storeId)) {
                        if (cart.get("" + storeId) != null)
                            cart.get("" + storeId).setShip_receive_time(time);
                        else {
                            CartModel cartModel = new CartModel();
                            cartModel.setShip_receive_time(time);
                            cart.put("" + storeId, cartModel);
                        }
                    } else {
                        CartModel cartModel = new CartModel();
                        cartModel.setShip_receive_time(time);
                        cart.put("" + storeId, cartModel);
                    }

                    Helper.saveObJectToFile(cart, activity, ConstantValue.CART_FILE);
                }
                shipping_receive_time = time.toString();
            }

            @Override
            public void onWatch() {
                isWatch = true;
            }

            @Override
            public void onExit() {
                isWatch = false;
                finish();
            }
        });
    }

    private long money = 0L;

    public void setCartData() {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        ArrayList<ProductModel> productList = new ArrayList<>();
        if (cart.containsKey("" + storeId)) {
            CartModel cartModel = cart.get("" + storeId);
            if (cartModel != null && cartModel.getProduct_list() != null) {
                Set<String> keySet = cartModel.getProduct_list().keySet();
                for (String key : keySet) {
                    productList.addAll(cartModel.getProduct_list().get(key));
                }
            }
        }

        int total = 0;
        money = 0L;
        if (productList.size() > 0) {
            for (int i = 0; i < productList.size(); i++) {
                total += productList.get(i).getQuantity();
                money += productList.get(i).getPrice() * productList.get(i).getQuantity();
            }
        }

        if (total > 0) {
            tv_count.setVisibility(View.VISIBLE);
            rl_order.setBackgroundColor(getResources().getColor(R.color.app_violet));
            rl_cart_background.setBackground(ContextCompat.getDrawable(activity, R.drawable.bg_cirlce_violet));
        } else {
            tv_count.setVisibility(View.GONE);
            img_count.setVisibility(View.GONE);
            rl_order.setBackgroundColor(getResources().getColor(R.color.color_order));
            rl_cart_background.setBackground(ContextCompat.getDrawable(activity, R.drawable.bg_circle_cart_gray));
            deleteCart(cart);
        }

        tv_count.setText(Html.fromHtml(total + ""));
        tv_total.setText(Html.fromHtml(Helper.getVNCurrency(money) + "đ"));
    }

    private void deleteCart(HashMap<String, CartModel> cart) {
        if (orderAdapter.getItem(0) != null && ((MenuFragment) orderAdapter.getItem(0)).getAdapter() != null) {
            ArrayList<ProductModel> listProductOfMenu = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItemList();
            if (listProductOfMenu != null) {
                if (cart.containsKey("" + storeId)) {
                    if (cart.get("" + storeId) != null
                            && cart.get("" + storeId).getProduct_list() != null
                            && cart.get("" + storeId).getProduct_list().size() > 0) {
                        Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
                        for (String key : keySet) {
                            ProductModel mProductTemp = cart.get("" + storeId).getProduct_list().get(key).get(0);
                            for (int j = 0; j < listProductOfMenu.size(); j++) {
                                if (mProductTemp.getId() == listProductOfMenu.get(j).getId()) {
                                    ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(j, 0);
                                    if (mSearchAdapter != null)
                                        mSearchAdapter.updateQuantityForList(j, 0);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    ArrayList<Long> addedList = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getAddedList();
                    for (int i = 0; i < addedList.size(); i++) {
                        for (int j = 0; j < listProductOfMenu.size(); j++) {
                            if (addedList.get(i) == listProductOfMenu.get(j).getId()) {
                                ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(j, 0);
                                if (mSearchAdapter != null)
                                    mSearchAdapter.updateQuantityForList(j, 0);
                                break;
                            }
                        }
                    }
                }
                cart.remove("" + storeId);
                Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                mCartAdapter.clearData();
                rl_cart_dialog.setVisibility(View.GONE);
            }
        }
    }

    //endregion

    //region CREATE VIEW PAGER
    private void initPager() {
        orderAdapter = new FragmentOrderAdapter(getSupportFragmentManager());
        addTabs(orderAdapter);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(orderAdapter);
        viewPager.setOffscreenPageLimit(2);

        // Give the PagerSlidingTabStrip the ViewPager
        // Attach the view pager to the tab strip
        final PagerSlidingTabStrip tabsStrip = findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setTextColorResource(R.drawable.selector_text_color_lixi);

        //  tabsStrip.setTypeface(Typeface.DEFAULT, R.style.AccountKit_InputText);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LinearLayout tab = (LinearLayout) tabsStrip.getChildAt(0);
                for (int i = 0; i < tab.getChildCount(); i++) {
                    TextView tv = (TextView) tab.getChildAt(i);
                    if (i != position) {
                        tv.setTextColor(0xAAAAAAAA);
                    } else {
                        tv.setTextColor(0xffac1ea1);
                    }
                }

                if (position == 0) {
                    if (isCollapsed) {
                        if (menuTitleAdapter != null && menuTitleAdapter.getItemCount() > 0) {
                            translateDownAnimation(card, card.getHeight());
                            card.setVisibility(View.VISIBLE);
                        }
                    } else {
                        translateUpAnimation(card, card.getHeight());
                        card.setVisibility(View.GONE);
                    }

                } else {
                    translateUpAnimation(card, card.getHeight());
                    card.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setCurrentItem(1);
        viewPager.setCurrentItem(0);
        tabsStrip.setPadding(0, 0, 0, 0);
    }

    MenuFragment menuFragment;

    private void addTabs(FragmentOrderAdapter adapter) {
        Bundle data = new Bundle();
        data.putLong("ID", storeId);
        if (getIntent().getExtras() != null && getIntent().getExtras().getLong(PRODUCT_ID) != 0L) {
            data.putLong("PRODUCT_ID", getIntent().getExtras().getLong(PRODUCT_ID));
        }
        menuFragment = new MenuFragment();
        menuFragment.setArguments(data);

        DetailInfoFragment detailInfoFragment = new DetailInfoFragment();
        detailInfoFragment.setArguments(data);

        adapter.addFragment(menuFragment, LanguageBinding.getString(R.string.home_menu_title_deli, activity));
        adapter.addFragment(detailInfoFragment, LanguageBinding.getString(R.string.detail, activity));
    }

    @Override
    public void onOk(ArrayList<MenuModel> mMenu) {
        if (mMenu != null && mMenu.size() > 0) {
            mList.clear();
            listProduct.clear();
            for (int i = 0; i < mMenu.size(); i++) {
                MenuTitleModel titleModel = new MenuTitleModel(mMenu.get(i).getCategory_name());
                mList.add(titleModel);
                listProduct.addAll(mMenu.get(i).getList_product());
            }

            mSearchAdapter.setListData(listProduct);

            if (menuTitleAdapter != null) {
                last = -1;
                menuTitleAdapter.setList(mList);
                toggleTitle(0);
            }
        }
    }

    class FragmentOrderAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> lis_fragments = new ArrayList<>();
        ArrayList<String> list_titles = new ArrayList<>();

        FragmentOrderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return lis_fragments.size();
        }

        void addFragment(Fragment fragment, String titles) {
            lis_fragments.add(fragment);
            list_titles.add(titles);
        }

        @Override
        public Fragment getItem(int position) {
            return lis_fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list_titles.get(position);
        }
    }

    //endregion

    //region ANIMATION
    private void translateDownAnimation(View view, float distance) {
        view.setTranslationY(-distance);
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", 0);
        animator.removeAllListeners();
        animator.start();
    }

    private void translateUpAnimation(View view, float distance) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", -distance);
        animator.setDuration(500);
        animator.removeAllListeners();
        animator.start();
    }

    private void quadToAnimation(final View view, int value) {
        int fromLoc[] = new int[2];
        ImageView imageView = view.findViewById(R.id.iv_add);
        imageView.getLocationOnScreen(fromLoc);
        float startX = fromLoc[0];
        float startY = fromLoc[1];

        int toLoc[] = new int[2];
        iv_cart.getLocationOnScreen(toLoc);

        TextView tvCount = new TextView(activity);
        final RelativeLayout rlCount = new RelativeLayout(activity);
        rlCount.setLayoutParams(new ViewGroup.LayoutParams((int) activity.getResources().getDimension(R.dimen.value_30),
                (int) activity.getResources().getDimension(R.dimen.value_30)));
        rlCount.setBackgroundResource(R.drawable.bg_cirlce_violet);
        tvCount.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int padding = (int) activity.getResources().getDimension(R.dimen.value_2);
        rlCount.setPadding(padding, padding, padding, padding);
        tvCount.setTextSize(activity.getResources().getDimension(R.dimen.value_6));
        tvCount.setTextColor(getResources().getColor(R.color.white));
        rlCount.setGravity(Gravity.CENTER);
        tvCount.setText(Html.fromHtml("" + value));
        rlCount.addView(tvCount);
        rootView.addView(rlCount);

        final Path path = new Path();
        path.moveTo(startX, startY);
        path.quadTo(startX, startY, activity.widthScreen / 3, startY);
        path.quadTo(activity.widthScreen / 3 - 2 * iv_cart.getWidth(), startY + iv_cart.getHeight(), toLoc[0], toLoc[1]);

        ValueAnimator pathAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator.setDuration(800);
        pathAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path, true);
                pathMeasure.getPosTan(pathMeasure.getLength() / 2 * val, point, null);
                Log.d("mapi", "" + point[0] + "/" + point[1]);

                rlCount.setTranslationX(point[0]);
                rlCount.setTranslationY(point[1]);
            }
        });

        pathAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                Log.d("mapi", "cancel animation");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.d("mapi", "end animation");
                rootView.removeView(rlCount);
                animateView();
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                Log.d("mapi", "start animation");
            }

            @Override
            public void onAnimationPause(Animator animation) {
                super.onAnimationPause(animation);
                Log.d("mapi", "pause animation");
            }

            @Override
            public void onAnimationResume(Animator animation) {
                super.onAnimationResume(animation);
                Log.d("mapi", "resume animation");
            }
        });
        pathAnimator.start();
        rlCount.animate().scaleX(0.5f).scaleY(0.5f).setDuration(800).start();
    }

    public void animateView() {
        Animation zoom = AnimationUtils.loadAnimation(activity, R.anim.zoom_out_anim);
        iv_cart.setAnimation(zoom);
        iv_cart.startAnimation(zoom);
    }
    //endregion

    //region UPLOAD ORDER

    private String shipping_receive_time = null;

    private void createOrder(final HashMap<String, CartModel> cart) {
        final JSONObject params = new JSONObject();
        try {
            params.put("store_id", storeId);
            params.put("receive_name", UserModel.getInstance().getFullname());
            params.put("receive_phone", UserModel.getInstance().getPhone());
            params.put("note", "");

            ArrayList<ProductModel> temp = new ArrayList<>();
            Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
            for (String key : keySet) {
                temp.addAll(cart.get("" + storeId).getProduct_list().get(key));
            }

            JSONArray productArr = new JSONArray();
            for (int i = 0; i < temp.size(); i++) {
                JSONObject productParams = new JSONObject();
                productParams.put("product_id", temp.get(i).getId());
                productParams.put("quantity", temp.get(i).getQuantity());
                productParams.put("note", "");

                if (temp.get(i).getList_addon() != null) {
                    JSONArray arrAddon = new JSONArray();
                    for (int j = 0; j < temp.get(i).getList_addon().size(); j++) {
                        JSONObject addParams = new JSONObject();
                        addParams.put("addon_id", temp.get(i).getList_addon().get(j).getId());
                        if (temp.get(i).getList_addon().get(j).isSelected())
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).isSelected()));
                        else
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).getDefault_value()));
                        arrAddon.put(addParams);
                    }
                    productParams.put("list_addon", arrAddon);
                }
                productArr.put(productParams);
            }

            params.put("list_product", productArr);
            params.put("shipping_receive_address", mLocation.getFullAddress());
            params.put("shipping_receive_lat", mLocation.getLat());
            params.put("shipping_receive_lng", mLocation.getLng());

            if (shipping_receive_time != null)
                params.put("shipping_receive_time", Long.valueOf(shipping_receive_time));
            else params.put("shipping_receive_time", JSONObject.NULL);
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            return;
        } catch (NullPointerException ex) {
            Crashlytics.logException(ex);
            return;
        }

        DeliDataLoader.createOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    try {
                        cart.get("" + storeId).setUuid((String) object);
                        cart.get("" + storeId).setState("draft");
                        cart.get("" + storeId).setShip_receive_time(shipping_receive_time == null ? null : Long.parseLong(shipping_receive_time));
                        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                    } catch (Exception e) {
                        Crashlytics.logException(new Throwable(new Exception("Cart null")));
                        Helper.showErrorDialog(activity, "Có lỗi xảy ra, vui lòng thử lại!");
                        return;
                    }

                    if (rl_cart_dialog.getVisibility() == View.VISIBLE) {
                        rl_cart_dialog.setVisibility(View.GONE);
                    }

                    Intent intent = new Intent(activity, OrderInfoActivity.class);
                    intent.putExtra(OrderInfoActivity.UUID, (String) object);
                    intent.putExtra(OrderInfoActivity.ID, store.getId());
                    intent.putExtra(OrderInfoActivity.PAYMENT_METHOD_DEFAULT, paymentMethodDefault);
                    intent.putExtra(PromotionCodeUserGroupActivity.USER_PROMOTION_CODE, getIntent().getExtras().getString(PromotionCodeUserGroupActivity.USER_PROMOTION_CODE));
                    intent.putExtra(GlobalTracking.SOURCE, source);
                    startActivityForResult(intent, ConstantValue.GET_LAST_ORDER_DATA);
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void updateOrder(HashMap<String, CartModel> cart) {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", cart.get("" + storeId).getUuid());
            ArrayList<ProductModel> temp = new ArrayList<>();
            Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
            for (String key : keySet) {
                temp.addAll(cart.get("" + storeId).getProduct_list().get(key));
            }

            JSONArray productArr = new JSONArray();
            for (int i = 0; i < temp.size(); i++) {
                JSONObject productParams = new JSONObject();
                productParams.put("product_id", temp.get(i).getId());
                productParams.put("quantity", temp.get(i).getQuantity());
                productParams.put("note", "");
                if (temp.get(i).getList_addon() != null) {
                    JSONArray arrAddon = new JSONArray();
                    for (int j = 0; j < temp.get(i).getList_addon().size(); j++) {
                        JSONObject addParams = new JSONObject();
                        addParams.put("addon_id", temp.get(i).getList_addon().get(j).getId());
                        if (temp.get(i).getList_addon().get(j).isSelected())
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).isSelected()));
                        else
                            addParams.put("value", String.valueOf(temp.get(i).getList_addon().get(j).getDefault_value()));
                        arrAddon.put(addParams);
                    }

                    productParams.put("list_addon", arrAddon);
                }
                productArr.put(productParams);

            }

            params.put("list_product", productArr);

            cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
            if (cart.get("" + storeId) != null && cart.get("" + storeId).getUpdated_ship_receive_time() != null)
                params.put("receive_date", cart.get("" + storeId).getUpdated_ship_receive_time());
            else params.put("receive_date", JSONObject.NULL);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.updateOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Intent intent = new Intent(activity, OrderInfoActivity.class);
                    intent.putExtra(OrderInfoActivity.UUID, (String) object);
                    intent.putExtra(OrderInfoActivity.ID, store.getId());
                    intent.putExtra(OrderInfoActivity.PAYMENT_METHOD_DEFAULT, paymentMethodDefault);
                    intent.putExtra(PromotionCodeUserGroupActivity.USER_PROMOTION_CODE, getIntent().getExtras().getString(PromotionCodeUserGroupActivity.USER_PROMOTION_CODE));
                    intent.putExtra(GlobalTracking.SOURCE, source);
                    startActivityForResult(intent, ConstantValue.GET_LAST_ORDER_DATA);
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    //endregion

    //region BASE EVENT ACTIVITY

    private Intent lastOrderData = null;
    private CartModel lastCartModel = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstantValue.LOGIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                if (cart.get("" + storeId) != null) {
                    if (mLocation != null) {
                        if (cart.get("" + storeId).getUuid() == null)
                            createOrder(cart);
                        else {
                            updateOrder(cart);
                            CartModel cartModel = cart.get("" + storeId);
                            lastOrderData = data;
                            lastCartModel = cartModel;
                        }
                    } else {
                        String receiveTime = null;
                        if (cart.get("" + storeId) != null) {
                            if (isUpdate) {
                                receiveTime = cart.get("" + storeId).getUpdated_ship_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getUpdated_ship_receive_time());
                            } else
                                receiveTime = cart.get("" + storeId).getShip_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getShip_receive_time());
                        }
                        OrderEmptyInfoActivity.startEmptyOrderCheckoutActivity(activity, storeId, source, receiveTime);
                    }
                } else {
                    Helper.showErrorToast(activity, "Có lỗi xảy ra, vui lòng quay lại sau");
                    finish();
                }
            } else {
                if (resultCode == ConstantValue.RESULT_NO_PROFILE) {
                    LoginHelper.Logout(StoreDetailActivity.this);
                    Toast.makeText(StoreDetailActivity.this, "Đăng nhập thất bại. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == ProductDetailActivity.REQUEST_CODE_DETAIL_PRODUCT) {
            menuFragment.onActivityResult(ProductDetailActivity.REQUEST_CODE_DETAIL_PRODUCT, resultCode, data);
        } else if (requestCode == ConstantValue.GET_LAST_ORDER_DATA) {
            if (resultCode == RESULT_OK) {
                HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                if (cart.containsKey("" + storeId)) {
                    CartModel cartModel = cart.get("" + storeId);
                    lastOrderData = data;
                    lastCartModel = cartModel;
                }
            }
        } else if (requestCode == OrderEmptyInfoActivity.EMPTY_ORDER_RESUME_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                isUpdate = true;
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == iv_back || view == iv_back_clone)
            onBackPressed();
        else if (view == rl_order) {
            if ((isWatch)) {
                Toast.makeText(activity, "Xin lỗi không thể thêm sản phẩm này vào giỏ hàng. Vui lòng quay lại sau.", Toast.LENGTH_SHORT).show();
                return;
            }

            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
            if (cart.containsKey("" + storeId)) {
                if (!LoginHelper.isLoggedIn(activity)) {
                    LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_ORDER_DELI);
                } else {
                    GlobalTracking.trackStartCheckout(TrackingConstant.ContentType.STORE, storeId, source, money);
                    mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
                    try {
                        if (mLocation != null) {
                            if (cart.get("" + storeId) != null) {
                                if (cart.get("" + storeId).getUuid() == null)
                                    createOrder(cart);
                                else {
                                    updateOrder(cart);
                                    trackingUpdateOrder(lastOrderData, lastCartModel);
                                    lastCartModel = null;
                                    lastOrderData = null;
                                }
                            }
                        } else {
                            String receiveTime = null;
                            if (isUpdate) {
                                receiveTime = cart.get("" + storeId).getUpdated_ship_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getUpdated_ship_receive_time());
                            } else
                                receiveTime = cart.get("" + storeId).getShip_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getShip_receive_time());
                            OrderEmptyInfoActivity.startEmptyOrderCheckoutActivity(activity, storeId, source, receiveTime);
                        }
                    } catch (Exception e) {
                        Crashlytics.logException(new Throwable(new Exception("Cart null " + storeId)));
                        Helper.showErrorToast(activity, "Có lỗi xảy ra, vui lòng quay lại sau");
                        finish();
                    }
                }
            } else
                Toast.makeText(activity, "Vui lòng chọn món trước khi đặt món", Toast.LENGTH_SHORT).show();
        } else if (view == rl_cart) {
            if (rl_cart_dialog.getVisibility() == View.GONE)
                setCartDialogData();
            else {
                translateUpAnimation(card, card.getHeight());
                rl_cart_dialog.setVisibility(View.GONE);
            }

        } else if (view == iv_search || view == iv_search_clone) {
            card.setVisibility(View.GONE);
            translateUpAnimation(card, card.getHeight());
            //mSearchAdapter.setListData(listProduct);
            mSearchAdapter.getFilter().filter(edt_search.getText().toString());
            rl_search_menu.setVisibility(View.VISIBLE);
            edt_search.setInputType(InputType.TYPE_CLASS_TEXT);
            edt_search.requestFocus();
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.showSoftInput(edt_search,
                        InputMethodManager.SHOW_IMPLICIT);

        } else if (view == iv_share || view == iv_share_clone) {
            if (store.getWeb_mobile_url() != null) {
                Helper.shareLink(activity, store.getWeb_mobile_url(), false);
            } else {
                Toast.makeText(activity, "Không thể chia sẻ thông tin", Toast.LENGTH_SHORT).show();
            }
        } else if (view == tv_shipping_fee) {
            FreeShipDialog dialog = new FreeShipDialog(activity, false, true, false);
            dialog.show();
            dialog.setData(store.getMin_money_to_free_ship_price(), store.getMax_free_ship_money());
        } else if (view == iv_like || view == iv_like_clone) {
            store.setIs_favorite(!store.isIs_favorite());
            updateViewLike(store.isIs_favorite());
            if (store.isIs_favorite()) {
                addFavoriteStore(storeId);
            } else {
                deleteFavoriteStore(storeId);
            }
        }
    }

    @Override
    protected void onResume() {
        id = storeId;
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        setCartData();
        super.onResume();
    }

    public RecyclerView getRv_menu_title() {
        return rv_menu_title;
    }

    @Override
    public void onBackPressed() {
        if (rl_cart_dialog.getVisibility() == View.VISIBLE) {
            rl_cart_dialog.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        try {
            AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    //endregion

    //region ITEM_DIALOG_LISTENER

    private int getIndexInCart(HashMap<String, CartModel> cart, ProductModel productModel) {
        int index = 0;
        for (int i = 0; i < cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).size(); i++) {
            if (productModel.isHave_addon()) {
                if (cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(i).getIndentifyAddon().equals(productModel.getIndentifyAddon())) {
                    index = i;
                    break;
                }
            } else {
                if (productModel.getId() == cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(i).getId()) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private ItemClickListener totalDialogItemClickListener = new ItemClickListener() {
        @Override
        public void onItemCLick(final int pos, final String action, Object object, final Long count) {
            final ProductModel productModel = (ProductModel) object;
            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);

            int posInMenu = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getPosOfItem(productModel);
            int quantity = 0;
            if (action.equals(ConstantValue.ADD_CLICK)) {
                if (productModel.isHave_addon()) {
                    int index = getIndexInCart(cart, productModel);
                    cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(index).setQuantity(count.intValue());
                } else
                    cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(0).setQuantity(count.intValue());

                quantity = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItem(posInMenu).getQuantity() + 1;
            } else if (action.equals(ConstantValue.SUB_CLICK)) {
                if (count == 0) {
                    mCartAdapter.removeItemAtPos(pos);
                    if (productModel.isHave_addon()) {
                        int index = getIndexInCart(cart, productModel);
                        cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).remove(index);
                    } else {
                        cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).remove(0);
                    }

                    //check product id have existed in store
                    if (cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).size() == 0) {
                        cart.get("" + storeId).getProduct_list().remove("" + productModel.getId());
                    }

                    if (mCartAdapter.getItemCount() == 0) {
                        lnCartNoResult.setVisibility(View.VISIBLE);
                        cart.remove("" + storeId);
                    } else
                        lnCartNoResult.setVisibility(View.GONE);
                } else {
                    if (productModel.isHave_addon()) {
                        int index = getIndexInCart(cart, productModel);
                        cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(index).setQuantity(count.intValue());
                    } else
                        cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(0).setQuantity(count.intValue());
                }

                quantity = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItem(posInMenu).getQuantity() - 1;

            }

            ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(posInMenu, quantity);
            if (mSearchAdapter != null)
                mSearchAdapter.notifyDataSetChanged();
            Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
            setCartData();
            tv_cart_total.setText(Html.fromHtml("Tổng cộng: " + tv_count.getText() + " món"));
        }
    };

    private ItemClickListener specifyDialogItemClickListener = new ItemClickListener() {
        @Override
        public void onItemCLick(int pos, String action, Object object, Long count) {
            ProductModel productModel = (ProductModel) object;
            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
            int posInMenu = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getPosOfItem(productModel);
            int quantity = 0;
            if (action.equals(ConstantValue.ADD_CLICK)) {
                int index = getIndexInCart(cart, productModel);
                cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(index).setQuantity(count.intValue());
                quantity = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItem(posInMenu).getQuantity() + 1;
            } else if (action.equals(ConstantValue.SUB_CLICK)) {
                if (count == 0) {
                    mCartAdapter.removeItemAtPos(pos);
                    int index = getIndexInCart(cart, productModel);
                    cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).remove(index);

                    if (cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).size() == 0) {
                        ((MenuFragment) orderAdapter.getItem(0)).getAdapter().removeProductInAddedList(productModel.getId());
                        cart.get("" + storeId).getProduct_list().remove("" + productModel.getId());
                        ((MenuFragment) orderAdapter.getItem(0)).getAdapter().removeProductInAddedList(productModel.getId());
                    }
                    if (mCartAdapter.getItemCount() == 0) {
                        lnCartNoResult.setVisibility(View.VISIBLE);
                        Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
                        Iterator<String> iterator = keySet.iterator();

                        if (!iterator.hasNext()) {
                            cart.remove("" + storeId);
                        }

                    } else lnCartNoResult.setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).size(); i++) {
                        if (cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(i).getIndentifyAddon().equals(productModel.getIndentifyAddon())) {
                            cart.get("" + storeId).getProduct_list().get("" + productModel.getId()).get(i).setQuantity(count.intValue());
                            break;
                        }
                    }
                }

                if (rl_search_menu.getVisibility() == View.VISIBLE) {
                    int index = mSearchAdapter.findIndexOfItem(productModel);
                    mSearchAdapter.notifyItemChanged(index);
                }

                quantity = ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItem(posInMenu).getQuantity() - 1;

            }

            ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateQuantity(posInMenu, quantity);
            if (productModel.getIs_hot() == 1) {
                ((MenuFragment) orderAdapter.getItem(0)).getAdapter().updateViewForSameProduct(productModel.getId(), posInMenu, action, quantity);
            }
            if (mSearchAdapter != null)
                mSearchAdapter.notifyDataSetChanged();
            Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
            setCartData();
            tv_product_title.setText(Html.fromHtml(productModel.getName() + " (" + ((MenuFragment) orderAdapter.getItem(0)).getAdapter().getItem(posInMenu).getQuantity() + ")"));

        }
    };
    //endregion

    //region SECTION_ADAPTER_AREA
    private void addProductToCart(ProductModel productModel, int quantity) {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        if (cart == null)
            cart = new HashMap<>();

        //If store is existed in cart
        if (cart.containsKey("" + storeId)) {
            CartModel cart_item = cart.get("" + storeId);
            //If store contains this product after that update its quantity
            if (cart_item.getProduct_list() != null && cart_item.getProduct_list().containsKey("" + productModel.getId())) {
                if (cart_item.getProduct_list().get("" + productModel.getId()).size() > 0)
                    cart_item.getProduct_list().get("" + productModel.getId()).get(0).setQuantity(quantity);
            } else { // add new product into cart
                ArrayList<ProductModel> temp = new ArrayList<>();
                temp.add(productModel);
                cart.get("" + storeId).getProduct_list().put(productModel.getId() + "", temp);
            }
        } else {
            ArrayList<ProductModel> temp = new ArrayList<>();
            temp.add(productModel);

            HashMap<String, ArrayList<ProductModel>> product_list = new HashMap<>();
            product_list.put("" + productModel.getId(), temp);

            CartModel cart_item = new CartModel();
            cart_item.setId(storeId);
            cart_item.setProduct_list(product_list);
            cart.put("" + storeId, cart_item);
        }

        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
    }

    private void getProductDetail(final ProductModel product, final int pos) {
        DeliDataLoader.getProductDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    final ProductModel productModel = (ProductModel) object;

                    AddonCategoryDialog categoryDialog = new AddonCategoryDialog(activity, false, true, false);
                    categoryDialog.show();
                    categoryDialog.setData(productModel);
                    categoryDialog.setIDialogEventListener(new AddonCategoryDialog.IDialogEvent() {
                        @Override
                        public void onOk(int quantity) {
                            ((MenuFragment) orderAdapter.getItem(0)).getAdapter().addProductOnClickEvent(product.getId());
                            mSearchAdapter.updateQuantityForFilterList(pos, quantity + product.getQuantity());
                            setCartData();
                        }
                    });

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, product.getId());
    }
    //endregion

    private OnTransferDataToFirstTabEvent mFirstTabEvent;

    private OnTransferDataToSecondTabEvent mSecondTabEvent;

    public void setReceiveStoreFirstEvent(OnTransferDataToFirstTabEvent event) {
        mFirstTabEvent = event;
    }

    public void setReceiveStoreSecondEvent(OnTransferDataToSecondTabEvent event) {
        mSecondTabEvent = event;
    }

    public interface OnTransferDataToFirstTabEvent {
        void onDataReceivedOnFirstTab(StoreOfCategoryModel store);
    }

    public interface OnTransferDataToSecondTabEvent {
        void onDataReceivedOnSecondTab(StoreOfCategoryModel store);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_STORE_DETAIL;
    }

    private void trackingUpdateOrder(Intent data, CartModel cartModel) {
        if (data == null || cartModel == null) {
            return;
        }
        if (data.getExtras() != null) {
            String coupon = data.getExtras().getBoolean(ConstantValue.LAST_ORDER_PROMOTION, false) ? TrackingConstant.CouponType.PROMOTION_CODE : "";
            GlobalTracking.trackUpdateCheckout(
                    TrackingConstant.ContentType.STORE,
                    storeId,
                    cartModel.getProduct_list().size(),
                    true,
                    TrackingConstant.Currency.VND,
                    data.getExtras().getString(ConstantValue.LAST_ORDER_PAYMENT_METHOD, ""),
                    coupon,
                    true,
                    data.getExtras().getString(ConstantValue.LAST_ORDER_TRANS_ID, ""),
                    data.getExtras().getLong(ConstantValue.LAST_ORDER_MONEY, 0),
                    source
            );
        }
    }

    private void getPaymentMethod() {
        DeliDataLoader.getPaymentMethod(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<PaymentModel> paymentList = (ArrayList<PaymentModel>) object;
                    if (paymentList == null) {
                        return;
                    }
                    boolean isExistCod = false;
                    for (int i = 0; i < paymentList.size(); i++) {
                        if (paymentList.get(i).getCode().equals("cod")) {
                            isExistCod = true;
                            break;
                        }
                    }
                    if (isExistCod) {
                        paymentMethodDefault = "";
                    } else {
                        if (paymentList.size() > 0) {
                            paymentMethodDefault = paymentList.get(0).getCode();
                        }
                    }
                }
            }
        }, store.getId(), null);
    }

    private void addFavoriteStore(long storeId) {
        JSONObject params = new JSONObject();
        try {
            params.put("store_id", storeId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.addFavoriteStore(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Helper.showLog("sucess add");
                } else {
                    Helper.showLog("sucess fail:" + object);
                }
            }
        }, params);
    }

    private void deleteFavoriteStore(long storeId) {
        DeliDataLoader.deleteFavoriteStore(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Helper.showLog("sucess delete");
                } else {
                    Helper.showLog("fail delete:" + object);
                }
            }
        }, storeId);
    }
}
