package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowNewEvoucherItemBinding;
import com.opencheck.client.home.account.new_layout.dialog.NotificationForCardDialog;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class ListEvoucherAdapter extends RecyclerView.Adapter<ListEvoucherAdapter.EvoucherHolder> {
    // Constructor
    private LixiActivity activity;
    private ArrayList<LixiShopEvoucherBoughtModel> listEvoucher;

    public ListEvoucherAdapter(LixiActivity activity, ArrayList<LixiShopEvoucherBoughtModel> listEvoucher) {
        this.activity = activity;
        this.listEvoucher = listEvoucher;
    }

    private EvoucherHolder evoucherHolder;

    @Override
    public EvoucherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowNewEvoucherItemBinding rowNewEvoucherItemBinding =
                RowNewEvoucherItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new EvoucherHolder(rowNewEvoucherItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(EvoucherHolder holder, int position) {
        evoucherHolder = holder;
        LixiShopEvoucherBoughtModel evoucher = listEvoucher.get(position);
        if (evoucher != null) {
            evoucherHolder.txtVoucherName.setText(evoucher.getProduct_name());
            try {
                if (evoucher.getProduct_cover_image() == null || evoucher.getProduct_cover_image().equals(""))
                    ImageLoader.getInstance().displayImage(evoucher.getProduct_image().get(0), evoucherHolder.imgPic, LixiApplication.getInstance().optionsNomal);
                else
                    ImageLoader.getInstance().displayImage(evoucher.getProduct_cover_image(), evoucherHolder.imgPic, LixiApplication.getInstance().optionsNomal);
            } catch (Exception e) {
            }

            evoucherHolder.cbActive.setChecked(evoucher.isSelected());

            if (evoucher.getState().equals(ConstantValue.STATUS_CAN_ACTIVE)) {
                evoucherHolder.txtStatus.setVisibility(View.GONE);
                evoucherHolder.cbActive.setVisibility(View.VISIBLE);
                evoucherHolder.txtPrice.setTextColor(activity.getResources().getColor(R.color.colorChecked_Green));
                evoucherHolder.txtDateText.setText(LanguageBinding.getString(R.string.expire_day, activity));
                evoucherHolder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy", evoucher.getExpired_time() + "000"));
            } else if (evoucher.getState().equals(ConstantValue.STATUS_WAITING)) {
                evoucherHolder.txtStatus.setVisibility(View.VISIBLE);
                evoucherHolder.cbActive.setVisibility(View.GONE);

                evoucherHolder.txtStatus.setTextColor(activity.getResources().getColor(R.color.colorChecked_Purple));
                evoucherHolder.txtPrice.setTextColor(activity.getResources().getColor(R.color.colorChecked_Green));
                evoucherHolder.txtDateText.setText(LanguageBinding.getString(R.string.expire_day, activity));
                evoucherHolder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy", evoucher.getExpired_time() + "000"));
            } else {
                evoucherHolder.txtStatus.setVisibility(View.VISIBLE);
                evoucherHolder.cbActive.setVisibility(View.GONE);

                evoucherHolder.txtStatus.setTextColor(activity.getResources().getColor(R.color.colorUncheck));
                evoucherHolder.txtPrice.setTextColor(activity.getResources().getColor(R.color.colorUncheck));
                if (evoucher.getState().equals("used")) {
                    evoucherHolder.txtStatus.setText(activity.getString(R.string.bought_evoucher_used));
                } else {
                    evoucherHolder.txtStatus.setText(activity.getString(R.string.bought_evoucher_expired));
                }

                if (evoucher.getActivate_date() != 0) {
                    evoucherHolder.txtDateText.setText(activity.getString(R.string.bought_evoucher_actived_day));
                    evoucherHolder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy", evoucher.getActivate_date() + "000"));
                } else {
                    evoucherHolder.txtDateText.setText(LanguageBinding.getString(R.string.expire_day, activity));
                    evoucherHolder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy", evoucher.getExpired_time() + "000"));
                }
            }

            evoucherHolder.txtPrice.setText(Helper.getVNCurrency(evoucher.getPrice()) + activity.getString(R.string.p_under));
        }

    }

    @Override
    public int getItemCount() {
        if (listEvoucher == null) {
            return 0;
        }
        return listEvoucher.size();
    }

    private OnItemClickListener onItemClickListener;
    public static final String ACTION_CLICK = "item click";
    public static final String ACTION_CHECK = "item check";

    public void addOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class EvoucherHolder extends RecyclerView.ViewHolder {
        ImageView imgPic;
        TextView txtVoucherName, txtPrice, txtStatus, txtDateText, txtDate;
        CheckBox cbActive;
        RelativeLayout constraintParent;

        public EvoucherHolder(View itemView) {
            super(itemView);
            imgPic = (ImageView) itemView.findViewById(R.id.imgPic);

            txtVoucherName = (TextView) itemView.findViewById(R.id.txtVoucherName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtDateText = (TextView) itemView.findViewById(R.id.txtDateText);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);

            cbActive = (CheckBox) itemView.findViewById(R.id.cbActive);

            constraintParent = (RelativeLayout) itemView.findViewById(R.id.constraintParent);
            constraintParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClickListener(null, getAdapterPosition(), ACTION_CLICK, listEvoucher.get(getAdapterPosition()), 0L);
                    }
                }
            });

            cbActive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listEvoucher.get(getAdapterPosition()).getIs_generated()) {
                        listEvoucher.get(getAdapterPosition()).setSelected(cbActive.isChecked());

                        if (listEvoucher.get(getAdapterPosition()).getState().equals(ConstantValue.STATUS_CAN_ACTIVE)) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onItemClickListener(null, getAdapterPosition(), ACTION_CHECK, listEvoucher.get(getAdapterPosition()), 0L);
                            }
                        }
                    } else {
                        cbActive.setChecked(false);
                        String content = String.format(LanguageBinding.getString(R.string.noti_voucher_not_lixi, activity), listEvoucher.get(getAdapterPosition()).getProduct_name());
                        showNotifyDialog(LanguageBinding.getString(R.string.active_voucher, activity), content);
                    }
                }
            });
        }
    }

    public void showNotifyDialog(String title, String content) {
        NotificationForCardDialog dialog = new NotificationForCardDialog(activity, false, false, false);
        dialog.show();
        dialog.setData(title, content);
    }
}
