package com.opencheck.client.home.rating.model;

public class RatingInfo {

    /**
     * average_rating : 0
     * total_rating : 0
     */

    private float average_rating;
    private long total_rating;

    public float getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(float average_rating) {
        this.average_rating = average_rating;
    }

    public long getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(long total_rating) {
        this.total_rating = total_rating;
    }
}
