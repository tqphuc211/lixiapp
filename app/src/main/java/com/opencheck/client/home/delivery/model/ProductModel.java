package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductModel implements Serializable{
    private long id;
    private long store_id;
    private String category_name = "name";
    private long category_id = 0L;
    private String name;
    private String state;
    private String pause_reason_message;
    private String logo;
    private String description;
    private long price;
    private long original_price;
    private String image_link;
    private boolean have_addon;
    private int is_hot;
    private long count_order;
    private int quantity = 0;
    private String note;
    private String identifyAddon;
    private ArrayList<AddonCategoryModel> list_addon_category;
    private ArrayList<AddonModel> list_addon;

    public String getIdentifyAddon() {
        return identifyAddon;
    }

    public void setIdentifyAddon(String identifyAddon) {
        this.identifyAddon = identifyAddon;
    }

    public int getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(int is_hot) {
        this.is_hot = is_hot;
    }

    public String getIndentifyAddon() {
        return identifyAddon;
    }

    public void setIndentifyAddon(String indentifyAddon) {
        this.identifyAddon = indentifyAddon;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPause_reason_message() {
        return pause_reason_message;
    }

    public void setPause_reason_message(String pause_reason_message) {
        this.pause_reason_message = pause_reason_message;
    }

    public long getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(long original_price) {
        this.original_price = original_price;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isHave_addon() {
        return have_addon;
    }

    public void setHave_addon(boolean have_addon) {
        this.have_addon = have_addon;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public long getCount_order() {
        return count_order;
    }

    public void setCount_order(long count_order) {
        this.count_order = count_order;
    }

    public ArrayList<AddonCategoryModel> getList_addon_category() {
        return list_addon_category;
    }

    public void setList_addon_category(ArrayList<AddonCategoryModel> list_addon_category) {
        this.list_addon_category = list_addon_category;
    }

    public ArrayList<AddonModel> getList_addon() {
        return list_addon;
    }

    public void setList_addon(ArrayList<AddonModel> list_addon) {
        this.list_addon = list_addon;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getStore_id() {
        return store_id;
    }

    public void setStore_id(long store_id) {
        this.store_id = store_id;
    }

    public ProductGetModel convert() {
        ProductGetModel productGetModel = new ProductGetModel();
        productGetModel.setName(name);
        productGetModel.setId(id);
        productGetModel.setNote(note);
        productGetModel.setPrice(price);
        productGetModel.setQuantity(quantity);

        if(list_addon != null && list_addon.size() > 0) {
            ArrayList<AddonGetModel> addonList = new ArrayList<>();
            for (int i = 0; i < list_addon.size(); i++) {
                addonList.add(list_addon.get(i).convert());
            }
            productGetModel.setList_addon(addonList);
        }
        return productGetModel;
    }
}
