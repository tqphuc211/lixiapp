package com.opencheck.client.home.account.evoucher.Model;

import com.opencheck.client.models.LixiModel;

public class GroupCodeApiBody extends LixiModel {
//    evoucher_ids:	string *
//    Chuỗi danh sách evoucher id cách nhau bởi dấu phẩy Ex:"23287,34"
//    merchant_id:	integer *
//    ID của merchant

    private String evoucher_ids;
    private long merchant_id;

    public String getEvoucher_ids() {
        return evoucher_ids;
    }

    public void setEvoucher_ids(String evoucher_ids) {
        this.evoucher_ids = evoucher_ids;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }
}
