package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityCategoryDetailBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.PlaceAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

import io.socket.client.Socket;

public class CategoryDetailActivity extends LixiActivity {

    private PlaceAdapter mAdapter;
    private ArrayList<StoreOfCategoryModel> storeList;
    private int page = 1;
    private SpacesItemDecoration spacesItemDecoration;
    private boolean isLoading = false;
    private boolean isReload = false;
    private boolean noRemain = false;
    private ArticleModel categoryModel;
    private DeliAddressModel mLocation;

    private ActivityCategoryDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_category_detail);
        categoryModel = (ArticleModel) getIntent().getSerializableExtra("CATEGORY");
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.DELI_CATEGORY,
                categoryModel.getId(),
                TrackingConstant.Currency.VND,
                0,
                TrackingConstant.ContentSource.HOME
        );
        initViews();
    }

    private void initViews() {
        mBinding.tvTitle.setText(categoryModel.getName());
        mBinding.ivBack.setOnClickListener(this);

        int space = (int) getResources().getDimension(R.dimen.value_8);
        spacesItemDecoration = new SpacesItemDecoration(1, space, true);

        mBinding.swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                noRemain = false;
                mBinding.rcvStore.removeItemDecoration(spacesItemDecoration);
                if (mAdapter != null) {
                    storeList.clear();
                    mAdapter.notifyDataSetChanged();
                }
                getStore(categoryModel.getId());
            }
        });

        mBinding.rcvStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || noRemain)
                    return;

                if (isRvBottom())
                    getStore(categoryModel.getId());
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getStore(categoryModel.getId());
            }
        }, 300);
    }

    private void getStore(long category_id) {
        try {
            isLoading = true;
            mBinding.swr.setRefreshing(true);
            DeliDataLoader.getStoreListByKey(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    mBinding.swr.setRefreshing(false);
                    if (isSuccess) {
                        if (page == 1 && !isReload) {
                            storeList = (ArrayList<StoreOfCategoryModel>) object;
                            if (storeList == null || storeList.size() == 0) {
                                showNodata();
                                noRemain = true;
                            } else
                                showData();
                        } else {
                            ArrayList<StoreOfCategoryModel> list = (ArrayList<StoreOfCategoryModel>) object;
                            if (isReload) {
                                storeList.clear();
                                mAdapter.notifyDataSetChanged();
                                isReload = false;
                                if (list == null || list.size() == 0) {
                                    showNodata();
                                    return;
                                }
                            }
                            if (storeList == null || list == null || list.size() == 0) {
                                noRemain = true;
                                return;
                            }
                            mBinding.swr.setVisibility(View.VISIBLE);
                            mBinding.rcvStore.setVisibility(View.VISIBLE);
                            mBinding.lnNoResult.setVisibility(View.GONE);
                            storeList.addAll(list);
                            mAdapter.notifyDataSetChanged();
                        }
                        page++;
                        isLoading = false;
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                    }

                }
            }, "", category_id, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean isRvBottom() {
        if (storeList == null || storeList.size() == 0)
            return false;
        int totalItemCount = storeList.size();
        int lastVisibleItem = ((LinearLayoutManager) mBinding.rcvStore.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        int record_per_page = DeliDataLoader.record_per_page;
        return (lastVisibleItem >= totalItemCount - 7 && storeList.size() >= record_per_page);
    }

    public void showNodata() {
        mBinding.rcvStore.setVisibility(View.GONE);
        mBinding.lnNoResult.setVisibility(View.VISIBLE);
    }

    public void showData() {
        mBinding.swr.setVisibility(View.VISIBLE);
        mBinding.rcvStore.setVisibility(View.VISIBLE);
        mBinding.lnNoResult.setVisibility(View.GONE);
        mAdapter = new PlaceAdapter(activity, storeList);
        mBinding.rcvStore.setLayoutManager(new GridLayoutManager(activity, 1));
//        mBinding.rcvStore.addItemDecoration(spacesItemDecoration);
        mBinding.rcvStore.setAdapter(mAdapter);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_CATEGORY;
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.ivBack) {
            finish();
        }
    }
}
