package com.opencheck.client.home.merchant.map;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StoreMapLayoutBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.FilterModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.SearchResultModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.models.user.CityDistrictModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class StoreMapFragment extends CustomGoogleMapFragment implements View.OnClickListener, View.OnFocusChangeListener, TextView.OnEditorActionListener {

    public static String SELECT_CITY_ACTION = "SELECT_CITY_ACTION";
    public static String SELECT_DISTRICT_ACTION = "SELECT_DISTRICT_ACTION";
    public static String SELECT_LOCATION_ACTION = "SELECT_LOCATION_ACTION";

    private View view;
    private SearchResultModel searchResultModel;
    private Activity activity;
    private LatLng locationResult;
    private OnDataPass dataPasser;
    ////////

    private RelativeLayout rlTop;
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private RelativeLayout rlMap;
    private Button btnSelectCity;
    private RelativeLayout rlFilter;
    private RelativeLayout rlCity;
    private TextView tvCity;
    private RelativeLayout rlDistrict;
    private TextView tvDistrict;
    private RelativeLayout rlCurrentLocation;
    private ImageView bt_navigator;

    private StoreLocationControllerDialog popupStoreLocationController;

    private ArrayList<StoreModel> listStore = new ArrayList<StoreModel>();
    private ArrayList<StoreModel> listTemp = new ArrayList<StoreModel>();

    private MerchantModel merchantModel;

    private FilterModel filterModel = new FilterModel();

    private ArrayList<CityDistrictModel> listCity = new ArrayList<CityDistrictModel>();
    private ArrayList<CityDistrictModel> listDistrict = new ArrayList<CityDistrictModel>();

    private StoreMapLayoutBinding mBinding;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mBinding = StoreMapLayoutBinding.inflate(inflater, container, false);
        this.view = mBinding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        this.activity = getActivity();
        if (isAdded()) {
            this.merchantModel = new Gson().fromJson(getArguments().getString(ConstantValue.MERCHANT_MODEL), MerchantModel.class);
            this.listStore = merchantModel.getListStore();
            this.filterModel.setCityModel(CityDistrictModel.getCityDefault(activity));
            this.filterModel.setDistrictModel(CityDistrictModel.getDistrictDefault(activity));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    initMap(view);
                }
            }, 1);
        }
    }

    private void initMap(final View view) {
        mVIew = view;

        mMapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapRoot));
        if (mMapFragment != null) {
            mMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    // setUpMapIfNeeded();

                    initView(view);
                    initData();

                    if (mMap == null) {
                        mMap = map;
                        initMarkerListener();
//                        mMap = mMapFragment.getMap();
                        if (mMap != null) {

                            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                mMap.setMyLocationEnabled(false);
                            } else {
//                                new PopupNotifyMessageController(activity, "You have to accept to enjoy all app's services!").show();
//                                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                                    mMap.setMyLocationEnabled(false);
//                                }
                            }
                            mMap.setMyLocationEnabled(false);
//                            listTemp.add(listStore.get(0));
                            setMapOverlay(listStore, itemClick);
                            setStoreFocus(listStore.get(0), 0);
                        } else {
                            Helper.showErrorDialog((LixiActivity) getActivity(), "Vui lòng cài đặt google play service trên thiết bị!");
                        }
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 25));
                        mMap.setMyLocationEnabled(false);
                    }
                }
            });
        } else {
            Helper.showErrorDialog((LixiActivity) getActivity(), "Không thể tải MAP!");
        }
    }

    private void initView(View view) {
        rlTop = (RelativeLayout) view.findViewById(R.id.rlTop);
        rlBack = (RelativeLayout) view.findViewById(R.id.rlBack);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        rlMap = (RelativeLayout) view.findViewById(R.id.rlMap);
        btnSelectCity = (Button) view.findViewById(R.id.btnSelectCity);
        bt_navigator = (ImageView) view.findViewById(R.id.bt_navigator);
        rlFilter = (RelativeLayout) view.findViewById(R.id.rlFilter);
        rlCity = (RelativeLayout) view.findViewById(R.id.rlCity);
        tvCity = (TextView) view.findViewById(R.id.tvCity);
        rlDistrict = (RelativeLayout) view.findViewById(R.id.rlDistrict);
        tvDistrict = (TextView) view.findViewById(R.id.tvDistrict);
        rlCurrentLocation = (RelativeLayout) view.findViewById(R.id.rlCurrentLocation);
    }

    private void initData() {
//        listCity = new RepoCityDistrict(activity).getAll(0);
        this.rlBack.setOnClickListener(this);
        this.btnSelectCity.setOnClickListener(this);
        this.rlCity.setOnClickListener(this);
        this.rlDistrict.setOnClickListener(this);
        rlCurrentLocation.setOnClickListener(this);
        bt_navigator.setOnClickListener(this);
        this.popupStoreLocationController = new StoreLocationControllerDialog(getActivity(), listStore, itemClick);
        if (listStore.size() > 1) {
            btnSelectCity.setVisibility(View.VISIBLE);
        } else {
            btnSelectCity.setVisibility(View.GONE);
        }
    }

    private void setStoreFocus(StoreModel storeModel, int position) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(storeModel.getLat()), Double.parseDouble(storeModel.getLng())), 11));
        focusStore(position);
    }

    private void getListResult(Boolean isLocation) {
        String lat = null, lon = null, city_id = null, district_id = null;

        if (isLocation) {
            lat = filterModel.getLat();
            lon = filterModel.getLng();
        } else {
            if (filterModel.getCityModel() != null) {
                if (!filterModel.getCityModel().getId().equalsIgnoreCase(CityDistrictModel.getCityDefault(activity).getId())) {
                    city_id = filterModel.getCityModel().getId();
                }
            }
            if (filterModel.getDistrictModel() != null) {
                if (!filterModel.getDistrictModel().getId().equalsIgnoreCase(CityDistrictModel.getDistrictDefault(activity).getId())) {
                    district_id = filterModel.getDistrictModel().getId();
                    tvDistrict.setText(filterModel.getDistrictModel().getName());
                }
            }
        }

        DataLoader.getStoreMapList((LixiActivity) activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listStore = (ArrayList<StoreModel>) object;
                    setMapOverlay(listStore, itemClick);
                } else {
                    Helper.showErrorDialog((LixiActivity) getActivity(), "Không có cửa hàng với địa điểm được chọn");
                }
            }
        }, merchantModel.getId(), lat, lon, city_id, district_id);
    }


    private void setCenterCamera(LatLng centerLocation) {
//        Helper.showProgressBar((LixiActivity) getActivity());
        ((LixiActivity) getActivity()).showProgressBar();
        LoadLocationAddress newTask = new LoadLocationAddress(new LocationCallBack() {
            @Override
            public void callback(LatLng position, ArrayList<String> address) {

            }
        });
        newTask.execute(centerLocation);
    }

    private Boolean isRun = false;

    @Override
    public void onResume() {
        super.onResume();
        this.isRun = true;

//        ((FiveMainActivity) getActivity()).sendNameScreen(ConstantValue.SCREEN_V2_DETAIL_MAP, true);
    }

    @Override
    public void onPause() {
        this.isRun = false;
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseMap();

    }

    private ItemClickListener itemClick = new ItemClickListener() {
        @Override
        public void onItemCLick(int pos, String action, final Object object, Long count) {
            if (action.equalsIgnoreCase(ConstantValue.MAP_MARKER_CLICK)) {
                setStoreFocus((StoreModel) object, pos);
            } else if (action.equalsIgnoreCase(ConstantValue.STORE_ADDRESS_CLICK_DIALOG)) {
                setStoreFocus((StoreModel) object, pos);
            } else if (action.equalsIgnoreCase(SELECT_CITY_ACTION)) {
                filterModel.setCityModel(listCity.get(pos));
                getListResult(false);
            } else if (action.equalsIgnoreCase(SELECT_DISTRICT_ACTION)) {
                filterModel.setDistrictModel(listDistrict.get(pos));
                getListResult(false);
            } else if (action.equalsIgnoreCase(SELECT_LOCATION_ACTION)) {

                StoreModel storeModel = listStore.get(pos);

                setStoreFocus(storeModel, pos);
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            getActivity().finish();
        } else if (v == btnSelectCity) {
            ArrayList<String> listItem = new ArrayList<String>();
            for (int i = 0; i < listStore.size(); i++) {
                listItem.add(listStore.get(i).getName());
            }
            StoreSelectionDialog dialog = new StoreSelectionDialog((LixiActivity) activity, false, true, false);
            dialog.show();
            dialog.setData(listItem, "Chọn chi nhánh", itemClick, SELECT_LOCATION_ACTION);

        } else if (v == rlCurrentLocation) {
//            Helper.showProgressBar((LixiActivity) getActivity());
            ((LixiActivity) getActivity()).showProgressBar();
            Helper.getGPSLocation(activity, new LocationCallBack() {
                @Override
                public void callback(LatLng position, ArrayList<String> address) {
//                    Helper.hideProgressBar();
                    ((LixiActivity) getActivity()).hideProgressBar();
                    if (position != null) {
                        filterModel.setLat(String.valueOf(position.latitude));
                        filterModel.setLng(String.valueOf(position.longitude));
                        moveToPoint(position, 11);
                        getListResult(true);
                    }
                }
            });
        } else if (v == bt_navigator) {

            Bundle bundle = new Bundle();
            bundle.putDouble("LAT", crLat);
            bundle.putDouble("LNG", crLng);
            this.locationBundle = bundle;
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_PERMISSON);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantValue.REQUEST_CODE_PERMISSON);
                }
            }
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_PERMISSON);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ConstantValue.REQUEST_CODE_PERMISSON);
                }
            }

            Helper.getGPSLocation(activity, new LocationCallBack() {
                @Override
                public void callback(LatLng position, ArrayList<String> address) {
                    float lat = (float) position.latitude;
                    float lng = (float) position.longitude;

                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + lat + "," + lng + "&daddr=" + crLat + "," + crLng));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
            });
        }
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
    }

    private Bundle locationBundle;

    public Bundle getLocation() {
        return locationBundle;
    }


}
