package com.opencheck.client.home.delivery.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;

import java.util.ArrayList;

public class GalleryAdapter extends PagerAdapter {

    private LixiActivity activity;
    private ArrayList<String> list;

    public GalleryAdapter(LixiActivity activity, ArrayList<String> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(activity)
                .inflate(R.layout.discovery_row_banner_fragment, container, false);
        ImageView imageView = view.findViewById(R.id.img_banner);
        ImageLoader.getInstance().displayImage(list.get(position), imageView, LixiApplication.getInstance().optionsNomal);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
