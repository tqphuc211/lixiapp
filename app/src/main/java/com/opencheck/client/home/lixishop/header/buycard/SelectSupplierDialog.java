package com.opencheck.client.home.lixishop.header.buycard;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SelectSuplierDialogBinding;
import com.opencheck.client.models.merchant.PaymentPartnerModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class SelectSupplierDialog extends LixiDialog {

    public SelectSupplierDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;
    private RecyclerView rv_suplier;

    private SelectSuplierDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = SelectSuplierDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
        initDialogEvent();
    }


    private void findViews() {
        rv_suplier = (RecyclerView) findViewById(R.id.rv_suplier);
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_suplier.setLayoutManager(mLayoutManager);
    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    public void setData(List<PaymentPartnerModel> list, int crSelected) {
        PaymentSuplierAdapter adapter = new PaymentSuplierAdapter(list, activity);
        adapter.setCurrentSelected(crSelected);
        rv_suplier.setAdapter(adapter);

        adapter.setOnClickListener(new PaymentSuplierAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                if (eventListener != null)
                    eventListener.onSelectItem(position);
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_dismiss:
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {
        public void onSelectItem(int pos);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
