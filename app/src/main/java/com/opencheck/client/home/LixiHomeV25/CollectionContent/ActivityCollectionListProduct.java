package com.opencheck.client.home.LixiHomeV25.CollectionContent;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.FlingBehavior;
import com.opencheck.client.databinding.LixiShopListProductActivityBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vutha_000 on 3/8/2018.
 */

public class ActivityCollectionListProduct extends LixiActivity {

    public static String EXTRA_COLLECTION_ID = "EXTRA_COLLECTION_ID";
    public static String EXTRA_COLLECTION_NAME = "EXTRA_COLLECTION_NAME";
    public static String EXTRA_COLLECTION_BANNER = "EXTRA_COLLECTION_BANNER";

    public static void startActivityCollectionListProduct(LixiActivity activity, int collectionId, String name, String banner) {
        Intent intent = new Intent(activity, ActivityCollectionListProduct.class);
        intent.putExtra(EXTRA_COLLECTION_ID, collectionId);
        intent.putExtra(EXTRA_COLLECTION_NAME, name);
        intent.putExtra(EXTRA_COLLECTION_BANNER, banner);
        activity.startActivity(intent);
    }

    //region Layout Variable
    private RelativeLayout rl_back;
    private LinearLayoutManager rc_layout_manager;
    private TextView tv_title;
    private ImageView iv_back, iv_banner;
    private Toolbar toolbar;
    private SwipeRefreshLayout swr;
    private RecyclerView rv;
    private TextView tv_empty;
    private AppBarLayout appbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    //endregion

    private AdapterCollectionProduct adapter;

    private Long collectionId;
    private String collectionName;
    private String collectionBanner;

    int pageSize = 20;
    int page = 1;
    int record_per_page = DataLoader.record_per_page;
    boolean isLoading = false;
    boolean noRemain = false;
    private int height = 0;

    LixiShopListProductActivityBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.lixi_shop_list_product_activity);
        collectionId = (long) getIntent().getIntExtra(EXTRA_COLLECTION_ID, 0);
        collectionName = getIntent().getStringExtra(EXTRA_COLLECTION_NAME);
        collectionBanner = getIntent().getStringExtra(EXTRA_COLLECTION_BANNER);
        activity.id = collectionId;
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.VOUCHER_COLLECTION,
                collectionId,
                TrackingConstant.Currency.VND,
                0,
                TrackingConstant.ContentSource.HOME
        );

        findView();
        initView();
        startLoadData();
    }

    private void findView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        rl_back = findViewById(R.id.rl_back);
        tv_empty = findViewById(R.id.tv_empty);
        iv_banner = findViewById(R.id.iv_banner);
        swr = findViewById(R.id.swr);
        rv = findViewById(R.id.rv);
        collapsingToolbarLayout = findViewById(R.id.collapsing);
        toolbar = findViewById(R.id.toolbar);
        appbar = findViewById(R.id.appbar);
        height = toolbar.getHeight();
        rc_layout_manager = new LinearLayoutManager(ActivityCollectionListProduct.this);
        rv.setLayoutManager(rc_layout_manager);
        setSupportActionBar(toolbar);
        rl_back.setOnClickListener(this);
    }

    public void initView() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        params.setBehavior(new FlingBehavior(getBaseContext(), null));

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
//        rv.setLayoutManager(gridLayoutManager);
        rv.setLayoutManager(new LinearLayoutManager(activity));

        adapter = new AdapterCollectionProduct(activity, null);
        rv.setAdapter(adapter);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrollDy = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollDy += dy;
                if (isLoading || noRemain)
                    return;

                if (isRvBottom())
                    startLoadData();
            }

        });

        swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                noRemain = false;
                startLoadData();
            }
        });

        ImageLoader.getInstance().displayImage(collectionBanner, iv_banner, LixiApplication.getInstance().optionsNomal);

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());
                iv_banner.setAlpha(1 - (offsetAlpha * -1));
                Log.d("ddd", verticalOffset + " " + appBarLayout.getTotalScrollRange());
                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    tv_title.setText(collectionName);
                    tv_title.setVisibility(View.VISIBLE);
                    iv_back.setImageResource(R.drawable.btn_back_icon);
                    collapsingToolbarLayout.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));
                } else {
                    //expands
                    tv_title.setVisibility(View.GONE);
                    iv_back.setImageResource(R.drawable.icon_back_white);
                    collapsingToolbarLayout.setScrimVisibleHeightTrigger(5);
                }
            }
        });
    }

    public boolean isRvBottom() {
        if (adapter.getListCode() == null || adapter.getListCode().size() == 0)
            return false;
        int totalItemCount = adapter.getListCode().size();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7 && adapter.getListCode().size() >= record_per_page);
    }

    public void showNodata() {
        swr.setVisibility(View.GONE);
        rv.setVisibility(View.GONE);
        tv_empty.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private boolean isReload = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                onBackPressed();
                break;
        }
    }


    public void resetList() {
        if (adapter == null || adapter.getListCode() == null) return;
        adapter.getListCode().clear();
        adapter.notifyDataSetChanged();
    }

    public void startLoadData() {
        isLoading = true;
        swr.setRefreshing(true);

        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getCollectionEvoucher(page, pageSize, collectionId.intValue());

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                isLoading = false;
                swr.setRefreshing(false);
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        adapter.setListCode(result.getRecords());
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
