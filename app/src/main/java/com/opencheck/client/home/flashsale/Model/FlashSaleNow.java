package com.opencheck.client.home.flashsale.Model;

import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FlashSaleNow {

    /**
     * end_time : 0
     * id : 0
     * name : string
     * products : [{"cashback_price":0,"discount_price":0,"id":0,"name":"string","payment_discount":0,"payment_discount_price":0,"payment_price":0,"price":0,"product_cover_image":"string","product_image":["string"],"product_image_medium":["string"],"product_logo":"string","product_type":"string","quantity":0,"quantity_limit":0,"quantity_order_done":0,"reminder_count":0,"state":"string"}]
     * start_time : 0
     * state : string
     */

    private long end_time;
    private int id;
    private String name;
    private long start_time;
    private String state;
    private ArrayList<ProductsBean> products;

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<ProductsBean> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductsBean> products) {
        this.products = products;
    }

    public static class ProductsBean {
        /**
         * cashback_price : 0
         * discount_price : 0
         * id : 0
         * name : string
         * payment_discount : 0
         * payment_discount_price : 0
         * payment_price : 0
         * price : 0
         * product_cover_image : string
         * product_image : ["string"]
         * product_image_medium : ["string"]
         * product_logo : string
         * product_type : string
         * quantity : 0
         * quantity_limit : 0
         * quantity_order_done : 0
         * reminder_count : 0
         * state : string
         */

        private int cashback_price;
        private int discount_price;
        private int id;
        private String name;
        private int payment_discount;
        private int payment_discount_price;
        private int payment_price;
        private int price;
        private String product_cover_image;
        private String product_logo;
        private String product_type;
        private int quantity;
        private int quantity_limit;
        private int quantity_order_done;
        private int reminder_count;
        private String state;
        private List<String> product_image;
        private List<String> product_image_medium;

        public int getCashback_price() {
            return cashback_price;
        }

        public void setCashback_price(int cashback_price) {
            this.cashback_price = cashback_price;
        }

        public int getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(int discount_price) {
            this.discount_price = discount_price;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPayment_discount() {
            return payment_discount;
        }

        public void setPayment_discount(int payment_discount) {
            this.payment_discount = payment_discount;
        }

        public int getPayment_discount_price() {
            return payment_discount_price;
        }

        public void setPayment_discount_price(int payment_discount_price) {
            this.payment_discount_price = payment_discount_price;
        }

        public int getPayment_price() {
            return payment_price;
        }

        public void setPayment_price(int payment_price) {
            this.payment_price = payment_price;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getProduct_cover_image() {
            return product_cover_image;
        }

        public void setProduct_cover_image(String product_cover_image) {
            this.product_cover_image = product_cover_image;
        }

        public String getProduct_logo() {
            return product_logo;
        }

        public void setProduct_logo(String product_logo) {
            this.product_logo = product_logo;
        }

        public String getProduct_type() {
            return product_type;
        }

        public void setProduct_type(String product_type) {
            this.product_type = product_type;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getQuantity_limit() {
            return quantity_limit;
        }

        public void setQuantity_limit(int quantity_limit) {
            this.quantity_limit = quantity_limit;
        }

        public int getQuantity_order_done() {
            return quantity_order_done;
        }

        public void setQuantity_order_done(int quantity_order_done) {
            this.quantity_order_done = quantity_order_done;
        }

        public int getReminder_count() {
            return reminder_count;
        }

        public void setReminder_count(int reminder_count) {
            this.reminder_count = reminder_count;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public List<String> getProduct_image() {
            if (product_image == null || product_image.size() == 0){
                return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
            }
            return product_image;
        }

        public void setProduct_image(List<String> product_image) {
            this.product_image = product_image;
        }

        public List<String> getProduct_image_medium() {
            if (product_image_medium == null || product_image_medium.size() == 0){
                return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
            }
            return product_image_medium;
        }

        public void setProduct_image_medium(List<String> product_image_medium) {
            this.product_image_medium = product_image_medium;
        }
    }
}
