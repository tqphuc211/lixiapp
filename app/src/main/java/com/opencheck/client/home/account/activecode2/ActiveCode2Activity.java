package com.opencheck.client.home.account.activecode2;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.home.account.activecode2.fragment.NearestStoreFragment;
import com.opencheck.client.home.account.activecode2.fragment.ScannerQRFragment;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

public class ActiveCode2Activity extends LixiActivity {

    public FrameLayout fr_main;
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.active_code_2_activity);
       // boolean is_scanner = getIntent().getExtras().getBoolean("is_to_scanner", false);
        initUI();
//        if(is_scanner) {
//            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
//                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
//                } else {
//                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
//                }
//            } else  {
//                startScannerFragment();
//            }
//
//        }else
            startNearestStoreFragment();
    }

    private void startScannerFragment() {
        ScannerQRFragment fragment = new ScannerQRFragment();
        replaceFragment(fragment);
    }

    public void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private NearestStoreFragment nearestStoreFragment = new NearestStoreFragment();

    private void startNearestStoreFragment() {
        replaceFragment(nearestStoreFragment);
    }

    private void initUI() {
        fr_main = findViewById(R.id.fr_main);
        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void replaceFragment (LixiFragment fragment){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped || manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fr_main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }

    public LixiFragment getCurrentFragment() {
        FragmentManager manager = getSupportFragmentManager();
        int latesIndex = manager.getBackStackEntryCount() - 1;
        if (latesIndex < 0)
            return null;
        return (LixiFragment) manager.findFragmentByTag(manager.getBackStackEntryAt(latesIndex).getName());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_CAMERA_QR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ScannerQRFragment fragment = new ScannerQRFragment();
                    replaceFragment(fragment);
                } else {
                    Helper.showErrorDialog(activity, "Xin quyền không thành công!");
                }
                break;
            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
                nearestStoreFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1)
            finish();
        else
            super.onBackPressed();
    }
}
