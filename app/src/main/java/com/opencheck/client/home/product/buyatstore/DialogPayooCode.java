package com.opencheck.client.home.product.buyatstore;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiEvoucherPayooCodeDialogBinding;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class DialogPayooCode extends LixiDialog {

    public DialogPayooCode(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

//    @Override
//    public String getDialogName() {
//        return "Cash voucher payment offline-success-" + idVoucher;
//    }
//
//    @Override
//    public String getPreDialogName() {
//        return null;
//    }

    private RelativeLayout rl_dismiss;
    private LinearLayout ll_root_popup_menu;
    private TextView tv_code;
    private TextView tv_message;
    private TextView tv_close;
    private TextView tv_shop;
    private String idVoucher = "";
    private TextView tv_timeout;

    SpannableStringBuilder spanMessage;

    private LixiEvoucherPayooCodeDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiEvoucherPayooCodeDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    public void setData(String code, String idVoucher) {
        tv_code.setText(code);
        this.idVoucher = idVoucher;
        tv_timeout.setText(getTimeOut());
        //trackScreen();
    }

    private void findViews() {
        rl_dismiss = findViewById(R.id.rl_dismiss);
        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_close = (TextView) findViewById(R.id.tv_close);
        tv_shop = (TextView) findViewById(R.id.tv_shop);
        tv_timeout = (TextView) findViewById(R.id.tv_timeout);

        rl_dismiss.setOnClickListener(this);
        tv_close.setOnClickListener(this);
        tv_shop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_dismiss:
            case R.id.tv_close:
                dismiss();
                break;
            case R.id.tv_shop:
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://payoo.vn/map/public/?verify=true")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    public String getTimeOut() {
        int timeout = ConfigModel.getInstance(activity).getOrder_payment_expired_at_store();
        int h = 0;
        int d = 0;
        int m = 0;
        if (timeout > 1440) {
            d = timeout / 1440;
            h = timeout % 1440;
            m = h % 60;
            h = h / 60;
        } else {
            h = timeout / 60;
            m = timeout % 60;
        }

        String str = "";
        if (d > 0)
            str += " " + d + " " + LanguageBinding.getString(R.string.day, activity);
        if (h > 0)
            str += " " + h + " " + LanguageBinding.getString(R.string.hour, activity);
        if (m > 0)
            str += " " + m + " " + LanguageBinding.getString(R.string.minute, activity);
        return str;
    }
}
