package com.opencheck.client.home.account.new_layout.fragment.bought_evoucher;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentInvalidEvoucherBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.adapter.ListEvoucherAdapter;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;
import java.util.List;

public class InvalidEvoucherFragment extends LixiFragment {

    // View
    View view;
    RecyclerView recListEvoucher;
    SwipeRefreshLayout swipeRefresh;
    LinearLayout linearNoResult;

    // Var
    private long total = 0;
    private int page = 1;
    private int record_per_page = DataLoader.record_per_page;
    private Boolean isLoading = false;
    private boolean isOutOfVoucherPage = false;

    // Adapter
    ListEvoucherAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<LixiShopEvoucherBoughtModel> listEvoucher;

    private FragmentInvalidEvoucherBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentInvalidEvoucherBinding.inflate(inflater, container, false);
        view = mBinding.getRoot();
        initView();
        setAdapter();
        getVoucherList();
        return view;
    }

    private void initView(){
        recListEvoucher = (RecyclerView) view.findViewById(R.id.recListEvoucher);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        linearNoResult = (LinearLayout) view.findViewById(R.id.linearNoResult);

        listEvoucher = new ArrayList<>();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                listEvoucher.clear();
                getVoucherList();
            }
        });

        recListEvoucher.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recListEvoucher.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount  = linearLayoutManager.getChildCount();
                    int totalItemCount    = linearLayoutManager.getItemCount();
                    int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 3) {
                        Helper.showLog("Load more");
                        getVoucherList();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void setAdapter(){
        adapter = new ListEvoucherAdapter(activity, listEvoucher);
        linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recListEvoucher.setLayoutManager(linearLayoutManager);
        recListEvoucher.setAdapter(adapter);
        adapter.addOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                if (onItemClickListener != null){
                    onItemClickListener.onItemClickListener(view, pos, action, object, total);
                }
            }
        });
    }

    private OnItemClickListener onItemClickListener;
    public void addOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    private void getVoucherList() {
        if (!isLoading) {
            if (!isOutOfVoucherPage) {
                swipeRefresh.setRefreshing(true);
                DataLoader.getBoughtEVoucher(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            ApiWrapperForListModel<LixiShopEvoucherBoughtModel> oj = (ApiWrapperForListModel<LixiShopEvoucherBoughtModel>) object;
                            total = oj.get_meta().getTotal();
                            if (page == 1) {
                                listEvoucher = oj.getRecords();
                                setOutOfVoucherPage(listEvoucher);
                                setAdapter();

                            } else {
                                List<LixiShopEvoucherBoughtModel> list = oj.getRecords();
                                if (list == null || list.size() == 0 || list == null) {
                                    swipeRefresh.setRefreshing(false);
                                    return;
                                } else {
                                    setOutOfVoucherPage(list);
                                    listEvoucher.addAll(list);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            page++;
                            isLoading = false;
                            swipeRefresh.setRefreshing(false);
                        } else {
                            isLoading = true;
                        }
                        setStateResult();
                    }
                }, page, "unavailable");
                return;
            }
        }
        setStateResult();
    }

    private void setOutOfVoucherPage(List<LixiShopEvoucherBoughtModel> list) {
//        if (list.size() < record_per_page) {
//            isOutOfVoucherPage = true;
//        }
    }

    private void setStateResult(){
        swipeRefresh.setRefreshing(false);
        if (listEvoucher.size() == 0){
            linearNoResult.setVisibility(View.VISIBLE);
        }else {
            linearNoResult.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

    }
}
