package com.opencheck.client.home.account.new_layout.model;

import com.opencheck.client.models.LixiModel;

public class HistoryIntroModel extends LixiModel {

    /**
     * create_date : 0
     * id : 0
     * object_id : string
     * object_type : string
     * state : string
     * user : 0
     * user_bonus : 0
     * user_ref : 0
     * user_ref_bonus : 0
     * user_ref_info : {"avatar":"string","name":"string"}
     * write_date : 0
     */

    private long create_date;
    private int id;
    private String object_id;
    private String object_type;
    private String state;
    private int user;
    private int user_bonus;
    private int user_ref;
    private int user_ref_bonus;
    private UserRefInfo user_ref_info;
    private long write_date;

    public long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(long create_date) {
        this.create_date = create_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObject_id() {
        return object_id;
    }

    public void setObject_id(String object_id) {
        this.object_id = object_id;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getUser_bonus() {
        return user_bonus;
    }

    public void setUser_bonus(int user_bonus) {
        this.user_bonus = user_bonus;
    }

    public int getUser_ref() {
        return user_ref;
    }

    public void setUser_ref(int user_ref) {
        this.user_ref = user_ref;
    }

    public int getUser_ref_bonus() {
        return user_ref_bonus;
    }

    public void setUser_ref_bonus(int user_ref_bonus) {
        this.user_ref_bonus = user_ref_bonus;
    }

    public UserRefInfo getUser_ref_info() {
        return user_ref_info;
    }

    public void setUser_ref_info(UserRefInfo user_ref_info) {
        this.user_ref_info = user_ref_info;
    }

    public long getWrite_date() {
        return write_date;
    }

    public void setWrite_date(long write_date) {
        this.write_date = write_date;
    }
}
