package com.opencheck.client.home.account.evoucher.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogInvalidLixiCodeBinding;

public class InvalidLiXiCodeDialog extends LixiDialog {

    private TextView txtContent, txtDone;
    private Activity activity;
    private String merName;

    public InvalidLiXiCodeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar, String merName) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
        this.activity = _activity;
        this.merName = merName;
    }

    private DialogInvalidLixiCodeBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogInvalidLixiCodeBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        setCanceledOnTouchOutside(false);
        txtContent = (TextView) findViewById(R.id.txt_content);
        txtDone = (TextView) findViewById(R.id.txt_done);

        txtContent.setText(activity.getString(R.string.code_message) + " " + merName + " " + activity.getString(R.string.invalid_code_message));
        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvalidLiXiCodeDialog.this.cancel();
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
