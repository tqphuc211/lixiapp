package com.opencheck.client.home.product.cod;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentCodSuccessLayoutBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodModel;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class PaymentCODSuccessDialog extends LixiTrackingDialog {

    public PaymentCODSuccessDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private String supportPhone;

    private OnPaymentCodClickListener onPaymentCodClickListener;
    private PaymentCodSuccessLayoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PaymentCodSuccessLayoutBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        setUpView();
    }

    private void checkFirstTimeUsingCod(String phoneNumber) {
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
        if (appPreferenceHelper.getIsFirstUseCodPayment()) {
            appPreferenceHelper.setIsFirstUseCODPayment(false);
            FirstUseCODPaymentDialog firstUseCODPaymentDialog =
                    new FirstUseCODPaymentDialog(activity, false, true, false);
            firstUseCODPaymentDialog.show();
            firstUseCODPaymentDialog.setData(phoneNumber);
        }
    }

    private void setUpView() {
        mBinding.tvDone.setOnClickListener(this);
        mBinding.tvCall.setOnClickListener(this);
    }

    public void setData(PaymentCodModel paymentCodModel,
                        ProductCashVoucherDetailModel lixiShopProductDetailModel,
                        long totalPrice,
                        long lixiDiscount,
                        long totalLixiBonus,
                        String phoneNumber) {
        long quantity = paymentCodModel.getQuantity();
        String discount = "-" + Helper.getVNCurrency(lixiDiscount) + "đ";
        String calculatePrice = Helper.getVNCurrency(totalPrice * quantity) + "đ";
        String productPrice = Helper.getVNCurrency(totalPrice) + "đ";
        String price = Helper.getVNCurrency(totalPrice * quantity - lixiDiscount) + "đ";
        String expiredDate = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME,
                String.valueOf(paymentCodModel.getPayment_expired() * 1000));

        this.supportPhone = Helper.getPhoneSupport(activity);
        String callHotLine = LanguageBinding.getString(R.string.call_hot_line, activity) + " " + this.supportPhone;

        renderLixiCashBack(totalLixiBonus * quantity);
        mBinding.tvLixiUse.setText(discount);
        mBinding.tvCalculatePrice.setText(calculatePrice);
        mBinding.tvProductName.setText(lixiShopProductDetailModel.getName());
        mBinding.tvProductPrice.setText(productPrice);
        String totalProduct = "x" + quantity;
        mBinding.totalProduct.setText(totalProduct);
        mBinding.tvFullPrice.setText(price);
        mBinding.tvId.setText(paymentCodModel.getBilling_code());
        mBinding.tvExpiredDate.setText(expiredDate);
        mBinding.tvCall.setText(callHotLine);
        checkFirstTimeUsingCod(phoneNumber);

        id = paymentCodModel.getId();
        trackScreen();

    }

    private void renderLixiCashBack(long price) {
        if (price > 0) {
            mBinding.groupCashBack.setVisibility(View.VISIBLE);
            String lixiCashBack = "+" + Helper.getVNCurrency(price) + " Lixi";
            mBinding.tvLixiCashBackPrice.setText(lixiCashBack);
        } else {
            mBinding.groupCashBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDone: {
//                if (onPaymentCodClickListener != null) {
//                    onPaymentCodClickListener.onDone();
//                }
//                dismiss();

                Intent intent = new Intent(activity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                break;
            }
            case R.id.tvCall: {
                //Action call
                String phone = supportPhone.replace(" ", "").trim();
                Helper.openDial(activity, phone);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (onPaymentCodClickListener != null) {
                onPaymentCodClickListener.onDone();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public interface OnPaymentCodClickListener {

        void onDone();
    }

    public void setListener(OnPaymentCodClickListener listener) {
        onPaymentCodClickListener = listener;
    }
}
