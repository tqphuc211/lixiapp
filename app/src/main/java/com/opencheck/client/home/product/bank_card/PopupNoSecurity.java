package com.opencheck.client.home.product.bank_card;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PopupNoSecurityBinding;
import com.opencheck.client.home.product.communicate.OnSettingListener;

public class PopupNoSecurity extends LixiDialog {
    public static final int SETTING = 0;
    public static final int SKIP = 1;
    public static final int CANCEL = 2;

    public PopupNoSecurity(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private TextView txtNo, txtDelete;

    private PopupNoSecurityBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PopupNoSecurityBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        initView();

        txtDelete.setOnClickListener(this);
        txtNo.setOnClickListener(this);
    }

    private void initView(){
        txtNo = (TextView) findViewById(R.id.txtNo);
        txtDelete = (TextView) findViewById(R.id.txtSetting);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtNo:
                if (onSettingListener != null){
                    onSettingListener.onSetting(SKIP);
                    cancel();
                }
                break;
            case R.id.txtSetting:
                if (onSettingListener != null){
                    onSettingListener.onSetting(SETTING);
                    cancel();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        onSettingListener.onSetting(CANCEL);
        super.onBackPressed();
    }

    public void setSettingEventListener(OnSettingListener onSettingListener) {
        this.onSettingListener = onSettingListener;
    }

    private OnSettingListener onSettingListener;
}
