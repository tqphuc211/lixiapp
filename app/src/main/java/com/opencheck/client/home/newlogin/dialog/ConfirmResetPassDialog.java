package com.opencheck.client.home.newlogin.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogConfirmResetPassBinding;
import com.opencheck.client.home.newlogin.communicate.OnAgreeUpdate;
import com.opencheck.client.utils.LanguageBinding;

public class ConfirmResetPassDialog extends LixiDialog {

    public ConfirmResetPassDialog(@NonNull LixiActivity _activity) {
        super(_activity,  false, true, false);
    }

    private DialogConfirmResetPassBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogConfirmResetPassBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        mBinding.txtCancel.setOnClickListener(this);
        mBinding.txtDone.setOnClickListener(this);
    }

    private OnAgreeUpdate onAgreeUpdate;
    public void setOnAgreeUpdate(OnAgreeUpdate onAgreeUpdate){
        this.onAgreeUpdate = onAgreeUpdate;
    }

    public void setData(String phone){
        String content = String.format(LanguageBinding.getString(R.string.login_reset_pass_content, activity), phone);
        mBinding.txtContent.setText(content);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtCancel:
                cancel();
                break;
            case R.id.txtDone:
                if (onAgreeUpdate != null){
                    onAgreeUpdate.onAgree();
                }
                cancel();
                break;
        }
    }
}
