package com.opencheck.client.home.flashsale.Global;

import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FlashSaleProductInfoBinding;
import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

public class FlashSaleDetailController {

    //region FACTORY
    public static final String BUY_NOW = "BUY_NOW";

    public static String getParamBuyNow(boolean isbuyNow) {
        return BUY_NOW + ":" + (isbuyNow ? "1" : "0");
    }
    //endregion

    LixiActivity activity;
    View rootView;
    LinearLayout parantView;
    FlashSaleInfo flashSaleInfo;

    private CountDownTimer timer;

    public FlashSaleDetailController(LixiActivity activity, LinearLayout parantView, FlashSaleInfo flashSaleInfo) {
        this.activity = activity;
        this.parantView = parantView;
        this.flashSaleInfo = flashSaleInfo;
        FlashSaleProductInfoBinding productInfoBinding =
                FlashSaleProductInfoBinding.inflate(LayoutInflater.from(activity), null, false);
        rootView = productInfoBinding.getRoot();
        parantView.addView(rootView);
        findViews();
        showData();
    }

    public void showData() {

        tv_price.setText(Helper.getVNCurrency(flashSaleInfo.getPayment_price()) + "đ");
        tv_price.setPaintFlags(tv_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tv_sale_price.setText(Helper.getVNCurrency(flashSaleInfo.getPayment_discount_price()) + "đ");

        tv_saled.setText(flashSaleInfo.getQuantity_order_done() + "");

        if (flashSaleInfo.getPayment_discount_price() != flashSaleInfo.getPayment_price()) {
            tv_price.setVisibility(View.VISIBLE);
        } else
            tv_price.setVisibility(View.GONE);

        if (flashSaleInfo.getCashback_price() > 0) {
//            ctl_lixi_cashback.setVisibility(View.VISIBLE);
            tv_lixi_cashback.setText(Helper.getVNCurrency(flashSaleInfo.getCashback_price()) + "Lixi");
        } else
            ctl_lixi_cashback.setVisibility(View.GONE);

        startTimer((flashSaleInfo.getEnd_time() - flashSaleInfo.getServer_current()) * 1000
                - (System.currentTimeMillis() - flashSaleInfo.getMy_start_time()));
    }

    //region timer
    public void startTimer(long millisSecond) {
        cancelTimer();
        if (millisSecond <= 0) {
            tv_hours.setText(formatTimer(0));
            tv_minute.setText(formatTimer(0));
            tv_second.setText(formatTimer(0));
            return;
        }

        timer = new CountDownTimer(millisSecond, 1000) {
            @Override
            public void onTick(long l) {
                int[] arrTime = getTimer(l);
                tv_hours.setText(formatTimer(arrTime[0]));
                tv_minute.setText(formatTimer(arrTime[1]));
                tv_second.setText(formatTimer(arrTime[2]));
                if (eventListener != null)
                    eventListener.onTick(arrTime);
            }

            @Override
            public void onFinish() {
                tv_hours.setText(formatTimer(0));
                tv_minute.setText(formatTimer(0));
                tv_second.setText(formatTimer(0));
                if (eventListener != null) {
                    eventListener.onFinish();
                }
            }
        }.start();
    }

    private int[] getTimer(long millisSecond) {
        long second = millisSecond / 1000;
        int h, m, s;
        h = (int) (second / 3600);
        m = (int) (second % 3600) / 60;
        s = (int) (second % 3600) % 60;
        return new int[]{h, m, s};
    }

    public static String formatTimer(int numb) {
        String result = "";
        if (numb < 10) {
            result = "0" + numb;
        } else {
            result = numb + "";
        }
        return result;
    }

    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }
    //endregion

    public interface FlashSaleEvent {
        void onFinish();

        void onTick(int[] arrTime);
    }

    public void setiFlashSaleEvent(FlashSaleEvent iFlashSaleEvent) {
        this.eventListener = iFlashSaleEvent;
    }

    private FlashSaleEvent eventListener;


    private ConstraintLayout ctl_price;
    private TextView textView2;
    private LinearLayout ll_timer;
    private TextView tv_hours;
    private TextView tv_minute;
    private TextView tv_second;
    private ConstraintLayout constraintPrice;
    private LinearLayout linearLayout6;
    private TextView tv_price;
    private TextView tv_sale_price;
    private TextView textView4;
    private TextView tv_saled;
    private ConstraintLayout ctl_lixi_cashback;
    private LinearLayout ll_lixi_cashback;
    private TextView tv_lixi_cashback;
    private TextView tv_guild_lixi;
    private TextView tv_show_detail;

    private void findViews() {
        ctl_price = (ConstraintLayout) rootView.findViewById(R.id.ctl_price);
        textView2 = (TextView) rootView.findViewById(R.id.textView2);
        ll_timer = (LinearLayout) rootView.findViewById(R.id.ll_timer);
        tv_hours = (TextView) rootView.findViewById(R.id.tv_hours);
        tv_minute = (TextView) rootView.findViewById(R.id.tv_minute);
        tv_second = (TextView) rootView.findViewById(R.id.tv_second);
        constraintPrice = (ConstraintLayout) rootView.findViewById(R.id.constraintPrice);
        linearLayout6 = (LinearLayout) rootView.findViewById(R.id.linearLayout6);
        tv_price = (TextView) rootView.findViewById(R.id.tv_price);
        tv_sale_price = (TextView) rootView.findViewById(R.id.tv_sale_price);
        textView4 = (TextView) rootView.findViewById(R.id.textView4);
        tv_saled = (TextView) rootView.findViewById(R.id.tv_saled);
        ctl_lixi_cashback = (ConstraintLayout) rootView.findViewById(R.id.ctl_lixi_cashback);
        ll_lixi_cashback = (LinearLayout) rootView.findViewById(R.id.ll_lixi_cashback);
        tv_lixi_cashback = (TextView) rootView.findViewById(R.id.tv_lixi_cashback);
        tv_guild_lixi = (TextView) rootView.findViewById(R.id.tv_guild_lixi);
        tv_show_detail = (TextView) rootView.findViewById(R.id.tv_show_detail);
    }

}
