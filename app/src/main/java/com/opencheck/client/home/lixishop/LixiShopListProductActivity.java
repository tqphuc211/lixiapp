package com.opencheck.client.home.lixishop;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.FlingBehavior;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.lixishop.LixiProductModel;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/8/2018.
 */

public class LixiShopListProductActivity extends LixiActivity {

    //region Layout Variable
    private RelativeLayout rl_back;
    private LinearLayoutManager rc_layout_manager;
    private TextView tv_title;
    private ImageView iv_back, iv_banner;
    private Toolbar toolbar;
    private SwipeRefreshLayout swr;
    private RecyclerView rv;
    private View scrim;
    private TextView tv_empty;
    private AppBarLayout appbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private LinearLayout ll_root;
    private CoordinatorLayout coordinate;
    //endregion


    private ArrayList<LixiProductModel> listProduct;
    private AdapterProduct adapter;

    int page = 1;
    int record_per_page = DataLoader.record_per_page;
    boolean isLoading = false;
    boolean noRemain = false;
    private int height = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.lixi_shop_list_product_activity);
        ImageUrl = getIntent().getStringExtra("ImageUrl");
        Category = getIntent().getStringExtra("Category");
        Name = getIntent().getStringExtra("Name");
        activity.id = Long.valueOf(Category);
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.VOUCHER_COLLECTION,
                activity.id,
                TrackingConstant.Currency.VND,
                0,
                TrackingConstant.ContentSource.VOUCHER_CATEGORY
        );
        findView();
        initView();
        startLoadData();
    }

    private void findView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        rl_back = findViewById(R.id.rl_back);
        tv_empty = findViewById(R.id.tv_empty);
        iv_banner = findViewById(R.id.iv_banner);
        swr = findViewById(R.id.swr);
        rv = findViewById(R.id.rv);
        collapsingToolbarLayout = findViewById(R.id.collapsing);
        toolbar = findViewById(R.id.toolbar);
        appbar = findViewById(R.id.appbar);
        ll_root = findViewById(R.id.ll_root);
        coordinate = findViewById(R.id.coordinate);
        scrim = findViewById(R.id.scrim);
        height = toolbar.getHeight();
        rc_layout_manager = new LinearLayoutManager(LixiShopListProductActivity.this);
        rv.setLayoutManager(rc_layout_manager);
        setSupportActionBar(toolbar);
        rl_back.setOnClickListener(this);
    }

    public void initView() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        params.setBehavior(new FlingBehavior(getBaseContext(), null));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(gridLayoutManager);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrollDy = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollDy += dy;
                if (isLoading || noRemain)
                    return;

                if (isRvBottom())
                    startLoadData();
            }

        });

        swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listProduct = null;
                adapter.notifyDataSetChanged();
                page = 1;
                isLoading = false;
                noRemain = false;
                startLoadData();
            }
        });

        ImageLoader.getInstance().displayImage(ImageUrl, iv_banner, LixiApplication.getInstance().optionsNomal);

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());
                iv_banner.setAlpha(1 - (offsetAlpha * -1));
                Log.d("ddd", verticalOffset + " " + appBarLayout.getTotalScrollRange());
                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    tv_title.setText(Name);
                    tv_title.setVisibility(View.VISIBLE);
                    iv_back.setImageResource(R.drawable.btn_back_icon);
                    collapsingToolbarLayout.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));
                } else {
                    //expands
                    tv_title.setVisibility(View.GONE);
                    iv_back.setImageResource(R.drawable.icon_back_white);
                    collapsingToolbarLayout.setScrimVisibleHeightTrigger(5);
                }
            }
        });
    }


    public boolean isRvBottom() {
        if (listProduct == null || listProduct.size() == 0)
            return false;
        int totalItemCount = listProduct.size();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7 && listProduct.size() >= record_per_page);
    }


    public void showData() {
        swr.setVisibility(View.VISIBLE);
        rv.setVisibility(View.VISIBLE);
        tv_empty.setVisibility(View.GONE);
        adapter = new AdapterProduct(this, listProduct, R.layout.lixi_shop_product_item);
        rv.setAdapter(adapter);
    }

    public void showNodata() {
        swr.setVisibility(View.GONE);
        rv.setVisibility(View.GONE);
        tv_empty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private String Category, ImageUrl, Name;

    private boolean isReload = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                onBackPressed();
                break;
        }
    }

    public void startLoadData() {
        isLoading = true;
        swr.setRefreshing(true);
        DataLoader.getLixiShopProductCategory(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swr.setRefreshing(false);
                if (isSuccess) {
                    if (page == 1 && isReload == false) {
                        listProduct = (ArrayList<LixiProductModel>) object;
                        if (listProduct == null || listProduct.size() == 0) {
                            showNodata();
                            noRemain = true;
                        } else
                            showData();
                    } else {
                        ArrayList<LixiProductModel> list = (ArrayList<LixiProductModel>) object;
                        if (isReload) {
                            listProduct.clear();
                            adapter.notifyDataSetChanged();
                            isReload = false;
                            if (list == null || list.size() == 0) {
                                showNodata();
                                return;
                            }
                        }
                        if (listProduct == null || list == null || list.size() == 0) {
                            noRemain = true;
                            return;
                        }
                        swr.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.VISIBLE);
                        tv_empty.setVisibility(View.GONE);
                        listProduct.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                    page++;
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
                isLoading = false;
            }
        }, page, Category);
    }
}
//
//    }
