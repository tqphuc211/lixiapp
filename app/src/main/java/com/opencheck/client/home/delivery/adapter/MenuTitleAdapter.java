package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.MenuTitleItemLayoutBinding;
import com.opencheck.client.home.delivery.model.MenuTitleModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class MenuTitleAdapter extends RecyclerView.Adapter<MenuTitleAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<MenuTitleModel> mList;
    private ItemClickListener itemClickListener;

    public MenuTitleAdapter(LixiActivity activity, ArrayList<MenuTitleModel> mList) {
        this.activity = activity;
        this.mList = mList;
    }

    public void setList(ArrayList<MenuTitleModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MenuTitleItemLayoutBinding menuTitleItemLayoutBinding =
                MenuTitleItemLayoutBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);
        return new ViewHolder(menuTitleItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_title.setText(mList.get(position).getName());

        if (mList.get(position).isSelected()) {
            holder.tv_title.setTextColor(activity.getResources().getColor(R.color.app_violet));
            holder.v_line.setBackgroundColor(activity.getResources().getColor(R.color.app_violet));
        } else {
            holder.tv_title.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
            holder.v_line.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_title;
        private View v_line;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            v_line = itemView.findViewById(R.id.v_line);

            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == rootView) {
                Log.d("position", "adapter = " + getAdapterPosition());
                itemClickListener.onItemCLick(getAdapterPosition(), "ITEM_CLICK", mList.get(getAdapterPosition()), 0L);
            }
        }
    }
}
