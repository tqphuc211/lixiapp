package com.opencheck.client.home.lixishop.payment;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopBuySuccesDialogBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.models.lixishop.LixiShopTransactionResultModel;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class SuccessDialog extends LixiTrackingDialog {
    public SuccessDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu;
    private TextView tv_title;
    private TextView tv_message;
    private TextView tv_view_log;
    private TextView tv_ok;

    private LixiShopTransactionResultModel transactionResult;

    SpannableStringBuilder spanMessage;
    String mss = "";
    String title = "";
    String okText = "";


    public void setData( String _title, String _ms, String _okText, LixiShopTransactionResultModel _lixiShopTransactionResultModel) {

        title = _title;
        mss = _ms;
        okText = _okText;
        transactionResult = _lixiShopTransactionResultModel;


        tv_title.setText(title);
        if (spanMessage != null)
            tv_message.setText(spanMessage);
        else if (mss.length() > 0)
            tv_message.setText(Html.fromHtml(mss));

        if (okText.length() > 0)
            tv_ok.setText(okText);
    }

    private LixiShopBuySuccesDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopBuySuccesDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }



    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);

        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_view_log = (TextView) findViewById(R.id.tv_view_log);
        tv_ok = (TextView) findViewById(R.id.tv_ok);


        rl_dismiss.setOnClickListener(this);
        tv_ok.setOnClickListener(this);
        tv_view_log.setOnClickListener(this);
    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rl_dismiss:
            case R.id.tv_ok:
                dismiss();
                break;
            case R.id.tv_view_log:
                OrderShopDetailDialog dialog = new OrderShopDetailDialog(activity, false, true, false);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dismiss();
                    }
                });
                dialog.show();
                dialog.setData(transactionResult.getTransaction_id());
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {
        public void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
