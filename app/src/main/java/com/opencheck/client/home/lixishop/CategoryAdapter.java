package com.opencheck.client.home.lixishop;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopRowBannerCategoryBinding;
import com.opencheck.client.databinding.LixiShopVoucherRowBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.models.lixishop.LixiShopCategoryModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LixiActivity activity;
    private final int VOUCHER = 1;
    private final int CATEGORY = 2;

    private ArrayList<LixiShopCategoryModel> list_category;
    private ArrayList<LixiHotProductModel> list_voucher = new ArrayList<>();
    private ArrayList<ServiceCategoryModel> listService = new ArrayList<ServiceCategoryModel>();
    private int widthScreen;
    private ProductHotAdapter productHotAdapter;
    private LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    public boolean isLoading = false;
    public int page = 1;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private boolean isOutOfVoucherPage = false;

    public CategoryAdapter(LixiActivity activity, ArrayList<LixiShopCategoryModel> list_category, ArrayList<ServiceCategoryModel> listService, int widthScreen) {
        this.activity = activity;
        this.list_category = list_category;
        this.widthScreen = widthScreen;
        this.listService = listService;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VOUCHER: {
                LixiShopVoucherRowBinding lixiShopVoucherRowBinding =
                        LixiShopVoucherRowBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                return new VoucherViewHolder(lixiShopVoucherRowBinding.getRoot());
            }
            default: {
//                CATEGORY
                LixiShopRowBannerCategoryBinding lixiShopRowBannerCategoryBinding =
                        LixiShopRowBannerCategoryBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                return new CategoryViewHolder(lixiShopRowBannerCategoryBinding.getRoot());
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == VOUCHER) {
            final VoucherViewHolder voucherViewHolder = (VoucherViewHolder) holder;
            if (productHotAdapter == null) {
                productHotAdapter = new ProductHotAdapter(activity, list_voucher);
                voucherViewHolder.rclv_voucher.setAdapter(productHotAdapter);
            }
            voucherViewHolder.getHotProduct();
            voucherViewHolder.rclv_voucher.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (voucherViewHolder.rclv_voucher.computeHorizontalScrollOffset() > 0) {
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                        if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            voucherViewHolder.getHotProduct();
                            isLoading = true;
                        }
                    }
                }
            });
            voucherViewHolder.tvHotProduct.setText(LanguageBinding.getString(R.string.shop_product_title, activity));
        }
        if (holder.getItemViewType() == CATEGORY) {

            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            if (list_category != null) {
                categoryViewHolder.txt_name_of_category.setText(list_category.get(position - 2).getName());

                //set ratio for image
                int heightWith169Aspect = widthScreen * 9 / 16;
                ViewGroup.LayoutParams params = categoryViewHolder.rl_Image.getLayoutParams();
                params.height = heightWith169Aspect;
                categoryViewHolder.rl_Image.setLayoutParams(params);

                //Exception
//                ImageLoader.getInstance().displayImage(list_category.get(position - 2).getBanner(),
//                        categoryViewHolder.img_banner_category, LixiApplication.getInstance().optionsNomal);
                LixiImage.with(activity)
                        .load(list_category.get(position - 2).getBanner())
                        .size(ImageSize.MEDIUM)
                        .into(categoryViewHolder.img_banner_category);
                categoryViewHolder.itemView.setTag(position - 2);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 0;
        else if (position == 1)
            return VOUCHER;
        else if (position >= 2)
            return CATEGORY;
        return -1;
    }

    @Override
    public int getItemCount() {
        return list_category.size() + 2;
    }

    public class VoucherViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView rclv_voucher;
        TextView tvHotProduct;

        public VoucherViewHolder(View itemView) {
            super(itemView);
            rclv_voucher = itemView.findViewById(R.id.rcl_voucher);
            tvHotProduct = itemView.findViewById(R.id.tvHotProduct);
        }

        private void getHotProduct() {
            if (!isLoading) {
                if (!isOutOfVoucherPage) {
                    DataLoader.getLixiShopHotProduct(activity, new ApiCallBack() {
                        @Override
                        public void handleCallback(boolean isSuccess, Object object) {
                            if (isSuccess) {
                                if (page == 1) {
                                    list_voucher = (ArrayList<LixiHotProductModel>) object;
                                    if (list_voucher == null || list_voucher.size() == 0)
                                        return;
                                    rclv_voucher.setLayoutManager(layoutManager);
                                    productHotAdapter = new ProductHotAdapter(activity, list_voucher);
                                    rclv_voucher.setAdapter(productHotAdapter);
                                    setOutOfVoucherPage(list_voucher);
                                } else {
                                    List<LixiHotProductModel> list = (List<LixiHotProductModel>) object;
                                    if (list == null || list.size() == 0 || list_voucher == null)
                                        return;
                                    setOutOfVoucherPage(list);
                                    list_voucher.addAll(list);
                                    productHotAdapter.notifyDataSetChanged();
                                }

                                page++;
                                isLoading = false;
                            } else {
                                isLoading = true;
                            }
                        }
                    }, page);
                }
            }
        }
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView img_banner_category;
        TextView txt_name_of_category;
        RelativeLayout rl_Image;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            img_banner_category = itemView.findViewById(R.id.img_banner_category);
            txt_name_of_category = itemView.findViewById(R.id.txt_name_of_category);
            rl_Image = itemView.findViewById(R.id.rl_Image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, LixiShopListProductActivity.class);
                    intent.putExtra("Category", list_category.get((Integer) v.getTag()).getId() + "");
                    intent.putExtra("ImageUrl", list_category.get((Integer) v.getTag()).getBanner());
                    intent.putExtra("Name", list_category.get((Integer) v.getTag()).getName());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    public ServiceCategoryModel checkService(String type) {
        if (listService != null) {
            for (int i = 0; i < listService.size(); i++) {
                ServiceCategoryModel service = listService.get(i);
                String stype = service.getType();
                boolean isTheSameType = service.getType().equals(type);
                if (isTheSameType) {
                    return service;
                }
            }
        }
        return null;
    }

    private void setOutOfVoucherPage(List<LixiHotProductModel> list) {
        if (list.size() < RECORD_PER_PAGE) {
            isOutOfVoucherPage = true;
        }
    }

    public void notifyCategoryChange(ArrayList<LixiShopCategoryModel> _list) {
        list_category = _list;
    }


    public void notifyServiceChange(ArrayList<ServiceCategoryModel> _list) {
        listService = _list;
    }


}