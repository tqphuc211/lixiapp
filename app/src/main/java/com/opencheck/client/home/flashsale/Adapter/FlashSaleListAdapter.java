package com.opencheck.client.home.flashsale.Adapter;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowFlashSaleDetailItemBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.flashsale.Global.FlashSaleDetailController;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindRequest;
import com.opencheck.client.home.flashsale.Model.FlashSaleRemindResponse;
import com.opencheck.client.home.flashsale.Model.RemindFlashSale;
import com.opencheck.client.home.flashsale.Service.AlarmFlashSale;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class FlashSaleListAdapter extends RecyclerView.Adapter<FlashSaleListAdapter.FlashSaleViewHolder> {
    // Constructor
    private LixiActivity activity;
    private ArrayList<FlashSaleNow.ProductsBean> listProductsBean;
    private ItemClickListener itemClickListener;


    // Var
    private boolean isHappening = false;
    long flashSaleID = -1;
    private long startTime;
    private AppPreferenceHelper appPreferenceHelper;

    // Action
    public static final String ACTION_ITEM_CLICK = "action item click";
    public static final String ACTION_REMIND = "action remind";

    public FlashSaleListAdapter(LixiActivity activity, ArrayList<FlashSaleNow.ProductsBean> listProductsBean, ItemClickListener itemClickListener) {
        this.activity = activity;
        this.listProductsBean = listProductsBean;
        this.itemClickListener = itemClickListener;
        appPreferenceHelper = new AppPreferenceHelper(activity);
    }

    @Override
    public FlashSaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowFlashSaleDetailItemBinding rowFlashSaleDetailItemBinding =
                RowFlashSaleDetailItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new FlashSaleViewHolder(rowFlashSaleDetailItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(final FlashSaleViewHolder holder, final int position) {
        FlashSaleNow.ProductsBean productsBean = listProductsBean.get(position);
        long orgPrice = productsBean.getPayment_price();
        long salePrice = productsBean.getPayment_discount_price();
        long disPrice = (long) (((double) (orgPrice - salePrice) / orgPrice) * 100);

        if (productsBean.getProduct_cover_image() != null) {
            ImageLoader.getInstance().displayImage(productsBean.getProduct_cover_image(), holder.imgBanner, LixiApplication.getInstance().optionsNomal);
        } else {
            ImageLoader.getInstance().displayImage(productsBean.getProduct_image_medium().get(0), holder.imgBanner, LixiApplication.getInstance().optionsNomal);
        }

        holder.txtName.setText(productsBean.getName());
        holder.txtOrgPrice.setText(Helper.getVNCurrency(orgPrice) + activity.getString(R.string.p));
        holder.txtOrgPrice.setPaintFlags(holder.txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtSalePrice.setText(Helper.getVNCurrency(salePrice) + activity.getString(R.string.p));

        if (disPrice != 0) {
            holder.frameSaleOff.setVisibility(View.VISIBLE);
            holder.frameLixi.setVisibility(View.INVISIBLE);
            holder.txtSaleOff.setText(disPrice + "%");
        } else {
            holder.frameSaleOff.setVisibility(View.INVISIBLE);
            holder.frameLixi.setVisibility(View.VISIBLE);
            holder.txtLixi.setText("+" + Helper.getVNCurrency(productsBean.getCashback_price()));
        }

        if (orgPrice == salePrice) {
            holder.txtOrgPrice.setVisibility(View.GONE);
        } else {
            holder.txtOrgPrice.setVisibility(View.VISIBLE);
        }

        if (isHappening) {
            if (productsBean.getQuantity_order_done() == productsBean.getQuantity_limit()) {
                holder.btnBuy.setVisibility(View.GONE);
                holder.btnRemind.setVisibility(View.GONE);
                holder.txtRemind.setVisibility(View.GONE);
                holder.txtSold.setVisibility(View.GONE);
                holder.progSold.setVisibility(View.GONE);
                holder.txtSoldOut.setVisibility(View.VISIBLE);
            } else {
                holder.btnBuy.setVisibility(View.VISIBLE);
                holder.btnRemind.setVisibility(View.GONE);
                holder.txtRemind.setVisibility(View.GONE);
                holder.txtSold.setVisibility(View.VISIBLE);
                holder.progSold.setVisibility(View.VISIBLE);
                holder.txtSoldOut.setVisibility(View.GONE);
                holder.txtSold.setText("Đã bán " + productsBean.getQuantity_order_done());
                holder.progSold.setMax(productsBean.getQuantity_limit());
                holder.progSold.setProgress(productsBean.getQuantity_order_done());
            }
        } else {

            holder.btnBuy.setVisibility(View.GONE);
            holder.btnRemind.setVisibility(View.VISIBLE);
            holder.txtRemind.setVisibility(View.VISIBLE);
            holder.txtSold.setVisibility(View.INVISIBLE);
            holder.progSold.setVisibility(View.INVISIBLE);
            holder.txtSoldOut.setVisibility(View.GONE);
            holder.txtRemind.setText(productsBean.getReminder_count() + " người đã nhắc mua");
            holder.btnRemind.setChecked(false);
            holder.btnRemind.setBackground(activity.getResources().getDrawable(R.drawable.btn_buy_corner));
            holder.btnRemind.setText("Nhắc tôi");
            ArrayList<RemindFlashSale> listRemind = appPreferenceHelper.getRemind();
            if (listRemind.size() != 0) {
                for (int i = 0; i < listRemind.size(); i++) {
                    if (listRemind.get(i).getIdProduct() == listProductsBean.get(position).getId()) {
                        holder.btnRemind.setChecked(true);
                        holder.btnRemind.setBackground(activity.getResources().getDrawable(R.drawable.btn_cancel_remind));
                        holder.btnRemind.setText("Bỏ nhắc");
                        break;
                    }
                }
            }


            holder.btnRemind.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (holder.btnRemind.isChecked()) {
                        holder.btnRemind.setBackground(activity.getResources().getDrawable(R.drawable.btn_cancel_remind));
                        holder.btnRemind.setText("Bỏ nhắc");
                    } else {
                        holder.btnRemind.setBackground(activity.getResources().getDrawable(R.drawable.btn_buy_corner));
                        holder.btnRemind.setText("Nhắc tôi");
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listProductsBean.size();
    }

    public void isHappening(boolean isHappening, long flashSaleID, long startTime) {
        this.isHappening = isHappening;
        this.flashSaleID = flashSaleID;
        this.startTime = startTime;
        notifyDataSetChanged();
    }

    public class FlashSaleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName, txtOrgPrice, txtSalePrice, txtSold, txtSaleOff, txtLixi, txtRemind, txtSoldOut;
        Button btnBuy;
        CheckBox btnRemind;
        ImageView imgBanner;
        RelativeLayout frameSaleOff, frameLixi;
        ProgressBar progSold;

        public FlashSaleViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtOrgPrice = (TextView) itemView.findViewById(R.id.txtOrgPrice);
            txtSalePrice = (TextView) itemView.findViewById(R.id.txtSalePrice);
            txtSold = (TextView) itemView.findViewById(R.id.txtSold);
            txtSaleOff = (TextView) itemView.findViewById(R.id.txtSaleOff);
            txtLixi = (TextView) itemView.findViewById(R.id.txtLixi);
            txtRemind = (TextView) itemView.findViewById(R.id.txtRemind);
            txtSoldOut = (TextView) itemView.findViewById(R.id.txtSoldOut);

            btnBuy = (Button) itemView.findViewById(R.id.btnBuy);

            btnRemind = (CheckBox) itemView.findViewById(R.id.btnRemind);

            imgBanner = (ImageView) itemView.findViewById(R.id.imgBanner);

            frameSaleOff = (RelativeLayout) itemView.findViewById(R.id.frameSaleOff);
            frameLixi = (RelativeLayout) itemView.findViewById(R.id.frameLixi);

            progSold = (ProgressBar) itemView.findViewById(R.id.progSold);

            itemView.setOnClickListener(this);
            btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(activity, FlashSaleVoucherActivity.class);
//                    intent.putExtra(FlashSaleHomeAdapter.FLASH_SALE_VOUCHER_ID, (long) listProductsBean.get(getAdapterPosition()).getId());
//                    intent.putExtra(FlashSaleVoucherActivity.BUY_NOW, true);
//                    activity.startActivity(intent);
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, listProductsBean.get(getAdapterPosition()).getId(),
                            null, FlashSaleDetailController.getParamBuyNow(true));
                }
            });

            btnRemind.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (btnRemind.isChecked()) {
                        // chọn nhắc tôi
                        FlashSaleRemindRequest flashSaleRemindRequest = new FlashSaleRemindRequest();
                        flashSaleRemindRequest.setFlash_sale(flashSaleID);
                        flashSaleRemindRequest.setProduct(listProductsBean.get(getAdapterPosition()).getId());

                        DataLoader.requestFlashSaleRemind(activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                FlashSaleRemindResponse remindResponse = (FlashSaleRemindResponse) object;

                                RemindFlashSale remindFlashSale = new RemindFlashSale();
                                remindFlashSale.setIdFlashSale(flashSaleID);
                                remindFlashSale.setIdProduct(listProductsBean.get(getAdapterPosition()).getId());
                                remindFlashSale.setIdRemind(remindResponse.getId());

                                AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
                                appPreferenceHelper.setRemind(remindFlashSale);

                                long pendingID = flashSaleID * listProductsBean.get(getAdapterPosition()).getId();
                                itemClickListener.onItemCLick(getAdapterPosition(), ACTION_REMIND, pendingID, 0L);
                            }
                        }, flashSaleRemindRequest);


                    } else {
                        // chọn bỏ nhắc
                        DataLoader.deleteFlashSaleRemind(activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                AlarmFlashSale alarmFlashSale = new AlarmFlashSale(activity);
                                alarmFlashSale.cancel(flashSaleID * listProductsBean.get(getAdapterPosition()).getId());
                                appPreferenceHelper.clearRemind(flashSaleID * listProductsBean.get(getAdapterPosition()).getId());
                            }
                        }, listProductsBean.get(getAdapterPosition()).getId());
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemCLick(getAdapterPosition(), ACTION_ITEM_CLICK, (long) listProductsBean.get(getAdapterPosition()).getId(), 0L);
            }
        }
    }
}
