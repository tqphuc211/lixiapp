package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityProductMerchantHotBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.ProductMerchantHotDetailAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class ProductMerchantHotActivity extends LixiActivity {

    private ProductMerchantHotDetailAdapter adapter;
    private ArrayList<ProductMerchantHot> productMerchantHotList;
    private int page = 1;
    private SpacesItemDecoration spacesItemDecoration;
    private boolean isLoading = false;
    private int record_per_page = DeliDataLoader.record_per_page;
    private DeliAddressModel mLocation;

    private ActivityProductMerchantHotBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_merchant_hot);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        initViews();
    }

    private void initViews() {
        mBinding.ivBack.setOnClickListener(this);

        int space = (int) getResources().getDimension(R.dimen.value_8);
        spacesItemDecoration = new SpacesItemDecoration(2, space, true);

        mBinding.swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                noRemain = false;
                mBinding.rcvFood.removeItemDecoration(spacesItemDecoration);
                if (adapter != null) {
                    productMerchantHotList.clear();
                    adapter.notifyDataSetChanged();
                }
                getProductMerchantHot();
            }
        });

//        mBinding.rcvFood.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (isLoading || noRemain)
//                    return;
//
//                if (isRvBottom())
//                    getProductMerchantHot();
//            }
//        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getProductMerchantHot();
            }
        }, 300);
    }

    public boolean isRvBottom() {
        if (productMerchantHotList == null || productMerchantHotList.size() == 0)
            return false;
        int totalItemCount = productMerchantHotList.size();
        int lastVisibleItem = ((LinearLayoutManager) mBinding.rcvFood.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7 && productMerchantHotList.size() >= record_per_page);
    }

    public void showNodata() {
        mBinding.swr.setVisibility(View.GONE);
        mBinding.rcvFood.setVisibility(View.GONE);
        mBinding.lnNoResult.setVisibility(View.VISIBLE);
    }

    public void showData() {
        mBinding.swr.setVisibility(View.VISIBLE);
        mBinding.rcvFood.setVisibility(View.VISIBLE);
        mBinding.lnNoResult.setVisibility(View.GONE);
        adapter = new ProductMerchantHotDetailAdapter(activity, productMerchantHotList);
        mBinding.rcvFood.setLayoutManager(new GridLayoutManager(activity, 2));
        mBinding.rcvFood.addItemDecoration(spacesItemDecoration);
        mBinding.rcvFood.setAdapter(adapter);
    }

    private boolean isReload = false;
    private boolean noRemain = false;

    private void getProductMerchantHot() {
        isLoading = true;
        mBinding.swr.setRefreshing(true);
        DeliDataLoader.getProductMerchantHot(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                mBinding.swr.setRefreshing(false);
                if (isSuccess) {
                    if (page == 1 && !isReload) {
                        productMerchantHotList = (ArrayList<ProductMerchantHot>) object;
                        if (productMerchantHotList == null || productMerchantHotList.size() == 0) {
                            showNodata();
                            noRemain = true;
                        } else
                            showData();
                    } else {
                        ArrayList<ProductMerchantHot> list = (ArrayList<ProductMerchantHot>) object;
                        if (isReload) {
                            productMerchantHotList.clear();
                            adapter.notifyDataSetChanged();
                            isReload = false;
                            if (list == null || list.size() == 0) {
                                showNodata();
                                return;
                            }
                        }
                        if (productMerchantHotList == null || list == null || list.size() == 0) {
                            noRemain = true;
                            return;
                        }
                        mBinding.swr.setVisibility(View.VISIBLE);
                        mBinding.rcvFood.setVisibility(View.VISIBLE);
                        mBinding.lnNoResult.setVisibility(View.GONE);
                        productMerchantHotList.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                    page++;
                    isLoading = false;
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
    }

    @Override
    public void onClick(View v) {
        if (v == mBinding.ivBack) {
            finish();
        }
    }
}
