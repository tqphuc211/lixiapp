package com.opencheck.client.home.flashsale.View;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewFlashSaleBannerBinding;
import com.opencheck.client.utils.Helper;

public class FlashSaleBanner extends ConstraintLayout {
    // Constructor
    private Context context;
    private boolean isLarge = false;

    public FlashSaleBanner(Context context, Context context1) {
        super(context);
        this.context = context1;
        initView(context);
    }

    public FlashSaleBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initData(attrs);
        initView(context);
    }

    public FlashSaleBanner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initData(attrs);
        initView(context);
    }

    private void initData(AttributeSet attributeSet) {
        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.FlashSaleBanner);
        isLarge = a.getBoolean(R.styleable.FlashSaleBanner_isLarge, false);
        a.recycle();
    }

    private ViewFlashSaleBannerBinding mBinding;

    private void initView(Context context) {
        mBinding = ViewFlashSaleBannerBinding.inflate(LayoutInflater.from(getContext()),
                this, true);

        if (isLarge) {
            mBinding.constraintPrice.setVisibility(VISIBLE);
        } else {
            mBinding.constraintPrice.setVisibility(GONE);
        }
    }

    public TimerView getTimer() {
        return mBinding.timerView;
    }

    public void setPrice(long orgPrice, long salePrice) {
        long disPrice = (long) (((double) (orgPrice - salePrice) / orgPrice) * 100);
        if (disPrice != 0) {
            mBinding.txtOrgPrice.setVisibility(View.VISIBLE);
            mBinding.txtOrgPrice.setText(Helper.getVNCurrency(orgPrice) + context.getString(R.string.p));
            mBinding.txtOrgPrice.setPaintFlags(mBinding.txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            mBinding.txtOrgPrice.setVisibility(View.GONE);
        }

        mBinding.txtSalePrice.setText(Helper.getVNCurrency(salePrice) + context.getString(R.string.p));
    }

    public void setItemSold(long quant) {
        mBinding.txtSold.setText(quant + "");
    }

    public void isLarge(boolean isLarge) {
        this.isLarge = isLarge;
        if (isLarge) {
            mBinding.constraintPrice.setVisibility(VISIBLE);
        } else {
            mBinding.constraintPrice.setVisibility(GONE);
        }
    }
}
