package com.opencheck.client.home.delivery.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityOrderInfoBinding;
import com.opencheck.client.databinding.DialogPromotionAlertBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.controller.ExtraInfoController;
import com.opencheck.client.home.delivery.controller.FoodController;
import com.opencheck.client.home.delivery.controller.StorePolicyCheckoutController;
import com.opencheck.client.home.delivery.dialog.InfoOrderDialog;
import com.opencheck.client.home.delivery.dialog.PaymentMethodDialog;
import com.opencheck.client.home.delivery.dialog.UnavailablePaymentWithPromotionDialog;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.DeliOrderResponseModel;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.home.delivery.model.QuotationModel;
import com.opencheck.client.home.delivery.model.ResponsePromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.model.StorePolicyModel;
import com.opencheck.client.home.rating.dialog.LixiGiftDialog;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.FirebaseTracker;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class OrderInfoActivity extends LixiActivity {
    public final static String UUID = "uuid";
    public final static String ID = "ID";
    public final static String PAYMENT_METHOD_DEFAULT = "PAYMENT_METHOD_DEFAULT";
    private String uuid = "";
    private DeliOrderModel orderModel;
    private boolean isChange = false;
    private long totalMoney = 0L;
    private DeliAddressModel receiveAddress;
    private StoreOfCategoryModel mStore;
    private long storeId = 0L;
    private String source;
    private long lixiReduce = 0L;
    HashMap<String, Integer> countMap;

    private String mReceiveTimeChange = null;
    private boolean isGetTimeFirst = false;
    private ArrayList<PaymentModel> paymentList;
    private PaymentModel payment, lastPayment;
    private boolean isApplied = false;
    private boolean isRepay = false;
    private IntentFilter intentFilter = new IntentFilter("LOCATION_CHANGE");
    private ActivityOrderInfoBinding mBinding;
    private long tempMoneyTotal = 0L;

    public static void startOrderCheckoutActivity(Context context, String uuid, long storeId, String source) {
        Intent intent = new Intent(context, OrderInfoActivity.class);
        intent.putExtra(UUID, uuid);
        intent.putExtra(ID, storeId);
        intent.putExtra(GlobalTracking.SOURCE, source);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_info);
        receiveAddress = AppPreferenceHelper.getInstance().getDefaultLocation();
        uuid = getIntent().getStringExtra(UUID);
        storeId = getIntent().getLongExtra(ID, 0L);
        source = getIntent().getStringExtra(GlobalTracking.SOURCE);
        intViews();
    }

    private void intViews() {
        mBinding.rlPayment.setOnClickListener(this);
        mBinding.llInfo.setOnClickListener(this);
        mBinding.rlInfo.setOnClickListener(this);
        mBinding.tvOrder.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.rlPromotion.setOnClickListener(this);
        mBinding.llPromotion.setOnClickListener(this);
        mBinding.tvDeleteOrder.setOnClickListener(this);
        mBinding.vInvisible.setOnClickListener(this);
        mBinding.tvMessageLixi.setOnClickListener(this);
        mBinding.rlLoading.setOnClickListener(this);
        mBinding.tvEditInfo.setOnClickListener(this);
        mBinding.ivDelete.setOnClickListener(this);
        mBinding.switchLixi.setOnClickListener(this);

        mBinding.etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isChange = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        activity.registerReceiver(mReceiver, intentFilter);

        getUserConfig();
        getStoreDetail();
    }

    private void getOrderDetail() {
        mBinding.rlLoading.setVisibility(View.VISIBLE);
        DeliDataLoader.getUserOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    orderModel = (DeliOrderModel) object;
                    showData();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, uuid, null);
    }

    private void setPaymentMethod(String key) {
        String methodName = "";
        String type = "";
        switch (key) {
            case DeliConfigModel.PAYMENT_BY_ATM:
                methodName = "Thanh toán bằng thẻ ATM";
                type = PaymentModel.PRE_PAY;
                break;
            case DeliConfigModel.PAYMENT_BY_CC:
                methodName = "Thanh toán bằng thẻ quốc tế";
                type = PaymentModel.PRE_PAY;
                break;
            case DeliConfigModel.PAYMENT_BY_COD:
                methodName = "Thanh toán khi nhận hàng";
                type = PaymentModel.POST_PAY;
                break;
            case DeliConfigModel.PAYMENT_AT_STORE:
                methodName = "Thanh toán tại cửa hàng tiện lợi";
                type = PaymentModel.PRE_PAY;
                break;
            case DeliConfigModel.PAYMENT_BY_MOMO:
                methodName = "Ví điện tử MoMo";
                type = PaymentModel.PRE_PAY;
                break;
            case DeliConfigModel.PAYMENT_BY_ZALO:
                methodName = "Ví điện tử ZaloPay";
                type = PaymentModel.PRE_PAY;
                break;
        }
        lastPayment = payment = new PaymentModel(key, methodName, type, false);
        mBinding.tvMethod.setText(payment.getName());
    }

    private void getUserConfig() {
        ArrayList<DeliConfigModel> list = DeliConfigModel.getData(this);
        for (DeliConfigModel model : list) {
            if (model.getKey().equals(DeliConfigModel.DEFAULT_PAYMENT_METHOD)) {
                setPaymentMethod(model.getValue());
                break;
            }
        }
        if(getIntent().getStringExtra(PAYMENT_METHOD_DEFAULT) != null && !getIntent().getStringExtra(PAYMENT_METHOD_DEFAULT).isEmpty()) {
            setPaymentMethod(getIntent().getStringExtra(PAYMENT_METHOD_DEFAULT));
        }
    }

    private void getStoreDetail() {
        DeliDataLoader.getStoreDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mStore = (StoreOfCategoryModel) object;
                    mBinding.tvNameMerchant.setText(mStore.getName());
                    mBinding.tvTimeDistance.setText(Html.fromHtml(mStore.getShipping_duration_min() + "-" + mStore.getShipping_duration_max() + " phút"));
                    if (mStore.getLogo_link() != null)
                        ImageLoader.getInstance().displayImage(mStore.getLogo_link(), mBinding.ivMerchant, LixiApplication.getInstance().optionsNomal);
                    if (mStore.isTrusted_store())
                        mBinding.llTrueStore.setVisibility(View.VISIBLE);

                    getOrderDetail();

                } else {
                    getOrderDetail();
                }
            }
        }, storeId, receiveAddress.getFullAddress(), receiveAddress.getLat(), receiveAddress.getLng());
    }

    private void showData() {
        mBinding.rlLoading.setVisibility(View.GONE);

        mBinding.tvUserInfo.setText(Html.fromHtml(orderModel.getReceive_name() + " - " + orderModel.getReceive_phone()));
        mBinding.tvAddress.setText(orderModel.getReceive_address().substring(0,
                orderModel.getReceive_address().contains(",") ? orderModel.getReceive_address().indexOf(",") : orderModel.getReceive_address().length()));
        mBinding.tvFullAddress.setText(orderModel.getReceive_address());

        if (orderModel.getReceive_date() != null) {
            Calendar currentDate = Calendar.getInstance();
            Calendar receiveDate = Calendar.getInstance();
            receiveDate.setTimeInMillis(orderModel.getReceive_date());
            int diff = Helper.daysBetween(currentDate, receiveDate);

            if (diff == 0)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(orderModel.getReceive_date())) + " - " + LanguageBinding.getString(R.string.today, activity)));
            else if (diff == 1)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(orderModel.getReceive_date())) + " - " + LanguageBinding.getString(R.string.tomorrow, activity)));
            else if (diff > 1)
                mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, String.valueOf(orderModel.getReceive_date())));
        } else {
            mBinding.tvTime.setText(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity));
        }

        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        if (cart.containsKey("" + storeId) && cart.get("" + storeId) != null) {
            cart.get("" + storeId).setUpdated_ship_receive_time(orderModel.getReceive_date());
            Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
        }

        if (!isGetTimeFirst) {
            mReceiveTimeChange = orderModel.getReceive_date() == null ? null : String.valueOf(orderModel.getReceive_date());
            isGetTimeFirst = true;
        }

        if (orderModel.getCashback() > 0) {
            mBinding.rlCashback.setVisibility(View.VISIBLE);
            mBinding.tvCashback.setText(Html.fromHtml("+" + Helper.getVNCurrency(orderModel.getCashback()) + " Lixi"));
        }
        if (orderModel.getShipping_distance() >= 100) {
            double distance = (double) Math.round(((float) orderModel.getShipping_distance() / 1000) * 10) / 10;
            mBinding.tvDistance.setText(Html.fromHtml(LanguageBinding.getString(R.string.order_confirm_delivery_distance, activity) + ": " + String.valueOf(distance + "km")));
        } else
            mBinding.tvDistance.setText(Html.fromHtml(LanguageBinding.getString(R.string.order_confirm_delivery_distance, activity) + ": " + String.valueOf(orderModel.getShipping_distance()) + "m"));

        long quantity = 0L;
        tempMoneyTotal = 0L;
        mBinding.llFoodList.removeAllViews();

        for (int i = 0; i < orderModel.getList_product().size(); i++) {
            tempMoneyTotal += orderModel.getList_product().get(i).getPrice() * orderModel.getList_product().get(i).getQuantity();
            quantity += orderModel.getList_product().get(i).getQuantity();
            new FoodController(activity, orderModel.getList_product().get(i), mBinding.llFoodList, true, i, orderModel.getList_product().size());
        }

//        for (int i = 0; i < orderModel.getList_quotation().size(); i++) {
//            QuotationModel quotationModel = orderModel.getList_quotation().get(i);
//            if (quotationModel.getCode().equalsIgnoreCase("product_money")
//                    || (!orderModel.isApply_service_price() && quotationModel.getCode().equalsIgnoreCase("service_money"))
//                    || (quotationModel.getMoney() == 0 && !quotationModel.getCode().equalsIgnoreCase("ship_money") && !quotationModel.getCode().equalsIgnoreCase("service_money")))
//                continue;
//
//            mBinding.llOptionInfo.setVisibility(View.VISIBLE);
//            new ExtraInfoController(activity, mStore, orderModel, mBinding.llOptionInfo, true, i, orderModel.getList_quotation().size());
//        }

        mBinding.tvQuantityOrder.setText(String.format(LanguageBinding.getString(R.string.order_confrim_temp_value, activity), quantity));
        mBinding.tvMoneyTotal.setText(Html.fromHtml(Helper.getVNCurrency(tempMoneyTotal) + "đ"));

        mBinding.llPolicy.removeAllViews();
        if (mStore.getList_store_policy() != null && mStore.getList_store_policy().size() > 0) {
            boolean hasListPaymentMethodNameNonNull = false;
            for (StorePolicyModel storePolicyModel : mStore.getList_store_policy()) {
                if (storePolicyModel.getList_payment_method_name() != null) {
                    hasListPaymentMethodNameNonNull = true;
                    new StorePolicyCheckoutController(activity, mBinding.llPolicy, storePolicyModel);
                }
            }
            if(hasListPaymentMethodNameNonNull){
                mBinding.llPolicy.setVisibility(View.VISIBLE);
            }
        }

        if (!mStore.isIs_use_lixi()) {
            mBinding.tvUsingLixi.setText(getString(R.string.order_confirm_not_support_using_lixi));
            mBinding.switchLixi.setEnabled(false);
            mBinding.llCurrentLixi.setVisibility(View.INVISIBLE);
        }

        if (orderModel.getPoint_lixi_money() == 0) {
            mBinding.llLixiPayment.setVisibility(View.GONE);
        } else {
            mBinding.tvCurrentLixi.setText(String.format(LanguageBinding.getString(R.string.order_confirm_current_lixi, activity), Helper.getVNCurrency(orderModel.getPoint_lixi_money())));
        }

        if (orderModel.getState().equals(DeliOrderModel.PAYMENT)) {
            isRepay = true;
            mBinding.tvOrder.setText(LanguageBinding.getString(R.string.pay, activity));
            mBinding.vInvisible.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams params = mBinding.llContent.getLayoutParams();
            mBinding.vInvisible.setLayoutParams(params);
            countMap = Helper.getRepayCount(activity, "REPAY_FILE");
            if (countMap.containsKey(orderModel.getUuid())) {
                if (countMap.get(orderModel.getUuid()) < 2) {
                    setPaymentMethod(orderModel.getPayment_method().getCode());
                    if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() > 0) {
                        getCalculatePrice(orderModel.getList_promotion_code_apply().get(0).getCode());
                    } else {
                        getCalculatePrice(null);
                    }
                } else {
                    mBinding.tvMethod.setText(payment != null ? payment.getName() : "Thanh toán khi nhận hàng");
                    getCalculatePrice(null);
                }
            } else {
                mBinding.tvMethod.setText(payment != null ? payment.getName() : "Thanh toán khi nhận hàng");
                getCalculatePrice(null);
            }
        } else {
            if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() > 0) {
                getCalculatePrice(orderModel.getList_promotion_code_apply().get(0).getCode());
            } else if (!mBinding.tvPromotionCode.getText().equals(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity))) {
                getCalculatePrice(mBinding.tvPromotionCode.getText().toString());
            } else  {
                String promotionCodeFromUser = getIntent().getStringExtra(PromotionCodeUserGroupActivity.USER_PROMOTION_CODE);
                Helper.showLog("promotion:" + promotionCodeFromUser);
                if(promotionCodeFromUser != null && !promotionCodeFromUser.isEmpty()) {
                    isApplied = true;
                    getCalculatePrice(promotionCodeFromUser);
                } else {
                    getCalculatePrice(null);
                }
            }
        }
    }

    private void confirmOrder() {
        if (!isChange) {
            submitOrder();
        } else {
            updateOrder();
        }
    }

    private void updateOrder() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", uuid);
            params.put("note", mBinding.etNote.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.updateOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    uuid = (String) object;
                    isChange = false;
                    submitOrder();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void getPaymentMethod() {
        DeliDataLoader.getPaymentMethod(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    paymentList = (ArrayList<PaymentModel>) object;
                    PaymentMethodDialog dialog = new PaymentMethodDialog(activity, false, true, false);
                    dialog.show();
                    dialog.setIDialogEventListener(new PaymentMethodDialog.IDialogEvent() {
                        @Override
                        public void onOk(PaymentModel paymentModel) {
                            // Kiểm tra promotion có phù hợp với phương thức thanh toán không
                            lastPayment = payment;
                            payment = paymentModel;

                            if (!mBinding.tvPromotionCode.getText().toString().equals(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity))) {
                                getCalculatePrice(mBinding.tvPromotionCode.getText().toString());
                            } else getCalculatePrice(null);
                        }
                    });
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            isGotPaymentMethod = false;
                        }
                    });
                    dialog.setData(paymentList, payment.getCode());
                    isGotPaymentMethod = true;
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, orderModel.getStore_id(), uuid);
    }

    private void submitOrder() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", uuid);
            if (!mBinding.tvPromotionCode.getText().toString().equals(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity))) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(mBinding.tvPromotionCode.getText().toString());
                params.put("promotion_codes", jsonArray);
            }
            params.put("payment_method_code", payment.getCode());
            params.put("payment_gateway_data", "");
            params.put("is_use_lixi_money", mBinding.switchLixi.isChecked());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Crashlytics.logException(new CrashModel("UUID: " + uuid + " StoreID: " + storeId));
        }

        Analytics.trackOrderDelivery(uuid);

        DeliDataLoader.submitOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                trackingCheckedOut(isSuccess);
                if (isSuccess) {
                    HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                    if (cart.containsKey("" + storeId)) {
                        if (cart.get("" + storeId) != null && cart.get("" + storeId).getProduct_list() != null) {
                            cart.get("" + storeId).getProduct_list().clear();
                            cart.get("" + storeId).setUuid(null);
                            Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                        }
                    }

                    AppPreferenceHelper.getInstance().setDefaultLocation(receiveAddress);

                    if (mBinding.tvPromotionCode.getText().toString().equals(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity)))
                        FirebaseTracker.getInstance().trackerBeginCheckoutEvent(orderModel.getUuid(), totalMoney, payment.getCode(), null);
                    else
                        FirebaseTracker.getInstance().trackerBeginCheckoutEvent(orderModel.getUuid(), totalMoney, payment.getCode(), mBinding.tvPromotionCode.getText().toString());

                    DeliOrderResponseModel response = (DeliOrderResponseModel) object;
                    if (payment.getType().equals(PaymentModel.PRE_PAY)) {
                        if (response.getPayment_data() == null) {
                            Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.try_again, activity));
                            return;
                        }
                    }
                    trackingPurchaseSuccess();
                    trackingOrderComplete();
                    OrderTrackingActivity.startOrderTrackingActivity(activity,
                            uuid.substring(uuid.indexOf('O') + 1), response, false);
                    finish();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);

    }

    private void repayOrder() {
        try {
            JSONObject params = new JSONObject();
            params.put("user_order_uuid", uuid);
            params.put("payment_method_code", payment.getCode());
            params.put("is_use_lixi_money", mBinding.switchLixi.isChecked());

            DeliDataLoader.repayOrder(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        trackingRepaySuccess();
                        DeliOrderResponseModel response = (DeliOrderResponseModel) object;

                        if (payment.getType().equals(PaymentModel.PRE_PAY)) {
                            if (response.getPayment_data() == null) {
                                Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.try_again, activity));
                                return;
                            }
                        }

                        OrderTrackingActivity.startOrderTrackingActivity(activity,
                                uuid.substring(uuid.indexOf('O') + 1), response, false);
                        finish();
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                    }
                }
            }, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkStateOrder() {
        final boolean[] isTrue = {false};
        DeliDataLoader.getUserOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    DeliOrderModel orderModel = (DeliOrderModel) object;
                    switch (orderModel.getState()) {
                        case DeliOrderModel.SUBMITTED:
                            trackingOrderComplete();
                        case DeliOrderModel.CANCELED:
                            isTrue[0] = true;
                            break;
                    }
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, uuid, null);
        return isTrue[0];
    }

    private void showAlertPromotionDialog() {
        int count = getQuantityPromotion();
        if (count < 1) {
            confirmOrder();
            return;
        }

        final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        DialogPromotionAlertBinding binding = DataBindingUtil.inflate(LayoutInflater.from(activity),
                R.layout.dialog_promotion_alert, null, false);

        alertDialog.setView(binding.getRoot());
        binding.tvMessage.setText(String.format(LanguageBinding.getString(R.string.order_confirm_count_promotion, activity), count));
        binding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApplied = false;
                alertDialog.dismiss();
                confirmOrder();
            }
        });
        binding.tvUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PromotionCodeListActivity.startPromotionCodeActivity(activity, storeId, orderModel.getMoney(), uuid, mBinding.tvPromotionCode.getText().toString(), payment.getCode());
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void getCalculatePrice(final String listCode, final boolean isUseLixiMoney) {
        DeliDataLoader.getCalculatePrice(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mBinding.tvMethod.setText(payment.getName());
                    ResponsePromotionModel model = (ResponsePromotionModel) object;
                    if (model == null) {
                        return;
                    }

                    totalMoney = model.getFinal_money();
                    if (listCode != null)
                        showDataWithPromotionCode(listCode, model);
                    else showDataWithoutPromotionCode(model);

                } else {
                    HashMap<String, Integer> countMap = Helper.getRepayCount(activity, "REPAY_FILE");
                    if (countMap.containsKey(orderModel.getUuid())) {
                        if (countMap.get(orderModel.getUuid()) < 2) {
                            showUnavailablePaymentDialog(listCode);
                        } else {
                            countMap.remove(orderModel.getUuid());
                        }
                    } else {
                        if (listCode != null) {
                            showUnavailablePaymentDialog(listCode);
                        }
                    }
                }
            }
        }, orderModel.getUuid(), payment.getCode(), listCode, isUseLixiMoney);
    }

    private void getCalculatePrice(final String listCode) {
        getCalculatePrice(listCode, mBinding.switchLixi.isChecked());
    }

    private void showUnavailablePaymentDialog(String listCode) {
        final boolean[] isOk = {false};
        UnavailablePaymentWithPromotionDialog dialog = new UnavailablePaymentWithPromotionDialog(activity, false, true, false);
        dialog.show();
        dialog.setData(listCode, lastPayment.getName());
        dialog.setIDialogEventListener(new UnavailablePaymentWithPromotionDialog.IDialogEvent() {
            @Override
            public void onOk(boolean isAgree) {
                //Đổi phương thức thanh toán - client xử lý bỏ mã code
                isOk[0] = isAgree;
                if (isAgree) {
                    isApplied = false;
                    getCalculatePrice(null);
                } else {
                    payment = lastPayment;
                }
                mBinding.tvMethod.setText(payment.getName());
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (!isOk[0]) {
                    payment = lastPayment;
                }
            }
        });
    }

    private void showDataWithPromotionCode(String listCode, ResponsePromotionModel model) {
        mBinding.tvPriceTotal.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_money()) + "đ"));
        mBinding.tvPromotionCode.setText(listCode);
        mBinding.llPromotion.setBackground(getDrawable(R.drawable.bg_button_violet));
        mBinding.tvPromotionCode.setTextColor(getResources().getColor(R.color.white));
        mBinding.ivDelete.setVisibility(View.VISIBLE);
        mBinding.ivPromotion.setVisibility(View.GONE);
        mBinding.ivArrow.setVisibility(View.GONE);
        mBinding.llCashbackCode.setVisibility(View.VISIBLE);

        for (QuotationModel item : model.getList_quotation()) {
            if (item.getCode().equalsIgnoreCase("promotion_money")) {
                if (item.getMoney() == 0)
                    mBinding.tvReduceMoney.setVisibility(View.GONE);
                else {
                    mBinding.tvReduceMoney.setVisibility(View.VISIBLE);
                    mBinding.tvReduceMoney.setText(Html.fromHtml(Helper.getVNCurrency(item.getMoney()) + "đ"));
                }
            }
        }

        if (model.getList_quotation() != null) {
            //             set min height for parent
            if (mBinding.llOptionInfo.getChildCount() > 0) {
                int childHeight = mBinding.llOptionInfo.getChildAt(0).getHeight();
                int parentMinHeight = childHeight * mBinding.llOptionInfo.getChildCount();
                mBinding.llOptionInfo.setMinimumHeight(parentMinHeight);
            }
            mBinding.llOptionInfo.removeAllViews();
            for (int i = 0; i < model.getList_quotation().size(); i++) {
                QuotationModel quotationModel = model.getList_quotation().get(i);
                if (quotationModel.getCode().equals("lixi_money_avaiable")) {
                    mBinding.tvUsingLixi.setText(quotationModel.getName());
                    if (quotationModel.getMoney() != 0) {
                        mBinding.tvLixiReduce.setVisibility(View.VISIBLE);
                        mBinding.tvLixiReduce.setText(String.format(LanguageBinding.getString(R.string.order_confirm_reduce_lixi, activity), Helper.getVNCurrency(quotationModel.getMoney())));
                        mBinding.switchLixi.setEnabled(true);
                        mBinding.llCurrentLixi.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.tvLixiReduce.setVisibility(View.INVISIBLE);
                        mBinding.switchLixi.setChecked(false);
                        mBinding.switchLixi.setEnabled(false);
                        mBinding.llCurrentLixi.setVisibility(View.INVISIBLE);
                    }
                    continue;
                }

                if (quotationModel.getCode().equals("product_money")
                        || (quotationModel.getCode().equals("promotion_money"))
                        || (!orderModel.isApply_service_price() && quotationModel.getCode().equals("service_money"))
                        || (quotationModel.getMoney() == 0 && !quotationModel.getCode().equals("ship_money") && !quotationModel.getCode().equals("service_money"))
                        || (quotationModel.getCode().equals("lixi_money_use")))
                    continue;

                mBinding.llOptionInfo.setVisibility(View.VISIBLE);
                new ExtraInfoController(activity, mStore, orderModel, mBinding.llOptionInfo, true, i, model.getList_quotation());
            }
        }

        if (model.getReward_point() == 0) {
            mBinding.tvDiscount.setVisibility(View.GONE);
            mBinding.tvMessageLixi.setVisibility(View.GONE);
        } else {
            mBinding.tvMessageLixi.setVisibility(View.VISIBLE);
            mBinding.tvDiscount.setVisibility(View.VISIBLE);
            mBinding.tvDiscount.setText(Html.fromHtml(Helper.getVNCurrency(model.getReward_point()) + " Lixi"));
        }

        totalMoney = model.getFinal_money();
        mBinding.tvPriceTotal.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_money()) + "đ"));
        if (model.getFinal_cashback() > 0) {
            mBinding.rlCashback.setVisibility(View.VISIBLE);
            mBinding.tvCashback.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_cashback()) + " Lixi"));
        }
    }

    private void showDataWithoutPromotionCode(ResponsePromotionModel model) {
        if (model.getList_quotation() != null) {
            //             set min height for parent
            if (mBinding.llOptionInfo.getChildCount() > 0) {
                int childHeight = mBinding.llOptionInfo.getChildAt(0).getHeight();
                int parentMinHeight = childHeight * mBinding.llOptionInfo.getChildCount();
                mBinding.llOptionInfo.setMinimumHeight(parentMinHeight);
            }
            mBinding.llOptionInfo.removeAllViews();
            for (int i = 0; i < model.getList_quotation().size(); i++) {
                QuotationModel quotationModel = model.getList_quotation().get(i);
                if (quotationModel.getCode().equals("lixi_money_avaiable")) {
                    mBinding.tvUsingLixi.setText(quotationModel.getName());
                    if (quotationModel.getMoney() != 0) {
                        mBinding.tvLixiReduce.setVisibility(View.VISIBLE);
                        mBinding.tvLixiReduce.setText(String.format(LanguageBinding.getString(R.string.order_confirm_reduce_lixi, activity), Helper.getVNCurrency(quotationModel.getMoney())));
                        mBinding.switchLixi.setEnabled(true);
                        mBinding.llCurrentLixi.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.tvLixiReduce.setVisibility(View.INVISIBLE);
                        mBinding.switchLixi.setChecked(false);
                        mBinding.switchLixi.setEnabled(false);
                        mBinding.llCurrentLixi.setVisibility(View.INVISIBLE);
                    }
                    continue;
                }

                if (quotationModel.getCode().equals("product_money")
                        || (quotationModel.getCode().equals("promotion_money"))
                        || (!orderModel.isApply_service_price() && quotationModel.getCode().equals("service_money"))
                        || (quotationModel.getMoney() == 0 && !quotationModel.getCode().equals("ship_money") && !quotationModel.getCode().equals("service_money"))
                        || (quotationModel.getCode().equals("lixi_money_use")))
                    continue;

                mBinding.llOptionInfo.setVisibility(View.VISIBLE);
                new ExtraInfoController(activity, mStore, orderModel, mBinding.llOptionInfo, true, i, model.getList_quotation());
            }
        }

        mBinding.tvPromotionCode.setText(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity));
        mBinding.tvPromotionCode.setTextColor(getResources().getColor(R.color.app_violet));
        mBinding.llPromotion.setBackgroundColor(getResources().getColor(R.color.white));
        mBinding.ivArrow.setVisibility(View.VISIBLE);
        mBinding.ivPromotion.setVisibility(View.VISIBLE);
        if (model.getFinal_cashback() < 1)
            mBinding.rlCashback.setVisibility(View.GONE);
        else {
            mBinding.rlCashback.setVisibility(View.VISIBLE);
            mBinding.tvCashback.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_cashback()) + " Lixi"));
        }
        mBinding.llCashbackCode.setVisibility(View.GONE);
        mBinding.tvPriceTotal.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_money()) + "đ"));
    }

    private void showDeleteAlertDialog() {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle(LanguageBinding.getString(R.string.order_confirm_delete_order, activity))
                .setMessage(String.format(LanguageBinding.getString(R.string.order_confirm_verify, activity), orderModel.getUuid()))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteOrder();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void deleteOrder() {
        DeliDataLoader.deleteOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Helper.showSuccessDialog(activity, LanguageBinding.getString(R.string.order_confirm_deleted_complete, activity));
                    HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                    cart.remove("" + orderModel.getStore_id());
                    Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                    finish();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, orderModel.getUuid());
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final DeliAddressModel location = (DeliAddressModel) intent.getSerializableExtra("LOCATION");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateLocationForOrder(location);
                }
            });
        }
    };

    private void updateLocationForOrder(final DeliAddressModel model) {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", uuid);
            params.put("receive_address", model.getFullAddress());
            params.put("receive_lat", model.getLat());
            params.put("receive_lng", model.getLng());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.updateOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    uuid = (String) object;
                    receiveAddress = AppPreferenceHelper.getInstance().getDefaultLocation();
                    getOrderDetail();
                } else {
                    AppPreferenceHelper.getInstance().setDefaultLocation(receiveAddress);
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private int getQuantityPromotion() {
        int count = 0;
        for (int i = 0; i < mStore.getList_promotion_code().size(); i++) {
            if (mStore.getList_promotion_code().get(i).getMin_bill_money() <= orderModel.getMoney()) {
                count++;
            }
        }
        return count;
    }

    private boolean isGotPaymentMethod = false;

    @Override
    public void onClick(View view) {
        Long mLixiReceived = 0L;
        switch (view.getId()) {
            case R.id.v_invisible:
                break;
            case R.id.rl_payment: {
                if (!isGotPaymentMethod) {
                    isGotPaymentMethod = true;
                    getPaymentMethod();
                }
            }
            break;
            case R.id.tv_order: {
                if (isRepay) {
                    if (checkStateOrder()) {
                        OrderTrackingActivity.startOrderTrackingActivity(activity, uuid, null, false);
                        finish();
                        return;
                    }
                    repayOrder();
                    return;
                }

                if (mStore.getList_promotion_code() == null || mStore.getList_promotion_code().size() == 0) {
                    confirmOrder();
                } else {
                    if (!isApplied)
                        showAlertPromotionDialog();
                    else {
                        confirmOrder();
                    }
                }
            }
            break;
            case R.id.iv_back: {
                onBackPressed();
            }
            break;
            case R.id.rl_promotion: {
                GlobalTracking.trackPromotionCodeStart(storeId);
                try {
//                    if (mStore.getList_promotion_code() == null || mStore.getList_promotion_code().size() == 0) {
//                        Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.deli_store_no_promotion, activity));
//                    } else {
//                        PromotionCodeListActivity.startPromotionCodeActivity(activity, storeId, tempMoneyTotal, uuid, mBinding.tvPromotionCode.getText().toString(), payment.getCode());
//                    }
                    PromotionCodeListActivity.startPromotionCodeActivity(activity, storeId, tempMoneyTotal, uuid, mBinding.tvPromotionCode.getText().toString(), payment.getCode());
                } catch (NullPointerException e) {
                    Crashlytics.logException(new CrashModel("StoreID: " + storeId));
                }
            }
            break;
            case R.id.ll_promotion: {
                PromotionCodeListActivity.startPromotionCodeActivity(activity, storeId, tempMoneyTotal, uuid, mBinding.tvPromotionCode.getText().toString(), payment.getCode());
            }
            break;
            case R.id.iv_delete: {
                isApplied = false;
                getCalculatePrice(null);
            }
            break;
            case R.id.tvEditInfo: {
                activity.unregisterReceiver(mReceiver);
                InfoOrderDialog dialog = new InfoOrderDialog(activity, false, false, false);
                dialog.show();
                dialog.setData(orderModel, false, mReceiveTimeChange);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        activity.registerReceiver(mReceiver, intentFilter);
                    }
                });
                dialog.setIDialogEventListener(new InfoOrderDialog.IDialogEvent() {
                    @Override
                    public void onOk(DeliAddressModel deliAddressModel, String name, String phone, String time) {
                        receiveAddress = deliAddressModel;
                        mReceiveTimeChange = time;
                        getOrderDetail();
                    }
                });
            }
            break;
            case R.id.tv_delete_order:
                showDeleteAlertDialog();
                break;
            case R.id.tv_message_lixi:
                LixiGiftDialog lixiGiftDialog = new LixiGiftDialog(activity, false, true, false);
                lixiGiftDialog.show();
                lixiGiftDialog.setData(mLixiReceived.intValue());
                break;
            case R.id.rl_info:
                Bundle bundle = new Bundle();
                bundle.putBoolean("LAST_LOCATION", false);
                try {
                    AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(activity, NewSearchLocationActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.switchLixi:
                mBinding.switchLixi.setEnabled(false);
                if (mBinding.switchLixi.isChecked()) {
                    mBinding.tvLixiReduce.setTextColor(getResources().getColor(R.color.black));
                } else {
                    mBinding.tvLixiReduce.setTextColor(getResources().getColor(R.color.color_gray));
                }
                if (orderModel.getState().equals(DeliOrderModel.PAYMENT)) {
//                    HashMap<String, Integer> countMap = Helper.getRepayCount(activity, "REPAY_FILE");
                    if (countMap.containsKey(orderModel.getUuid())) {
                        if (countMap.get(orderModel.getUuid()) < 2) {
                            if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() > 0) {
                                getCalculatePrice(orderModel.getList_promotion_code_apply().get(0).getCode());
                            } else {
                                getCalculatePrice(null);
                            }
                        } else {
                            getCalculatePrice(null);
                        }
                    } else {
                        getCalculatePrice(null);
                    }
                } else {
                    if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() > 0) {
                        getCalculatePrice(orderModel.getList_promotion_code_apply().get(0).getCode());
                    } else if (!mBinding.tvPromotionCode.getText().equals(LanguageBinding.getString(R.string.order_confirm_add_code_promotion, activity))) {
                        getCalculatePrice(mBinding.tvPromotionCode.getText().toString());
                    } else {
                        getCalculatePrice(null);
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        //For tracking
        try {
            id = Long.valueOf(uuid.replaceAll("UO", ""));
        } catch (Exception e) {
            id = 0l;
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        activity.unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PromotionCodeListActivity.REQUEST_CODE_PROMOTION_CODE) {
            if (resultCode == RESULT_OK) {
                ResponsePromotionModel model = (ResponsePromotionModel) data.getExtras().getSerializable(PromotionCodeListActivity.PROMOTION_RESPONSE);

                if (model == null)
                    return;

                isApplied = true;
//                showDataWithPromotionCode(model.getPromotion_code(), model);
                getCalculatePrice(model.getPromotion_code());
//                mBinding.tvPromotionCode.setText(model.getPromotion_code());
//                mBinding.llPromotion.setBackground(getResources().getDrawable(R.drawable.bg_button_violet));
//                mBinding.tvPromotionCode.setTextColor(getResources().getColor(R.color.white));
//                mBinding.ivDelete.setVisibility(View.VISIBLE);
//                mBinding.ivPromotion.setVisibility(View.GONE);
//                mBinding.ivArrow.setVisibility(View.GONE);
//                mBinding.llCashbackCode.setVisibility(View.VISIBLE);
//
//                if (model.getReduce_money() == 0) {
//                    mBinding.tvReduceMoney.setVisibility(View.GONE);
//                } else {
//                    mBinding.tvReduceMoney.setVisibility(View.VISIBLE);
//                    mBinding.tvReduceMoney.setText(Html.fromHtml(Helper.getVNCurrency(model.getReduce_money()) + "đ"));
//                }
//                if (model.getReward_point() == 0) {
//                    mBinding.tvDiscount.setVisibility(View.GONE);
//                    mBinding.tvMessageLixi.setVisibility(View.GONE);
//                } else {
//                    mLixiReceived = model.getReward_point();
//                    mBinding.tvMessageLixi.setVisibility(View.VISIBLE);
//                    mBinding.tvDiscount.setVisibility(View.VISIBLE);
//                    mBinding.tvDiscount.setText(Html.fromHtml(Helper.getVNCurrency(model.getReward_point()) + " Lixi"));
//                }
//
//                totalMoney = model.getFinal_money();
//                mBinding.tvPriceTotal.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_money()) + "đ"));
//                if (model.getFinal_cashback() > 0) {
//                    mBinding.rlCashback.setVisibility(View.VISIBLE);
//                    mBinding.tvCashback.setText(Html.fromHtml(Helper.getVNCurrency(model.getFinal_cashback()) + " Lixi"));
//                }
            }
        }
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_CHECKOUT;
    }

    @Override
    public void onBackPressed() {
        Intent lastOrderIntent = new Intent();
        try {
            lastOrderIntent.putExtra(ConstantValue.LAST_ORDER_TRANS_ID, orderModel.getUuid());
            lastOrderIntent.putExtra(ConstantValue.LAST_ORDER_PAYMENT_METHOD, payment.getMethodTracking());
            lastOrderIntent.putExtra(ConstantValue.LAST_ORDER_PROMOTION, isApplied);
            lastOrderIntent.putExtra(ConstantValue.LAST_ORDER_MONEY, orderModel.getMoney());
        } catch (NullPointerException e) {
            Crashlytics.logException(new CrashModel(new Gson().toJson(orderModel)));
        }
        setResult(RESULT_OK, lastOrderIntent);
        super.onBackPressed();
    }

    // region tracking
    // 5
    private void trackingCheckedOut(boolean isSuccess) {
        String coupon = isApplied ? TrackingConstant.CouponType.PROMOTION_CODE : "";
        GlobalTracking.trackCheckedOut(
                TrackingConstant.ContentType.STORE,
                storeId,
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                payment.getMethodTracking(),
                coupon,
                isSuccess,
                orderModel.getUuid(),
                orderModel.getMoney(),
                source
        );
    }

    // 10
    private void trackingPurchaseSuccess() {
        String coupon = isApplied ? TrackingConstant.CouponType.PROMOTION_CODE : "";

        GlobalTracking.trackPurchaseComplete(
                TrackingConstant.ContentType.STORE,
                storeId,
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                payment.getMethodTracking(),
                coupon,
                true,
                orderModel.getUuid(),
                orderModel.getMoney(),
                source
        );
    }

    // 11
    private void trackingOrderComplete() {
        String coupon = isApplied ? TrackingConstant.CouponType.PROMOTION_CODE : "";

        GlobalTracking.trackOrderComplete(
                TrackingConstant.ContentType.STORE,
                storeId,
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                payment.getMethodTracking(),
                coupon,
                true,
                orderModel.getUuid(),
                orderModel.getMoney(),
                source
        );
    }

    // 14
    private void trackingRepaySuccess() {
        String coupon = isApplied ? TrackingConstant.CouponType.PROMOTION_CODE : "";

        GlobalTracking.trackRepaySuccess(
                TrackingConstant.ContentType.STORE,
                storeId,
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                payment.getMethodTracking(),
                coupon,
                orderModel.getUuid(),
                orderModel.getMoney(),
                source
        );
    }
    // endregion
}