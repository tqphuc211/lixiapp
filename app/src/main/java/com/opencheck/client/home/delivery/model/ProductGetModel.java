package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductGetModel implements Serializable {
    private long id;
    private long quantity;
    private String note;
    private String name;
    private long price;
    private ArrayList<AddonGetModel> list_addon;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public ArrayList<AddonGetModel> getList_addon() {
        return list_addon;
    }

    public void setList_addon(ArrayList<AddonGetModel> list_addon) {
        this.list_addon = list_addon;
    }
}
