package com.opencheck.client.home.delivery.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopTransactionDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.ApiEntity;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.product.cart_old.SuccessCashDialog;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.utils.LanguageBinding;

import java.util.List;

public class TransactionDialog extends LixiDialog {

    public TransactionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private boolean isChecked = false;

    private RelativeLayout rlRoot;
    private RelativeLayout rl_close;
    private RelativeLayout rl_refresh;
    private WebView webView;
    private String payment_url;
    private ProgressBar progressBar;

    private String provider = "";
    private String currentUrl = "";
    private String uuid;

    //region setup dialog
    private LixiShopTransactionDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopTransactionDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    public void setData(String payment_link, String uuid) {
        payment_url = payment_link;
        currentUrl = payment_url;
        this.uuid = uuid;
        setListener();
        setViewData();
    }

    private void setListener() {
        rl_close.setOnClickListener(this);
        rl_refresh.setOnClickListener(this);
    }

    private void setViewData() {
        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setDomStorageEnabled(true);

        String mime = "text/html";
        String encoding = "utf-8";
//        String testHtml = "<p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><img src=\"http://picture.lixiapp.com/oc_web_editor/2017/10/10/b6da4462049113d350111ee1018b149051639d33.jpeg\" class=\"pull-left\" style=\"width: 100%;\"><span style=\"font-weight: 700\">FNgày phát hành: 19/9/2017</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Hạn order đợt 1: 25-27/8</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">Tình trạng: ORDER 7-15 NGÀY SAU PHÁT HÀNH LÀ CÓ HÀNG</span></span><span style=\"font-weight: 700\"></span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Giá trên là giá nhận trực tiếp tại HCM, Ship sẽ tính lúc các bạn order</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Đợt 1 mới có poster và có khả năng trúng special poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Sau đợt 1 shop vẫn tiếp tục nhận order nhưng ko chắc chắn là còn poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Trọn bộ bao gồm:</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- 4 version: L.O.V.E (được chọn)- 1 CD + Photobook 100p- Mini Book (HYYH The Notes)- Random Photocard (có 28 mẫu tất cả)- Sticker Pack- Special Photocard (firstpress có khả năng trúng)<span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">- Quà firstpress (đợt 1): Poster</span></p>";
//        payment_url = "https://sandbox.payoo.com.vn/bank-simulate.php?vpc_AdditionData=990313&vpc_Amount=60000&vpc_Command=PAY&vpc_CurrencyCode=VND&vpc_Locale=VN&vpc_MerchTxnRef=PY7327000031&vpc_Merchant=OPPREPAID&vpc_OrderInfo=PY7327000031&vpc_TransactionNo=133088&vpc_Version=2&vpc_SecureHash=AAF5D28BAFD10023EE68DB9CC2E5868E4AD89DC4C9966EBD9B91C6625FFDD0CF&system=0&vpc_ReturnURL=https%3a%2f%2fnewsandbox.payoo.com.vn%2fv2%2fbank%2ffinish%3fiframe%3d0%26_token%3d89aa17ffdca0e9e04a5077f2b0a6a9ae%26bank%3dOCB";
//        webView.loadUrl(payment_url);
//        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(activity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                Toast.makeText(activity, "SSL error " + error , Toast.LENGTH_LONG).show();
//                super.onReceivedSslError(view, handler, error);
//                handler.proceed();
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        dismiss();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Here put your code
                currentUrl = url;
                Log.d("Payment Url", url);

                if (payment_url.equals(url))
                    return false;//Allow WebView to load url
                else {
                    String rootUrl = getRootUrl(url);
                    if (rootUrl.equals(ApiEntity.getPaymentLink()) || rootUrl.equals(ApiEntity.getPaymentLinkSafe())) {
                        String urlParams = getUrlParams(url);
                        provider = getUrlProvider(url);
                        //checkShoppingStatus(urlParams);
                        checkStatus(urlParams);
                        return true; //Indicates WebView to NOT load the url;
                    } else {
                        Log.d("Wrong redirect url", url);
                        return false;
                    }
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl(payment_url);

    }

    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2017-10-06 15:23:44 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void findViews() {
        rlRoot = (RelativeLayout) findViewById(R.id.rlRoot);
        rl_close = (RelativeLayout) findViewById(R.id.rl_close);
        rl_refresh = (RelativeLayout) findViewById(R.id.rl_refresh);
        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    private String getMessage(String ms, List<String> par) {
        String res = ms;

        if (par != null) {
            for (int i = 0; i < par.size(); i++) {
                String holder = "{" + String.valueOf(i) + "}";

                if (res.contains(holder)) {
                    res = res.replace(holder, "<b>" + par.get(i) + "</b>");
                }
            }
        }

        return res;
    }

    //endregion

    //region Handle Shopping
    private void checkShoppingStatus(String params) {
        DataLoader.checkShoppingStatus(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    if (!isChecked) {
                        LixiShopBankTransactionResult transactionResult = (LixiShopBankTransactionResult) object;
                        handleTransactionResult(transactionResult);
                    }
                } else {
                    showErrorToast((String) object);
                }
                isChecked = true;
            }
        }, params + "&provider=" + provider);
    }

    private void checkStatus(String params) {
        if (params.contains("status=1")) {
            Bundle bundle = new Bundle();
            bundle.putString("uuid", uuid);
            Intent intent = new Intent(activity, OrderTrackingActivity.class);
            intent.putExtras(bundle);
            activity.startActivity(intent);
            onEventOK(true);
            dismiss();
        } else {
            FailedDialog dialogFailed = new FailedDialog(activity, false, false, false);
            dismiss();
            dialogFailed.show();
            dialogFailed.setData(uuid);
        }
    }

    private void handleTransactionResult(LixiShopBankTransactionResult transactionResult) {
        String ms = transactionResult.getMessage().get(0).getText();
        if (!transactionResult.getStatus()) {
            showPopupFailed(ms, transactionResult);
        } else {
            showPopupSuccess(ms, transactionResult);
            Bundle bundle = new Bundle();
            bundle.putString("uuid", transactionResult.getOrder_uuid());
            Intent intent = new Intent(activity, OrderTrackingActivity.class);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void showPopupSuccess(String ms, LixiShopBankTransactionResult transactionResult) {
        ms = getMessage(ms, transactionResult.getMessage().get(0).getParams());
        SuccessCashDialog dialog = new SuccessCashDialog(activity, false, false, false);
        dialog.setCancelable(false);
        onEventOK(true);
        dismiss();
        dialog.show();
        dialog.setData(LanguageBinding.getString(R.string.cash_evoucher_payment_success, activity), ms, "", transactionResult);

    }

    private void onEventOK(boolean ok) {
        if (eventListener != null) {
            eventListener.onOk(ok);
        }
    }

    private void showPopupFailed(String ms, LixiShopBankTransactionResult transactionResult) {
//        FailedCashDialog popupBuyFailed = new FailedCashDialog(activity, false, false, false);
//        popupBuyFailed.setCancelable(true);
//        onEventOK(true);
//        dismiss();
//        popupBuyFailed.show();
//        popupBuyFailed.setData(transactionResult.getTitle(), ms, "", transactionResult);
        FailedDialog dialogFailed = new FailedDialog(activity, false, false, false);
        onEventOK(true);
        dismiss();
        dialogFailed.show();
        dialogFailed.setData(uuid);
    }
    //endregion

    //region get incoming url and its params

    private String getRootUrl(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(0, j);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }

    private String getUrlParams(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                return url.substring(i, url.length());
            }
        }
        return "";
    }

    private String getUrlProvider(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(j + 1, i);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_close:
//                Intent intent = new Intent().setAction("reLoadLoadQuantityAllow");activity.sendBroadcast(intent);
                eventListener.onDimiss(true);
                dismiss();
                break;
            case R.id.rl_refresh:
                webView.loadUrl(currentUrl);
                break;
            default:
                break;
        }
    }

    //region dialog callback

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onOk(boolean success);

        void onDimiss(boolean isDimiss);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    //endregion
}
