package com.opencheck.client.home.rating.model;

import com.opencheck.client.models.LixiModel;

public class RatingResponse extends LixiModel {
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
