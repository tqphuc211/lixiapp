package com.opencheck.client.home.newlogin.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityInputPhoneBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.AutoDismissNotifyDialog;
import com.opencheck.client.home.account.new_layout.fragment.UserFragment;
import com.opencheck.client.home.newlogin.communicate.OnAgreeUpdate;
import com.opencheck.client.home.newlogin.communicate.OnForgotPassListener;
import com.opencheck.client.home.newlogin.communicate.OnLoginResult;
import com.opencheck.client.home.newlogin.communicate.OnResetPassResult;
import com.opencheck.client.home.newlogin.dialog.ConfirmResetPassDialog;
import com.opencheck.client.home.newlogin.dialog.CreateNewPassDialog;
import com.opencheck.client.home.newlogin.dialog.InputPasswordDialog;
import com.opencheck.client.home.newlogin.dialog.UpdateUserInfoDialog;
import com.opencheck.client.login.LoginActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.user.UserLoginModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONObject;

import java.util.Arrays;

import static com.opencheck.client.utils.ConstantValue.APP_REQUEST_CODE;

public class InputPhoneActivity extends LixiActivity implements View.OnClickListener {

    // Var
    public CallbackManager callbackManager;
    private String facebook_token = "";
    private boolean isResetPass = false;
    private String phone = "";
    private boolean isVerifyFacebook = false;
    private boolean isAccountKit = false;

    private ActivityInputPhoneBinding mBinding;

    private String ACTION_AFTER_LOGIN = "NONE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_input_phone);
        Intent intent = getIntent();
        if (intent != null) {
            isVerifyFacebook = intent.getBooleanExtra(LoginActivity.VERIFY_FACEBOOK, false);
            facebook_token = intent.getStringExtra(LoginActivity.FACEBOOK_TOKEN);
            ACTION_AFTER_LOGIN = intent.getStringExtra(ConstantValue.ACTION_AFTER_LOGIN);
        }
        initView();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.LOGIN_PHONE_GA;
    }

    private void initView() {
        if (isVerifyFacebook) {
            mBinding.linearFacebook.setVisibility(View.GONE);
            mBinding.txtLogout.setVisibility(View.VISIBLE);
        } else {
            mBinding.linearFacebook.setVisibility(View.VISIBLE);
            mBinding.txtLogout.setVisibility(View.GONE);
        }

        mBinding.txtNext.setOnClickListener(this);
        mBinding.linearFacebook.setOnClickListener(this);
        mBinding.btnBack.setOnClickListener(this);
        mBinding.txtLogout.setOnClickListener(this);

        setupFacebookButton();
        Helper.showSoftKeyBoard(activity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                hideKeyboard();
                finish();
                break;
            case R.id.txtNext:
                if (isVerifyFacebook) {
                    phone = mBinding.edtPhoneInput.getText().toString().trim();
                    phoneLogin(phone);
                } else {
                    checkPhone();
                }
                break;
            case R.id.linearFacebook:
                hideKeyboard();
                GlobalTracking.trackLoginStartEvent(TrackingConstant.Login.FACEBOOK);
                new AppPreferenceHelper(activity).setReceivedReward(false);
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
                break;
            case R.id.txtLogout:
                finish();
                break;

        }
    }

    // Func
    private void checkPhone() {
        phone = mBinding.edtPhoneInput.getText().toString().trim();
        // check text input
        if (!checkPhonePattern(phone)) {
            return;
        }

        JsonObject params = new JsonObject();
        params.addProperty("phone", phone);
        DataLoader.postCheckPhone(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    UserLoginModel userLoginModel = (UserLoginModel) object;
                    if (userLoginModel.isIs_phone_verified()) {
                        if (userLoginModel.isNeed_update()) {
                            // verify - update
                            phoneLogin(phone);
                        } else {
                            // input pass
                            gotoInputPass();
                        }
                    } else {
                        phoneLogin(phone);
                    }
                }
            }
        }, params);
    }

    private void setupFacebookButton() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                requestAccessToken(loginResult);
            }

            @Override
            public void onCancel() {
                showNotifyDialog(LanguageBinding.getString(R.string.login_error_cannot_login_facebook, activity));
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                showNotifyDialog(LanguageBinding.getString(R.string.login_error_login_facebook, activity));
            }
        });
    }

    private void requestAccessToken(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Helper.showLog("Facebook object: " + object);
                        if (object != null) {
                            facebook_token = AccessToken.getCurrentAccessToken().getToken();
                            handleFacebookToken(AccessToken.getCurrentAccessToken().getToken());
                        } else {
                            showNotifyDialog(LanguageBinding.getString(R.string.login_error_account_facebook, activity));
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday,first_name,last_name, gender, middle_name, link, name_format");//link,picture{url},
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void showNotifyDialog(String message) {
        AutoDismissNotifyDialog dialog = new AutoDismissNotifyDialog(activity, false, true, false);
        dialog.show();
        dialog.setMessage(message);
    }

    private void handleFacebookToken(String token) {
        // call api facebook
        JsonObject params = new JsonObject();
        params.addProperty("facebook_token", token);
        DataLoader.postLoginFacebook(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    UserModel userModel = (UserModel) object;
                    if (userModel.isIs_phone_verified()) {
                        // vào home
                        getResult(true);
                    } else {
                        // check account kit
                        isVerifyFacebook = true;
                        mBinding.linearFacebook.setVisibility(View.GONE);
                        mBinding.txtLogout.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, params);
    }

    // region pass screen
    private void gotoInputPass() {
        InputPasswordDialog dialog = new InputPasswordDialog(activity, mBinding.edtPhoneInput.getText().toString().trim());
        dialog.show();
        dialog.setOnLoginResult(new OnLoginResult() {
            @Override
            public void onLogin(boolean result, Object obj) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                getResult(result);
            }
        });

        dialog.setOnForgotPassListener(new OnForgotPassListener() {
            @Override
            public void onForgotPass(String phone) {
                confirmResetPass();
            }
        });
    }

    private void confirmResetPass() {
        ConfirmResetPassDialog dialog = new ConfirmResetPassDialog(activity);
        dialog.show();
        dialog.setData(phone);
        dialog.setOnAgreeUpdate(new OnAgreeUpdate() {
            @Override
            public void onAgree() {
                isResetPass = true;
                phoneLogin(phone);
            }
        });
    }

    UpdateUserInfoDialog updateDialog;

    private void gotoUpdateUserInfo() {
//        if (updateDialog == null) {
//            updateDialog = new UpdateUserInfoDialog(activity, phone);
//        }
//        updateDialog.cancel();
//        updateDialog.show();
//        updateDialog.setOnUpdateInfo(new OnLoginResult() {
//            @Override
//            public void onLogin(boolean result, Object obj) {
//                getResult(result);
//            }
//        });
//
//        updateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                getResult(true);
//            }
//        });
    }

    private void getResult(boolean res) {
        isVerifyFacebook = false;
        Intent data = new Intent();
        data.putExtra(ConstantValue.LOGIN_RESULT, res);
        setResult(RESULT_OK, data);
        InputPhoneActivity.this.finish();
    }
    // endregion

    private boolean checkPhonePattern(String phone) {
        if (phone == null || phone.equals("")) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_phone_null, activity), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            int length = phone.length();
            if (length == 10 || length == 11) {
                return true;
            } else {
                Toast.makeText(this, LanguageBinding.getString(R.string.login_error_phone_number, activity), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    private void phoneLogin(String phone) {
        if (!checkPhonePattern(phone)) {
            return;
        }

        isAccountKit = true;
        PhoneNumber phoneNumber = new PhoneNumber("84", phone, null);
        final Intent intent = new Intent(InputPhoneActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder
                        .setInitialPhoneNumber(phoneNumber)
                        .build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    private void handlePhoneLoginResult(Intent data) {
        AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        if (loginResult.getError() != null) {
            Helper.showErrorDialog(activity, loginResult.getError().getErrorType().getMessage());
        } else if (loginResult.wasCancelled()) {
            Helper.showErrorDialog(activity, "Login Cancelled!");
        } else {
            // đã xác thực
            handleGetUserToken(AccountKit.getCurrentAccessToken().getToken());
        }
    }

    private void handleGetUserToken(String token) {
        JsonObject params = new JsonObject();
        params.addProperty("accountkit_token", token);

        DataLoader.postGetUserToken(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    GlobalTracking.trackLoggedInEvent(TrackingConstant.Login.ACCOUNT_KIT);
                    UserFragment.UPDATE_USER_INFO = true;
                    getResult(true);
//                    if (ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_BUY_EVOUCHER)
//                            || ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_EXCHANGE_EVOUCHER)
//                            || ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_EXCHANGE_CLASSIC_PRODUCT)
//                            || ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_ORDER_DELI)
//                            || ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_SAVE_LOCATION)
//                            || ACTION_AFTER_LOGIN.equals(ConstantValue.ACTION_NOTIFICATION)) {
//                        UserFragment.UPDATE_USER_INFO = true;
//                        getResult(true);
//                    } else {
//                        gotoUpdateUserInfo();
//                    }
                } else {
                    Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, params);
    }

    private void handleResetPass(Intent data) {
        final AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        if (loginResult.getError() != null) {
            Helper.showErrorDialog(activity, loginResult.getError().getErrorType().getMessage());
        } else if (loginResult.wasCancelled()) {
            Helper.showErrorDialog(activity, "Login Cancelled!");
        } else {
            // đã xác thực
            final JsonObject param = new JsonObject();
            param.addProperty("accountkit_token", loginResult.getAccessToken().getToken());

            DataLoader.postForgotPass(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        UserModel userModel = (UserModel) object;
                        if (userModel.isNeed_update()) {
                            getResult(true);
                        } else {
                            gotoCreateNewPass(loginResult.getAccessToken().getToken());
                        }
                    } else {
                        Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, param);
        }
    }

    private void handleFacebookLogin(Intent data) {
        AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        if (loginResult.getError() != null) {
            Helper.showErrorDialog(activity, loginResult.getError().getErrorType().getMessage());
        } else if (loginResult.wasCancelled()) {
            Helper.showErrorDialog(activity, "Login Cancelled!");
        } else {
            // put login phone
            final JsonObject param = new JsonObject();
            param.addProperty("accountkit_token", AccountKit.getCurrentAccessToken().getToken());

            DataLoader.putLoginPhone(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        GlobalTracking.trackPhoneVerify(isVerifyFacebook ? TrackingConstant.Login.FACEBOOK : TrackingConstant.Login.ACCOUNT_KIT);
                        if ((boolean) object) {
                            JsonObject params = new JsonObject();
                            params.addProperty("facebook_token", facebook_token);
                            DataLoader.postCheckFacebook(activity, new ApiCallBack() {
                                @Override
                                public void handleCallback(boolean isSuccess, Object object) {
                                    if (isSuccess) {
                                        GlobalTracking.trackLoggedInEvent(TrackingConstant.Login.FACEBOOK);
                                        getResult(true);
                                    } else {
                                        Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, params);
                        } else {
                            Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, param);
        }
    }

    private void gotoCreateNewPass(String token) {
        CreateNewPassDialog dialog = new CreateNewPassDialog(activity, token);
        dialog.show();
        dialog.setOnResetPassResult(new OnResetPassResult() {
            @Override
            public void onResetResult(boolean result) {
                getResult(result);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            Toast.makeText(this, LanguageBinding.getString(R.string.try_again, this), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case APP_REQUEST_CODE:
                isAccountKit = false;
                if (isResetPass) {
                    handleResetPass(data);
                } else {
                    if (isVerifyFacebook) {
                        handleFacebookLogin(data);
                    } else {
                        handlePhoneLoginResult(data);
                    }
                }
                break;
            case ConstantValue.LOGIN_RESULT_CODE:
                if (data.getBooleanExtra(ConstantValue.LOGIN_RESULT, false)) {
                    setResult(ConstantValue.LOGIN_RESULT_CODE, data);
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideKeyboard();
        if (!isAccountKit) {
            if (isVerifyFacebook) {
                LoginHelper.Logout(this);
            }
        }
    }
}
