package com.opencheck.client.home.account.new_layout.control;

import com.opencheck.client.home.account.new_layout.model.MerchantApplyUnion;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;

import java.util.ArrayList;

public class OperateListCode {
    private static OperateListCode INSTANCE;
    public static OperateListCode getInstance(){
        if (INSTANCE == null){
            INSTANCE = new OperateListCode();
        }
        return INSTANCE;
    }

    private ArrayList<LixiShopEvoucherBoughtModel> listEvoucher;
    private ArrayList<ArrayList<MerchantApplyUnion>> listMAU; // MerchantApplyUnion
    private MerchantApplyUnion mau;

    public OperateListCode() {
        if (listEvoucher == null){
            listEvoucher = new ArrayList<>();
        }

        if (listMAU == null){
            listMAU = new ArrayList<>();
        }
    }

    public void clearData(){
        listEvoucher.clear();
        listMAU.clear();
    }

    public ArrayList<LixiShopEvoucherBoughtModel> getListEvoucher(){
        return listEvoucher;
    }

    /**
     * Check các voucher trong list có cùng ít nhất 1 merchant không
     * @return
     */
    public boolean checkMerchant(){
        // List tất cả id
        listMAU.clear();
        listMAU = new ArrayList<>();
        groupMAU();
        ArrayList<Integer> listID = new ArrayList<>();
        for (int i = 0; i < listMAU.size(); i++){
            ArrayList<MerchantApplyUnion> tempList = listMAU.get(i);
            for (int j = 0; j < tempList.size(); j++){
                if (!listID.contains(tempList.get(j).getId())){
                    listID.add(tempList.get(j).getId());
                }
            }
        }

        for (int x = 0; x < listID.size(); x++){
            MerchantApplyUnion m = new MerchantApplyUnion();
            m.setId(listID.get(x));
            for (int y = 0; y < listMAU.size(); y++){
                if (!listMAU.get(y).contains(m)){
                    break;
                }else {
                    if (y == listMAU.size() - 1){
                        int pos = listMAU.get(y).indexOf(m);
                        mau = listMAU.get(y).get(pos);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void groupMAU(){
        for (int i = 0; i < listEvoucher.size(); i++){
            ArrayList<MerchantApplyUnion> listTemp = listEvoucher.get(i).getMerchant_apply_union();
            MerchantInfoModel merchantInfoModel = listEvoucher.get(i).getMerchant_info();
            MerchantApplyUnion mau = OperateObject.getInstance().parseData(merchantInfoModel, MerchantApplyUnion.class);
//            for (int j = 0; j < listTemp.size(); j++){
//                if (!listTemp.get(j).equals(mau)){
//                    if (j == listTemp.size() - 1){
//                        listTemp.add(mau);
//                    }
//                }
//            }
            if (!listTemp.contains(mau)){
                listTemp.add(mau);
            }
            this.listMAU.add(listTemp);
        }
    }

    public String getEvoucherIds(){
        String res = "";
        for (int i = 0; i < listEvoucher.size(); i++){
            res += listEvoucher.get(i).getId() + " ";
        }
        return res.trim().replace(" ", ",");
    }

    public MerchantApplyUnion getMAU(){
        return mau;
    }

}
