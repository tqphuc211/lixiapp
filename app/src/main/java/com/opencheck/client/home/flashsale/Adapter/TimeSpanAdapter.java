package com.opencheck.client.home.flashsale.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.RowHorizontalTimeBinding;
import com.opencheck.client.home.flashsale.Model.FlashSaleTime;
import com.opencheck.client.home.flashsale.View.TimerView;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.Date;

public class TimeSpanAdapter extends RecyclerView.Adapter<TimeSpanAdapter.TimeSpanViewHolder> {
    // Constructor
    private Context context;
    private ArrayList<FlashSaleTime> listFlashTime;
    private long currentTime;

    // Var
    private int checkedItem = 0;
    private static final String FORMAT = "%02d:%02d";
    private static boolean INIT = true;

    // Interface
    ItemClickListener itemClickListener;

    public TimeSpanAdapter(Context context, ArrayList<FlashSaleTime> listFlashTime, long currentTime, ItemClickListener itemClickListener) {
        this.context = context;
        this.listFlashTime = listFlashTime;
        this.currentTime = currentTime;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public TimeSpanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowHorizontalTimeBinding rowHorizontalTimeBinding =
                RowHorizontalTimeBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);
        return new TimeSpanViewHolder(rowHorizontalTimeBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(TimeSpanViewHolder holder, int position) {
        // check item click
        if (position == checkedItem) {
            holder.viewBottom.setVisibility(View.VISIBLE);
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.color_fs_main));
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.color_fs_main));
        } else {
            holder.viewBottom.setVisibility(View.INVISIBLE);
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.black));
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.black));
        }

        // set data
        if (listFlashTime.get(position).getStart_time() * 1000 <= currentTime) {
            holder.txtStatus.setText("Đang diễn ra");
        } else {
            holder.txtStatus.setText("Chuẩn bị bán");
        }

        if (position == checkedItem) {
            if (INIT) {
                itemClickListener.onItemCLick(position, "INIT", listFlashTime.get(position), 0L);
                INIT = false;
            }
        }

        Long t = listFlashTime.get(position).getStart_time() * 1000;
        Date date = new Date();
        date.setTime(t);
        holder.txtTime.setText(TimerView.formatTimer(date.getHours()) + ":" + TimerView.formatTimer(date.getMinutes()));
    }

    public void setData(ArrayList<FlashSaleTime> listTime, long currentTime) {
        this.listFlashTime = listTime;
        this.currentTime = currentTime;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listFlashTime.size();
    }

    public void onItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class TimeSpanViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtTime, txtStatus;
        View viewBottom;

        public TimeSpanViewHolder(View itemView) {
            super(itemView);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            viewBottom = (View) itemView.findViewById(R.id.viewBottom);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            checkedItem = getAdapterPosition();
            notifyDataSetChanged();
            if (itemClickListener != null) {
                itemClickListener.onItemCLick(checkedItem, "CLICK", listFlashTime.get(checkedItem), null);
            }
        }
    }
}
