package com.opencheck.client.home.product.gateway.result;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentAtBankSuccesssLayoutBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.new_layout.fragment.UserFragment;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.home.product.bank_card.PopupNoSavedCard;
import com.opencheck.client.home.product.bank_card.SavedBankCardAdapter;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by I'm Sugar on 5/24/2018.
 */

public class PaymentAtBankSuccessDialog extends LixiTrackingDialog {

    private ImageView img;
    private NestedScrollView scollView;
    private TextView tvPhone;
    private TextView tvProductName;
    private TextView tvProductPrice;
    private TextView tvTotalProduct;
    private TextView tvDetailTrans;
    private TextView tv_tam_tinh;
    private TextView full_price;
    private TextView tv_lixi_use;
    private TextView tv_tong_tien;
    private TextView tvTotalPrice;
    private TextView totalLixiBonus;
    private LinearLayout ll_payment_method;
    private TextView tv_payment_method;
    private RecyclerView rvLixiShop;
    private LinearLayout ll_action;
    private TextView tv_view_voucher;
    private TextView tvCancel;
    private TextView txtBankNumb;
    private ImageView imgBankLogo;
    private LinearLayout linearCard;

    private String phone = "";
    private ProductCashVoucherDetailModel productModel;
    private String paymentMethod;
    private int totalProduct = 0;
    private long totalPrice = 0;
    private long lixiDiscount = 0;
    private ProductHotAdapters productHotAdapter;
    private ArrayList<LixiHotProductModel> listProductLixiShop;
    private String orderID = "";
    private long lixi_discount = 0;
    private AppPreferenceHelper appPreferenceHelper;
    private TokenData tokenData;
    private boolean isSaveCard;


    public PaymentAtBankSuccessDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, false, _dimBackgroud, _titleBar);
    }

    private PaymentAtBankSuccesssLayoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PaymentAtBankSuccesssLayoutBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        appPreferenceHelper = new AppPreferenceHelper(activity);
        findViews();
    }

    private void findViews() {

        img = (ImageView) findViewById(R.id.img);
        scollView = (NestedScrollView) findViewById(R.id.scollView);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvProductPrice = (TextView) findViewById(R.id.tvProductPrice);
        tvDetailTrans = (TextView) findViewById(R.id.tvDetailTrans);
        tv_tam_tinh = (TextView) findViewById(R.id.tv_tam_tinh);
        full_price = (TextView) findViewById(R.id.full_price);
        tv_lixi_use = (TextView) findViewById(R.id.tv_lixi_use);
        tv_tong_tien = (TextView) findViewById(R.id.tv_tong_tien);
        ll_payment_method = (LinearLayout) findViewById(R.id.ll_payment_method);
        tv_payment_method = (TextView) findViewById(R.id.tv_payment_method);
        ll_action = (LinearLayout) findViewById(R.id.ll_action);
        tv_view_voucher = (TextView) findViewById(R.id.tv_view_voucher);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        txtBankNumb = (TextView) findViewById(R.id.txtBankNumb);
        imgBankLogo = (ImageView) findViewById(R.id.imgBankLogo);
        linearCard = (LinearLayout) findViewById(R.id.linearCard);

        tvTotalProduct = (TextView) findViewById(R.id.totalProduct);
        tvTotalPrice = (TextView) findViewById(R.id.totalPrice);
        totalLixiBonus = (TextView) findViewById(R.id.totalLixiBonus);
        rvLixiShop = (RecyclerView) findViewById(R.id.rv);


        tvCancel.setOnClickListener(this);
        tv_view_voucher.setOnClickListener(this);
        tvDetailTrans.setOnClickListener(this);
    }

    public void loadData(String phone, ProductCashVoucherDetailModel productModel, int totalProduct,
                         long totalPrice, long lixiDiscount, String orderID, String paymentMethod, long lixi_discount, TokenData tokenData, boolean isSaveCard) {
        this.phone = phone;
        this.productModel = productModel;
        this.totalProduct = totalProduct;
        this.totalPrice = totalPrice;
        this.lixiDiscount = lixiDiscount;
        this.orderID = orderID;
        this.paymentMethod = paymentMethod;
        this.lixi_discount = lixi_discount;
        this.tokenData = tokenData;
        this.isSaveCard = isSaveCard;
        initData();
    }

    private void initData() {
        try {
            String text = "Mã voucher sẽ được gửi SMS về số điện  <br> thoại " + "<b>" + phone + "</b>";
            tvPhone.setText(Html.fromHtml(text));
            tvProductName.setText(productModel.getName());
            tvProductPrice.setText(Helper.getVNCurrency(totalPrice/totalProduct) + "đ");
            tvTotalProduct.setText("x" + totalProduct);

            full_price.setText(Helper.getVNCurrency(totalPrice) + "đ");
            if (lixi_discount == 0) {
                tv_lixi_use.setText(Helper.getVNCurrency(lixi_discount) + "đ");
            } else {
                tv_lixi_use.setText("-" + Helper.getVNCurrency(lixi_discount) + "đ");
            }

            tvTotalPrice.setText(Helper.getVNCurrency(totalPrice - lixi_discount) + "đ");
            totalLixiBonus.setText("+" + Helper.getVNCurrency(lixiDiscount) + " Lixi");
            if (paymentMethod != null && paymentMethod.length() > 0) {
                tv_payment_method.setText(paymentMethod);

                if (paymentMethod.contains("Napas")) {
                    // napas
                    if (tokenData != null) {
                        if (isSaveCard) {
                            // nhập thẻ mới có lưu
                            linearCard.setVisibility(View.VISIBLE);
                            String[] format = SavedBankCardAdapter.formatString(tokenData.getCard().getNumber());
                            txtBankNumb.setText(format[0] + "-****-****-" + format[1]);
                            ImageLoader.getInstance().displayImage(tokenData.getCard().getLogo_url(), imgBankLogo);
                        }else {
                            // nhập thẻ mới không lưu
                            linearCard.setVisibility(View.GONE);
                        }
                    }else {
                        // chọn thẻ đã lưu
                        linearCard.setVisibility(View.GONE);
                        if (isSaveCard){
                            // lưu thẻ mới có mã số đã tồn tại
                            showNotification(LanguageBinding.getString(R.string.notification_label, activity), LanguageBinding.getString(R.string.noti_card_existed, activity));
                        }
                    }
                }else {
                    // không phải napas
                    linearCard.setVisibility(View.GONE);
                }
            } else ll_payment_method.setVisibility(View.GONE);
            listProductLixiShop = new ArrayList<>();
            productHotAdapter = new ProductHotAdapters(activity, listProductLixiShop, new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    {
                        onEventOK(true);
                        dismiss();
                    }
                }
            });
            rvLixiShop.setLayoutManager(new GridLayoutManager(activity, 2));
            rvLixiShop.setNestedScrollingEnabled(false);
            rvLixiShop.setAdapter(productHotAdapter);
            getLixiShopProduct();

            id = Long.valueOf(orderID.substring(orderID.indexOf("-") + 1, orderID.length()));
            trackScreen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotification(String title, String content){
        PopupNoSavedCard popupNoSavedCard = new PopupNoSavedCard(activity, false, false, false);
        popupNoSavedCard.show();
        popupNoSavedCard.setData(title, content);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvCancel) {
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            UserFragment.UPDATE_USER_INFO = true;
            activity.startActivity(intent);
            //dismiss();

        }
        if (view.getId() == R.id.tv_view_voucher) {
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            UserFragment.UPDATE_USER_INFO = true;
            intent.putExtra(HomeActivity.RESUME_NAVIGATE, 4);
            activity.startActivity(intent);
            //dismiss();
        }
        if (view.getId() == R.id.tvDetailTrans) {
            OrderShopDetailDialog orderShopDetailDialog = new OrderShopDetailDialog(activity, false, false, false);
            orderShopDetailDialog.show();
            orderShopDetailDialog.setData(orderID);

        }

    }


    private void getLixiShopProduct() {
        DataLoader.getLixiShopHotProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listProductLixiShop = (ArrayList<LixiHotProductModel>) object;
                    productHotAdapter.loadData(listProductLixiShop);

                }
            }
        }, 1);
    }

    private void onEventOK(boolean ok) {
        if (eventListener != null) {
            eventListener.onOk(ok);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onOk(boolean success);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    @Override
    public void onBackPressed() {
        onEventOK(true);
        dismiss();
    }
}
