package com.opencheck.client.home.delivery.dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.InfoOrderDialogBinding;
import com.opencheck.client.databinding.TimePickerDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.activities.NewSearchLocationActivity;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.OpenTimeModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class InfoOrderDialog extends LixiDialog {

    private DeliOrderModel orderModel;
    public static String address = "";
    private String mReceiveTime, mReceiveTimeChange;
    private StoreOfCategoryModel mStore = null;
    private long storeId;
    private boolean isCallFromEmptyLocation = false;
    private boolean isToday = false;

    public InfoOrderDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackground, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackground, _titleBar);
    }

    private InfoOrderDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.info_order_dialog, null, false);
        setContentView(mBinding.getRoot());
        initViews();
        setUpReceiver();
    }

    private void initViews() {
        mBinding.ivDeleteName.setOnClickListener(this);
        mBinding.ivDeletePhone.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.rlAddress.setOnClickListener(this);
        mBinding.rlShippingMethod.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);

        mBinding.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (!s.toString().equals(""))
//                    iv_delete_phone.setVisibility(View.VISIBLE);
//                else iv_delete_phone.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (!s.toString().equals(""))
//                    iv_delete_name.setVisibility(View.VISIBLE);
//                else iv_delete_name.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setData(DeliOrderModel orderModel, boolean isRequired, String receiveTimeChange) {
        this.orderModel = orderModel;
        this.isRequired = isRequired;
        this.mReceiveTimeChange = timeStamp = receiveTimeChange;
        this.storeId = orderModel.getStore_id();

        showMap();

        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        mReceiveTime = cart.get("" + storeId).getShip_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getShip_receive_time());

        if (this.orderModel.getReceive_date() == null) {
            mBinding.tvTime.setText(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity));
        } else {
            Calendar currentDate = Calendar.getInstance();
            Calendar receiveDate = Calendar.getInstance();
            receiveDate.setTimeInMillis(Long.parseLong(mReceiveTimeChange));
            int diff = Helper.daysBetween(currentDate, receiveDate);
            if (diff == 0)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, this.mReceiveTimeChange) + " - " + LanguageBinding.getString(R.string.today, activity)));
            else if (diff == 1)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, this.mReceiveTimeChange) + " - " + LanguageBinding.getString(R.string.tomorrow, activity)));
            else
                mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, this.mReceiveTimeChange));
        }
        mBinding.tvFullAddress.setText(orderModel.getReceive_address());

        mBinding.tvGetAddress.setPaintFlags(mBinding.tvGetAddress.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.etName.setText(this.orderModel == null ? UserModel.getInstance().getFullname() : this.orderModel.getReceive_name());
        mBinding.etName.setSelection(this.orderModel == null ? UserModel.getInstance().getFullname().length() : this.orderModel.getReceive_name().length());
        mBinding.etPhone.setText(this.orderModel == null ? UserModel.getInstance().getPhone() : this.orderModel.getReceive_phone());

        getStoreDetail();

    }

    public void setDataWithoutLocation(long storeId, String userName, String phone, String mReceiveTimeChanged) {
        this.mReceiveTimeChange = timeStamp = mReceiveTimeChanged;
        this.isCallFromEmptyLocation = true;
        this.storeId = storeId;

        mBinding.llGetAddress.setVisibility(View.VISIBLE);
        mBinding.rlUnknown.setVisibility(View.VISIBLE);
        mBinding.vAlpha.setVisibility(View.VISIBLE);

        showDefaultMap();

        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        mReceiveTime = cart.get("" + storeId).getShip_receive_time() == null ? null : String.valueOf(cart.get("" + storeId).getShip_receive_time());

        if (mReceiveTimeChanged == null) {
            mBinding.tvTime.setText(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity));
        } else {
            Calendar currentDate = Calendar.getInstance();
            Calendar receiveDate = Calendar.getInstance();
            receiveDate.setTimeInMillis(Long.parseLong(mReceiveTimeChanged));
            int diff = Helper.daysBetween(currentDate, receiveDate);
            if (diff == 0)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, this.mReceiveTimeChange) + " - " + LanguageBinding.getString(R.string.today, activity)));
            else if (diff == 1)
                mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, this.mReceiveTimeChange) + " - " + LanguageBinding.getString(R.string.tomorrow, activity)));
            else
                mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, this.mReceiveTimeChange));
        }

        mBinding.tvGetAddress.setPaintFlags(mBinding.tvGetAddress.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.etName.setText(userName);
        mBinding.etName.setSelection(userName.length());
        mBinding.etPhone.setText(phone);

        getStoreDetail();
    }

    private void showMap() {
        String url = "http://maps.google.com/maps/api/staticmap?markers=color:blue|" + this.orderModel.getReceive_lat() + "," + this.orderModel.getReceive_lng() + "&zoom=15&size=200x200&sensor=false";
        mBinding.rlUnknown.setVisibility(View.GONE);
        mBinding.llGetAddress.setVisibility(View.GONE);
        mBinding.llAddress.setVisibility(View.VISIBLE);
        mBinding.vAlpha.setVisibility(View.GONE);
        //ImageLoader.getInstance().displayImage(url, iv, LixiApplication.getInstance().optionsNomal);
    }

    private void showDefaultMap() {
        String url = "http://maps.google.com/maps/api/staticmap?" + DeliDataLoader.shipping_receive_lat + "," + DeliDataLoader.shipping_receive_lng + "&zoom=15&size=200x200&sensor=false";
        mBinding.rlUnknown.setVisibility(View.VISIBLE);
        mBinding.llGetAddress.setVisibility(View.VISIBLE);
        mBinding.llAddress.setVisibility(View.GONE);
        mBinding.vAlpha.setVisibility(View.VISIBLE);
        //ImageLoader.getInstance().displayImage(url, iv, LixiApplication.getInstance().optionsNomal);
    }

    private String timeStamp = null;
    private boolean isRequired = false;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayList<Long> timeStampList = new ArrayList<>();
    private ArrayList<OpenTimeModel> listTimeOfToday = new ArrayList<>();
    private ArrayList<OpenTimeModel> listTimeOfTomorrow = new ArrayList<>();

    private void showPickerDialog() {
        final TimePickerDialogBinding timePickerDialogBinding =
                TimePickerDialogBinding.inflate(
                        LayoutInflater.from(activity), null, false);
        final BottomSheetDialog dialog = new BottomSheetDialog(activity);

        if (!(timeStamp + "").equals("null")) {
            Calendar receiveChangeCal = Calendar.getInstance();
            Calendar currentCal = Calendar.getInstance();
            receiveChangeCal.setTimeInMillis(Long.parseLong(timeStamp));
            int diff = Helper.daysBetween(currentCal, receiveChangeCal);
            if (diff == 0) setUpSchedule(timePickerDialogBinding, true);
            else if (diff == 1) setUpSchedule(timePickerDialogBinding, false);
            else timePickerDialogBinding.vSpace.setVisibility(View.VISIBLE);
        } else setUpSchedule(timePickerDialogBinding, true);

        //set up dialog
        if (arrayList.size() > 0) {
            timePickerDialogBinding.np.setMinValue(0);
            timePickerDialogBinding.np.setMaxValue(arrayList.size() - 1);
            String[] arr = new String[arrayList.size()];
            timePickerDialogBinding.np.setDisplayedValues(arrayList.toArray(arr));
        }
        timePickerDialogBinding.np.setWrapSelectorWheel(true);
        timePickerDialogBinding.np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                index = newVal;
            }
        });

        timePickerDialogBinding.rgTime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int isChekced) {
                int id = radioGroup.getCheckedRadioButtonId();
                if (id == timePickerDialogBinding.rbToday.getId()) {
                    setUpSchedule(timePickerDialogBinding, true);
                } else {
                    setUpSchedule(timePickerDialogBinding, false);
                }
            }
        });

        timePickerDialogBinding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        timePickerDialogBinding.tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (index == 0 || index == -1) {
                    if (!arrayList.get(0).equalsIgnoreCase(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity))) {
                        timeStamp = String.valueOf(timeStampList.get(0));
                        if (timePickerDialogBinding.rbToday.isChecked())
                            mBinding.tvTime.setText(Html.fromHtml(arrayList.get(0) + " - " + LanguageBinding.getString(R.string.today, activity)));
                        else
                            mBinding.tvTime.setText(Html.fromHtml(arrayList.get(0) + " - " + LanguageBinding.getString(R.string.tomorrow, activity)));
                    } else {
                        timeStamp = null;
                        mBinding.tvTime.setText(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity));
                    }
                } else {
                    if (arrayList.get(0).equalsIgnoreCase(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity))) {
                        timeStamp = String.valueOf(timeStampList.get(index - 1));
                    } else timeStamp = String.valueOf(timeStampList.get(index));

                    if (timePickerDialogBinding.rbToday.isChecked())
                        mBinding.tvTime.setText(Html.fromHtml(arrayList.get(index) + " - " + LanguageBinding.getString(R.string.today, activity)));
                    else
                        mBinding.tvTime.setText(Html.fromHtml(arrayList.get(index) + " - " + LanguageBinding.getString(R.string.tomorrow, activity)));
                }

                dialog.dismiss();
            }
        });

        dialog.setContentView(timePickerDialogBinding.getRoot());
        dialog.show();
    }

    private void setUpSchedule(TimePickerDialogBinding timePickerDialogBinding, boolean isToday) {
        Calendar minTimeCal = Calendar.getInstance();
        Calendar maxTimeCal = Calendar.getInstance();
        Calendar currentTimeCal = Calendar.getInstance();
        Calendar tempTime = Calendar.getInstance();
        arrayList.clear();
        timeStampList.clear();
        index = -1;
        timePickerDialogBinding.llBody.setVisibility(View.VISIBLE);
        timePickerDialogBinding.vSpace.setVisibility(View.GONE);

        if (listTimeOfTomorrow.size() == 0)
            timePickerDialogBinding.rbTomorrow.setVisibility(View.GONE);

        if (isToday) {
            timePickerDialogBinding.rbTomorrow.setChecked(false);
            timePickerDialogBinding.rbToday.setChecked(true);
            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
            //cart.get("" + storeId).getShip_receive_time();

            Calendar storeCloseTimeCal = Calendar.getInstance();
            storeCloseTimeCal.setTimeInMillis(listTimeOfToday.get(listTimeOfToday.size() - 1).getClose_time());
            storeCloseTimeCal.set(Calendar.YEAR, currentTimeCal.get(Calendar.YEAR));
            storeCloseTimeCal.set(Calendar.MONTH, currentTimeCal.get(Calendar.MONTH));
            storeCloseTimeCal.set(Calendar.DAY_OF_MONTH, currentTimeCal.get(Calendar.DAY_OF_MONTH));

            // Lấy giới hạng max là min(giờ đóng app | thời gian đóng cửa lớn nhất hôm nay)
            Long maxTime = getMinTime(getDeliWorkingTime().getClose_time(), storeCloseTimeCal.getTimeInMillis());
            maxTimeCal.setTimeInMillis(maxTime);
            maxTimeCal.add(Calendar.HOUR_OF_DAY, -1);

            if (mReceiveTime == null) {
                minTimeCal.add(Calendar.MINUTE, 30);
//                tempTime.setTimeInMillis(listTimeOfToday.get(listTimeOfToday.size() - 1).getClose_time());
//                maxTimeCal.set(Calendar.HOUR_OF_DAY, tempTime.get(Calendar.HOUR_OF_DAY));
//                maxTimeCal.set(Calendar.MINUTE, tempTime.get(Calendar.MINUTE));
//                maxTimeCal.add(Calendar.HOUR_OF_DAY, -1);
                arrayList.add(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity));
            } else {
                // Có thời gian đặt hàng trước
                // Lấy giới hạn min là thời gian đặt hàng trước
                minTimeCal.setTimeInMillis(Long.parseLong(mReceiveTime));
            }
        } else {
            timePickerDialogBinding.rbTomorrow.setChecked(true);
            timePickerDialogBinding.rbToday.setChecked(false);

            if (mReceiveTime != null) {
                Calendar receive = Calendar.getInstance();
                receive.setTimeInMillis(Long.parseLong(mReceiveTime));
                Long firstTime = getSuitableOrderTime(mStore);
                if (firstTime != null) {
                    Calendar firstTimeCal = Calendar.getInstance();
                    firstTimeCal.setTimeInMillis(mStore.getDelay_after_open() * 60 * 1000 + firstTime);
                    if (Helper.daysBetween(currentTimeCal, firstTimeCal) > 0 && Helper.daysBetween(currentTimeCal, receive) > 0)
                        timePickerDialogBinding.rbToday.setVisibility(View.GONE);
                }
            }

            // Lấy giới hạn min là max(giờ mở app | thời gian mở cửa nhỏ nhất ngày mai | thời gian mở cửa lại)
            Long minTime;
            Calendar storeOpenTimeCal = Calendar.getInstance();
            storeOpenTimeCal.setTimeInMillis(listTimeOfTomorrow.get(0).getOpen_time());
            storeOpenTimeCal.set(Calendar.YEAR, currentTimeCal.get(Calendar.YEAR));
            storeOpenTimeCal.set(Calendar.MONTH, currentTimeCal.get(Calendar.MONTH));
            storeOpenTimeCal.set(Calendar.DAY_OF_MONTH, currentTimeCal.get(Calendar.DAY_OF_MONTH) + 1);

            Calendar lixiOpenTimeCal = Calendar.getInstance();
            lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
            lixiOpenTimeCal.set(Calendar.DAY_OF_MONTH, currentTimeCal.get(Calendar.DAY_OF_MONTH) + 1);

            if (mStore.getState().equals("paused") && mStore.getEnd_pause_time() != null) {
                minTime = getMaxTime(getMaxTime(lixiOpenTimeCal.getTimeInMillis(), storeOpenTimeCal.getTimeInMillis()), mStore.getEnd_pause_time());
            } else {
                minTime = getMaxTime(lixiOpenTimeCal.getTimeInMillis(), storeOpenTimeCal.getTimeInMillis());
            }

            minTimeCal.setTimeInMillis(minTime);
            minTimeCal.add(Calendar.MINUTE, mStore.getDelay_after_open());

            // Lấy giới hạn max là min(giờ đóng app | thời gian đóng cửa lớn nhất ngày mai)
            Calendar storeCloseTimeCal = Calendar.getInstance();
            storeCloseTimeCal.setTimeInMillis(listTimeOfTomorrow.get(listTimeOfTomorrow.size() - 1).getClose_time());
            storeCloseTimeCal.set(Calendar.YEAR, currentTimeCal.get(Calendar.YEAR));
            storeCloseTimeCal.set(Calendar.MONTH, currentTimeCal.get(Calendar.MONTH));
            storeCloseTimeCal.set(Calendar.DAY_OF_MONTH, currentTimeCal.get(Calendar.DAY_OF_MONTH) + 1);

            Calendar lixiCloseTimeCal = Calendar.getInstance();
            lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());
            lixiCloseTimeCal.set(Calendar.DAY_OF_MONTH, currentTimeCal.get(Calendar.DAY_OF_MONTH) + 1);

            Long maxTime = getMinTime(lixiCloseTimeCal.getTimeInMillis(), storeCloseTimeCal.getTimeInMillis());
            maxTimeCal.setTimeInMillis(maxTime);
            maxTimeCal.add(Calendar.HOUR_OF_DAY, -1);
        }

        if (mReceiveTime != null && minTimeCal.compareTo(maxTimeCal) >= 0) {
            timePickerDialogBinding.rbTomorrow.setVisibility(View.GONE);
            timePickerDialogBinding.llBody.setVisibility(View.GONE);
            timePickerDialogBinding.vSpace.setVisibility(View.VISIBLE);
            return;
        }

        long diff = (maxTimeCal.getTimeInMillis() - minTimeCal.getTimeInMillis()) / (60 * 1000);
        long nLoop = (diff / 5) + 1;

        int checkDiff = minTimeCal.get(Calendar.MINUTE) % 5;
        if (checkDiff % 5 != 0) {
            if (checkDiff > 2)
                minTimeCal.set(Calendar.MINUTE, minTimeCal.get(Calendar.MINUTE) + 5 - checkDiff);
            else minTimeCal.set(Calendar.MINUTE, minTimeCal.get(Calendar.MINUTE) - checkDiff);
        }

        if (nLoop > 0) {
            for (int i = 0; i < nLoop; i++) {
                Calendar c = Calendar.getInstance();
                c.setTime(minTimeCal.getTime());
                c.add(Calendar.MINUTE, 5 * i);
                if (checkSuitableTime(c.getTimeInMillis(), isToday)) {
                    String time = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(c.getTime().getTime()));
                    arrayList.add(time);
                    timeStampList.add(c.getTime().getTime());
                }
            }
        }

        timePickerDialogBinding.np.setDisplayedValues(null);
        timePickerDialogBinding.np.setMinValue(0);
        timePickerDialogBinding.np.setMaxValue(arrayList.size() == 0 ? 0 : arrayList.size() - 1);
        String[] arr = new String[arrayList.size()];
        timePickerDialogBinding.np.setDisplayedValues(arrayList.toArray(arr));
        timePickerDialogBinding.np.setWrapSelectorWheel(true);
        timePickerDialogBinding.np.setValue(0);
    }

    private boolean checkSuitableTime(Long timestamp, boolean isToday) {
        ArrayList<OpenTimeModel> times;
        if (isToday)
            times = new ArrayList<>(listTimeOfToday);
        else times = new ArrayList<>(listTimeOfTomorrow);

        Calendar comparedTimeCal = Calendar.getInstance();
        comparedTimeCal.setTimeInMillis(timestamp);

        for (OpenTimeModel item : times) {
            Calendar storeOpenTimeCal = Calendar.getInstance();
            Calendar storeCloseTimeCal = Calendar.getInstance();

            storeOpenTimeCal.setTimeInMillis(item.getOpen_time());
            storeOpenTimeCal.set(Calendar.YEAR, comparedTimeCal.get(Calendar.YEAR));
            storeOpenTimeCal.set(Calendar.MONTH, comparedTimeCal.get(Calendar.MONTH));
            storeOpenTimeCal.set(Calendar.DAY_OF_MONTH, comparedTimeCal.get(Calendar.DAY_OF_MONTH));

            storeCloseTimeCal.setTimeInMillis(item.getClose_time());
            storeCloseTimeCal.set(Calendar.YEAR, comparedTimeCal.get(Calendar.YEAR));
            storeCloseTimeCal.set(Calendar.MONTH, comparedTimeCal.get(Calendar.MONTH));
            storeCloseTimeCal.set(Calendar.DAY_OF_MONTH, comparedTimeCal.get(Calendar.DAY_OF_MONTH));

            if (comparedTimeCal.compareTo(storeOpenTimeCal) >= 0 && comparedTimeCal.compareTo(storeCloseTimeCal) < 0) {
                return true;
            }
        }

        return false;
    }

    private OpenTimeModel getDeliWorkingTime() {
        String deli_working_time = DeliConfigModel.DELI_WORKING_TIME_DEFAULT;
        DeliConfigModel deliWorkingTime = Helper.getConfigByKey(activity, DeliConfigModel.DELI_WORKING_TIME_KEY);
        if (deliWorkingTime != null) {
            deli_working_time = deliWorkingTime.getValue();
        }
        Calendar deliOpenTime = Calendar.getInstance();
        deliOpenTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[0]));
        deliOpenTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[0].trim().split(":")[1]));

        Calendar deliCloseTime = Calendar.getInstance();
        deliCloseTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[0]));
        deliCloseTime.set(Calendar.MINUTE, Integer.valueOf(deli_working_time.split("-")[1].trim().split(":")[1]));
        return new OpenTimeModel(deliOpenTime.get(Calendar.DAY_OF_WEEK), deliOpenTime.getTimeInMillis(), deliCloseTime.getTimeInMillis());
    }

    private Long getMinTime(Long t1, Long t2) {
        if (t1 == null && t2 == null)
            return null;
        if (t1 == null)
            return t2;
        if (t2 == null)
            return t1;
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(t1);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(t2);
        if (c1.compareTo(c2) < 0)
            return t1;
        return t2;
    }

    private Long getMaxTime(Long t1, Long t2) {
        if (t1 == null && t2 == null)
            return null;
        if (t1 == null)
            return t2;
        if (t2 == null)
            return t1;

        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(t1);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(t2);
        if (c1.compareTo(c2) < 0)
            return t2;
        return t1;
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.tvDone) {
            if (mBinding.etName.getText().toString().trim().length() > 0 && mBinding.etPhone.getText().toString().trim().length() > 0) {
                if (isCallFromEmptyLocation) {
                    eventListener.onOk(receiveInfo, mBinding.etName.getText().toString(), mBinding.etPhone.getText().toString(),
                            timeStamp);
                    dismiss();
                    return;
                }
                if (isRequired)
                    requireUpdateOrder();
                else updateOrder();

            } else
                Toast.makeText(activity, "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_SHORT).show();

        } else if (view == mBinding.ivBack) {
            dismiss();
        } else if (view == mBinding.ivDeletePhone) {
            mBinding.etPhone.setText("");
            mBinding.ivDeletePhone.setVisibility(View.GONE);
        } else if (view == mBinding.ivDeleteName) {
            mBinding.etName.setText("");
            mBinding.ivDeleteName.setVisibility(View.GONE);
        } else if (view == mBinding.rlAddress) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("LAST_LOCATION", false);
            try {
                AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(activity, NewSearchLocationActivity.class);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        } else if (view == mBinding.rlShippingMethod) {
            if (mStore != null) showPickerDialog();
        }
    }

    private int index = -1;
    private DeliAddressModel receiveInfo = null;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            receiveInfo = (DeliAddressModel) intent.getSerializableExtra("LOCATION");

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.llGetAddress.setVisibility(View.GONE);
                    mBinding.rlUnknown.setVisibility(View.GONE);
                    mBinding.vAlpha.setVisibility(View.GONE);
                    mBinding.llAddress.setVisibility(View.VISIBLE);
                    mBinding.tvAddress.setText(receiveInfo.getAddress());
                    mBinding.tvFullAddress.setText(receiveInfo.getFullAddress());
                }
            });
        }
    };

    private void setUpReceiver() {
        IntentFilter intentFilter = new IntentFilter("LOCATION_CHANGE");
        activity.registerReceiver(receiver, intentFilter);
    }

    private void updateOrder() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", orderModel.getUuid());
            params.put("receive_name", mBinding.etName.getText().toString());
            params.put("receive_phone", mBinding.etPhone.getText().toString());
            if (receiveInfo != null) {
                params.put("receive_address", receiveInfo.getFullAddress());
                params.put("receive_lat", receiveInfo.getLat());
                params.put("receive_lng", receiveInfo.getLng());
            }
            if (timeStamp != null)
                params.put("receive_date", timeStamp);
            else {
                if (index == -1) {
                    if (mBinding.tvTime.getText().equals(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity))) {
                        params.put("receive_date", JSONObject.NULL);
                    } else {
                        if (orderModel.getReceive_date() == null)
                            params.put("receive_date", JSONObject.NULL);
                        else
                            params.put("receive_date", String.valueOf(orderModel.getReceive_date()));
                    }
                } else params.put("receive_date", JSONObject.NULL);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.updateOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    if (receiveInfo == null) {
                        DeliAddressModel mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
                        eventListener.onOk(mLocation, mBinding.etName.getText().toString(), mBinding.etPhone.getText().toString(),
                                timeStamp);
                    } else {
                        getOrderDetail();
                    }
                    dismiss();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void requireUpdateOrder() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", orderModel.getUuid());
            params.put("type", "update_receive");

            JSONObject data = new JSONObject();
            data.put("receive_name", mBinding.etName.getText().toString());
            data.put("receive_phone", mBinding.etPhone.getText().toString());
            if (receiveInfo != null) {
                data.put("receive_address", receiveInfo.getFullAddress());
                data.put("receive_lat", receiveInfo.getLat());
                data.put("receive_lng", receiveInfo.getLng());
            }

            if (timeStamp == null)
                if (index == -1) {
                    if (mBinding.tvTime.getText().equals(LanguageBinding.getString(R.string.order_confirm_as_soon_as_possible, activity))) {
                        data.put("receive_date", JSONObject.NULL);
                    } else {
                        if (orderModel.getReceive_date() == null)
                            data.put("receive_date", JSONObject.NULL);
                        else data.put("receive_date", String.valueOf(orderModel.getReceive_date()));
                    }
                } else data.put("receive_date", JSONObject.NULL);
            else data.put("receive_date", timeStamp);

            params.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeliDataLoader.userUpdateOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    DeliAddressModel mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
                    eventListener.onOk(mLocation, mBinding.etName.getText().toString(), mBinding.etPhone.getText().toString(),
                            timeStamp);
                    dismiss();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void getOrderDetail() {
        DeliDataLoader.getUserOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    orderModel = (DeliOrderModel) object;
                    eventListener.onOk(receiveInfo, mBinding.etName.getText().toString(), mBinding.etPhone.getText().toString(),
                            timeStamp);
                }
            }
        }, orderModel.getUuid(), null);
    }

    private void getStoreDetail() {
        DeliAddressModel mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        DeliDataLoader.getStoreDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mStore = (StoreOfCategoryModel) object;
                    getLimitTimeOfStore(mStore);
                }
            }
        }, storeId, mLocation == null ? null : mLocation.getFullAddress(), mLocation == null ? null : mLocation.getLat(), mLocation == null ? null : mLocation.getLng());
    }

    private void getLimitTimeOfStore(StoreOfCategoryModel mStore) {
        Calendar cal = Calendar.getInstance();
        int mDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        for (OpenTimeModel item : mStore.getList_open_time()) {
            if (mDayOfWeek == item.getWeekday()) {
                listTimeOfToday.add(item);
            } else if (mDayOfWeek + 1 == item.getWeekday() || (mDayOfWeek == 7 && item.getWeekday() == 1)) {
                listTimeOfTomorrow.add(item);
            }
        }
    }

    private Long getOrderTime(StoreOfCategoryModel store) {
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        Calendar storeOpenTimeCal = Calendar.getInstance();
        Long storeOpenTime = getSuitableOpenTime(store);

        if (storeOpenTime != null)
            storeOpenTimeCal.setTimeInMillis(storeOpenTime);
        else return null;

        if (store.getState().equals("paused")) {
            Calendar endPauseTimeCal = Calendar.getInstance();
            if (store.getEnd_pause_time() != null) {
                endPauseTimeCal.setTimeInMillis(store.getEnd_pause_time());
            } else endPauseTimeCal.setTimeInMillis(System.currentTimeMillis());

            if (TimeUnit.MILLISECONDS.toDays(storeOpenTimeCal.getTimeInMillis() - lixiOpenTimeCal.getTimeInMillis()) > 1)
                return null;

            if (storeOpenTimeCal.compareTo(endPauseTimeCal) < 0)
                storeOpenTimeCal.setTimeInMillis(endPauseTimeCal.getTimeInMillis());
        }

        lixiOpenTimeCal.set(Calendar.DAY_OF_MONTH, storeOpenTimeCal.get(Calendar.DAY_OF_MONTH));
        lixiCloseTimeCal.set(Calendar.DAY_OF_MONTH, storeOpenTimeCal.get(Calendar.DAY_OF_MONTH));
        if (lixiCloseTimeCal.compareTo(storeOpenTimeCal) <= 0)
            return null;

        if (lixiOpenTimeCal.compareTo(storeOpenTimeCal) < 0)
            lixiOpenTimeCal.setTimeInMillis(storeOpenTimeCal.getTimeInMillis());
        lixiOpenTimeCal.add(Calendar.MINUTE, store.getDelay_after_open());

        return lixiOpenTimeCal.getTimeInMillis();
    }

    private Long getSuitableOpenTime(StoreOfCategoryModel store) {
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        Long todayOpenTime = getTodayOpenTime(store);
        if (todayOpenTime != null) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(todayOpenTime);

            if (lixiCloseTimeCal.compareTo(openTimeCal) < 0) {
                Long afterOpenTime = getOpenTimeAfterToday(store);
                if (afterOpenTime != null)
                    return afterOpenTime;
            }
            return todayOpenTime;
        } else {
            Long afterOpenTime = getOpenTimeAfterToday(store);
            if (afterOpenTime != null)
                return afterOpenTime;
        }
        return null;
    }

    private Long getTodayOpenTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();

        if (listTimeOfToday.size() == 0)
            return null;

        for (int i = 0; i < listTimeOfToday.size(); i++) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(listTimeOfToday.get(i).getOpen_time());
            openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            Calendar closeTimeCal = Calendar.getInstance();
            closeTimeCal.setTimeInMillis(listTimeOfToday.get(i).getClose_time());
            closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            if (store.getState().equals("paused")) {
                Calendar endPausedTimeCal = Calendar.getInstance();
                if (store.getEnd_pause_time() != null)
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                if (endPausedTimeCal.compareTo(closeTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
            } else {
                if (currentDateCal.compareTo(openTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
                else if (currentDateCal.compareTo(openTimeCal) >= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                    return currentDateCal.getTimeInMillis();
            }
        }

        return null;
    }

    private Long getOpenTimeAfterToday(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);

        if (store.getState().equals("paused")) {
            for (int i = 0; i < listTimeOfTomorrow.size(); i++) {
                Calendar openTimeCal = Calendar.getInstance();
                openTimeCal.setTimeInMillis(listTimeOfTomorrow.get(i).getOpen_time());
                openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar closeTimeCal = Calendar.getInstance();
                closeTimeCal.setTimeInMillis(listTimeOfTomorrow.get(i).getClose_time());
                closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar endPausedTimeCal = Calendar.getInstance();
                if (store.getEnd_pause_time() != null)
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                if (endPausedTimeCal.compareTo(closeTimeCal) < 0)
                    return openTimeCal.getTimeInMillis();
            }
        } else {
            if (listTimeOfTomorrow.size() > 0) {
                Calendar resCal = Calendar.getInstance();
                resCal.setTimeInMillis(listTimeOfTomorrow.get(0).getOpen_time());
                resCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                resCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                resCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                return resCal.getTimeInMillis();
            } else {
                // Nếu ngày mai không có khung giờ, tìm khung giờ gần nhất
                int i = 0, index = -1;
                int min = 9;
                while (i < store.getList_open_time().size()) {
                    int temp = store.getList_open_time().get(i).getWeekday();
                    if (temp > mDayOfWeek && min > temp - mDayOfWeek) {
                        min = temp - mDayOfWeek;
                        index = i;
                    }

                    if (temp == 1) {
                        index = i;
                    }
                    i++;
                }

                if (index != -1) {
                    Calendar resCal = Calendar.getInstance();
                    resCal.setTimeInMillis(store.getList_open_time().get(index).getOpen_time());
                    resCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                    resCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                    resCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) +
                            (store.getList_open_time().get(index).getWeekday() == 7 ? 7 - mDayOfWeek + 1 : store.getList_open_time().get(index).getWeekday() - mDayOfWeek));
                    return resCal.getTimeInMillis();
                }
            }
        }

        return null;
    }

    //region NEW
    private Long getSuitableOrderTime(StoreOfCategoryModel store) {
        Long res = getTodayActiveTime(store);
        if (res != null) {
            return res;
        }
        return getTomorrowActiveTime(store);
    }

    private Long getTodayActiveTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        if (listTimeOfToday.size() == 0)
            return null;

        //get lixi time
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        for (int i = 0; i < listTimeOfToday.size(); i++) {
            Calendar openTimeCal = Calendar.getInstance();
            openTimeCal.setTimeInMillis(listTimeOfToday.get(i).getOpen_time());
            openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            Calendar closeTimeCal = Calendar.getInstance();
            closeTimeCal.setTimeInMillis(listTimeOfToday.get(i).getClose_time());
            closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
            closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
            closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH));

            if (store.getState().equals("paused")) {
                if (store.getEnd_pause_time() != null) {
                    Calendar endPausedTimeCal = Calendar.getInstance();
                    endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());
                    if (endPausedTimeCal.compareTo(closeTimeCal) < 0) {
                        if (endPausedTimeCal.compareTo(openTimeCal) >= 0) {
                            openTimeCal.setTimeInMillis(endPausedTimeCal.getTimeInMillis());
                        }

                        if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                            if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                                return lixiOpenTimeCal.getTimeInMillis();
                        } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                            return openTimeCal.getTimeInMillis();
                        }
                    }

                } else {
                    if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                        if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                            return lixiOpenTimeCal.getTimeInMillis();
                    } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                        return openTimeCal.getTimeInMillis();
                    }
                }

            } else {
                if (currentDateCal.compareTo(openTimeCal) >= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                    openTimeCal.setTimeInMillis(currentDateCal.getTimeInMillis());

                if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                    if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0 && currentDateCal.compareTo(closeTimeCal) < 0)
                        return lixiOpenTimeCal.getTimeInMillis();
                } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0 && openTimeCal.compareTo(currentDateCal) >= 0) {
                    return openTimeCal.getTimeInMillis();
                }
            }
        }

        return null;
    }

    private Long getTomorrowActiveTime(StoreOfCategoryModel store) {
        Calendar currentDateCal = Calendar.getInstance();
        //get lixi time
        Calendar lixiOpenTimeCal = Calendar.getInstance();
        lixiOpenTimeCal.setTimeInMillis(getDeliWorkingTime().getOpen_time());
        Calendar lixiCloseTimeCal = Calendar.getInstance();
        lixiCloseTimeCal.setTimeInMillis(getDeliWorkingTime().getClose_time());

        if (listTimeOfTomorrow != null && listTimeOfTomorrow.size() > 0) {
            for (int i = 0; i < listTimeOfTomorrow.size(); i++) {
                Calendar openTimeCal = Calendar.getInstance();
                openTimeCal.setTimeInMillis(listTimeOfTomorrow.get(i).getOpen_time());
                openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                Calendar closeTimeCal = Calendar.getInstance();
                closeTimeCal.setTimeInMillis(listTimeOfTomorrow.get(i).getClose_time());
                closeTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
                closeTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));
                closeTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                closeTimeCal.set(Calendar.MINUTE, -8);

                lixiOpenTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);
                lixiCloseTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 1);

                if (store.getState().equals("paused")) {
                    if (store.getEnd_pause_time() != null) {
                        Calendar endPausedTimeCal = Calendar.getInstance();
                        endPausedTimeCal.setTimeInMillis(store.getEnd_pause_time());

                        if (endPausedTimeCal.compareTo(closeTimeCal) < 0) {
                            if (endPausedTimeCal.compareTo(openTimeCal) >= 0) {
                                openTimeCal.setTimeInMillis(endPausedTimeCal.getTimeInMillis());
                            }

                            if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                                if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                                    return lixiOpenTimeCal.getTimeInMillis();
                            } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                                return openTimeCal.getTimeInMillis();
                            }
                        }
                    }
                } else {
                    if (openTimeCal.compareTo(lixiOpenTimeCal) < 0) {
                        if (lixiOpenTimeCal.compareTo(closeTimeCal) <= 0)
                            return lixiOpenTimeCal.getTimeInMillis();
                    } else if (openTimeCal.compareTo(lixiCloseTimeCal) < 0) {
                        return openTimeCal.getTimeInMillis();
                    }
                }
            }
        }
        return null;
    }

    private Long getTheDayAfterTomorrowActiveTime(StoreOfCategoryModel store) {

        Calendar currentDateCal = Calendar.getInstance();
        int mDayOfWeek = currentDateCal.get(Calendar.DAY_OF_WEEK);

        // Nếu ngày mai không có khung giờ, tìm khung giờ gần nhất
        int i = 0, indexLeft = -1, indexRight = -1;
        int min = 15, max = -1;
        while (i < store.getList_open_time().size()) {
            int temp = store.getList_open_time().get(i).getWeekday();

            if (temp <= mDayOfWeek) {
                if (temp == 1) {
                    indexRight = i;
                    min = 0;
                } else if (max <= mDayOfWeek - temp) {
                    indexLeft = i;
                    max = mDayOfWeek - temp;
                }
            } else if (min > temp - mDayOfWeek) {
                indexRight = i;
                min = temp - mDayOfWeek;
            }

            i++;
        }

        if (indexLeft == -1 && indexRight == -1)
            return null;

        Calendar openTimeCal = Calendar.getInstance();
        openTimeCal.setTimeInMillis(store.getList_open_time().get(indexRight != -1 ? indexRight : indexLeft).getOpen_time());
        openTimeCal.set(Calendar.YEAR, currentDateCal.get(Calendar.YEAR));
        openTimeCal.set(Calendar.MONTH, currentDateCal.get(Calendar.MONTH));

        if (indexLeft != -1 && store.getList_open_time().get(indexLeft).getWeekday() == mDayOfWeek)
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + 7);
        else
            openTimeCal.set(Calendar.DAY_OF_MONTH, currentDateCal.get(Calendar.DAY_OF_MONTH) + ((store.getList_open_time().get(indexRight != -1 ? indexRight : indexLeft).getWeekday() - mDayOfWeek + 7) % 7));


        return openTimeCal.getTimeInMillis();
    }
    //endregion

    @Override
    protected void onStop() {
        super.onStop();
        activity.unregisterReceiver(receiver);
    }

    public interface IDialogEvent {
        void onOk(DeliAddressModel deliAddressModel, String name, String phone, String time);
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}