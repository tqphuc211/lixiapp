package com.opencheck.client.home.product.communicate;

public interface OnCreateNewCard {
    void onNew(boolean isSaveCard);
}
