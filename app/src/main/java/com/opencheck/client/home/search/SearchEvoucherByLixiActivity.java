package com.opencheck.client.home.search;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.databinding.ActivitySearchEvoucherByLixiBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.lixishop.AdapterProduct;
import com.opencheck.client.models.lixishop.LixiProductModel;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

public class SearchEvoucherByLixiActivity extends LixiActivity {

    private RecyclerView rclvVoucher;
    private RelativeLayout rlBack;
    private LinearLayout lnNoResult;
    private AdapterProduct adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<LixiProductModel> list_voucher = new ArrayList<>();
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager layoutManager;
    private TextView tv_message;
    private Boolean isLoading = false;
    private int page = 1;
    private int record_per_page = 20;
    private boolean isOutOfVoucherPage = false;

    private ActivitySearchEvoucherByLixiBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_evoucher_by_lixi);
        initView();
    }

    private void initView() {
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        rclvVoucher = (RecyclerView) findViewById(R.id.rclv_voucher);
        lnNoResult = (LinearLayout) findViewById(R.id.lnNoResult);
        tv_message = (TextView) findViewById(R.id.tv_message);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        initData();
    }

    private void initData() {
        rlBack.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isLoading = false;
                page = 1;
                list_voucher.clear();
                adapter.notifyDataSetChanged();
                isOutOfVoucherPage = false;
                startLoadData(mBinding.searchBar.getText());
            }
        });

        rclvVoucher.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                hideKeyboard();
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (rclvVoucher.computeVerticalScrollOffset() > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        startLoadData(mBinding.searchBar.getText());
                        isLoading = true;
                    }
                }
            }
        });

        mBinding.searchBar.setOnSearchEventListener(new SearchBar.OnSearchEventListener() {
            @Override
            public void onTextChange(@NonNull String text) {
                page = 1;
                isLoading = false;
                isOutOfVoucherPage = false;
                startLoadData(text);
            }

            @Override
            public void onComplete(@NonNull String text) {
                hideKeyboard();
            }
        });

        startLoadData(null);
    }

    private void startLoadData(final String str) {
        if (!isLoading)
            if (!isOutOfVoucherPage) {
                DataLoader.searchVoucherByLixi(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (isSuccess) {
                            handleGetVoucherSuccess(object, str);
                        } else {
                            isLoading = true;
                        }
                    }
                }, page, str);
            }
    }

    private void handleGetVoucherSuccess(Object objects, String key) {
        if (page == 1) {
            list_voucher = (ArrayList<LixiProductModel>) objects;
            if (list_voucher == null || list_voucher.size() == 0) {
                GlobalTracking.trackEventSearch(TrackingConstant.ContentType.REWARD, key, false);
            } else {
                GlobalTracking.trackEventSearch(TrackingConstant.ContentType.REWARD, key, true);
            }
            setOutOfVoucherPage(list_voucher);
            setAdapter();

        } else {
            List<LixiProductModel> list = (List<LixiProductModel>) objects;
            setOutOfVoucherPage(list);
            list_voucher.addAll(list);
            adapter.notifyDataSetChanged();
        }

        if (list_voucher == null || list_voucher.size() == 0) {
            lnNoResult.setVisibility(View.VISIBLE);
        } else {
            lnNoResult.setVisibility(View.GONE);
        }

        page++;
        isLoading = false;
    }

    private void setOutOfVoucherPage(List<LixiProductModel> list) {
        if (list.size() < record_per_page) {
            isOutOfVoucherPage = true;
        }
    }

    private void setAdapter() {
        adapter = new AdapterProduct(activity, list_voucher, R.layout.detail_voucher_row);
        rclvVoucher.setLayoutManager(layoutManager);
        rclvVoucher.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                hideKeyboard();
                activity.onBackPressed();
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.REWARD_SEARCH;
    }
}
