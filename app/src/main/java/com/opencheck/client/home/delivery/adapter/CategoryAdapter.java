package com.opencheck.client.home.delivery.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ArticleItemLayoutBinding;
import com.opencheck.client.home.delivery.activities.CategoryDetailActivity;
import com.opencheck.client.home.delivery.model.ArticleModel;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<ArticleModel> listCategory = new ArrayList<>();

    public CategoryAdapter(LixiActivity activity, ArrayList<ArticleModel> listCategory) {
        this.activity = activity;
        this.listCategory = listCategory;
    }

    public void setListCategory(ArrayList<ArticleModel> list) {
        if (list == null)
            return;
        this.listCategory = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ArticleItemLayoutBinding itemLayoutBinding = ArticleItemLayoutBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ArticleModel model = listCategory.get(position);
        ViewGroup.LayoutParams params = holder.rlItemCategory.getLayoutParams();
        params.width = activity.widthScreen / 5;
        holder.rlItemCategory.setLayoutParams(params);
        holder.tv_title.setText(model.getName());
        ImageLoader.getInstance().displayImage(model.getImage_link(), holder.iv, LixiApplication.getInstance().optionsNomal);
    }

    @Override
    public int getItemCount() {
        return listCategory == null ? 0 : listCategory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv;
        private RelativeLayout rlItemCategory;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_title = itemView.findViewById(R.id.tv_name_category);
            iv = itemView.findViewById(R.id.iv_category);
            rlItemCategory = itemView.findViewById(R.id.rlItemCategory);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("CATEGORY", listCategory.get(getAdapterPosition()));
                    Intent intent = new Intent(activity, CategoryDetailActivity.class);
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            });
        }
    }
}
