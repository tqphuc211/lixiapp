package com.opencheck.client.home.delivery.model;

public class MenuTitleModel {
    private String name;
    private boolean isSelected = false;

    public MenuTitleModel(String name) {
        this.name = name;
    }

    public MenuTitleModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
