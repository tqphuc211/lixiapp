package com.opencheck.client.home.LixiHomeV25.CategoryContent;

import android.graphics.Paint;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowLixiMainMerchantLayoutBinding;
import com.opencheck.client.home.LixiHomeV25.AdapterCategory;
import com.opencheck.client.home.LixiHomeV25.ControllerMerchant;
import com.opencheck.client.home.flashsale.Global.VoucherFlashSaleHolder;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.ProductCategoryModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class AdapterCategoryContent extends RecyclerView.Adapter {

    private LixiActivity activity;
    private String isChildCategory = "";

    public static final int CATEGORY = 0;
    public static final int MERCHANT = 1;
    public static final int VOUCHER = 2;
    public static final int VOUCHER_FS = -1;

    public static final int NUM_MERCHANT_SHOW = 3;

    private CategoryListHolder categoryListHolder;
    private MerchantListHolder merchantListHolder;

    private ArrayList<ProductCategoryModel> listCategory;
    private ArrayList<MerchantModel> listMerchant;

    public ArrayList<LixiShopEvoucherModel> getListVoucher() {
        return listVoucher;
    }

    private ArrayList<LixiShopEvoucherModel> listVoucher;

    private AdapterCategory categoryAdapter;

    //region setdata

    public void setListCategory(ArrayList<ProductCategoryModel> listCategory) {
        this.listCategory = listCategory;
        if (categoryAdapter != null) {
            categoryAdapter.setList_category(listCategory);
            if (listCategory != null)
                categoryAdapter.setChildCategory(true);
        }

    }


    public void setListVoucher(ArrayList<LixiShopEvoucherModel> listVoucher) {
        if (this.listVoucher == null)
            this.listVoucher = listVoucher;
        else
            this.listVoucher.addAll(listVoucher);
        notifyDataSetChanged();
    }

    public void setListMerchant(ArrayList<MerchantModel> listMerchant) {
        this.listMerchant = listMerchant;
        if (merchantListHolder != null)
            setviewDataListMerchant();
    }

    public void setviewDataListMerchant() {
        merchantListHolder.ll_list_merchant.removeAllViews();
        if (listMerchant == null || listMerchant.size() == 0) {
            //TODO hiển thị ko có data
            merchantListHolder.ll_view_all.setOnClickListener(null);
            merchantListHolder.tv_view_all.setText(LanguageBinding.getString(R.string.emty_data, activity));
        } else {
            if (listMerchant.size() > NUM_MERCHANT_SHOW) {
                merchantListHolder.ll_view_all.setVisibility(View.VISIBLE);
                merchantListHolder.tv_view_all.setText(LanguageBinding.getString(R.string.see_more, activity) + " (" + listMerchant.size() + ")");
            } else merchantListHolder.ll_view_all.setVisibility(View.GONE);
            int size = Math.min(listMerchant.size(), NUM_MERCHANT_SHOW);
            for (int i = 0; i < size; i++)
                new ControllerMerchant(activity, merchantListHolder.ll_list_merchant, listMerchant.get(i), i);
        }
    }

    public void showSubCategory(boolean show) {
        if (categoryListHolder == null || categoryListHolder.rootView == null)
            return;

        if (show)
            categoryListHolder.rootView.setVisibility(View.VISIBLE);
        else
            categoryListHolder.rootView.setVisibility(View.GONE);
    }

    //endregion

    public AdapterCategoryContent(LixiActivity activity, ArrayList<LixiShopEvoucherModel> listVoucher, String isChildCategory) {
        this.activity = activity;
        this.listVoucher = listVoucher;
        this.isChildCategory = isChildCategory;
    }

    //region RecyclerView Basic
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == CATEGORY) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lixi_main_category_layout, parent, false);
            categoryListHolder = new CategoryListHolder(view);


            if (listCategory != null) {
                categoryAdapter = new AdapterCategory(activity, listCategory);
                categoryAdapter.setChildCategory(true);
            }
            else
                categoryAdapter = new AdapterCategory(activity, null);
//            LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            GridLayoutManager layoutManager = new GridLayoutManager(activity, 5);
            categoryListHolder.rvCategory.setLayoutManager(layoutManager);
            categoryListHolder.rvCategory.setAdapter(categoryAdapter);

            if (isChildCategory.length() > 0)
                categoryListHolder.rootView.setVisibility(View.GONE);
            return categoryListHolder;
        }
        if (viewType == MERCHANT) {
            RowLixiMainMerchantLayoutBinding rowLixiMainMerchantLayoutBinding =
                    RowLixiMainMerchantLayoutBinding.inflate(LayoutInflater
                            .from(parent.getContext()),parent, false);
            merchantListHolder = new MerchantListHolder(rowLixiMainMerchantLayoutBinding.getRoot());

            if (listMerchant != null)
                setviewDataListMerchant();

            return merchantListHolder;
        }
        if (viewType == VOUCHER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lixi_main_voucher_layout, parent, false);
            VoucherHolder holder = new VoucherHolder(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (pos < listVoucher.size())
//                        CashEvoucherDetailActivity.startCashVoucherDetail(activity, listVoucher.get(pos).getId());
                        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, listVoucher.get(pos).getId(), TrackingConstant.ContentSource.VOUCHER_CATEGORY);
                }
            });
            return holder;
        }
        if (viewType == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherFsHolder = VoucherFlashSaleHolder.newHolder(activity, parent);
            return voucherFsHolder;
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < VOUCHER)
            return position;
        if (listVoucher != null && listVoucher.size() > (position - VOUCHER))
            if (listVoucher.get(position - VOUCHER).getFlash_sale_id() > 0)
                return VOUCHER_FS;
        return VOUCHER;
    }

    @Override
    public int getItemCount() {
        if (listVoucher == null)
            return VOUCHER;
        return listVoucher.size() + VOUCHER;
    }

    //endregion

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == CATEGORY) {
            if (isChildCategory.length() > 0)
                categoryListHolder.rootView.setVisibility(View.GONE);
        }
        if (viewType == MERCHANT) {

        }
        if (viewType == VOUCHER) {
            VoucherHolder voucherHolder = (VoucherHolder) holder;
            holder.itemView.setTag(position - VOUCHER);
            if (listVoucher.size() <= position - VOUCHER)
                return;
            LixiShopEvoucherModel voucher = listVoucher.get(position - VOUCHER);

            if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0)
                ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
            else {
                if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0)
                    ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
            }
            voucherHolder.tvVoucherName.setText(voucher.getName());

            voucherHolder.tvVoucherPrice.setText(Helper.getVNCurrency(voucher.getPayment_discount_price()) + "đ");
            if (voucher.getPayment_discount_price() != voucher.getPayment_price()) {
                voucherHolder.tvOldPrice.setVisibility(View.VISIBLE);
                voucherHolder.tvOldPrice.setText(Helper.getVNCurrency(voucher.getPayment_price()) + "đ");
                voucherHolder.tvOldPrice.setPaintFlags(voucherHolder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else voucherHolder.tvOldPrice.setVisibility(View.GONE);
            if (voucher.getCashback_price() > 0) {
                voucherHolder.tvLixiBonus.setVisibility(View.VISIBLE);
                voucherHolder.tvLixiBonus.setText(Helper.getVNCurrency(voucher.getCashback_price()) + " Lixi");
            } else voucherHolder.tvLixiBonus.setVisibility(View.GONE);

            voucherHolder.tvMerchantInfo.setText(voucher.getMerchant_name() + " | " + voucher.getCount_store() + " cửa hàng");
            voucherHolder.tvCountAttend.setText(voucher.getQuantity_order_done() + " " + LanguageBinding.getString(R.string.quantity_bought_customer, activity));

            if (voucher.getPayment_price() != voucher.getPayment_discount_price()) {
                voucherHolder.tv_percent_discount.setVisibility(View.VISIBLE);
                int percent = 100 - (int) (voucher.getPayment_discount_price() * 100 / voucher.getPayment_price());
                voucherHolder.tv_percent_discount.setText("-" + percent + "%");
            } else voucherHolder.tv_percent_discount.setVisibility(View.GONE);
        }


        if (getItemViewType(position) == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherHolder = (VoucherFlashSaleHolder) holder;
            if (listVoucher.size() <= position - VOUCHER)
                return;
            LixiShopEvoucherModel voucher = listVoucher.get(position - VOUCHER);

            VoucherFlashSaleHolder.bindViewHolder(voucherHolder, voucher);

        }
    }

    //region ViewHolder

    class CategoryListHolder extends RecyclerView.ViewHolder {
        View rootView;
        RecyclerView rvCategory;

        public CategoryListHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            rvCategory = itemView.findViewById(R.id.rvCategory);
        }
    }

    class MerchantListHolder extends RecyclerView.ViewHolder {
        private View rootView;
        private LinearLayout ll_merchant;
        private LinearLayout ll_view_all;
        private TextView tv_view_all;
        private LinearLayout ll_list_merchant;

        public MerchantListHolder(View itemView) {
            super(itemView);

            rootView = itemView;
            ll_merchant = (LinearLayout) itemView.findViewById(R.id.ll_merchant);
            tv_view_all = (TextView) itemView.findViewById(R.id.tv_view_all);
            ll_view_all = (LinearLayout) itemView.findViewById(R.id.ll_view_all);
            ll_list_merchant = (LinearLayout) itemView.findViewById(R.id.ll_list_merchant);
            ll_view_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClickViewAllMerchant();
                }
            });
        }
    }

    class VoucherHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        private View v_line;
        private ImageView imgLogoVoucher;
        private TextView tv_percent_discount;
        private TextView tvVoucherName;
        private TextView tvVoucherPrice;
        private TextView tvOldPrice;
        private TextView tvLixiBonus;
        private TextView tvMerchantInfo;
        private TextView tvCountAttend;
        private TextView tvDistance;


        public VoucherHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            rootView.setOnClickListener(this);
            v_line = itemView.findViewById(R.id.v_line);
            imgLogoVoucher = (ImageView) itemView.findViewById(R.id.imgLogoVoucher);
            tv_percent_discount = (TextView) itemView.findViewById(R.id.tv_percent_discount);
            tvVoucherName = (TextView) itemView.findViewById(R.id.tvVoucherName);

            tvVoucherPrice = (TextView) itemView.findViewById(R.id.tvVoucherPrice);
            tvOldPrice = (TextView) itemView.findViewById(R.id.tvOldPrice);
            tvLixiBonus = (TextView) itemView.findViewById(R.id.tvLixiBonus);
            tvMerchantInfo = (TextView) itemView.findViewById(R.id.tvMerchantInfo);
            tvCountAttend = (TextView) itemView.findViewById(R.id.tvCountAttend);

            tvDistance = (TextView) itemView.findViewById(R.id.tvDistance);
        }

        @Override
        public void onClick(View v) {
            if (v == rootView) {
//                CashEvoucherDetailActivity.startCashVoucherDetail(activity, listVoucher.get((int) v.getTag()).getId());
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, listVoucher.get((int) v.getTag()).getId(), TrackingConstant.ContentSource.VOUCHER_CATEGORY);
            }
        }
    }

//endregion

    public interface IAdapterCategoryContetnListener {
        void onClickViewAllMerchant();
    }

    public void setListener(IAdapterCategoryContetnListener listener) {
        this.listener = listener;
    }

    private IAdapterCategoryContetnListener listener;
}
