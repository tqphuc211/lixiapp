package com.opencheck.client.home.product.communicate;

import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;

public interface OnCheckFingerPrintListener {
    void onFailed();
    void onSucceeded(FingerprintManagerCompat.AuthenticationResult result);
    void onHelp(int helpMsgId, CharSequence helpString);
    void onError(int errMsgId, CharSequence errString);
}
