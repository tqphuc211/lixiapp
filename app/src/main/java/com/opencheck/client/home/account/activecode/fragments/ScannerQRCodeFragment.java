package com.opencheck.client.home.account.activecode.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.zxing.Result;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.activecode.dialogs.NoApplyDialog;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.Helper;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by vutha_000 on 3/26/2018.
 */

public class ScannerQRCodeFragment extends LixiFragment implements ZXingScannerView.ResultHandler{

    @Override
    public void onClick(View view) {

    }

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private EvoucherDetailModel evoucherModel;
    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private boolean mFlash;
    private boolean mAutoFocus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle state) {
        // Inflate the layout for this fragment
        evoucherModel = new Gson().fromJson(getArguments().getString("ev"), EvoucherDetailModel.class);
        scannerView = new ZXingScannerView(activity);
        if (activity instanceof ActiveCodeActivity)
            ((ActiveCodeActivity) activity).toolbar.setVisibility(View.VISIBLE);

        if (state != null) {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
        } else {
            mFlash = false;
            mAutoFocus = true;
        }

        return scannerView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
        scannerView.setFlash(mFlash);
        scannerView.setAutoFocus(mAutoFocus);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menuItem;
        if (mFlash) {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_flash, 0, R.string.flash_off);
        }
        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);
        if (mAutoFocus) {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_on);
        } else {
            menuItem = menu.add(Menu.NONE, R.id.menu_auto_focus, 0, R.string.auto_focus_off);
        }

        MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.menu_flash:
                mFlash = !mFlash;
                if (mFlash) {
                    item.setTitle(R.string.flash_on);
                } else {
                    item.setTitle(R.string.flash_off);
                }
                scannerView.setFlash(mFlash);
                return true;
            case R.id.menu_auto_focus:
                mAutoFocus = !mAutoFocus;
                if (mAutoFocus) {
                    item.setTitle(R.string.auto_focus_on);
                } else {
                    item.setTitle(R.string.auto_focus_off);
                }
                scannerView.setAutoFocus(mAutoFocus);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    private void startLoadData(String storeId){
        DataLoader.getStoreApplyCashCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if(isSuccess){
                    activity.onBackPressed();
                    StoreApplyModel model = (StoreApplyModel) object;
                    if(model.isAllow_user_send_request_active()) {
                        Bundle data = new Bundle();
                        data.putString("sa", new Gson().toJson(model));
                        data.putString("ev", new Gson().toJson(evoucherModel));
                        ConfirmActiveCodeFragment activeCodeFragment = new ConfirmActiveCodeFragment();
                        activeCodeFragment.setArguments(data);
                        ((ActiveCodeActivity) activity).replaceFragment(activeCodeFragment);
                    }else{
                        NoApplyDialog dialog = new NoApplyDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData(model, evoucherModel);
                    }

                }
                else{
                    Helper.showErrorDialog(activity, object.toString());
                    activity.onBackPressed();
                }
            }
        }, storeId);
    }

    @Override
    public void handleResult(Result result) {
        Log.d("mapi", "open camera");
        startLoadData(result.getText());
    }
}
