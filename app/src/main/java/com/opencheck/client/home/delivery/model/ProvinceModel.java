package com.opencheck.client.home.delivery.model;

public class ProvinceModel {
    private String name;
    private String district;

    public ProvinceModel(String name, String district) {
        this.name = name;
        this.district = district;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
