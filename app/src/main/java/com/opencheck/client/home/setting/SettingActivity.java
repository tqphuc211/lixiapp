package com.opencheck.client.home.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivitySettingBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.newlogin.dialog.UpdateUserInfoDialog;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;

public class SettingActivity extends LixiActivity {

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.PROFILE_SETTINGS;
    }

    private RelativeLayout rl_update_info;
    private RelativeLayout rl_setting;
    private RelativeLayout rl_instruction;
    private RelativeLayout rl_using_law;
    private RelativeLayout rl_policy;
    private RelativeLayout rl_contact;
    private TextView tv_log_out;
    private ImageView iv_back;

    private ActivitySettingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);
        initView();
    }

    private void initView() {
        findView();
        setClickListenerForLayout();
        //rendering user profile
        getUserProfile(false);
    }

    private void findView() {
        rl_update_info = findViewById(R.id.rl_update_info);
        rl_setting = findViewById(R.id.rl_setting);
        rl_instruction = findViewById(R.id.rl_instruction);
        rl_using_law = findViewById(R.id.rl_using_law);
        rl_policy = findViewById(R.id.rl_policy);
        rl_contact = findViewById(R.id.rl_contact);
        tv_log_out = findViewById(R.id.tv_log_out);
        iv_back = findViewById(R.id.iv_back);
    }

    private void setClickListenerForLayout() {
        rl_update_info.setOnClickListener(this);
        rl_setting.setOnClickListener(this);
        rl_instruction.setOnClickListener(this);
        rl_using_law.setOnClickListener(this);
        rl_policy.setOnClickListener(this);
        tv_log_out.setOnClickListener(this);
        rl_contact.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        WebviewDialog popupWebview;
        switch (view.getId()) {
            case R.id.rl_update_info:
                if (UserModel.getInstance() != null) {
                    if (UserModel.getInstance().isNeed_update()) {
                        UpdateUserInfoDialog updateUserInfoDialog = new UpdateUserInfoDialog(activity, UserModel.getInstance().getPhone());
                        updateUserInfoDialog.show();
                    } else {
                        activity.startActivity(new Intent(activity, UpdateInfoActivity.class));
                    }
                }
                break;
            case R.id.rl_setting:
                activity.startActivity(new Intent(activity, ConfigurationActivity.class));
                break;
            case R.id.rl_instruction:
                InstructionDialog popupInstruction = new InstructionDialog(activity, false, true, false);
                popupInstruction.show();
                break;
            case R.id.rl_using_law:
                popupWebview = new WebviewDialog(activity, false, true, false);
                popupWebview.show();
                popupWebview.setData("https://m.lixiapp.com/terms", LanguageBinding.getString(R.string.profile_setting_term, this));
                break;
            case R.id.rl_policy:
                popupWebview = new WebviewDialog(activity, false, true, false);
                popupWebview.show();
                popupWebview.setData("https://m.lixiapp.com/policy", LanguageBinding.getString(R.string.profile_setting_info_policy, this));
                break;
            case R.id.tv_log_out:
                final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setTitle(LanguageBinding.getString(R.string.login_logout, this));
                dialog.setNegativeButton(LanguageBinding
                        .getString(R.string.no, this), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                dialog.setPositiveButton(LanguageBinding
                        .getString(R.string.yes, this), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        GlobalTracking.trackLoggedOutEvent();
                        LoginHelper.Logout(activity);
                    }
                });
                dialog.show();
                break;
            case R.id.rl_contact:
                activity.startActivity(new Intent(activity, FeedBackActivity.class));
                break;
            case R.id.iv_back:
                activity.onBackPressed();
                break;

        }
    }

    //region Background Processing

    private void getUserProfile(final Boolean isRefreshView) {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                } else {

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);
//            ((HomeActivity) activity).navigation.setVisibility(View.VISIBLE);

        updateUserModel();

    }

    private void updateUserModel() {
        getUserProfile(true);
    }

    //endregion
}
