package com.opencheck.client.home.flashsale.View;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewTimerBinding;
import com.opencheck.client.home.flashsale.Interface.OnFinishTimerListener;
import com.opencheck.client.home.flashsale.Interface.OnStartTimerListener;

public class TimerView extends ConstraintLayout {
    // Constructor
    private Context context;


    // Var
    private CountDownTimer timer;
    private OnStartTimerListener onStartTimerListener;
    private OnFinishTimerListener onFinishTimerListener;

    private long untilFinishMillis = 0;

    public TimerView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public TimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private ViewTimerBinding mBinding;

    private void initView(){
        mBinding = ViewTimerBinding.inflate(LayoutInflater.from(getContext()),
                this, true);
    }

    public void startTimer(long millisSecond){
        cancelTimer();
        untilFinishMillis = millisSecond;
        if (millisSecond <= 0){
            mBinding.txtHour.setText(formatTimer(0));
            mBinding.txtMinute.setText(formatTimer(0));
            mBinding.txtSecond.setText(formatTimer(0));
            return;
        }

        timer = new CountDownTimer(millisSecond, 1000) {
            @Override
            public void onTick(long l) {
                untilFinishMillis = l;
                int[] arrTime = getTimer(l);
                mBinding.txtHour.setText(formatTimer(arrTime[0]));
                mBinding.txtMinute.setText(formatTimer(arrTime[1]));
                mBinding.txtSecond.setText(formatTimer(arrTime[2]));
            }

            @Override
            public void onFinish() {
                untilFinishMillis = 0;
                mBinding.txtHour.setText(formatTimer(0));
                mBinding.txtMinute.setText(formatTimer(0));
                mBinding.txtSecond.setText(formatTimer(0));
                if (onFinishTimerListener != null) {
                    onFinishTimerListener.onFinish();
                }
            }
        }.start();
    }

    public void addOnFinishListener(OnFinishTimerListener onFinishTimerListener){
        this.onFinishTimerListener = onFinishTimerListener;
    }

    public long getUntilFinishMillis(){
        return untilFinishMillis;
    }

    private int[] getTimer(long millisSecond){
        long second = millisSecond/1000;
        int h, m, s;
        h = (int) (second / 3600);
        m = (int) (second % 3600) / 60;
        s = (int) (second % 3600) % 60;
        return new int[]{h, m, s};
    }

    public static String formatTimer(int numb){
        String result = "";
        if (numb < 10){
            result = "0" + numb;
        }else {
            result = numb + "";
        }
        return result;
    }

    public void cancelTimer(){
        if (timer != null){
            timer.cancel();
        }
    }
}
