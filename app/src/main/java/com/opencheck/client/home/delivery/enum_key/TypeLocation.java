package com.opencheck.client.home.delivery.enum_key;

public enum TypeLocation {
    HOME, WORK, LOCAL;

    @Override
    public String toString() {
        String result = "";
        switch (this){
            case HOME:
                result = "home";
                break;
            case WORK:
                result = "work";
                break;
            case LOCAL:
                result = "local";
                break;
        }

        return result;
    }
}
