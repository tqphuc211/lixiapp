package com.opencheck.client.home.product.bank_card;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PopupDeleteBankCardBinding;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;

public class PopupDeleteBankCard extends LixiDialog {

    public PopupDeleteBankCard(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    TextView txtNo, txtDelete;

    private PopupDeleteBankCardBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PopupDeleteBankCardBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        initView();

        txtDelete.setOnClickListener(this);
        txtNo.setOnClickListener(this);
    }

    private void initView() {
        txtNo = (TextView) findViewById(R.id.txtNo);
        txtDelete = (TextView) findViewById(R.id.txtDelete);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtNo:
                cancel();
                break;
            case R.id.txtDelete:
                if (onDeleteBankCard != null) {
                    onDeleteBankCard.onDelete(0);
                    cancel();
                }
                break;
        }
    }

    public void setDeleteEventListener(OnDeleteBankCard onDeleteBankCard) {
        this.onDeleteBankCard = onDeleteBankCard;
    }

    OnDeleteBankCard onDeleteBankCard;
}
