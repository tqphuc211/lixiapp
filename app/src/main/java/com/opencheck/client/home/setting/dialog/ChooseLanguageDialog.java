package com.opencheck.client.home.setting.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogChooseLanguageBinding;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

public class ChooseLanguageDialog extends LixiDialog {
    private LixiActivity activity;

    private ItemClickListener itemClickListener;
    private AppPreferenceHelper appPreferenceHelper;

    public ChooseLanguageDialog(@NonNull LixiActivity _activity) {
        super(_activity, false, true, false);
        this.activity = _activity;
    }

    private DialogChooseLanguageBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogChooseLanguageBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        appPreferenceHelper = new AppPreferenceHelper(activity);

        mBinding.txtLangVN.setOnClickListener(this);
        mBinding.txtLangEN.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLangVN:
                appPreferenceHelper.setLanguageSource(ConstantValue.LANG_CONFIG_VN);
                if (itemClickListener != null) {
                    itemClickListener.onItemCLick(0, mBinding.txtLangVN.getText().toString(), null, 0L);
                }
                cancel();
                break;
            case R.id.txtLangEN:
                appPreferenceHelper.setLanguageSource(ConstantValue.LANG_CONFIG_EN);
                if (itemClickListener != null) {
                    itemClickListener.onItemCLick(0, mBinding.txtLangEN.getText().toString(), null, 0L);
                }
                cancel();
                break;
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_OUTSIDE) {
            cancel();
        }
        return false;
    }

    public void addOnItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}