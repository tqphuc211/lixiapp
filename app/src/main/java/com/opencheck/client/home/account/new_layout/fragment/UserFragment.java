package com.opencheck.client.home.account.new_layout.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.FragmentUserBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.ChangeAvatarDialog;
import com.opencheck.client.home.account.SelectAvatarActivity;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.new_layout.activity.BankCardManagerActivity;
import com.opencheck.client.home.account.new_layout.activity.BoughtEvoucherActivity;
import com.opencheck.client.home.account.new_layout.activity.IntroCodeActivity;
import com.opencheck.client.home.account.new_layout.activity.UserTransactionActivity;
import com.opencheck.client.home.delivery.activities.FavoriteStoreActivity;
import com.opencheck.client.home.delivery.activities.PromotionCodeUserGroupActivity;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.setting.SettingActivity;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.user.ImageModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class UserFragment extends LixiFragment {

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.PROFILE_HOME;
    }

    // View
    private View view;
    private TextView txtUserName, txtUserNumb, txtLixiPoint, txtBought;
    private RelativeLayout relInvoice, relCardManager, relActive, relBought;
    private ImageView imgAvatar, imgSetting;

    // Var
    private UserModel userModel;
    private int total = 0;

    private FragmentUserBinding mBinding;

    private DeliAddressModel mLocation;

    public static boolean UPDATE_USER_INFO = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = FragmentUserBinding.inflate(inflater, container, false);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        view = mBinding.getRoot();
        rootview = view;
        initView();
        getTotalVoucher();
        getListPromotionCode();
        getTotalFavoriteStore();
        return view;
    }

    private void initView() {
        txtUserName = view.findViewById(R.id.txtUserName);
        txtUserNumb = view.findViewById(R.id.txtUserNumb);
        txtLixiPoint = view.findViewById(R.id.txtLixiPoint);
        txtBought = view.findViewById(R.id.txtBought);
        mBinding.tvPromotion.setText(String.format(LanguageBinding.getString(R.string.profile_list_promotion, activity), 0));

        relInvoice = view.findViewById(R.id.relInvoice);
        relCardManager = view.findViewById(R.id.relCardManager);
        relActive = view.findViewById(R.id.relActive);
        relBought = view.findViewById(R.id.relBought);

        imgAvatar = view.findViewById(R.id.imgAvatar);
        imgSetting = view.findViewById(R.id.imgSetting);

        if (ConfigModel.getInstance(activity).getReference_code_profile_enable() == 1) {
            mBinding.relIntroCode.setVisibility(View.VISIBLE);
            mBinding.viewIntroCode.setVisibility(View.VISIBLE);
        } else {
            mBinding.relIntroCode.setVisibility(View.GONE);
            mBinding.viewIntroCode.setVisibility(View.GONE);
        }

        imgSetting.setOnClickListener(this);
        imgAvatar.setOnClickListener(this);
        relInvoice.setOnClickListener(this);
        relCardManager.setOnClickListener(this);
        relActive.setOnClickListener(this);
        relBought.setOnClickListener(this);
        mBinding.relIntroCode.setOnClickListener(this);
        mBinding.rlPromotion.setOnClickListener(this);
        mBinding.llFavoriteStore.setOnClickListener(this);

        mBinding.swpieRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // refresh user data
                refreshAll();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgSetting:
                Intent intent = new Intent(activity, SettingActivity.class);
                activity.startActivity(intent);
                break;
            case R.id.imgAvatar:
                ChangeAvatarDialog dialog = new ChangeAvatarDialog(activity, false, true, false);
                dialog.show();
                break;
            case R.id.relInvoice:
                gotoNext(UserTransactionActivity.class);
                break;
            case R.id.relCardManager:
                gotoNext(BankCardManagerActivity.class);
                break;
            case R.id.relActive:
                gotoNext(ActiveCode2Activity.class);
                break;
            case R.id.relBought:
                gotoNext(BoughtEvoucherActivity.class);
                break;
            case R.id.relIntroCode:
                gotoNext(IntroCodeActivity.class);
                break;
            case R.id.rl_promotion:
                PromotionCodeUserGroupActivity.startPromotionCodeUserGroupActivity(activity);
                break;
            case R.id.ll_favorite_store:
                FavoriteStoreActivity.startFavoriteStoreActivity(activity);
                break;
        }
    }

    private void gotoNext(Class<?> screen) {
        Intent intent = new Intent(activity, screen);
        startActivity(intent);
    }

    private void initData() {
        this.userModel = UserModel.getInstance();

        if (userModel != null) {
            ImageLoader.getInstance().displayImage(userModel.getAvatar(), imgAvatar, LixiApplication.getInstance().optionsNomal);
            if (!userModel.getFullname().equalsIgnoreCase("")) {
                txtUserName.setText(userModel.getFullname());
            } else if (!userModel.getUsername().equalsIgnoreCase("")) {
                txtUserName.setText(userModel.getUsername());
            } else if (!userModel.getPhone().equalsIgnoreCase("")) {
                txtUserName.setText(userModel.getPhone());
            } else if (!userModel.getEmail().equalsIgnoreCase("")) {
                txtUserName.setText(userModel.getEmail());
            }
            txtUserNumb.setText(Helper.formatPhone(userModel.getPhone()));
            mBinding.txtIntroCode.setText(userModel.getUser_code());
            try {
                txtLixiPoint.setText(Helper.getVNCurrency(Long.parseLong(userModel.getAvailable_point())) + " " + activity.getResources().getString(R.string.lixi));
            } catch (Exception e) {

            }
        }

    }

    private void getTotalVoucher() {
        mBinding.swpieRefresh.setRefreshing(true);
        DataLoader.getBoughtEVoucher(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                mBinding.swpieRefresh.setRefreshing(false);
                if (isSuccess) {
                    ApiWrapperForListModel<LixiShopEvoucherBoughtModel> oj = (ApiWrapperForListModel<LixiShopEvoucherBoughtModel>) object;
                    total = oj.get_meta().getTotal();
                    String totalEVoucher = String.format(LanguageBinding
                            .getString(R.string.profile_e_voucher_buy, getContext()), total);
                    txtBought.setText(totalEVoucher);
                    initData();
                }
            }
        }, 1, null);
    }

    private void getListPromotionCode() {
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (mLocation != null) {
            DeliDataLoader.getPromotionListUserGroup(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ArrayList<PromotionModel> promotionModels = (ArrayList<PromotionModel>) object;
                        if (promotionModels != null) {
                            mBinding.tvPromotion.setText(String.format(LanguageBinding.getString(R.string.profile_list_promotion, activity), promotionModels.size()));
                        }
                    }
                }
            }, null, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        }
    }

    private void getTotalFavoriteStore() {
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (mLocation != null) {
            DeliDataLoader.getTotalFavoriteStore(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        int total = (int) object;
                        mBinding.tvFavoriteStoreCount.setText(String.valueOf(total));
                    }
                }
            }, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        }
    }

    public void uploadAvatar(ImageModel image) {
        final int MAX_IMAGE_SIZE = 500 * 1024; // max final file size in kilobytes

        File file = new File(image.path);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getPath(), options);

        options.inSampleSize = calculateInSampleSize(options, 500, 500);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bmpPic = BitmapFactory.decodeFile(file.getPath(), options);
        if (bmpPic == null) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.try_again, activity), Toast.LENGTH_SHORT).show();
            return;
        }
        bmpPic = rotate(bmpPic, 90);

        int compressQuality = 100; // quality decreasing by 1 every loop.
        int streamLength;
        do {
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            compressQuality -= 1;
        } while (streamLength >= MAX_IMAGE_SIZE);

        try {
            FileOutputStream bmpFile = new FileOutputStream(activity.getCacheDir() + "/uploadTemp.jpg");
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
            imgAvatar.setImageBitmap(bmpPic);
            bmpFile.flush();
            bmpFile.close();
        } catch (Exception e) {
            Log.e("mapi", "Error on saving file");
        }

        DataLoader.updateAvatar(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Helper.showErrorDialog(activity, LanguageBinding.getString(
                            R.string.profile_success_update_profile_image, getContext()));
                    getUserProfile();
                } else {
                    Helper.showErrorDialog(activity, object.toString());
                }
            }
        }, activity.getCacheDir() + "/uploadTemp.jpg");
    }

    private void getUserProfile() {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    userModel = UserModel.getInstance();
//                    if (accountAdapter != null)
//                        accountAdapter.setData(currentModel);
                }
//                swipeLayout.setRefreshing(false);
            }
        });
    }

    private Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        String debugTag = "MemoryInformation";

        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d(debugTag, "image height: " + height + "---image width: " + width);
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d(debugTag, "inSampleSize: " + inSampleSize);
        return inSampleSize;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);
//            ((HomeActivity) activity).navigation.setVisibility(View.VISIBLE);

        if (ConstantValue.image.size() > 0) {
            ArrayList<ImageModel> img = ConstantValue.image;
            ConstantValue.image = new ArrayList<>();
            if (img.size() > 0)
                uploadAvatar(img.get(0));
        }

        if (new AppPreferenceHelper(activity).getOnRefresh()) {
            refresh();
            new AppPreferenceHelper(activity).setOnRefresh(false);
        }

        getTotalFavoriteStore();
    }

    public void refresh() {
        getTotalVoucher();
        getListPromotionCode();
        getTotalFavoriteStore();
    }

    public void refreshAll() {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        UserFragment.UPDATE_USER_INFO = false;
                        initData();
                    }
                }
        );
        (new AppPreferenceHelper(activity)).setTimeResetConfig(System.currentTimeMillis());
        DataLoader.getUserConfig(activity);
        DeliDataLoader.getUserConfig(activity);
        getTotalVoucher();
        getListPromotionCode();
        getTotalFavoriteStore();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR:
                if (activity instanceof HomeActivity)
                    ((HomeActivity) activity).captureNewImage();
                break;
            case ConstantValue.REQUEST_CODE_WRITE_EXTERNAL_STORAGE_CHANGING_AVATAR:
                activity.startActivity(new Intent(activity, SelectAvatarActivity.class));
                break;
        }
    }
}
