package com.opencheck.client.home.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemAlbumLayoutBinding;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tony Tuan on 04/02/2018.
 */

public class AlbumAdapter extends BaseAdapter{

    private LixiActivity activity;
    private ArrayList<HashMap<String, String>> data;

    public AlbumAdapter(LixiActivity activity, ArrayList<HashMap<String, String>> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AlbumViewHolder holder = null;
        if(convertView == null){
            holder = new AlbumViewHolder();
            ItemAlbumLayoutBinding itemAlbumLayoutBinding = ItemAlbumLayoutBinding
                    .inflate(LayoutInflater.from(activity), parent, false);
            convertView = itemAlbumLayoutBinding.getRoot();
            holder.gallery_image = (ImageView) convertView.findViewById(R.id.iv_gallery_image);
            holder.gallery_count = (TextView) convertView.findViewById(R.id.tv_gallery_count);
            holder.gallery_title = (TextView) convertView.findViewById(R.id.tv_gallery_title);

            convertView.setTag(holder);
        }else{
            holder = (AlbumViewHolder) convertView.getTag();
        }

        holder.gallery_image.setId(position);
        holder.gallery_count.setId(position);
        holder.gallery_title.setId(position);
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        try{
            holder.gallery_title.setText(song.get(ConstantValue.KEY_ALBUM));
            holder.gallery_count.setText(song.get(ConstantValue.KEY_COUNT));

            ImageLoader.getInstance().displayImage("file://" + song.get(ConstantValue.KEY_PATH), holder.gallery_image);

        } catch (Exception e){}

        return convertView;
    }

    class AlbumViewHolder {
        ImageView gallery_image;
        TextView gallery_count, gallery_title;
    }
}
