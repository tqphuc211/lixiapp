package com.opencheck.client.home.merchant.map;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StoreMapLayoutActivityBinding;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class StoreMapActivity extends LixiActivity implements OnDataPass {

    private static double curLat, curLng ;

    private StoreMapLayoutActivityBinding mBinding;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.store_map_layout_activity);
        this.initView();
    }


    private void initView() {
        this.initData();
    }

    private StoreMapFragment storeMapFragment;

    private void initData() {
        storeMapFragment = new StoreMapFragment();
        Bundle data = getIntent().getExtras();
        storeMapFragment.setArguments(data);

        String backStateName =  storeMapFragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fr_main, storeMapFragment, fragmentTag);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_PERMISSON:
                if (resultCode == RESULT_OK) {
                    Helper.getGPSLocation(this, new LocationCallBack() {
                        @Override
                        public void callback(LatLng position, ArrayList<String> address) {
                            float lat = (float) position.latitude;
                            float lng = (float) position.longitude;

                            curLat = (double) storeMapFragment.getLocation().get("LAT");
                            curLng = (double) storeMapFragment.getLocation().get("LNG");

                            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + lat + "," + lng + "&daddr=" + curLat + "," + curLng));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        }
                    });
                } else {
                    Toast.makeText(this, "Chưa xin quyền tìm tọa độ", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onDataPass(double curLat, double curLng) {
        this.curLat = curLat;
        this.curLng = curLng;
    }
}
