package com.opencheck.client.home.delivery.controller;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.common.StringUtils;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FoodItemLayoutBinding;
import com.opencheck.client.home.delivery.model.ProductGetModel;
import com.opencheck.client.utils.Helper;

public class FoodController {

    private LixiActivity activity;
    private LinearLayout lnRoot;
    private int position;
    private int size;
    private View viewChild;
    private TextView tv_count, tv_name, tv_price, tv_addon;
    private RelativeLayout rl_amount;
    private ProductGetModel productModel;
    private boolean isHaveBackground;

    public FoodController(LixiActivity _activity, ProductGetModel _productModel, LinearLayout _lnRoot, boolean isHaveBackground, int _position, int _size) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.isHaveBackground = isHaveBackground;
        this.position = _position;
        this.size = _size;
        this.productModel = _productModel;
        FoodItemLayoutBinding foodItemLayoutBinding =
                FoodItemLayoutBinding.inflate(LayoutInflater.from(activity), null, false);
        this.viewChild = foodItemLayoutBinding.getRoot();
        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initData() {

        if (!isHaveBackground) {
            rl_amount.setBackground(ContextCompat.getDrawable(activity, R.drawable.bg_item_count_gray));
            tv_count.setTextColor(activity.getResources().getColor(R.color.white));
            tv_count.setText(productModel.getQuantity() + "");
            tv_name.setTextColor(activity.getResources().getColor(R.color.color_product_2));
        } else {
            tv_count.setText(productModel.getQuantity() + "");
            tv_name.setTextColor(activity.getResources().getColor(R.color.color_product_1));
        }

        tv_price.setText(Helper.getVNCurrency(productModel.getPrice() * productModel.getQuantity()) + "đ");
        StringBuilder builder = new StringBuilder();
        if (productModel.getList_addon() != null) {
            for (int i = 0; i < productModel.getList_addon().size(); i++) {
                if (productModel.getList_addon().get(i).getValue() != null &&
                        isNumeric(productModel.getList_addon().get(i).getValue()))
                    builder.append(productModel.getList_addon().get(i).getValue()).append("x");
                builder.append(productModel.getList_addon().get(i).getAddon_name());
                if (i < productModel.getList_addon().size() - 1) {
                    builder.append(", ");
                }
            }
        }

        tv_name.setText(productModel.getName());

        if (builder.toString().length() > 0) {
            tv_addon.setVisibility(View.VISIBLE);
            tv_addon.setText(builder.toString());
        } else {
            tv_addon.setVisibility(View.GONE);
        }
    }

    private boolean isNumeric(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    private void initView() {
        tv_count = viewChild.findViewById(R.id.tv_count);
        tv_name = viewChild.findViewById(R.id.tv_name);
        tv_price = viewChild.findViewById(R.id.tv_price);
        tv_addon = viewChild.findViewById(R.id.tv_addon);
        rl_amount = viewChild.findViewById(R.id.rl_amount);
    }


}
