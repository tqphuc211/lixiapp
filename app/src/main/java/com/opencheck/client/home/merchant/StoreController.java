package com.opencheck.client.home.merchant;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemStoreLayoutBinding;
import com.opencheck.client.home.merchant.map.StoreMapActivity;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class StoreController implements View.OnClickListener {

    private LixiActivity activity;
    private View viewChild;
    private LinearLayout lnRoot;
    private int limitSize;
    private int position = 0;

    private TextView tv_number;
    private TextView tv_name;
    private TextView tv_address;
    private TextView tv_phone;
    private ImageView iv_map;
    private ImageView iv_call;
    private View v_line;

    private StoreModel storeModel;
    private MerchantModel merchantModel;

    public StoreController(LixiActivity _activity, StoreModel storeModel, MerchantModel merchantModel, int _position, int _limitSize, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;

        this.storeModel = storeModel;
        this.merchantModel = merchantModel;
        ItemStoreLayoutBinding itemStoreLayoutBinding =
                ItemStoreLayoutBinding.inflate(LayoutInflater.from(activity),
                        null, false);
        this.viewChild = itemStoreLayoutBinding.getRoot();

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initView() {
        tv_number = viewChild.findViewById(R.id.tv_number);
        tv_name = viewChild.findViewById(R.id.tv_name);
        tv_address = viewChild.findViewById(R.id.tv_address);
        tv_phone = viewChild.findViewById(R.id.ed_phone);
        iv_map = viewChild.findViewById(R.id.iv_map);
        iv_call = viewChild.findViewById(R.id.iv_call);
        v_line = viewChild.findViewById(R.id.v_line);

        iv_call.setVisibility(View.GONE);
        iv_map.setVisibility(View.GONE);

        iv_call.setOnClickListener(this);
        iv_map.setOnClickListener(this);
        viewChild.setOnClickListener(this);
    }

    private void initData() {
        if (position == 0) {
            v_line.setVisibility(View.GONE);
        }

        tv_number.setText((position + 1) + ".");
        tv_name.setText(storeModel.getName());
        tv_address.setText(storeModel.getAddress());
        tv_phone.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), storeModel.getPhone()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_call:
                Helper.callPhone(activity, storeModel.getPhone());
                break;
            case R.id.iv_map:
                if (merchantModel != null) {
                    merchantModel.getListStore().remove(storeModel);
                    merchantModel.getListStore().add(0, storeModel);
                    Intent intent = new Intent(activity, StoreMapActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
                break;
        }
        if (v == viewChild) {
            if (merchantModel != null) {
                merchantModel.getListStore().remove(storeModel);
                merchantModel.getListStore().add(0, storeModel);
                Intent intent = new Intent(activity, StoreMapActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        }
    }
}