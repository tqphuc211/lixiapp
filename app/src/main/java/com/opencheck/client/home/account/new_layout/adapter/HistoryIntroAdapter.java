package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowHistoryIntroBinding;
import com.opencheck.client.home.account.new_layout.model.HistoryIntroModel;
import com.opencheck.client.home.account.new_layout.model.UserRefInfo;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class HistoryIntroAdapter extends RecyclerView.Adapter<HistoryIntroAdapter.IntroHolder> {
    private LixiActivity activity;
    private ArrayList<HistoryIntroModel> listIntro;

    public HistoryIntroAdapter(LixiActivity activity) {
        this.activity = activity;
        this.listIntro = new ArrayList<>();
    }

    @Override
    public IntroHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowHistoryIntroBinding mBinding = RowHistoryIntroBinding.inflate(inflater, parent, false);
        return new IntroHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(IntroHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listIntro.size();
    }

    public void setData(ArrayList<HistoryIntroModel> listIntro) {
        this.listIntro.addAll(listIntro);
        notifyDataSetChanged();
    }

    public void clearData(){
        this.listIntro.clear();
        notifyDataSetChanged();
    }

    class IntroHolder extends RecyclerView.ViewHolder {
        private RowHistoryIntroBinding mBinding;

        public IntroHolder(RowHistoryIntroBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind() {
            HistoryIntroModel intro = listIntro.get(getAdapterPosition());
            if (intro != null) {
                mBinding.txtDate.setText(Helper.getFormatDateVN("dd-MM-yyyy", intro.getWrite_date() * 1000));
                mBinding.txtLixi.setText("+" + Helper.getVNCurrency(intro.getUser_ref_bonus()) + " Lixi");
                UserRefInfo userRef = intro.getUser_ref_info();
                if (userRef != null) {
                    if (userRef.getAvatar() != null && !userRef.getAvatar().isEmpty()) {
                        ImageLoader.getInstance().displayImage(userRef.getAvatar(), mBinding.imgAvatar);

                    } else {
                        mBinding.imgAvatar.setImageResource(R.drawable.ic_user_rating_def);
                    }
                    mBinding.txtName.setText(userRef.getName());
                }
            }
        }
    }
}
