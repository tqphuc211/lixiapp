package com.opencheck.client.home.delivery.interfaces;

import android.view.View;

public interface OnItemClickListener {
    public void onItemClickListener(View view, final int pos, final String action, Object object, final Long count);
}
