package com.opencheck.client.home.rating.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogAllRatingBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.rating.adapter.RatingAdapter;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.Helper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class AllRatingDialog extends LixiDialog {

    public AllRatingDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    // View
    private ImageView imgBack;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recListRating;

    // Var
    private int page = 1;
    private int record_per_page = DataLoader.record_per_page;
    private Boolean isLoading = false;
    private boolean isOutOfVoucherPage = false;
    private long total = 0;
    private long productID;

    RatingAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<RatingModel> listRating;

    private DialogAllRatingBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogAllRatingBinding.inflate(LayoutInflater
                .from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
        setAdapter();
    }

    private void initView(){
        imgBack = (ImageView) findViewById(R.id.imgBack);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        recListRating = (RecyclerView) findViewById(R.id.recListRating);

        imgBack.setOnClickListener(this);

        recListRating.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recListRating.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount  = linearLayoutManager.getChildCount();
                    int totalItemCount    = linearLayoutManager.getItemCount();
                    int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 3) {
                        Helper.showLog("Load more");
                        getRatingList();
                        isLoading = true;
                    }
                }
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                isOutOfVoucherPage = false;
                listRating.clear();
                getRatingList();
            }
        });
    }

    private void setAdapter(){
        adapter = new RatingAdapter(activity, listRating, false);
        linearLayoutManager = new LinearLayoutManager(activity);
        recListRating.setLayoutManager(linearLayoutManager);
        recListRating.setAdapter(adapter);
    }

    public void setData(long productID){
        this.productID = productID;
        getRatingList();
    }

    private void getRatingList(){
        if (!isLoading) {
            if (!isOutOfVoucherPage) {
                swipeRefresh.setRefreshing(true);
                DataLoader.getListRating(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            if (page == 1) {
                                listRating = (ArrayList<RatingModel>) object;
                                setOutOfVoucherPage(listRating);
                                setAdapter();
                            } else {
                                List<RatingModel> list = (List<RatingModel>) object;
                                if (list == null || list.size() == 0 || list == null) {
                                    return;
                                } else {
                                    setOutOfVoucherPage(list);
                                    listRating.addAll(list);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            page++;
                            isLoading = false;
                            swipeRefresh.setRefreshing(false);
                        } else {
                            isLoading = true;
                        }
                    }
                }, (int) productID, page, record_per_page);
            }
        }
    }

    private void setOutOfVoucherPage(List<RatingModel> list) {
        if (list.size() < record_per_page) {
            isOutOfVoucherPage = true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                cancel();
                break;
        }
    }
}
