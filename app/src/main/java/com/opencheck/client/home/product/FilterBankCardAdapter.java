package com.opencheck.client.home.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.Filterable;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.home.product.bank_card.ChooseBankCardAdapter;
import com.opencheck.client.models.lixishop.BankModel;

import java.util.ArrayList;
import java.util.List;

public abstract class FilterBankCardAdapter extends RecyclerView.Adapter<ChooseBankCardAdapter.ViewHolder> implements Filterable{

    protected ArrayList<BankModel> list;
    protected ArrayList<BankModel> originalList;
    protected LixiActivity activity;

    public FilterBankCardAdapter() {
    }

    public FilterBankCardAdapter(LixiActivity activity, ArrayList<BankModel> list)
    {
        this.originalList = list;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<BankModel>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<BankModel> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = originalList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected ArrayList<BankModel> getFilteredResults(String constraint) {
        ArrayList<BankModel> results = new ArrayList<>();

        for (BankModel item : originalList) {
            if (item.getName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

}
