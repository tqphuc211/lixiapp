package com.opencheck.client.home.history.detail;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class GeneralQRCodeAndCropTask extends AsyncTask<String, Integer, String> {

    public static String GENERAL_BITMAP_RETURN = "GENERAL_BITMAP_RETURN";
    private Activity activity;
    private String textString;
    private ImageView imgSet;
    private ItemClickListener itemClickListener;

    private Bitmap bitmap;

    public GeneralQRCodeAndCropTask(Activity _activity, String _textString, ImageView _imgSet, ItemClickListener _itemClickListener) {

        this.activity = _activity;
        this.textString = _textString;
        this.imgSet = _imgSet;
        this.itemClickListener = _itemClickListener;

//        WIDTH = (int) activity.getResources().getDimension(R.dimen.value_188);
//        HEIGHT = WIDTH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            execute("");
        }
    }

    public GeneralQRCodeAndCropTask(Activity _activity, String _textString, ImageView _imgSet, int QRColorCode, ItemClickListener _itemClickListener) {

        this.activity = _activity;
        this.textString = _textString;
        this.imgSet = _imgSet;
        this.itemClickListener = _itemClickListener;
        this.VISIBLE_COLOR = QRColorCode;

//        WIDTH = (int) activity.getResources().getDimension(R.dimen.value_188);
//        HEIGHT = WIDTH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            execute("");
        }
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            bitmap = encodeAsBitmap(textString);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        if (bitmap == null) {
            itemClickListener.onItemCLick(0, GENERAL_BITMAP_RETURN, null, 0L);
        } else {
            itemClickListener.onItemCLick(1, GENERAL_BITMAP_RETURN, bitmap, 0L);
        }
    }

    public int INVISIBLE_CODE = 0xFFFFFFFF;
    public int VISIBLE_COLOR = 0xFF000000;
    public static int WIDTH = 400;
    public static int HEIGHT = 400;

    private Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int startX = 0;
        int startY = 0;
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            if (startX != 0 || startY != 0)
                break;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? VISIBLE_COLOR : INVISIBLE_CODE;
                if (startX == 0 && startY == 0 && result.get(x, y)) {
                    startX = x;
                    startY = y;
                    break;
                }
            }
        }

        int width1 = result.getWidth() - (2 * startX);
        int height1 = result.getHeight() - (2 * startY);
        int[] pixels1 = new int[width1 * height1];
        for (int y = 0; y < height1; y++) {
            int offset = y * width1;
            for (int x = 0; x < width1; x++) {
                pixels1[offset + x] = result.get(x + startX, y + startY) ? VISIBLE_COLOR : INVISIBLE_CODE;
            }
        }


        Bitmap bitmap = Bitmap.createBitmap(width1, height1, Bitmap.Config.ARGB_8888);
//        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        bitmap.setPixels(pixels1, 0, width1, 0, 0, width1, height1);
        return bitmap;
    }
}

