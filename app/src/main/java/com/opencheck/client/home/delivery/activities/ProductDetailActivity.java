package com.opencheck.client.home.delivery.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityProductDetailDeliBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.GalleryAdapter;
import com.opencheck.client.home.delivery.libs.section.SectionParameters;
import com.opencheck.client.home.delivery.libs.section.SectionedRecyclerViewAdapter;
import com.opencheck.client.home.delivery.libs.section.StatelessSection;
import com.opencheck.client.home.delivery.model.AddonCategoryModel;
import com.opencheck.client.home.delivery.model.AddonModel;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ProductDetailActivity extends LixiActivity {

    private long ID = 0L;
    private int QUANTITY = 0;
    private String TITLE = "";
    private boolean IS_HAVE_ADDON = false;
    private int height;
    private SectionedRecyclerViewAdapter mAdapter;
    private ProductModel mProductModel;
    private int POSITION = -1;
    public final static int REQUEST_CODE_DETAIL_PRODUCT = 123;

    private ActivityProductDetailDeliBinding mBinding;

    public static void startProductDetailActivity(LixiActivity context, long id, String title, int quantity, int position, boolean isHaveAddon) {
        Intent intent = new Intent(context, ProductDetailActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", title);
        intent.putExtra("QUANTITY", quantity);
        intent.putExtra("POSITION", position);
        intent.putExtra("IS_HAVE_ADDON", isHaveAddon);
        context.startActivityForResult(intent, REQUEST_CODE_DETAIL_PRODUCT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail_deli);
        ID = getIntent().getLongExtra("ID", 0L);
        TITLE = getIntent().getStringExtra("TITLE");
        QUANTITY = getIntent().getIntExtra("QUANTITY", 0);
        POSITION = getIntent().getIntExtra("POSITION", 0);
        IS_HAVE_ADDON = getIntent().getBooleanExtra("IS_HAVE_ADDON", false);
        initView();
    }

    private void initView() {

        height = mBinding.toolbar.getHeight();
        mBinding.ivBack.setOnClickListener(this);
        mBinding.rlAdd.setOnClickListener(this);
        mBinding.ivAdd.setOnClickListener(this);
        mBinding.ivSub.setOnClickListener(this);

        mBinding.appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    mBinding.tvTitle.setText(TITLE);
                    mBinding.tvTitle.setVisibility(View.VISIBLE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 172, 30, 161));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));

                } else {
                    //expands
                    mBinding.tvTitle.setVisibility(View.GONE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 255, 255, 255));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(5);
                }
            }
        });

        getProductDetail();
    }

    private void initData() {
        mBinding.tvName.setText(mProductModel.getName());
        mBinding.tvPrice.setText(Helper.getVNCurrency(mProductModel.getPrice()) + "đ");
        if (mProductModel.getDescription() != null && mProductModel.getDescription().length() > 0) {
            mBinding.tvDescription.setText(mProductModel.getDescription());
            mBinding.tvDescription.setVisibility(View.VISIBLE);
        }

        if (mProductModel.getIs_hot() == 1)
            mBinding.tvHot.setVisibility(View.VISIBLE);

        if (mProductModel.getState().equals("paused")) {
            mBinding.tvPausedMessage.setVisibility(View.VISIBLE);
            mBinding.lnCount.setVisibility(View.GONE);
            mBinding.tvTotal.setBackgroundColor(getResources().getColor(R.color.dark_gray_light));
            mBinding.rlAdd.setClickable(false);
        }

        ArrayList<String> list = new ArrayList<>();
        list.add(mProductModel.getImage_link());
        GalleryAdapter mGalleryAdapter = new GalleryAdapter(activity, list);
        mBinding.viewPager.setAdapter(mGalleryAdapter);
        mBinding.indicator.setViewPager(mBinding.viewPager);
        mBinding.indicator.requestLayout();

        mAdapter = new SectionedRecyclerViewAdapter();
        for (int i = 0; i < mProductModel.getList_addon_category().size(); i++) {
            AddonCategoryModel addonCategoryModel = mProductModel.getList_addon_category().get(i);
            mAdapter.addSection(addonCategoryModel.getName(), new OptionSection(addonCategoryModel.getName(),
                    addonCategoryModel.isRequired(), addonCategoryModel.getData_type(), addonCategoryModel.isMultichoice(),
                    addonCategoryModel.getList_addon(), addonCategoryModel.getMin_total_quantity(), addonCategoryModel.getMax_total_quantity()));
            if (addonCategoryModel.isRequired()) {
                mSectionMap.put(addonCategoryModel.getName(), false);
                mKey.add(addonCategoryModel.getName());
            }
        }

        mBinding.rvAddon.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mBinding.rvAddon.setAdapter(mAdapter);
        mBinding.rvAddon.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        updateMoney();
    }

    private void getProductDetail() {
        DeliDataLoader.getProductDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mProductModel = (ProductModel) object;
                    initData();
                }
            }
        }, ID);
    }

    private void handleOnIncreaseQuatityClick() {
        count++;
        updateMoney();
    }

    private void handleOnDecreaseQuantityClick() {
        if (count > 1) {
            count--;
            updateMoney();
        }
    }

    private boolean checkItemSelected() {
        if (mKey == null || mKey.size() == 0)
            return true;
        for (int i = 0; i < mKey.size(); i++) {
            if (!mSectionMap.get(mKey.get(i))) {
                return false;
            }
        }
        return true;
    }

    private void formatOptionList(ProductModel mProduct) {
        int k = 0;
        for (int i = 0; i < mProduct.getList_addon_category().size(); i++) {
            if (k == mAddonList.size())
                break;
            for (int j = 0; j < mAddonList.size(); j++) {
                if (mProduct.getList_addon_category().get(i).getName().equals(mAddonList.get(j).getCategory())) {
                    Collections.swap(mAddonList, k, j);
                    k++;
                    break;
                }
            }
        }
    }

    private void handleOnAddToppingClick() {
        if (!checkItemSelected()) {
            int index = 0;
            for (int i = mKey.size() - 1; i >= 0; i--) {
                if (!mSectionMap.get(mKey.get(i))) {
                    isSelected = true;
                    index = i;
                    mAdapter.notifyDataSetChanged();
                }
            }
            ((LinearLayoutManager) mBinding.rvAddon.getLayoutManager())
                    .scrollToPositionWithOffset(mAdapter.getHeaderPositionInAdapter(mKey.get(index)), 0);

        } else {
            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
            //HashMap<String, ArrayList<String>> mapping = Helper.getProductKeyListFromFile(activity, ConstantValue.PRODUCT_FILE);

            if (cart != null && !cart.containsKey("" + mProductModel.getStore_id())) {
                CartModel cartModel = new CartModel();
                HashMap<String, ArrayList<ProductModel>> productList = new HashMap<>();
                cartModel.setId(mProductModel.getStore_id());
                cartModel.setProduct_list(productList);
                cart.put("" + mProductModel.getStore_id(), cartModel);
                //mapping.put("" + mProductModel.getStore_id(), new ArrayList<String>());
            }

            if (cart.get("" + mProductModel.getStore_id()).getProduct_list() != null
                    && cart.get("" + mProductModel.getStore_id()).getProduct_list().containsKey("" + mProductModel.getId())) {

                ArrayList<ProductModel> temp = cart.get("" + mProductModel.getStore_id()).getProduct_list().get("" + mProductModel.getId());
                boolean isExist = false;
                for (int i = 0; i < temp.size(); i++) {
                    ArrayList<AddonModel> addons = temp.get(i).getList_addon();
                    if (addons.size() != mAddonList.size()) {
                        isExist = false;
                    } else {
                        boolean isSame = true;
                        for (int j = 0; j < addons.size(); j++) {
                            if (addons.get(j).getId() != mAddonList.get(j).getId()) {
                                isSame = false;
                                break;
                            }
                        }

                        int curQuantity = cart.get("" + mProductModel.getStore_id()).getProduct_list().get("" + mProductModel.getId()).get(i).getQuantity();
                        if (isSame) {
                            isExist = true;
                            cart.get("" + mProductModel.getStore_id()).getProduct_list().get("" + mProductModel.getId()).get(i).setQuantity(curQuantity + count);
                            break;
                        } else {
                            isExist = false;
                        }
                    }
                }

                if (!isExist) {
                    ProductModel produc = new ProductModel();
                    produc.setName(mProductModel.getName());
                    produc.setId(mProductModel.getId());
                    formatOptionList(mProductModel);
                    produc.setList_addon(mAddonList);
                    produc.setHave_addon(true);
                    produc.setQuantity(count);
                    produc.setImage_link(mProductModel.getImage_link());
                    produc.setPrice(mTotal);
                    produc.setNote("");
                    StringBuilder builder = new StringBuilder();
                    for (int k = 0; k < mAddonList.size(); k++) {
                        builder.append(mAddonList.get(k).getId());
                    }
                    produc.setIndentifyAddon(builder.toString());
                    cart.get("" + mProductModel.getStore_id()).getProduct_list().get("" + mProductModel.getId()).add(produc);
                }

            } else {
                //mapping.get("" + mProductModel.getStore_id()).add(mProductModel.getId() + "");
                ArrayList<ProductModel> temp = new ArrayList<>();
                ProductModel pro = new ProductModel();
                pro.setId(mProductModel.getId());
                pro.setName(mProductModel.getName());
                pro.setQuantity(count);
                pro.setHave_addon(true);
                pro.setImage_link(mProductModel.getImage_link());
                pro.setPrice(mTotal);
                formatOptionList(mProductModel);
                pro.setList_addon(mAddonList);
                StringBuilder builder = new StringBuilder();
                for (int k = 0; k < mAddonList.size(); k++) {
                    builder.append(mAddonList.get(k).getId());
                }
                pro.setIndentifyAddon(builder.toString());
                temp.add(pro);
                cart.get("" + mProductModel.getStore_id()).getProduct_list().put(mProductModel.getId() + "", temp);
            }

            Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
            //Helper.saveProductToFile(mapping, activity, ConstantValue.PRODUCT_FILE);

            Intent resultIntent = new Intent();
            resultIntent.putExtra("COUNT", QUANTITY + count);
            resultIntent.putExtra("IS_ADDON", IS_HAVE_ADDON);
            resultIntent.putExtra("POSITION", POSITION);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
    }

    private void handleUpdateProductQuantityEvent(boolean isIncrease) {
        if (isIncrease) {
            count++;
        } else {
            if (count > 1)
                count--;
        }

        updateMoney();
    }


    @Override
    public void onClick(View view) {
        if (view == mBinding.ivBack) {
            finish();
        } else if (view == mBinding.ivSub) {
            handleOnDecreaseQuantityClick();
        } else if (view == mBinding.ivAdd) {
            handleOnIncreaseQuatityClick();
        } else if (view == mBinding.rlAdd) {
            if (IS_HAVE_ADDON)
                handleOnAddToppingClick();
            else
                handleWithoutToppingClick();

        }
    }

    private void handleWithoutToppingClick() {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        //HashMap<String, ArrayList<String>> mapping = Helper.getProductKeyListFromFile(activity, ConstantValue.PRODUCT_FILE);

        //If store is existed in cart
        if (cart.containsKey("" + mProductModel.getStore_id())) {
            CartModel cart_item = cart.get("" + mProductModel.getStore_id());
            //If store contains this product after that update its quantity
            if (cart_item.getProduct_list() != null && cart_item.getProduct_list().containsKey("" + mProductModel.getId())) {
                if (cart_item.getProduct_list().get("" + mProductModel.getId()).size() > 0)
                    cart_item.getProduct_list().get("" + mProductModel.getId()).get(0).setQuantity(count + QUANTITY);
            } else { // add new product into cart
                ArrayList<ProductModel> temp = new ArrayList<>();
                temp.add(mProductModel);
                cart.get("" + mProductModel.getStore_id()).getProduct_list().put(mProductModel.getId() + "", temp);
                //mapping.get("" + mProductModel.getStore_id()).add(mProductModel.getId() + "");
            }
        } else {

            ArrayList<ProductModel> temp = new ArrayList<>();
            temp.add(mProductModel);

            HashMap<String, ArrayList<ProductModel>> product_list = new HashMap<>();
            product_list.put("" + mProductModel.getId(), temp);

            CartModel cart_item = new CartModel();
            cart_item.setId(mProductModel.getStore_id());
            cart_item.setProduct_list(product_list);
            cart.put("" + mProductModel.getStore_id(), cart_item);

            ArrayList<String> key_list = new ArrayList<>();
            key_list.add(mProductModel.getId() + "");
            //mapping.put("" + mProductModel.getStore_id(), key_list);
        }

        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
        //Helper.saveProductToFile(mapping, activity, ConstantValue.PRODUCT_FILE);

        Intent resultIntent = new Intent();
        resultIntent.putExtra("COUNT", QUANTITY + count);
        resultIntent.putExtra("IS_ADDON", IS_HAVE_ADDON);
        resultIntent.putExtra("POSITION", POSITION);
        setResult(RESULT_OK, resultIntent);
        finish();

    }

    //initialize logic variables
    private int count = 1;
    private ArrayList<String> mKey = new ArrayList<>();
    private HashMap<String, Boolean> mSectionMap = new HashMap<>();
    private ArrayList<AddonModel> mAddonList = new ArrayList<>();
    private boolean isSelected = false;
    private long mTotal = 0L;

    private void updateMoney() {
        int temp = 0;
        if (mAddonList.size() > 0) {
            for (int i = 0; i < mAddonList.size(); i++) {
                if (mAddonList.get(i).getDefault_value() != null) {
                    Double dObj = Double.valueOf(String.valueOf(mAddonList.get(i).getDefault_value()));
                    if (dObj.intValue() > 0)
                        temp += mAddonList.get(i).getPrice_modifier() * dObj.intValue();
                } else
                    temp += mAddonList.get(i).getPrice_modifier();
            }
        }

        long total = (mProductModel.getPrice() + temp) * count;
        mTotal = mProductModel.getPrice() + temp;
        mBinding.tvCount.setText("" + count);
        mBinding.tvTotal.setText(LanguageBinding.getString(R.string.product_adding_to_cart, activity)+ " - " + Helper.getVNCurrency(total) + "đ");
    }

    class OptionSection extends StatelessSection {
        private ArrayList<AddonModel> optionList;
        private String category;
        private boolean required;
        private int lastPos = -1;
        private String data_type = "";
        private boolean multiple;
        private int max, min;
        private int total_quantity = 0;

        public OptionSection(String category, boolean required, String data_type, boolean multiple,
                             ArrayList<AddonModel> optionList, int min_val, int max_val) {
            super(SectionParameters.builder()
                    .itemResourceId(R.layout.food_option_item_layout)
                    .headerResourceId(R.layout.header_addon_category_layout)
                    .build());
            this.optionList = optionList;
            this.category = category;
            this.required = required;
            this.data_type = data_type;
            this.multiple = multiple;
            this.max = max_val;
            this.min = min_val;
        }

        @Override
        public int getContentItemsTotal() {
            return optionList.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new OptionSection.ItemHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new OptionSection.HeaderHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final OptionSection.ItemHolder itemHolder = (OptionSection.ItemHolder) holder;
            AddonModel model = optionList.get(position);
            model.setCategory(category);
            itemHolder.tv_type.setText(model.getName());
            if (model.getPrice_modifier() > 0) {
                itemHolder.tv_price.setVisibility(View.VISIBLE);
                itemHolder.tv_price.setText(Helper.getVNCurrency(model.getPrice_modifier()) + "đ");
            } else {
                itemHolder.tv_price.setVisibility(View.GONE);
            }

            if (data_type.equals("number")) {
                itemHolder.iv_tick.setVisibility(View.GONE);
                itemHolder.cb.setVisibility(View.GONE);

                Double dObj = Double.valueOf(String.valueOf(model.getDefault_value()));
                int _default = dObj.intValue();

                if (_default < 1) {
                    itemHolder.rl_sub.setVisibility(View.GONE);
                    itemHolder.tv_check_quantity.setVisibility(View.GONE);
                    mAddonList.remove(model);

                } else {
                    itemHolder.rl_sub.setVisibility(View.VISIBLE);
                    itemHolder.tv_check_quantity.setVisibility(View.VISIBLE);
                    itemHolder.tv_check_quantity.setText("" + _default + "x");

                    if (lastPos != position) {
                        if (lastPos == -1)
                            mAddonList.add(model);
                        else {
                            int j = -1;
                            for (int i = 0; i < mAddonList.size(); i++) {
                                if (mAddonList.get(i).getId() == model.getId()) {
                                    j = i;
                                    break;
                                }
                            }

                            if (j != -1) {
                                mAddonList.get(j).setDefault_value(dObj.doubleValue());
                            } else {
                                mAddonList.add(model);
                            }
                        }
                    } else {
                        if (mAddonList.size() > 0) {
                            boolean isExist = false;
                            for (int i = 0; i < mAddonList.size(); i++) {
                                if (mAddonList.get(i).getId() == model.getId()) {
                                    mAddonList.get(i).setDefault_value(dObj.doubleValue());
                                    isExist = true;
                                    break;
                                }
                            }

                            if (!isExist)
                                mAddonList.add(model);
                        } else {
                            mAddonList.add(model);
                        }
                    }
                    lastPos = position;
                }

                updateMoney();

            } else {
                itemHolder.tv_check_quantity.setVisibility(View.GONE);
                if (!multiple) {
                    itemHolder.iv_tick.setVisibility(View.VISIBLE);
                    itemHolder.cb.setVisibility(View.GONE);

                    if (model.getDefault_value() != null) {
                        boolean _default = (boolean) model.getDefault_value();
                        model.setSelected(_default);
                        model.setDefault_value(null);
                    }

                    if (model.isSelected()) {
                        if (mAddonList.size() > 0 && lastPos != -1) {
                            mAddonList.remove(optionList.get(lastPos));
                        }
                        mAddonList.add(model);
                        itemHolder.iv_tick.setImageResource(R.drawable.ic_tick_violet);
                        updateMoney();
                        lastPos = position;
                        if (required)
                            mSectionMap.put(category, true);
                    } else {
                        if (!required)
                            updateMoney();
                        itemHolder.iv_tick.setImageResource(R.drawable.bg_circle_grey);
                    }
                } else {
                    itemHolder.iv_tick.setVisibility(View.GONE);
                    itemHolder.cb.setVisibility(View.VISIBLE);

                    if (model.isSelected()) {
                        itemHolder.cb.setChecked(true);
                        lastPos = position;
                    } else {
                        itemHolder.cb.setChecked(false);
                    }

                    updateMoney();
                }

            }

            if (model.getState().equals("paused")) {
                itemHolder.tv_price.setVisibility(View.GONE);
                itemHolder.tv_paused_message.setVisibility(View.VISIBLE);

                if (model.getPause_reason_message() != null)
                    itemHolder.tv_paused_message.setText(model.getPause_reason_message());
            } else {
                itemHolder.tv_paused_message.setVisibility(View.GONE);
                itemHolder.tv_price.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            super.onBindHeaderViewHolder(holder);
            OptionSection.HeaderHolder headerHolder = (OptionSection.HeaderHolder) holder;
            headerHolder.tv_category.setText(category);
            if (!isSelected) {
                if (required)
                    headerHolder.tv_required.setVisibility(View.VISIBLE);
                else
                    headerHolder.tv_required.setVisibility(View.GONE);
            } else {
                if (mKey.contains(category)) {
                    if (!mSectionMap.get(category)) {
                        headerHolder.tv_category.setTextColor(activity.getResources().getColor(R.color.red_point));
                        headerHolder.tv_required.setTextColor(activity.getResources().getColor(R.color.red_point));
                    } else {
                        headerHolder.tv_category.setTextColor(activity.getResources().getColor(R.color.color_header_topping_name));
                        headerHolder.tv_required.setTextColor(activity.getResources().getColor(R.color.color_required));
                    }
                }
            }
        }

        class HeaderHolder extends RecyclerView.ViewHolder {
            private TextView tv_category, tv_required;

            public HeaderHolder(View itemView) {
                super(itemView);
                tv_category = itemView.findViewById(R.id.tv_category);
                tv_required = itemView.findViewById(R.id.tv_required);
            }
        }

        class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView iv_tick, iv_sub;
            private TextView tv_type, tv_price, tv_check_quantity, tv_paused_message;
            private CheckBox cb;
            private RelativeLayout rl_sub;
            private View rootView;

            public ItemHolder(View itemView) {
                super(itemView);
                rootView = itemView;
                iv_tick = itemView.findViewById(R.id.iv_tick);
                iv_sub = itemView.findViewById(R.id.iv_sub);
                tv_type = itemView.findViewById(R.id.tv_type);
                tv_price = itemView.findViewById(R.id.tv_price);
                tv_check_quantity = itemView.findViewById(R.id.tv_check_quantity);
                tv_paused_message = itemView.findViewById(R.id.tv_paused_message);
                rl_sub = itemView.findViewById(R.id.rl_sub);
                cb = itemView.findViewById(R.id.cb);

                rootView.setOnClickListener(this);
                rl_sub.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (v == rootView || v == cb) {
                    AddonModel cur = optionList.get(mAdapter.getPositionInSection(getAdapterPosition()));
                    if (cur.getState().equals("paused")) {
                        Toast.makeText(activity, cur.getName() + " đã hết, vui lòng chọn món khác!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (data_type.equals("boolean")) {
                            if (!multiple) {
                                if (required) {
                                    if (lastPos != mAdapter.getPositionInSection(getAdapterPosition())) {
                                        optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).setSelected(!cur.isSelected());
                                        if (lastPos != -1)
                                            optionList.get(lastPos).setSelected(false);
                                        mAdapter.notifyItemRangeChangedInSection(category, 0, optionList.size());
                                    }
                                } else {
                                    if (cur.isSelected())
                                        mAddonList.remove(cur);
                                    optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).setSelected(!cur.isSelected());
                                    mAdapter.notifyItemRangeChangedInSection(category, 0, optionList.size());
                                }
                            } else {
                                // AddonModel cur = optionList.get(mAdapter.getPositionInSection(getAdapterPosition()));
                                if (cur.isSelected()) {
                                    total_quantity--;
                                    mAddonList.remove(cur);
                                } else {
                                    if (total_quantity >= max) {
                                        Toast.makeText(activity, "Số lượng tối đa là " + max, Toast.LENGTH_SHORT).show();
                                        return;
                                    } else {
                                        total_quantity++;
                                        mAddonList.add(cur);
                                    }
                                }
                                optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).setSelected(!cur.isSelected());
                                mAdapter.notifyItemChangedInSection(category, mAdapter.getPositionInSection(getAdapterPosition()));
                            }

                        } else {
                            Double dObj = Double.valueOf(String.valueOf(optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getDefault_value()));
                            double incr = dObj.doubleValue() + 1.0;
                            if ((double) max >= total_quantity) {
                                if (optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMax_number_value() != null &&
                                        optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMax_number_value() >= incr) {
                                    total_quantity++;
                                    optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).setDefault_value(incr);
                                    mAdapter.notifyItemChangedInSection(category, mAdapter.getPositionInSection(getAdapterPosition()));
                                } else {
                                    Toast.makeText(activity, "Số lượng tối đa của topping này là " + optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMax_number_value(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(activity, "Số lượng tối đa là " + max, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                } else if (v == rl_sub) {
                    Double dObj = Double.valueOf(String.valueOf(optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getDefault_value()));
                    double dec = dObj.doubleValue() - 1.0;
                    if ((double) min <= total_quantity) {

                        if (optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMin_number_value() != null &&
                                optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMin_number_value() <= dec) {
                            total_quantity--;
                            optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).setDefault_value(dec);
                            mAdapter.notifyItemChangedInSection(category, mAdapter.getPositionInSection(getAdapterPosition()));
                        } else {
                            Toast.makeText(activity, "Số lượng tối thiểu của topping này là " + optionList.get(mAdapter.getPositionInSection(getAdapterPosition())).getMin_number_value(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, "Số lượng tối thiểu là " + min, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }
}
