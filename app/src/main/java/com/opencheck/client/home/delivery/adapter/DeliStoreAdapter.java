package com.opencheck.client.home.delivery.adapter;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowSearchStoreDeliBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.models.merchant.TagModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class DeliStoreAdapter extends RecyclerView.Adapter<DeliStoreAdapter.StoreHolder> {

    private LixiActivity activity;
    private ArrayList<StoreOfCategoryModel> arrayList = new ArrayList<>();

    public DeliStoreAdapter(LixiActivity activity, ArrayList<StoreOfCategoryModel> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    public void setListStore(ArrayList<StoreOfCategoryModel> list) {
        if (arrayList == null || arrayList.size() == 0) {
            arrayList = list;
        } else
            arrayList.addAll(list);
        notifyDataSetChanged();
    }

    public ArrayList<StoreOfCategoryModel> getListStore() {
        return arrayList;
    }

    @Override
    public StoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowSearchStoreDeliBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_search_store_deli, parent, false);
        return new StoreHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(StoreHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    private OnStoreClickListener onStoreClickListener;

    public void setOnStoreClickListener(OnStoreClickListener onStoreClickListener) {
        this.onStoreClickListener = onStoreClickListener;
    }

    class StoreHolder extends RecyclerView.ViewHolder {
        RowSearchStoreDeliBinding mBinding;

        public StoreHolder(RowSearchStoreDeliBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onStoreClickListener != null) {
                        onStoreClickListener.onClick();
                    }
                    try {
                        StoreDetailActivity.startStoreDetailActivity(activity, arrayList.get(getAdapterPosition()).getId(), TrackingConstant.ContentSource.SEARCH);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            });
        }

        public void bind() {
            final StoreOfCategoryModel store = arrayList.get(getAdapterPosition());
            if (store != null) {
                LixiImage.with(activity)
                        .load(store.getLogo_link())
                        .size(ImageSize.SMALL)
                        .into(mBinding.imgPic);

                int width = activity.widthScreen / 4;
                ViewGroup.LayoutParams params = mBinding.imgPic.getLayoutParams();
                params.width = width;
                params.height = 3 * width / 4;
                mBinding.imgPic.setLayoutParams(params);
                mBinding.tvOpenTime.setMaxWidth(width);

                mBinding.txtName.setText(store.getName());
                mBinding.tagView.removeAll();
                if (store.getList_tag() != null && store.getList_tag().size() != 0) {
                    mBinding.tagView.setVisibility(View.VISIBLE);
                    for (TagModel tag : store.getList_tag()) {
                        mBinding.tagView.addTag(tag.getName());
                    }
                } else {
                    mBinding.tagView.setVisibility(View.GONE);
                }

                String time = "";
                if (store.getShipping_duration_min() <= 60) {
                    time = store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + "'";
                } else {
                    time = ">" + store.getShipping_duration_min() + "'";
                }
                mBinding.txtTime.setText(time);

                String discount = "";

                String lixi = store.getCashback_price() + "";
                if (lixi.equals("0")) {
                    lixi = "";
                }

                if (store.isIs_opening()) {
                    mBinding.tvOpenTime.setVisibility(View.GONE);
                    mBinding.ivStoreClose.setVisibility(View.GONE);
                    mBinding.imgPic.setColorFilter(null);
                } else {
                    mBinding.tvOpenTime.setText(String.format(LanguageBinding.getString(R.string.open_close_time, activity), store.getOpen_time_text()));
                    mBinding.tvOpenTime.setVisibility(View.VISIBLE);
                    mBinding.ivStoreClose.setVisibility(View.VISIBLE);
                    mBinding.imgPic.setColorFilter(Color.parseColor("#CC000000"));
                }

                String promotion = store.getPromotion_text();
                if (promotion == null || promotion.equals("")) {
                    if (lixi.equals("")) {
                        mBinding.linearDiscount.setVisibility(View.GONE);
                    } else {
                        mBinding.linearDiscount.setVisibility(View.VISIBLE);
                        mBinding.txtDiscount.setText(lixi);
                    }
                } else {
                    if (lixi.equals("")) {
                        discount = promotion;
                    } else {
                        discount = lixi + "Lixi|" + promotion;
                    }
                    mBinding.linearDiscount.setVisibility(View.VISIBLE);
                    mBinding.txtDiscount.setText(discount);
                }
            }
        }
    }

    public interface OnStoreClickListener {
        void onClick();
    }
}
