package com.opencheck.client.home.account.new_layout.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogNotificationForCardBinding;
import com.opencheck.client.utils.AppPreferenceHelper;

public class NotificationForCardDialog extends LixiDialog {

    public NotificationForCardDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    TextView txtTitle, txtContent, txtOk;

    private DialogNotificationForCardBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogNotificationForCardBinding
                .inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtContent = (TextView) findViewById(R.id.txtContent);
        txtOk = (TextView) findViewById(R.id.txtOk);

        txtOk.setOnClickListener(this);
    }

    public void setData(String title, String content) {
        txtTitle.setText(title);
        txtContent.setText(content);
    }

    @Override
    public void onClick(View view) {
        cancel();
    }
}
