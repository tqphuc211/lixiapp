package com.opencheck.client.home.rating.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowRatingOptionBinding;
import com.opencheck.client.home.rating.control.ColorFilterUtils;
import com.opencheck.client.home.rating.model.RatingOption;

import java.util.ArrayList;

public class RatingOptionAdapter extends RecyclerView.Adapter<RatingOptionAdapter.OptionHolder> {
    private LixiActivity activity;
    private ArrayList<RatingOption> listOption;

    // Var
    private int checkedItem = -1;
    private int lastChecked = -1;

    public RatingOptionAdapter(LixiActivity activity, ArrayList<RatingOption> listOption) {
        this.activity = activity;
        this.listOption = listOption;
    }

    @Override
    public OptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowRatingOptionBinding rowRatingOptionBinding =
                RowRatingOptionBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new OptionHolder(rowRatingOptionBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(OptionHolder holder, int position) {
        RatingOption option = listOption.get(position);
        if (option != null) {
            Log.d("TTTT", (new Gson()).toJson(option));
            ImageLoader.getInstance().displayImage(option.getImage(), holder.imgOption);
            holder.txtContent.setText(option.getValue());

            if (checkedItem == position) {
                holder.imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_VIOLET));
                holder.frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating_violet));
                holder.txtContent.setTextColor(activity.getResources().getColor(R.color.app_violet));
            } else {
                holder.imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_DARK));
                holder.frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating));
                holder.txtContent.setTextColor(Color.parseColor("#AEAEAE"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOption != null ? listOption.size() : 0;
    }

    public class OptionHolder extends RecyclerView.ViewHolder {
        ImageView imgOption;
        TextView txtContent;
        FrameLayout frameOption;

        public OptionHolder(View itemView) {
            super(itemView);
            imgOption = (ImageView) itemView.findViewById(R.id.imgOption);
            txtContent = (TextView) itemView.findViewById(R.id.txtContent);
            frameOption = (FrameLayout) itemView.findViewById(R.id.frameOption);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkedItem == getAdapterPosition()) {
                        return;
                    }

                    checkedItem = getAdapterPosition();
                    imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_VIOLET));
                    frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating_violet));
                    txtContent.setTextColor(activity.getResources().getColor(R.color.app_violet));
                    if (lastChecked >= 0) {
                        notifyItemChanged(lastChecked);
                    }
                    lastChecked = checkedItem;
                }
            });
        }
    }
}
