package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ProductMerchantHotItemBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class ProductMerchantHotAdapter extends RecyclerView.Adapter<ProductMerchantHotAdapter.ViewHolder>  {

    private LixiActivity activity;
    private ArrayList<ProductMerchantHot> listProductMerchantHot;

    public ProductMerchantHotAdapter(LixiActivity activity, ArrayList<ProductMerchantHot> listProductMerchantHot) {
        this.activity = activity;
        this.listProductMerchantHot = listProductMerchantHot;
    }

    public void setListProductMerchantHot(ArrayList<ProductMerchantHot> listProductMerchantHot) {
        this.listProductMerchantHot = listProductMerchantHot;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ProductMerchantHotItemBinding binding = ProductMerchantHotItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return listProductMerchantHot == null ? 0 : listProductMerchantHot.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ProductMerchantHotItemBinding binding;

        public ViewHolder(ProductMerchantHotItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindData(int position) {
            final ProductMerchantHot model = listProductMerchantHot.get(position);

            binding.tvName.setText(model.getName());

            LixiImage.with(activity).load(model.getImage_link()).size(ImageSize.AUTO).into(binding.ivAvatar);
            int width = activity.widthScreen * 5 / 9;
            ViewGroup.LayoutParams params = binding.ivAvatar.getLayoutParams();
            params.height = 3 * width / 4;
            params.width = params.height;
            binding.ivAvatar.setLayoutParams(params);
            binding.tvName.setMaxWidth(3 * width / 4);

            if (position == listProductMerchantHot.size() - 1) {
                float density = activity.getResources().getDisplayMetrics().density;
                int paddingPixel = (int) (activity.getResources().getDimension(R.dimen.value_3) * density);
                binding.rootView.setPadding(paddingPixel, 0, paddingPixel, paddingPixel);
            } else {
                float density = activity.getResources().getDisplayMetrics().density;
                int paddingPixel = (int) (activity.getResources().getDimension(R.dimen.value_3) * density);
                binding.rootView.setPadding(paddingPixel, 0, 0, paddingPixel);
            }

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(params.width, ViewGroup.LayoutParams.WRAP_CONTENT);
            binding.rootView.setLayoutParams(layoutParams);

            binding.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StoreDetailActivity.startStoreDetailActivity(activity, model.getStore_id(), TrackingConstant.ContentSource.HOME, model.getId());
                }
            });
        }
    }
 }
