package com.opencheck.client.home.game.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.opencheck.client.R;

@SuppressLint("AppCompatCustomView")
public class GameButton extends ImageView {
    private Context context;

    public GameButton(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public GameButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public GameButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    public GameButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        initView();
    }

    private void initView() {

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.shake_button_game_anim);
        this.setAnimation(animation);
    }
}
