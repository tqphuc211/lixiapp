package com.opencheck.client.home.delivery.controller;

import android.databinding.DataBindingUtil;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StorePolicyItemLayoutBinding;
import com.opencheck.client.home.delivery.model.StorePolicyModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class StorePolicyController {
    private LixiActivity activity;
    private LinearLayout lnRoot;
    private StorePolicyItemLayoutBinding mBinding;
    private StorePolicyModel storePolicyModel;
    private boolean isHaveDefault;
    private int position;

    public StorePolicyController(LixiActivity activity, LinearLayout _lnRoot, StorePolicyModel storePolicyModel, int position, boolean isHaveDefault) {
        this.activity = activity;
        this.lnRoot = _lnRoot;
        this.storePolicyModel = storePolicyModel;
        this.isHaveDefault = isHaveDefault;
        this.position = position;

        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.store_policy_item_layout, null, false);
        this.initData();
        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(mBinding.getRoot());
            }
        });
    }

    private void initData() {
        String dot = LanguageBinding.getString(R.string.dot, activity);
        if (storePolicyModel.getName().contains("-")) {
            String nameStr = storePolicyModel.getName();
            String[] list = nameStr.split("-");
            mBinding.tvName.setText(Html.fromHtml("<b>" + list[0] + " - <font color='#b0b0b0'>" + list[1] + "</font></b>"));
        } else mBinding.tvName.setText(Html.fromHtml(storePolicyModel.getName() + " "));
        if (storePolicyModel.getList_promotion_code() != null && storePolicyModel.getList_promotion_code().size() > 0)
            mBinding.tvCode.setText(Html.fromHtml("- Mã <b><font color='#ac1ea1'>" + storePolicyModel.getList_promotion_code().get(0) + "</font></b>"));
        if (storePolicyModel.getDescription() != null && !storePolicyModel.getDescription().isEmpty())
            mBinding.tvContent.setText(Html.fromHtml(dot + storePolicyModel.getDescription()));
        else if (storePolicyModel.getPromotion_text() != null && !storePolicyModel.getPromotion_text().isEmpty())
            mBinding.tvContent.setText(Html.fromHtml(dot + storePolicyModel.getPromotion_text()));
        else mBinding.tvContent.setVisibility(View.GONE);

        mBinding.tvExpireDate.setText(Html.fromHtml(dot + "Hạn dùng: <b><font color='#000000'>" + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_NO_SECONDS, String.valueOf(storePolicyModel.getEnd_date())) + "</font></b>"));

        if (storePolicyModel.getList_payment_method_name() != null && storePolicyModel.getList_payment_method_name().size() > 0) {
            mBinding.tvPaymentMethod.setVisibility(View.VISIBLE);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < storePolicyModel.getList_payment_method_name().size(); i++) {
                builder.append(storePolicyModel.getList_payment_method_name().get(i));
                if (i != storePolicyModel.getList_payment_method_name().size() - 1)
                    builder.append(",");
            }
            mBinding.tvPaymentMethod.setText(Html.fromHtml(dot + "Áp dụng với: <font color='#b0b0b0'>" + builder.toString() + "</font>"));
        } else {
            mBinding.tvPaymentMethod.setVisibility(View.GONE);
        }

        if (storePolicyModel.getType().equals("promotion_code")) {
            mBinding.rootView.setBackgroundColor(activity.getResources().getColor(R.color.color_orange_light));
        } else {
            mBinding.rootView.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }

        if (position == 0 && !isHaveDefault) {
            mBinding.vPolicy.setVisibility(View.GONE);
        } else mBinding.vPolicy.setVisibility(View.VISIBLE);
    }
}
