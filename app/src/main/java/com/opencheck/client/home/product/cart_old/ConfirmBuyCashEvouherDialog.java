package com.opencheck.client.home.product.cart_old;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.EditTextView;
import com.opencheck.client.databinding.LixiEvoucherConfirmBuyActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodModel;
import com.opencheck.client.home.product.buyatstore.DialogBuyAtStore;
import com.opencheck.client.home.product.cod.PaymentCODSuccessDialog;
import com.opencheck.client.home.product.gateway.DialogTransactions;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.OrderCheckModel;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.lixishop.QuantityProductModel;
import com.opencheck.client.models.lixishop.SingleMethodPaymentModel;
import com.opencheck.client.models.merchant.DiscountModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class ConfirmBuyCashEvouherDialog extends LixiTrackingDialog {

    public ConfirmBuyCashEvouherDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private static final int MAX_MONEY_ALLOWED = 500000000;
    private BroadcastReceiver broadcastReceiver;

    public static final String PRODUCT_MODEL = "PRODUCT_MODEL";


    private ImageView iv_back;
    private ImageView iv_sub;
    private ImageView iv_add;
    private ImageView iv_tick_total, iv_tick_min;
    private TextView tv_product_name;
    private TextView tv_count;
    private TextView tv_acc;
    private TextView tv_buy;
    private TextView tv_cash;
    private EditTextView ed_phone;
    private TextView tv_cashback;
    private TextView tv_lixi_cashback;
    private TextView tv_lixi_point_use;
    private TextView tv_point, tv_total_message, tv_min_message;
    private TextView tv_product_name_title, tv_count_title, tv_account_title, tv_fee_title, tv_cashback_title, tv_lixi_title, tv_total_title, tv_title, tv_method_title;
    private RelativeLayout rlMessenger;
    private TextView tvLimit;
    private LinearLayout lnNote;
    private QuantityProductModel quantityProductModel;
    private LinearLayout lnCount;
    private LinearLayout llPrice;

    private CheckBox cb_lixi_point_use;
    private RecyclerView rv;

    private ProductCashVoucherDetailModel productModel;
    private UserModel userInfo;
    private OrderCheckModel orderCheckModel;
    private AdapterPaymentMethod adapterPaymentMethod;
    boolean isExist = false;
    //    private PopupReorder popupReorder;
    int sl = 1;
    long id = -1;

    private boolean isStartUp = true;
    private long lixi_discount = 0;
    private String idProduct;
    private String supportPhone;
    private boolean isFlashSale;
    private long totalPrice = -1;
    private long totalLixi = -1;

    //region setup dialog


    public void setData(long id, String idProduct, ProductCashVoucherDetailModel productModel, String supportPhone, boolean isFlashSale) {
        this.id = id;
        this.supportPhone = supportPhone;
        this.idProduct = idProduct;
        this.productModel = productModel;
        this.isFlashSale = isFlashSale;

        if (isFlashSale) {
            totalPrice = productModel.getFlash_sale_info().getPayment_discount_price();
            totalLixi = productModel.getFlash_sale_info().getCashback_price();
        } else {
            totalPrice = productModel.getPayment_discount_price();
            totalLixi = productModel.getCashback_price();
        }

        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LoadQuantityAllow();
                    Receiver();
                    setViewData();
                    setClickListener();
                } else {
                    Helper.showErrorDialog(activity, "Thông tin người dùng không hợp lệ!");
                    dismiss();
                }
            }
        });

        trackScreen();
    }

    private void setClickListener() {
        iv_back.setOnClickListener(this);
        iv_sub.setOnClickListener(this);
        iv_add.setOnClickListener(this);
        tv_buy.setOnClickListener(this);
        cb_lixi_point_use.setOnClickListener(this);
        rlMessenger.setOnClickListener(this);
        lnCount.setOnClickListener(this);
        lnCount.setClickable(false);
    }

    private LixiEvoucherConfirmBuyActivityBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiEvoucherConfirmBuyActivityBinding.inflate(
                getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();

    }

    private void Receiver() {
        try {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    LoadQuantityAllow();
                }
            };
        } catch (Exception e) {
        }

    }

    private void findViews() {
        lnCount = (LinearLayout) findViewById(R.id.lnCount);
        tvLimit = (TextView) findViewById(R.id.tvLimit);
        lnNote = (LinearLayout) findViewById(R.id.lnNote);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_product_name = (TextView) findViewById(R.id.tv_product_name);
        tv_count = (TextView) findViewById(R.id.tv_count);
        tv_acc = (TextView) findViewById(R.id.tv_acc);
        tv_buy = (TextView) findViewById(R.id.tv_buy);
        tv_cash = (TextView) findViewById(R.id.tv_cash);
        tv_cashback = (TextView) findViewById(R.id.tv_cashback);
        tv_lixi_point_use = (TextView) findViewById(R.id.tv_lixi_point_use);
        tv_lixi_cashback = (TextView) findViewById(R.id.tv_lixi_cashback);
        tv_point = (TextView) findViewById(R.id.tv_point);
        tv_product_name_title = (TextView) findViewById(R.id.tv_product_name_title);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_count_title = (TextView) findViewById(R.id.tv_count_title);
        tv_account_title = (TextView) findViewById(R.id.tv_account_title);
        tv_fee_title = (TextView) findViewById(R.id.tv_fee_title);
        tv_cashback_title = (TextView) findViewById(R.id.tv_cashback_title);
        tv_lixi_title = (TextView) findViewById(R.id.tv_lixi_title);
        tv_method_title = (TextView) findViewById(R.id.tv_method_title);
        tv_total_title = (TextView) findViewById(R.id.tv_total_title);
        ed_phone = (EditTextView) findViewById(R.id.ed_phone);
        tv_total_message = (TextView) findViewById(R.id.tv_total_message);
        tv_min_message = (TextView) findViewById(R.id.tv_min_message);
        iv_sub = (ImageView) findViewById(R.id.iv_sub);
        iv_add = (ImageView) findViewById(R.id.iv_add);
        iv_tick_total = (ImageView) findViewById(R.id.iv_tick_total);
        iv_tick_min = (ImageView) findViewById(R.id.iv_tick_min);
        rlMessenger = (RelativeLayout) findViewById(R.id.rlMessenger);
        rv = (RecyclerView) findViewById(R.id.rv);
        llPrice = findViewById(R.id.ll_price);
        cb_lixi_point_use = (CheckBox) findViewById(R.id.cb_lixi_point_use);
    }


    public void setViewData() {

        UserModel userModel = UserModel.getInstance();

        userInfo = userModel;

        tv_product_name.setText(productModel.getName());
        tv_count.setText("1");
        tv_acc.setText(userModel.getFullname());
        tv_cashback.setText(totalLixi + "%");
        tv_cash.setText(Helper.getVNCurrency(getPaymentPrice()) + activity.getString(R.string.p));
        ed_phone.setText(userInfo.getPhone());
        ed_phone.setSelection(userInfo.getPhone().length());
        ed_phone.clearFocus();
        ed_phone.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event != null &&
                                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            if (event == null || !event.isShiftPressed()) {
                                // the user is done typing.
                                ed_phone.clearFocus();
                                hiddenKeyboard();
                                return true; // consume.
                            }
                        }
                        return false; // pass on to other listeners.
                    }
                }
        );
        ed_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    checkPhoneValidate();
                }
            }
        });
        getLixiDiscount();

//        cb_lixi_point_use.performClick();

        int lixi_cashback = calculateLixiCashback();
        //Hidden lixi cash back when lixi cash back price equal 0
        if (lixi_cashback <= 0) {
            llPrice.setVisibility(View.GONE);
        } else {
            llPrice.setVisibility(View.VISIBLE);
            tv_lixi_cashback.setText("+" + Helper.getVNCurrency(lixi_cashback) + " " + activity.getString(R.string.lixi));
        }
        rv.setLayoutManager(new LinearLayoutManager(activity));
        adapterPaymentMethod = new AdapterPaymentMethod(activity, productModel.getPayment_method());
        rv.setAdapter(adapterPaymentMethod);
        adapterPaymentMethod.notifyDataSetChanged();
//        setDefaultPaymentMethod();

        changeCheckBoxColor(cb_lixi_point_use);

        tv_product_name_title.setText(LanguageBinding.getString(R.string.popup_confirm_product, activity));
        tv_account_title.setText(LanguageBinding.getString(R.string.account, activity));
        tv_cashback_title.setText(LanguageBinding.getString(R.string.lixi_cash_back, activity));
        tv_method_title.setText(LanguageBinding.getString(R.string.payment_method, activity));
        tv_point.setText(LanguageBinding.getString(R.string.popup_confirm_using, activity));

    }

    private boolean checkPhoneValidate() {
        if (ed_phone.getText().toString().equals("") ||
                !Helper.isValidPhone(ed_phone.getText().toString())) {
            ed_phone.setText(userInfo.getPhone());
            ed_phone.setSelection(userInfo.getPhone().length());
            return false;
        }
        return true;
    }

    private void hiddenKeyboard() {
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    private void setDefaultPaymentMethod() {
        boolean isClicked = false;
        if (productModel.getPayment_method().size() == 0) {
            return;
        }
        for (int i = 0; i < productModel.getPayment_method().size(); i++) {
            if (productModel.getPayment_method().get(i).isSelected()) {
                isClicked = true;
            }
        }
        if (!isClicked)
            adapterPaymentMethod.performClick(0);
    }

    private int calculateLixiCashback() {
        return (int) (totalLixi * sl);
    }

    //endregion

    public static long order_id = -1;
    public static boolean isReorder = false;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_sub:
                LoadQuantityAllow();
                sl = Math.max(1, sl - 1);
                getLixiDiscount();
                if (sl == 1) {
                    iv_sub.setVisibility(View.GONE);
                }
                iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                iv_add.setVisibility(View.VISIBLE);
                iv_add.setClickable(true);
                tv_count.setText(sl + "");
                int lixi_cashback_sub = calculateLixiCashback();
                tv_lixi_cashback.setText("+" + Helper.getVNCurrency(lixi_cashback_sub) + activity.getString(R.string.lixi));
                lnCount.setClickable(false);
                break;
            case R.id.iv_add:
                sl = sl + 1;
                LoadQuantityAllowForAdd();
                getLixiDiscount();
                iv_sub.setVisibility(View.VISIBLE);
                float totalPayment = getPaymentPrice();

                if (productModel.getPayment_discount_price() > 0) {
                    if (MAX_MONEY_ALLOWED / productModel.getPayment_discount_price() == sl) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                    }

                    if (MAX_MONEY_ALLOWED / productModel.getPayment_discount_price() == sl) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                    }
                }

                int lixi_cashback_add = calculateLixiCashback();
                tv_lixi_cashback.setText("+" + Helper.getVNCurrency(lixi_cashback_add) + activity.getString(R.string.lixi));

                tv_count.setText(sl + "");
                break;
            case R.id.tv_buy:
                if (!checkPhoneValidate()) {
                    showErrorToast(activity.getString(R.string.recharge_require_phone_syntax));
                    return;
                }
                if (adapterPaymentMethod.getCurrentPaymentMethod() != null) {
                    checkExistOrder();
//                    LoadQuantityAllow();
                } else
                    showErrorToast(activity.getString(R.string.popup_confim_err_select_method));
                break;
            case R.id.cb_lixi_point_use:
                changeCheckBoxColor(cb_lixi_point_use);
                if (cb_lixi_point_use.isChecked()) {
                    tv_cash.setText(Helper.getVNCurrency(getPaymentPrice() - lixi_discount) + activity.getString(R.string.p));
                    tv_lixi_point_use.setTextColor(activity.getResources().getColor(R.color.app_violet));
                    tv_point.setTextColor(activity.getResources().getColor(R.color.my_black));
                    cb_lixi_point_use.setText(activity.getString(R.string.use) + " " + Helper.getVNCurrency(lixi_discount) + " " + activity.getString(R.string.popup_confim_cb_payment_point));
                } else {
                    tv_cash.setText(Helper.getVNCurrency(getPaymentPrice()) + activity.getString(R.string.p));
                    tv_lixi_point_use.setTextColor(activity.getResources().getColor(R.color.app_violet));
                    tv_point.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
                    cb_lixi_point_use.setText(activity.getString(R.string.popup_confim_cb_can_using) + " " + Helper.getVNCurrency(lixi_discount) + " " + activity.getString(R.string.popup_confim_cb_payment_point));
                }
                break;
            case R.id.rlMessenger:
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                } catch (Exception e) {
                    showErrorToast(activity.getString(R.string.popup_confim_err_email));
                }
                break;
            case R.id.lnCount:
                if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                    return;
                } else {
                    if (quantityProductModel.getMax_cash_quantity_allowed() == 0) {
                        String content = LanguageBinding.getString(R.string.over_quantity, activity);
                        showErrorToast(content);
                    } else {
                        String content = String.format(LanguageBinding.getString(R.string.quantity_limit, activity), quantityProductModel.getMax_cash_quantity_allowed());
                        showErrorToast(content);
                    }

                }
                break;
        }
    }

    private boolean checkExistOrder() {
        DataLoader.checkProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    orderCheckModel = (OrderCheckModel) object;
                    if (orderCheckModel != null) {
                        callReorderPopup(orderCheckModel);
                        //check order type here
                    } else {
                        if (quantityProductModel.getMax_cash_quantity_allowed() == 0) {
                            String content = LanguageBinding.getString(R.string.over_quantity, activity);
                            showErrorToast(content);
                        } else
                            startTransaction();
                    }


                } else {
                    showErrorToast(object.toString());
                }
            }
        }, productModel.getId());

        return isExist;
    }

    private void LoadQuantityAllow() {
        DataLoader.getProductQuantity(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    quantityProductModel = null;
                    quantityProductModel = (QuantityProductModel) object;
                }

                if (quantityProductModel.getLimit_cash() > 0) {
                    lnNote.setVisibility(View.VISIBLE);
                    tvLimit.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_limit_product, activity), quantityProductModel.getLimit_cash()));
                } else {
                    if (quantityProductModel.getLimit_cash() == -1 || quantityProductModel.getLimit_cash() == 0)
                        lnNote.setVisibility(View.GONE);
                }

                if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                    return;
                } else {
                    if (sl < quantityProductModel.getMax_cash_quantity_allowed()) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }
                    if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }
                    if (quantityProductModel.getMax_cash_quantity_allowed() == 0 || quantityProductModel.getMax_cash_quantity_allowed() == 1) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                        sl = 1;
                        tv_count.setText(sl + "");

                    } else
                        lnCount.setClickable(false);

                    if (sl >= quantityProductModel.getMax_cash_quantity_allowed()) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                    }

                    if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }
                }
            }
        }, Long.parseLong(idProduct));


    }

    private void LoadQuantityAllowForAdd() {

        DataLoader.getProductQuantity(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    quantityProductModel = null;
                    quantityProductModel = (QuantityProductModel) object;
                }

                if (quantityProductModel.getLimit_cash() > 0) {
                    lnNote.setVisibility(View.VISIBLE);
                    tvLimit.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_limit_product, activity), quantityProductModel.getLimit_cash()));
                } else {
                    if (quantityProductModel.getLimit_cash() == -1 || quantityProductModel.getLimit_cash() == 0)
                        lnNote.setVisibility(View.GONE);
                }

                if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                    return;
                } else {
                    if (sl < quantityProductModel.getMax_cash_quantity_allowed()) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }
                    if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }
                    if (quantityProductModel.getMax_cash_quantity_allowed() == 0 || quantityProductModel.getMax_cash_quantity_allowed() == 1) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                        sl = 1;
                        tv_count.setText(sl + "");

                    } else
                        lnCount.setClickable(false);

                    if (sl >= quantityProductModel.getMax_cash_quantity_allowed()) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                        String content = String.format(LanguageBinding.getString(R.string.quantity_limit, activity), quantityProductModel.getMax_cash_quantity_allowed());
                        showErrorToast(content);
                    }

                    if (quantityProductModel.getMax_cash_quantity_allowed() == -1) {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }

                }
            }
        }, Long.parseLong(idProduct));

    }

    private void callReorderPopup(final OrderCheckModel orderCheckModel) {
        if (orderCheckModel.getId() != 0) {
            ReordeDialog reorderDialog = new ReordeDialog(activity, false, true, false);
            reorderDialog.setCancelable(true);
            reorderDialog.setCanceledOnTouchOutside(true);
            reorderDialog.setData(orderCheckModel, productModel.getProduct_image().get(0), productModel.getName());
            reorderDialog.show();
            return;
        } else {
            if (quantityProductModel.getMax_cash_quantity_allowed() == 0) {
                String content = LanguageBinding.getString(R.string.over_quantity, activity);
                showErrorToast(content);
            } else
                startTransaction();
        }
    }

    private final String VTC_PAYMENT = "VTC";
    private final String ZALO_PAYMENT = "ZALO";
    private final String PAYOO_PAYMENT = "PAYOO";
    private String paymentMethod = "VTC";

    private void startTransaction() {
        PaymentMethodModel paymentMethodModel = adapterPaymentMethod.getCurrentPaymentMethod();
        if (paymentMethodModel.getKey().equals("cod")) {
            startCODTransaction(paymentMethodModel);
            return;
        }
        if (paymentMethodModel.getKey().equals("at_store")) {
            DialogBuyAtStore basDialog = new DialogBuyAtStore(activity, false, false, false);
            basDialog.setIDidalogEventListener(new DialogBuyAtStore.IDidalogEvent() {
                @Override
                public void onOk() {
                    dismiss();
                }
            });
            basDialog.show();
            basDialog.setData(productModel, paymentMethodModel, sl, userInfo, cb_lixi_point_use.isChecked(), lixi_discount, totalPrice, totalLixi);
            return;
        }
        if (paymentMethodModel.getKey().contains("zalo"))
            paymentMethod = "ZALO";
        else if (paymentMethodModel.getKey().contains("payoo"))
            paymentMethod = "PAYOO";
        else if (paymentMethodModel.getKey().contains("napas"))
            paymentMethod = "NAPAS";
        else paymentMethod = "VTC";

        if (paymentMethod.equals(VTC_PAYMENT)) {
            startVTCBankTransaction();
            return;
        }

        int selectBankInApp = 0;
        try {
            selectBankInApp = ConfigModel.getInstance(activity).getLixi_select_bank();
        } catch (Exception e) {
        }
        if (paymentMethod.equals(PAYOO_PAYMENT)) {
            if (selectBankInApp > 0) {
                startPayooBankTransaction();
            } else {
                if (productModel.getOrder_method().equals("v2")
                        && paymentMethodModel.getKey().contains("payoo"))
                    startTransactionV2(paymentMethodModel);
                else
                    startTransaction(paymentMethodModel);

            }
            return;
        }

        if (paymentMethod.equals(ZALO_PAYMENT)) {
            startZaloBankTransaction();
            return;
        }

        if (paymentMethodModel.getKey().contains("napas")) {
            checkoutNapas(paymentMethodModel);
        }
    }

    //region NAPAS
    public void checkoutNapas(PaymentMethodModel paymentMethodModel) {
        if (productModel.getOrder_method().equals("v2")) {
            buyProductNapasV2(paymentMethodModel);
        } else {
            buyProductNapas(paymentMethodModel);
        }
    }

    private void buyProductNapas(PaymentMethodModel paymentMethodModel) {
        final JsonObject obj = new JsonObject();
        obj.addProperty("create_token", true);
//        obj.addProperty("napas_token", napas_token);
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        obj.addProperty("phone", ed_phone.getText().toString().trim());
        obj.addProperty("payment_method", paymentMethodModel.getKey());
        if (cb_lixi_point_use.isChecked())
            obj.addProperty("user_discount", true);

        DataLoader.buyProductNapas(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = "";
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    Long productId = jsonObject.get("product").getAsLong();
                    showTransactionDialog(payment_link, orderId, productId);
                }
            }
        }, obj);
    }

    private void buyProductNapasV2(PaymentMethodModel paymentMethodModel) {
        final JsonObject obj = new JsonObject();
        obj.addProperty("create_token", true);
//        obj.addProperty("napas_token", napas_token);
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        obj.addProperty("phone", ed_phone.getText().toString().trim());
        obj.addProperty("payment_method", paymentMethodModel.getKey());
        if (cb_lixi_point_use.isChecked())
            obj.addProperty("user_discount", true);

        DataLoader.buyProductNapasV2(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = "";
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    Long productId = jsonObject.get("product").getAsLong();
                    showTransactionDialog(payment_link, orderId, productId);
                }
            }
        }, obj);
    }

    public void showTransactionDialog(String payment_link, Long orderId, Long productId) {
        DialogTransactions popupTransaction = new DialogTransactions(activity, false, false, false);
        popupTransaction.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dismiss();
            }
        });
        popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
            @Override
            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                dismiss();
                if (eventListener != null)
                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
            }
        });
        popupTransaction.show();
        //TODO: fix name
        popupTransaction.setData(payment_link, false, orderId);
    }
    //endregion

    private void startCODTransaction(final PaymentMethodModel paymentMethodModel) {
        try {
            PaymentCodBody paymentCodBody = new PaymentCodBody();
            paymentCodBody.setId(productModel.getId());
            if (ConfirmBuyCashEvouherDialog.isReorder) {
                ConfirmBuyCashEvouherDialog.isReorder = false;
                paymentCodBody.setOrder_id(ConfirmBuyCashEvouherDialog.order_id);
                ConfirmBuyCashEvouherDialog.order_id = -1;
            }
            paymentCodBody.setPayment_method(paymentMethodModel.getKey());
            paymentCodBody.setQuantity(sl);
            paymentCodBody.setPhone(ed_phone.getText().toString().trim());
            paymentCodBody.setUser_discount(cb_lixi_point_use.isChecked());
            DataLoader.paymentCod(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        PaymentCodModel paymentCodModel = (PaymentCodModel) object;
                        PaymentCODSuccessDialog codSuccessDialog = new PaymentCODSuccessDialog(activity, false, true, false);
                        codSuccessDialog.show();
                        long lixiDiscount = cb_lixi_point_use.isChecked() ? lixi_discount : 0;
//                        long totalPrice = getPaymentPrice() - lixiDiscount;
                        codSuccessDialog.setData(paymentCodModel, productModel, totalPrice,
                                lixiDiscount, totalLixi,
                                ed_phone.getText().toString().trim());
                        codSuccessDialog.setListener(new PaymentCODSuccessDialog.OnPaymentCodClickListener() {
                            @Override
                            public void onDone() {
                                dismiss();
                                activity.finish();
                            }
                        });
                    } else {
                        showErrorToast((String) object);
                    }

                }
            }, paymentCodBody);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void startTransactionV2(final PaymentMethodModel paymentMethodModel) {

        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", productModel.getId());
            jsonObject.put("quantity", sl);
            jsonArray.put(jsonObject);

            JSONObject objs = new JSONObject();
//            objs.put("bank_code", paymentMethodAdapter.getSingleMethodPaymentModel().getPayment_method().getBankSelected().getCode());
            objs.put("phone", ed_phone.getText().toString().trim());
            objs.put("payment_method", paymentMethodModel.getKey());
            objs.put("user_discount", cb_lixi_point_use.isChecked());
            objs.put("list_product", jsonArray);

            DataLoader.buyCashProductV2(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        JsonObject jsonObject = (JsonObject) object;
                        String payment_link = "";
                        payment_link = jsonObject.get("payment_link").getAsString();
                        Long orderId = jsonObject.get("id").getAsLong();
                        Long productId = jsonObject.get("product").getAsLong();
                        DialogTransactions popupTransaction = new DialogTransactions(activity, false, false, false);
                        popupTransaction.setOnDismissListener(new OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //  dismiss();
                            }
                        });
                        popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
                            @Override
                            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                                dismiss();
                                if (eventListener != null)
                                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
                            }
                        });
//                        long cash = 0;
//                        if (cb_lixi_point_use.isChecked()) {
//                            cash = getPaymentPrice() - lixi_discount;
//                        } else {
//                            cash = getPaymentPrice();
//                        }
                        SingleMethodPaymentModel singleMethodPaymentModel = new SingleMethodPaymentModel();
                        singleMethodPaymentModel.setPayment_method(paymentMethodModel);
                        popupTransaction.show();
                        //TODO: fix name
                        popupTransaction.setData(payment_link, false, orderId);
                    } else {
                        showErrorToast((String) object);
                    }
                }
            }, objs, "/payoo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startTransaction(final PaymentMethodModel paymentMethodModel) {

        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        obj.addProperty("phone", ed_phone.getText().toString().trim());
        obj.addProperty("payment_method", paymentMethodModel.getKey());

        if (cb_lixi_point_use.isChecked()) {
            obj.addProperty("user_discount", true);
        }

        if (ConfirmBuyCashEvouherDialog.isReorder) {
            ConfirmBuyCashEvouherDialog.isReorder = false;
            obj.addProperty("order_id", ConfirmBuyCashEvouherDialog.order_id);
            ConfirmBuyCashEvouherDialog.order_id = -1;
        }

        String method = "";

        if (paymentMethodModel.getKey().contains("zalo"))
            method = "/zalo";
        if (paymentMethodModel.getKey().contains("payoo"))
            method = "/payoo";

        DataLoader.buyCashProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = null;
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    Long productId = jsonObject.get("product").getAsLong();
                    DialogTransactions popupTransaction = new DialogTransactions(activity, false, false, false);
                    popupTransaction.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            dismiss();
                        }
                    });
                    popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
                        @Override
                        public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                            dismiss();
                            if (eventListener != null)
                                eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
                        }
                    });
                    popupTransaction.show();
                    //TODO: fix name
                    popupTransaction.setData(payment_link, false, orderId);
                } else {
                    showErrorToast((String) object);
                }
            }
        }, obj, method, "showloading");


    }

    @Override
    protected void onStart() {
        super.onStart();
        activity.registerReceiver(broadcastReceiver, new IntentFilter("reLoadLoadQuantityAllow"));
    }


    private void startPayooBankTransaction() {
        BankSelectionDialog BankSelectionDialog = new BankSelectionDialog(activity, false, false, false);
        BankSelectionDialog.show();
        PaymentMethodModel paymentMethodModel = adapterPaymentMethod.getCurrentPaymentMethod();
        long cash = 0;
        if (cb_lixi_point_use.isChecked()) {
            cash = getPaymentPrice() - lixi_discount;
        } else {
            cash = getPaymentPrice();
        }
        if (paymentMethodModel.getKey().equals("payoo_cc")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getPayoo_card_international(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback());
        } else if (paymentMethodModel.getKey().equals("payoo_bank")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getPayoo_bank(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback(), "bank");
        }
        if (isReorder)
            BankSelectionDialog.setPaymentPrice(orderCheckModel.getDiscount_price());
        BankSelectionDialog.setIDidalogEventListener(new BankSelectionDialog.IDidalogEvent() {
            @Override
            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                dismiss();
                if (eventListener != null)
                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
            }
        });
    }

    private void startVTCBankTransaction() {
        BankSelectionDialog BankSelectionDialog = new BankSelectionDialog(activity, false, false, false);
        BankSelectionDialog.show();
        PaymentMethodModel paymentMethodModel = adapterPaymentMethod.getCurrentPaymentMethod();
        long cash = 0;
        if (cb_lixi_point_use.isChecked()) {
            cash = getPaymentPrice() - lixi_discount;
        } else {
            cash = getPaymentPrice();
        }
        if (paymentMethodModel.getKey().equals("cc")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getCard_international(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback());
        } else if (paymentMethodModel.getKey().equals("bank")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getBank(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback(), "bank");
        }
        if (isReorder)
            BankSelectionDialog.setPaymentPrice(orderCheckModel.getDiscount_price());
        BankSelectionDialog.setIDidalogEventListener(new BankSelectionDialog.IDidalogEvent() {
            @Override
            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                dismiss();
                if (eventListener != null)
                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
            }
        });
    }

    private void startZaloBankTransaction() {

        BankSelectionDialog BankSelectionDialog = new BankSelectionDialog(activity, false, false, false);
        BankSelectionDialog.show();
        PaymentMethodModel paymentMethodModel = adapterPaymentMethod.getCurrentPaymentMethod();
        long cash = 0;
        if (cb_lixi_point_use.isChecked()) {
            cash = getPaymentPrice() - lixi_discount;
        } else {
            cash = getPaymentPrice();
        }
        if (paymentMethodModel.getKey().equals("zalo_pay_cc")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getZalo_card_international(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback());
        } else if (paymentMethodModel.getKey().equals("zalo_pay_atm")) {
            BankSelectionDialog.setData(lixi_discount, id, productModel, sl, userInfo, productModel.getZalo_bank(), cb_lixi_point_use, paymentMethodModel, cash, calculateLixiCashback(), "bank");
        }
        if (isReorder)
            BankSelectionDialog.setPaymentPrice(orderCheckModel.getDiscount_price());
        BankSelectionDialog.setIDidalogEventListener(new BankSelectionDialog.IDidalogEvent() {
            @Override
            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                dismiss();
                if (eventListener != null)
                    eventListener.onPayResult(success, isCancel, isTimeout, orderId, transactionResult);
            }
        });
    }

    private void handleBuyAtStoreCallback(int id, final String order_id) {
        DataLoader.handleBuyAtStoreCallback(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LixiShopBankTransactionResult transactionResult = (LixiShopBankTransactionResult) object;
                    String ms = transactionResult.getMessage().get(0).getText();
                    ms = getMessage(ms, transactionResult.getMessage().get(0).getParams());
                    transactionResult.setOrder_id(order_id);
                    SuccessCashDialog dialog = new SuccessCashDialog(activity, true, true, false);
                    dialog.setCancelable(false);
                    dismiss();
                    dialog.show();
                    dialog.setData(activity.getString(R.string.popup_success_bank_title), ms, "", transactionResult);
                } else {
                    FailedCashDialog popupBuyFailed = new FailedCashDialog(activity, true, true, false);
                    popupBuyFailed.setCancelable(false);
                    dismiss();
                    popupBuyFailed.show();
                    popupBuyFailed.setData(activity.getString(R.string.popup_fail_bank_title), (String) object, "", null);
                }
            }
        }, id);
    }

    private String getMessage(String ms, List<String> par) {
        String res = ms;

        if (par != null) {
            for (int i = 0; i < par.size(); i++) {
                String holder = "{" + String.valueOf(i) + "}";

                if (res.contains(holder)) {
                    res = res.replace(holder, "<b>" + par.get(i) + "</b>");
                }
            }
        }

        return res;
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void changeCheckBoxColor(CheckBox checkBox) {
        if (checkBox == null)
            return;
        if (!checkBox.isChecked()) {
            checkBox.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
        } else if (checkBox.isChecked()) {
            checkBox.setTextColor(activity.getResources().getColor(R.color.my_black));
        }
    }

    public void getLixiDiscount() {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", productModel.getId());
        obj.addProperty("quantity", sl);

        DataLoader.postLixiDiscount(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    DiscountModel discountModel = (DiscountModel) object;
                    long discount = discountModel.getDiscount();
                    lixi_discount = discount;
                    if (discount <= 0) {
                        cb_lixi_point_use.setActivated(false);
                        cb_lixi_point_use.setClickable(false);
                        cb_lixi_point_use.setEnabled(false);
                        cb_lixi_point_use.setText(" " + activity.getString(R.string.popup_confirm_message_transaction));
                        cb_lixi_point_use.setChecked(false);
                        changeCheckBoxColor(cb_lixi_point_use);
//                        tv_lixi_point_use.setText("0 " + activity.getString(R.string.lixi));
                        tv_lixi_point_use.setText(Helper.getVNCurrency(Long.parseLong(userInfo.getAvailable_point())) + " " + activity.getString(R.string.lixi));
                        tv_lixi_point_use.setTextColor(activity.getResources().getColor(R.color.app_violet));
                        tv_cash.setText(Helper.getVNCurrency(getPaymentPrice() - discount) + activity.getString(R.string.p));
                        tv_point.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
                    } else {
                        cb_lixi_point_use.setActivated(true);
                        cb_lixi_point_use.setClickable(true);
                        cb_lixi_point_use.setEnabled(true);

                        tv_lixi_point_use.setText(Helper.getVNCurrency(Long.parseLong(userInfo.getAvailable_point())) + " " + activity.getString(R.string.lixi));
                        if (cb_lixi_point_use.isChecked()) {
                            tv_cash.setText(Helper.getVNCurrency(getPaymentPrice() - discount) + activity.getString(R.string.p));
                            tv_lixi_point_use.setTextColor(activity.getResources().getColor(R.color.app_violet));
                            tv_point.setTextColor(activity.getResources().getColor(R.color.my_black));
                            cb_lixi_point_use.setText(activity.getString(R.string.use) + " " + Helper.getVNCurrency(discount) + " " + activity.getString(R.string.popup_confim_cb_payment_point));
                        } else {
                            tv_cash.setText(Helper.getVNCurrency(getPaymentPrice()) + activity.getString(R.string.p));
                            tv_lixi_point_use.setTextColor(activity.getResources().getColor(R.color.app_violet));
                            tv_point.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
                            cb_lixi_point_use.setText(activity.getString(R.string.popup_confim_cb_can_using) + " " + Helper.getVNCurrency(discount) + " " + activity.getString(R.string.popup_confim_cb_payment_point));
                        }

                    }

                    for (int i = 0; i < discountModel.getInfo().size(); i++) {
                        if (discountModel.getInfo().get(i).getText().contains("Tài khoản cần")) {
                            tv_min_message.setText(discountModel.getInfo().get(i).getText());
                            if (discountModel.getInfo().get(i).isDone()) {
                                iv_tick_min.setImageResource(R.drawable.icon_check_big_green);
                            } else
                                iv_tick_min.setImageResource(R.drawable.icon_check_big_grey);
                        } else {
                            tv_total_message.setText(discountModel.getInfo().get(i).getText());
                            if (discountModel.getInfo().get(i).isDone()) {
                                iv_tick_total.setImageResource(R.drawable.icon_check_big_green);
                            } else
                                iv_tick_total.setImageResource(R.drawable.icon_check_big_grey);
                        }
                    }

                } else {
                    showErrorToast((String) object);
                }
            }
        }, obj);

    }
    //region dialog callback


    public long getPaymentPrice() {
        return totalPrice * sl;
    }

    public interface IDidalogEvent {
        void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    //endregion
}
