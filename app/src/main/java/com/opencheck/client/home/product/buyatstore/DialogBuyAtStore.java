package com.opencheck.client.home.product.buyatstore;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.databinding.BuyAtStoreNewDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class DialogBuyAtStore extends LixiTrackingDialog {
    public DialogBuyAtStore(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private RelativeLayout rl_header;
    private TextView tv_title;
    private ImageView iv_back;
    private RelativeLayout rlMessenger;
    private TextView tv_timeout;
    private LinearLayout ll_wait_code;
    private LinearLayout ll_code;
    private TextView tv_payoo_code;
    private ProgressBar pg_2;
    private LinearLayout ll_payoo_code;
    private TextView tv_payoo_code_2;
    private TextView tv_time;
    private View v_point_1;
    private View v_point_2;
    private View v_point_3;
    private TextView tv_step_1;
    private TextView tv_step_2;
    private TextView tv_step_3;
    private TextView tv_hotline;
    private RelativeLayout rl_content;
    private WebView wv_logo;
    private ProgressBar progressBar;
    private RelativeLayout rl_nearest_store;
    private LinearLayout ll_support;
    private TextView tv_total_title;
    private TextView tv_price;
    private TextView tv_lixi_cashback;
    private LinearLayout ll_buy;
    private TextView tv_buy;

    private ProductCashVoucherDetailModel productModel;
    private int sl = 0;
    private UserModel userInfo;
    private boolean isUseDiscount = false;
    private long numDiscount = 0;
    private String currentUrl = "";
    private String image_url;
    private PaymentMethodModel paymentMethodModel;

    SpannableStringBuilder spanMessage;

    long totalPrice;
    long totalLixi;
    private BuyAtStoreNewDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = BuyAtStoreNewDialogBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        findViews();
        //initDialogEvent();
    }

    private String calculateLixiCashback() {

        return "+ " + Helper.getVNCurrency(totalLixi * sl) + " " + activity.getString(R.string.lixi);
    }

    public String getPaymentPrice() {
        return Helper.getVNCurrency(totalPrice * sl - (isUseDiscount ? numDiscount : 0)) + activity.getString(R.string.p);
    }


    public void setData(ProductCashVoucherDetailModel productModel,
                        PaymentMethodModel paymentMethodModel,
                        int sl,
                        UserModel userInfo,
                        boolean isUseDiscount,
                        long numDiscount,
                        long totalPrice,
                        long totalLixi) {
        this.productModel = productModel;
        this.sl = sl;
        this.userInfo = userInfo;
        this.isUseDiscount = isUseDiscount;
        this.numDiscount = numDiscount;
        this.paymentMethodModel = paymentMethodModel;
        this.totalPrice = totalPrice;
        this.totalLixi = totalLixi;
        setListener();
        setViews();

        ll_code.setVisibility(View.GONE);
        ll_wait_code.setVisibility(View.VISIBLE);
        tv_buy.setEnabled(false);
        ll_buy.setOnClickListener(null);
        tv_buy.setText(LanguageBinding.getString(R.string.cash_evoucher_getting_payment_code, activity));
        tv_payoo_code_2.setText("");
        pg_2.setVisibility(View.VISIBLE);
        ll_payoo_code.setVisibility(View.GONE);

        if (!productModel.getOrder_method().equals("v2"))
            startTransaction();
        else
            startTransactionV2();

        //trackScreen();
    }

    private void setListener() {

        iv_back.setOnClickListener(this);
        rlMessenger.setOnClickListener(this);
        rl_nearest_store.setOnClickListener(this);
        ll_buy.setOnClickListener(this);
        tv_hotline.setOnClickListener(this);
    }

    private void findViews() {
        rl_header = (RelativeLayout) findViewById(R.id.rl_header);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        rlMessenger = (RelativeLayout) findViewById(R.id.rlMessenger);
        tv_timeout = findViewById(R.id.tv_timeout);
        ll_wait_code = (LinearLayout) findViewById(R.id.ll_wait_code);
        ll_code = (LinearLayout) findViewById(R.id.ll_code);
        tv_payoo_code = (TextView) findViewById(R.id.tv_payoo_code);
        ll_payoo_code = findViewById(R.id.ll_payoo_code);
        tv_payoo_code_2 = (TextView) findViewById(R.id.tv_payoo_code_2);
        pg_2 = findViewById(R.id.pg_2);
        tv_time = (TextView) findViewById(R.id.tv_time);
        v_point_1 = (View) findViewById(R.id.v_point_1);
        v_point_2 = (View) findViewById(R.id.v_point_2);
        v_point_3 = (View) findViewById(R.id.v_point_3);
        tv_step_1 = (TextView) findViewById(R.id.tv_step_1);
        tv_step_2 = (TextView) findViewById(R.id.tv_step_2);
        tv_step_3 = (TextView) findViewById(R.id.tv_step_3);
        tv_hotline = (TextView) findViewById(R.id.tv_hotline);
        rl_content = (RelativeLayout) findViewById(R.id.rl_content);
        wv_logo = (WebView) findViewById(R.id.wv_logo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        rl_nearest_store = (RelativeLayout) findViewById(R.id.rl_nearest_store);
        ll_support = (LinearLayout) findViewById(R.id.ll_support);
        tv_total_title = (TextView) findViewById(R.id.tv_total_title);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_lixi_cashback = (TextView) findViewById(R.id.tv_lixi_cashback);
        ll_buy = (LinearLayout) findViewById(R.id.ll_buy);
        tv_buy = (TextView) findViewById(R.id.tv_buy);
    }

    public String getTimeOut() {
        int timeout = ConfigModel.getInstance(activity).getOrder_payment_expired_at_store();
        int h = 0;
        int d = 0;
        int m = 0;
        if (timeout > 1440) {
            d = timeout / 1440;
            h = timeout % 1440;
            m = h % 60;
            h = h / 60;
        } else {
            h = timeout / 60;
            m = timeout % 60;
        }

        String str = "";
        if (d > 0)
            str += " " + d + " " + LanguageBinding.getString(R.string.day, activity);
        if (h > 0)
            str += " " + h + " " + LanguageBinding.getString(R.string.hour, activity);
        if (m > 0)
            str += " " + m + " " + LanguageBinding.getString(R.string.minute, activity);
        return str;
    }

    private void setViews() {
        tv_price.setText("" + getPaymentPrice());
        tv_lixi_cashback.setText("" + calculateLixiCashback());
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
       tv_timeout.setText(Html.fromHtml(String.format(LanguageBinding.getString(R.string.store_time_payment, activity), "<b>" + getTimeOut() + "</b>")));
//        tv_hotline.setText(activity.getResources().getString(R.string.DEFAULT_NUMBER));
        tv_hotline.setText(Helper.getPhoneSupport(activity));

        wv_logo.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        wv_logo.getSettings().setDomStorageEnabled(true);

        wv_logo.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv_logo.loadUrl("https://lixiapp.com/payoo/payoo");
    }

    private String getRootUrl(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(0, j);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }

    private String getUrlParams(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                return url.substring(i, url.length());
            }
        }
        return "";
    }

    private String getUrlProvider(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(j + 1, i);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                dismiss();
                break;
            case R.id.rlMessenger:
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ll_buy:
                dismiss();
//                if (eventListener != null)
//                    eventListener.onOk();
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                break;
            case R.id.rl_nearest_store:
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://payoo.vn/map/public/?verify=true")));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.tv_hotline:
                Helper.callPhoneSupport(activity);
                break;
            default:
                break;
        }
    }

    private void startTransaction() {

        String method = "/payoo";

        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        obj.addProperty("phone", userInfo.getPhone());
        obj.addProperty("payment_method", paymentMethodModel.getKey());

        if (isUseDiscount) {
            obj.addProperty("user_discount", true);
        }

        DataLoader.buyCashProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject records = (JsonObject) object;
                    String billing_code = null;
                    String expired_day = "";
                    billing_code = records.get("billing_code").getAsString();
                    long payment_expired = records.get("payment_expired").getAsLong();
                    String transaction_id = records.get("transaction_id").getAsString();
                    id = records.get("id").getAsLong();
                    trackScreen();

                    expired_day = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDERS, String.valueOf(payment_expired * 1000));

                    DialogPayooCode dialog = new DialogPayooCode(activity, false, true, false);
                    dialog.show();
                    dialog.setData(billing_code, productModel != null ? productModel.getId() + "" : "");

                    ll_code.setVisibility(View.VISIBLE);
                    ll_wait_code.setVisibility(View.GONE);
                    tv_payoo_code.setText(billing_code);
                    tv_time.setText(expired_day);
                    tv_buy.setEnabled(true);
                    ll_buy.setOnClickListener(DialogBuyAtStore.this);
                    tv_buy.setText(LanguageBinding.getString(R.string.completed, activity));
                    tv_payoo_code_2.setText(billing_code);
                    pg_2.setVisibility(View.GONE);
                    ll_payoo_code.setVisibility(View.VISIBLE);
                    Analytics.trackOrderEVoucher(transaction_id);
                } else {
                    showErrorToast((String) object);
                }
                ((ActivityProductCashEvoucherDetail) activity).trackingVoucher(isSuccess);
            }
        }, obj, method);

    }

    private void startTransactionV2() {

        String method = "/payoo";

        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", productModel.getId());
            jsonObject.put("quantity", sl);
            jsonArray.put(jsonObject);

            JSONObject objs = new JSONObject();
            objs.put("phone", userInfo.getPhone());
            objs.put("payment_method", "at_store");
            objs.put("user_discount", isUseDiscount);
            objs.put("list_product", jsonArray);

            DataLoader.buyCashProductV2(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        JsonObject records = (JsonObject) object;
                        String billing_code = null;
                        String expired_day = "";
                        billing_code = records.get("billing_code").getAsString();
                        long payment_expired = records.get("payment_expired").getAsLong();
                        expired_day = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDERS, String.valueOf(payment_expired * 1000));

                        DialogPayooCode dialog = new DialogPayooCode(activity, false, true, false);
                        dialog.show();
                        dialog.setData(billing_code, productModel != null ? productModel.getId() + "" : "");

                        ll_code.setVisibility(View.VISIBLE);
                        ll_wait_code.setVisibility(View.GONE);
                        tv_payoo_code.setText(billing_code);
                        tv_time.setText(expired_day);
                        tv_buy.setEnabled(true);
                        ll_buy.setOnClickListener(DialogBuyAtStore.this);
                        tv_buy.setText(LanguageBinding.getString(R.string.completed, activity)
                        );
                        tv_payoo_code_2.setText(billing_code);
                        pg_2.setVisibility(View.GONE);
                        ll_payoo_code.setVisibility(View.VISIBLE);


                    } else {
                        showErrorToast((String) object);
                    }
                }
            }, objs, method);
        } catch (Exception ex) {
        }
    }

    public interface IDidalogEvent {
        public void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
