package com.opencheck.client.home.delivery.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.opencheck.client.home.delivery.model.DeliAddressModel;

public class DatabaseUserLocation extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "UserDatabase.db";
    private static final String TABLE_NAME = "UserLocation";
    private static final String KEY_TYPE = "Id";
    private static final String KEY_PLACE_ID = "placeId";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_FULL_ADDRESS = "fullAddress";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LNG = "lng";
    private static final String KEY_GPS = "gps";
    private static final String KEY_TIME_STAMP = "timeStamp";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_LOCATION_ID = "loactionId";

    public static final int HOME = 0;
    public static final int OFFICE = 1;

    private Context context;
    @SuppressLint("StaticFieldLeak")
    private static DatabaseUserLocation instance;

    private DatabaseUserLocation(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static synchronized DatabaseUserLocation getInstance(Context context){
        if (instance == null){
            instance = new DatabaseUserLocation(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATION_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + KEY_TYPE + " TEXT PRIMARY KEY, "
                + KEY_PLACE_ID + " TEXT, "
                + KEY_ADDRESS + " TEXT, "
                + KEY_FULL_ADDRESS + " TEXT, "
                + KEY_LAT + " REAL, "
                + KEY_LNG + " REAL, "
                + KEY_GPS + " INTEGER DEFAULT 0, "
                + KEY_TIME_STAMP + " TEXT, "
                + KEY_USER_ID + " INTEGER, "
                + KEY_LOCATION_ID + " INTEGER)";

        sqLiteDatabase.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(sqLiteDatabase);
    }

    public void addLocation(DeliAddressModel address, String type){
        if (address == null){
            if (isExist(type)){
                remove(type);
            }
            return;
        }

        if (isExist(address.getType())){
            remove(address.getType());
        }
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_TYPE, address.getType());
            values.put(KEY_PLACE_ID, address.getPlaceId());
            values.put(KEY_ADDRESS, address.getAddress());
            values.put(KEY_FULL_ADDRESS, address.getFullAddress());
            values.put(KEY_LAT, address.getLat());
            values.put(KEY_LNG, address.getLng());
            values.put(KEY_GPS, address.isGPS());
            values.put(KEY_TIME_STAMP, System.currentTimeMillis() + "");
            values.put(KEY_USER_ID, address.getUser_id());
            values.put(KEY_LOCATION_ID, address.getId());
            // insert
            db.insertOrThrow(TABLE_NAME, null, values);
            db.close();
        } catch (SQLException e) {
            Log.d("TTTT", e.getMessage());
            e.printStackTrace();
        }
    }

    public DeliAddressModel getLocation(String type){
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_TYPE + " = '" + type + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        DeliAddressModel addressModel = null;
        if (cursor.moveToFirst()){
            addressModel = new DeliAddressModel();
            addressModel.setType(cursor.getString(0));
            addressModel.setPlaceId(cursor.getString(1));
            addressModel.setAddress(cursor.getString(2));
            addressModel.setFullAddress(cursor.getString(3));
            addressModel.setLat(Double.parseDouble(cursor.getString(4)));
            addressModel.setLng(Double.parseDouble(cursor.getString(5)));
            addressModel.setGPS((cursor.getInt(cursor.getColumnIndex("gps")) == 1));
            addressModel.setCreate_date(Long.valueOf(cursor.getString(7)));
            addressModel.setUser_id(cursor.getInt(8));
            addressModel.setId(cursor.getInt(9));
        }

        cursor.close();
        return addressModel;
    }

    private boolean isExist(String type){
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_TYPE + " = '" + type + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        boolean isExist = cursor.getCount() > 0;
        cursor.close();
        return isExist;
    }

    public void remove(String type){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_TYPE + " = '" + type + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
}
