package com.opencheck.client.home.notification;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.PagerSlidingTabStrip;
import com.opencheck.client.databinding.NotificationListingActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/6/2018.
 */

public class NotificationListingActivity extends LixiActivity {

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.NOTIFICATION_HOME;
    }

    private RelativeLayout rlBack;
    private TextView tvTitle;
    //private SwipeRefreshLayout swr;
    private RecyclerView rv;
    //private LinearLayout lnNoResult;
    boolean noRemain = false;

    //ArrayList<NotificationModel> listNoti = new ArrayList<>();

    //private NotificationAdapter adapter;

    private int page = 1;
    private Boolean isLoading = false;

    private NotificationListingActivityBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.notification_listing_activity);
        initView();
        initPager();
    }

    private void initView() {
        rlBack = findViewById(R.id.rlBack);
        tvTitle = findViewById(R.id.tvTitle);
        rv = findViewById(R.id.rv);
//        lnNoResult = findViewById(R.id.lnNoResult);
//        swr = findViewById(R.id.swr);
        rlBack.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //readAllNotification();
        tvTitle.setText(getString(R.string.notification_label));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                onBackPressed();
                break;
        }
    }

    private void openVoucherDetail(String id) {
        LixiShopEvoucherModel lixiShopEvoucherModel = new LixiShopEvoucherModel();
        lixiShopEvoucherModel.setId(Long.parseLong(id));
//        activity.startActivity(new Intent(activity, CashEvoucherDetailActivity.class).putExtra(CashEvoucherDetailActivity.PRODUCT_ID, (lixiShopEvoucherModel.getId())));
        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, lixiShopEvoucherModel.getId(), TrackingConstant.ContentSource.LIST_NOTIFICATION);
    }

    public void copyPin(Object object) {
        if (((NotificationModel) object).getType().equals("payment.add.buycard")) {
            String tt = ((NotificationModel) object).getBody();
            //Bạn vừa sử dụng OC Credit cho việc Mua thẻ cào [Viettel] serial: 57706827880, code:6552989031555
            int l = tt.length();
            int st = tt.indexOf("code:");
            if (st >= 0) {
                int e = st + 6;
                while (e < l && tt.charAt(e) >= '0' && tt.charAt(e) <= '9')
                    e++;
                tt = tt.substring(st + 5, e).trim();
                Helper.copyCode(tt, activity);
            } else {
                Toast.makeText(activity, "Tin nhắn không đúng, không thể copy mã pin!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void readAllNotification() {
        DataLoader.readAllNotication(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {

            }
        });
    }

    private PromotionNotificationFragment promotionNotificationFragment;
    private PersonalNotificationFragment personalNotificationFragment;

    private void addTabs(FragmentOrderAdapter fragmentOrderAdapter) {
        promotionNotificationFragment = new PromotionNotificationFragment();
        personalNotificationFragment = new PersonalNotificationFragment();

        fragmentOrderAdapter.addFragment(promotionNotificationFragment, LanguageBinding.getString(R.string.tab_promotion, activity));
        fragmentOrderAdapter.addFragment(personalNotificationFragment, LanguageBinding.getString(R.string.individual, activity));
    }

    public void initPager() {
        FragmentOrderAdapter fragmentOrderAdapter = new FragmentOrderAdapter(getSupportFragmentManager());
        addTabs(fragmentOrderAdapter);
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(fragmentOrderAdapter);
        viewPager.setOffscreenPageLimit(2);

        // Give the PagerSlidingTabStrip the ViewPager
        final PagerSlidingTabStrip tabsStrip = findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setTextColorResource(R.drawable.selector_text_color_lixi);

//        tabsStrip.setTypeface(Typeface.DEFAULT,R.style.AccountKit_InputText);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LinearLayout tab = (LinearLayout) tabsStrip.getChildAt(0);
                for (int i = 0; i < tab.getChildCount(); i++) {
                    TextView tv = (TextView) tab.getChildAt(i);
                    if (i != position) {
                        tv.setTextColor(0xAAAAAAAA);
                    } else {
                        tv.setTextColor(0xffac1ea1);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(1);
        viewPager.setCurrentItem(0);
        tabsStrip.setPadding(0, 0, 0, 0);
    }

    public class FragmentOrderAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;
        List<String> tabTitles = new ArrayList<>();
        List<Fragment> fragments = new ArrayList<>();

        FragmentOrderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            tabTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        promotionNotificationFragment.onActivityResult(requestCode, resultCode, data);
        personalNotificationFragment.onActivityResult(requestCode, resultCode, data);
    }
}
