package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.delivery.model.ImageModel;

import java.util.ArrayList;

public class ImageDetailAdapter extends RecyclerView.Adapter<ImageDetailAdapter.ImageHolder> {

    private LixiActivity activity;
    private ArrayList<ImageModel> arrImg;

    public ImageDetailAdapter(LixiActivity activity, ArrayList<ImageModel> arrImg) {
        this.activity = activity;
        this.arrImg = arrImg;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(activity, R.layout.item_image_layout, null);
        return new ImageHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        ImageLoader.getInstance().displayImage(arrImg.get(position).getImage_link(), holder.iv_food, LixiApplication.getInstance().optionsNomal);
    }

    @Override
    public int getItemCount() {
        return arrImg.size();
    }

    class ImageHolder extends RecyclerView.ViewHolder {
        private ImageView iv_food;

        public ImageHolder(View itemView) {
            super(itemView);
            iv_food = itemView.findViewById(R.id.image_view_image_select);
        }
    }
}
