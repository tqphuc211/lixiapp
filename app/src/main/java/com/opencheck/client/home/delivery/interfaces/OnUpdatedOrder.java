package com.opencheck.client.home.delivery.interfaces;

public interface OnUpdatedOrder {
    void onUpdated();
}
