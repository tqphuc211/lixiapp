package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.FlingBehavior;
import com.opencheck.client.databinding.ActivityTopicBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.ListTopicAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DetailTopicModel;
import com.opencheck.client.home.delivery.model.StoreTopicModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;

import java.util.ArrayList;

public class TopicActivity extends LixiActivity {

    private long topic_id = -1;
    private int height = 0;

    private ListTopicAdapter adapter;

    private SpacesItemDecoration spacesItemDecoration;

    private AppPreferenceHelper appPreferenceHelper;
    private ActivityTopicBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_topic);
        appPreferenceHelper = AppPreferenceHelper.getInstance();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            topic_id = bundle.getLong("TOPIC_ID", -1);
        }

        initView();
        setAdapter();
        getDetailTopic();
        getListTopic();
    }

    private void initView() {
        ViewGroup.LayoutParams layoutParams = mBinding.appbar.getLayoutParams();
        layoutParams.height = 9 * activity.widthScreen / 16;
        mBinding.appbar.setLayoutParams(layoutParams);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mBinding.appbar.getLayoutParams();
        params.setBehavior(new FlingBehavior(getBaseContext(), null));

        mBinding.appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());
                mBinding.ivBanner.setAlpha(1 - (offsetAlpha * -1));

                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    mBinding.tvTitle.setVisibility(View.VISIBLE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 172, 30, 161));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));
                } else {
                    //expands
                    mBinding.tvTitle.setVisibility(View.GONE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 255, 255, 255));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(5);
                }
            }
        });

        mBinding.swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListTopic();
            }
        });
        mBinding.ivBack.setOnClickListener(this);
    }

    private void getListTopic() {
        DeliAddressModel deliAddressModel = appPreferenceHelper.getDefaultLocation();
        DeliDataLoader.getListTopic(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                mBinding.swr.setRefreshing(false);
                if (isSuccess) {
                    ArrayList<StoreTopicModel> listTopic = (ArrayList<StoreTopicModel>) object;
                    adapter.setData(listTopic);
                }
            }
        }, topic_id, deliAddressModel.getAddress(), deliAddressModel.getLat(), deliAddressModel.getLng());
    }

    private void getDetailTopic() {
        DeliDataLoader.getDetailTopic(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    DetailTopicModel detailTopicModel = (DetailTopicModel) object;
                    mBinding.tvTitle.setText(detailTopicModel.getName());
                    LixiImage.with(activity)
                            .load(detailTopicModel.getCover_link())
                            .size(ImageSize.MEDIUM)
                            .into(mBinding.ivBanner);
                }
            }
        }, topic_id);
    }

    private void setAdapter() {
        setSupportActionBar(mBinding.toolbar);
        int space = (int) getResources().getDimension(R.dimen.value_8);
        spacesItemDecoration = new SpacesItemDecoration(2, space, true);
        adapter = new ListTopicAdapter(this, null);
        mBinding.rv.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.rv.setAdapter(adapter);
        mBinding.rv.addItemDecoration(spacesItemDecoration);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
