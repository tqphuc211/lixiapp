package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class LogModel implements Serializable {
    private long date;
    private String state;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
