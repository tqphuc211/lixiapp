package com.opencheck.client.home.delivery.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentDeleteOrderBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.delivery.adapter.ReasonCancelAdapter;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.delivery.interfaces.OnUpdatedOrder;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class DeleteOrderFragment extends Fragment implements View.OnClickListener {

    public static final String REASONS = "reasons";
    public static final String DELI_ORDER = "deli_order";
    private FragmentDeleteOrderBinding mBinding;
    private List<String> listReasons;
    private DeliOrderModel orderModel;
    private ReasonCancelAdapter adapter;

    private LixiActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_delete_order, container, false);
        activity = (LixiActivity) getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            listReasons = (new AppPreferenceHelper(getContext())).convertStrings(bundle.getString(REASONS));
            orderModel = (DeliOrderModel) bundle.getSerializable(DELI_ORDER);
        }

        initEvent();
        initAdapter();
        return mBinding.getRoot();
    }

    private void initEvent() {
        mBinding.relParent.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.txtCancel.setOnClickListener(this);
    }

    private void initAdapter() {
        adapter = new ReasonCancelAdapter(activity, listReasons);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        mBinding.recReasons.setLayoutManager(linearLayoutManager);
        mBinding.recReasons.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                if (pos == adapter.getItemCount() - 1) {
                    mBinding.edtInput.setEnabled(true);
                    mBinding.edtInput.requestFocus();
                    Helper.showSoftKeyBoard(activity);
                } else {
                    mBinding.edtInput.setEnabled(false);
                    mBinding.edtInput.getText().clear();
                    activity.hideKeyboard();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                getFragmentManager().beginTransaction().detach(this).commit();
                break;
            case R.id.txtCancel:
                cancelOrder();
                break;
        }
    }

    private void cancelOrder() {
        if (!adapter.reasonSelect.equals("")) {
            if (adapter.getReasonIndex() == adapter.getItemCount() - 1) {
                String temp = mBinding.edtInput.getText().toString().trim();
                if (temp.equals("")) {
                    Toast.makeText(activity, LanguageBinding.getString(R.string.deli_support_required_input, activity), Toast.LENGTH_SHORT).show();
                } else {
                    postCancelOrder(temp);
                }
            } else {
                postCancelOrder(adapter.reasonSelect);
            }
        } else {
            Toast.makeText(activity, LanguageBinding.getString(R.string.deli_support_required_reason, activity), Toast.LENGTH_SHORT).show();
        }
    }

    private void postCancelOrder(String reason) {
        if (orderModel != null) {
            JSONObject params = new JSONObject();
            try {
                // data
                JSONObject data = new JSONObject();
                data.put("close_reason_message", reason);

                params.put("user_order_uuid", orderModel.getUuid());
                params.put("type", "cancel_order");
                params.put("data", data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            DeliDataLoader.userUpdateOrder(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        Helper.showSuccessDialog(activity, LanguageBinding.getString(R.string.deli_support_sent_request, activity));
                        if (onUpdatedOrder != null) {
                            onUpdatedOrder.onUpdated();
                        }
                        getFragmentManager().beginTransaction().detach(DeleteOrderFragment.this).commit();
                    } else {
                        showDialog(LanguageBinding.getString(R.string.deli_support_cancel_order, activity));
                    }
                }
            }, params);
        }
    }

    private OnUpdatedOrder onUpdatedOrder;

    public void setOnUpdatedOrderListener(OnUpdatedOrder onUpdatedOrder) {
        this.onUpdatedOrder = onUpdatedOrder;
    }

    private void showDialog(String title) {
        ManualDismissDialog dialog = new ManualDismissDialog(activity, false, true, false);
        dialog.show();
        dialog.setTitle(title);
        dialog.setMessage("Đơn hàng này đã được xác nhận hoặc đang giao hàng nên không thể hủy. Vui lòng liên hệ hotline " + ConfigModel.getInstance(activity).getDefaultSupportPhone() + " để được hỗ trợ");
        dialog.setTitleButton("GỌI");
        dialog.setIDidalogEventListener(new ManualDismissDialog.IDialogEvent() {
            @Override
            public void onOk() {
                Helper.callPhoneSupport(activity);
            }
        });
    }
}
