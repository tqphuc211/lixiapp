package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.ConstantValue;

public class DetailTopicModel extends LixiModel {

    /**
     * id : 1
     * state : active
     * name : test
     * priority : 2
     * refresh_time : 5
     * max_data_count : 10
     * parameter : {"numb_day":103}
     * next_sync_time : 1548053433877
     * province_id : 55
     * image_link : abc.com
     * cover_link : ac.com
     */

    private int id;
    private String state;
    private String name;
    private int priority;
    private int refresh_time;
    private int max_data_count;
    private Parameter parameter;
    private long next_sync_time;
    private int province_id;
    private String image_link;
    private String cover_link;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getRefresh_time() {
        return refresh_time;
    }

    public void setRefresh_time(int refresh_time) {
        this.refresh_time = refresh_time;
    }

    public int getMax_data_count() {
        return max_data_count;
    }

    public void setMax_data_count(int max_data_count) {
        this.max_data_count = max_data_count;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public long getNext_sync_time() {
        return next_sync_time;
    }

    public void setNext_sync_time(long next_sync_time) {
        this.next_sync_time = next_sync_time;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public String getImage_link() {
        if (image_link == null || image_link.equals("")) {
            return ConstantValue.DEFAULT_IMAGE[0];
        }
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getCover_link() {
        if (cover_link == null || cover_link.equals("")) {
            return getImage_link();
        }
        return cover_link;
    }

    public void setCover_link(String cover_link) {
        this.cover_link = cover_link;
    }

    public static class Parameter {
        /**
         * numb_day : 103
         */

        private int numb_day;

        public int getNumb_day() {
            return numb_day;
        }

        public void setNumb_day(int numb_day) {
            this.numb_day = numb_day;
        }
    }
}
