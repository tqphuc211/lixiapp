package com.opencheck.client.home.newlogin.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogUpdateSuccessBinding;
import com.opencheck.client.utils.LanguageBinding;

public class UpdateSuccessDialog extends LixiDialog {
    private String phone;

    public UpdateSuccessDialog(@NonNull LixiActivity _activity, String phone) {
        super(_activity, false, true, false);
        this.phone = phone;

    }

    private DialogUpdateSuccessBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogUpdateSuccessBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        String htmlPhone = "<b><font color=\"black\">" + phone + "</font></b>";
        String content = String.format(LanguageBinding.getString(R.string.login_update_content, activity), htmlPhone);
        mBinding.txtPhone.setText(Html.fromHtml(content));

        mBinding.txtYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtYes:
                cancel();
                break;
        }

    }
}
