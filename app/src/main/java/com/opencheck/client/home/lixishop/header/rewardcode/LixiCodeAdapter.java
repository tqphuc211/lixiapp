package com.opencheck.client.home.lixishop.header.rewardcode;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RewardCodeItemBinding;
import com.opencheck.client.databinding.RewardCodeItemHeaderBinding;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.LixiCodeModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class LixiCodeAdapter extends RecyclerView.Adapter {

    LixiActivity activity;
    ArrayList<LixiCodeModel> listCode;

    LixiCodeHeaderController headerController;

    public LixiCodeAdapter(LixiActivity activity, ArrayList<LixiCodeModel> listCode) {
        this.activity = activity;
        this.listCode = listCode;
    }

    ArrayList<LixiCodeBarnerModel> barner;

    public void setHeaderData(ArrayList<LixiCodeBarnerModel> barner) {
        this.barner = barner;
        if (headerController != null && barner.size() > 0) {
            headerController.setData(barner.get(0));
        }
        if (dialogDetail != null && dialogDetail.isShowing() && barner.size() > 0)
            dialogDetail.setbaner(barner.get(0));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            boolean isEmpty;
            isEmpty = listCode.size() == 1;
            RewardCodeItemHeaderBinding rewardCodeItemHeaderBinding =
                    RewardCodeItemHeaderBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            headerController = new LixiCodeHeaderController(activity,
                    rewardCodeItemHeaderBinding.getRoot(), isEmpty);
            return new HolderHeader(rewardCodeItemHeaderBinding.getRoot());
        }
        if (viewType == TYPE_ITEM) {
            RewardCodeItemBinding rewardCodeItemBinding =
                    RewardCodeItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            return new HolderItemCode(rewardCodeItemBinding.getRoot());
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_HEADER) {
            HolderHeader viewHolder = (HolderHeader) holder;
            if (barner != null && barner.size() > 0)
                headerController.setData(barner.get(0));
        }
        if (holder.getItemViewType() == TYPE_ITEM) {
            HolderItemCode viewHolder = (HolderItemCode) holder;
            LixiCodeModel dto = listCode.get(position);

            viewHolder.rootItemView.setTag(position);

            if (position == 1)
                viewHolder.v_line.setVisibility(View.GONE);
            else
                viewHolder.v_line.setVisibility(View.VISIBLE);

            viewHolder.tv_code.setText(LanguageBinding.getString(R.string.lixi_code, activity) + ": " + dto.getCode());
            viewHolder.tv_description.setText(getSpanable(dto.getDesc(), dto.getDesc_data()));
            viewHolder.tv_date.setText(Helper.getFormatDateISO("dd/MM/yyyy", dto.getStart_date()) + "-" +
                    Helper.getFormatDateISO("dd/MM/yyyy", dto.getEnd_date()));
            viewHolder.iv_logo.setImageBitmap(null);
            if (dto.getList_banner().size() > 0)
                ImageLoader.getInstance().displayImage(dto.getList_banner().get(0).getImage(),
                        viewHolder.iv_logo, LixiApplication.getInstance().optionsNomal);
            else
                ImageLoader.getInstance().displayImage("", viewHolder.iv_logo,
                        LixiApplication.getInstance().optionsNomal);
        }
    }

    public class HolderHeader extends RecyclerView.ViewHolder {
        public HolderHeader(View itemView) {
            super(itemView);
        }
    }

    LixiCodeDetailDialog dialogDetail;

    public class HolderItemCode extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        private View v_line;
        private ImageView iv_logo;
        private TextView tv_code;
        private TextView tv_description;
        private TextView tv_date;

        public HolderItemCode(View itemView) {
            super(itemView);
            rootItemView = itemView;
            v_line = itemView.findViewById(R.id.v_line);
            iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tv_code = (TextView) itemView.findViewById(R.id.tv_code);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            rootItemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView) {
                dialogDetail = new LixiCodeDetailDialog(activity, false, false, false);
                dialogDetail.show();
                dialogDetail.setData(listCode.get((int) v.getTag()).getId());
                if (barner != null && barner.size() > 0)
                    dialogDetail.setbaner(barner.get(0));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listCode == null)
            return 0;
        return listCode.size();
    }

    final int TYPE_HEADER = 0;
    final int TYPE_ITEM = 1;

    @Override
    public int getItemViewType(int position) {
        return Math.min(position, 1);
    }

    public SpannableStringBuilder getSpanable(String mess, ArrayList<String> data) {
        SpannableStringBuilder messageBuilder = new SpannableStringBuilder();
        if (data != null) {
            int st = 0;
            for (int i = 0; i < data.size(); i++) {
                int pos = mess.indexOf("{" + i + "}");
                if (pos > 0) {
                    messageBuilder.append(mess.substring(st, pos));
                    st = pos + 2 + (i > 9 ? 2 : 1);
                    SpannableString redSpannable = new SpannableString(data.get(i));
                    redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.app_violet)), 0, data.get(i).length(), 0);
                    redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, data.get(i).length(), 0);
                    messageBuilder.append(redSpannable);
                }
            }
            if (st < mess.length() - 1)
                messageBuilder.append(mess.substring(st, mess.length()));
//                                            mess.replace("{" + i + "}", data.getString(i));
        } else
            messageBuilder.append(mess);

        return messageBuilder;

    }
}