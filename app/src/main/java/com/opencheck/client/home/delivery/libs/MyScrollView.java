package com.opencheck.client.home.delivery.libs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.util.Log;

public class MyScrollView extends NestedScrollView {

    private OnMyScrollChangeListener myScrollChangeListener;

    public MyScrollView(@NonNull Context context) {
        super(context);
    }

    public MyScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        Log.d("scrollHeight", t + "-" + oldt);
        if (myScrollChangeListener != null) {
            if (t >= oldt) {
                myScrollChangeListener.onScrollDown();
            } else {
                myScrollChangeListener.onScrollUp();
            }
        }
    }

    public void setMyScrollChangeListener(OnMyScrollChangeListener onMyScrollChangeListener) {
        this.myScrollChangeListener = onMyScrollChangeListener;
    }

    public interface OnMyScrollChangeListener {
        void onScrollUp();

        void onScrollDown();
    }
}
