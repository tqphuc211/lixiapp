package com.opencheck.client.home.account.new_layout.control;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

public class OperateObject {
    private static OperateObject INSTANCE;
    private Gson gson;

    public static OperateObject getInstance(){
        if (INSTANCE == null){
            INSTANCE = new OperateObject();
        }
        return INSTANCE;
    }

    public OperateObject() {
        gson = new Gson();
    }

    /**
     * Parse from E to T
     * @param data
     * @param type
     * @param <T>
     * @param <E>
     * @return
     */
    public <T, E> T parseData(E data, Class<T> type){
        String temp = gson.toJson(data);
        return gson.fromJson(temp, type);
    }

    public <T, E> ArrayList<T> sort(ArrayList<T> list, E condition){
        ArrayList<T> listResult = new ArrayList<>();

        for (int i = 0; i < list.size(); i++){
            if (list.get(i).equals(condition)){
                listResult.add(list.get(i));
            }
        }

        return listResult;
    }
}
