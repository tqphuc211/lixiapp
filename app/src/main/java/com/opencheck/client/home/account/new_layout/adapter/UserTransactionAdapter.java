package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowOrderLayoutBinding;
import com.opencheck.client.home.account.new_layout.dialog.DetailTransactionDialog;
import com.opencheck.client.home.account.new_layout.model.UserTransaction;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class UserTransactionAdapter extends RecyclerView.Adapter<UserTransactionAdapter.TransactionHolder> {
    private int COLOR_COMPLETE, COLOR_FAIL, COLOR_WATING;
    private final String STATUS_COMPLETE = "complete";
    private final String STATUS_FAIL = "payment_expired";
    private final String STATUS_CANCEL = "user_cancel";
    private final String STATUS_WAITING = "waiting";

    private LixiActivity activity;
    private ArrayList<UserTransaction> listOrder;

    public UserTransactionAdapter(LixiActivity activity, ArrayList<UserTransaction> listOrder) {
        this.activity = activity;
        this.listOrder = listOrder;

        COLOR_COMPLETE = activity.getResources().getColor(R.color.colorSuccess);
        COLOR_FAIL = activity.getResources().getColor(R.color.colorFail);
        COLOR_WATING = activity.getResources().getColor(R.color.colorWaiting);
    }

    @Override
    public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowOrderLayoutBinding rowOrderLayoutBinding =
                RowOrderLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);

        return new TransactionHolder(rowOrderLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(TransactionHolder holder, int position) {
        UserTransaction userTransaction = listOrder.get(position);
        if (userTransaction != null) {
            holder.txtCode.setText("#" + userTransaction.getOrder_uuid());
            long timeStamp = userTransaction.getDate_order() * 1000;
            holder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy HH:mm:ss", String.valueOf(timeStamp)));

            if (userTransaction.getStatus().equals(STATUS_COMPLETE)) {
                holder.txtStatus.setText(activity.getString(R.string.voucher_transaction_success));
                holder.txtStatus.setTextColor(COLOR_COMPLETE);
            } else if (userTransaction.getStatus().equals(STATUS_FAIL) || userTransaction.getStatus().equals(STATUS_CANCEL)) {
                holder.txtStatus.setText(activity.getString(R.string.voucher_transaction_fail));
                holder.txtStatus.setTextColor(COLOR_FAIL);
            } else {
                holder.txtStatus.setText(activity.getString(R.string.waiting_for_payment));
                holder.txtStatus.setTextColor(COLOR_WATING);
            }

            holder.txtVoucherName.setText(userTransaction.getProduct_info().getName());
            holder.txtPrice.setText(Helper.getVNCurrency(userTransaction.getProduct_info().getPrice()) + activity.getString(R.string.p));
            holder.txtQuant.setText("x" + userTransaction.getProduct_info().getQuantity());
            holder.txtTotalPrice.setText(Helper.getVNCurrency(userTransaction.getTotal_price()) + activity.getString(R.string.p));

            if (userTransaction.getTotal_cashback() == 0) {
                holder.txtLixiTitle.setVisibility(View.GONE);
                holder.txtLixi.setVisibility(View.GONE);
            } else {
                if (userTransaction.getStatus().equals(STATUS_FAIL) || userTransaction.getStatus().equals(STATUS_CANCEL)) {
                    holder.txtLixiTitle.setVisibility(View.GONE);
                    holder.txtLixi.setVisibility(View.GONE);
                } else {
                    holder.txtLixiTitle.setVisibility(View.VISIBLE);
                    holder.txtLixi.setVisibility(View.VISIBLE);
                    holder.txtLixi.setText("+" + Helper.getVNCurrency(userTransaction.getTotal_cashback()) + activity.getString(R.string.p));
                }
            }

            holder.txtPaymentMethod.setText(userTransaction.getPayment_method());
        }
    }

    @Override
    public int getItemCount() {
        if (listOrder == null) {
            return 0;
        }
        return listOrder.size();
    }

    public class TransactionHolder extends RecyclerView.ViewHolder {
        TextView txtCode, txtDate, txtStatus, txtVoucherName, txtPrice, txtQuant;
        TextView txtTotalPrice, txtLixi, txtLixiTitle, txtPaymentMethod;

        public TransactionHolder(View itemView) {
            super(itemView);
            txtCode = (TextView) itemView.findViewById(R.id.txtCode);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtVoucherName = (TextView) itemView.findViewById(R.id.txtVoucherName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtQuant = (TextView) itemView.findViewById(R.id.txtQuant);
            txtTotalPrice = (TextView) itemView.findViewById(R.id.txtTotalPrice);
            txtLixi = (TextView) itemView.findViewById(R.id.txtLixi);
            txtLixiTitle = (TextView) itemView.findViewById(R.id.txtLixiTitle);
            txtPaymentMethod = (TextView) itemView.findViewById(R.id.txtPaymentMethod);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // xem chi tiết
                    String index = "";
                    if (listOrder.get(getAdapterPosition()).getProduct_info().getName().matches("V2 - .*")) {
                        index = "oc_v1_order";
                    } else {
                        index = "oc.shop.order";
                    }
                    DetailTransactionDialog dialog = new DetailTransactionDialog(activity, false, false, false);
                    dialog.show();
                    dialog.setData(listOrder.get(getAdapterPosition()).getId(), index);
                }
            });
        }
    }
}
