package com.opencheck.client.home.account.activecode2.fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SearchStoreFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.activecode2.adapter.StoreByAdapter;
import com.opencheck.client.home.account.activecode2.dialog.NoApplyDialog;
import com.opencheck.client.home.merchant.map.StoreMapActivity;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchStoreFragment extends LixiFragment implements SwipeRefreshLayout.OnRefreshListener{

    private RelativeLayout rlBack, rlSearch;
    private RecyclerView rv_store;
    private SwipeRefreshLayout srf_refresh;
    private ImageView iv_scan;
    private EditText edt_search;
    private LinearLayout ll_no_result;
    private int page = 1;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private boolean isLoading = false;
    private ArrayList<NearestStoreModel> list_store = new ArrayList<>();
    private StoreByAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private boolean isOutOfPage = false;

    public SearchStoreFragment() {
        // Required empty public constructor
    }

    private SearchStoreFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        mBinding = SearchStoreFragmentBinding.inflate(inflater, container, false);
        this.initView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initView(View v){
        rlBack = v.findViewById(R.id.rlBack);
        rlSearch = v.findViewById(R.id.rl_search);
        rv_store = v.findViewById(R.id.rv_store);
        srf_refresh = v.findViewById(R.id.srf_refresh);
        ll_no_result = v.findViewById(R.id.ll_no_result);
        iv_scan = v.findViewById(R.id.iv_scan);
        edt_search = v.findViewById(R.id.edt_search);

        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rlBack.setOnClickListener(this);
        rlSearch.setOnClickListener(this);
        srf_refresh.setOnRefreshListener(this);
        iv_scan.setOnClickListener(this);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                page = 1;
                isOutOfPage = false;
                isLoading = false;
                startLoadData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rv_store.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(recyclerView.computeVerticalScrollOffset() > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                        startLoadData(edt_search.getText().toString());
                        isLoading = true;
                    }
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoadData("");
            }
        }, 300);
    }

    private void setAdapter(){
        adapter = new StoreByAdapter(activity, list_store);
        rv_store.setLayoutManager(mLayoutManager);
        rv_store.setAdapter(adapter);
        adapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if(action.equalsIgnoreCase(ConstantValue.ON_MAP_CLICK)){
                    ArrayList<StoreModel> arrTemp = new ArrayList<StoreModel>();
                    for (int i = 0; i < list_store.size(); i++) {
                        StoreModel storeModel = list_store.get(i).convertToStoreModel();
                        arrTemp.add(storeModel);
                    }
                    StoreModel tempStore = arrTemp.get(pos);
                    arrTemp.remove(arrTemp.get(pos));
                    arrTemp.add(0, tempStore);

                    MerchantModel merchantModel = new MerchantModel();
                    Bundle bundle = new Bundle();
                    merchantModel.setListStore(arrTemp);
                    merchantModel.setId(String.valueOf(list_store.get(pos).getMerchant_info().getId()));
                    Intent intent = new Intent(activity, StoreMapActivity.class);
                    bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
                else if(action.equalsIgnoreCase(ConstantValue.ON_ITEM_STORE_CLICK)){
                    NearestStoreModel model = (NearestStoreModel) object;
                    if(model.isAllow_user_send_request_active()) {
                        Bundle bundle = new Bundle();
                        bundle.putString("store", new Gson().toJson(list_store.get(pos)));
                        UsedVoucherFragment fragment = new UsedVoucherFragment();
                        fragment.setArguments(bundle);
                        ((ActiveCode2Activity) activity).replaceFragment(fragment);
                    }
                    else{
                        NoApplyDialog dialog = new NoApplyDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData((NearestStoreModel) object);
                    }
                }
            }
        });
    }

    private void startLoadData(String key){

        if(!isLoading) {
            if (!isOutOfPage) {
                srf_refresh.setRefreshing(false);
                DataLoader.getStoreNearby(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            if (page == 1) {
                                list_store = (ArrayList<NearestStoreModel>) object;
                                if (list_store != null && list_store.size() > 0) {
                                    setAdapter();
                                    setOutOfPage(list_store);
                                }
                            } else {
                                ArrayList<NearestStoreModel> list = (ArrayList<NearestStoreModel>) object;
                                if (list != null && list.size() > 0) {
                                    setOutOfPage(list);
                                    list_store.addAll(list);
                                }
                            }

                            if (list_store.size() > 0 && list_store != null) {
                                rv_store.setVisibility(View.VISIBLE);
                                ll_no_result.setVisibility(View.GONE);
                            } else {
                                rv_store.setVisibility(View.GONE);
                                list_store.clear();
                                ll_no_result.setVisibility(View.VISIBLE);
                            }
                            adapter.notifyDataSetChanged();
                            isLoading = false;
                            page++;
                        } else {
                            isLoading = true;
                        }
                    }
                }, page, key);
            }
        }
    }

    private void setOutOfPage(ArrayList<NearestStoreModel> arrayList){
        if(arrayList.size() < RECORD_PER_PAGE){
            isOutOfPage = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActiveCode2Activity) activity).toolbar.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);

    }

    @Override
    public void onClick(View view) {
        if(view.getId()== R.id.rl_search) {
            edt_search.requestFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edt_search, InputMethodManager.SHOW_FORCED);
        }
        else if(view == rlBack){
            activity.onBackPressed();
        }else if(view == iv_scan){
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_QR);
                }
            } else  {
                ScannerQRFragment fragment = new ScannerQRFragment();
                ((ActiveCode2Activity) activity).replaceFragment(fragment);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        isLoading = false;
        page = 1;
        isOutOfPage = false;
        list_store.clear();
        adapter.notifyDataSetChanged();
        startLoadData(edt_search.getText().toString());
    }
}
