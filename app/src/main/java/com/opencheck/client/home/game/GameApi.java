package com.opencheck.client.home.game;

import com.opencheck.client.models.game.GameModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GameApi {
    @GET("/game/add/sharefb")
    Call<GameModel> addShareFacebook();
}
