package com.opencheck.client.home.rating.model;

import com.opencheck.client.models.LixiModel;

import java.util.ArrayList;
import java.util.List;

public class MarkUsedResponse extends LixiModel{


    /**
     * evoucher : [{"id":0,"pin":"string","product":0,"serial":"string"}]
     * state : string
     */

    private String state;
    private ArrayList<EvoucherDetailModel> evoucher;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<EvoucherDetailModel> getEvoucher() {
        return evoucher;
    }

    public void setEvoucher(ArrayList<EvoucherDetailModel> evoucher) {
        this.evoucher = evoucher;
    }

    public static class EvoucherDetailModel {
    }
}
