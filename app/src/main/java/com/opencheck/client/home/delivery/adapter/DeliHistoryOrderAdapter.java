package com.opencheck.client.home.delivery.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.PinnedSectionListView;
import com.opencheck.client.custom.SelectableRoundedImageView;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.models.user.OrderModel;
import com.opencheck.client.models.user.PurchaseModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DeliHistoryOrderAdapter extends ArrayAdapter<DeliOrderModel> implements PinnedSectionListView.PinnedSectionListAdapter {
    public static String ITEM_CLICK_ACTION = "ITEM_CLICK_ACTION";

    private Activity activity;
    private int layoutItem;
    private int posSelection = 1;
    private ItemClickListener itemClickListener;
    private final String ONLY_DAY = "dd-MM-yyyy";
    private final String DAY_TIME = "dd-MM-yyyy HH:mm:ss";

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getTypeItem();
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == com.opencheck.client.models.user.OrderModel.SECTION;
    }

    private class ViewHolder {
        private RelativeLayout rlItem;
        private SelectableRoundedImageView imgItem;
        private TextView tvName;
        private TextView tvTime;
        private TextView tvPrice;
        private RelativeLayout rlLine;
        private TextView tvHeader;
        private TextView tvState;
        private LinearLayout lnImage;
    }

    public DeliHistoryOrderAdapter(Activity _activity, int layoutItem, ItemClickListener itemClickListener) {
        super(_activity, layoutItem);
        this.activity = _activity;
        this.layoutItem = layoutItem;
        this.itemClickListener = itemClickListener;
    }

    public void clearListItems() {
        clear();
        notifyDataSetChanged();
    }

    public void addListItems(List<DeliOrderModel> popups) {

        if (popups.size() > 0) {
            if (getCount() == 0) {
                DeliOrderModel orders = new DeliOrderModel();
                orders.setTypeItem(PurchaseModel.SECTION);
                orders.setCreate_date(popups.get(0).getCreate_date());
                add(orders);
            }

            add(popups.get(0));
            for (int i = 1; i < popups.size(); i++) {
                Date time = new Date();
                Date timeP = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy hh:mm:ss");
                try {
                    time = dateFormat.parse(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER, String.valueOf(popups.get(i).getCreate_date())));
                    timeP = dateFormat.parse(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER, String.valueOf(popups.get(i - 1).getCreate_date())));
                } catch (ParseException e) {
                }

                if (!Helper.getDifferentDay(time, timeP)) {
                    DeliOrderModel orders = new DeliOrderModel();
                    orders.setTypeItem(OrderModel.SECTION);
                    orders.setCreate_date(popups.get(i).getCreate_date());
                    orders.setCreate_date(popups.get(i).getCreate_date());
                    add(orders);
                }
                popups.get(i).setTypeItem(OrderModel.ITEM);
                add(popups.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public ArrayList<DeliOrderModel> getListItems() {
        ArrayList<DeliOrderModel> listItems = new ArrayList<DeliOrderModel>();
        for (int i = 0; i < getCount(); i++) {
            listItems.add(getItem(i));
        }
        return listItems;
    }

    public void setPosSelection(int posSelection) {
        this.posSelection = posSelection;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(layoutItem, null);
            holder.rlItem = convertView.findViewById(R.id.rlItem);
            holder.imgItem = convertView.findViewById(R.id.imgItem);
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvTime = convertView.findViewById(R.id.tvTime);
            holder.tvPrice = convertView.findViewById(R.id.tvPrice);
            holder.rlLine = convertView.findViewById(R.id.rlLine);
            holder.tvHeader = convertView.findViewById(R.id.tvHeader);
            holder.tvState = convertView.findViewById(R.id.tvState);
            holder.lnImage = convertView.findViewById(R.id.lnImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final DeliOrderModel item = getItem(position);

        if (item.getTypeItem() == PurchaseModel.ITEM) {
            // item
            holder.tvHeader.setVisibility(View.GONE);
            holder.rlItem.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(item.getStore_logo_link(), holder.imgItem, LixiApplication.getInstance().optionsNomal);

            holder.tvName.setText(item.getStore_name());
            holder.tvState.setText(item.getStateString(activity));
            holder.tvState.setTextColor(item.getStateColor());
            holder.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER, String.valueOf(item.getCreate_date())));
            holder.tvPrice.setText(Helper.getVNCurrency(item.getMoney()) + LanguageBinding.getString(R.string.p, activity));
            holder.tvPrice.setTypeface(holder.tvPrice.getTypeface(), Typeface.BOLD);
        } else {
            // header
            holder.rlItem.setVisibility(View.GONE);
            holder.tvHeader.setVisibility(View.VISIBLE);
            String time = Helper.getFormatDateVN(ONLY_DAY, item.getCreate_date());
            String currentTime = Helper.getFormatDateVN(ONLY_DAY, System.currentTimeMillis());
            if (time.equals(currentTime)) {
                holder.tvHeader.setText(LanguageBinding.getString(R.string.today, activity));
            } else {
                holder.tvHeader.setText(String.format(LanguageBinding.getString(R.string.trans_day, activity), Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER, String.valueOf(item.getCreate_date())).substring(0, 10)));
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getTypeItem() == OrderModel.ITEM) {
                    itemClickListener.onItemCLick(position, ITEM_CLICK_ACTION, item, 0L);
                }
            }
        });

        return convertView;
    }
}

