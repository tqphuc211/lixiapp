package com.opencheck.client.home.delivery.adapter;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.AutocompletePredictionBufferResponse;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.PlacesOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LocationItemLayoutBinding;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> implements Filterable {

    private LixiActivity activity;
    private ItemClickListener itemClickListener;
    private GoogleApiClient mGoogleApiClient;
    private GeoDataClient mGeoDataClient;
    private LatLngBounds mBounds; // 105.742, 20.945, 105.911, 21.098
    private AutocompleteFilter mPlaceFilter;
    private LinearLayout lnNoResult;
    private ArrayList<DeliAddressModel> list = new ArrayList<>();
    private ArrayList<DeliAddressModel> listAvailableAddress = new ArrayList<>();
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);

    public LocationAdapter(LixiActivity activity, GoogleApiClient googleApiClient,
                           LatLngBounds bounds, AutocompleteFilter filter, LinearLayout lnNoResult) {
        this.activity = activity;
        this.mGoogleApiClient = googleApiClient;
        this.mBounds = bounds;
        this.mPlaceFilter = filter;
        this.lnNoResult = lnNoResult;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LocationItemLayoutBinding locationItemLayoutBinding =
                DataBindingUtil.inflate(LayoutInflater
                        .from(parent.getContext()), R.layout.location_item_layout, parent, false);
        return new ViewHolder(locationItemLayoutBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DeliAddressModel deliAddressModel = list.get(position);
        holder.binding.tvName.setText(deliAddressModel.getAddress());
        holder.binding.tvAddress.setText(deliAddressModel.getFullAddress());

        if (deliAddressModel.getType() == null) {
            holder.binding.iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_new_location));
        } else if (deliAddressModel.getType().equals("default")) { //default location
            holder.binding.iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_current_location));
        } else if (deliAddressModel.getType().equals("gps")) {//gps location
            holder.binding.iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_deli_send_located));
        } else if (deliAddressModel.getType().equals("history")) { //history location
            holder.binding.iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_location_history));
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void addItem(DeliAddressModel address) {
        if (address == null || list == null || listAvailableAddress == null) {
            return;
        }
        list.add(address);
        listAvailableAddress.add(address);
        notifyDataSetChanged();
    }

    public void addItemList(List<DeliAddressModel> listItem) {
        if (list == null || listItem == null || listItem.size() == 0)
            return;
        list.addAll(listItem);
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null) list.clear();
        if (listAvailableAddress != null) listAvailableAddress.clear();
    }

    public ArrayList<DeliAddressModel> getList() {
        return list;
    }

    private ArrayList<DeliAddressModel> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.d("mapi", "Starting autocomplete query for: " + constraint);
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);

            final AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                Toast.makeText(activity, "Error contacting API: " + status.toString(),
                        Toast.LENGTH_SHORT).show();
                Log.d("mapi", "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.d("mapi", "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());

            while (iterator.hasNext()) {
                final AutocompletePrediction prediction = iterator.next();
                // Get the details of this prediction and copy it into a new DeliAddressModel object.
                resultList.add(new DeliAddressModel(prediction.getPlaceId(), prediction.getFullText(null).toString(),
                        prediction.getPrimaryText(null).toString()));

            }

            // Release the buffer now that all data has been copied.
            autocompletePredictions.release();
            return resultList;
        }
        Log.d("mapi", "Google API client is not connected for autocomplete query.");
        return null;
    }

//    private ArrayList<DeliAddressModel> getAddressFromPlaceApi(String keyword){
//        mGeoDataClient = Places.getGeoDataClient(activity.getBaseContext());
//        Task<AutocompletePredictionBufferResponse> results = null;
//        if (mBounds == null)
//            results = mGeoDataClient.getAutocompletePredictions(keyword, null, GeoDataClient.BoundsMode.BIAS, mPlaceFilter);
//        else results = mGeoDataClient.getAutocompletePredictions(keyword, mBounds, GeoDataClient.BoundsMode.STRICT, mPlaceFilter);
//        try {
//            Tasks.await(results, 60, TimeUnit.SECONDS);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            AutocompletePredictionBufferResponse autocompletePredictions = results.getResult();
//            Log.i("mapi", "Query completed. Received " + autocompletePredictions.getCount()
//                    + " predictions.");
//
//            // Freeze the results immutable representation that can be stored safely.
//            ArrayList<AutocompletePrediction> al = DataBufferUtils.freezeAndClose(autocompletePredictions);
//            ArrayList resultList = new ArrayList();
//
//            for (AutocompletePrediction p : al) {
//                CharSequence cs = p.getFullText(new CharacterStyle() {
//                    @Override
//                    public void updateDrawState(TextPaint tp) {
//
//                    }
//                });
//                resultList.add(new DeliAddressModel(p.getPlaceId(), p.getFullText(null).toString(),
//                        p.getPrimaryText(null).toString()));
//                Log.i("mapi", cs.toString());
//            }
//            autocompletePredictions.release();
//            return resultList;
//        } catch (RuntimeExecutionException e) {
//            // If the query did not complete successfully return null
//            Log.e("mapi", "Error getting autocomplete prediction API call", e);
//        }
//        return null;
//    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                synchronized (results) {
                    if (constraint.toString().equals("")) {
                        list.clear();
                        list.addAll(listAvailableAddress);
                        results.values = list;
                        results.count = list.size();
                    } else {
                        // Query the autocomplete API for the (constraint) search string.
                        list = getAutocomplete(constraint);
                        if (list != null) {
                            // The API successfully returned results.
                            results.values = list;
                            results.count = list.size();
                            listAvailableAddress.clear();
                        }
                    }
                    return results;
                }
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                synchronized (filterResults) {
                    list = (ArrayList<DeliAddressModel>) filterResults.values;
                    notifyDataSetChanged();
                }
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private LocationItemLayoutBinding binding;

        public ViewHolder(LocationItemLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemCLick(getAdapterPosition(), "LOCATION_CLICK", list.get(getAdapterPosition()), 0L);
                }
            });
        }

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

}
