package com.opencheck.client.home.newlogin.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogUpdatePasswordBinding;
import com.opencheck.client.home.newlogin.communicate.OnAgreeUpdate;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.tracking.GlobalTracking;

public class UpdatePasswordDialog extends LixiDialog {

    private static UpdatePasswordDialog instance;
    public static UpdatePasswordDialog getInstance(LixiActivity activity){
        if (instance == null){
            instance = new UpdatePasswordDialog(activity);
        }
        return instance;
    }

    private UpdatePasswordDialog(@NonNull LixiActivity _activity) {
        super(_activity, false, true, false);
    }

    private DialogUpdatePasswordBinding mBinding;

    private boolean canShow = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogUpdatePasswordBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        mBinding.txtRewardContent.setText(ConfigModel.getInstance(activity).getReference_code_lixi_text());

        mBinding.txtYes.setOnClickListener(this);
        mBinding.txtLater.setOnClickListener(this);
    }

    private OnAgreeUpdate onAgreeUpdate;
    public void setOnAgreeUpdate(OnAgreeUpdate onAgreeUpdate){
        this.onAgreeUpdate = onAgreeUpdate;
    }

    public void setCanShow(boolean canShow){
        this.canShow = canShow;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtYes:
                // goto update
                GlobalTracking.trackReferenceCodePopupAccepts();
                if (onAgreeUpdate != null){
                    onAgreeUpdate.onAgree();
                }
                cancel();
                break;
            case R.id.txtLater:
                cancel();
                canShow = true;
                break;
        }
    }

    @Override
    public void show() {
        if (canShow){
            cancel();
            GlobalTracking.trackReferenceCodePopupImpressionsEvent();
            super.show();
            canShow = false;
        }
    }
}
