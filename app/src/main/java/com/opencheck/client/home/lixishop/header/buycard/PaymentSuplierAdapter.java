package com.opencheck.client.home.lixishop.header.buycard;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SuplierItemBinding;
import com.opencheck.client.models.merchant.PaymentPartnerModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class PaymentSuplierAdapter extends RecyclerView.Adapter<PaymentSuplierAdapter.ViewHolder> {

    private LixiActivity activity;
    private List<PaymentPartnerModel> arrData;
    private int currentSelected = -1;

    public PaymentSuplierAdapter(List<PaymentPartnerModel> data, LixiActivity activity) {
        this.activity = activity;
        arrData = data;
    }

    public int getCurrentSelected() {
        return currentSelected;
    }

    public void setCurrentSelected(int currentSelected) {
        this.currentSelected = currentSelected;
    }

    //private List<UserDTO>
    public class ViewHolder extends RecyclerView.ViewHolder {

        public int pos;
        public View rootView;
        private ImageView iv_icon;
        private TextView tv_name;
        private TextView tv_link;
        private ImageView iv_select;
        private View v_divider;

        public ViewHolder(View itemView) {
            super(itemView);

            rootView = itemView;
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_link = (TextView) itemView.findViewById(R.id.tv_link);
            iv_select = (ImageView) itemView.findViewById(R.id.iv_select);
            v_divider = itemView.findViewById(R.id.v_divider);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCurrentSelected(pos);
                    notifyDataSetChanged();
                    if (listener != null)
                        listener.onClick(pos);
                }
            });
        }
    }

    @Override
    public PaymentSuplierAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SuplierItemBinding suplierItemBinding = SuplierItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(suplierItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(PaymentSuplierAdapter.ViewHolder viewHolder, int position) {
        viewHolder.pos = position;
        //viewHolder.tv_value.setText(arrData.get(position).getValue());

        if (position == getCurrentSelected()) {
            viewHolder.iv_select.setImageResource(R.drawable.ic_tick_violet);
        } else {
            viewHolder.iv_select.setImageResource(R.drawable.icon_radio_uncheck);
        }

        if (position == getItemCount() - 1)
            viewHolder.v_divider.setVisibility(View.GONE);
        else
            viewHolder.v_divider.setVisibility(View.VISIBLE);

        ImageLoader.getInstance().displayImage(arrData.get(position).getAvatar(), viewHolder.iv_icon, LixiApplication.getInstance().optionsNomal);
        viewHolder.tv_name.setText(arrData.get(position).getName());
        viewHolder.tv_link.setText(arrData.get(position).getWebsite());
//        ImageLoader.getInstance().displayImage(arrData.get(position).getUser().getAvatarPhoto(),
//                viewHolder.iv_avatar, ImageLoaderHelper.option_circle);
    }

    @Override
    public int getItemCount() {
        if (arrData == null)
            return 0;
        return arrData.size();
    }

//    public void addData(List<UserDetailDTO> listUser){
//        if (arrData==null)
//            arrData= new ArrayList<UserDetailDTO>();
//
//        arrData.addAll(listUser);
//    }

    public interface OnItemClickListener {
        public void onClick(int position);
    }

    public OnItemClickListener listener;

    public void setOnClickListener(OnItemClickListener lsn) {
        listener = lsn;
    }

}