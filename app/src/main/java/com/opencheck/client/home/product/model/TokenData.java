package com.opencheck.client.home.product.model;

public class TokenData {

    /**
     * token : 5dd840f87ba386e25b156891472fbbc8ac3d145032ad5e01
     * card : {"bank_code":"napas_atm","logo_url":"https://png.icons8.com/metro/1600/bank-cards.png","gateway_code":"napas","number":"970400xxxxxx0018","name":"NGUYEN VAN A","issue_date":"0307"}
     */

    private String token;
    private BankCard card;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BankCard getCard() {
        return card;
    }

    public void setCard(BankCard card) {
        this.card = card;
    }
}
