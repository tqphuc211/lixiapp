package com.opencheck.client.home.lixishop.header.rewardcode;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.LixiCodeDetailModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.List;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class LixiCodeDetailDialog extends LixiDialog {

    public LixiCodeDetailDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ImageView iv;
    private LinearLayout ll_info;
    private TextView tv_code;
    private TextView tv_dec;
    private TextView tv_condition;
    private TextView tv_copy;

    private long idCode;
    private LixiCodeDetailModel lixiCodeDetail;
    private LixiCodeBarnerModel barnerModel;


    public void setData(long id) {
        idCode = id;
        initDialogEvent();
        startLoadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reward_code_detail_dialog);
        findViews();
    }

    private void findViews() {
        iv = (ImageView) findViewById(R.id.iv);
        ll_info = (LinearLayout) findViewById(R.id.ll_info);
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_dec = (TextView) findViewById(R.id.tv_dec);
        tv_condition = (TextView) findViewById(R.id.tv_condition);
        tv_copy = (TextView) findViewById(R.id.tv_copy);
        tv_copy.setText(activity.getResources().getString(R.string.popup_lixi_code_detail_copy));
    }

    public void initDialogEvent() {

    }

    private void startLoadData() {
        DataLoader.getLixiShopRewardCodeDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    lixiCodeDetail = (LixiCodeDetailModel) object;
                    setViewData();
                }
            }
        }, idCode);
    }

    public void setViewData() {
        tv_dec.setText(getSpanable(lixiCodeDetail.getDesc(), lixiCodeDetail.getDesc_data()));
        tv_code.setText(LanguageBinding.getString(R.string.lixi_code, activity) + ":" + lixiCodeDetail.getCode());
        tv_condition.setText(lixiCodeDetail.getText_detail());
        if (lixiCodeDetail.getList_banner().size() > 0)
            ImageLoader.getInstance().displayImage(lixiCodeDetail.getList_banner().get(0).getImage(),
                    iv, LixiApplication.getInstance().optionsNomal);
        tv_copy.setOnClickListener(this);
    }

    public void setbaner(LixiCodeBarnerModel barnerModel) {
        this.barnerModel = barnerModel;

//        ImageLoader.getInstance().displayImage(barnerModel.getImage(), iv, AppController.getInstance().optionsNomal);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_copy:
                Helper.copyCode(lixiCodeDetail.getCode(), activity);
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    public SpannableStringBuilder getSpanable(String mess, List<String> data) {
        SpannableStringBuilder messageBuilder = new SpannableStringBuilder();
        if (data != null) {
            int st = 0;
            for (int i = 0; i < data.size(); i++) {
                int pos = mess.indexOf("{" + i + "}");
                if (pos > 0) {
                    messageBuilder.append(mess.substring(st, pos));
                    st = pos + 2 + (i > 9 ? 2 : 1);
                    SpannableString redSpannable = new SpannableString(data.get(i));
                    redSpannable.setSpan(new ForegroundColorSpan(0xff525252/*activity.getResources().getColor(R.color.app_color)*/), 0, data.get(i).length(), 0);
                    redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, data.get(i).length(), 0);
                    messageBuilder.append(redSpannable);
                }
            }
            if (st < mess.length() - 1)
                messageBuilder.append(mess.substring(st, mess.length()));
        } else
            messageBuilder.append(mess);

        return messageBuilder;

    }
}
