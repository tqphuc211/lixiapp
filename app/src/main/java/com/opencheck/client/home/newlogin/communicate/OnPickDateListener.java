package com.opencheck.client.home.newlogin.communicate;

public interface OnPickDateListener {
    void onPicked(String date);
}
