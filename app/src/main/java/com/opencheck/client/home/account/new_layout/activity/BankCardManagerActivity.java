package com.opencheck.client.home.account.new_layout.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityBankCardManagerBinding;
import com.opencheck.client.home.account.new_layout.communicate.OnRefreshListener;
import com.opencheck.client.home.account.new_layout.dialog.ChooseCardTypeDialog;
import com.opencheck.client.home.product.Control.CheckSecurity;
import com.opencheck.client.home.product.bank_card.SavedBankCardAdapter;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.ArrayList;

public class BankCardManagerActivity extends LixiActivity {
    private static String ACTION_DELETE_CARD = "action delete card";
    private static String DELETE_POSITION = "delete position";

    ImageView imgBack;
    RecyclerView recListCard;
    RelativeLayout relCreateNew;
    LinearLayout linearNoResult;

    SavedBankCardAdapter adapter;
    LinearLayoutManager layoutManager;
    ArrayList<TokenData> listToken;
    AppPreferenceHelper appPreferenceHelper;

    private ActivityBankCardManagerBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bank_card_manager);
        appPreferenceHelper = new AppPreferenceHelper(this);
        initView();
        setAdapter();
    }

    private void initView() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        recListCard = (RecyclerView) findViewById(R.id.recListCard);
        relCreateNew = (RelativeLayout) findViewById(R.id.rl_buy);
        linearNoResult = (LinearLayout) findViewById(R.id.linearNoResult);

        if (ConfigModel.getInstance(activity).getNapas_add_card_enable() == 1) {
            relCreateNew.setVisibility(View.VISIBLE);
        } else {
            relCreateNew.setVisibility(View.GONE);
        }

        imgBack.setOnClickListener(this);
        relCreateNew.setOnClickListener(this);
    }

    private void setAdapter() {
        listToken = appPreferenceHelper.getTokenBankCard();
        if (listToken == null) {
            listToken = new ArrayList<>();
        }
        adapter = new SavedBankCardAdapter(activity, listToken);
        layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recListCard.setLayoutManager(layoutManager);
        recListCard.setAdapter(adapter);

        adapter.addOnDeleteListener(new OnDeleteBankCard() {
            @Override
            public void onDelete(int position) {
                Intent intent = new Intent(BankCardManagerActivity.this, CheckSecurity.class);
                intent.putExtra(CheckSecurity.ACTION_CODE, ACTION_DELETE_CARD);
                intent.putExtra(DELETE_POSITION, position);
                startActivityForResult(intent, CheckSecurity.CHECK_SECURITY_CODE);
            }
        });

        if (listToken.size() == 0) {
            recListCard.setVisibility(View.GONE);
            linearNoResult.setVisibility(View.VISIBLE);
        } else {
            recListCard.setVisibility(View.VISIBLE);
            linearNoResult.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.rl_buy:
                showChooseCardTypeDialog();
                break;
        }
    }

    ChooseCardTypeDialog dialog;

    private void showChooseCardTypeDialog() {
//        ArrayList<TokenData> list = appPreferenceHelper.getTokenBankCard(key);
        dialog = new ChooseCardTypeDialog(activity, false, false, false);
        dialog.show();
        dialog.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                setAdapter();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case CheckSecurity.CHECK_SECURITY_CODE:
                String res = data.getStringExtra(CheckSecurity.ACTION_CODE);
                if (res.equals(ACTION_DELETE_CARD)) {
                    int position = data.getIntExtra(DELETE_POSITION, -1);
                    appPreferenceHelper.removeToken(listToken.get(position));
                    listToken.remove(position);
                    if (listToken.size() == 0) {
                        recListCard.setVisibility(View.GONE);
                        linearNoResult.setVisibility(View.VISIBLE);
                    } else {
                        recListCard.setVisibility(View.VISIBLE);
                        linearNoResult.setVisibility(View.GONE);
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }
}
