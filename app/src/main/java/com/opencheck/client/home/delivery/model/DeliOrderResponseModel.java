package com.opencheck.client.home.delivery.model;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.Serializable;

public class DeliOrderResponseModel implements Serializable {

    public final static String WEB_KEY = "web";
    public final static String APP_MOMO_KEY = "app_momo";
    public final static String APP_ZALO_KEY = "app_zalo";
    public final static String OFFLINE_KEY = "offline";
    public final static String RESULT_KEY = "result";

    private String user_order_uuid;
    private String payment_order_uuid;
    private PaymentDataModel payment_data;
    private String payment_flow;
    private long expiry_date;

    public String getPayment_order_uuid() {
        return payment_order_uuid;
    }

    public void setPayment_order_uuid(String payment_order_uuid) {
        this.payment_order_uuid = payment_order_uuid;
    }

    public String getUser_order_uuid() {
        return user_order_uuid;
    }

    public void setUser_order_uuid(String user_order_uuid) {
        this.user_order_uuid = user_order_uuid;
    }

    public PaymentDataModel getPayment_data() {
        return payment_data;
    }

    public String getPayment_flow() {
        return payment_flow;
    }

    public long getExpiry_date() {
        return expiry_date;
    }
}
