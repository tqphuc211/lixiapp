package com.opencheck.client.home.account.activecode.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.custom.SelectableRoundedImageView;
import com.opencheck.client.databinding.ActiveCodeFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.activecode.dialogs.ExpiredRequirementDialog;
import com.opencheck.client.home.history.detail.GeneralQRCodeAndCropTask;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.lixishop.CompletedVoucherModel;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class ActiveCodeFragment extends LixiFragment {

    private ImageView iv_back, iv_merchant;
    private TextView tv_title, tv_price, tv_code, tv_message, tv_expire_day, tv_condition_content, tv_name_merchant, tv_time_open, tv_price_value, tv_quantity_place, tv_place, tv_phone, tv_buy;
    private RelativeLayout rl_buy, rl_place;
    private SelectableRoundedImageView iv_qr;
    private LinearLayout ll_condition, ll_place, ll_merchant;
    private View v_line;
    public long idVoucher;
    private EvoucherDetailModel evoucherModel;
    private ActiveCodeFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = ActiveCodeFragmentBinding.inflate(inflater, container, false);
        idVoucher = getArguments().getLong(ConstantValue.EVOUCHER_ID);
        ConstantValue.ID = idVoucher;
        this.initView(mBinding.getRoot());
        loadData();
        return mBinding.getRoot();
    }

    private void initView(View v) {
        iv_back = v.findViewById(R.id.iv_back);
        iv_merchant = v.findViewById(R.id.iv_merchant);
        tv_title = v.findViewById(R.id.tv_title);
        tv_price = v.findViewById(R.id.tv_price);
        tv_code = v.findViewById(R.id.tv_code);
        tv_message = v.findViewById(R.id.tv_message);
        tv_expire_day = v.findViewById(R.id.tv_expire_day);
        tv_condition_content = v.findViewById(R.id.tv_condition_content);
        tv_name_merchant = v.findViewById(R.id.tv_name_merchant);
        tv_time_open = v.findViewById(R.id.tv_time_open);
        tv_price_value = v.findViewById(R.id.tv_price_value);
        tv_quantity_place = v.findViewById(R.id.tv_quantity_place);
        tv_place = v.findViewById(R.id.tv_place);
        tv_phone = v.findViewById(R.id.ed_phone);
        tv_buy = v.findViewById(R.id.tv_buy);
        ll_condition = v.findViewById(R.id.ll_condition);
        ll_place = v.findViewById(R.id.ll_place);
        ll_merchant = v.findViewById(R.id.ll_merchant);
        rl_buy = v.findViewById(R.id.rl_buy);
        rl_place = v.findViewById(R.id.rl_place);
        iv_qr = v.findViewById(R.id.iv_qr);
        v_line = v.findViewById(R.id.v_line);

        //click event
        iv_back.setOnClickListener(this);
        rl_buy.setOnClickListener(this);
        rl_place.setOnClickListener(this);
        ll_merchant.setOnClickListener(this);
        // loadData();

    }

    @SuppressLint("SetTextI18n")
    private void showData() {
        tv_title.setText(evoucherModel.getName());
        tv_price.setText(Helper.getVNCurrency(evoucherModel.getPrice()) + activity.getString(R.string.p));
        new GeneralQRCodeAndCropTask(activity, evoucherModel.getPin(), iv_qr, new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (pos == 1) {
                    iv_qr.setImageBitmap((Bitmap) object);
                }
            }
        });
        if (evoucherModel.isSend_pin())
            tv_code.setText(evoucherModel.getPin());
        else
            tv_code.setText("(Quét QR để kích hoạt mã)");
        tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.evoucher_detail_use_to, activity), Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y,
                String.valueOf(evoucherModel.getExpired_time() * 1000))));

        if (evoucherModel.getUse_condition() != null) {
            if (!evoucherModel.getUse_condition().equals("")) {
                tv_condition_content.setText(evoucherModel.getUse_condition());
                ll_condition.setVisibility(View.VISIBLE);
                v_line.setVisibility(View.VISIBLE);
            } else {
                ll_condition.setVisibility(View.GONE);
                v_line.setVisibility(View.GONE);
            }

        } else {
            ll_condition.setVisibility(View.GONE);
            v_line.setVisibility(View.GONE);
        }

        if (evoucherModel.getState().equals("used")) {
            tv_buy.setText(LanguageBinding.getString(R.string.bought_evoucher_used_state, activity));
            tv_buy.setBackground(activity.getDrawable(R.drawable.bg_promotion_green_corner));
        } else if (evoucherModel.getState().equals("expired")) {
            tv_buy.setText(LanguageBinding.getString(R.string.bought_evoucher_expiry, activity));
            tv_buy.setBackground(activity.getDrawable(R.drawable.bg_signup_black));
        } else if (evoucherModel.getState().equals("waiting")) {
            if (evoucherModel.getActive_ttl() > 0) {
                startTimer(evoucherModel.getActive_ttl() * 1000);
            }
        } else if (evoucherModel.getState().equals("can_active")) {
            tv_buy.setText(LanguageBinding.getString(R.string.bought_evoucher_active, activity));
            tv_buy.setBackground(activity.getDrawable(R.drawable.bg_promotion_red_corner_right));
        }

        setMerchantInfo();

        rl_buy.setVisibility(View.VISIBLE);
    }

    private void setMerchantInfo() {
        ImageLoader.getInstance().displayImage(evoucherModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
        tv_name_merchant.setText(evoucherModel.getMerchant_info().getName());
        tv_time_open.setText(activity.getString(R.string.open_time_title) + " " + evoucherModel.getMerchant_info().getOpening_time()
                + " - " + evoucherModel.getMerchant_info().getClose_time());
        tv_price_value.setText(getString(R.string.price_title) + " " +
                Helper.getVNCurrency(evoucherModel.getMerchant_info().getMin_price()) + " - " +
                Helper.getVNCurrency(evoucherModel.getMerchant_info().getMax_price()));
        tv_quantity_place.setText(String.format(LanguageBinding.getString(R.string.evoucher_detail_apply_at, activity), evoucherModel.getStore_apply().size()));
        if (evoucherModel.getStore_apply().size() > 1) {
            rl_place.setVisibility(View.VISIBLE);
            ll_place.setVisibility(View.GONE);
        } else {
            rl_place.setVisibility(View.GONE);
            ll_place.setVisibility(View.VISIBLE);
            tv_place.setText(evoucherModel.getStore_apply().get(0).getAddress());
            tv_phone.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), evoucherModel.getStore_apply().get(0).getPhone()));
        }
    }

    private void loadData() {
        DataLoader.getCashBoughtEvoucherDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    evoucherModel = (EvoucherDetailModel) object;
                    showData();

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, idVoucher);
    }

    public void reloadStatusCode(String status, Drawable drawable) {
        tv_buy.setText(status);
        tv_buy.setBackground(drawable);
    }

    private void checkLixiEvoucherStatus() {
        if (!evoucherModel.is_lixi_code()) {
            Helper.showErrorDialog(activity, activity.getString(R.string.code_message) + " " +
                    evoucherModel.getMerchant_info().getName() + " " + activity.getString(R.string.invalid_code_message));
        } else if (tv_buy.getText().toString().equalsIgnoreCase(LanguageBinding.getString(R.string.bought_evoucher_active, activity))) { //can active (new code, unused, not expired)
            if (evoucherModel.getStore_apply().size() < 2) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DataLoader.getCashBoughtEvoucherStoreApply(activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                if (isSuccess) {
                                    ArrayList<StoreApplyModel> storeApplyModels = (ArrayList<StoreApplyModel>) object;
                                    if (storeApplyModels == null || storeApplyModels.size() == 0)
                                        return;
                                    ConfirmActiveCodeFragment fragment = new ConfirmActiveCodeFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ev", new Gson().toJson(evoucherModel));
                                    bundle.putString("sa", new Gson().toJson(storeApplyModels.get(0)));
                                    fragment.setArguments(bundle);
                                    ((ActiveCodeActivity) activity).replaceFragment(fragment);
                                } else {
                                    Helper.showErrorToast(activity, object.toString());
                                }
                            }
                        }, evoucherModel.getProduct());
                    }
                }, 300);

            } else {
                Bundle bundle = new Bundle();
                bundle.putString("ev", new Gson().toJson(evoucherModel));
                bundle.putBoolean("is_only_seen", false);
                StoreSelectionFragment fragment = new StoreSelectionFragment();
                fragment.setArguments(bundle);
                ((ActiveCodeActivity) activity).replaceFragment(fragment);
            }
        } else if (tv_buy.getText().toString().equalsIgnoreCase(LanguageBinding.getString(R.string.bought_evoucher_used_state, activity))) { //activated
            rl_buy.setClickable(false);
        } else if (tv_buy.getText().toString().equalsIgnoreCase(LanguageBinding.getString(R.string.bought_evoucher_expiry, activity))) { //unused but no longer expired
            rl_buy.setClickable(false);
        } else if (tv_buy.getText().toString().contains(LanguageBinding.getString(R.string.bought_evoucher_waiting, activity))) { //waiting (checking by cashier)

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                activity.onBackPressed();
                break;
            case R.id.rl_buy:
                checkLixiEvoucherStatus();
                break;
            case R.id.rl_place:
                Bundle bundle = new Bundle();
                bundle.putString("ev", new Gson().toJson(evoucherModel));
                bundle.putBoolean("is_only_seen", true);
                StoreSelectionFragment fragment = new StoreSelectionFragment();
                fragment.setArguments(bundle);
                ((ActiveCodeActivity) activity).replaceFragment(fragment);
                break;
            case R.id.ll_merchant:
                MerchantModel merchantModel = new MerchantModel();
                merchantModel.setId(String.valueOf(evoucherModel.getMerchant_info().getId()));
                Intent detailIntent = new Intent(activity, MerchantDetailActivity.class);
                Bundle data = new Bundle();
                data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                detailIntent.putExtras(data);
                startActivity(detailIntent);
                break;
            default:
                break;
        }
    }

    private static final String FORMAT = "%02d:%02d";
    private CountDownTimer countDownTimer = null;

    private void startTimer(long timer) {
        long id = evoucherModel.getId();
        if (ConstantValue.map.containsKey(String.valueOf(id))) {
            if (!ConstantValue.map.get(String.valueOf(id))) {
                Intent intent1 = new Intent(ConstantValue.TIMER);
                intent1.putExtra("timer", evoucherModel.getActive_ttl() * 1000);
                intent1.putExtra("evoucher_id", id);
                intent1.putExtra("evoucher_name", evoucherModel.getName());
                intent1.putExtra("evoucher_code", evoucherModel.getPin());
                activity.sendOrderedBroadcast(intent1, null);
                ConstantValue.map.put(String.valueOf(id), true);
            }
        } else {
            Intent intent1 = new Intent(ConstantValue.TIMER);
            intent1.putExtra("timer", evoucherModel.getActive_ttl() * 1000);
            intent1.putExtra("evoucher_id", id);
            intent1.putExtra("evoucher_name", evoucherModel.getName());
            intent1.putExtra("evoucher_code", evoucherModel.getPin());
            activity.sendOrderedBroadcast(intent1, null);
            ConstantValue.map.put(String.valueOf(id), true);
        }


        countDownTimer = new CountDownTimer(timer, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                Log.d("mapi", "timer " + millisUntilFinished);
                tv_buy.setText(LanguageBinding.getString(R.string.bought_evoucher_waiting, activity) + " (" + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + ")");

                tv_buy.setBackground(activity.getDrawable(R.drawable.bg_active_code_waiting_status_orange));
            }

            public void onFinish() {
                cancelActiveCode();
            }
        }.start();
    }

    private void cancelActiveCode() {
        DataLoader.getCompleteVoucherCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    CompletedVoucherModel model = (CompletedVoucherModel) object;
                    if (!model.isIs_voucher_complete()) {
                        reloadStatusCode(LanguageBinding.getString(R.string.bought_evoucher_active, activity), activity.getDrawable(R.drawable.bg_promotion_red_corner_right));

                        ExpiredRequirementDialog dialog = new ExpiredRequirementDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData(model.getStore_name(), model.getProduct_name(), evoucherModel);
                    }

                } else {
                    reloadStatusCode(LanguageBinding.getString(R.string.bought_evoucher_active, activity), activity.getDrawable(R.drawable.bg_promotion_red_corner_right));
                    Helper.showErrorDialog(activity, object.toString());
                }
            }
        }, evoucherModel.getId());
    }

    public void cancelTimer() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cancelTimer();
    }
}
