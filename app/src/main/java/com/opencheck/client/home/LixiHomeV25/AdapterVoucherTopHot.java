package com.opencheck.client.home.LixiHomeV25;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowTopVoucherHotLayoutBinding;
import com.opencheck.client.home.flashsale.Global.VoucherFlashSaleHorizontalHolder;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class AdapterVoucherTopHot extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LixiActivity activity;
    private boolean haveFlashSale;

    public void setList_voucher(ArrayList<LixiShopEvoucherModel> list_voucher) {
        this.list_voucher = list_voucher;

        notifyDataSetChanged();
    }

    private ArrayList<LixiShopEvoucherModel> list_voucher;

    public AdapterVoucherTopHot(LixiActivity activity, ArrayList<LixiShopEvoucherModel> list) {
        this.activity = activity;
        this.list_voucher = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_FS:
//                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_top_voucher_hot_layout, parent, false);
//                return new ViewHolder(view1);
                return VoucherFlashSaleHorizontalHolder.newHolder(activity, parent);
            default:
                //case ITEM_TYPE_NORMAL:
                RowTopVoucherHotLayoutBinding rowTopVoucherHotLayoutBinding =
                        RowTopVoucherHotLayoutBinding.
                                inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ViewHolder(rowTopVoucherHotLayoutBinding.getRoot());
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, final int position) {
        if (getItemViewType(position) == ITEM_TYPE_FS) {
            VoucherFlashSaleHorizontalHolder voucherHolder = (VoucherFlashSaleHorizontalHolder) holder1;
            if (list_voucher.size() <= position)
                return;
            LixiShopEvoucherModel voucher = list_voucher.get(position);

            VoucherFlashSaleHorizontalHolder.bindViewHolder(voucherHolder, voucher);
            return;
        }
        ViewHolder holder = (ViewHolder) holder1;
//        ViewGroup.LayoutParams layoutParams = holder.lnItemTopVoucher.getLayoutParams();
//        layoutParams.width = (int) (activity.widthScreen / 3 + activity.getResources().getDimension(R.dimen.value_30));
//        holder.lnItemTopVoucher.setLayoutParams(layoutParams);
        LixiShopEvoucherModel voucher = list_voucher.get(position);
        if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0) {
//            ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), holder.imgLogoTopHotVoucher, LixiApplication.getInstance().optionsNomal);
            LixiImage.with(activity)
                    .load(voucher.getProduct_cover_image())
                    .size(ImageSize.SMALL)
                    .into(holder.imgLogoTopHotVoucher);
        } else {
            if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0) {
//                ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), holder.imgLogoTopHotVoucher, LixiApplication.getInstance().optionsNomal);
                LixiImage.with(activity)
                        .load(voucher.getProduct_image().get(0))
                        .size(ImageSize.SMALL)
                        .into(holder.imgLogoTopHotVoucher);
            }
        }
        holder.tvLixiBonusVoucherHot.setText("+" + Helper.getVNCurrency(voucher.getCashback_price()));
        holder.tvNameHotVoucher.setText(voucher.getName());
        holder.tvPriceHotVoucher.setText(Helper.getVNCurrency(voucher.getPayment_discount_price()) + "đ");
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CashEvoucherDetailActivity.startCashVoucherDetail(activity, list_voucher.get(position).getId());
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list_voucher.get(position).getId(), TrackingConstant.ContentSource.HOME);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list_voucher == null)
            return 0;
        return list_voucher.size();

    }

    public static final int ITEM_TYPE_NORMAL = 0;
    public static final int ITEM_TYPE_FS = 1;

    @Override
    public int getItemViewType(int position) {
        if (list_voucher != null && list_voucher.size() > position)
            if (list_voucher.get(position).getFlash_sale_id() > 0)
                return ITEM_TYPE_FS;
        return ITEM_TYPE_NORMAL;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private View rootView;
        private ImageView imgLogoTopHotVoucher;
        private TextView tvLixiBonusVoucherHot;
        private TextView tvNameHotVoucher;
        private TextView tvPriceHotVoucher;
        private LinearLayout lnItemTopVoucher;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            lnItemTopVoucher = itemView.findViewById(R.id.lnItemTopVoucher);
            imgLogoTopHotVoucher = (ImageView) itemView.findViewById(R.id.imgLogoTopHotVoucher);
            tvLixiBonusVoucherHot = (TextView) itemView.findViewById(R.id.tvLixiBonusVoucherHot);
            tvNameHotVoucher = (TextView) itemView.findViewById(R.id.tvNameHotVoucher);
            tvPriceHotVoucher = (TextView) itemView.findViewById(R.id.tvPriceHotVoucher);

        }
    }

    class ViewHolderFS extends RecyclerView.ViewHolder {
        private View rootView;
        private ImageView imgLogoTopHotVoucher;
        private TextView tvLixiBonusVoucherHot;
        private TextView tvNameHotVoucher;
        private TextView tvPriceHotVoucher;
        private LinearLayout lnItemTopVoucher;

        public ViewHolderFS(View itemView) {
            super(itemView);
            rootView = itemView;
            lnItemTopVoucher = itemView.findViewById(R.id.lnItemTopVoucher);
            imgLogoTopHotVoucher = (ImageView) itemView.findViewById(R.id.imgLogoTopHotVoucher);
            tvLixiBonusVoucherHot = (TextView) itemView.findViewById(R.id.tvLixiBonusVoucherHot);
            tvNameHotVoucher = (TextView) itemView.findViewById(R.id.tvNameHotVoucher);
            tvPriceHotVoucher = (TextView) itemView.findViewById(R.id.tvPriceHotVoucher);

        }
    }
}
