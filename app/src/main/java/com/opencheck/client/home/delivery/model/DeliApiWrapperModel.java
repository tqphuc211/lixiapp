package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.apiwrapper.MetaDataModel;

import java.io.Serializable;

public class DeliApiWrapperModel<T> implements Serializable {
    private MetaDataModel meta;
    private T data;
    private ErrorModel error;

    public MetaDataModel get_meta() {
        return meta;
    }

    public void set_meta(MetaDataModel _meta) {
        this.meta = meta;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel error) {
        this.error = error;
    }
}
