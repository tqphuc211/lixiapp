package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class FeedbackOrderModel implements Serializable {
    private long id;
    private String message;
    private float rating;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
