package com.opencheck.client.home.account.activecode2.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.RowNearestStoreLayoutBinding;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.activecode2.fragment.NearestStoreFragment;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by Tony Tuan on 03/27/2018.
 */

public class StoreByAdapter extends RecyclerView.Adapter<StoreByAdapter.StoreViewHolder> {

    LixiActivity activity;
    private ArrayList<NearestStoreModel> arrStore;
    private ItemClickListener itemClickListener;

    public StoreByAdapter(LixiActivity activity, ArrayList<NearestStoreModel> arrStore) {
        this.activity = activity;
        this.arrStore = arrStore;
    }

    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowNearestStoreLayoutBinding rowNearestStoreLayoutBinding =
                RowNearestStoreLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new StoreViewHolder(rowNearestStoreLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(StoreViewHolder holder, int position) {
        if (((ActiveCode2Activity) activity).getCurrentFragment() instanceof NearestStoreFragment) {
            holder.rootViewItem.setTag(position);
            NearestStoreModel storeApplyModel = arrStore.get(position);
            if (position == arrStore.size() - 1)
                holder.v_line.setVisibility(View.GONE);

            ImageLoader.getInstance().displayImage(storeApplyModel.getMerchant_info().getLogo(), holder.iv_store, LixiApplication.getInstance().optionsNomal);
            holder.tv_name.setText(storeApplyModel.getName());
            holder.tv_address.setText(storeApplyModel.getAddress());
            holder.tv_distance.setVisibility(View.VISIBLE);

            if (storeApplyModel.isAllow_user_send_request_active())
                holder.iv_map.setImageResource(R.drawable.icon_scan_qr);
            else
                holder.iv_map.setImageResource(R.drawable.icon_scan_qr_none);

            double distance = storeApplyModel.getUser_distance();
            if (distance < 1.0) {
                distance = distance * 1000;
                holder.tv_distance.setText("(" + ((double) Math.round(distance * 10) / 10) + " m)");
            } else
                holder.tv_distance.setText("(" + ((double) Math.round(distance * 10) / 10) + " km)");
        } else {
            holder.rootViewItem.setTag(position);
            NearestStoreModel storeApplyModel = arrStore.get(position);
            if (position == arrStore.size() - 1)
                holder.v_line.setVisibility(View.GONE);

            ImageLoader.getInstance().displayImage(storeApplyModel.getMerchant_info().getLogo(), holder.iv_store, LixiApplication.getInstance().optionsNomal);
            holder.tv_name.setText(storeApplyModel.getName());
            holder.tv_address.setText(storeApplyModel.getAddress());
            holder.tv_distance.setVisibility(View.GONE);

            if (storeApplyModel.isAllow_user_send_request_active())
                holder.iv_map.setImageResource(R.drawable.icon_scan_qr);
            else
                holder.iv_map.setImageResource(R.drawable.icon_scan_qr_none);
        }
    }

    @Override
    public int getItemCount() {
        return arrStore.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class StoreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_distance;
        private TextView tv_name, tv_address;
        private ImageView iv_map;
        private CircleImageView iv_store;
        private View v_line;
        private View rootViewItem;

        public StoreViewHolder(View itemView) {
            super(itemView);
            rootViewItem = itemView;
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            iv_map = (ImageView) itemView.findViewById(R.id.iv_map);
            iv_store = (CircleImageView) itemView.findViewById(R.id.iv_store);
            v_line = itemView.findViewById(R.id.v_line);
            tv_distance = (TextView) itemView.findViewById(R.id.tv_distance);
            rootViewItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == rootViewItem) {
                itemClickListener.onItemCLick(getAdapterPosition(), ConstantValue.ON_ITEM_STORE_CLICK, arrStore.get(getAdapterPosition()), 0L);
            }

        }
    }
}
