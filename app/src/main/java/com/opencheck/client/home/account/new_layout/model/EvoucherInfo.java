package com.opencheck.client.home.account.new_layout.model;

import android.os.Parcel;
import android.os.Parcelable;

public class EvoucherInfo implements Parcelable {

    /**
     * discount_price : 0
     * expired_time : 0
     * id : 0
     * payment_discount_price : 0
     * pin : string
     * price : 0
     * product : 0
     * send_pin : true
     * send_serial : true
     * serial : string
     */

    private int discount_price;
    private long expired_time;
    private int id;
    private int payment_discount_price;
    private String pin;
    private int price;
    private long product;
    private boolean send_pin;
    private boolean send_serial;
    private String serial;

    protected EvoucherInfo(Parcel in) {
        discount_price = in.readInt();
        expired_time = in.readLong();
        id = in.readInt();
        payment_discount_price = in.readInt();
        pin = in.readString();
        price = in.readInt();
        product = in.readLong();
        send_pin = in.readByte() != 0;
        send_serial = in.readByte() != 0;
        serial = in.readString();
    }

    public static final Creator<EvoucherInfo> CREATOR = new Creator<EvoucherInfo>() {
        @Override
        public EvoucherInfo createFromParcel(Parcel in) {
            return new EvoucherInfo(in);
        }

        @Override
        public EvoucherInfo[] newArray(int size) {
            return new EvoucherInfo[size];
        }
    };

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public long getExpired_time() {
        return expired_time;
    }

    public void setExpired_time(long expired_time) {
        this.expired_time = expired_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public boolean isSend_pin() {
        return send_pin;
    }

    public void setSend_pin(boolean send_pin) {
        this.send_pin = send_pin;
    }

    public boolean isSend_serial() {
        return send_serial;
    }

    public void setSend_serial(boolean send_serial) {
        this.send_serial = send_serial;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public boolean equals(Object obj) {
        long i = (long) obj;
        return product == i;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(discount_price);
        parcel.writeLong(expired_time);
        parcel.writeInt(id);
        parcel.writeInt(payment_discount_price);
        parcel.writeString(pin);
        parcel.writeInt(price);
        parcel.writeLong(product);
        parcel.writeByte((byte) (send_pin ? 1 : 0));
        parcel.writeByte((byte) (send_serial ? 1 : 0));
        parcel.writeString(serial);
    }
}
