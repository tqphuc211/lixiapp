package com.opencheck.client.home.history.detail.controller;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemCardHistoryBinding;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class CardController implements View.OnClickListener {

    private Activity activity;
    private LinearLayout llRoot;
    private int pos;
    private View viewChild;
    private View viewLine;
    private TextView tv_label, tv_value;
    private String value, label;

    public CardController(Activity activity, String label, String value, int pos, final LinearLayout _llRoot){
        this.activity = activity;
        this.pos = pos;
        this.llRoot = _llRoot;
        this.label = label;
        this.value = value;
        ItemCardHistoryBinding itemCardHistoryBinding =
                ItemCardHistoryBinding.inflate(LayoutInflater.from(activity),
                        null, false);
        this.viewChild = itemCardHistoryBinding.getRoot();
        this.initView();
        this.initData();

        this.llRoot.post(new Runnable() {
            @Override
            public void run() {
                llRoot.addView(viewChild);
            }
        });

    }

    private void initView(){
        tv_label = (TextView) viewChild.findViewById(R.id.tv_label_card);
        tv_value = (TextView) viewChild.findViewById(R.id.tv_value_card);
        viewLine = viewChild.findViewById(R.id.view_line);
    }

    private void initData(){
        tv_value.setText(value);
        tv_label.setText(label);

        tv_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tv_value.getText().toString().contains("-")) {
                    Helper.copyCode(tv_value.getText().toString(), activity);
                }
            }
        });

    }


    @Override
    public void onClick(View view) {

    }
}