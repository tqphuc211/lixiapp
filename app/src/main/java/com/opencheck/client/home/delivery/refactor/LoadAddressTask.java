package com.opencheck.client.home.delivery.refactor;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoadAddressTask extends AsyncTask<LatLng, Object, Object> {

    private AddressCallback mCallBack;
    private LatLng mPosition;

    public LoadAddressTask(AddressCallback callback) {
        mCallBack = callback;
    }

    @Override
    protected Object doInBackground(LatLng... params) {
        mPosition = params[0];
        return getLocationName(mPosition);
    }

    @Override
    protected void onPostExecute(Object result) {

        super.onPostExecute(result);

//        mCallBack.callback(mPosition, updateLocation(String.valueOf(result)));
        String fullAddress = String.valueOf(result);
        mCallBack.callback(mPosition, fullAddress);
    }


    private String getLocationName(LatLng key) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(makeUrl(key))
                    .build();

            Response response = client.newCall(request).execute();

            String responseStr = response.body().string();
            JSONObject jsonObject = new JSONObject(responseStr);
            JSONArray routeArray = jsonObject.getJSONArray("results");
            JSONObject note = routeArray.getJSONObject(0);
            Helper.showLog("Address: " + note.toString());
            if (note.getString("formatted_address") == null || note.getString("formatted_address").length() == 0) {
                getLocationName(key);
            } else {
                return note.getString("formatted_address");
//                res.add(note.getString("formatted_address"));
//                res.add(note.getString("place_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public String updateLocation(String location) {
        try {
            location = new String(location.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        } catch (Throwable e) {
            location = "";
            e.printStackTrace();
        }
        return location;
    }

    private String makeUrl(LatLng point) {
        String urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                point.latitude + "," + point.longitude +
                "&sensor=false&key=AIzaSyAfxluMz7e0E1jdulRlUuABzFP3PX0h2Lk";
        return urlString.trim().replace(" ", "%20");
    }
}
