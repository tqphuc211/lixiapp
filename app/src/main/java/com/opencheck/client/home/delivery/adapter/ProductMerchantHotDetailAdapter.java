package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ProductMerchantHotItemBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class ProductMerchantHotDetailAdapter extends RecyclerView.Adapter<ProductMerchantHotDetailAdapter.ViewHolder> {
    private LixiActivity activity;
    private ArrayList<ProductMerchantHot> productMerchantHotList;

    public ProductMerchantHotDetailAdapter(LixiActivity activity, ArrayList<ProductMerchantHot> productMerchantHotList) {
        this.activity = activity;
        this.productMerchantHotList = productMerchantHotList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ProductMerchantHotItemBinding productMerchantHotItemBinding =
                ProductMerchantHotItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(productMerchantHotItemBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ProductMerchantHot model = productMerchantHotList.get(position);
        LixiImage.with(activity)
                .load(model.getImage_link())
                .size(ImageSize.AUTO)
                .into(holder.binding.ivAvatar);

        holder.binding.tvName.setText(model.getName());


        ViewGroup.LayoutParams params = holder.binding.ivAvatar.getLayoutParams();
        int width = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_1)) / 2;
        params.width = width;
        params.height = width * 3 / 4;
        holder.binding.ivAvatar.setLayoutParams(params);

        holder.binding.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreDetailActivity.startStoreDetailActivity(activity, model.getStore_id(), TrackingConstant.ContentSource.HOME, model.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return productMerchantHotList == null ? 0 : productMerchantHotList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ProductMerchantHotItemBinding binding;

        public ViewHolder(ProductMerchantHotItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
