package com.opencheck.client.home.delivery.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityOrderTrackingBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.ApiEntity;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.dialogs.OptionDialog;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.adapter.SupportReasonAdapter;
import com.opencheck.client.home.delivery.controller.ExtraInfoController;
import com.opencheck.client.home.delivery.controller.FoodController;
import com.opencheck.client.home.delivery.database.SQLiteDatabaseHandler;
import com.opencheck.client.home.delivery.dialog.PaymentProcessDialog;
import com.opencheck.client.home.delivery.libs.OnlinePaymentHelper;
import com.opencheck.client.home.delivery.model.ClaimReasonModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.DeliOrderResponseModel;
import com.opencheck.client.home.delivery.model.QuotationModel;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;
import vn.zalopay.sdk.ZaloPayErrorCode;

public class OrderTrackingActivity extends LixiActivity {
    public static final String uuid = "uuid";
    public static final String RESPONSE_DATA = "data";
    public final static String WEB_KEY = "web";
    public final static String APP_MOMO_KEY = "app_momo";
    public final static String APP_ZALO_KEY = "app_zalo";
    public final static String OFFLINE_KEY = "offline";
    public final static String RESULT_KEY = "result";

    private Socket mSocket;
    private String orderId;
    private DeliOrderModel orderModel;
    private ActivityOrderTrackingBinding mBinding;
    private int progressInt = 0;
    private DeliOrderResponseModel responseData = null;
    private boolean isDone = false, isFromHistory = false;
    private OnlinePaymentHelper paymentHelper = null;
    private SupportReasonAdapter mReasonAdapter;

    public static void startOrderTrackingActivity(@NonNull Context context,
                                                  @NonNull String _uuid,
                                                  DeliOrderResponseModel responseData,
                                                  Boolean isFromHistory) {
        Bundle bundle = new Bundle();
        bundle.putString(uuid, _uuid);
        bundle.putBoolean("HISTORY", isFromHistory);
        bundle.putSerializable(RESPONSE_DATA, responseData);
        Intent intent = new Intent(context, OrderTrackingActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_tracking);
        orderId = getIntent().getExtras().getString(uuid);
        isFromHistory = getIntent().getExtras().getBoolean("HISTORY", false);
        responseData = (DeliOrderResponseModel) getIntent().getExtras().getSerializable(RESPONSE_DATA);
        initViews();
    }

    private void initViews() {
        mBinding.ivClose.setOnClickListener(this);
        mBinding.tvSupport.setOnClickListener(this);
        mBinding.rlStore.setOnClickListener(this);
        mBinding.tvHotline.setOnClickListener(this);
        mBinding.tvMessageEvent.setOnClickListener(this);
        mBinding.ivMessenger.setOnClickListener(this);

        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrder();
            }
        });

        mBinding.scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                Helper.setScrollProrgess(mBinding.scrollView.getScrollY(), mBinding.vBack, mBinding.tvChitietsanpham, mBinding.tvSupport, mBinding.ivClose);
            }
        });

        getUserProfile();
        getOrder();
        getReasons();
    }

    private void updateProgress(ProgressBar pr, Runnable runnable) {
        progressInt += 1;
        if (progressInt > pr.getMax()) {
            handler.removeCallbacks(runnable);
        } else {
            pr.setProgress(progressInt);
        }
    }

    private void connectSocket() {
        String tokenWithoutJWT = DataLoader.token;
        tokenWithoutJWT = tokenWithoutJWT.replaceFirst("JWT ", "");
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;
        opts.transports = new String[]{WebSocket.NAME};
        opts.query = "token=" + tokenWithoutJWT;

        final Handler mHandler = new Handler();

        try {
            String url = ConfigModel.getInstance(activity).getSocket_url();
            mSocket = IO.socket(url, opts);
            final String finalTokenWithoutJWT = tokenWithoutJWT;

            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("mapi", "Connected");

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject params = new JSONObject();
                                params.putOpt("user_authorization", finalTokenWithoutJWT);
                                mSocket.emit("connection/user_join", params, new Ack() {
                                    @Override
                                    public void call(Object... args) {
                                        Log.d("mapi", args[0].toString());
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, 1000);

                }
            }).on("user_order/state_changed", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("mapi", args[0].toString());
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        final String uuid = jsonObject.getString("user_order_uuid");
                        final String newState = jsonObject.getString("new_state");

//                        JSONObject currentTracking = jsonObject.getJSONObject("current_tracking");
//                        final String newState = currentTracking.getString("state");
//                        final String textState = currentTracking.getString("text");
//                        final Long startDate = currentTracking.getLong("start_date");
//                        final Long endDate = currentTracking.getLong("end_date");
//                        final String shippingDriverName = jsonObject.getString("shipping_driver_name");
//                        final String shippingDriverPhone = jsonObject.getString("shipping_driver_phone");
                        if (uuid.equals(orderModel.getUuid())) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DeliDataLoader.getUserOrderDetail(activity, new ApiCallBack() {
                                        @Override
                                        public void handleCallback(boolean isSuccess, Object object) {
                                            if (isSuccess) {
                                                DeliOrderModel model = (DeliOrderModel) object;
                                                orderModel = model;
                                                mBinding.tvStatus.setText(model.getCurrent_tracking().getText());
                                                mBinding.tvStartTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING,
                                                        String.valueOf(model.getCurrent_tracking().getStart_date())) + " - "));
                                                mBinding.tvEndTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(model.getCurrent_tracking().getEnd_date())));
                                                long diff = model.getCurrent_tracking().getEnd_date() - model.getCurrent_tracking().getStart_date();
                                                Long timer = TimeUnit.MILLISECONDS.toSeconds(diff);
                                                progressInt = 0;
                                                String state = model.getCurrent_tracking().getState();
                                                switch (state) {
                                                    case DeliOrderModel.SUBMITTED:
                                                        handler.removeCallbacks(runnable1);
                                                        mBinding.tvStartTime.setVisibility(View.VISIBLE);
                                                        mBinding.tvEndTime.setVisibility(View.VISIBLE);
                                                        mBinding.tvMessageEvent.setVisibility(View.GONE);
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.vLine.setVisibility(View.GONE);
                                                        mBinding.sb1.setMax(timer.intValue());
                                                        runnable1.run();
                                                        break;
                                                    case DeliOrderModel.CONFIRMED:
                                                        handler.removeCallbacks(runnable1);
                                                        mBinding.sb1.setProgress(mBinding.sb1.getMax());
                                                        mBinding.sb2.setMax(timer.intValue());
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.vLine.setVisibility(View.GONE);
                                                        runnable2.run();
                                                        break;
                                                    case DeliOrderModel.ASSIGNED:
                                                        handler.removeCallbacks(runnable2);
                                                        mBinding.sb2.setProgress(mBinding.sb2.getMax());
                                                        mBinding.sb3.setMax(timer.intValue());
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.vLine.setVisibility(View.GONE);
                                                        runnable3.run();
                                                        break;
                                                    case DeliOrderModel.PICKED:
                                                        handler.removeCallbacks(runnable3);
                                                        mBinding.sb3.setProgress(mBinding.sb3.getMax());
                                                        mBinding.sb4.setMax(timer.intValue());
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.vLine.setVisibility(View.GONE);
                                                        mBinding.tvDriverInfo.setText(Html.fromHtml(model.getShipping_driver_name() + " - " + model.getShipping_driver_phone()));
                                                        mBinding.tvSupport.setVisibility(View.GONE);
                                                        runnable4.run();
                                                        break;
                                                    case DeliOrderModel.COMPLETED:
                                                        handler.removeCallbacks(runnable4);
                                                        mBinding.sb4.setProgress(mBinding.sb4.getMax());
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.vLine.setVisibility(View.GONE);
                                                        mBinding.llProgress.setVisibility(View.GONE);
                                                        mBinding.tvStartTime.setVisibility(View.GONE);
                                                        mBinding.tvEndTime.setVisibility(View.GONE);
                                                        mBinding.vLineInCard.setVisibility(View.VISIBLE);
                                                        mBinding.llPaid.setVisibility(View.GONE);
                                                        mBinding.tvMessageEvent.setVisibility(View.VISIBLE);
                                                        mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_send_feedback, activity));
                                                        mBinding.tvSupport.setVisibility(View.GONE);

                                                        SQLiteDatabaseHandler sqLiteDatabaseHandler = SQLiteDatabaseHandler.getInstance(activity);
                                                        if (!sqLiteDatabaseHandler.isExist(orderModel.getReceive_address())) {
                                                            sqLiteDatabaseHandler.addAddress(new DeliAddressModel(orderModel.getReceive_address(),
                                                                    orderModel.getReceive_address().substring(0, orderModel.getReceive_address().contains(",") ? orderModel.getReceive_address().indexOf(",") : orderModel.getReceive_address().length()),
                                                                    orderModel.getReceive_lat(), orderModel.getReceive_lng()));
                                                        }
                                                        break;
                                                    case DeliOrderModel.CANCELED:
                                                        mBinding.rlDriver.setVisibility(View.GONE);
                                                        mBinding.tvSupport.setVisibility(View.GONE);
                                                        mBinding.tvStartTime.setVisibility(View.GONE);
                                                        mBinding.tvEndTime.setVisibility(View.GONE);
                                                        mBinding.tvMessageEvent.setVisibility(View.GONE);
                                                        mBinding.llProgress.setVisibility(View.GONE);
                                                        mBinding.tvMessage.setVisibility(View.VISIBLE);
                                                        mBinding.vLineInCard.setVisibility(View.VISIBLE);
                                                        mBinding.llPaid.setVisibility(View.GONE);
                                                        mBinding.tvStatus.setText(LanguageBinding.getString(R.string.deli_tracking_cancel_order, activity));
                                                        mBinding.tvMessage.setText(Html.fromHtml("Lý do: " + model.getClose_reason_message()));
                                                        break;
                                                }
                                            }
                                        }
                                    }, uuid, null);
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("mapi", "disconnected");
                }
            });

            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void showData() {
        mBinding.rlLoading.setVisibility(View.GONE);
        int width = mBinding.cardView.getWidth() - (int) activity.getResources().getDimension(R.dimen.value_18);
        LinearLayout.LayoutParams param1 = (LinearLayout.LayoutParams) mBinding.sb1.getLayoutParams();
        param1.width = (int) (0.1 * width);

        LinearLayout.LayoutParams param2 = (LinearLayout.LayoutParams) mBinding.sb2.getLayoutParams();
        param2.width = (int) (0.2 * width);

        LinearLayout.LayoutParams param3 = (LinearLayout.LayoutParams) mBinding.sb3.getLayoutParams();
        param3.width = (int) (0.2 * width);

        LinearLayout.LayoutParams param4 = (LinearLayout.LayoutParams) mBinding.sb4.getLayoutParams();
        param4.width = (int) (0.5 * width);

        mBinding.sb1.setLayoutParams(param1);
        mBinding.sb2.setLayoutParams(param2);
        mBinding.sb3.setLayoutParams(param3);
        mBinding.sb4.setLayoutParams(param4);

        ImageLoader.getInstance().displayImage(orderModel.getStore_logo_link(), mBinding.ivAvatar, LixiApplication.getInstance().optionsNomal);
        mBinding.tvNameStore.setText(orderModel.getStore_name());
        mBinding.tvTotal.setText(Html.fromHtml("ID #" + orderModel.getRef_code()));
        mBinding.tvUserInfo.setText(Html.fromHtml(orderModel.getReceive_name() + " - " + orderModel.getReceive_phone()));
        mBinding.tvPriceTotal.setText(Html.fromHtml(Helper.getVNCurrency(orderModel.getMoney()) + "đ"));
        mBinding.tvHotline.setText(ConfigModel.getInstance(activity).getDefaultSupportPhone());

        if (isFromHistory) {
            HashMap<String, Integer> countMap = Helper.getRepayCount(this, "REPAY_FILE");
            if (countMap.containsKey(orderModel.getUuid())) {
                if (countMap.get(orderModel.getUuid()) == 1) {
                    countMap.put(orderModel.getUuid(), 0);
                    Helper.saveProductToFile(countMap, this, "REPAY_FILE");
                }
            }
        }

        if (orderModel.getCashback() > 0) {
            mBinding.rlCashback.setVisibility(View.VISIBLE);
            mBinding.tvCashback.setText(Html.fromHtml("+" + Helper.getVNCurrency(orderModel.getCashback()) + " Lixi"));
        }

        mBinding.tvFullAddress.setText(orderModel.getReceive_address());
        mBinding.tvChitietsanpham.setText(String.format(LanguageBinding.getString(R.string.deli_tracking_order_code, activity), orderModel.getRef_code()));

        if (orderModel.getReceive_date() == null) {
            mBinding.tvTime.setText(LanguageBinding.getString(R.string.deli_tracking_earliest, activity));
        } else {
            try {
                Calendar currentDate = Calendar.getInstance();
                currentDate.add(Calendar.DATE, 1);
                String tomorrow = Helper.getFormatDateVN("dd/MM/yyyy", currentDate.getTimeInMillis());
                String curTime = Helper.getFormatDateVN("dd/MM/yyyy", System.currentTimeMillis());
                String receiveTime = Helper.getFormatDateVN("dd/MM/yyyy", orderModel.getReceive_date());

                if (curTime.equals(receiveTime))
                    mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING,
                            String.valueOf(orderModel.getReceive_date()))
                            + LanguageBinding.getString(R.string.deli_tracking_today, activity)));
                else if (orderModel.getReceive_date() > System.currentTimeMillis()
                        && tomorrow.equals(receiveTime))
                    mBinding.tvTime.setText(Html.fromHtml(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING,
                            String.valueOf(orderModel.getReceive_date()))
                            + LanguageBinding.getString(R.string.deli_tracking_tomorrow, activity)));
                else
                    mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, String.valueOf(orderModel.getReceive_date())));
            } catch (Exception ex) {
                mBinding.tvTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_ORDER_WITHOUT_SECONDS, String.valueOf(orderModel.getReceive_date())));
            }
        }

        mBinding.llItems.removeAllViews();
        mBinding.llExtraInfo.removeAllViews();

        for (int i = 0; i < orderModel.getList_product().size(); i++) {
            new FoodController(activity, orderModel.getList_product().get(i), mBinding.llItems, false, i, orderModel.getList_product().size());
        }

        for (int i = 0; i < orderModel.getList_quotation().size(); i++) {
            QuotationModel quotationModel = orderModel.getList_quotation().get(i);
            if ((!orderModel.isApply_service_price() && quotationModel.getCode().equalsIgnoreCase("service_money"))
                    || (quotationModel.getMoney() == 0 && !quotationModel.getCode().equalsIgnoreCase("ship_money") && !quotationModel.getCode().equalsIgnoreCase("service_money")))
                continue;
            new ExtraInfoController(activity, null, orderModel, mBinding.llExtraInfo, false, i, orderModel.getList_quotation());
        }

        hideQuickSupport();

        if (orderModel.getPayment_method() != null) {
            mBinding.tvMethod.setText(orderModel.getPayment_method().getName());
            if (!orderModel.getPayment_method().getCode().equals(DeliConfigModel.PAYMENT_BY_COD)) {
//                mBinding.tvSupport.setVisibility(View.GONE);
                if (!orderModel.getState().equals(DeliOrderModel.PAYMENT))
                    mBinding.llPaid.setVisibility(View.VISIBLE);
                if (!isDone && responseData != null) {
                    switch (responseData.getPayment_flow()) {
                        case WEB_KEY:
                            PaymentProcessDialog dialog = new PaymentProcessDialog(activity, false, false, false);
                            dialog.show();
                            dialog.setData(responseData.getPayment_data().getPay_url(),
                                    responseData.getPayment_data().getRedirect_pattern());
                            dialog.setIDialogEventListener(new PaymentProcessDialog.IDialogEvent() {
                                @Override
                                public void onOk(boolean isOk) {
                                    isDone = true;
                                    mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));
                                    trackingPurchaseSuccess(isOk);
                                    if (isOk) {
                                        getOrder();
                                    } else {
                                        trackingCancelPurchase();
                                    }
                                }
                            });
                            break;
                        case APP_MOMO_KEY:
                            OnlinePaymentHelper.Builder momoBuilder = new OnlinePaymentHelper.Builder(activity);
                            momoBuilder.setType(0)
                                    .setSubmitUrl(responseData.getPayment_data().getSubmit_url())
                                    .setSdkParams((HashMap<String, Object>) responseData.getPayment_data().getSdk_param())
                                    .setOnlinePaymentListener(new OnlinePaymentHelper.OnlinePaymentListener() {
                                        @Override
                                        public void onSuccess() {
                                            trackingPurchaseSuccess(true);
                                            isDone = true;
                                            mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));
                                            getOrder();
                                        }

                                        @Override
                                        public void onFailed(int errorCode) {
                                            mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));
                                            trackingPurchaseSuccess(false);
                                        }
                                    });
                            paymentHelper = momoBuilder.build();
                            break;
                        case APP_ZALO_KEY:
                            OnlinePaymentHelper.Builder zaloBuilder = new OnlinePaymentHelper.Builder(activity);
                            zaloBuilder.setType(1)
                                    .setSubmitUrl(responseData.getPayment_data().getSubmit_url())
                                    .setSdkParams((HashMap<String, Object>) responseData.getPayment_data().getSdk_param())
                                    .setOnlinePaymentListener(new OnlinePaymentHelper.OnlinePaymentListener() {
                                        @Override
                                        public void onSuccess() {
                                            trackingPurchaseSuccess(true);
                                            isDone = true;
                                            mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));
                                            getOrder();
                                        }

                                        @Override
                                        public void onFailed(int errorCode) {
                                            isDone = true;
                                            mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));
                                            trackingPurchaseSuccess(false);

                                            if (errorCode == ZaloPayErrorCode.USER_CANCEL.getValue()) {
                                                trackingCancelPurchase();
                                            }
                                        }
                                    });
                            paymentHelper = zaloBuilder.build();
                            break;
                        case OFFLINE_KEY:
                            break;
                    }
                }
            }
        }

        long distance = 3600L;
        if (orderModel.getCurrent_tracking() != null) {
            mBinding.tvStartTime.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(orderModel.getCurrent_tracking().getStart_date())));
            mBinding.tvEndTime.setText(Html.fromHtml(" - " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(orderModel.getCurrent_tracking().getEnd_date()))));

            if (orderModel.getCurrent_tracking().getStart_date() != null
                    && orderModel.getCurrent_tracking().getEnd_date() != null
                    && orderModel.getCurrent_tracking().getEnd_date() > 0) {
                distance = orderModel.getCurrent_tracking().getEnd_date() - orderModel.getCurrent_tracking().getStart_date();
            } else distance = 360000L;

            long duration = TimeUnit.MILLISECONDS.toSeconds(distance);
            setProgressbar(orderModel.getCurrent_tracking().getState(), duration);
        } else setProgressbar(orderModel.getState(), distance);
//        if (orderModel.getCurrent_tracking().getIndex() == 5) {
//            mBinding.rlDriver.setVisibility(View.VISIBLE);
//            mBinding.vLine.setVisibility(View.VISIBLE);
//            try {
//                mBinding.tvDriverInfo.setText(orderModel.getShipping_driver_name() + " - " + orderModel.getShipping_driver_phone());
//            } catch (Exception e) {
//
//            }
//        } else if (orderModel.getState().equals(DeliOrderModel.COMPLETED)) {
//
//        }
    }

    private void hideQuickSupport() {
        if (orderModel.getClaim() != null) {
            mBinding.llDetailReason.setVisibility(View.VISIBLE);
            mBinding.tvQuicklySupportTitle.setVisibility(View.GONE);
            mBinding.rvReason.setVisibility(View.GONE);
            mBinding.tvReason.setText(orderModel.getClaim().getReason());
        } else {
            mBinding.llDetailReason.setVisibility(View.GONE);
            mBinding.tvQuicklySupportTitle.setVisibility(View.VISIBLE);
            mBinding.rvReason.setVisibility(View.VISIBLE);

            Calendar currentTimeCal = Calendar.getInstance();
            Calendar createTimeCal = Calendar.getInstance();
            createTimeCal.setTimeInMillis(orderModel.getCreate_date());
            String startDay = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_REMAIN, String.valueOf(orderModel.getCreate_date()));
            String endDay = Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_REMAIN, String.valueOf(currentTimeCal.getTimeInMillis()));

            long difference = (currentTimeCal.getTimeInMillis() - orderModel.getCreate_date());
            if (difference > 24 * 3600 * 1000) {
                mBinding.llQuicklySupport.setVisibility(View.GONE);
            }
//            long diff = Helper.dayDiff(startDay, endDay);
//
//            if (diff > 1) {
//
//            }
        }
    }

    private void showAdapter(List<ClaimReasonModel> list) {
        mReasonAdapter = new SupportReasonAdapter(this);
        mBinding.rvReason.setVisibility(View.VISIBLE);
        mBinding.rvReason.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mBinding.rvReason.setAdapter(mReasonAdapter);
        mReasonAdapter.setItems(list);
        mReasonAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(final int pos, String action, Object object, Long count) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("reason", mReasonAdapter.getItem(pos).getMessage());

                    JSONObject objectType = new JSONObject();
                    objectType.put("type", "user_order");
                    jsonObject.put("target_object_type", objectType);

                    JSONObject objectId = new JSONObject();
                    objectId.put("id", orderModel.getUuid());
                    jsonObject.put("target_object_id", objectId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONObject jsonObject1 = jsonObject;

                final OptionDialog optionDialog = new OptionDialog(activity, false, true, false);
                optionDialog.setIDidalogEventListener(new OptionDialog.IDialogEvent() {
                    @Override
                    public void onLeftClick() {
                        optionDialog.dismiss();
                    }

                    @Override
                    public void onRightClick() {
                        DeliDataLoader.sendReasonSupport(activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                if (isSuccess) {
                                    optionDialog.dismiss();
                                    mBinding.tvQuicklySupportTitle.setVisibility(View.GONE);
                                    mBinding.rvReason.setVisibility(View.GONE);
                                    mBinding.llDetailReason.setVisibility(View.VISIBLE);
                                    mBinding.tvReason.setText(mReasonAdapter.getItem(pos).getMessage());
                                    Toast.makeText(activity, "Đã gửi yêu cầu hỗ trợ", Toast.LENGTH_SHORT).show();
                                } else {
                                    Helper.showErrorDialog(activity, (String) object);
                                }
                            }
                        }, jsonObject1);
                    }
                });
                optionDialog.show();
                optionDialog.setData("Xác nhận", "Yêu cầu Lixi hỗ trợ. Lý do:\n" + mReasonAdapter.getItem(pos).getMessage(),
                        "Bỏ qua", "Gởi", 0xff000000, 0xffff0000, false, true);

            }
        });
    }

    private void getReasons() {
        DeliDataLoader.getReasons(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    List<ClaimReasonModel> list = (List<ClaimReasonModel>) object;
                    if (list != null && list.size() > 0) {
                        showAdapter(list);
                    } else {
                        mBinding.rvReason.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void getUserProfile() {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                } else {

                }
            }
        });
    }

    private void sendSupportReason() {

    }

    private Timer timer;

    private void waitStateChangeResult() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getOrder();
            }
        };

        timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 5000);
    }

    private void getOrder() {
        mBinding.rlLoading.setVisibility(View.VISIBLE);
        mBinding.swipeRefresh.setRefreshing(true);
        DeliDataLoader.getUserOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                mBinding.swipeRefresh.setRefreshing(false);
                if (isSuccess) {
                    orderModel = (DeliOrderModel) object;
                    showData();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                    if(!isFromHistory) {
                        Crashlytics.logException(new CrashModel("ERROR!! message: " + object + "\n" + (BuildConfig.DELIVERY + ApiEntity.USER_ORDER_DETAIL + "?user_order_uuid=" + "UO" + orderId) + "\n" + new AppPreferenceHelper(activity).getUserToken() + "\n" + System.currentTimeMillis()));
                    }
                }
            }
        }, "UO" + orderId, null);
        if(!isFromHistory) {
            Crashlytics.logException(new CrashModel("GET: \n" + (BuildConfig.DELIVERY + ApiEntity.USER_ORDER_DETAIL + "?user_order_uuid=" + "UO" + orderId) + "\n" + new AppPreferenceHelper(activity).getUserToken() + "\n" + System.currentTimeMillis()));
        }
    }

    private void setProgressbar(String state, final Long duration) {
        switch (state) {
            case DeliOrderModel.PAYMENT:
                mBinding.tvMessage.setVisibility(View.GONE);
                mBinding.tvStartTime.setVisibility(View.GONE);
                mBinding.tvEndTime.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                mBinding.tvMessageEvent.setVisibility(View.VISIBLE);
                if (!isDone && responseData != null)
                    mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_waiting, activity));
                else
                    mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity));

                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);

                if (orderModel.getCurrent_tracking().getEnd_date() == 0)
                    mBinding.tvEndTime.setText("....");

                mBinding.sb1.setMax(duration.intValue());

                if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getStart_date()) {
                    if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getEnd_date()) {
                        mBinding.sb1.setProgress(mBinding.sb1.getMax());
                        return;
                    }
                    Long progress = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - orderModel.getCurrent_tracking().getStart_date());
                    progressInt = progress.intValue();
                }
                runnable1.run();
                break;
            case DeliOrderModel.SUBMITTED:
                trackingOrderComplete();
                HashMap<String, Integer> countMap = Helper.getRepayCount(this, "REPAY_FILE");
                if (countMap.containsKey(orderModel.getUuid())) {
                    countMap.remove(orderModel.getUuid());
                    Helper.saveProductToFile(countMap, this, "REPAY_FILE");
                }
                mBinding.tvStartTime.setVisibility(View.VISIBLE);
                mBinding.tvEndTime.setVisibility(View.VISIBLE);
                mBinding.tvMessageEvent.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);


                if (orderModel.getCurrent_tracking().getEnd_date() == 0)
                    mBinding.tvEndTime.setText("....");

                mBinding.sb1.setMax(duration.intValue());
                if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getStart_date()) {
                    if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getEnd_date()) {
                        mBinding.sb1.setProgress(mBinding.sb1.getMax());
                        return;
                    }
                    Long progress = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - orderModel.getCurrent_tracking().getStart_date());
                    progressInt = progress.intValue();
                }
                runnable1.run();
                break;
            case DeliOrderModel.CONFIRMED:
                handler.removeCallbacks(runnable1);

                mBinding.sb1.setProgress(mBinding.sb1.getMax());
                mBinding.sb2.setMax(duration.intValue());

                mBinding.tvStartTime.setVisibility(View.VISIBLE);
                mBinding.tvEndTime.setVisibility(View.VISIBLE);
                mBinding.tvMessageEvent.setVisibility(View.GONE);
                mBinding.tvMessage.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);


                if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getStart_date()) {
                    if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getEnd_date()) {
                        mBinding.sb2.setProgress(mBinding.sb2.getMax());
                        return;
                    }
                    Long progress = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - orderModel.getCurrent_tracking().getStart_date());
                    progressInt = progress.intValue();
                }
                runnable2.run();
                break;
            case DeliOrderModel.ASSIGNED:
                handler.removeCallbacks(runnable2);

                mBinding.tvStartTime.setVisibility(View.VISIBLE);
                mBinding.tvEndTime.setVisibility(View.VISIBLE);
                mBinding.tvMessageEvent.setVisibility(View.GONE);
                mBinding.tvMessage.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);


                mBinding.sb1.setProgress(mBinding.sb1.getMax());
                mBinding.sb2.setProgress(mBinding.sb2.getMax());
                mBinding.sb3.setMax(duration.intValue());

                if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getStart_date()) {
                    if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getEnd_date()) {
                        mBinding.sb3.setProgress(mBinding.sb3.getMax());
                        return;
                    }
                    Long progress = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - orderModel.getCurrent_tracking().getStart_date());
                    progressInt = progress.intValue();
                }
                runnable3.run();
                break;
            case DeliOrderModel.PICKED:
                handler.removeCallbacks(runnable3);
                mBinding.sb1.setProgress(mBinding.sb1.getMax());
                mBinding.sb2.setProgress(mBinding.sb2.getMax());
                mBinding.sb3.setProgress(mBinding.sb3.getMax());
                mBinding.sb4.setMax(duration.intValue());

                mBinding.tvStartTime.setVisibility(View.VISIBLE);
                mBinding.tvEndTime.setVisibility(View.VISIBLE);
                mBinding.tvMessageEvent.setVisibility(View.GONE);
                mBinding.tvMessage.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);


                mBinding.tvDriverInfo.setText(Html.fromHtml(orderModel.getShipping_driver_name() + " - " + orderModel.getShipping_driver_phone()));
                if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getStart_date()) {
                    if (System.currentTimeMillis() > orderModel.getCurrent_tracking().getEnd_date()) {
                        mBinding.sb4.setProgress(mBinding.sb4.getMax());
                        return;
                    }
                    Long progress = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - orderModel.getCurrent_tracking().getStart_date());
                    progressInt = progress.intValue();
                }

                runnable4.run();
                break;
            case DeliOrderModel.COMPLETED:
                handler.removeCallbacks(runnable4);

                mBinding.tvMessageEvent.setVisibility(View.VISIBLE);
                mBinding.tvMessageEvent.setText(LanguageBinding.getString(R.string.deli_tracking_send_feedback, activity));
                mBinding.tvMessage.setVisibility(View.GONE);
                mBinding.tvSupport.setVisibility(View.GONE);
                mBinding.tvStartTime.setVisibility(View.GONE);
                mBinding.tvEndTime.setVisibility(View.GONE);
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.vLine.setVisibility(View.GONE);
                mBinding.llPaid.setVisibility(View.GONE);
                if (orderModel.getCurrent_tracking() != null)
                    mBinding.tvStatus.setText(orderModel.getCurrent_tracking().getText());
                else mBinding.tvStatus.setText(state);


                mBinding.sb1.setProgress(mBinding.sb1.getMax());
                mBinding.sb2.setProgress(mBinding.sb2.getMax());
                mBinding.sb3.setProgress(mBinding.sb3.getMax());
                mBinding.sb4.setProgress(mBinding.sb4.getMax());

                if (orderModel.getFeedback() != null) {
                    mBinding.tvMessageEvent.setVisibility(View.GONE);
                    mBinding.ratingBar.setVisibility(View.VISIBLE);
                    mBinding.ratingBar.setRating(orderModel.getFeedback().getRating());
                    mBinding.llQuicklySupport.setVisibility(View.GONE);
                }

                SQLiteDatabaseHandler sqLiteDatabaseHandler = SQLiteDatabaseHandler.getInstance(activity);
                if (!sqLiteDatabaseHandler.isExist(orderModel.getReceive_address())) {
                    sqLiteDatabaseHandler.addAddress(new DeliAddressModel(orderModel.getReceive_address(),
                            orderModel.getReceive_address().substring(0, orderModel.getReceive_address().contains(",") ? orderModel.getReceive_address().indexOf(",") : orderModel.getReceive_address().length()),
                            orderModel.getReceive_lat(), orderModel.getReceive_lng()));
                }
                break;
            case DeliOrderModel.CANCELED:
                mBinding.rlDriver.setVisibility(View.GONE);
                mBinding.tvSupport.setVisibility(View.GONE);
                mBinding.tvStartTime.setVisibility(View.GONE);
                mBinding.tvEndTime.setVisibility(View.GONE);
                mBinding.tvMessageEvent.setVisibility(View.GONE);
                mBinding.llProgress.setVisibility(View.GONE);
                mBinding.tvMessage.setVisibility(View.VISIBLE);
                mBinding.llPaid.setVisibility(View.GONE);
                mBinding.vLineInCard.setVisibility(View.VISIBLE);
                mBinding.tvStatus.setText(LanguageBinding.getString(R.string.deli_tracking_cancel_order, activity));
                mBinding.tvMessage.setText(Html.fromHtml("Lý do: " + orderModel.getClose_reason_message()));
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FeedbackActivity.REQUEST_FEEDBACK_CODE) {
            if (resultCode == RESULT_OK) {
                isDone = true;
                getOrder();
            }
        } else paymentHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String id = intent.getExtras().getString("uuid", "");
        if (!id.equals(orderId)) {
            finish();
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        //For tracking id
        id = Long.valueOf(orderId);
        super.onResume();
        connectSocket();
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.ivClose) {
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if(HomeActivity.getInstance() != null) {
                HomeActivity.getInstance().tab = 1;
            }
            startActivity(intent);
            finish();
        } else if (view == mBinding.tvSupport) {
            if (orderModel != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("order", orderModel);
                Intent intent = new Intent(activity, SupportOrderActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else if (view == mBinding.rlStore) {
            if (orderModel != null)
                StoreDetailActivity.startStoreDetailActivity(activity, orderModel.getStore_id(), null);
        } else if (view == mBinding.tvHotline) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + mBinding.tvHotline.getText().toString().trim()));
            startActivity(callIntent);
        } else if (view == mBinding.tvMessageEvent) {
            if (mBinding.tvMessageEvent.getText().toString().equalsIgnoreCase(LanguageBinding.getString(R.string.deli_tracking_payment_again, activity))) {
                trackingRepayOrder();
                HashMap<String, Integer> countMap = Helper.getRepayCount(activity, "REPAY_FILE");
                int count = 0;
                if (countMap.containsKey(orderModel.getUuid()))
                    count = countMap.get(orderModel.getUuid());
                countMap.put(orderModel.getUuid(), ++count);
                Helper.saveProductToFile(countMap, activity, "REPAY_FILE");

                OrderInfoActivity.startOrderCheckoutActivity(activity,
                        orderModel.getUuid(), orderModel.getStore_id(), null);
                finish();
            } else if (mBinding.tvMessageEvent.getText().toString().equalsIgnoreCase(LanguageBinding.getString(R.string.deli_tracking_send_feedback, activity))) {
                Intent intent = new Intent(activity, FeedbackActivity.class);
                intent.putExtra("uuid", orderModel.getUuid());
                startActivityForResult(intent, FeedbackActivity.REQUEST_FEEDBACK_CODE);
            }
        } else if (view == mBinding.ivMessenger) {
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
            } catch (Exception e) {
                Toast.makeText(activity, LanguageBinding.getString(R.string.profile_setting_notify_error, activity), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(HomeActivity.getInstance() != null) {
            HomeActivity.getInstance().tab = 1;
        }
        activity.startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSocket != null)
            mSocket.disconnect();

        handler.removeCallbacks(runnable1);
        handler.removeCallbacks(runnable2);
        handler.removeCallbacks(runnable3);
        handler.removeCallbacks(runnable4);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_ORDER_TRACKING;
    }

    //region RUNNABLE
    private Handler handler = new Handler();

    private Runnable runnable1 = new Runnable() {
        @Override
        public void run() {
            try {
                updateProgress(mBinding.sb1, this);
            } catch (Exception ignored) {

            } finally {
                handler.postDelayed(runnable1, 970);
            }
        }
    };

    private Runnable runnable2 = new Runnable() {
        @Override
        public void run() {
            try {
                updateProgress(mBinding.sb2, this);
            } catch (Exception ignored) {

            } finally {
                handler.postDelayed(runnable2, 970);
            }
        }
    };

    private Runnable runnable3 = new Runnable() {
        @Override
        public void run() {
            try {
                updateProgress(mBinding.sb3, this);
            } catch (Exception ignored) {

            } finally {
                handler.postDelayed(runnable3, 970);
            }
        }
    };

    private Runnable runnable4 = new Runnable() {
        @Override
        public void run() {
            try {
                updateProgress(mBinding.sb4, this);
            } catch (Exception ignored) {

            } finally {
                handler.postDelayed(runnable4, 970);
            }
        }
    };

    //endregion

    // region tracking
    // 10
    private void trackingPurchaseSuccess(boolean isSuccess) {
        String coupon = "";
        if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() != 0) {
            coupon = TrackingConstant.CouponType.PROMOTION_CODE;
        }
        GlobalTracking.trackPurchaseComplete(
                TrackingConstant.ContentType.STORE,
                orderModel.getStore_id(),
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                orderModel.getPayment_method().getMethodTracking(),
                coupon,
                isSuccess,
                orderModel.getUuid(),
                orderModel.getMoney(),
                TrackingConstant.ContentSource.ORDER_TRACKING
        );
    }

    // 11
    private void trackingOrderComplete() {
        String coupon = "";
        if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() != 0) {
            coupon = TrackingConstant.CouponType.PROMOTION_CODE;
        }

        GlobalTracking.trackOrderComplete(
                TrackingConstant.ContentType.STORE,
                orderModel.getStore_id(),
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                orderModel.getPayment_method().getMethodTracking(),
                coupon,
                true,
                orderModel.getUuid(),
                orderModel.getMoney(),
                TrackingConstant.ContentSource.ORDER_TRACKING
        );
    }

    // 12
    private void trackingCancelPurchase() {
        String coupon = "";
        if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() != 0) {
            coupon = TrackingConstant.CouponType.PROMOTION_CODE;
        }

        GlobalTracking.trackCancelPurchase(
                TrackingConstant.ContentType.STORE,
                orderModel.getStore_id(),
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                orderModel.getPayment_method().getMethodTracking(),
                coupon,
                orderModel.getUuid(),
                orderModel.getMoney(),
                TrackingConstant.ContentSource.ORDER_TRACKING
        );
    }

    // 13
    private void trackingRepayOrder() {
        String coupon = "";
        if (orderModel.getList_promotion_code_apply() != null && orderModel.getList_promotion_code_apply().size() != 0) {
            coupon = TrackingConstant.CouponType.PROMOTION_CODE;
        }

        GlobalTracking.trackRepayOrder(
                TrackingConstant.ContentType.STORE,
                orderModel.getStore_id(),
                (int) orderModel.getProduct_count(),
                true,
                TrackingConstant.Currency.VND,
                orderModel.getPayment_method().getMethodTracking(),
                coupon,
                orderModel.getUuid(),
                orderModel.getMoney(),
                TrackingConstant.ContentSource.ORDER_TRACKING
        );
    }
    // endregion
}
