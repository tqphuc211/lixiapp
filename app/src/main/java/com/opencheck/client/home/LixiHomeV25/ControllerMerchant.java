package com.opencheck.client.home.LixiHomeV25;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiHomeMerchantItemBinding;
import com.opencheck.client.home.LixiHomeV25.CategoryContent.ActivityCategoryDetail;
import com.opencheck.client.home.LixiHomeV25.Search.SearchingActivity;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

public class ControllerMerchant {

    private LixiActivity activity;
    private ViewGroup parentView;
    private View rootView;

    private View v_space;
    private ImageView iv_logo;
    private TextView tv_name;
    private TextView tv_voucher_count;
    private TextView tv_store_count;
    private TextView tv_lixi_count;

    MerchantModel merchantModel;

    private int pos;
    private String source;

    public ControllerMerchant(final LixiActivity activity, ViewGroup parentView, final MerchantModel merchantModel, int pos) {
        this.activity = activity;
        this.parentView = parentView;
        this.merchantModel = merchantModel;
        this.pos = pos;
        LixiHomeMerchantItemBinding lixiHomeMerchantItemBinding =
                LixiHomeMerchantItemBinding.inflate(LayoutInflater.from(activity),
                        parentView, false);
        rootView = lixiHomeMerchantItemBinding.getRoot();
        parentView.addView(rootView);

        findViews();
        if (pos == 0)
            v_space.setVisibility(View.GONE);

        ImageLoader.getInstance().displayImage(merchantModel.getLogo(), iv_logo, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(merchantModel.getName());
        tv_voucher_count.setText(String.format(LanguageBinding.getString(R.string.promotion, activity), merchantModel.getTotal_evoucher_count()));
        tv_lixi_count.setText("+" + Helper.getVNCurrency(merchantModel.getTotal_cashback_price()) + " lixi");
        tv_store_count.setText(merchantModel.getCount_store() + " cửa hàng");

        if (activity instanceof SearchingActivity) {
            source = TrackingConstant.ContentSource.SEARCH;
        } else if (activity instanceof ActivityCategoryDetail) {
            source = TrackingConstant.ContentSource.VOUCHER_CATEGORY;
        }

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MerchantDetailActivity.startMerchantDetailActivity(activity, merchantModel.getId(), source);
            }
        });
    }

    private void findViews() {
        v_space = rootView.findViewById(R.id.v_space);
        iv_logo = (ImageView) rootView.findViewById(R.id.iv_logo);
        tv_name = (TextView) rootView.findViewById(R.id.tv_name);
        tv_voucher_count = (TextView) rootView.findViewById(R.id.tv_voucher_count);
        tv_store_count = (TextView) rootView.findViewById(R.id.tv_store_count);
        tv_lixi_count = (TextView) rootView.findViewById(R.id.tv_lixi_count);
    }

}
