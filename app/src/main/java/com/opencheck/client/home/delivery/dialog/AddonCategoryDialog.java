package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogAddonCategoryBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.adapter.AddonCategoryAdapter;
import com.opencheck.client.home.delivery.libs.sticky_recycler_view.StickyHeaderLayoutManager;
import com.opencheck.client.home.delivery.libs.drag.DraggingLayout;
import com.opencheck.client.home.delivery.model.AddonCategoryModel;
import com.opencheck.client.home.delivery.model.AddonModel;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.HashMap;

public class AddonCategoryDialog extends LixiDialog {

    public AddonCategoryDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackground, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackground, _titleBar);
    }

    private DialogAddonCategoryBinding mBinding;
    private boolean isHeaderShowed = false;
    private ProductModel mProductModel;
    private int mProductQuantity = 1;
    private long mTotal = 0L;
    private AddonCategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_addon_category, null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    public void setData(ProductModel productModel) {
        this.mProductModel = productModel;
        mBinding.tvName.setText(mProductModel.getName());
        mBinding.tvTitle.setText(mProductModel.getName());
        mBinding.tvPrice.setText(Helper.getVNCurrency(mProductModel.getPrice()) + "đ");

        if (mProductModel.getDescription() != null && mProductModel.getDescription().length() > 0) {
            mBinding.tvDescription.setVisibility(View.VISIBLE);
            mBinding.tvDescription.setText(mProductModel.getDescription());
        }

        if (mProductModel.getImage_link() != null)
            ImageLoader.getInstance().displayImage(mProductModel.getImage_link(), mBinding.ivMerchant, LixiApplication.getInstance().optionsNomal);

        categoryAdapter = new AddonCategoryAdapter(activity, mProductModel.getList_addon_category());
        mBinding.rcvOption.setLayoutManager(new StickyHeaderLayoutManager());
        mBinding.rcvOption.setAdapter(categoryAdapter);
        categoryAdapter.setAddonCallback(new AddonCategoryAdapter.AddonCallback() {
            @Override
            public void onUpdate() {
                updateMoney();
            }

            @Override
            public void onError(AddonCategoryModel categoryModel, int sectionIndex) {
                try {
                    mBinding.rcvOption.smoothScrollToPosition(sectionIndex == 0 ? categoryAdapter.getItemTotalBeforeSection(sectionIndex) : categoryAdapter.getItemTotalBeforeSection(sectionIndex) + 1);
                } catch (IndexOutOfBoundsException e) {
                    Crashlytics.logException(new CrashModel(sectionIndex + ""));
                }
                Toast.makeText(activity,
                        "Số lượng tối thiếu cần cho nhóm " + categoryModel.getName() + " là " + categoryModel.getMin_total_quantity(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCompleted() {
                addProductToCart();
            }
        });

        updateMoney();
    }

    private void initView() {
        mBinding.dragLayout.setPanelHeight(2 * activity.heightScreen / 3);
        mBinding.dragLayout.setDragCallback(new DraggingLayout.DragCallback() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (!isHeaderShowed && slideOffset == 1.0) {
                    slideToBottom(mBinding.rlBack, activity.getResources().getDimension(R.dimen.value_46));
                    isHeaderShowed = true;
                } else if (isHeaderShowed) {
                    isHeaderShowed = false;
                    slideToTop(mBinding.rlBack, activity.getResources().getDimension(R.dimen.value_46));
                }
            }

            @Override
            public void onPanelStateChanged(View panel, DraggingLayout.PanelState previousState, DraggingLayout.PanelState newState) {

            }
        });

        mBinding.rlAdd.setOnClickListener(this);
        mBinding.ivSub.setOnClickListener(this);
        mBinding.ivAdd.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.tvDismiss.setOnClickListener(this);
        mBinding.vHeader.setOnClickListener(this);
    }

    private void slideToBottom(View view, float distance) {
        view.setTranslationY(-distance);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, distance);
        animate.setDuration(300);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    private void slideToTop(View view, float distance) {
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, -distance);
        animate.setDuration(300);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    private void updateMoney() {
        int temp = 0;
        for (int j = 0; j < mProductModel.getList_addon_category().size(); j++) {
            String category = mProductModel.getList_addon_category().get(j).getName();
            if (categoryAdapter.getMapCurrentAddons().containsKey(category)) {
                for (int i = 0; i < categoryAdapter.getMapCurrentAddons().get(category).size(); i++) {
                    if (categoryAdapter.getMapCurrentAddons().get(category).get(i).getDefault_value() != null) {
                        Double dObj = Double.valueOf(String.valueOf(categoryAdapter.getMapCurrentAddons().get(category).get(i).getDefault_value()));
                        if (dObj.intValue() > 0)
                            temp += categoryAdapter.getMapCurrentAddons().get(category).get(i).getPrice_modifier() * dObj.intValue();
                    } else
                        temp += categoryAdapter.getMapCurrentAddons().get(category).get(i).getPrice_modifier();
                }
            }
        }

        long total = (mProductModel.getPrice() + temp) * mProductQuantity;
        mTotal = mProductModel.getPrice() + temp;
        mBinding.tvCount.setText("" + mProductQuantity);
        mBinding.tvTotal.setText(LanguageBinding.getString(R.string.product_adding_to_cart, activity) + " - " + Helper.getVNCurrency(total) + "đ");
    }

    private int getRequiredAddonQuantity() {
        int count = 0;
        for (int i = 0; i < mProductModel.getList_addon_category().size(); i++) {
            if (mProductModel.getList_addon_category().get(i).isRequired())
                count++;
        }
        return count;
    }

    private void handleAddingToCartEvent() {
        if ((StoreDetailActivity.isWatch)) {
            Toast.makeText(activity, "Sản phẩm không được thêm vào giỏ hàng. Vui lòng quay lại sau.", Toast.LENGTH_SHORT).show();
            return;
        }
        categoryAdapter.setAddingToCartState(true);
        categoryAdapter.notifyDataSetChanged();

        // Kiểm tra các nhóm addon bắt buộc đã được chọn hết chưa ?
        if (getRequiredAddonQuantity() == categoryAdapter.getMapRequiredAddon().size()) {
            // Kiểm tra các nhóm addon đã chọn đã thỏa mản yêu cầu chưa ?
            // Nếu số lượng hiện tại của addon nào nhỏ hơn min_total
            categoryAdapter.checkValidProductQuantity();
        } else {
            final int position = categoryAdapter.getPositionRequiredSection();
            if (position != -1 && position <= categoryAdapter.getItemCount()) {
                mBinding.rcvOption.post(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.rcvOption.smoothScrollToPosition(position);
                    }
                });

            }
        }
    }

    private void addProductToCart() {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);

        if (!cart.containsKey("" + mProductModel.getStore_id()) || cart.get("" + mProductModel.getStore_id()) == null) {
            CartModel cartModel = new CartModel();
            HashMap<String, ArrayList<ProductModel>> productList = new HashMap<>();
            cartModel.setId(mProductModel.getStore_id());
            cartModel.setProduct_list(productList);
            cart.put("" + mProductModel.getStore_id(), cartModel);
        }

        ArrayList<AddonModel> currentAddons = getAddons();

        CartModel cartModel = cart.get("" + mProductModel.getStore_id());

        if (cartModel.getProduct_list() != null
                && cartModel.getProduct_list().containsKey("" + mProductModel.getId())) {

            ArrayList<ProductModel> tempList = cartModel.getProduct_list().get("" + mProductModel.getId());
            // Kiểm tra tập addon mới đã tồn tại trong product
            boolean isExist = false;
            for (int i = 0; i < tempList.size(); i++) {
                ArrayList<AddonModel> addons = tempList.get(i).getList_addon();

                if (addons.size() != currentAddons.size())
                    isExist = false;
                else {
                    boolean isSame = true;
                    for (int j = 0; j < addons.size(); j++) {

                        if (addons.get(j).getId() != currentAddons.get(j).getId()) {
                            isSame = false;
                            break;
                        }

                        if (currentAddons.get(j).getDefault_value() != null) {
                            int t1 = Double.valueOf(String.valueOf(currentAddons.get(j).getDefault_value())).intValue();
                            int t2 = Double.valueOf(String.valueOf(addons.get(j).getDefault_value())).intValue();
                            if (t1 != t2) {
                                isSame = false;
                                break;
                            }
                        }
                    }

                    int curQuantity = cartModel.getProduct_list().get("" + mProductModel.getId()).get(i).getQuantity();
                    if (isSame) {
                        isExist = true;
                        cartModel.getProduct_list().get("" + mProductModel.getId()).get(i).setQuantity(curQuantity + mProductQuantity);
                        break;
                    } else {
                        isExist = false;
                    }

                }
            }

            if (!isExist) {
                ProductModel productModel = new ProductModel();
                productModel.setName(mProductModel.getName());
                productModel.setId(mProductModel.getId());
                productModel.setList_addon(currentAddons);
                productModel.setHave_addon(true);
                productModel.setQuantity(mProductQuantity);
                productModel.setImage_link(mProductModel.getImage_link());
                productModel.setPrice(mTotal);
                productModel.setNote("");
                StringBuilder builder = new StringBuilder();
                for (int k = 0; k < currentAddons.size(); k++) {
                    builder.append(currentAddons.get(k).getId());
                    if (currentAddons.get(k).getDefault_value() != null)
                        builder.append(currentAddons.get(k).getDefault_value().toString());
                }
                productModel.setIndentifyAddon(builder.toString());
                cartModel.getProduct_list().get("" + mProductModel.getId()).add(productModel);
            }

        } else {
            ArrayList<ProductModel> res = new ArrayList<>();
            ProductModel productModel = new ProductModel();
            productModel.setId(mProductModel.getId());
            productModel.setName(mProductModel.getName());
            productModel.setQuantity(mProductQuantity);
            productModel.setHave_addon(true);
            productModel.setImage_link(mProductModel.getImage_link());
            productModel.setPrice(mTotal);
            productModel.setList_addon(currentAddons);
            StringBuilder builder = new StringBuilder();
            for (int k = 0; k < currentAddons.size(); k++) {
                builder.append(currentAddons.get(k).getId());
                if (currentAddons.get(k).getDefault_value() != null)
                    builder.append(currentAddons.get(k).getDefault_value());
            }
            productModel.setIndentifyAddon(builder.toString());

            res.add(productModel);

            if (cartModel.getProduct_list() == null) {
                HashMap<String, ArrayList<ProductModel>> productList = new HashMap<>();
                cartModel.setProduct_list(productList);
            }
            cartModel.getProduct_list().put(mProductModel.getId() + "", res);
        }

        cart.put("" + mProductModel.getStore_id(), cartModel);
        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
        eventListener.onOk(mProductQuantity);
        dismiss();
    }

    private ArrayList<AddonModel> getAddons() {
        ArrayList<AddonModel> res = new ArrayList<>();
        for (int i = 0; i < mProductModel.getList_addon_category().size(); i++) {
            if (categoryAdapter.getMapCurrentAddons().containsKey(mProductModel.getList_addon_category().get(i).getName())) {
                res.addAll(categoryAdapter.getMapCurrentAddons().get(mProductModel.getList_addon_category().get(i).getName()));
            }
        }
        return res;
    }

    private void handleUpdateProductQuantityEvent(boolean isIncrease) {
        if (isIncrease) {
            mProductQuantity++;
        } else {
            if (mProductQuantity > 1)
                mProductQuantity--;
        }

        updateMoney();
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.ivSub) {
            handleUpdateProductQuantityEvent(false);
        } else if (view == mBinding.ivAdd) {
            handleUpdateProductQuantityEvent(true);
        } else if (view == mBinding.rlAdd) {
            handleAddingToCartEvent();
        } else if (view == mBinding.ivBack || view == mBinding.tvDismiss || view == mBinding.vHeader) {
            dismiss();
        }
    }

    public interface IDialogEvent {
        void onOk(int quantity);
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(IDialogEvent listener) {
        eventListener = listener;
    }
}