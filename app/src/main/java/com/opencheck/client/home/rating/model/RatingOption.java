package com.opencheck.client.home.rating.model;

import com.opencheck.client.models.LixiModel;

public class RatingOption extends LixiModel{

    /**
     * id : 0
     * image : string
     * key : string
     * priority : 0
     * status : string
     * value : string
     */

    private int id;
    private String image;
    private String key;
    private int priority;
    private String status;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
