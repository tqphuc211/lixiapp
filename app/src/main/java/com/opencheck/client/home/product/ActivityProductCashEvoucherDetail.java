package com.opencheck.client.home.product;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.ProductEvoucherDetailActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodBody;
import com.opencheck.client.home.account.evoucher.Model.PaymentCodModel;
import com.opencheck.client.home.flashsale.Dialog.FlashSaleTimeOutDialog;
import com.opencheck.client.home.flashsale.Global.FlashSaleDetailController;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.merchant.StoreController;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.home.product.Control.CheckSecurity;
import com.opencheck.client.home.product.bank_card.PopupChooseNewCard;
import com.opencheck.client.home.product.bank_card.PopupChooseSavedCard;
import com.opencheck.client.home.product.bank_card.PopupNoSavedCard;
import com.opencheck.client.home.product.buyatstore.DialogBuyAtStore;
import com.opencheck.client.home.product.cart_new.PopupCart;
import com.opencheck.client.home.product.cart_new.PopupPayment;
import com.opencheck.client.home.product.cart_old.ConfirmBuyCashEvouherDialog;
import com.opencheck.client.home.product.cart_old.FailedCashDialog;
import com.opencheck.client.home.product.cart_old.SuccessCashDialog;
import com.opencheck.client.home.product.cod.PaymentCODSuccessDialog;
import com.opencheck.client.home.product.communicate.OnCreateNewCard;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;
import com.opencheck.client.home.product.communicate.OnUseSavedCard;
import com.opencheck.client.home.product.gateway.DialogTransactions;
import com.opencheck.client.home.product.gateway.result.PaymentAtBankFailDialog;
import com.opencheck.client.home.product.gateway.result.PaymentAtBankSuccessDialog;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.home.rating.adapter.RatingAdapter;
import com.opencheck.client.home.rating.dialog.AllRatingDialog;
import com.opencheck.client.home.rating.dialog.LixiGiftDialog;
import com.opencheck.client.home.rating.dialog.ReviewRatingDialog;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.PaymentTypeModel;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperModel;
import com.opencheck.client.models.lixishop.BankModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.momo.momo_partner.AppMoMoLib;
import vn.zalopay.sdk.ZaloPayErrorCode;
import vn.zalopay.sdk.ZaloPayListener;
import vn.zalopay.sdk.ZaloPaySDK;

public class ActivityProductCashEvoucherDetail extends LixiActivity {

    //region FACTORY
    private static ActivityProductCashEvoucherDetail detailActivity;

    public static final String PRODUCT_ID = "PRODUCT_ID";

    public static final String IS_COMBO_ITEM = "IS_COMBO_ITEM";

    public static String getParamIsComboItem(boolean isComboItem) {
        return IS_COMBO_ITEM + ":" + (isComboItem ? "1" : "0");
    }

    public static void startActivityProductCashEvoucherDetail(Context context, long id, String source, String... param) {
        Intent intent = new Intent(context, ActivityProductCashEvoucherDetail.class);
        intent.putExtra(ActivityProductCashEvoucherDetail.PRODUCT_ID, id);
        intent.putExtra(GlobalTracking.SOURCE, source);
        if (param.length > 0) {
            for (int i = 0; i < param.length; i++) {
                String key = param[i].substring(0, param[i].indexOf(":"));
                String value = param[i].substring(param[i].indexOf(":") + 1, param[i].length());

                if (key.equals(IS_COMBO_ITEM)) {
                    intent.putExtra(IS_COMBO_ITEM, value.equals("1"));
                }

                if (key.equals(FlashSaleDetailController.BUY_NOW)) {
                    intent.putExtra(FlashSaleDetailController.BUY_NOW, value.equals("1"));
                }
            }
        }
        context.startActivity(intent);
    }

    public static Intent getCallingIntent(Context context, long id, String source, String... param) {
        Intent intent = new Intent(context, ActivityProductCashEvoucherDetail.class);
        intent.putExtra(ActivityProductCashEvoucherDetail.PRODUCT_ID, id);
        intent.putExtra(GlobalTracking.SOURCE, source);
        if (param.length > 0) {
            for (String aParam : param) {
                String key = aParam.substring(0, aParam.indexOf(":"));
                String value = aParam.substring(aParam.indexOf(":") + 1, aParam.length());

                if (key.equals(IS_COMBO_ITEM)) {
                    intent.putExtra(IS_COMBO_ITEM, value.equals("1"));
                }

                if (key.equals(FlashSaleDetailController.BUY_NOW)) {
                    intent.putExtra(FlashSaleDetailController.BUY_NOW, value.equals("1"));
                }
            }
        }
        return intent;
    }

    public static void finishCashVoucherDetail() {
        if (detailActivity != null) {
            detailActivity.finish();
        }
    }
    //endregion

    //region VARIABLE
    //region variable VIEW
    private NestedScrollView scroll_view;
    private RelativeLayout rl_viewpager;
    private ViewPager viewpager;
    private CirclePageIndicator indicator;

    private TextView tv_name;
    private TextView txtRating;
    private com.opencheck.client.home.delivery.libs.RatingBar ratingBar;
    //    private RelativeLayout rl_remain;
    private TextView tv_remain;
    private TextView tv_buyer;
    private View v_line_name;

    //    private LinearLayout ll_price;
    private TextView tv_price;
    private TextView tv_old_price;
    private TextView tv_percent_discount;
    //    private TextView tv_old_price_r;
//    private TextView tv_percent_discount_r;
//    private LinearLayout ll_lixi_cashback;
//    private TextView tv_lixi_cashback;
    private View v_line_price;

    private LinearLayout ll_flashsale;

    private TextView tv_expire_day;
    private LinearLayout ll_detail_combo;
    private TextView tv_list_combo;
    private RecyclerView rv_combo;
    private LinearLayout ll_place;
    private LinearLayout ll_merchant;
    private CircleImageView iv_merchant;
    private TextView tv_name_merchant;
    private TextView tv_store_count;
    private LinearLayout ln_one_store;
    private TextView tv_address;
    private TextView tv_sdt;
    private LinearLayout ll_list_store;
    private LinearLayout ll_condition;
    private TextView tv_condition_content;
    //    private WebView wv_product_details;
    private TextView txtReviewContent;
    private TextView txtShowDetail;
    private RelativeLayout rl_buy;
    private TextView tv_buy;
    private View v_back;
    private ImageView iv_back;
    private TextView tv_title;
    private ImageView iv_share;
    private ConstraintLayout constraintLixi;
    private ConstraintLayout constraintPrice;
    private LinearLayout linearUserBought;
    private TextView txtLixi;
    private LinearLayout linearRating;
    private RecyclerView recListRating;
    private TextView txtShowAll;
    private FrameLayout frameRating;

    private void findView() {
        scroll_view = findViewById(R.id.scroll_view);
        rl_viewpager = findViewById(R.id.rl_viewpager);
        viewpager = findViewById(R.id.viewpager);
        indicator = findViewById(R.id.indicator);

        tv_name = findViewById(R.id.tv_name);
        txtRating = findViewById(R.id.txtRating);
        ratingBar = findViewById(R.id.ratingBar);

//        rl_remain = findViewById(R.id.rl_remain);
        tv_remain = findViewById(R.id.tv_remain);
        tv_buyer = findViewById(R.id.tv_buyer);

        v_line_name = findViewById(R.id.v_line_name);

//        ll_price = findViewById(R.id.ll_price);
        tv_price = findViewById(R.id.tv_price);
        tv_old_price = findViewById(R.id.tv_old_price);
        tv_percent_discount = findViewById(R.id.tv_percent_discount);
        v_line_price = findViewById(R.id.v_line_price);

        ll_flashsale = findViewById(R.id.ll_flashsale);

        tv_expire_day = findViewById(R.id.tv_expire_day);
        ll_detail_combo = findViewById(R.id.ll_detail_combo);
        tv_list_combo = findViewById(R.id.tv_list_combo);
        rv_combo = findViewById(R.id.rv_combo);
        ll_place = findViewById(R.id.ll_place);
        ll_merchant = findViewById(R.id.ll_merchant);
        iv_merchant = findViewById(R.id.iv_merchant);
        tv_name_merchant = findViewById(R.id.tv_name_merchant);
        tv_store_count = findViewById(R.id.tv_store_count);
        ln_one_store = findViewById(R.id.ln_one_store);
        tv_address = findViewById(R.id.tv_address);
        tv_sdt = findViewById(R.id.tv_sdt);
        ll_list_store = findViewById(R.id.ll_list_store);
        ll_condition = findViewById(R.id.ll_condition);
        tv_condition_content = findViewById(R.id.tv_condition_content);
//        wv_product_details = (WebView) findViewById(R.id.wv_product_details);
        txtReviewContent = findViewById(R.id.txtReviewContent);
        txtShowDetail = findViewById(R.id.txtShowDetail);
        rl_buy = findViewById(R.id.rl_buy);
        tv_buy = findViewById(R.id.tv_buy);
        v_back = findViewById(R.id.v_back);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        iv_share = findViewById(R.id.iv_share);
        constraintLixi = findViewById(R.id.constraintLixi);
        constraintPrice = findViewById(R.id.constraintPrice);
        linearUserBought = findViewById(R.id.linearUserBought);
        txtLixi = findViewById(R.id.txtLixi);
        linearRating = findViewById(R.id.linearRating);
        recListRating = findViewById(R.id.recListRating);
        txtShowAll = findViewById(R.id.txtShowAll);
        frameRating = findViewById(R.id.frameRating);

        iv_back.setOnClickListener(this);
        iv_share.setOnClickListener(this);
        ll_merchant.setOnClickListener(this);
        tv_buy.setOnClickListener(this);
        txtShowDetail.setOnClickListener(this);
        txtShowAll.setOnClickListener(this);
        constraintLixi.setOnClickListener(this);

        COLOR_YELLOW = Color.parseColor("#ABA617");
        COLOR_DARK = Color.parseColor("#9B9B9B");
    }
    //endregion

    //region variable ADAPTER
    private AdapterImageEvoucherDetail adapterImageProductDetail;
    private AdapterComboProduct adapterDetailCombo;
    private FlashSaleDetailController flashSaleView;
    //endregion

    //region variable DATA
    private long idProduct;
    private ProductCashVoucherDetailModel productModel;

    private boolean isCombo = false;
    private boolean isComboItem = false;
    private boolean isProductLixiPayment = false;
    private boolean isFlashSale = false;
    private boolean isBuyNow = false;
    private String source = "";
    //endregion
    //endregion

    private final String ACTION_CHOOSE_CARD = "action choose card";
    private final String ACTION_DELETE_CARD = "action delete card";
    private final String DELETE_POSITION = "delete position";
    private int COLOR_YELLOW, COLOR_DARK;
    private String screenName = "evoucher-detail-cash";
    private boolean isResume = false;

    AppPreferenceHelper appPreferenceHelper;
    private ProductEvoucherDetailActivityBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.product_evoucher_detail_activity);
        detailActivity = this;

        idProduct = getIntent().getLongExtra(PRODUCT_ID, 0);
        isComboItem = getIntent().getBooleanExtra(ConstantValue.IS_COMBO_ITEM, false);
        isBuyNow = getIntent().getBooleanExtra(FlashSaleDetailController.BUY_NOW, false);
        source = getIntent().getStringExtra(GlobalTracking.SOURCE);
        appPreferenceHelper = new AppPreferenceHelper(this);
        listTokenBankCard = new ArrayList<>();

        findView();
        setViewObserver();

        setProducState();
        startLoadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_share:
                Helper.shareLink(activity, productModel.getShare_link(), false);
                break;
            case R.id.ll_merchant:
                try {
                    MerchantModel merchantModel = new MerchantModel();
                    merchantModel.setId(String.valueOf(productModel.getMerchant_info().getId()));
                    Intent merchant = new Intent(activity, MerchantDetailActivity.class);
                    Bundle data = new Bundle();
                    data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                    merchant.putExtras(data);
                    merchant.putExtra(ConstantValue.CURRENT_PRODUCT_ID, idProduct);
                    startActivity(merchant);
                } catch (Exception e) {
                    Crashlytics.logException(new CrashModel("Product Model" + productModel));
                    Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.try_again, activity));
                }
                break;

            case R.id.tv_buy:
                if (!LoginHelper.isLoggedIn(activity)) {
                    LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_BUY_EVOUCHER);
                } else
                    startBuyProduct();
                break;
            case R.id.txtShowDetail:
                ReviewRatingDialog dialog = new ReviewRatingDialog(activity, false, false, false);
                dialog.show();
                dialog.setData(productModel.getHtml_text());
                break;
            case R.id.txtShowAll:
                AllRatingDialog allRatingDialog = new AllRatingDialog(activity, false, false, false);
                allRatingDialog.show();
                allRatingDialog.setData(productModel.getId());
                break;
            case R.id.constraintLixi:
                LixiGiftDialog lixiGiftDialog = new LixiGiftDialog(activity, false, true, false);
                lixiGiftDialog.show();
                lixiGiftDialog.setData(isFlashSale ? productModel.getFlash_sale_info().getCashback_price() : productModel.getCashback_price());
                break;
            default:
                break;
        }
    }

    //region SHOW DATA

    private void checkData() {
        if (productModel == null || productModel.getPayment_type() == null || productModel.getPayment_type().size() == 0)
            return;
        if (productModel.getPayment_type().get(0).getPayment_type().equals(PaymentTypeModel.PAY_BY_LIXI)) {
            isProductLixiPayment = true;
            return;
        }

        isFlashSale = false;
        if (productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {

            long startTime = productModel.getFlash_sale_info().getStart_time();
            long endTime = productModel.getFlash_sale_info().getEnd_time();
            long currentTime = productModel.getFlash_sale_info().getServer_current()
                    + (System.currentTimeMillis() - productModel.getFlash_sale_info().getMy_start_time());
            if (endTime != 0 && startTime != 0) {
                if (startTime < currentTime) {
                    isFlashSale = true;
                }
            }
        }
    }

    private void showData() {
        mBinding.loadingView.setVisibility(View.GONE);
        checkData();
        if (isProductLixiPayment) {
            //TODO chuyển qua activity sản phẩm lixishop
            finish();
        }

        Log.i("Combos", String.valueOf(isComboItem));
        //TODO tracking here
        if (!isComboItem) {
            super.handleGoogleAnalytics();
            Analytics.trackScreenView(getScreenName(), idProduct);
        }

        setProducState();

        setSlideBanner();

        tv_title.setText(productModel.getName());
        tv_name.setText(productModel.getName());
        if (productModel.getExpired_text().length() == 0 || productModel.getExpired_text().equals("")) {
            if (productModel.getProduct_type().equals("combo")) {
                tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_apply_to, activity),
                        Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y, String.valueOf(Long.parseLong(productModel.getContract_end_date()) * 1000)))
                );
            } else
                tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_expired, activity),
                        Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y, String.valueOf(Long.parseLong(productModel.getContract_end_date()) * 1000))));
        } else tv_expire_day.setText(productModel.getExpired_text());

        ratingBar.setCount((int) productModel.getRating_info().getAverage_rating());

        setProducPrice();

        setComboInfo();

        setPlaceApply();

        setProductCondition();

        setWebView();

        if (isComboItem) {
            rl_buy.setVisibility(View.GONE);

            constraintLixi.setVisibility(View.GONE);
            if (v_line_name != null)
                v_line_name.setVisibility(View.GONE);
            if (v_line_price != null)
                v_line_price.setVisibility(View.GONE);
        }

        showFlashsale();
    }

    private void showFlashsale() {
        if (flashSaleView != null) {
            //TODO check lại timer đếm ngược
            return;
        }
        if (isFlashSale) {
            constraintPrice.setVisibility(View.GONE);
            ll_flashsale.setVisibility(View.VISIBLE);
            flashSaleView = new FlashSaleDetailController(this, ll_flashsale, productModel.getFlash_sale_info());
            flashSaleView.setiFlashSaleEvent(new FlashSaleDetailController.FlashSaleEvent() {
                @Override
                public void onFinish() {
                    timeOutDialog = new FlashSaleTimeOutDialog(activity, false, false, false);
                    timeOutDialog.show();
                    timeOutDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            if (popupCart != null && popupCart.isShowing())
                                popupCart.cancel();
                            if (popupPayment != null && popupPayment.isShowing())
                                popupPayment.cancel();
                            if (popupChooseSavedCard != null && popupChooseSavedCard.isShowing()) {
                                popupChooseSavedCard.cancel();
                            }
                            if (popupChooseNewCard != null && popupChooseNewCard.isShowing()) {
                                popupChooseNewCard.cancel();
                            }

                            startLoadData();
                        }
                    });
                }

                @Override
                public void onTick(int[] arrTime) {
                    if (popupCart != null && popupCart.isShowing())
                        popupCart.updateFsTimer(arrTime);
                    if (popupPayment != null && popupPayment.isShowing())
                        popupPayment.updateFsTimer(arrTime);
                    if (popupChooseSavedCard != null && popupChooseSavedCard.isShowing()) {
                        popupChooseSavedCard.updateFsTimer(arrTime);
                    }
                    if (popupChooseNewCard != null && popupChooseNewCard.isShowing()) {
                        popupChooseNewCard.updateFsTimer(arrTime);
                    }
                }
            });
        } else {
            ll_flashsale.setVisibility(View.GONE);
        }

        setRemainFlashSale();
    }

    private void setRemainFlashSale() {
        if (isFlashSale) {
            linearUserBought.setVisibility(View.GONE);
            tv_remain.setTextColor(COLOR_DARK);
            tv_remain.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_rating, activity), productModel.getRating_info().getTotal_rating()));
        } else {
            linearUserBought.setVisibility(View.VISIBLE);
            tv_remain.setTextColor(COLOR_YELLOW);
            if (productModel.getQuantity() > 10) {
                tv_remain.setText(LanguageBinding.getString(R.string.cash_evoucher_stocking, activity));
            } else if (productModel.getQuantity() != 0) {
                tv_remain.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_still, activity), productModel.getQuantity()));
            } else {
                tv_remain.setText(LanguageBinding.getString(R.string.cash_evoucher_sold_out, activity));
            }
            txtRating.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_rating, activity), productModel.getRating_info().getTotal_rating()));
            tv_buyer.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_user_bought, activity), productModel.getQuantity_order_done()));
        }
    }

    private void setProducState() {
        if (isFlashSale) {
            if (productModel == null) {
                tv_buy.setText(LanguageBinding.getString(R.string.cash_evoucher_loading, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_gray_corner_right);
                tv_buy.setOnClickListener(null);
                return;
            }
            if (productModel.getFlash_sale_info().getQuantity_limit() == productModel.getFlash_sale_info().getQuantity_order_done()) {
                tv_buy.setText(LanguageBinding.getString(R.string.cash_evoucher_sold_out, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_gray_corner_right);
                tv_buy.setOnClickListener(null);
            } else {
                tv_buy.setText(LanguageBinding.getString(R.string.buy, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_red_corner_right);
                tv_buy.setOnClickListener(this);
            }
        } else {
            if (productModel == null) {
                tv_buy.setText(LanguageBinding.getString(R.string.cash_evoucher_loading, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_gray_corner_right);
                tv_buy.setOnClickListener(null);
                return;
            }
            if (productModel.getQuantity() <= 0) {
                tv_buy.setText(LanguageBinding.getString(R.string.cash_evoucher_sold_out, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_gray_corner_right);
                tv_buy.setOnClickListener(null);
            } else {
                tv_buy.setText(LanguageBinding.getString(R.string.buy, activity));
                tv_buy.setBackgroundResource(R.drawable.bg_promotion_red_corner_right);
                tv_buy.setOnClickListener(this);
            }
        }
    }

    private void setSlideBanner() {
        List<String> list;
        if (productModel.getProduct_image() != null && productModel.getProduct_image().size() > 0)
            list = productModel.getProduct_image();
        else
            list = new ArrayList<>();
        adapterImageProductDetail = new AdapterImageEvoucherDetail(this, list);
        adapterImageProductDetail.setData(activity, productModel.getName());
        viewpager.setAdapter(adapterImageProductDetail);
        indicator.setViewPager(viewpager);

        indicator.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.value_10) * (adapterImageProductDetail.getCount() + 1);
        indicator.requestLayout();
        indicator.setViewPager(viewpager);
    }

    private void setProducPrice() {
        tv_price.setText(Helper.getVNCurrency(productModel.getPayment_discount_price()) + getString(R.string.p));

        if (isFlashSale) {
            if (productModel.getFlash_sale_info().getCashback_price() > 0) {
                constraintLixi.setVisibility(View.VISIBLE);
                String s = String.format(LanguageBinding.getString(R.string.cash_evoucher_offer_lixi, activity), Helper.getVNCurrency(productModel.getFlash_sale_info().getCashback_price()));
                txtLixi.setText(Html.fromHtml(s));
            } else {
                constraintLixi.setVisibility(View.GONE);
            }
        } else {
            if (productModel.getCashback_price() > 0) {
                constraintLixi.setVisibility(View.VISIBLE);
                String s = String.format(LanguageBinding.getString(R.string.cash_evoucher_offer_lixi, activity), Helper.getVNCurrency(productModel.getCashback_price()));
                txtLixi.setText(Html.fromHtml(s));
            } else {
                constraintLixi.setVisibility(View.GONE);
            }
        }

        if (productModel.getPayment_discount_price() != productModel.getPayment_price()) {
            tv_old_price.setText(Helper.getVNCurrency(productModel.getPayment_price()) + "đ");
            tv_old_price.setPaintFlags(tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            int percent = 100 - (int) (productModel.getPayment_discount_price() * 100 / productModel.getPayment_price());
            tv_percent_discount.setText("-" + percent + "%");
        } else {
            tv_price.requestLayout();
        }
    }

    private void setComboInfo() {
        if (isCombo && productModel.getCombo_item() != null) {
            ll_place.setVisibility(View.GONE);
            ll_detail_combo.setVisibility(View.VISIBLE);

            adapterDetailCombo = new AdapterComboProduct(this, productModel.getCombo_item());
            rv_combo.setLayoutManager(new LinearLayoutManager(this));
            rv_combo.setAdapter(adapterDetailCombo);
//            rv_combo.setNestedScrollingEnabled(false);
            tv_list_combo.setText(String.format(LanguageBinding.getString(R.string.cash_evoucehr_reward, activity), productModel.getCombo_item().size()));
            frameRating.setVisibility(View.GONE);
            txtRating.setVisibility(View.GONE);
            linearRating.setVisibility(View.GONE);
        } else {
            ll_place.setVisibility(View.VISIBLE);
            ll_detail_combo.setVisibility(View.GONE);
            frameRating.setVisibility(View.VISIBLE);
            txtRating.setVisibility(View.VISIBLE);
            linearRating.setVisibility(View.VISIBLE);
        }
    }

    private void setPlaceApply() {

        ImageLoader.getInstance().displayImage(productModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
        tv_name_merchant.setText(productModel.getMerchant_info().getName());

        if (productModel.getStore_apply() == null) {
            ln_one_store.setVisibility(View.GONE);
            tv_store_count.setVisibility(View.GONE);
            ll_list_store.setVisibility(View.GONE);
            return;
        }

        if (productModel.getStore_apply().size() == 1) {
            ln_one_store.setVisibility(View.VISIBLE);
            tv_store_count.setVisibility(View.GONE);
            ll_list_store.setVisibility(View.GONE);

            tv_address.setText(productModel.getStore_apply().get(0).getAddress());
            tv_sdt.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), productModel.getStore_apply().get(0).getPhone()));
        } else {
            ln_one_store.setVisibility(View.GONE);
            tv_store_count.setVisibility(View.VISIBLE);
            ll_list_store.setVisibility(View.VISIBLE);

            tv_store_count.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_where_apply, activity), productModel.getStore_apply().size()));
            int STORE_SHOW = Math.min(productModel.getStore_apply().size(), 3);
            for (int i = 0; i < STORE_SHOW; i++) {
                new StoreController(activity, productModel.getStore_apply().get(i).convertToStoreModel(), null, i, STORE_SHOW, ll_list_store);
            }
        }
    }

    private void setProductCondition() {
        if (productModel.getUse_condition() != null) {
            if (!productModel.getUse_condition().equals("")) {
                tv_condition_content.setText(productModel.getUse_condition());
                ll_condition.setVisibility(View.VISIBLE);
            } else {
                ll_condition.setVisibility(View.VISIBLE);
                tv_condition_content.setVisibility(View.GONE);
            }

        } else {
            ll_condition.setVisibility(View.VISIBLE);
            tv_condition_content.setVisibility(View.GONE);
        }
    }

    private void setWebView() {
        txtReviewContent.setText(productModel.getHtml_strip_text()
                .replace("\n", "")
                .replace("\t", "")
                .trim()
        );

        DataLoader.getListRating(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<RatingModel> listRating = (ArrayList<RatingModel>) object;
                    if (listRating != null && listRating.size() != 0) {
                        if (listRating.size() > 3) {
                            txtShowAll.setVisibility(View.VISIBLE);
                        } else {
                            txtShowAll.setVisibility(View.GONE);
                        }
                        linearRating.setVisibility(View.VISIBLE);
                        RatingAdapter adapter = new RatingAdapter(activity, listRating, true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recListRating.setLayoutManager(layoutManager);
                        recListRating.setAdapter(adapter);
                    } else {
                        linearRating.setVisibility(View.GONE);
                    }
                }
            }
        }, (int) productModel.getId(), 1, 4);
    }
    //endregion

    //region LOAD DATA
    public void startLoadData() {
        Helper.getGPSLocation(activity, new LocationCallBack() {
            @Override
            public void callback(LatLng position, ArrayList<String> addressInfo) {
                DataLoader.setupAPICall(activity, null);

                Call<ApiWrapperModel<ProductCashVoucherDetailModel>> call = DataLoader.apiService._getLixiShopProductDetailNew(idProduct, DataLoader.region, String.valueOf(position.latitude), String.valueOf(position.longitude));

                call.enqueue(new Callback<ApiWrapperModel<ProductCashVoucherDetailModel>>() {
                    @Override
                    public void onResponse(Call<ApiWrapperModel<ProductCashVoucherDetailModel>> call, Response<ApiWrapperModel<ProductCashVoucherDetailModel>> response) {
                        if (response.isSuccessful()) {
                            ApiWrapperModel<ProductCashVoucherDetailModel> result = response.body();
                            long currentTime = result.get_meta().getCurrentTime();

                            String status = result.get_meta().getStatus();
                            if (status.equals(ConstantValue.SUCCESS)) {
                                productModel = result.getRecords();
                                if (productModel != null && productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {
                                    productModel.getFlash_sale_info().setServer_current(currentTime);
                                }
                                isCombo = productModel.getProduct_type().indexOf("combo") >= 0;
                                showData();
                                getUserConfig();
                                GlobalTracking.trackShowDetail(
                                        TrackingConstant.ContentType.EVOUCHER,
                                        productModel.getId(),
                                        TrackingConstant.Currency.VND,
                                        productModel.getPayment_price(),
                                        source
                                );
                            } else {
                                finish();
                                Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                                Helper.showErrorDialog(activity, "API CALL ERROR!");
                            }
                        } else {
                            finish();
                            Helper.showErrorDialog(activity, response.message());
                            Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ApiWrapperModel<ProductCashVoucherDetailModel>> call, Throwable t) {
                        Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
    }

    private void getUserConfig() {
        DataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    try {
                        ConfigModel configModel = (ConfigModel) object;
                        List<PaymentMethodModel> paymentMethodModels = productModel.getPayment_method();
                        for (int i = 0; i < paymentMethodModels.size(); i++) {
                            if (paymentMethodModels.get(i).getKey().equals("cod")) {
                                productModel.getPayment_method().get(i).setPaymentCodFee(configModel.getPayment_cod_fee());
                            }
                        }

                        // buy now flash sale
                        if (isBuyNow) {
                            if (!LoginHelper.isLoggedIn(activity)) {
                                LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_BUY_EVOUCHER);
                            } else
                                startBuyProduct();
                        }
                    } catch (Exception e) {
                        Helper.showErrorDialog(activity, "Cannot get user config");
                    }
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }
    //endregion

    //region BUY PRODUCT - CART_PAYMENT

    int countProduct;
    String phoneReceiveSmsVoucher = "";
    boolean isUserDisCount = false;
    long lixiDiscount;
    PaymentMethodModel paymentMethod = null;
    BankModel bankModel;
    boolean isSaveCard = false;
    long payPrice = 0;
    long lixiCashback = 0;
    TokenData tokenData;
    ArrayList<TokenData> listTokenBankCard;

    PopupCart popupCart;
    PopupPayment popupPayment;
    PopupChooseNewCard popupChooseNewCard;
    PopupChooseSavedCard popupChooseSavedCard;
    DialogTransactions popupTransaction;
    FlashSaleTimeOutDialog timeOutDialog;

    //old UI
    ConfirmBuyCashEvouherDialog buyOldDialog;

    private void startBuyProduct() {
        double price = 0;
        if (productModel.getFlash_sale_id() > 0) {
            price = productModel.getFlash_sale_info().getPayment_price();
        } else {
            price = productModel.getPayment_price();
        }
        GlobalTracking.trackStartCheckout(TrackingConstant.ContentType.EVOUCHER, idProduct, source, price);
        int newUI = 0;
        try {
            newUI = ConfigModel.getInstance(this).getLixi_pay_new();
        } catch (Exception e) {
        }

        if (newUI > 0) {
            popupCart = new PopupCart(activity, false, false, false);
            popupCart.show();
            popupCart.setData(productModel);
            popupCart.setCartEventListener(new PopupCart.CartEventListener() {
                @Override
                public void onNext(int sl, String sdt) {
                    countProduct = sl;
                    phoneReceiveSmsVoucher = sdt;
                    showPopupPayment();
                }
            });
            popupCart.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if ((paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing()) ||
                            (paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing()))
                        return;
                    if (!isComboItem) {
                        Analytics.trackScreenView(getScreenName(), idProduct);
                    }
                }
            });
        } else {
            buyOldDialog = new ConfirmBuyCashEvouherDialog(this, false, false, false);
            buyOldDialog.setIDidalogEventListener(new ConfirmBuyCashEvouherDialog.IDidalogEvent() {
                @Override
                public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                    if (success) {
                        String ms = transactionResult.getMessage().get(0).getText();
                        ms = getMessage(ms, transactionResult.getMessage().get(0).getParams());
                        SuccessCashDialog dialog = new SuccessCashDialog(activity, false, false, false);
                        dialog.setCancelable(false);
                        dialog.show();
                        dialog.setData(LanguageBinding.getString(R.string.cash_evoucher_payment_success, activity), ms, "", transactionResult);
                    } else {
                        String ms = transactionResult.getMessage().get(0).getText();
                        FailedCashDialog popupBuyFailed = new FailedCashDialog(activity, false, false, false);
                        popupBuyFailed.setCancelable(false);
                        popupBuyFailed.setIDidalogEventListener(new FailedCashDialog.IDidalogEvent() {
                            @Override
                            public void onBuyAgain() {
                                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, productModel.getId(), TrackingConstant.ContentSource.HOME);
                                finish();
                            }
                        });
                        popupBuyFailed.show();
                        popupBuyFailed.setData(transactionResult.getTitle(), ms, "", transactionResult);
                    }
                }
            });
            buyOldDialog.show();
            buyOldDialog.setData(0, productModel.getId() + "", productModel, "", false);
        }
    }

    public void showPopupPayment() {
        popupPayment = new PopupPayment(activity, false, false, false);
        popupPayment.show();
        popupPayment.setData(productModel, countProduct);
        popupPayment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if ((paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing())
                        || (paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing()))
                    return;
                Analytics.trackScreenView(TrackEvent.SCREEN.VOUCHER_CART_STEP_1, productModel.getId());
            }
        });

        popupPayment.setPaymentEventListener(new PopupPayment.PaymentEventListener() {
            @Override
            public void onNext(boolean isUserLixiDiscount, long lixiDiscount, PaymentMethodModel paymentMethodModel, long price, long lixi, boolean isSaveCard) {
                paymentMethod = paymentMethodModel;
                isUserDisCount = isUserLixiDiscount;
                payPrice = price;
                lixiCashback = lixi;
                ActivityProductCashEvoucherDetail.this.isSaveCard = isSaveCard;
                ActivityProductCashEvoucherDetail.this.lixiDiscount = lixiDiscount;
                ActivityProductCashEvoucherDetail.this.bankModel = paymentMethodModel.getBankSelected();
                String key = paymentMethod.getKey();
                switch (key) {
                    case PaymentMethodModel.KEY_PAYMENT_COD:
                        buyCOD();
                        break;
                    case PaymentMethodModel.KEY_PAYMENT_AT_STORE:
                        buyAtStore();
                        break;
                    case PaymentMethodModel.KEY_PAYMENT_MOMO:
                        paymentByMomo();
                        break;
                    case PaymentMethodModel.KEY_PAYMENT_ZALO_WALLET:
                        paymentByZaloWallet();
                        break;
                    case PaymentMethodModel.KEY_PAYMENT_NAPAS:
                    case PaymentMethodModel.KEY_PAYMENT_NAPAS_CC:
                        if (appPreferenceHelper.getTokenBankCard(key) != null && appPreferenceHelper.getTokenBankCard(key).size() != 0) {
                            // chọn thẻ đã lưu
                            selectCardNapas(key);
                        } else {
                            // nhập thẻ mới
                            checkoutNapas("");
                        }
                        break;
                    default:
                        List<BankModel> listBankModel = new ArrayList<>();
                        if (key.equals("bank"))
                            listBankModel = productModel.getBank();

                        if (key.equals("cc"))
                            listBankModel = productModel.getCard_international();

                        if (key.equals("zalo_pay_cc"))
                            listBankModel = productModel.getZalo_card_international();

                        if (key.equals("zalo_pay_atm"))
                            listBankModel = productModel.getZalo_bank();

                        if (key.equals("payoo_bank"))
                            listBankModel = productModel.getPayoo_bank();

                        if (key.equals("payoo_cc"))
                            listBankModel = productModel.getPayoo_card_international();

                        if (key.indexOf("payoo") >= 0) {
                            int selectBankinApp = 0;
                            try {
                                selectBankinApp = ConfigModel.getInstance(activity).getLixi_select_bank();
                            } catch (Exception e) {
                            }
                            if (selectBankinApp == 0 || listBankModel == null || listBankModel.size() == 0) {
                                checkout(key);
                                return;
                            }
                        }
                        showPopupSelectBank(listBankModel);
                        break;
                }
            }
        });
    }

    public void showPopupSelectBank(List<BankModel> list) {
        if (popupChooseNewCard == null) {
            popupChooseNewCard = new PopupChooseNewCard(this, false, false, false);
        }

        popupChooseNewCard.show();
        popupChooseNewCard.setData(list, productModel);
        popupChooseNewCard.setChooseNewBankEventListener(new PopupChooseNewCard.ChooseNewBankEventListener() {
            @Override
            public void onNext(BankModel bankModel) {
                popupChooseNewCard.setShowProgress(true);
                ActivityProductCashEvoucherDetail.this.bankModel = bankModel;
                String key = paymentMethod.getKey();
                checkout(key);
            }
        });
        popupChooseNewCard.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if ((paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing()) ||
                        (paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing()))
                    return;
                Analytics.trackScreenView(TrackEvent.SCREEN.VOUCHER_CART_STEP_2, productModel.getId());
            }
        });
    }

    //region Payment GateWay
    public void buyCOD() {
        try {
            final PaymentCodBody paymentCodBody = new PaymentCodBody();
            paymentCodBody.setId(productModel.getId());
            if (ConfirmBuyCashEvouherDialog.isReorder) {
                ConfirmBuyCashEvouherDialog.isReorder = false;
                paymentCodBody.setOrder_id(ConfirmBuyCashEvouherDialog.order_id);
                ConfirmBuyCashEvouherDialog.order_id = -1;
            }
            paymentCodBody.setPayment_method(paymentMethod.getKey());
            paymentCodBody.setQuantity(countProduct);
            paymentCodBody.setPhone(phoneReceiveSmsVoucher);
            paymentCodBody.setUser_discount(isUserDisCount);
            DataLoader.paymentCod(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        trackingPurchaseSuccess();
                        PaymentCodModel paymentCodModel = (PaymentCodModel) object;
                        PaymentCODSuccessDialog codSuccessDialog = new PaymentCODSuccessDialog(activity, false, true, false);
                        codSuccessDialog.show();
                        codSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //Toast.makeText(activity, "sdsdsds", Toast.LENGTH_SHORT).show();
                                Analytics.trackScreenView(TrackEvent.SCREEN.VOUCHER_CART_STEP_2, productModel.getId());
                            }
                        });

                        long price = countProduct * productModel.getPayment_discount_price();
                        long lixiCashback = countProduct * productModel.getCashback_price();
                        if (productModel.getFlash_sale_id() > 0) {
                            price = countProduct * productModel.getFlash_sale_info().getPayment_discount_price();
                            lixiCashback = countProduct * productModel.getFlash_sale_info().getCashback_price();
                        }

                        codSuccessDialog.setData(paymentCodModel,
                                productModel, price / countProduct, isUserDisCount ? lixiDiscount : 0,
                                lixiCashback / countProduct, phoneReceiveSmsVoucher);
                        codSuccessDialog.setListener(new PaymentCODSuccessDialog.OnPaymentCodClickListener() {
                            @Override
                            public void onDone() {
                            }
                        });
                        Analytics.trackOrderEVoucher(paymentCodModel.getTransaction_id());
                    } else {
                        Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                    trackingVoucher(isSuccess);
                }
            }, paymentCodBody);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyAtStore() {
        DialogBuyAtStore basDialog = new DialogBuyAtStore(activity, false, false, false);
        basDialog.setIDidalogEventListener(new DialogBuyAtStore.IDidalogEvent() {
            @Override
            public void onOk() {

            }
        });

        basDialog.show();
        basDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //mLixiTrackingHelper.createNavigateTrackingSocket("payment-step2-pay-option", productModel.getId());
            }
        });
        long price = countProduct * productModel.getPayment_discount_price();
        long lixiCashback = countProduct * productModel.getCashback_price();
        if (productModel.getFlash_sale_id() > 0) {
            price = countProduct * productModel.getFlash_sale_info().getPayment_discount_price();
            lixiCashback = countProduct * productModel.getFlash_sale_info().getCashback_price();
        }
        UserModel userModel = UserModel.getInstance();
        basDialog.setData(productModel, paymentMethod, countProduct, userModel, isUserDisCount, lixiDiscount, price / countProduct, lixiCashback / countProduct);
    }

    private String submitUrl = "";

    private void paymentByMomo() {
        AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.PRODUCTION);
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);
        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", countProduct);
        obj.addProperty("phone", phoneReceiveSmsVoucher);
        obj.addProperty("payment_method", paymentMethod.getKey());
        obj.addProperty("url_return", "");
        obj.addProperty("user_discount", isUserDisCount);

        DataLoader.buyProductByMomo(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject records = (JsonObject) object;
                    JsonObject paymentData = records.getAsJsonObject("payment_data");
                    JsonObject sdkParam = paymentData.getAsJsonObject("sdk_param");
                    submitUrl = paymentData.get("submit_url").getAsString();

                    Map<String, Object> eventValue = new HashMap<>();

                    for (Iterator<Map.Entry<String, JsonElement>> entries = sdkParam.entrySet().iterator(); entries.hasNext(); ) {
                        Map.Entry<String, JsonElement> entry = entries.next();
                        eventValue.put(entry.getKey(), entry.getValue().isJsonNull() ? null : entry.getValue().getAsString());
                    }

                    AppMoMoLib.getInstance().requestMoMoCallBack(activity, eventValue);
                }
                trackingVoucher(isSuccess);
            }
        }, obj);
    }

    // region zalo

    @SuppressLint("WrongConstant")
    private boolean checkzaloPayInstalled() {
        try {
            PackageManager pm = getApplicationContext().getPackageManager();
            pm.getPackageInfo(ZaloPaySDK.PACKAGE_IN_PLAY_STORE, 1);
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    private void paymentByZaloWallet() {

        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", countProduct);
        obj.addProperty("phone", phoneReceiveSmsVoucher);
        obj.addProperty("payment_method", paymentMethod.getKey());
        obj.addProperty("url_return", "");
        obj.addProperty("user_discount", isUserDisCount);

        DataLoader.buyProductByZaloWallet(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    if (!checkzaloPayInstalled()) {
                        // chưa cài đặt
                        ZaloPaySDK.getInstance().navigateToStore(activity);
                        return;
                    }

                    final JsonObject records = (JsonObject) object;
                    JsonObject paymentData = records.getAsJsonObject("payment_data");
                    JsonObject sdkParam = paymentData.getAsJsonObject("sdk_param");
                    submitUrl = paymentData.get("submit_url").getAsString();

                    ZaloPaySDK.getInstance().initWithAppId(sdkParam.get("appid").getAsInt());
                    ZaloPaySDK.getInstance().payOrder(ActivityProductCashEvoucherDetail.this, sdkParam.get("zptranstoken").getAsString(), new ZaloPayListener() {
                        @Override
                        public void onPaymentSucceeded(String transactionId, String transToken) {
                            // thanh toán thành công
                            //hideAllPayment();

                            // submit to lixi
                            JsonObject postData = new JsonObject();
                            postData.addProperty("transactionid", transactionId);
                            postData.addProperty("zptranstoken", transToken);
                            submitToZaloWallet(postData);
                        }


                        @Override
                        public void onPaymentError(ZaloPayErrorCode zaloPayErrorCode, int paymentErrorCode, String zpTransToken) {
                            // thanh toán thất bại
                            switch (zaloPayErrorCode) {
                                case ZALO_PAY_NOT_INSTALLED:
                                    // client chưa cài zalo pay
                                    ZaloPaySDK.getInstance().navigateToStore(activity);
                                    break;
                                case USER_CANCEL:
                                    return;
                                default:
                                    break;
                            }

                            // submit to lixi
                            JsonObject postData = new JsonObject();
                            submitToZaloWallet(postData);
                        }
                    });
                } else {
                    Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                trackingVoucher(isSuccess);
            }
        }, obj);
    }

    private void submitToZaloWallet(JsonObject params) {
        DataLoader.submitToZaloWallet(activity, submitUrl, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                // sau khi submit
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    JsonObject data = jsonObject.getAsJsonObject("data");

                    StringBuilder builder = new StringBuilder();
                    builder.append("?");

                    for (Iterator<Map.Entry<String, JsonElement>> entries = data.entrySet().iterator(); entries.hasNext(); ) {
                        Map.Entry<String, JsonElement> entry = entries.next();
                        builder.append(entry.getKey()).append("=").append(entry.getValue().getAsString()).append("&");
                    }
                    String params = builder.substring(0, builder.lastIndexOf("&"));
                    checkShoppingStatus(params, "wallet_zalo");
                }
            }
        }, params);
    }

    // endregion

    private void checkout(String keyPaymentMethod) {
        String method = "";
        if (keyPaymentMethod.contains("zalo"))
            method = "/zalo";

        if (keyPaymentMethod.contains("payoo"))
            method = "/payoo";

        if (keyPaymentMethod.contains("napas"))
            method = "/napas";

        if (method.equals(""))
            buyVoucherV1(method); //VTC chỉ có api thanh toán v1
        else {
            if (productModel.getOrder_method().equals("v2"))
                buyVoucherV2(method);
            else buyVoucherV1(method);
        }
    }

    private void buyVoucherV1(String method) {
        final JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", countProduct);
        if (bankModel != null)
            obj.addProperty("bank_code", bankModel.getCode());
        obj.addProperty("phone", phoneReceiveSmsVoucher);
        obj.addProperty("payment_method", paymentMethod.getKey());
        obj.addProperty("user_discount", isUserDisCount);

        if (ConfirmBuyCashEvouherDialog.isReorder) {
            ConfirmBuyCashEvouherDialog.isReorder = false;
            obj.addProperty("order_id", ConfirmBuyCashEvouherDialog.order_id);
            ConfirmBuyCashEvouherDialog.order_id = -1;
        }

        DataLoader.buyCashProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = "";
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    Long productId = jsonObject.get("product").getAsLong();
                    showTransactionDialog(payment_link, orderId);
                } else {
                    Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                if (popupChooseNewCard != null) {
                    popupChooseNewCard.setShowProgress(false);
                }
                trackingVoucher(isSuccess);
            }
        }, obj, method);

    }

    private void buyVoucherV2(String method) {
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", productModel.getId());
            jsonObject.put("quantity", countProduct);
            jsonArray.put(jsonObject);

            JSONObject objs = new JSONObject();
            if (bankModel != null)
                objs.put("bank_code", bankModel.getCode());
            objs.put("phone", phoneReceiveSmsVoucher);
            objs.put("payment_method", paymentMethod.getKey());
            objs.put("user_discount", isUserDisCount);
            objs.put("list_product", jsonArray);

            DataLoader.buyCashProductV2(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        JsonObject jsonObject = (JsonObject) object;
                        String payment_link = "";
                        payment_link = jsonObject.get("payment_link").getAsString();
                        Long orderId = jsonObject.get("id").getAsLong();
                        showTransactionDialog(payment_link, orderId);
                    } else {
                        Toast toast = Toast.makeText(activity, object + "", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    if (popupChooseNewCard != null) {
                        popupChooseNewCard.setShowProgress(false);
                    }
                    trackingVoucher(isSuccess);
                }
            }, objs, method);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //region NAPAS gateway
    private void selectCardNapas(String key) {
        listTokenBankCard = appPreferenceHelper.getTokenBankCard(key);
        popupChooseSavedCard = new PopupChooseSavedCard(activity, false, false, false);
        popupChooseSavedCard.show();
        popupChooseSavedCard.setData(listTokenBankCard, productModel);
        popupChooseSavedCard.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if ((paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing()) ||
                        (paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing()))
                    return;
                Analytics.trackScreenView(TrackEvent.SCREEN.VOUCHER_CART_STEP_2, productModel.getId());
            }
        });

        popupChooseSavedCard.addOnCreateNewCard(new OnCreateNewCard() {
            @Override
            public void onNew(boolean isSaveCard) {
                ActivityProductCashEvoucherDetail.this.isSaveCard = isSaveCard;
                checkoutNapas("");
            }
        });

        popupChooseSavedCard.addOnUseSavedCard(new OnUseSavedCard() {
            @Override
            public void onUse(TokenData tokenData) {
                // check bảo mật
                ActivityProductCashEvoucherDetail.this.tokenData = tokenData;
                Intent intent = new Intent(ActivityProductCashEvoucherDetail.this, CheckSecurity.class);
                intent.putExtra(CheckSecurity.ACTION_CODE, ACTION_CHOOSE_CARD);
                startActivityForResult(intent, CheckSecurity.CHECK_SECURITY_CODE);
            }
        });

        popupChooseSavedCard.addOnDeleteBankCard(new OnDeleteBankCard() {
            @Override
            public void onDelete(int position) {
                Intent intent = new Intent(ActivityProductCashEvoucherDetail.this, CheckSecurity.class);
                intent.putExtra(CheckSecurity.ACTION_CODE, ACTION_DELETE_CARD);
                intent.putExtra(DELETE_POSITION, position);
                startActivityForResult(intent, CheckSecurity.CHECK_SECURITY_CODE);
            }
        });

        popupChooseSavedCard.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                popupPayment.notifyDataSetChanged();
            }
        });
    }

    private void checkoutNapas(String napas_token) {
        if (productModel.getOrder_method().equals("v2")) {
            buyProductNapasV2(napas_token);
        } else {
            buyProductNapas(napas_token);
        }
    }

    private void buyProductNapas(String napas_token) {
        final JsonObject obj = new JsonObject();
        obj.addProperty("create_token", true);
        obj.addProperty("napas_token", napas_token);
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", countProduct);
        obj.addProperty("phone", phoneReceiveSmsVoucher);
        obj.addProperty("payment_method", paymentMethod.getKey());
        obj.addProperty("user_discount", isUserDisCount);

        DataLoader.buyProductNapas(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = "";
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    showTransactionDialog(payment_link, orderId);
                }
                trackingVoucher(isSuccess);
            }
        }, obj);
    }

    private void buyProductNapasV2(String napas_token) {
        final JsonObject obj = new JsonObject();
        obj.addProperty("create_token", true);
        obj.addProperty("napas_token", napas_token);
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", countProduct);
        obj.addProperty("phone", phoneReceiveSmsVoucher);
        obj.addProperty("payment_method", paymentMethod.getKey());
        obj.addProperty("user_discount", isUserDisCount);

        DataLoader.buyProductNapasV2(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject jsonObject = (JsonObject) object;
                    String payment_link = "";
                    payment_link = jsonObject.get("payment_link").getAsString();
                    Long orderId = jsonObject.get("id").getAsLong();
                    showTransactionDialog(payment_link, orderId);
                }
                trackingVoucher(isSuccess);
            }
        }, obj);
    }
    //endregion NAPAS gateway

    public void hideAllPayment() {
        if (popupCart != null && popupCart.isShowing())
            popupCart.dismiss();
        if (popupPayment != null && popupPayment.isShowing())
            popupPayment.dismiss();
        if (popupChooseNewCard != null && popupChooseNewCard.isShowing())
            popupChooseNewCard.dismiss();
        if (popupChooseSavedCard != null && popupChooseSavedCard.isShowing())
            popupChooseSavedCard.dismiss();
        if (popupTransaction != null && popupTransaction.isShowing())
            popupTransaction.dismiss();
    }

    private boolean checkDialogShowing() {
        if (popupCart != null && popupCart.isShowing())
            return true;
        if (popupPayment != null && popupPayment.isShowing())
            return true;
        if (popupChooseNewCard != null && popupChooseNewCard.isShowing())
            return true;
        if (popupChooseSavedCard != null && popupChooseSavedCard.isShowing())
            return true;
        if (popupTransaction != null && popupTransaction.isShowing())
            return true;
        if (paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing())
            return true;
        return paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing();

    }

    private void showTransactionDialog(String payment_link, Long orderId) {
        popupTransaction = new DialogTransactions(activity, false, false, false);

        popupTransaction.setIDidalogEventListener(new DialogTransactions.IDidalogEvent() {
            @Override
            public void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult) {
                hideAllPayment();
                Analytics.trackOrderEVoucher(orderId);
                if (success) {
                    showPopupSuccess(orderId);
                } else
                    showPopupFailed(orderId);
            }
        });

        popupTransaction.getTokenData(new DialogTransactions.IGetTokenData() {
            @Override
            public void onResult(TokenData tokenData) {
                ActivityProductCashEvoucherDetail.this.tokenData = tokenData;
            }
        });

        popupTransaction.show();
        popupTransaction.setData(
                payment_link,
                isSaveCard,
                orderId
        );
        popupTransaction.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if ((paymentAtBankSuccessDialog != null && paymentAtBankSuccessDialog.isShowing()) ||
                        (paymentAtBankFailDialog != null && paymentAtBankFailDialog.isShowing()))
                    return;
                Analytics.trackScreenView(TrackEvent.SCREEN.VOUCHER_CART_STEP_2, productModel.getId());
            }
        });
    }

    private void checkShoppingStatus(String params, String... momo) {
        DataLoader.checkShoppingStatus(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LixiShopBankTransactionResult transactionResult = (LixiShopBankTransactionResult) object;
                    handleTransactionResult(transactionResult);
                }
            }
        }, params, momo);
    }

    private void handleTransactionResult(LixiShopBankTransactionResult transactionResult) {
        hideAllPayment();
        Analytics.trackOrderEVoucher(transactionResult.getOrder_id());
        if (transactionResult.getStatus()) {
            showPopupSuccess(transactionResult.getOrder_id());
        } else
            showPopupFailed(transactionResult.getOrder_id());
    }

    //endregion Payment GateWay

    //region Payment Result
    private PaymentAtBankSuccessDialog paymentAtBankSuccessDialog;
    private PaymentAtBankFailDialog paymentAtBankFailDialog;

    private void showPopupSuccess(final String orderId) {
        trackingPurchaseSuccess();
        paymentAtBankSuccessDialog = new PaymentAtBankSuccessDialog(activity, false, false, false);
        paymentAtBankSuccessDialog.show();
        paymentAtBankSuccessDialog.setIDidalogEventListener(new PaymentAtBankSuccessDialog.IDidalogEvent() {
            @Override
            public void onOk(boolean success) {

            }
        });
        paymentAtBankSuccessDialog.loadData(phoneReceiveSmsVoucher, productModel, countProduct,
                payPrice, lixiCashback, orderId,
                paymentMethod.getValue(), isUserDisCount ? lixiDiscount : 0, tokenData, isSaveCard);

        paymentAtBankSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (!isComboItem) {
                    Analytics.trackScreenView(getScreenName(), Long.valueOf(orderId.substring(
                            orderId.indexOf("-") + 1, orderId.length())));
                }
            }
        });
    }

    private void showPopupFailed(final String orderId) {
        paymentAtBankFailDialog = new PaymentAtBankFailDialog(activity, false, false, false);
        paymentAtBankFailDialog.show();
        paymentAtBankFailDialog.setIDidalogEventListener(new PaymentAtBankFailDialog.IDidalogEvent() {
            @Override
            public void onOk(boolean success) {
                if (!isComboItem) {
                    Analytics.trackScreenView(getScreenName(), Long.valueOf(
                            orderId.substring(orderId.indexOf("-") + 1, orderId.length())));
                }
            }
        });
        paymentAtBankFailDialog.loadData(productModel, phoneReceiveSmsVoucher, countProduct,
                payPrice, lixiCashback, orderId,
                paymentMethod.getValue(), isUserDisCount ? lixiDiscount : 0);
    }

    private String getMessage(String ms, List<String> par) {
        String res = ms;

        if (par != null) {
            for (int i = 0; i < par.size(); i++) {
                String holder = "{" + String.valueOf(i) + "}";

                if (res.contains(holder)) {
                    res = res.replace(holder, "<b>" + par.get(i) + "</b>");
                }
            }
        }

        return res;
    }
    //endregion
    //endregion

    //region handle ACTIVITY LIFECYCLE
    @Override
    protected void onResume() {
        super.onResume();
        if (productModel != null) {
            checkData();
            showFlashsale();
        }
        if (isResume && !checkDialogShowing() && !isComboItem) {
            Analytics.trackScreenView(getScreenName(), idProduct);
            isResume = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResume = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ZaloPaySDK.getInstance().onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                handleLoginResultAction(data);
                break;
            case CheckSecurity.CHECK_SECURITY_CODE:
                String res = data.getStringExtra(CheckSecurity.ACTION_CODE);
                if (res.equals(ACTION_CHOOSE_CARD)) {
                    checkoutNapas(tokenData.getToken());
                }

                if (res.equals(ACTION_DELETE_CARD)) {
                    int position = data.getIntExtra(DELETE_POSITION, -1);
                    if (position >= 0) {
                        appPreferenceHelper.removeToken(listTokenBankCard.get(position));
                        listTokenBankCard.remove(position);
                        popupChooseSavedCard.notifyDataSetChange(listTokenBankCard);
                        Toast.makeText(activity, LanguageBinding.getString(R.string.cash_evoucher_deleted_card, activity), Toast.LENGTH_SHORT).show();
                        if (listTokenBankCard.size() == 0) {
                            PopupNoSavedCard popupNoSavedCard = new PopupNoSavedCard(activity, false, false, false);
                            popupNoSavedCard.show();
                            popupNoSavedCard.setData(LanguageBinding.getString(R.string.notification_label, activity), LanguageBinding.getString(R.string.cash_evoucher_no_saved_card, activity));
                        }
                    }
                }
                break;
            case ConstantValue.VOICE_SEARCH_CODE:
                ArrayList<String> voiceSearchResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                popupChooseNewCard.setVoiceSearchData(voiceSearchResult.get(0));
                break;
        }

        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO) {
            if (data != null) {
                JsonObject postData = new JsonObject();
                postData.addProperty("data", data.getStringExtra("data"));
                postData.addProperty("phonenumber", data.getStringExtra("phonenumber"));

                DataLoader.submitToMomo(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            JsonObject jsonObject = (JsonObject) object;
                            boolean success = jsonObject.get("meta").getAsJsonObject()
                                    .get("success").getAsBoolean();
                            JsonObject data = (JsonObject) jsonObject.get("data");

                            if (success) {
                                StringBuilder builder = new StringBuilder();
                                builder.append("?");

                                for (Iterator<Map.Entry<String, JsonElement>> entries = data.entrySet().iterator(); entries.hasNext(); ) {
                                    Map.Entry<String, JsonElement> entry = entries.next();
                                    builder.append(entry.getKey()).append("=").append(entry.getValue().getAsString()).append("&");
                                }
                                String params = builder.substring(0, builder.lastIndexOf("&"));
                                checkShoppingStatus(params, "momo");
                            }
                        }
                    }
                }, postData, submitUrl);
            } else {
                Log.d("mapi", "message: not receive info");
            }
        } else {
            Log.d("mapi", "message: not receive info error");
        }
    }

    private void handleLoginResultAction(Intent intent) {
        Bundle data = intent.getExtras();
        String action = data.getString(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        switch (action) {
            case ConstantValue.ACTION_BUY_EVOUCHER:
                startBuyProduct();
                break;
        }

    }
    //endregion

    //region for TRACKING

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_DETAIL;
    }

    @Override
    public void handleGoogleAnalytics() {
        // ko gọi tracking ở đây
        // do activity này có nhiều trường hợp track sau khi load data
    }
    //endregion

    //region view effect
    private int heightWith169Aspect = 0;
    private int y;

    private void setViewObserver() {

        //set ratio for viewpager
//        heightWith169Aspect = rl_viewpager.getWidth() * 9 / 16;
        rl_viewpager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int availableHeight = rl_viewpager.getMeasuredHeight();
                if (availableHeight > 0) {
                    rl_viewpager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    rl_viewpager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    //save height here and do whatever you want with it
                    heightWith169Aspect = rl_viewpager.getWidth() * 9 / 16;

                    ViewGroup.LayoutParams params = rl_viewpager.getLayoutParams();
                    params.height = heightWith169Aspect;
                    rl_viewpager.setLayoutParams(params);
                }
            }
        });

        scroll_view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                y = scroll_view.getScrollY();
                Log.d("myps", y + "");
                Helper.setScrollProrgess(y, v_back, tv_title, iv_back, iv_share);

            }
        });

    }
    //endregion

    public void trackingVoucher(boolean result) {
        String coupon = isUserDisCount ? TrackingConstant.CouponType.LIXI_DISCOUNT : "";
        String method = "";
        switch (paymentMethod.getKey()) {
            case PaymentMethodModel.KEY_PAYMENT_COD:
                method = TrackingConstant.PaymentMethod.COD;
                break;
            case PaymentMethodModel.KEY_PAYMENT_AT_STORE:
                method = TrackingConstant.PaymentMethod.AT_STORE;
                break;
            case PaymentMethodModel.KEY_PAYMENT_MOMO:
            case PaymentMethodModel.KEY_PAYMENT_ZALO_WALLET:
                method = TrackingConstant.PaymentMethod.WALLET;
                break;
            default:
                method = TrackingConstant.PaymentMethod.ONLINE;
                break;
        }

        GlobalTracking.trackCheckedOut(
                TrackingConstant.ContentType.EVOUCHER,
                productModel.getId(),
                countProduct,
                true,
                TrackingConstant.Currency.VND,
                method,
                coupon,
                result,
                null,
                payPrice,
                source
        );
    }

    public void trackingPurchaseSuccess() {
        String coupon = isUserDisCount ? TrackingConstant.CouponType.LIXI_DISCOUNT : "";
        String method = "";
        switch (paymentMethod.getKey()) {
            case PaymentMethodModel.KEY_PAYMENT_COD:
                method = TrackingConstant.PaymentMethod.COD;
                break;
            case PaymentMethodModel.KEY_PAYMENT_AT_STORE:
                method = TrackingConstant.PaymentMethod.AT_STORE;
                break;
            case PaymentMethodModel.KEY_PAYMENT_MOMO:
            case PaymentMethodModel.KEY_PAYMENT_ZALO_WALLET:
                method = TrackingConstant.PaymentMethod.WALLET;
                break;
            default:
                method = TrackingConstant.PaymentMethod.ONLINE;
                break;
        }

        GlobalTracking.trackCheckedOut(
                TrackingConstant.ContentType.EVOUCHER,
                productModel.getId(),
                countProduct,
                true,
                TrackingConstant.Currency.VND,
                method,
                coupon,
                true,
                null,
                payPrice,
                source
        );
    }
}