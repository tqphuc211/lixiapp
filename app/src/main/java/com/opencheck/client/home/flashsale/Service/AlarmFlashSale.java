package com.opencheck.client.home.flashsale.Service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

public class AlarmFlashSale {
    private Context context;

    public static final long TIME_ALARM = 5 * 60 * 1000;
    public static final String REMIND_ID = "remind id";
    public static final String START_TIME = "start time";

    public AlarmFlashSale(Context context) {
        this.context = context;
    }

    private AlarmManager getAlarmManager(){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        return alarmManager;
    }

    private PendingIntent getPending(long id, long startTime){
        Intent intent = new Intent(context, RemindManager.class);
        intent.putExtra(REMIND_ID, id);
        intent.putExtra(START_TIME, startTime);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) id, intent, PendingIntent.FLAG_ONE_SHOT);
        return pendingIntent;
    }

    public void start(long id, long timer, long startTime){
//        getAlarmManager().set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 30 * 1000, getPending(id));
        getAlarmManager().set(AlarmManager.ELAPSED_REALTIME, timer - TIME_ALARM + SystemClock.elapsedRealtime() + 100, getPending(id, startTime));
    }

    public void cancel(long id){
        getAlarmManager().cancel(getPending(id, -1));
    }
}
