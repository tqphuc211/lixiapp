package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class PromotionModel implements Serializable{
    private long id;
    private String code;
    private String type;
    private String name;
    private String description;
    private long money;
    private long value;
    private String image_link;
    private long start_date;
    private long end_date;
    private long min_bill_money;
    private long reduce_money_fixed;
    private long reduce_money_percent;
    private long max_reduce_money;
    private long reward_point_fixed;
    private long reward_point_percent;
    private long max_reward_point;
    private long reduce_money;
    private Long reward_point;
    private boolean isChecked = false;
    private long discount_money;
    private ArrayList<StoreOfCategoryModel> list_store;
    private ArrayList<ProvinceModel> list_province;

    public ArrayList<StoreOfCategoryModel> getList_store() {
        return list_store;
    }

    public void setList_store(ArrayList<StoreOfCategoryModel> list_store) {
        this.list_store = list_store;
    }

    public ArrayList<ProvinceModel> getList_province() {
        return list_province;
    }

    public void setList_province(ArrayList<ProvinceModel> list_province) {
        this.list_province = list_province;
    }

    public long getDiscount_money() {
        return discount_money;
    }

    public void setDiscount_money(long discount_money) {
        this.discount_money = discount_money;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public long getReduce_money() {
        return reduce_money;
    }

    public void setReduce_money(long reduce_money) {
        this.reduce_money = reduce_money;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getReward_point() {
        return reward_point;
    }

    public void setReward_point(Long reward_point) {
        this.reward_point = reward_point;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public long getMin_bill_money() {
        return min_bill_money;
    }

    public void setMin_bill_money(long min_bill_money) {
        this.min_bill_money = min_bill_money;
    }

    public long getReduce_money_fixed() {
        return reduce_money_fixed;
    }

    public void setReduce_money_fixed(long reduce_money_fixed) {
        this.reduce_money_fixed = reduce_money_fixed;
    }

    public long getReduce_money_percent() {
        return reduce_money_percent;
    }

    public void setReduce_money_percent(long reduce_money_percent) {
        this.reduce_money_percent = reduce_money_percent;
    }

    public long getMax_reduce_money() {
        return max_reduce_money;
    }

    public void setMax_reduce_money(long max_reduce_money) {
        this.max_reduce_money = max_reduce_money;
    }

    public long getReward_point_fixed() {
        return reward_point_fixed;
    }

    public void setReward_point_fixed(long reward_point_fixed) {
        this.reward_point_fixed = reward_point_fixed;
    }

    public long getReward_point_percent() {
        return reward_point_percent;
    }

    public void setReward_point_percent(long reward_point_percent) {
        this.reward_point_percent = reward_point_percent;
    }

    public long getMax_reward_point() {
        return max_reward_point;
    }

    public void setMax_reward_point(long max_reward_point) {
        this.max_reward_point = max_reward_point;
    }

    public StorePolicyModel convertToStorePolicy() {
        StorePolicyModel storePolicyModel = new StorePolicyModel();
        storePolicyModel.setId(id);
        storePolicyModel.setType("promotion_code");
        storePolicyModel.setName(name);
        storePolicyModel.setDescription(description);
        storePolicyModel.setEnd_date(end_date);
        storePolicyModel.setStart_date(start_date);
        storePolicyModel.setList_promotion_code_id(new ArrayList<Long>(Collections.singleton(id)));
        storePolicyModel.setList_promotion_code(new ArrayList<String>(Collections.singleton(code)));
        return storePolicyModel;
    }
}
