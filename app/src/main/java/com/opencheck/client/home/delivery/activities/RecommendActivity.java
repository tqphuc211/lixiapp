package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityRecommendBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.PlaceAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LixiTrackingHelper;

import java.util.ArrayList;

import io.socket.client.Socket;

public class RecommendActivity extends LixiActivity {

    private PlaceAdapter adapter;
    private ArrayList<StoreOfCategoryModel> storeList;
    private int page = 1;
    private SpacesItemDecoration spacesItemDecoration;
    private boolean isLoading = false;
    private int record_per_page = DeliDataLoader.record_per_page;
    private DeliAddressModel mLocation;

    private ActivityRecommendBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_recommend);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        initViews();
    }

    private void initViews() {
        mBinding.ivBack.setOnClickListener(this);

        int space = (int) getResources().getDimension(R.dimen.value_8);
        spacesItemDecoration = new SpacesItemDecoration(1, space, true);

        mBinding.swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                noRemain = false;
                mBinding.rcvStore.removeItemDecoration(spacesItemDecoration);
                if (adapter != null) {
                    storeList.clear();
                    adapter.notifyDataSetChanged();
                }
                getRecommendStore();
            }
        });

        mBinding.rcvStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || noRemain)
                    return;

                if (isRvBottom())
                    getRecommendStore();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getRecommendStore();
            }
        }, 300);
    }

    public boolean isRvBottom() {
        if (storeList == null || storeList.size() == 0)
            return false;
        int totalItemCount = storeList.size();
        int lastVisibleItem = ((LinearLayoutManager) mBinding.rcvStore.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7 && storeList.size() >= record_per_page);
    }

    public void showNodata() {
        mBinding.swr.setVisibility(View.GONE);
        mBinding.rcvStore.setVisibility(View.GONE);
        mBinding.lnNoResult.setVisibility(View.VISIBLE);
    }

    public void showData() {
        mBinding.swr.setVisibility(View.VISIBLE);
        mBinding.rcvStore.setVisibility(View.VISIBLE);
        mBinding.lnNoResult.setVisibility(View.GONE);
        adapter = new PlaceAdapter(activity, storeList);
        mBinding.rcvStore.setLayoutManager(new GridLayoutManager(activity, 1));
//        mBinding.rcvStore.addItemDecoration(spacesItemDecoration);
        mBinding.rcvStore.setAdapter(adapter);
    }

    private boolean isReload = false;
    private boolean noRemain = false;

    private void getRecommendStore() {
        isLoading = true;
        mBinding.swr.setRefreshing(true);
        DeliDataLoader.getStoreRecommend(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                mBinding.swr.setRefreshing(false);
                if (isSuccess) {
                    if (page == 1 && !isReload) {
                        storeList = (ArrayList<StoreOfCategoryModel>) object;
                        if (storeList == null || storeList.size() == 0) {
                            showNodata();
                            noRemain = true;
                        } else
                            showData();
                    } else {
                        ArrayList<StoreOfCategoryModel> list = (ArrayList<StoreOfCategoryModel>) object;
                        if (isReload) {
                            storeList.clear();
                            adapter.notifyDataSetChanged();
                            isReload = false;
                            if (list == null || list.size() == 0) {
                                showNodata();
                                return;
                            }
                        }
                        if (storeList == null || list == null || list.size() == 0) {
                            noRemain = true;
                            return;
                        }
                        mBinding.swr.setVisibility(View.VISIBLE);
                        mBinding.rcvStore.setVisibility(View.VISIBLE);
                        mBinding.lnNoResult.setVisibility(View.GONE);
                        storeList.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                    page++;
                    isLoading = false;
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
    }

    @Override
    public void onClick(View v) {
        if (v == mBinding.ivBack) {
            finish();
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Socket mSocket = LixiApplication.getInstance().getSocket();
//        new LixiTrackingHelper(activity, mSocket).createNavigateTrackingSocket("recommend", null);
//    }
}
