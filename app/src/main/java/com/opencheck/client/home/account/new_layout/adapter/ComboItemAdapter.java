package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.RowComboItemBinding;
import com.opencheck.client.models.lixishop.ComboItemModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ComboItemAdapter extends RecyclerView.Adapter<ComboItemAdapter.ComboHolder> {
    // Constructor
    ArrayList<ComboItemModel> listCombo;

    public ComboItemAdapter(ArrayList<ComboItemModel> listCombo) {
        this.listCombo = listCombo;
    }

    @Override
    public ComboHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowComboItemBinding rowComboItemBinding =
                RowComboItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ComboHolder(rowComboItemBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ComboHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listCombo != null ? listCombo.size() : 0;
    }

    public class ComboHolder extends RecyclerView.ViewHolder{
        TextView txtQuant, txtVoucherName;

        public ComboHolder(View itemView) {
            super(itemView);
            txtQuant = (TextView) itemView.findViewById(R.id.txtQuant);
            txtVoucherName = (TextView) itemView.findViewById(R.id.txtVoucherName);
        }

        public void bind(){
            ComboItemModel combo = listCombo.get(getAdapterPosition());
            if (combo != null){
                txtVoucherName.setText(combo.getName());
                txtQuant.setText(combo.getQuantity() + "x");
            }
        }
    }
}
