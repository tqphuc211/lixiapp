package com.opencheck.client.home.account.activecode.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.StoreItemRowBinding;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreViewHolder> {

    private LixiActivity activity;
    private ArrayList<StoreApplyModel> arrStore;
    private ItemClickListener itemClickListener;

    public StoreAdapter(LixiActivity activity, ArrayList<StoreApplyModel> arrStore) {
        this.activity = activity;
        this.arrStore = arrStore;
    }

    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        StoreItemRowBinding storeItemRowBinding =
                StoreItemRowBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new StoreViewHolder(storeItemRowBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(StoreViewHolder holder, int position) {
        holder.rootViewItem.setTag(position);
        StoreApplyModel storeApplyModel = arrStore.get(position);
        if (position == arrStore.size() - 1)
            holder.v_line.setVisibility(View.GONE);

        holder.iv_map.setVisibility(View.GONE);
        holder.iv_map1.setVisibility(View.GONE);
        holder.tv_number.setText("" + (position + 1) + ".");
        holder.tv_name.setText(storeApplyModel.getName());
        holder.tv_address.setText(storeApplyModel.getAddress());
        holder.tv_phone.setText(storeApplyModel.getPhone());
    }

    @Override
    public int getItemCount() {
        return arrStore.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class StoreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_number, tv_name, tv_address, tv_phone;
        private ImageView iv_map, iv_map1;
        private RelativeLayout rl_info;
        private View v_line;
        private View rootViewItem;

        public StoreViewHolder(View itemView) {
            super(itemView);
            rootViewItem = itemView;
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_phone = itemView.findViewById(R.id.ed_phone);
            iv_map = itemView.findViewById(R.id.iv_map);
            iv_map1 = itemView.findViewById(R.id.iv_map1);
            v_line = itemView.findViewById(R.id.v_line);
            rl_info = itemView.findViewById(R.id.rl_info);
            rootViewItem.setOnClickListener(this);
            iv_map1.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == rootViewItem) {
                itemClickListener.onItemCLick(getAdapterPosition(), ConstantValue.ON_ITEM_STORE_CLICK, arrStore.get(getAdapterPosition()), 0L);
            } else if (view == iv_map1) {
                itemClickListener.onItemCLick(getAdapterPosition(), ConstantValue.ON_MAP_CLICK, null, 0L);
            }

        }
    }
}
