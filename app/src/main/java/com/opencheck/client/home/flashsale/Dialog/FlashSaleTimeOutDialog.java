package com.opencheck.client.home.flashsale.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogFlashSaleTimeOutBinding;
import com.opencheck.client.home.account.evoucher.Dialog.InvalidLiXiCodeDialog;
import com.opencheck.client.utils.AppPreferenceHelper;

public class FlashSaleTimeOutDialog extends LixiDialog {
    TextView txtDone;
    Activity activity;

    public FlashSaleTimeOutDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
        this.activity = _activity;
    }

    private DialogFlashSaleTimeOutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogFlashSaleTimeOutBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        setCanceledOnTouchOutside(false);
        txtDone = (TextView) findViewById(R.id.txt_done);

        txtDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        FlashSaleTimeOutDialog.this.cancel();
    }
}
