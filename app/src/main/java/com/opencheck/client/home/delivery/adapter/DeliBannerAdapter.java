package com.opencheck.client.home.delivery.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.databinding.DiscoveryRowBannerFragmentBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.activities.TopicActivity;
import com.opencheck.client.home.delivery.model.DeliBannerModel;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class DeliBannerAdapter extends PagerAdapter {

    LixiActivity activity;
    public ArrayList<DeliBannerModel> list;
    LayoutInflater inflater;

    public DeliBannerAdapter(LixiActivity _activity, ArrayList<DeliBannerModel> list) {
        this.activity = _activity;
        this.list = list;
        inflater = LayoutInflater.from(activity);
    }

    public void setListBanner(ArrayList<DeliBannerModel> arr) {
        this.list = arr;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    private DiscoveryRowBannerFragmentBinding mBinding;

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        mBinding = DiscoveryRowBannerFragmentBinding.inflate(
                LayoutInflater.from(container.getContext()), container, false);
        final DeliBannerModel model = list.get(position);
//        ImageLoader.getInstance().displayImage(model.getImage_link(),
//                mBinding.imgBanner, LixiApplication.getInstance().optionsNomal);

        LixiImage.with(activity)
                .load(model.getImage_link())
                .size(ImageSize.MEDIUM)
                .into(mBinding.imgBanner);

        mBinding.imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getObject_type() != null) {
                    Bundle data = new Bundle();
                    Intent intent = null;
                    data.putLong("ID", model.getObject_id());
                    data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.HOME);

                    if (model.getObject_type().equals(DeliBannerModel.Type.STORE.toString())) {
                        intent = new Intent(activity, StoreDetailActivity.class);
                    } else if (list.get(position).getObject_type().equals(DeliBannerModel.Type.ARTICLE.toString())) {
                        intent = new Intent(activity, StoreListOfArticleActivity.class);
                    } else if (model.getObject_type().equals(DeliBannerModel.Type.TOPIC.toString())) {
                        intent = new Intent(activity, TopicActivity.class);
                        data = new Bundle();
//                        data.putSerializable("BANNER" , model);
                        data.putLong("TOPIC_ID", model.getObject_id());
                        data.putString("TOPIC_IMAGE", model.getImage_link());
                        data.putString("TOPIC_TITLE", model.getName());
                    }

                    if (intent != null) {
                        intent.putExtras(data);
                        activity.startActivity(intent);
                    }
                }
            }
        });
        container.addView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
