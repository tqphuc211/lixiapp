package com.opencheck.client.home.merchant.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public interface LocationCallBack {
    void callback(LatLng position, ArrayList<String> addressInfo);
}