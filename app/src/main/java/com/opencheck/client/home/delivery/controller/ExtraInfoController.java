package com.opencheck.client.home.delivery.controller;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ExtraInfoItemLayoutBinding;
import com.opencheck.client.home.delivery.dialog.ServiceFeeDialog;
import com.opencheck.client.home.delivery.dialog.ShipFeeDialog;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.QuotationModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class ExtraInfoController {
    private LixiActivity activity;
    private LinearLayout lnRoot;
    private int position;
    private DeliOrderModel orderModel;
    private boolean isCheckoutScreen;
    private StoreOfCategoryModel mStore;
    private ArrayList<QuotationModel> quotations;
    private ExtraInfoItemLayoutBinding mBinding;

    public ExtraInfoController(LixiActivity _activity, StoreOfCategoryModel _mStore, DeliOrderModel _orderModel, LinearLayout _lnRoot, boolean _isCheckoutScreen, int _position, ArrayList<QuotationModel> _quotations) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.quotations = _quotations;
        this.position = _position;
        this.isCheckoutScreen = _isCheckoutScreen;
        this.orderModel = _orderModel;
        this.mStore = _mStore;

        mBinding = ExtraInfoItemLayoutBinding.inflate(LayoutInflater.from(activity), null, false);
        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(mBinding.getRoot());
            }
        });
    }

    private void initData() {
        if (isCheckoutScreen) {
            mBinding.tvKey.setTextColor(activity.getResources().getColor(R.color.color_ship_fee));
            mBinding.tvValue.setTextColor(activity.getResources().getColor(R.color.color_ship_fee));
        } else {
            mBinding.tvKey.setTextColor(activity.getResources().getColor(R.color.color_product_2));
            mBinding.tvValue.setTextColor(activity.getResources().getColor(R.color.color_product_2));
        }

        if (isCheckoutScreen)
            mBinding.ivInfo.setVisibility(View.VISIBLE);

        QuotationModel quotationModel = quotations.get(position);
        if (quotationModel.getCode().equalsIgnoreCase("ship_money")) {
            if (orderModel.getShipping_distance() >= 100) {
                double distance = (double) Math.round(((float) orderModel.getShipping_distance() / 1000) * 10) / 10;
                mBinding.tvKey.setText(Html.fromHtml(quotationModel.getName() + " (" + distance + "km)"));
            } else mBinding.tvKey.setText(Html.fromHtml(quotationModel.getName() + " (" + orderModel.getShipping_distance() + "m)"));
        } else if (quotationModel.getCode().equalsIgnoreCase("service_money")) {
            mBinding.tvKey.setText(quotationModel.getName());
        } else {
            mBinding.ivInfo.setVisibility(View.GONE);
            if (quotationModel.getCode().equalsIgnoreCase("promotion_money")
                    && orderModel.getList_promotion_code_apply() != null
                    && orderModel.getList_promotion_code_apply().size() > 0)
                mBinding.tvKey.setText(Html.fromHtml(quotationModel.getName() + " - <b>" + orderModel.getList_promotion_code_apply().get(0).getCode() + "</b>"));
            else mBinding.tvKey.setText(quotationModel.getName());
        }

        if (quotationModel.getMoney() == 0 && !quotationModel.getCode().equalsIgnoreCase("service_money")) {
            mBinding.tvValue.setText(LanguageBinding.getString(R.string.store_detail_free, activity));
        } else
            mBinding.tvValue.setText(Html.fromHtml(Helper.getVNCurrency(quotationModel.getMoney()) + "đ"));
    }

    private void initView() {
        mBinding.ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderModel.getList_quotation().get(position).getCode().equalsIgnoreCase("ship_money")) {
                    ShipFeeDialog dialog = new ShipFeeDialog(activity, false, true, false);
                    dialog.show();
                    dialog.setData(mStore, orderModel, position);
                } else {
                    ServiceFeeDialog dialog = new ServiceFeeDialog(activity, false, true, false);
                    dialog.show();
                    dialog.showData(orderModel);
                }
            }
        });
    }
}
