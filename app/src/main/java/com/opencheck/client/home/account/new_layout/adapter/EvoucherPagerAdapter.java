package com.opencheck.client.home.account.new_layout.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.home.account.new_layout.fragment.bought_evoucher.InvalidEvoucherFragment;
import com.opencheck.client.home.account.new_layout.fragment.bought_evoucher.ValidEvoucherFragment;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;

public class EvoucherPagerAdapter extends FragmentPagerAdapter {
    private LixiActivity activity;

    public EvoucherPagerAdapter(FragmentManager fm, LixiActivity activity) {
        super(fm);
        this.activity = activity;
    }

    private ValidEvoucherFragment validEvoucherFragment;
    private InvalidEvoucherFragment invalidEvoucherFragment;

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                if (validEvoucherFragment == null){
                    validEvoucherFragment = new ValidEvoucherFragment();
                }
                return validEvoucherFragment;
            case 1:
                if (invalidEvoucherFragment == null){
                    invalidEvoucherFragment = new InvalidEvoucherFragment();
                }
                return invalidEvoucherFragment;
        }

        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return activity.getString(R.string.bought_evoucher_valid);
            case 1:
                return activity.getString(R.string.bought_evoucher_invalid);
        }

        return "";
    }

    @Override
    public int getCount() {
        return 2;
    }
}
