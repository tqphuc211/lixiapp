package com.opencheck.client.home;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.custom.LixiNavigationBar;
import com.opencheck.client.custom.notification.NotiViewManager;
import com.opencheck.client.databinding.HomeActivityBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.deeplink.ApplicationLifecycle;
import com.opencheck.client.firebase.FCMInstantIDService;
import com.opencheck.client.firebase.SendPushTokenController;
import com.opencheck.client.home.LixiHomeV25.LixiHomeFragment;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.account.new_layout.activity.IntroCodeActivity;
import com.opencheck.client.home.account.new_layout.fragment.UserFragment;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.activities.TopicActivity;
import com.opencheck.client.home.delivery.dialog.UpdateLocationDialog;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.refactor.LocationSelectionFragment;
import com.opencheck.client.home.game.GameActivity;
import com.opencheck.client.home.history.HistoryFragment;
import com.opencheck.client.home.lixishop.LixiShopFragment;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.newlogin.communicate.OnAgreeUpdate;
import com.opencheck.client.home.newlogin.communicate.OnLoginResult;
import com.opencheck.client.home.newlogin.dialog.UpdatePasswordDialog;
import com.opencheck.client.home.newlogin.dialog.UpdateUserInfoDialog;
import com.opencheck.client.home.notification.NotificationListingActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.user.ImageModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.BottomNavigationHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.LocationHelper;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.List;
import java.util.Locale;

import static com.opencheck.client.login.LoginHelper.isLoggedIn;
import static com.opencheck.client.login.LoginHelper.startLoginActivityWithAction;

public class HomeActivity extends LixiActivity {

    public static HomeActivity mInstance;

    public static HomeActivity getInstance() {
        return mInstance;
    }

    //region Layout Variable
    public LixiNavigationBar navigation;
    public FrameLayout fr_main;
    //endregion

    //region Set up navigation

    public void visibleBottomNavigationView(int visible) {
        if (navigation != null)
            navigation.setVisibility(visible);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Analytics.homeActivityScreenTrack(item.getItemId());
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startHomeFragment();
//                    Analytics.trackScreenView("home", null);
                    return true;
                case R.id.navigation_delivery:
                    startLocationFragment();
                    try {
                        locationSelectionFragment.recheckUserConfiguration();
                        updateAddress();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    return true;
                case R.id.navigation_history:
                    if (!isLoggedIn(activity))
                        startLoginActivityWithAction(activity, ConstantValue.ACTION_TAB_HISTORY);
                    else {
                        startHistoryFragment();
//                        Analytics.trackScreenView("order-history", null);
                    }
                    return true;
                case R.id.navigation_shop:
                    startLixiShopFragment();
//                    Analytics.trackScreenView("order-shop", null);
                    return true;
                case R.id.navigation_user_info:
                    if (!isLoggedIn(activity))
                        startLoginActivityWithAction(activity, ConstantValue.ACTION_TAB_ACCOUNT);
                    else {
                        startAccountFragment();
//                        Analytics.trackScreenView("profile-main", null);
                    }
                    return true;
            }
            return false;
        }


    };


    //endregion

    //region handle tab fragment

//    private static int SEP_SDK = 20;

//    public boolean isNewSDK() {
//        return Build.VERSION.SDK_INT > SEP_SDK;
//    }

    //    private HomeFragment homeFragment;
    private LixiHomeFragment homeFragment;
    private LixiShopFragment lixiShopFragment;
    private HistoryFragment historyFragment;
    public UserFragment userFragment;
    private LocationSelectionFragment locationSelectionFragment;

    public void recreateFragment() {
        removeFragment();
        createFragment();
        setUpFragment();
    }

    private void removeFragment() {
        try {
            getSupportFragmentManager().beginTransaction().remove(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().remove(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().remove(historyFragment).commit();
            getSupportFragmentManager().beginTransaction().remove(locationSelectionFragment).commit();
            getSupportFragmentManager().beginTransaction().remove(userFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().remove(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(historyFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(locationSelectionFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(userFragment).commitAllowingStateLoss();
        }

    }

    private void createFragment() {
        homeFragment = new LixiHomeFragment();
        lixiShopFragment = new LixiShopFragment();
        historyFragment = new HistoryFragment();
        locationSelectionFragment = new LocationSelectionFragment();
        userFragment = new UserFragment();
    }

    private void startHomeFragment() {
        try {
            getSupportFragmentManager().beginTransaction().show(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().show(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commitAllowingStateLoss();
        }
    }

    private void startLixiShopFragment() {
        try {
            getSupportFragmentManager().beginTransaction().show(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().show(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commitAllowingStateLoss();
        }
    }

    private void startHistoryFragment() {
        try {
            getSupportFragmentManager().beginTransaction().show(historyFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().show(historyFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commitAllowingStateLoss();
        }
    }

    private void startAccountFragment() {
        try {
            getSupportFragmentManager().beginTransaction().show(userFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().show(userFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(locationSelectionFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commitAllowingStateLoss();
        }
    }

    private void startLocationFragment() {
        try {
            getSupportFragmentManager().beginTransaction().show(locationSelectionFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commit();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().show(locationSelectionFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(userFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().hide(historyFragment).commitAllowingStateLoss();
        }
    }

    private void setUpFragment() {
        try {
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, homeFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, lixiShopFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, historyFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, locationSelectionFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, userFragment).commit();
        } catch (IllegalStateException e) {
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, homeFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, lixiShopFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, historyFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, locationSelectionFragment).commitAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().add(R.id.fr_main, userFragment).commitAllowingStateLoss();
        }
    }

    private LocationManager locationManager;

    public void hideFragment() {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().remove(locationSelectionFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .commit();
    }

    public void replaceFragment(LixiFragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fr_main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    //endregion

    public boolean active = false;
    public boolean isLogout = false;
    public int tab = -1;
    public boolean isFirstLauch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("mPush", "Home create");
        appPreferenceHelper = new AppPreferenceHelper(activity);
        mLocationHelper = new LocationHelper(activity);
        if (!isTaskRoot()) {
            finish();
            return;
        }
        mInstance = this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.home_activity);

        // title
        if (ConfigModel.getInstance(this).getShop_tabbar_title() != null && !ConfigModel.getInstance(this).getShop_tabbar_title().equals("")) {
            mBinding.navigation.getMenu().getItem(3).setTitle(ConfigModel.getInstance(this).getShop_tabbar_title());
        }

        // icon
        if (ConfigModel.getInstance(this).getShop_tabbar_icon() != null) {
            mBinding.navigation.setImage(3, ConfigModel.getInstance(this).getShop_tabbar_icon());
        }

        createFragment();
        setUpFragment();
        initUI();
        if (activity != null)
            new loadData().execute();
        try {
            getAppPreferenceHelper().setUpdateTokenInterval(0l);
        } catch (Exception ex) {
        }
        AppPreferenceHelper.getInstance().setUpdateAddressTime(-1);

        NotiViewManager.getInstance().hasNotify();
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
        if (isLogout) {
            recreateFragment();
            navigation.setSelectedItemId(R.id.navigation_delivery);
            isLogout = false;
        }
        gotoTab();
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    public void gotoTab() {
        if (tab == -1)
            return;
        switch (tab) {
            case 1:
                navigation.setSelectedItemId(R.id.navigation_delivery);
                break;
            case 2:
                navigation.setSelectedItemId(R.id.navigation_home);
                break;
            case 3:
                navigation.setSelectedItemId(R.id.navigation_history);
                break;
            case 4:
                navigation.setSelectedItemId(R.id.navigation_shop);
                break;
            case 5:
                navigation.setSelectedItemId(R.id.navigation_user_info);
                break;
        }
        tab = -1;
    }

    private HomeActivityBinding mBinding;
    private AppPreferenceHelper appPreferenceHelper;

    public AppPreferenceHelper getAppPreferenceHelper() {
        if (appPreferenceHelper == null)
            appPreferenceHelper = new AppPreferenceHelper(activity);
        return appPreferenceHelper;
    }

    private LocationHelper mLocationHelper;

    private void remindUpdateInfo() {
        if (UserModel.getInstance() != null) {
            if (UserModel.getInstance().isNeed_update()) {
                if (getAppPreferenceHelper().getRemindUpdateTime() < 0) {
                    // chưa lưu
                    showRemindDialog();
                } else {
                    // đã lưu
                    long offsetTime = getAppPreferenceHelper().getRemindUpdateTime() + ConfigModel.getInstance(activity).getRemind_update_info_interval();
                    if (offsetTime > System.currentTimeMillis()) {
                        // chưa tới giờ remind
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (UserModel.getInstance() != null) {
                                    if (UserModel.getInstance().isNeed_update()) {
                                        showRemindDialog();
                                    }
                                }
                            }
                        }, offsetTime - System.currentTimeMillis());
                    } else {
                        showRemindDialog();
                    }
                }
            }
        }
    }

    private void showRemindDialog() {
        getAppPreferenceHelper().setRemindUpdateTime(System.currentTimeMillis());
        UpdatePasswordDialog.getInstance(activity).show();
        UpdatePasswordDialog.getInstance(activity).setOnAgreeUpdate(new OnAgreeUpdate() {
            @Override
            public void onAgree() {
                updateUserInfo();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getAppPreferenceHelper().getUserToken().equals("JWT ")) {
                    if (UserModel.getInstance().isNeed_update()) {
                        remindUpdateInfo();
                    }
                }
            }
        }, ConfigModel.getInstance(activity).getRemind_update_info_interval());
    }

    private UpdateUserInfoDialog updateDialog;

    private void updateUserInfo() {
        if (updateDialog == null) {
            updateDialog = new UpdateUserInfoDialog(activity, UserModel.getInstance().getPhone());
        }
        updateDialog.cancel();
        updateDialog.show();
        updateDialog.setOnUpdateInfo(new OnLoginResult() {
            @Override
            public void onLogin(boolean result, Object obj) {
                activity.hideKeyboard();
                if (result) {
                    if (userFragment != null) {
                        userFragment.refreshAll();
                    }
                }
            }
        });
        UpdatePasswordDialog.getInstance(activity).setCanShow(false);
    }

    private void checkUpdate() {
        if (UserModel.getInstance() != null) {
            if (UserModel.getInstance().isNeed_update()) {
                updateUserInfo();
            }
        }
    }

    private void checkUpdateAddress() {
        if (AppPreferenceHelper.getInstance().getDefaultLocation() != null) {
            if (checkLocationPermission() && Helper.checkGPSStatus(activity)) {
                mLocationHelper.requestLocationGoogle(new LocationHelper.LocationCallBack() {
                    @Override
                    public void onResponse(final LocationHelper.Task task) {
                        if (task.isSuccess()) {
                            String fullAddress = getAddressFromLocation(task.getLocation().getLatitude(), task.getLocation().getLongitude());
                            if (fullAddress != null && fullAddress.length() > 0) {
                                if (fullAddress.contains("Vietnam"))
                                    fullAddress = fullAddress.replace("Vietnam", "Việt Nam");

                                //tính khoảng cách giữa 2 địa điểm
                                if (AppPreferenceHelper.getInstance().getDefaultLocation() == null) {
                                    return;
                                }

                                Location defaultLocation = new Location("");
                                defaultLocation.setLatitude(AppPreferenceHelper.getInstance().getDefaultLocation().getLat());
                                defaultLocation.setLongitude(AppPreferenceHelper.getInstance().getDefaultLocation().getLng());
                                float distance = task.getLocation().distanceTo(defaultLocation);

                                if (distance > 500) {
                                    AppPreferenceHelper.getInstance().setUpdateAddressTime(System.currentTimeMillis());
                                    UpdateLocationDialog updateDialog = new UpdateLocationDialog(activity, false, true, false);
                                    updateDialog.show();
                                    updateDialog.setData(AppPreferenceHelper.getInstance().getDefaultLocation().getFullAddress());
                                    final String finalFullAddress = fullAddress;
                                    updateDialog.setIDialogEventListener(new UpdateLocationDialog.IDialogEvent() {
                                        @Override
                                        public void onUpdate() {
                                            try {
                                                String placeName = finalFullAddress.substring(0, finalFullAddress.contains(",") ? finalFullAddress.indexOf(",") : finalFullAddress.length());
                                                DeliAddressModel addressModel = new DeliAddressModel(finalFullAddress, placeName, task.getLocation().getLatitude(), task.getLocation().getLongitude(), true);
                                                AppPreferenceHelper.getInstance().setDefaultLocation(addressModel);
                                                DeliDataLoader.checkChangeProvinceHeader(activity);
                                                if (LocationSelectionFragment.deliHomeShowing)
                                                    locationSelectionFragment.getDeliveryHomeFragment().onRefresh();
                                                else
                                                    locationSelectionFragment.getDefaultUserConfiguration();
                                            } catch (Exception e) {
                                                Crashlytics.logException(new CrashModel(finalFullAddress));
                                            }
                                        }
                                    });

                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private void updateAddress() {
        try {
            long timer = AppPreferenceHelper.getInstance().getUpdateAddressTime();
            if (timer < 0 || System.currentTimeMillis() - timer > 30 * 60 * 1000) {
                checkUpdateAddress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAddressFromLocation(double lat, double lng) {
        String res = "";
        Geocoder geocoder = new Geocoder(activity, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));
                    if (i != returnedAddress.getMaxAddressLineIndex())
                        strReturnedAddress.append(", ");
                }

                res = strReturnedAddress.toString();
            } else {
                Log.w("mapi", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private boolean checkLocationPermission() {
        return (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    private void initUI() {

        navigation = findViewById(R.id.navigation);
        fr_main = findViewById(R.id.fr_main);

        BottomNavigationHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setSelectedItemId(R.id.navigation_delivery);
        //navigation.setItemIconTintList(null);

        // phóng to icon
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigation.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }
    }

    @Override
    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.frameLixiShop:
//                navigation.setSelectedItemId(R.id.navigation_shop);
//                break;
//        }
    }

    public void handleDeepLink() {
        Uri uri = getIntent().getData();
        if (uri != null) {
            String id = "";
            try {
                id = uri.getQueryParameter("id");
            } catch (Exception ex) {

            }
            String storeId = "";
            try {
                storeId = uri.getQueryParameter("store_id");
            } catch (Exception ex) {

            }
            String articleId = "";
            try {
                articleId = uri.getQueryParameter("article_id");
            } catch (Exception ex) {

            }
            String type = uri.getQueryParameter("type");
            String objectId = uri.getQueryParameter("object_id");
            if (type == null) return;
            switch (type) {
                case "merchant":
                    MerchantModel merchantModel = new MerchantModel();
                    merchantModel.setId(id);
                    Intent merchant = new Intent(this, MerchantDetailActivity.class);
                    Bundle data = new Bundle();
                    data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                    data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
                    merchant.putExtras(data);
                    startActivity(merchant);
                    break;
                case "evoucher":
                    //TODO ADD COMBO CHECK
                    long product_id = Long.parseLong(id);
//                Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, product_id);
//                activity.startActivity(intent);
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(this, product_id, TrackingConstant.ContentSource.DEEP_LINK);
                    break;
                case "deli_store":
                    //Intent storeIntent = new Intent(activity, StoreDetailActivity.class);
                    long store_id = 0;
                    try {
                        store_id = Long.parseLong(id);
                    } catch (Exception ex) {

                    }
                    try {
                        if (storeId.length() > 0)
                            store_id = Long.parseLong(storeId);
                        Log.d(ApplicationLifecycle.DEEPLINK, "store_id s: " + store_id);
                    } catch (Exception ex) {
                        Log.d(ApplicationLifecycle.DEEPLINK, "store_id e: " + store_id);
                    }
                    Log.d(ApplicationLifecycle.DEEPLINK, "store_id: " + storeId);
                    StoreDetailActivity.startStoreDetailActivity(this, store_id, TrackingConstant.ContentSource.DEEP_LINK);
                    AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(true);
                    break;
                case "deli_article":
//                Intent articleIntent = new Intent(activity, StoreListOfArticleActivity.class);
//
//                Bundle bundle1 = new Bundle();
//                bundle1.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
//                bundle1.putLong("ID", article_id);
                    long article_id = 0;

                    try {
                        article_id = Long.parseLong(id);
                    } catch (Exception ex) {

                    }
                    try {
                        if (articleId.length() > 0)
                            article_id = Long.parseLong(articleId);
                        Log.d(ApplicationLifecycle.DEEPLINK, "article_id s: " + article_id);
                    } catch (Exception ex) {
                        Log.d(ApplicationLifecycle.DEEPLINK, "article_id e: " + article_id);
                    }
                    Log.d(ApplicationLifecycle.DEEPLINK, "article_id: " + articleId);

                    if (objectId == null) {
                        objectId = "0";
                    }
                    StoreListOfArticleActivity.startArticleActivity(this, Long.parseLong(objectId), article_id, "", "", TrackingConstant.ContentSource.DEEP_LINK);
                    AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(true);
                    break;
                case "reference_code":
                    IntroCodeActivity.start(this);
                    break;
                case "deli_topic":
                    Bundle dataTopic = new Bundle();
                    dataTopic.putLong("TOPIC_ID", Long.valueOf(objectId));
                    Intent intentTopic = new Intent(this, TopicActivity.class);
                    intentTopic.putExtras(dataTopic);
                    startActivity(intentTopic);
                    break;
                case "deli_order":
                    Bundle dataOrder = new Bundle();
                    String orderId = uri.getQueryParameter("order_id");
                    orderId = orderId.replace("UO", "");
                    if (LoginHelper.isLoggedIn(activity)) {
                        OrderTrackingActivity.startOrderTrackingActivity(activity, orderId, null, false);
                    } else {
                        store_order_id = orderId;
                        LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_VIEW_ORDER);
                    }
                    break;
                case "deli_noty":
                    Intent intent = new Intent(this, NotificationListingActivity.class);
                    startActivity(intent);
                    break;
                case "game_detail":
                    if (LoginHelper.isLoggedIn(activity)) {
                        Intent intentGame = new Intent(activity, GameActivity.class);
                        intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
                        activity.startActivity(intentGame);
                    } else {
                        LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_PLAY_GAME);
                    }
                    break;
                case "open_tabbar":
                    tab = Integer.parseInt(id) + 1;
                    gotoTab();
                    break;
            }
        }
        getIntent().setData(null);
    }

    String store_order_id = "";
    //region background task

    @SuppressLint("StaticFieldLeak")
    private class loadData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {
        }
    }

    //endregion

    //region PUSH NOTIFICATION

    private void setUpTimeOutReceiver() {
        final IntentFilter intentFilter = new IntentFilter(ConstantValue.TIMER_OUT);
        registerReceiver(expiredReceiver, intentFilter);
    }

    BroadcastReceiver expiredReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("mapi", getClassNameTop() + "/" + EVoucherAccountActivity.class.getName());
            if ((!getClassNameTop().equals(EVoucherAccountActivity.class.getName())
                    && !getClassNameTop().equals(ActiveCodeActivity.class.getName()))
                    || (getClassNameTop().equals(ActiveCodeActivity.class.getName()) && intent.getLongExtra("id", 0L) != ConstantValue.ID)
                    || (getClassNameTop().equals(EVoucherAccountActivity.class.getName()) && intent.getLongExtra("id", 0L) != ConstantValue.ID)) {
                ConstantValue.map.remove(String.valueOf(intent.getLongExtra("id", 0L)));
                int icon = R.drawable.icon_app_small_trans;
                int mNotificationId = Integer.parseInt(Helper.getTimestamp());
                Intent notificationIntent = new Intent(context, EVoucherAccountActivity.class);
                Bundle data = new Bundle();
                data.putLong(ConstantValue.EVOUCHER_ID, intent.getLongExtra("id", 0L));
                notificationIntent.putExtras(data);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(context, (int) (Math.random() * 100), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
                Notification notification = mBuilder.setSmallIcon(icon).setTicker("").setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(LanguageBinding.getString(R.string.expired_activate, activity))
                        .setContentIntent(resultPendingIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentText(String.format(LanguageBinding.getString(R.string.out_of_time_activate, activity), intent.getStringExtra("name"), intent.getStringExtra("code")))
                        .build();

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(mNotificationId, notification);

            }
        }
    };

    private void setTimerReceiver() {
        IntentFilter intentFilter = new IntentFilter(ConstantValue.TIMER);
        registerReceiver(timerReceiver, intentFilter);
    }

    BroadcastReceiver timerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                long duration = intent.getLongExtra("timer", 0L);
                final long id = intent.getLongExtra("evoucher_id", 0L);
                final String name = intent.getStringExtra("evoucher_name");
                final String pin = intent.getStringExtra("evoucher_code");
                CountDownTimer mCountDownTimer = new CountDownTimer(duration, 1000) {
                    @Override
                    public void onTick(long l) {
                        Log.d("mapi", l + " tick /" + id);
                    }

                    @Override
                    public void onFinish() {
                        Log.d("mapi", "finished");
                        Intent timeOutReceiver = new Intent(ConstantValue.TIMER_OUT);
                        timeOutReceiver.putExtra("id", id);
                        timeOutReceiver.putExtra("name", name);
                        timeOutReceiver.putExtra("code", pin);
                        sendBroadcast(timeOutReceiver);
                        this.cancel();
                    }
                };

                mCountDownTimer.start();

                ConstantValue.timer.put(String.valueOf(id), mCountDownTimer);
                Log.d("mapi", ConstantValue.timer.size() + " / " + mCountDownTimer.hashCode());
            }
        }
    };

    private String getClassNameTop() {
        String mClassName = "";
        ActivityManager mActivityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        mClassName = mActivityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        return mClassName;
    }

    private void setUpActiveReceiver() {
        final IntentFilter intentFilter = new IntentFilter(ConstantValue.PUSH_EVOUCHER_ACTIVE);
        registerReceiver(activeReceiver, intentFilter);
    }

    public void captureNewImage() {
        ConstantValue.PHOTO_NAME = "photo_" + Helper.getTimestamp() + ".jpg";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Helper.getPhotoFileUri(activity, ConstantValue.PHOTO_NAME));
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR);
        }
    }

    BroadcastReceiver activeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent == null)
                return;
            if (intent.getStringExtra("ev_id") != null && ConstantValue.timer.containsKey(intent.getStringExtra("ev_id"))) {
                ConstantValue.timer.get(intent.getStringExtra("ev_id")).cancel();
                ConstantValue.timer.remove(intent.getStringExtra("ev_id"));
                ConstantValue.map.remove(intent.getStringExtra("ev_id"));
            }
        }
    };

    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                handleLoginResultAction(data, resultCode);
                break;
            case ConstantValue.REQUEST_CODE_READ_CONTACTS:
                lixiShopFragment.onActivityResult(requestCode, resultCode, data);
                break;
            case ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR:
                if (resultCode == RESULT_OK) {
                    Uri takenPhotoUri = Helper.getPhotoFileUri(ConstantValue.PHOTO_NAME);
                    String sId = takenPhotoUri.getLastPathSegment();
                    Long id = Long.parseLong(Helper.getNumberFromString(sId));
                    ImageModel imageAdd = new ImageModel(id, ConstantValue.PHOTO_NAME, takenPhotoUri.getPath(), Helper.getTimestamp(), false);
                    userFragment.uploadAvatar(imageAdd);
                }
                break;
            case LocationHelper.LOCATION_PERMISSION:
            case LocationHelper.LOCATION_PERMISSION_GOOGLE:
                if (locationSelectionFragment != null && navigation.getSelectedItemId() == R.id.navigation_delivery)
                    locationSelectionFragment.onActivityResult(requestCode, resultCode, data);
                if (homeFragment != null && navigation.getSelectedItemId() == R.id.navigation_home) {
                    homeFragment.onActivityResult(requestCode, resultCode, data);
                }
                break;
            case ConstantValue.REQUEST_LIST_PROMOTION:
                if(resultCode == RESULT_OK) {
                    navigation.setSelectedItemId(R.id.navigation_delivery);
                }
                break;

        }
    }

    private void handleLoginResultAction(Intent intent, int resultCode) {
        if (resultCode != RESULT_OK) {
            navigation.setSelectedItemId(R.id.navigation_delivery);
            return;
        }

        Bundle data = intent.getExtras();
        String action = data.getString(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        if (userFragment != null) {
            userFragment.refresh();
        }
        switch (action) {
            case ConstantValue.ACTION_TAB_HISTORY:
                checkUpdate();
                navigation.setSelectedItemId(R.id.navigation_history);
                break;
            case ConstantValue.ACTION_TAB_ACCOUNT:
                checkUpdate();
                navigation.setSelectedItemId(R.id.navigation_user_info);
                break;
            case ConstantValue.ACTION_TOPUP:
            case ConstantValue.ACTION_BUY_CARD:
                lixiShopFragment.onActivityResult(ConstantValue.LOGIN_REQUEST_CODE,
                        RESULT_OK, intent);
                break;
            case ConstantValue.ACTION_LOGIN_QR:
                homeFragment.onActivityResult(ConstantValue.LOGIN_REQUEST_CODE, RESULT_OK, intent);
                break;
            case ConstantValue.ACTION_INTRO_CODE:
                IntroCodeActivity.start(activity);
                break;
            case ConstantValue.ACTION_PLAY_GAME:
                Intent intentGame = new Intent(activity, GameActivity.class);
                intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
                startActivity(intentGame);
                break;
            case ConstantValue.ACTION_VIEW_ORDER:
                OrderTrackingActivity.startOrderTrackingActivity(activity, store_order_id, null, false);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR:
                userFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
            case ConstantValue.REQUEST_CODE_WRITE_EXTERNAL_STORAGE_CHANGING_AVATAR:
                userFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
            case ConstantValue.REQUEST_CODE_READ_CONTACTS:
            case ConstantValue.REQUEST_CODE_READ_CONTACTS_PERMISSION:
                lixiShopFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (homeFragment != null) {
                        homeFragment.getLocation();
                    }
                } else {
                    Toast.makeText(activity, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                    if (homeFragment != null) {
                        homeFragment.selectPreTab();
                    }
                }
                break;
            case LocationHelper.LOCATION_PERMISSION:
            case LocationHelper.LOCATION_PERMISSION_GOOGLE:
                if (locationSelectionFragment != null && navigation.getSelectedItemId() == R.id.navigation_delivery)
                    locationSelectionFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                if (homeFragment != null && navigation.getSelectedItemId() == R.id.navigation_home) {
                    homeFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                break;
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public static final String RESUME_NAVIGATE = "RESUME_NAVIGATE";

    @Override
    protected void onResume() {
        super.onResume();
        handleDeepLink();
//        navigation.setVisibility(View.VISIBLE);
        visibleBottomNavigationView(View.VISIBLE);
        setUpTimeOutReceiver();
        sendPushTokenToServer();
        setUpActiveReceiver();
        setTimerReceiver();
        Helper.removeExpiredCart(activity);

        if (UserModel.getInstance() != null) {
            if (UserModel.getInstance().isIs_phone_verified()) {
                remindUpdateInfo();
            } else {
                new SendPushTokenController(activity, new AppPreferenceHelper(this).getPushToken(), true);
                DataLoader.clearDeviceToken();
                Helper.clearCart(activity);
                DataLoader.initAPICall(activity, "JWT ", null);
                DeliDataLoader.initAPICall(activity, "JWT ");
                LoginManager.getInstance().logOut();
                new AppPreferenceHelper(activity).clear();
            }
        }

        if (navigation.getSelectedItemId() == R.id.navigation_delivery)
            updateAddress();

        if (UserFragment.UPDATE_USER_INFO) {
            userFragment.refreshAll();
        }
        // loop config voucher
        if (getAppPreferenceHelper().getTimeResetConfig() + ConstantValue.TIME_RESET_CONFIG >= System.currentTimeMillis()) {
            if (handleGetConfig == null) {
                handleGetConfig = new Handler();
            }
            handleGetConfig.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DataLoader.getUserConfig(activity);
                    handleGetConfig.removeCallbacks(this);
                }
            }, getAppPreferenceHelper().getTimeResetConfig() + ConstantValue.TIME_RESET_CONFIG - System.currentTimeMillis());
        } else {
            DataLoader.getUserConfig(activity);
        }

        // loop config deli
        if (getAppPreferenceHelper().getTimeResetDeliConfig() + ConstantValue.TIME_RESET_CONFIG >= System.currentTimeMillis()) {
            if (handleGetDeliConfig == null) {
                handleGetDeliConfig = new Handler();
            }
            handleGetDeliConfig.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DeliDataLoader.getUserConfig(activity);
                    handleGetDeliConfig.removeCallbacks(this);
                }
            }, getAppPreferenceHelper().getTimeResetDeliConfig() + ConstantValue.TIME_RESET_CONFIG - System.currentTimeMillis());
        } else {
            DeliDataLoader.getUserConfig(activity);
        }
    }

    private Handler handleGetConfig = null;
    private Handler handleGetDeliConfig = null;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int gotoTab = intent.getIntExtra(RESUME_NAVIGATE, -1);
        switch (gotoTab) {
            case 0:
                navigation.setSelectedItemId(R.id.navigation_delivery);
                break;
            case 1:
                navigation.setSelectedItemId(R.id.navigation_home);
                break;
            case 2:
                navigation.setSelectedItemId(R.id.navigation_history);
                break;
            case 3:
                navigation.setSelectedItemId(R.id.navigation_shop);
                break;
            case 4:
                navigation.setSelectedItemId(R.id.navigation_user_info);
                break;
        }
    }

    private void sendPushTokenToServer() {
        (new FCMInstantIDService()).getToken();

        Long createDate = getAppPreferenceHelper().getUpdateTokenInterval();
        if (createDate == 0L) {
            new SendPushTokenController(this, new AppPreferenceHelper(this).getPushToken(), false);
            return;
        }

        if (System.currentTimeMillis() - createDate > 380000) {
            Helper.showLog("TT send push token");
            new SendPushTokenController(this, new AppPreferenceHelper(this).getPushToken(), false);
        }
    }

    @Override
    protected void onDestroy() {
        Log.d("mPush", "Home destroy");
        try {
            unregisterReceiver(expiredReceiver);
            unregisterReceiver(activeReceiver);
            unregisterReceiver(timerReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    //region HANDLE BACK PRESS

    public Fragment getcurrentFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
        return currentFragment;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            super.onBackPressed();
        else
            handleExitApp();
    }

    private boolean isExitable = false;

    private void handleExitApp() {
        if (!isExitable) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.back_to_exit, activity), Toast.LENGTH_SHORT).show();
            Runnable turnoffExit = new Runnable() {
                @Override
                public void run() {
                    isExitable = false;
                }
            };
            Handler handler = new Handler();
            handler.postDelayed(turnoffExit, 2000);
            isExitable = true;
        } else {
            LixiApplication.isAppRunning = false;
            finish();
        }

    }
    //endregion
}
