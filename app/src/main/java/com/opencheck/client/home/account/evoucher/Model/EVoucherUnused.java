package com.opencheck.client.home.account.evoucher.Model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.Arrays;
import java.util.List;

public class EVoucherUnused extends LixiModel {

    private String product_cover_image;
    private long expired_time;
    private List<String> product_image_medium = null;
    private long price;
    private String name;
    private long user;
    private long product;
    private String state;
    private String serial;
    private String pin;
    private boolean is_generated;
    private long id;
    private String merchant_pin;
    private boolean send_pin;

    public String getProduct_cover_image() {
        return product_cover_image;
    }

    public void setProduct_cover_image(String product_cover_image) {
        this.product_cover_image = product_cover_image;
    }

    public long getExpired_time() {
        return expired_time;
    }

    public void setExpired_time(long expired_time) {
        this.expired_time = expired_time;
    }

    public List<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(List<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isIs_generated() {
        return is_generated;
    }

    public void setIs_generated(boolean is_generated) {
        this.is_generated = is_generated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMerchant_pin() {
        return merchant_pin;
    }

    public void setMerchant_pin(String merchant_pin) {
        this.merchant_pin = merchant_pin;
    }

    public boolean isSend_pin() {
        return send_pin;
    }

    public void setSend_pin(boolean send_pin) {
        this.send_pin = send_pin;
    }
}
