package com.opencheck.client.home.delivery.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentProcessDialogBinding;
import com.opencheck.client.dataloaders.ApiEntity;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.product.gateway.DialogTransactions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentProcessDialog extends LixiDialog {
    public PaymentProcessDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private String payment_url, patternUrl;
    private boolean isDone = false;

    private PaymentProcessDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.payment_process_dialog, null, false);
        setContentView(mBinding.getRoot());
    }

    public void setData(String payment_link, String patternUrl) {
        this.payment_url = payment_link;
        this.patternUrl = patternUrl;
        setViewData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isDone)
            eventListener.onOk(true);
        else eventListener.onOk(false);
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.rlClose) {
            if (isDone)
                eventListener.onOk(true);
            else eventListener.onOk(false);
        }
        dismiss();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setViewData() {
        mBinding.rlClose.setOnClickListener(this);

        mBinding.wv.getSettings().setJavaScriptEnabled(true);
        mBinding.wv.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mBinding.wv.getSettings().setDomStorageEnabled(true);
        mBinding.wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(activity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        dismiss();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Here put your code
                Log.d("Payment Url", url);
                if (payment_url.equals(url))
                    return false;//Allow WebView to load url
                else {
                    if (isValidUrl(url) > 0) {
                        isDone = true;
                        eventListener.onOk(true);
                        dismiss();
                        return true; //Indicates WebView to NOT load the url;
                    } else {
                        Log.d("Wrong redirect url", url);
                        return false;
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mBinding.progressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mBinding.progressBar.setVisibility(View.GONE);
                Log.d("aaa", "finish");
                super.onPageFinished(view, url);
            }
        });
        mBinding.wv.loadUrl(payment_url);
    }

    private int isValidUrl(String url) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(patternUrl);
        matcher = pattern.matcher(url);
        int count = 0;

        while (matcher.find()) {
            count++;
        }
        return count;
    }

    public interface IDialogEvent {
        void onOk(boolean isDone);
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
