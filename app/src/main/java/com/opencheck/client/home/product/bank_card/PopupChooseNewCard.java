package com.opencheck.client.home.product.bank_card;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PopupChooseBankBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.flashsale.Global.FlashSaleTimerController;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.BankModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.List;

public class PopupChooseNewCard extends LixiTrackingDialog {

    public PopupChooseNewCard(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    RelativeLayout relBack, relFacebookMessenger;
    RecyclerView recBankCard;
    TextView txtConfirm;
    EditText edtSearch;
    ImageView imgVoiceSearch;
    LinearLayout linearFlashSale;
    ProgressBar progressBar;

    ChooseBankCardAdapter adapter;
    FlashSaleTimerController fsControler;
    ProductCashVoucherDetailModel productModel;

    private PopupChooseBankBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = PopupChooseBankBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        initView();

        txtConfirm.setOnClickListener(this);
    }

    private void initView(){
        relBack = (RelativeLayout) findViewById(R.id.rlBack);
        relFacebookMessenger = (RelativeLayout) findViewById(R.id.rlFacebookMessenger);
        linearFlashSale = (LinearLayout) findViewById(R.id.linearFlashSale);

        recBankCard = (RecyclerView) findViewById(R.id.recBankCard);
        txtConfirm = (TextView) findViewById(R.id.txtConfirm);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        imgVoiceSearch = (ImageView) findViewById(R.id.imgVoiceSearch);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        relBack.setOnClickListener(this);
        relFacebookMessenger.setOnClickListener(this);

        imgVoiceSearch.setOnClickListener(this);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable.toString());
            }
        });
    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{"en-US", "vi-VN"});
        intent.putExtra("android.speech.extra.LANGUAGE", new String[]{"vi-VN", "en-US"});
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nói gì đó");
        activity.startActivityForResult(intent, ConstantValue.VOICE_SEARCH_CODE);
    }

    public void setVoiceSearchData(String result){
        edtSearch.setText(result);
        edtSearch.setSelection(result.length());
    }

    public void setData(List<BankModel> list, ProductCashVoucherDetailModel productModel){
        this.productModel = productModel;

        adapter = new ChooseBankCardAdapter(activity, (ArrayList<BankModel>) list);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        recBankCard.setLayoutManager(manager);
        recBankCard.setAdapter(adapter);

        linearFlashSale.removeAllViews();
        if (productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {
            fsControler = new FlashSaleTimerController(activity, linearFlashSale);
        }

        id = this.productModel.getId();
        trackScreen();
//        Socket socket = LixiApplication.getInstance().getSocket();
//        new LixiTrackingHelper(activity, socket).createNavigateTrackingSocket("payment-step2-bank-card", this.productModel.getId());
    }

    public void updateFsTimer(int[] arrTime) {
        if (fsControler != null)
            fsControler.showTime(arrTime);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtConfirm:
                if (adapter.getBankModel() == null){
                    // thông báo
                    Toast.makeText(activity, LanguageBinding.getString(R.string.cash_evoucher_miss_choose_card, activity), Toast.LENGTH_SHORT).show();
                }else {
                    eventListener.onNext(adapter.getBankModel());
                }
                break;
            case R.id.rlBack:
                cancel();
                break;
            case R.id.rlFacebookMessenger:
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                break;
            case R.id.imgVoiceSearch:
                startVoiceInput();
                break;
        }
    }

    public interface ChooseNewBankEventListener {
        void onNext(BankModel bankModel);
    }

    public void setChooseNewBankEventListener(PopupChooseNewCard.ChooseNewBankEventListener eventListener) {
        this.eventListener = eventListener;
    }

    ChooseNewBankEventListener eventListener;

    public void setShowProgress(boolean isShow){
        progressBar.setVisibility(isShow ? View.VISIBLE : View.GONE);
        txtConfirm.setEnabled(!isShow);
    }
}
