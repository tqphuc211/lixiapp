package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogUnavailablePaymentWithPromotionBinding;
import com.opencheck.client.utils.LanguageBinding;

public class UnavailablePaymentWithPromotionDialog extends LixiDialog {

    public UnavailablePaymentWithPromotionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private DialogUnavailablePaymentWithPromotionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_unavailable_payment_with_promotion, null, false);
        setContentView(mBinding.getRoot());
        mBinding.tvAgree.setOnClickListener(this);
        mBinding.tvDisagree.setOnClickListener(this);
        mBinding.rlDismiss.setOnClickListener(this);
    }

    public void setData(String promotionCode, String paymentMethod) {
        mBinding.tvContent.setText(String.format(LanguageBinding.getString(R.string.content_unavailable_payment_with_promotion, activity), promotionCode, paymentMethod));
    }

    @Override
    public void onClick(View view) {
        if (mBinding.tvAgree == view) {
            eventListener.onOk(true);
        } else if (mBinding.tvDisagree == view) {
            eventListener.onOk(false);
        }
        dismiss();
    }

    public interface IDialogEvent {
        void onOk(boolean isAgree);
    }

    private IDialogEvent eventListener;

    public void setIDialogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
