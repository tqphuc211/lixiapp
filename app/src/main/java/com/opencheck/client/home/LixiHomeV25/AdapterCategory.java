package com.opencheck.client.home.LixiHomeV25;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowCategoryItemLayoutBinding;
import com.opencheck.client.home.LixiHomeV25.CategoryContent.ActivityCategoryDetail;
import com.opencheck.client.models.ProductCategoryModel;

import java.util.ArrayList;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.ViewHolder> {

    private LixiActivity activity;

    public void setList_category(ArrayList<ProductCategoryModel> list_category) {
        this.list_category = list_category;
        notifyDataSetChanged();
    }

    private ArrayList<ProductCategoryModel> list_category;
    private boolean isChildCategory = false;

    public AdapterCategory(LixiActivity activity, ArrayList<ProductCategoryModel> listCategory) {
        this.activity = activity;
        this.list_category = listCategory;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowCategoryItemLayoutBinding rowCategoryItemLayoutBinding =
                RowCategoryItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(rowCategoryItemLayoutBinding.getRoot());

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.rlItemCategory.getLayoutParams();
        layoutParams.width = activity.widthScreen / 5;
        holder.rlItemCategory.setLayoutParams(layoutParams);
        ImageLoader.getInstance().displayImage(list_category.get(position).getBanner(), holder.imgLogoCategory, LixiApplication.getInstance().optionsNomal);
        holder.tvCatogeryName.setText(list_category.get(position).getName());
        holder.rootView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if (list_category == null)
            return 0;
        return list_category.size();

    }

    public boolean isChildCategory() {
        return isChildCategory;
    }

    public void setChildCategory(boolean childCategory) {
        isChildCategory = childCategory;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        private ImageView imgLogoCategory;
        private TextView tvCatogeryName;
        private RelativeLayout rlItemCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imgLogoCategory = (ImageView) itemView.findViewById(R.id.imgLogoCategory);
            tvCatogeryName = (TextView) itemView.findViewById(R.id.tvCatogeryName);
            rlItemCategory = (RelativeLayout) itemView.findViewById(R.id.rlItemCategory);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCategoryDetail.startActivityCategoryDetail(activity, list_category.get((int) v.getTag()).getId(), isChildCategory, ActivityCategoryDetail.CATEGORY_NAME + ":" + list_category.get((int) v.getTag()).getName());
                }
            });
        }
    }
}
