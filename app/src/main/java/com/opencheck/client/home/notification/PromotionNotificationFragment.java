package com.opencheck.client.home.notification;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentNotificationBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionNotificationFragment extends LixiFragment implements AbsListView.OnScrollListener {

    public PromotionNotificationFragment() {
        // Required empty public constructor
    }

    private NotificationV1Adapter mAdapter;
    boolean noRemain = false;
    private ArrayList<NotificationModel> listNoti = new ArrayList<>();

    private int page = 1;
    private Boolean isLoading = false;
    private FragmentNotificationBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        initViews();
        return binding.getRoot();
    }

    private void initViews() {
        binding.lvItem.setOnScrollListener(this);
        binding.tvLogin.setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initData();
            }
        }, 200);
    }

    private void initData() {
        setAdapter();

        binding.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listNoti.clear();
                page = 1;
                isLoading = false;
                noRemain = false;
                mAdapter.clear();
//                setAdapter();
                getNotification();
            }
        });

        if (!LoginHelper.isLoggedIn(activity)) {
            binding.llLogin.setVisibility(View.VISIBLE);
        } else {
            getNotification();
        }
    }

    private void getNotification() {
        if (!isLoading) {
            isLoading = true;
            DataLoader.getNotificationList(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ArrayList<NotificationModel> _list = (ArrayList<NotificationModel>) object;
                        listNoti.addAll(_list);
                        mAdapter.addListItems(_list);
                        page++;
                        isLoading = false;
                    } else {
                        isLoading = true;
                    }
                    binding.swipeLayout.setRefreshing(false);
                    checkData();
                }
            }, page, "ads");
        }

    }

    private void checkData() {
        binding.llLogin.setVisibility(View.GONE);
        if (mAdapter.getListItems().size() <= 0) {
            binding.lnNoResult.setVisibility(View.VISIBLE);
        } else {
            binding.lnNoResult.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        mAdapter = new NotificationV1Adapter(activity, R.layout.notification_item_adapter, listNoti);
        binding.lvItem.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view == binding.tvLogin) {
            LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_NOTIFICATION);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!isLoading && totalItemCount > 0) {
            if (firstVisibleItem >= totalItemCount - visibleItemCount - 4) {
                getNotification();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstantValue.LOGIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getNotification();
            } else {
                if (resultCode == ConstantValue.RESULT_NO_PROFILE) {
                    LoginHelper.Logout(activity);
                    Toast.makeText(activity, "Đăng nhập thất bại. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
