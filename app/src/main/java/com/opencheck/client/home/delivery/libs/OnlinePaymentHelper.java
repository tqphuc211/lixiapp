package com.opencheck.client.home.delivery.libs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.google.gson.JsonObject;
import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;

import java.util.HashMap;
import java.util.Map;

import vn.momo.momo_partner.AppMoMoLib;
import vn.zalopay.sdk.ZaloPayErrorCode;
import vn.zalopay.sdk.ZaloPayListener;
import vn.zalopay.sdk.ZaloPaySDK;

public class OnlinePaymentHelper {
    private LixiActivity mContext;
    private int mType;
    private OnlinePaymentListener mOnlinePaymentListener;
    private String mSubmitUrl;
    private HashMap<String, Object> mSdkParams;

    private OnlinePaymentHelper(Builder builder) {
        mContext = builder.mContext;
        mType = builder.mType;
        mSubmitUrl = builder.mSubmitUrl;
        mSdkParams = builder.mSdkParams;
        mOnlinePaymentListener = builder.mOnlinePaymentListener;

        if (mType == 0) {
            setUpMomoApp();
        } else setUpZaloApp();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO) {
            JsonObject postData = new JsonObject();
            postData.addProperty("data", data.getStringExtra("data"));
            postData.addProperty("phonenumber", data.getStringExtra("phonenumber"));
            submitToAppServer(postData);
            return;
        }
        ZaloPaySDK.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("WrongConstant")
    private boolean checkZaloPayInstalled() {
        try {
            PackageManager pm = mContext.getPackageManager();
            pm.getPackageInfo(ZaloPaySDK.PACKAGE_IN_PLAY_STORE, 1);
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    private void setUpZaloApp() {
        ZaloPaySDK.getInstance().initWithAppId(((Double) mSdkParams.get("appid")).intValue());
        ZaloPaySDK.getInstance().payOrder(mContext, (String) mSdkParams.get("zptranstoken"), new ZaloPayListener() {
            @Override
            public void onPaymentSucceeded(String transactionId, String transToken) {
                mOnlinePaymentListener.onSuccess();
            }

            @Override
            public void onPaymentError(ZaloPayErrorCode zaloPayErrorCode, int paymentErrorCode, String zpTransToken) {
                // thanh toán thất bại
                switch (zaloPayErrorCode) {
                    case ZALO_PAY_NOT_INSTALLED:
                        ZaloPaySDK.getInstance().navigateToStore(mContext);
                        mOnlinePaymentListener.onFailed(zaloPayErrorCode.getValue());
                        break;
                    case EXCEPTION_PARSING_RESPONSE:
                    case INVALID_RESPONSE:
                    case USER_CANCEL:
                    case PAYMENT_ERROR:
                        mOnlinePaymentListener.onFailed(zaloPayErrorCode.getValue());
                        break;
                }
            }
        });
    }

    private void setUpMomoApp() {
        String packageClass;
        if (BuildConfig.DEBUG) {
            AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT);
            packageClass = "com.mservice";
        } else {
            AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.PRODUCTION);
            packageClass = "com.mservice.momotransfer";
        }
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);
        Map<String, Object> postData = new HashMap<>();
        for (Map.Entry<String, Object> entry : mSdkParams.entrySet()) {
            postData.put(entry.getKey(), entry.getValue() == null ? null : entry.getValue().toString());
        }
        AppMoMoLib.getInstance().requestMoMoCallBack(mContext, postData);

        if (!checkInstalledMomo(packageClass)) {
            mOnlinePaymentListener.onFailed(1);
        }
    }

    private boolean checkInstalledMomo(String uri) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed = true;

        try {
            pm.getPackageInfo(uri, 0);
        } catch (PackageManager.NameNotFoundException var6) {
            app_installed = false;;
        }

        return app_installed;
    }

    private void submitToAppServer(JsonObject params) {
        DataLoader.submitToMomo(mContext, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mOnlinePaymentListener.onSuccess();
                } else mOnlinePaymentListener.onFailed(1);
            }
        }, params, mSubmitUrl);
    }

    public static class Builder {
        private LixiActivity mContext;
        private int mType;
        private OnlinePaymentListener mOnlinePaymentListener;
        private String mSubmitUrl;
        private HashMap<String, Object> mSdkParams;

        public Builder(LixiActivity mContext) {
            this.mContext = mContext;
        }

        public Builder setType(int type) {
            this.mType = type;
            return this;
        }

        public Builder setSubmitUrl(String submitUrl) {
            this.mSubmitUrl = submitUrl;
            return this;
        }

        public Builder setSdkParams(HashMap<String, Object> sdkParams) {
            this.mSdkParams = sdkParams;
            return this;
        }

        public Builder setOnlinePaymentListener(OnlinePaymentListener onlinePaymentListener) {
            this.mOnlinePaymentListener = onlinePaymentListener;
            return this;
        }

        public OnlinePaymentHelper build() {
            return new OnlinePaymentHelper(this);
        }
    }

    public interface OnlinePaymentListener {
        void onSuccess();

        void onFailed(int errorCode);
    }
}
