package com.opencheck.client.home.product.bank_card;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowChooseSaveCardBinding;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;
import com.opencheck.client.home.product.communicate.OnUseSavedCard;
import com.opencheck.client.home.product.model.TokenData;

import java.util.ArrayList;

public class SavedBankCardAdapter extends RecyclerView.Adapter<SavedBankCardAdapter.CardHolder> {
    private LixiActivity activity;
    private ArrayList<TokenData> listTokenBankCard;

    public SavedBankCardAdapter(LixiActivity activity, ArrayList<TokenData> listTokenBankCard) {
        this.activity = activity;
        this.listTokenBankCard = listTokenBankCard;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowChooseSaveCardBinding rowChooseSaveCardBinding =
                RowChooseSaveCardBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new CardHolder(rowChooseSaveCardBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(CardHolder holder, int position) {
        if (listTokenBankCard.size() > 0) {
            TokenData tokenData = listTokenBankCard.get(position);
            if (tokenData != null) {
                ImageLoader.getInstance().displayImage(tokenData.getCard().getLogo_url(), holder.imgBankLogo);
                String cardName = tokenData.getCard().getName() == null ? tokenData.getCard().getBank_name() : tokenData.getCard().getName();
                holder.txtCardName.setText(cardName);
                String[] format = formatString(tokenData.getCard().getNumber());
                holder.txtCardNumer.setText(format[0] + "-****-****-" + format[1]);
            }
        }
    }

    public static String[] formatString(String source) {
        String header = source.substring(0, 4);
        String ender = source.substring(source.length() - 4, source.length());
        return new String[]{header, ender};
    }

    @Override
    public int getItemCount() {
        if (listTokenBankCard != null) {
            return listTokenBankCard.size();
        } else {
            return 0;
        }
    }


    private OnDeleteBankCard onDeleteBankCard;
    private OnUseSavedCard onUseSavedCard;

    public void addOnDeleteListener(OnDeleteBankCard onDeleteBankCard) {
        this.onDeleteBankCard = onDeleteBankCard;
    }

    public void addOnUseSavedCard(OnUseSavedCard onUseSavedCard) {
        this.onUseSavedCard = onUseSavedCard;
    }

    public class CardHolder extends RecyclerView.ViewHolder {
        ImageView imgBankLogo, imgDelete;
        TextView txtCardNumer, txtCardName;

        public CardHolder(View itemView) {
            super(itemView);
            imgBankLogo = (ImageView) itemView.findViewById(R.id.imgBankLogo);
            imgDelete = (ImageView) itemView.findViewById(R.id.imgDelete);

            txtCardNumer = (TextView) itemView.findViewById(R.id.txtCardNumber);
            txtCardName = (TextView) itemView.findViewById(R.id.txtCardName);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupDeleteBankCard popupDeleteBankCard = new PopupDeleteBankCard(activity, false, false, false);
                    popupDeleteBankCard.show();
                    popupDeleteBankCard.setDeleteEventListener(new OnDeleteBankCard() {
                        @Override
                        public void onDelete(int position) {
                            if (onDeleteBankCard != null) {
                                onDeleteBankCard.onDelete(getAdapterPosition());
                            }
                        }
                    });
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onUseSavedCard != null) {
                        onUseSavedCard.onUse(listTokenBankCard.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
