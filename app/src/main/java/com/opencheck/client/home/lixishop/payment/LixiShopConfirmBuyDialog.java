package com.opencheck.client.home.lixishop.payment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopConfirmBuyDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.models.lixishop.LixiShopTransactionResultModel;
import com.opencheck.client.models.lixishop.QuantityProductModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.List;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class LixiShopConfirmBuyDialog extends LixiTrackingDialog {
    public LixiShopConfirmBuyDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private static final int MAX_MONEY_ALLOWED = 500000000;

    public static final String PRODUCT_MODEL = "PRODUCT_MODEL";

    private RelativeLayout rl_header;
    private TextView tv_title;
    private ImageView iv_back;
    private ImageView iv_sub;
    private ImageView iv_add;
    private LinearLayout ll_name;
    private TextView tv_product_name;
    private LinearLayout ll_count;
    private TextView tv_count;
    private LinearLayout ll_phone;
    private TextView tv_phone;
    private LinearLayout ll_acc;
    private TextView tv_acc;
    private LinearLayout ll_price;
    private TextView tv_price;
    private LinearLayout ll_remain;
    private TextView tv_remain;
    private TextView tv_ship;
    private LinearLayout ll_support;
    private LinearLayout ll_buy;
    private LinearLayout ll_cash;
    private LinearLayout ll_cashback;
    private TextView tv_buy;
    private TextView tv_cash;
    private TextView tv_cashback;
    private TextView tv_sms_message;
    private TextView tv_message;
    private LixiShopProductDetailModel productModel;
    private TextView tv_last_point;
    private UserModel userInfo;
    private TextView tvLimit;
    private LinearLayout lnNote;
    private QuantityProductModel quantityProductModel;
    private String idProduct;
    private LinearLayout lnCount;
    int sl = 1;
    long id = -1;
    //region setup dialog

    public void setData(long id, String idProduct, LixiShopProductDetailModel productModel) {
        this.idProduct = idProduct;
        this.id = id;
        this.productModel = productModel;
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LoadQuantityAllow();
                    setViewData();
                } else {
                    Helper.showErrorDialog(activity, "Thông tin người dùng không hợp lệ!");
                    dismiss();
                }
            }
        });
    }

    private void LoadQuantityAllow() {
        DataLoader.getProductQuantity(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    quantityProductModel = (QuantityProductModel) object;

                    if (quantityProductModel.getMax_lixi_quantity_allowed() == 0) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                    } else lnCount.setClickable(false);

                    if (quantityProductModel.getLimit_gift() > 0) {
                        lnNote.setVisibility(View.VISIBLE);
                        tvLimit.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_limit_product, activity), quantityProductModel.getLimit_gift()));

                    } else {
                        lnNote.setVisibility(View.GONE);
                    }
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, Long.parseLong(idProduct));

    }

    private LixiShopConfirmBuyDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopConfirmBuyDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();

    }

    private void findViews() {
        lnCount = (LinearLayout) findViewById(R.id.lnCount);
        tvLimit = (TextView) findViewById(R.id.tvLimit);
        lnNote = (LinearLayout) findViewById(R.id.lnNote);
        rl_header = (RelativeLayout) findViewById(R.id.rl_header);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        ll_name = (LinearLayout) findViewById(R.id.ll_name);
        tv_product_name = (TextView) findViewById(R.id.tv_product_name);
        ll_count = (LinearLayout) findViewById(R.id.ll_count);
        tv_count = (TextView) findViewById(R.id.tv_count);
        ll_phone = (LinearLayout) findViewById(R.id.ll_phone);
        tv_phone = (TextView) findViewById(R.id.ed_phone);
        ll_acc = (LinearLayout) findViewById(R.id.ll_acc);
        tv_acc = (TextView) findViewById(R.id.tv_acc);
        ll_price = (LinearLayout) findViewById(R.id.ll_price);
        tv_price = (TextView) findViewById(R.id.tv_price);
        ll_remain = (LinearLayout) findViewById(R.id.ll_remain);
        tv_remain = (TextView) findViewById(R.id.tv_remain);
        tv_ship = (TextView) findViewById(R.id.tv_ship);
        ll_support = (LinearLayout) findViewById(R.id.ll_support);
        ll_buy = (LinearLayout) findViewById(R.id.ll_buy);
        ll_cash = (LinearLayout) findViewById(R.id.ll_cash);
        ll_cashback = (LinearLayout) findViewById(R.id.ll_cashback);
        tv_buy = (TextView) findViewById(R.id.tv_buy);
        tv_cash = (TextView) findViewById(R.id.tv_cash);
        tv_cashback = (TextView) findViewById(R.id.tv_cashback);
        tv_sms_message = (TextView) findViewById(R.id.tv_sms_message);
        iv_sub = (ImageView) findViewById(R.id.iv_sub);
        iv_add = (ImageView) findViewById(R.id.iv_add);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_last_point = (TextView) findViewById(R.id.tv_last_point);
        iv_back.setOnClickListener(this);
        iv_sub.setOnClickListener(this);
        iv_add.setOnClickListener(this);
        tv_buy.setOnClickListener(this);
        ll_support.setOnClickListener(this);
        lnCount.setOnClickListener(this);
        lnCount.setClickable(false);
    }

    public void initDialogEvent() {

    }


    public void setViewData() {
        UserModel userModel = UserModel.getInstance();

        userInfo = userModel;

        tv_product_name.setText(productModel.getName());
        tv_count.setText("1");
        tv_phone.setText(userModel.getPhone());
        tv_acc.setText(userModel.getFullname());
        tv_remain.setText(Helper.getVNCurrency(Long.parseLong(userModel.getAvailable_point()) - productModel.getDiscount_price()) + "đ");

        if (productModel.getProduct_type().equals(LixiShopProductDetailModel.PRODUCT_TYPE_VOUCHER))
            tv_ship.setVisibility(View.GONE);
        else
            tv_ship.setText(productModel.getShipping_info());

        tv_cashback.setText(productModel.getCashback_percent() + "%");
        tv_cash.setText(Helper.getVNCurrency(productModel.getPayment_discount_price()) + "VNĐ");
        tv_price.setText(Helper.getVNCurrency(productModel.getDiscount_price()) + " " + activity.getString(R.string.lixi));
        if (!productModel.getProduct_type().equalsIgnoreCase("evoucher"))
            tv_message.setText(productModel.getShipping_info());
        else
            tv_message.setText(Html.fromHtml(LanguageBinding.getString(R.string.popup_lixi_shop_confirm_buy_code_will, activity) +  " " + "<b>" + userInfo.getPhone() + "</b>"));

        tv_last_point.setText(Helper.getVNCurrency(Integer.parseInt(userModel.getAvailable_point()) - productModel.getDiscount_price()) + " " + activity.getResources().getString(R.string.lixi));
    }

    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_sub:
                sl = Math.max(1, sl - 1);
                if (sl == 1) {
                    iv_sub.setVisibility(View.GONE);
                }
                iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                iv_add.setVisibility(View.VISIBLE);
                iv_add.setClickable(true);
                tv_count.setText(sl + "");
                tv_price.setText(Helper.getVNCurrency(productModel.getDiscount_price() * sl) + " " + activity.getResources().getString(R.string.lixi));
                tv_cash.setText(Helper.getVNCurrency(productModel.getPayment_discount_price() * sl) + "VNĐ");
                tv_last_point.setText(Helper.getVNCurrency(Long.parseLong(userInfo.getAvailable_point()) - (productModel.getDiscount_price() * sl)) + " " + activity.getResources().getString(R.string.lixi));
                lnCount.setClickable(false);
                break;
            case R.id.iv_add:
//                sl = Math.min(sl + 1, productModel.getQuantity());
                sl = sl + 1;
                iv_sub.setVisibility(View.VISIBLE);
                float totalPayment = productModel.getDiscount_price() * sl;
                if (MAX_MONEY_ALLOWED / productModel.getDiscount_price() == sl) {
                    //    iv_add.setVisibility(View.GONE);
                    iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                    iv_add.setClickable(false);
                    //    sl--;
                }
//
//                if (MAX_MONEY_ALLOWED / productModel.getPayment_discount_price() == sl) {
//                    //    iv_add.setVisibility(View.GONE);
//                    iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
//                    iv_add.setClickable(false);
//                    //    sl--;
//                }

                try {
                    if (sl == quantityProductModel.getMax_lixi_quantity_allowed()) {
                        iv_add.setImageResource(R.drawable.ic_shop_plus_gray);
                        iv_add.setClickable(false);
                        lnCount.setClickable(true);
                        Toast toast = Toast.makeText(activity, "Bạn chỉ có thể mua thêm " + quantityProductModel.getMax_lixi_quantity_allowed() + " sản phẩm nữa", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        iv_add.setImageResource(R.drawable.lixi_shop_ic_add);
                        iv_add.setClickable(true);
                        lnCount.setClickable(false);
                    }

                } catch (Exception e) {
                }
                ;

                tv_count.setText(sl + "");
                tv_cash.setText(Helper.getVNCurrency(productModel.getPayment_discount_price() * sl) + "VNĐ");
                tv_price.setText(Helper.getVNCurrency(productModel.getDiscount_price() * sl) + " " + activity.getResources().getString(R.string.lixi));
                tv_last_point.setText(Helper.getVNCurrency(Long.parseLong(userInfo.getAvailable_point()) - (productModel.getDiscount_price() * sl)) + " " + activity.getResources().getString(R.string.lixi));
                break;
            case R.id.tv_buy:
                if (quantityProductModel.getMax_lixi_quantity_allowed() == 0) {
                    Toast toast = Toast.makeText(activity, "Bạn đã mua vượt số lượng cho phép", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    buyProduct();
                }
                break;
            case R.id.ll_support:
                Helper.callPhoneSupport(activity);
                break;

            case R.id.lnCount:
                if (quantityProductModel.getMax_lixi_quantity_allowed() == 0) {
                    Toast toast = Toast.makeText(activity, "Bạn đã mua vượt số lượng cho phép", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(activity, "Bạn chỉ có thể mua thêm " + quantityProductModel.getMax_lixi_quantity_allowed() + " sản phẩm nữa", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;
        }
    }


    //region dialog callback

    public void onEventOk(boolean b) {
        if (eventListener != null) {
            eventListener.onOk(b);
        }
    }

    public interface IDidalogEvent {
        public boolean onOk(boolean success);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    public void buyProduct() {
        JsonObject obj = new JsonObject();
        obj.addProperty("id", productModel.getId());
        obj.addProperty("quantity", sl);
        if (!productModel.getExchange_method().equals("v2"))
            buyProductV1(obj);
        else buyProductV2(obj);
    }


    private void buyProductV1(JsonObject obj) {
        DataLoader.exchangeLixiShopProduct(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LixiShopTransactionResultModel transactionResult = (LixiShopTransactionResultModel) object;
                    String ms = transactionResult.getMessage().get(0).getText();
                    ms = getMessage(ms, transactionResult.getMessage().get(0).getParams());
                    SuccessDialog dialog = new SuccessDialog(activity, false, true, false);
                    onEventOk(true);
                    dismiss();
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setData(activity.getResources().getString(R.string.popup_lixi_shop_confirm_buy_succ), ms, "", transactionResult);
                } else {
                    showErrorToast((String) object);
                }
            }
        }, obj);
    }

    private void buyProductV2(JsonObject obj) {
        DataLoader.exchangeLixiShopProductV2(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    LixiShopTransactionResultModel transactionResult = (LixiShopTransactionResultModel) object;
                    String ms = transactionResult.getMessage().get(0).getText();
                    ms = getMessage(ms, transactionResult.getMessage().get(0).getParams());
                    SuccessDialog dialog = new SuccessDialog(activity, false, true, false);
                    onEventOk(true);
                    dismiss();
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setData(activity.getResources().getString(R.string.popup_lixi_shop_confirm_buy_succ), ms, "", transactionResult);
                } else {
                    showErrorToast((String) object);
                }
            }
        }, obj);
    }
    //endregion

    private String getMessage(String ms, List<String> par) {
        String res = ms;

        if (par != null) {
            for (int i = 0; i < par.size(); i++) {
                String holder = "{" + String.valueOf(i) + "}";

                if (res.contains(holder)) {
                    res = res.replace(holder, "<b>" + par.get(i) + "</b>");
                }
            }
        }

        return res;
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
