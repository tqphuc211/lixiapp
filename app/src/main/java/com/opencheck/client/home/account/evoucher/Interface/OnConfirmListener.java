package com.opencheck.client.home.account.evoucher.Interface;

public interface OnConfirmListener {
    void onConfirm(boolean result);
}
