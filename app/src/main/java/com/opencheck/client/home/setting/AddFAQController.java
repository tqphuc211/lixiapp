package com.opencheck.client.home.setting;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemFaqBinding;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class AddFAQController implements View.OnClickListener {

    private Activity activity;
    private View viewChild;
    private LinearLayout lnRoot;
    private int limitSize;
    private int position = 0;

    private RelativeLayout rlItemFAQ;
    private TextView tv_question;
    private View viewLine;

    private String content = "";
    private String answers = "";
    private Dialog dialog = null;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }


    public AddFAQController(Activity _activity, String _content, String _answer, int _position, int _limitSize, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.limitSize = _limitSize;
        this.content = _content;
        this.answers = _answer;

        this.viewChild = View.inflate(activity, R.layout.item_faq, null);

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    public AddFAQController(Activity _activity, Dialog dialog, String _content, String _answer, int _position, int _limitSize, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.limitSize = _limitSize;
        this.content = _content;
        this.answers = _answer;
        this.dialog = dialog;
        ItemFaqBinding itemFaq = ItemFaqBinding.inflate(LayoutInflater.from(activity),
                null, false);
        this.viewChild = itemFaq.getRoot();

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }



    private void initView() {
        rlItemFAQ = (RelativeLayout) viewChild.findViewById(R.id.rlItemFAQ);
        tv_question = (TextView) viewChild.findViewById(R.id.tv_question);
        viewLine = viewChild.findViewById(R.id.viewLine);

    }

    private void initData() {
        tv_question.setText(content);
        rlItemFAQ.setOnClickListener(this);
//        if (position == limitSize - 1) {
//            viewLine.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onClick(View v) {
        if (v == rlItemFAQ) {
            InstructiondDetailDialog popupInstructiondDetail = new InstructiondDetailDialog((LixiActivity) activity, false, true, false);
            popupInstructiondDetail.show();
            popupInstructiondDetail.setData(content, answers);
        }
    }
}