package com.opencheck.client.home.account.new_layout.model;

import com.opencheck.client.models.lixishop.ComboItemModel;

import java.util.ArrayList;
import java.util.List;

public class DetailTransaction {

    /**
     * billing_code : string
     * date_order : 0
     * evoucher_info : [{"discount_price":0,"expired_time":0,"id":0,"payment_discount_price":0,"pin":"string","price":0,"product":0,"send_pin":true,"send_serial":true,"serial":"string"}]
     * id : 0
     * order_uuid : string
     * payment_expired_time : 0
     * payment_key : string
     * payment_method : string
     * product_info : {"combo_item":[{"id":0,"name":"string","quantity":0}],"id":0,"name":"string","price":0,"product_type":"string","quantity":0}
     * status : string
     * total_cashback : 0
     * total_discount : 0
     * total_price : 0
     */

    private String billing_code;
    private long date_order;
    private ArrayList<EvoucherInfo> evoucher_info;
    private int id;
    private String order_uuid;
    private long payment_expired_time;
    private String payment_key;
    private String payment_method;
    private ProductInfo product_info;
    private String status;
    private int total_cashback;
    private int total_discount;
    private int total_price;


    public String getBilling_code() {
        return billing_code;
    }

    public void setBilling_code(String billing_code) {
        this.billing_code = billing_code;
    }

    public long getDate_order() {
        return date_order;
    }

    public void setDate_order(long date_order) {
        this.date_order = date_order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder_uuid() {
        return order_uuid;
    }

    public void setOrder_uuid(String order_uuid) {
        this.order_uuid = order_uuid;
    }

    public long getPayment_expired_time() {
        return payment_expired_time;
    }

    public void setPayment_expired_time(long payment_expired_time) {
        this.payment_expired_time = payment_expired_time;
    }

    public String getPayment_key() {
        return payment_key;
    }

    public void setPayment_key(String payment_key) {
        this.payment_key = payment_key;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public ProductInfo getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductInfo product_info) {
        this.product_info = product_info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotal_cashback() {
        return total_cashback;
    }

    public void setTotal_cashback(int total_cashback) {
        this.total_cashback = total_cashback;
    }

    public int getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(int total_discount) {
        this.total_discount = total_discount;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public ArrayList<EvoucherInfo> getEvoucher_info() {
        return evoucher_info;
    }

    public void setEvoucher_info(ArrayList<EvoucherInfo> evoucher_info) {
        this.evoucher_info = evoucher_info;
    }
}
