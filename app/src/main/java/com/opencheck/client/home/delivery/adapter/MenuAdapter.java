package com.opencheck.client.home.delivery.adapter;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemProductOfSearchMenuLayoutBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> implements Filterable {

    private LixiActivity activity;
    private ArrayList<ProductModel> list;
    private ArrayList<ProductModel> listFilter = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private int count = 0;

    public MenuAdapter(LixiActivity activity, ArrayList<ProductModel> list) {
        this.activity = activity;
        this.list = list;
    }

    public void setListData(ArrayList<ProductModel> list) {
        this.list = list;
        listFilter.clear();
        listFilter.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductOfSearchMenuLayoutBinding itemProductOfSearchMenuLayoutBinding =
                ItemProductOfSearchMenuLayoutBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemProductOfSearchMenuLayoutBinding.getRoot());
    }

    public void clearData() {
        this.listFilter.clear();
        notifyDataSetChanged();
    }

    public void updateQuantityForFilterList(int pos, int count) {
        if (listFilter == null || listFilter.size() == 0)
            return;
        listFilter.get(pos).setQuantity(count);
        notifyItemChanged(pos);
    }

    public int findIndexOfItem(ProductModel productModel) {
        int index = 0;
        for (ProductModel model : listFilter) {
            if (productModel.isHave_addon() && model.getId() == productModel.getId()) {
                return index;
            }
            index++;
        }
        return -1;
    }

    public void updateQuantityForList(int pos, int count) {
        if (list == null || list.size() == 0)
            return;
        list.get(pos).setQuantity(count);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ProductModel item = listFilter.get(position);
        viewHolder.tv_name.setText(item.getName());
        viewHolder.tv_price.setText("" + Helper.getVNCurrency(item.getPrice()) + "đ");
        if (item.getDescription() != null && !item.getDescription().equals("")) {
            viewHolder.tv_description.setText(item.getDescription());
            viewHolder.tv_description.setVisibility(View.VISIBLE);
        } else viewHolder.tv_description.setVisibility(View.GONE);
        ImageLoader.getInstance().displayImage(item.getImage_link(), viewHolder.iv_food, LixiApplication.getInstance().optionsNomal);

        if (item.getQuantity() < 1) {
            viewHolder.tv_count.setVisibility(View.GONE);
            viewHolder.iv_sub.setVisibility(View.GONE);
        } else {
            viewHolder.tv_count.setVisibility(View.VISIBLE);
            viewHolder.iv_sub.setVisibility(View.VISIBLE);
            viewHolder.tv_count.setText("" + item.getQuantity());
        }

        if (item.getState().equals("paused")) {
            viewHolder.tv_no_result.setVisibility(View.VISIBLE);
            viewHolder.lnCount.setVisibility(View.GONE);
        } else {
            viewHolder.tv_no_result.setVisibility(View.GONE);
            viewHolder.lnCount.setVisibility(View.VISIBLE);
        }

        if (item.getPrice() < item.getOriginal_price()) {
            viewHolder.tv_original_price.setVisibility(View.VISIBLE);
            viewHolder.tv_original_price.setText(Helper.getVNCurrency(item.getOriginal_price()) + "đ");
            viewHolder.tv_original_price.setPaintFlags(viewHolder.tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            viewHolder.tv_original_price.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return listFilter == null ? 0 : listFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String key = charSequence.toString();
                if (key.isEmpty()) {
                    listFilter.clear();
                } else {
                    List<ProductModel> filteredList = new ArrayList<>();
                    for (ProductModel productModel : list) {
                        String temp = productModel.getName().toLowerCase();
                        if (formatString(temp).contains(formatString(key.toLowerCase()))) {
                            filteredList.add(productModel);
                        }
                    }

                    listFilter = (ArrayList<ProductModel>) filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = listFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFilter = (ArrayList<ProductModel>) filterResults.values;
                if (listFilter.size() > 0)
                    onDataChangeListener.onDataChange(false);
                else onDataChangeListener.onDataChange(true);
                notifyDataSetChanged();
            }
        };
    }

    private String formatString(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView iv_food, iv_add, iv_sub;
        private TextView tv_name, tv_price, tv_description, tv_count, tv_original_price, tv_no_result;
        private View rootView;
        private LinearLayout lnCount;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_no_result = itemView.findViewById(R.id.tv_no_result);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_count = itemView.findViewById(R.id.tv_count);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_original_price = itemView.findViewById(R.id.tv_original_price);
            iv_add = itemView.findViewById(R.id.iv_add);
            iv_sub = itemView.findViewById(R.id.iv_sub);
            iv_food = itemView.findViewById(R.id.iv_food);
            lnCount = itemView.findViewById(R.id.lnCount);

            iv_sub.setOnClickListener(this);
            iv_add.setOnClickListener(this);
            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == iv_sub) {
                count = listFilter.get(getAdapterPosition()).getQuantity() - 1;
                if (!listFilter.get(getAdapterPosition()).isHave_addon()) {
                    tv_count.setText("" + count);
                    listFilter.get(getAdapterPosition()).setQuantity(count);
                }
                if (count < 1) {
                    tv_count.setVisibility(View.GONE);
                    iv_sub.setVisibility(View.GONE);
                }

                itemClickListener.onItemClickListener(rootView, getAdapterPosition(), ConstantValue.SUB_CLICK, listFilter.get(getAdapterPosition()), (long) count);
            } else if (view == iv_add) {
                if (StoreDetailActivity.isWatch) {
                    Toast.makeText(activity, "Xin lỗi không thể thêm sản phẩm này. Vui lòng quay lại sau", Toast.LENGTH_SHORT).show();
                    return;
                }

                count = listFilter.get(getAdapterPosition()).getQuantity() + 1;
                if (!listFilter.get(getAdapterPosition()).isHave_addon()) {
                    tv_count.setVisibility(View.VISIBLE);
                    iv_sub.setVisibility(View.VISIBLE);
                    tv_count.setText("" + count);
                    listFilter.get(getAdapterPosition()).setQuantity(count);
                }
                itemClickListener.onItemClickListener(rootView, getAdapterPosition(), ConstantValue.ADD_CLICK, listFilter.get(getAdapterPosition()), (long) count);
            }
        }
    }

    public interface OnDataChangeListener {
        void onDataChange(boolean isEmpty);
    }

    private OnDataChangeListener onDataChangeListener;

    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        this.onDataChangeListener = onDataChangeListener;
    }

}
