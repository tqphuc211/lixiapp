package com.opencheck.client.home.lixishop.header;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.CardPaymentDoneDialogBinding;
import com.opencheck.client.models.lixishop.PaymentModel;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 3/22/2018.
 */

public class CardPaymentDoneDialog extends LixiDialog {
    public CardPaymentDoneDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }
    private View view;

    private RelativeLayout rlTop;
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private View viewLineTop;
    private TextView tvInfo;
    private TextView tvInfo1;
    private LinearLayout lnStatus;
    private TextView tvPrice;

    private PaymentModel paymentModel = new PaymentModel();

    private CardPaymentDoneDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = CardPaymentDoneDialogBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    public void setData(PaymentModel _paymentModel) {
        paymentModel = _paymentModel;
        initData();
    }


    private void initView() {
        rlTop = findViewById(R.id.rlTop);
        rlBack = findViewById(R.id.rlBack);
        tvTitle = findViewById(R.id.tvTitle);
        viewLineTop = findViewById(R.id.viewLineTop);
        tvInfo = findViewById(R.id.tvInfo);
        tvInfo1 = findViewById(R.id.tvInfo1);
        lnStatus = findViewById(R.id.lnStatus);
        tvPrice = findViewById(R.id.tvPrice);
    }

    private void initData() {
        rlBack.setOnClickListener(this);
        lnStatus.setOnClickListener(this);

        tvInfo1.setText("Quý khách đã thanh toán thành công " + Helper.getVNCurrency(Long.parseLong(paymentModel.getMoney_payment())) + "đ cho dịch vụ Internet " + paymentModel.getName());
        tvPrice.setText(Helper.getVNCurrency(Long.parseLong(paymentModel.getMoney_payment())) + "đ");
    }



    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            dismiss();
        } else if (v == lnStatus) {
            dismiss();
        }

    }

}
