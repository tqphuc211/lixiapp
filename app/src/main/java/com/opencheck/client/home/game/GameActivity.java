package com.opencheck.client.home.game;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityGameBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.game.GameModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GameActivity extends LixiActivity {

    private ActivityGameBinding mBinding;
    private Retrofit.Builder builder = null;
    private OkHttpClient okHttpClient;
    private GameApi gameApi = null;
    private String linkGame;
    private String source = null;

    // facebook
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    // constant
    private static final int CONNECTION_TIMEOUT = 40;
    private static final int WRITE_TIMEOUT = 40;
    private static final int READ_TIMEOUT = 40;

    // type link
    private final String TYPE_GIFT = "exchange_gift"; // đổi quà
    private final String TYPE_ORDER = "order_deli"; // đặt hàng
    private final String TYPE_SHARE = "fb_share"; // share facebook

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_game);
        linkGame = ConfigModel.getInstance(this).getGame_main_url() + "/" + (new AppPreferenceHelper(this)).getAccessToken();
        source = getIntent().getStringExtra(GlobalTracking.SOURCE);
        GlobalTracking.trackGameStart(source);
        initConfig();

        mBinding.webGame.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // redirect

                Helper.showLog(url);
                Helper.showLog(detectType(url));
//
                switch (detectType(url)) {
                    case TYPE_SHARE:
                        GlobalTracking.trackGameShare(TrackingConstant.ContentType.GAME);
                        shareFacebook();
                        break;
                    case TYPE_ORDER:
                        GlobalTracking.trackGameOpenTabbar(TrackingConstant.ContentType.GAME, 0);
                        try {
                            HomeActivity.getInstance().tab = 1;
                        } catch (NullPointerException e) {
                            Crashlytics.logException(new CrashModel("Action game to Deli"));
                        }
                        finish();
                        break;
                    case TYPE_GIFT:
                        GlobalTracking.trackGameOpenTabbar(TrackingConstant.ContentType.GAME, 3);
                        try {
                            HomeActivity.getInstance().tab = 4;
                        } catch (NullPointerException e) {
                            Crashlytics.logException(new CrashModel("Action game to LixiShop"));
                        }
                        finish();
                        break;
                }

                return false; //allow loading (true orride it)
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        mBinding.webGame.loadUrl(linkGame);
    }

    private void initConfig() {
        callbackManager = CallbackManager.Factory.create();
        mBinding.imgBack.setOnClickListener(this);

        mBinding.webGame.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mBinding.webGame.getSettings().setDomStorageEnabled(true);
        mBinding.webGame.getSettings().setJavaScriptEnabled(true);
        mBinding.webGame.getSettings().setAllowContentAccess(true);
    }

    private void callAddTurn() {
        // gọi api công lượt share
        if (gameApi == null) {
            if (ConfigModel.getInstance(activity).getGame_api_url() == null || ConfigModel.getInstance(activity).getGame_api_url().equals("")) {
                return;
            }
            initApi();
        }

        callApi();
    }

    private void initApi() {
        okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder = originalRequest.newBuilder().addHeader("Authorization", AppPreferenceHelper.getInstance().getAccessToken());
                Helper.showLog(originalRequest.toString());
                originalRequest = builder.build();
                return chain.proceed(originalRequest);
            }
        }).connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        builder = new Retrofit.Builder();
        builder.baseUrl(ConfigModel.getInstance(activity).getGame_api_url())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        gameApi = builder.build().create(GameApi.class);
    }

    private void callApi() {
        if (gameApi == null) {
            return;
        }

        Helper.showLog(gameApi.addShareFacebook().request().toString());
        Call<GameModel> call = gameApi.addShareFacebook();
        call.enqueue(new Callback<GameModel>() {
            @Override
            public void onResponse(Call<GameModel> call, retrofit2.Response<GameModel> response) {
                reload();
                if (response.code() == 500) {
                    Helper.showErrorDialog(activity, getErrorMessage(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<GameModel> call, Throwable t) {
                reload();
                Toast.makeText(activity, LanguageBinding.getString(R.string.try_again, activity), Toast.LENGTH_LONG).show();
            }
        });
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(responseBody.string()).getAsJsonObject();
            Type type = new TypeToken<GameModel>() {
            }.getType();
            GameModel gameModel = new Gson().fromJson(jsonObject, type);
            return gameModel.error.getMessage();
        } catch (Exception ex) {
            return "error";
        }
    }

    private boolean hasShowShareDialog = false;

    private void shareFacebook() {
        if (hasShowShareDialog) {
            return;
        }

        hasShowShareDialog = true;
        if (ConfigModel.getInstance(this).getGame_share_url() != null && !ConfigModel.getInstance(this).getGame_share_url().equals("")) {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(ConfigModel.getInstance(this).getGame_share_url()))
                        .build();

                shareDialog = new ShareDialog(this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        GlobalTracking.trackGameShareSuccess(TrackingConstant.ContentType.GAME);
                        callAddTurn();
                    }

                    @Override
                    public void onCancel() {
                        reload();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Helper.showErrorDialog(activity, error.getMessage());
                        reload();
                    }
                });
                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
            }
        }
    }

    private void reload() {
        if (mBinding.webGame.canGoBack()) {
            mBinding.webGame.goBack();
        } else {
            mBinding.webGame.loadUrl(linkGame);
        }
    }

    private String detectType(String link) {
        Uri uri = Uri.parse(link);
        String type = uri.getQueryParameter("type");
        return type == null ? "" : type;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        hasShowShareDialog = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
}
