package com.opencheck.client.home.account.new_layout.model;

public class UserRefInfo {
    /**
     * avatar : string
     * name : string
     */

    private String avatar;
    private String name;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
