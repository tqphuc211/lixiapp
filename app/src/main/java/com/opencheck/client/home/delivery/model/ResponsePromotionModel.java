package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponsePromotionModel implements Serializable{
    private long final_money;
    private long final_cashback;
    private long lixi_reduce;
    private ArrayList<PromotionModel> list_code;
    private long reduce_money;
    private long reward_point;
    private String promotion_code;
    private ArrayList<QuotationModel> list_quotation;

    public long getLixi_reduce() {
        return lixi_reduce;
    }

    public void setLixi_reduce(long lixi_reduce) {
        this.lixi_reduce = lixi_reduce;
    }

    public ArrayList<QuotationModel> getList_quotation() {
        return list_quotation;
    }

    public void setList_quotation(ArrayList<QuotationModel> list_quotation) {
        this.list_quotation = list_quotation;
    }

    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public ArrayList<PromotionModel> getList_code() {
        return list_code;
    }

    public void setList_code(ArrayList<PromotionModel> list_code) {
        this.list_code = list_code;
    }

    public long getReduce_money() {
        return reduce_money;
    }

    public void setReduce_money(long reduce_money) {
        this.reduce_money = reduce_money;
    }

    public long getReward_point() {
        return reward_point;
    }

    public void setReward_point(long reward_point) {
        this.reward_point = reward_point;
    }

    public long getFinal_money() {
        return final_money;
    }

    public void setFinal_money(long final_money) {
        this.final_money = final_money;
    }

    public ArrayList<PromotionModel> getPromotions() {
        return list_code;
    }

    public void setPromotions(ArrayList<PromotionModel> promotions) {
        this.list_code = promotions;
    }

    public long getFinal_cashback() {
        return final_cashback;
    }

    public void setFinal_cashback(long final_cashback) {
        this.final_cashback = final_cashback;
    }
}
