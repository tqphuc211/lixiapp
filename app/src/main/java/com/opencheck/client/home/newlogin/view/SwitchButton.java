package com.opencheck.client.home.newlogin.view;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewCustomSwitchBinding;

public class SwitchButton extends FrameLayout implements Animator.AnimatorListener {
    private Context context;
    private ViewCustomSwitchBinding mBinding;

    private int COLOR_CHECK, COLOR_UNCHECK;
    private Drawable CHECK_BGD, UNCHECK_BGD;

    public SwitchButton(@NonNull Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public SwitchButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initAttribute(attrs);
        initView();
    }

    public SwitchButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initAttribute(attrs);
        initView();
    }

    // Var
    private boolean isChangeBackground = true;
    private boolean isCheck = false;
    private boolean hasText = true;
    private String leftText, rightText;
    private boolean isEnable = true;
    private float textSize = 14;

    private void initAttribute(AttributeSet attrs){
        TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton);
        isChangeBackground = typeArray.getBoolean(R.styleable.SwitchButton_isChangeBackground, true);
        isCheck = typeArray.getBoolean(R.styleable.SwitchButton_isCheck, false);
        hasText = typeArray.getBoolean(R.styleable.SwitchButton_hasText, true);
        leftText = typeArray.getString(R.styleable.SwitchButton_leftText);
        rightText = typeArray.getString(R.styleable.SwitchButton_rightText);
        isEnable = typeArray.getBoolean(R.styleable.SwitchButton_enable, true);
        leftText = leftText == null ? "" : leftText;
        rightText = rightText == null ? "" : rightText;
        textSize = typeArray.getDimensionPixelOffset(R.styleable.SwitchButton_textSize, 14);
        typeArray.recycle();
    }

    private void initView(){
        COLOR_CHECK = Color.parseColor("#c7c7cc");
        COLOR_UNCHECK = Color.parseColor("#000000");

        CHECK_BGD = context.getDrawable(R.drawable.custom_switch_bg);
        UNCHECK_BGD = context.getDrawable(R.drawable.custom_switch_bg_uncheck);

        mBinding = ViewCustomSwitchBinding.inflate(LayoutInflater.from(getContext()), this, true);

        mBinding.txtLeft.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        mBinding.txtRight.setTextSize(textSize);

        if (hasText){
            mBinding.txtLeft.setVisibility(VISIBLE);
            mBinding.txtRight.setVisibility(VISIBLE);
            mBinding.txtLeft.setText(leftText);
            mBinding.txtRight.setText(rightText);
        }else {
            mBinding.txtLeft.setVisibility(GONE);
            mBinding.txtRight.setVisibility(GONE);
        }

        if (isEnable) {
            mBinding.linearParent.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    check();
                }
            });
        }

        if (isCheck) {
            setStatus(true);
        }

        changeBackground(isCheck);
    }

    private void check(){
        setStatusChange();
        isCheck = !isCheck;
        if (onCheckChangeListener != null){
            onCheckChangeListener.onChanged(this, isCheck);
        }
    }

    private void setStatusChange(){
        if (!isCheck){
            // đang check left
            mBinding.iconThumb.animate()
                    .translationX(mBinding.relIcon.getWidth() - mBinding.iconThumb.getWidth() - mBinding.relIcon.getPaddingRight() * 2)
                    .setDuration(100)
                    .setListener(this);
            mBinding.txtRight.setTextColor(COLOR_UNCHECK);
            mBinding.txtLeft.setTextColor(COLOR_CHECK);
        }else {
            // đang check right
            mBinding.iconThumb.animate()
                    .translationX(0)
                    .setDuration(100)
                    .setListener(this);
            mBinding.txtRight.setTextColor(COLOR_CHECK);
            mBinding.txtLeft.setTextColor(COLOR_UNCHECK);
        }
    }

    private void setStatus(boolean check){
        if (check){
            mBinding.iconThumb.setX(mBinding.relIcon.getWidth() - mBinding.iconThumb.getWidth() - mBinding.relIcon.getPaddingRight());
            mBinding.txtRight.setTextColor(COLOR_UNCHECK);
            mBinding.txtLeft.setTextColor(COLOR_CHECK);
        }else {
            mBinding.iconThumb.setX(mBinding.relIcon.getPaddingRight());
            mBinding.txtRight.setTextColor(COLOR_CHECK);
            mBinding.txtLeft.setTextColor(COLOR_UNCHECK);
        }
        changeBackground(check);
        isCheck = check;
    }

    private void changeBackground(boolean check){
        if (isChangeBackground){
            if (check){
                mBinding.relIcon.setBackground(CHECK_BGD);
            }else {
                mBinding.relIcon.setBackground(UNCHECK_BGD);
            }
        }
    }

    public boolean isCheck(){
        return isCheck;
    }

    /**
     *
     * @param check = true - right, false - left
     */
    public void setCheck(boolean check){
        setStatus(check);
    }

    public TextView getLeftText(){
        return mBinding.txtLeft;
    }

    public TextView getRightText(){
        return mBinding.txtRight;
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        changeBackground(isCheck);
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public void setOnCheckChangeListener(OnCheckChangeListener onCheckChangeListener){
        this.onCheckChangeListener = onCheckChangeListener;
    }

    private OnCheckChangeListener onCheckChangeListener;
    public interface OnCheckChangeListener{
        void onChanged(View v, boolean check);
    }
}
