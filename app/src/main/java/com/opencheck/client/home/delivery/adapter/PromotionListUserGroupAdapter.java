package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PromotionUserGroupItemLayoutBinding;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PromotionListUserGroupAdapter extends RecyclerView.Adapter<PromotionListUserGroupAdapter.ViewHolder> implements Filterable {
    private LixiActivity activity;
    private ArrayList<PromotionModel> promotionList;
    private ArrayList<PromotionModel> filterList;
    private OnAvailableData onAvailableData;
    private ItemClickListener itemClickListener;

    public PromotionListUserGroupAdapter(LixiActivity activity, ArrayList<PromotionModel> promotionList) {
        this.activity = activity;
        this.promotionList = promotionList;
        this.filterList = promotionList;
    }

    public void setPromotionList(ArrayList<PromotionModel> promotionList) {
        this.promotionList = promotionList;
        notifyDataSetChanged();
    }

    public void setOnAvailableData(OnAvailableData onAvailableData) {
        this.onAvailableData = onAvailableData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PromotionUserGroupItemLayoutBinding itemLayoutBinding =
                PromotionUserGroupItemLayoutBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);
        return new ViewHolder(itemLayoutBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PromotionModel model = filterList.get(position);
        holder.binding.tvCode.setText(model.getCode());
        if (model.getDescription() != null && model.getDescription().length() > 0) {
            holder.binding.tvDescription.setVisibility(View.VISIBLE);
            holder.binding.tvDescription.setText(model.getDescription());
        } else
            holder.binding.tvDescription.setVisibility(View.GONE);

        holder.binding.tvTimer.setText("Hạn sử dụng: " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(model.getEnd_date())));
        holder.binding.tvType.setText(model.getName());
        if(model.getList_store().size() != 0) {
            holder.binding.tvApply.setText(String.format(LanguageBinding.getString(R.string.apply_for_store, activity), model.getList_store().size()));
        } else if (model.getList_province().size() != 0) {
            holder.binding.tvApply.setText(String.format(LanguageBinding.getString(R.string.apply_for_place, activity), model.getList_province().get(0).getName()));
        }
    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String key = charSequence.toString();
                if (key.isEmpty()) {
                    filterList = promotionList;
                } else {
                    List<PromotionModel> filteredList = new ArrayList<>();
                    for (PromotionModel productModel : promotionList) {
                        String temp = productModel.getCode().toLowerCase();
                        if (formatString(temp).contains(formatString(key.toLowerCase()))) {
                            filteredList.add(productModel);
                        }
                    }

                    filterList = (ArrayList<PromotionModel>) filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<PromotionModel>) filterResults.values;
                if (filterList != null && filterList.size() > 0) {
                    onAvailableData.onHaveData(true);
                } else {
                    onAvailableData.onHaveData(false);
                }
                notifyDataSetChanged();
            }
        };
    }

    private String formatString(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private PromotionUserGroupItemLayoutBinding binding;

        public ViewHolder(PromotionUserGroupItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null) {
                        itemClickListener.onClick(promotionList.get(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
        }

    }

    public interface OnAvailableData {
        void onHaveData(boolean isHave);
    }

    public interface ItemClickListener {
        void onClick(PromotionModel model, int position);
    }
}