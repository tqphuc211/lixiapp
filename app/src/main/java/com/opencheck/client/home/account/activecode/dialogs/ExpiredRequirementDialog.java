package com.opencheck.client.home.account.activecode.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ExpiredRequirementDialogBinding;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/26/2018.
 */

public class ExpiredRequirementDialog extends LixiDialog {
    public ExpiredRequirementDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    public ExpiredRequirementDialog(@NonNull LixiActivity _activity, String storeName, String productName, EvoucherDetailModel productModel ) {
        super(_activity, false, true, false);
        this.storeName = storeName;
        this.productName = productName;
        this.productModel = productModel;
    }

    private TextView tv_done;
    private TextView tv_name, tv_address, tv_message, tv_name_store, tv_title;
    private RelativeLayout rl_dimiss;

    //region Logic Variable
    private EvoucherDetailModel productModel;
    private StoreApplyModel storeApplyModel;
    private String msg;
    private String storeName;
    private String productName;
    //endregion

    private ExpiredRequirementDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ExpiredRequirementDialogBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        setViewData();
    }


    public void setData(String storeName, String productName, EvoucherDetailModel productModel) {
        this.storeName = storeName;
        this.productName = productName;
        this.productModel = productModel;
        setViewData();
    }

//    public void setData(String storeName, String productName) {
//        this.storeName = storeName;
//        this.productName = productName;
//        this.productModel = productModel;
//        setViewData();
//    }

    private void setViewData(){
        tv_name.setText(productName);
        tv_name_store.setText(storeName);
        tv_message.setText(String.format(LanguageBinding.getString(R.string.out_of_time_activate, activity), productName, productModel.getPin()));
    }

    private void findViews() {
        tv_name = findViewById(R.id.tv_name);
        tv_name_store = findViewById(R.id.tv_name_store);
        tv_done = findViewById(R.id.tv_done);
        tv_message = findViewById(R.id.tv_message);
        rl_dimiss = findViewById(R.id.rl_dismiss);
        tv_done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.tv_done){
            dismiss();
        }
    }

    public interface IDidalogEvent {
        void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
