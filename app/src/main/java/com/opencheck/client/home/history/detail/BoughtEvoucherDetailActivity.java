package com.opencheck.client.home.history.detail;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.LixiShopBoughtVoucherDetailActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.lixishop.detail.AdapterImageProductDetail;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.models.lixishop.LixiShopVoucherCodeModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class BoughtEvoucherDetailActivity extends LixiActivity {

    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String VOUCHER = "VOUCHER";
    private LinearLayout ll_info;
    private LinearLayout ll_qrcode;
    private RelativeLayout rl_viewpager;
    private TextView tv_name;
    private TextView tv_serial;
    private TextView tv_pin;
    private TextView tv_date;
    //    private TextView tv_description;
//    private TextView tv_condition;
    private WebView wv_product_detail;
    private ViewPager viewPager;
    private CirclePageIndicator indicator;
    private AdapterImageProductDetail adapterImageProductDetail;
    private ImageView iv_back;
    private ImageView iv_qrcode;
    //
    private View v_back;
    private TextView tv_chitietsanpham;
    private ScrollView scroll_view;

    private long idProduct;
    private LixiShopProductDetailModel productModel;
    private LixiShopVoucherCodeModel voucher;
    private String status="";

    private List<String> VoucherImageList;

    private LixiShopBoughtVoucherDetailActivityBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.lixi_shop_bought_voucher_detail_activity);
        idProduct = getIntent().getLongExtra(PRODUCT_ID, 0);
        voucher = (LixiShopVoucherCodeModel) getIntent().getSerializableExtra(VOUCHER);
        status = getIntent().getStringExtra("STATUS");
        findView();
        startLoadData();
    }

    private int heightWith169Aspect = 0;

    private void findView() {
        ll_info = (LinearLayout) findViewById(R.id.ll_info);
        ll_qrcode = (LinearLayout) findViewById(R.id.ll_qrcode);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_serial = (TextView) findViewById(R.id.tv_serial);
        tv_pin = (TextView) findViewById(R.id.tv_pin);
        tv_date = (TextView) findViewById(R.id.tv_date);
//        tv_description = (TextView) findViewById(R.id.tv_description);
//        tv_condition = (TextView) findViewById(R.id.tv_condition);
        wv_product_detail = (WebView) findViewById(R.id.wv_product_detail);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_qrcode = (ImageView) findViewById(R.id.iv_qrcode);
        rl_viewpager = (RelativeLayout) findViewById(R.id.rl_viewpager);
        v_back = findViewById(R.id.v_back);
        tv_chitietsanpham = (TextView) findViewById(R.id.tv_chitietsanpham);
        scroll_view = (ScrollView) findViewById(R.id.scroll_view);

        scroll_view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int y = scroll_view.getScrollY();
                Log.d("myps",y+"");
                scroll(y);
            }
        });



        //


        //set ratio for viewpager
        rl_viewpager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int availableHeight = rl_viewpager.getMeasuredHeight();
                if(availableHeight>0) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        rl_viewpager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        rl_viewpager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
//                    rl_viewpager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    //save height here and do whatever you want with it
                    heightWith169Aspect = rl_viewpager.getWidth() * 9 / 16;

                    ViewGroup.LayoutParams params = rl_viewpager.getLayoutParams();
                    params.height = heightWith169Aspect;
                    rl_viewpager.setLayoutParams(params);
                }
            }
        });

        iv_back.setOnClickListener(this);


    }


    public void startLoadData() {
        DataLoader.getProductDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    productModel = (LixiShopProductDetailModel) object;
                    showData();
                } else {
                    Helper.showErrorToast(activity, (String) object);
                    finish();
                }
            }
        }, idProduct);

    }

    public void showData() {
        tv_name.setText(productModel.getName());
        tv_chitietsanpham.setText(productModel.getName());
        tv_pin.setText(voucher.getPin());
        tv_serial.setText(voucher.getSerial());
        if (!voucher.getExpired_time().contains("-") && !voucher.getExpired_time().contains("/")) {
            voucher.setExpired_time(String.valueOf(Long.parseLong(voucher.getExpired_time())*1000));
        }
        if(voucher.getExpired_time().contains("/")) {
            voucher.setExpired_time(voucher.getExpired_time().replaceAll("/","-"));
        }

        ll_qrcode.setOnClickListener(this);

        wv_product_detail.getSettings().setJavaScriptEnabled(true);

        wv_product_detail.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        wv_product_detail.getSettings().setDomStorageEnabled(true);


        String mime = "text/html";
        String encoding = "utf-8";
        String defaultFont = getDefaultFont();
        String pish = "<html><head><style type=\"text/css\">@font-face {font-family:"+ defaultFont +";src: url(\"file:///system/fonts/"+ defaultFont + ".ttf\")}body {font-family: MyFont;font-size: medium;text-align: justify;}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + productModel.getHtml_text() + pas;
        wv_product_detail.loadDataWithBaseURL(null, myHtmlString, mime, encoding, null);

        if (productModel.getProduct_image() != null && productModel.getProduct_image().size() > 0)
            VoucherImageList = productModel.getProduct_image();
        else
            VoucherImageList = new ArrayList<>();
        //set pager
        adapterImageProductDetail = new AdapterImageProductDetail(this, VoucherImageList);
        viewPager.setAdapter(adapterImageProductDetail);
        indicator.setViewPager(viewPager);
        new GeneralQRCodeAndCropTask(BoughtEvoucherDetailActivity.this, voucher.getPin(), iv_qrcode, 0xFF667991,new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (pos == 1) {
                    iv_qrcode.setImageBitmap((Bitmap) object);
                }
            }
        });

        if(status == null) {
            ll_qrcode.performClick();
            long countLimitDay = Helper.getDaysBetweenTimestamp(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(Long.parseLong(Helper.getTimestamp()) * 1000)), voucher.getExpired_time(), ConstantValue.FORMAT_TIME_DATE);
            if(countLimitDay > 0) {
                tv_date.setText("(" + String.format(LanguageBinding.getString(R.string.STILL, activity), countLimitDay) + ")");
            }
            else if(countLimitDay == 0) {
                tv_date.setText(getString(R.string.bought_evoucher_expired_today));
            }
            return;
        }
        else {
            tv_date.setText(status);
            if (status.equals(getString(R.string.bought_evoucher_expired)) || status.equals(getString(R.string.bought_evoucher_used))) {
                return;
            }
            else {
                ll_qrcode.performClick();
            }

        }

    }

    public void scroll(int y) {
        if (y < 100) {
            tv_chitietsanpham.setVisibility(View.GONE);
            v_back.setVisibility(View.GONE);
        }
        if (y >= 100 && y < 130) {
            tv_chitietsanpham.setTextColor(0x4B000000);
            v_back.setBackgroundColor(0x4BFFFFFF);
            tv_chitietsanpham.setVisibility(View.VISIBLE);
            v_back.setVisibility(View.VISIBLE);
        }
        if (y >= 130 && y < 160) {
            tv_chitietsanpham.setTextColor(0x6B000000);
            v_back.setBackgroundColor(0x6BFFFFFF);
            tv_chitietsanpham.setVisibility(View.VISIBLE);
            v_back.setVisibility(View.VISIBLE);
        }
        if (y >= 160 && y < 190) {
            tv_chitietsanpham.setTextColor(0x8B000000);
            v_back.setBackgroundColor(0x8BFFFFFF);
            tv_chitietsanpham.setVisibility(View.VISIBLE);
            v_back.setVisibility(View.VISIBLE);
        }

        if (y > 245) {
            tv_chitietsanpham.setTextColor(0xFF000000);
            v_back.setBackgroundColor(0xFFFFFFFF);
            tv_chitietsanpham.setVisibility(View.VISIBLE);
            v_back.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_qrcode:
                //Show popup QR codeoucher
                ShopQRCodeDialog dialog = new ShopQRCodeDialog(BoughtEvoucherDetailActivity.this, false, true, false);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                dialog.setData("Mã code", voucher.getPin());
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    //region handle font for product webview
    // The class which loads the TTF file, parses it and returns the TTF font name
    class TTFAnalyzer
    {
        // This function parses the TTF file and returns the font name specified in the file
        public String getTtfFontName( String fontFilename )
        {
            try
            {
                // Parses the TTF file format.
                // See http://developer.apple.com/fonts/ttrefman/rm06/Chap6.html
                m_file = new RandomAccessFile( fontFilename, "r" );

                // Read the version first
                int version = readDword();

                // The version must be either 'true' (0x74727565) or 0x00010000
                if ( version != 0x74727565 && version != 0x00010000 )
                    return null;

                // The TTF file consist of several sections called "tables", and we need to know how many of them are there.
                int numTables = readWord();

                // Skip the rest in the header
                readWord(); // skip searchRange
                readWord(); // skip entrySelector
                readWord(); // skip rangeShift

                // Now we can read the tables
                for ( int i = 0; i < numTables; i++ )
                {
                    // Read the table entry
                    int tag = readDword();
                    readDword(); // skip checksum
                    int offset = readDword();
                    int length = readDword();

                    // Now here' the trick. 'name' field actually contains the textual string name.
                    // So the 'name' string in characters equals to 0x6E616D65
                    if ( tag == 0x6E616D65 )
                    {
                        // Here's the name section. Read it completely into the allocated buffer
                        byte[] table = new byte[ length ];

                        m_file.seek( offset );
                        read( table );

                        // This is also a table. See http://developer.apple.com/fonts/ttrefman/rm06/Chap6name.html
                        // According to Table 36, the total number of table records is stored in the second word, at the offset 2.
                        // Getting the count and string offset - remembering it's big endian.
                        int count = getWord( table, 2 );
                        int string_offset = getWord( table, 4 );

                        // Record starts from offset 6
                        for ( int record = 0; record < count; record++ )
                        {
                            // Table 37 tells us that each record is 6 words -> 12 bytes, and that the nameID is 4th word so its offset is 6.
                            // We also need to account for the first 6 bytes of the header above (Table 36), so...
                            int nameid_offset = record * 12 + 6;
                            int platformID = getWord( table, nameid_offset );
                            int nameid_value = getWord( table, nameid_offset + 6 );

                            // Table 42 lists the valid name Identifiers. We're interested in 4 but not in Unicode encoding (for simplicity).
                            // The encoding is stored as PlatformID and we're interested in Mac encoding
                            if ( nameid_value == 4 && platformID == 1 )
                            {
                                // We need the string offset and length, which are the word 6 and 5 respectively
                                int name_length = getWord( table, nameid_offset + 8 );
                                int name_offset = getWord( table, nameid_offset + 10 );

                                // The real name string offset is calculated by adding the string_offset
                                name_offset = name_offset + string_offset;

                                // Make sure it is inside the array
                                if ( name_offset >= 0 && name_offset + name_length < table.length )
                                    return new String( table, name_offset, name_length );
                            }
                        }
                    }
                }

                return null;
            }
            catch (FileNotFoundException e)
            {
                // Permissions?
                return null;
            }
            catch (IOException e)
            {
                // Most likely a corrupted font file
                return null;
            }
        }

        // Font file; must be seekable
        private RandomAccessFile m_file = null;

        // Helper I/O functions
        private int readByte() throws IOException
        {
            return m_file.read() & 0xFF;
        }

        private int readWord() throws IOException
        {
            int b1 = readByte();
            int b2 = readByte();

            return b1 << 8 | b2;
        }

        private int readDword() throws IOException
        {
            int b1 = readByte();
            int b2 = readByte();
            int b3 = readByte();
            int b4 = readByte();

            return b1 << 24 | b2 << 16 | b3 << 8 | b4;
        }

        private void read( byte [] array ) throws IOException
        {
            if ( m_file.read( array ) != array.length )
                throw new IOException();
        }

        // Helper
        private int getWord( byte [] array, int offset )
        {
            int b1 = array[ offset ] & 0xFF;
            int b2 = array[ offset + 1 ] & 0xFF;

            return b1 << 8 | b2;
        }
    }
    public String getDefaultFont() {
        System.out.println("getFontList(): entry");
        File configFilename = new File("/system/etc/system_fonts.xml");
        String defaultFontName = "";
        TTFAnalyzer analyzer = new TTFAnalyzer();

        try {
            FileInputStream fontsIn = new FileInputStream(configFilename);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(fontsIn, null);
            Boolean done = false;
            Boolean getTheText = false;
            int eventType;
            String defaultFont = "";
            while (!done) {
                eventType = parser.next();
                if (eventType == parser.START_TAG && parser.getName().equalsIgnoreCase("file")) {
                    // the text is next up -- pull it
                    getTheText = true;
                }
                if (eventType == parser.TEXT && getTheText == true) {
                    // first name
                    defaultFont = parser.getText();
                    System.out.println("text for file tag:" + defaultFont);
                    done = true;
                }
                if (eventType == parser.END_DOCUMENT) {
                    System.out.println("hit end of system_fonts.xml document");
                    done = true;
                }
            }

            if (defaultFont.length() > 0) {
                // found the font filename, most likely in /system/fonts. Now pull out the human-readable
                // string from the font file
                System.out.println("Figuring out default Font info");
                String fontname = analyzer.getTtfFontName("/system/fonts/" + defaultFont);
                if ( fontname != null ) {
                    System.out.println("found font info: " + fontname);
                    defaultFontName = fontname;
                }
            }

        } catch (RuntimeException e) {
            System.err.println("Didn't create default family (most likely, non-Minikin build)");
        } catch (FileNotFoundException e) {
            System.err.println("GetDefaultFont: config file Not found");
        } catch (IOException e) {
            System.err.println("GetDefaultFont: IO exception: " + e.getMessage());
        } catch (XmlPullParserException e) {
            System.err.println("getDefaultFont: XML parse exception " + e.getMessage());
        }
        return defaultFontName;
    }
    //endregion


}
