package com.opencheck.client.home.account.new_layout.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentHistoryIntroBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.adapter.HistoryIntroAdapter;
import com.opencheck.client.home.account.new_layout.model.HistoryIntroModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class HistoryIntroFragment extends LixiFragment {

    private FragmentHistoryIntroBinding mBinding;
    private HistoryIntroAdapter adapter;
    private LinearLayoutManager layoutManager;

    private int page = 1;
    private boolean isLoading = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentHistoryIntroBinding.inflate(inflater, container, false);
        initEvent();
        setupAdapter();
        getList();
        return mBinding.getRoot();
    }

    private void initEvent() {
        mBinding.relParent.setOnClickListener(this);
        mBinding.imgBack.setOnClickListener(this);
        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isLoading = false;
                page = 1;
                adapter.clearData();
                getList();
            }
        });

        mBinding.recHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mBinding.recHistory.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount  = layoutManager.getChildCount();
                    int totalItemCount    = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 3) {
                        Helper.showLog("Load more");
                        getList();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void setupAdapter() {
        adapter = new HistoryIntroAdapter(activity);
        layoutManager = new LinearLayoutManager(activity);
        mBinding.recHistory.setLayoutManager(layoutManager);
        mBinding.recHistory.setAdapter(adapter);
    }

    private void getList() {
        if (!isLoading) {
            mBinding.swipeRefresh.setRefreshing(true);
            DataLoader.getHistoryIntro(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    mBinding.swipeRefresh.setRefreshing(false);
                    if (isSuccess) {
                        ArrayList<HistoryIntroModel> list = (ArrayList<HistoryIntroModel>) object;
                        if (page == 1) {
                            if (list.isEmpty()) {
                                mBinding.linearNoResult.setVisibility(View.VISIBLE);
                            }else {
                                mBinding.linearNoResult.setVisibility(View.GONE);
                            }
                        }

                        adapter.setData(list);
                        isLoading = false;
                        page++;
                    } else {
                        Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                        isLoading = true;
                    }
                }
            }, page);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getFragmentManager().beginTransaction().detach(this).commit();
                break;
        }
    }
}
