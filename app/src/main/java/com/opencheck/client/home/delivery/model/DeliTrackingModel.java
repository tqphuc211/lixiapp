package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.time.*;

public class DeliTrackingModel implements Serializable{
    private int index;
    private long total;
    private String state;
    private String text;
    private Long start_date;
    private Long end_date;
    private double end_date_estimate;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getStart_date() {
        if (start_date == null)
            start_date = 0L;
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public Long getEnd_date() {
        if (end_date == null)
            end_date = 0L;
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public double getEnd_date_estimate() {
        return end_date_estimate;
    }

    public void setEnd_date_estimate(double end_date_estimate) {
        this.end_date_estimate = end_date_estimate;
    }
}
