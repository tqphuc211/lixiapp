package com.opencheck.client.home.account.evoucher;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityEvoucherAccountBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.firebase.FCMService;
import com.opencheck.client.home.account.activecode.dialogs.ExpiredRequirementDialog;
import com.opencheck.client.home.account.activecode.fragments.ConfirmActiveCodeFragment;
import com.opencheck.client.home.account.activecode.fragments.StoreSelectionFragment;
import com.opencheck.client.home.account.evoucher.Dialog.ConfirmUsedVoucherDialog;
import com.opencheck.client.home.account.evoucher.Dialog.InvalidLiXiCodeDialog;
import com.opencheck.client.home.account.evoucher.Enum.DragAction;
import com.opencheck.client.home.account.evoucher.Interface.OnConfirmListener;
import com.opencheck.client.home.account.evoucher.View.CountDownButton;
import com.opencheck.client.home.account.new_layout.model.EvoucherInfo;
import com.opencheck.client.home.delivery.libs.RatingBar;
import com.opencheck.client.home.flashsale.Interface.OnFinishTimerListener;
import com.opencheck.client.home.rating.activity.RatingActivity;
import com.opencheck.client.home.rating.model.MarkUsedResponse;
import com.opencheck.client.home.rating.view.DragQRView;
import com.opencheck.client.models.MerchantActive;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.CompletedVoucherModel;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.lixishop.LixiShopVoucherCodeModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.List;

public class EVoucherAccountActivity extends LixiActivity implements DragQRView.OnActionChangedListener {

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.PROFILE_VOUCHER_DETAIL;
    }

    // View
    private ImageView imgBack, imgLogo;
    private ProgressBar progressBar;
    private DragQRView dragQR;
    private TextView txtCondition, txtStoreName, txtStoreQuant, txtTotal, txtQuantVoucher;
    private Button btnActive, btnMark;
    private CountDownButton btnWaiting;
    private ConstraintLayout constraintTotalValue, constraintExpandLocation;
    private RatingBar ratingBar;
    private LinearLayout linearCondition, linearRating;
    private FrameLayout frameActive;

    // parent View
    public ConstraintLayout constraintParentView;
    public FrameLayout frameParentView;

    // Var
    private Boolean isLoading = false;
    private boolean isOutOfVoucherPage = false;
    private int page = -1;
    private int minPage = page;
    private long idVoucher;
    private long totalCanActive;
    private long totalList = 0;
    private String STATE_VOUCHER = "selled";
    private EvoucherDetailModel evoucherModel;
    //    private EVoucherSummary eVoucherSummary;
    private ArrayList<LixiShopEvoucherBoughtModel> listEvoucher;
    private ArrayList<LixiShopVoucherCodeModel> listOrderCode;
    private ArrayList<EvoucherInfo> listEvoucherInfo;
    private long currentPosition;
    private int COLOR_BLACK, COLOR_GRAY;

    // Constant
    public static final String USED = "used";
    public static final String EXPIRED = "expired";
    public static final String WAITING = "waiting";
    public static final String CAN_ACTIVE = "can_active";
    public static final String USER_ACTIVE = "user_active";

    private void initView() {
        constraintParentView = (ConstraintLayout) findViewById(R.id.constraintParentView);
        frameParentView = (FrameLayout) findViewById(R.id.frameParentView);

        dragQR = (DragQRView) findViewById(R.id.dragQR);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtCondition = (TextView) findViewById(R.id.txtCondition);
        txtStoreName = (TextView) findViewById(R.id.txtStoreName);
        txtStoreQuant = (TextView) findViewById(R.id.txtStoreQuant);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtQuantVoucher = (TextView) findViewById(R.id.txtQuantVoucher);

        btnActive = (Button) findViewById(R.id.btnActive);
        btnWaiting = (CountDownButton) findViewById(R.id.btnWaiting);
        btnMark = (Button) findViewById(R.id.btnMark);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        linearCondition = (LinearLayout) findViewById(R.id.linearCondition);
        linearRating = (LinearLayout) findViewById(R.id.linearRating);
        constraintTotalValue = (ConstraintLayout) findViewById(R.id.constraintTotalValue);
        constraintExpandLocation = (ConstraintLayout) findViewById(R.id.constraintExpandLocation);

        frameActive = (FrameLayout) findViewById(R.id.frameActive);

        imgBack.setOnClickListener(this);
        constraintExpandLocation.setOnClickListener(this);
        btnActive.setOnClickListener(this);
        btnMark.setOnClickListener(this);
        dragQR.addOnActionChangedListener(this);

        listEvoucher = new ArrayList<>();
        COLOR_BLACK = activity.getResources().getColor(R.color.black);
        COLOR_GRAY = Color.parseColor("#9B9B9B");

        FCMService.merchantActive = ViewModelProviders.of(this).get(MerchantActive.class); // init
        FCMService.merchantActive.getActive().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    if (evoucherModel != null) {
                        if (evoucherModel.getState().equals(WAITING)) {
                            try {
                                load();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    private ActivityEvoucherAccountBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_evoucher_account);
        initView();
        Intent intent = getIntent();
        if (intent != null) {
            idVoucher = intent.getLongExtra(ConstantValue.EVOUCHER_ID, -1);
            STATE_VOUCHER = intent.getStringExtra(ConstantValue.STATE_VOUCHER);
            if (STATE_VOUCHER == null || STATE_VOUCHER.equals("")) {
                STATE_VOUCHER = "selled";
            }
            ConstantValue.ID = idVoucher;
            currentPosition = intent.getIntExtra(ConstantValue.CURRENT_POSITION, -1);
            page = getCurrentPage(currentPosition);
            minPage = page;
            totalCanActive = intent.getLongExtra(ConstantValue.TOTAL_CAN_ACTIVE, -1);
            listOrderCode = intent.getParcelableArrayListExtra(ConstantValue.ORDER_LIST_CODE);
            listEvoucherInfo = intent.getParcelableArrayListExtra(ConstantValue.LIST_EVOUCHER_INFO);
        }

        if (listOrderCode != null) {
            totalCanActive = listOrderCode.size();
        }

        if (listEvoucherInfo != null) {
            totalCanActive = listEvoucherInfo.size();
        }

        getVoucherList(0);
    }

    public void load() throws Exception {
        if (listOrderCode != null) {
            loadData(listOrderCode.get(getPositionInList(currentPosition)).getId());
            return;
        }

        if (listEvoucherInfo != null) {
            loadData(listEvoucherInfo.get(getPositionInList(currentPosition)).getId());
            return;
        }

        if (listEvoucher != null) {
            loadData(listEvoucher.get(getPositionInList(currentPosition)).getId());
        }
    }

    private void loadData(long id) {
        progressBar.setVisibility(View.VISIBLE);
        dragQR.setCanSwipe(false);
        btnActive.setEnabled(false);
        btnMark.setEnabled(false);
        btnWaiting.setEnabled(false);
        constraintExpandLocation.setEnabled(false);
        DataLoader.getCashBoughtEvoucherDetail((LixiActivity) activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    evoucherModel = (EvoucherDetailModel) object;
                    showData();
//                    loadTotalandPriceVoucher();
                } else {
                    Helper.showErrorDialog((LixiActivity) activity, (String) object);
                }
            }
        }, id);
    }

//    private void loadTotalandPriceVoucher() {
//        DataLoader.getEVoucherSummary((LixiActivity) activity, new ApiCallBack() {
//            @Override
//            public void handleCallback(boolean isSuccess, Object object) {
//                if (isSuccess) {
//                    eVoucherSummary = (EVoucherSummary) object;
//                    showData();
//                }
//            }
//        }, evoucherModel.getMerchant_info().getId());
//    }

    private void showData() {
        constraintExpandLocation.setEnabled(true);
        dragQR.resetView();
        if (evoucherModel != null) {
            dragQR.setData(evoucherModel);

            txtCondition.setText(evoucherModel.getUse_condition());

            dragQR.setImageQR(activity, evoucherModel.getPin(), evoucherModel.getTransform());
            dragQR.setVisibleArrow(totalCanActive, currentPosition);

            ImageLoader.getInstance().displayImage(evoucherModel.getMerchant_info().getLogo(), imgLogo);
            txtStoreName.setText(evoucherModel.getMerchant_info().getName());
            txtStoreQuant.setText(String.format(LanguageBinding.getString(R.string.evoucher_detail_apply_at, activity), evoucherModel.getStore_apply().size()));
//            txtQuantVoucher.setText("Bạn có " + eVoucherSummary.getCount() + " voucher có thể sử dụng tại " + evoucherModel.getMerchant_info().getName());
//            txtTotal.setText("Tổng giá trị: " + Helper.getVNCurrency(eVoucherSummary.getTotal_price()) + activity.getString(R.string.p));

            setStatus();
            checkLoadMore();
        }

        progressBar.setVisibility(View.GONE);
        dragQR.setCanSwipe(true);
        btnActive.setEnabled(true);
        btnMark.setEnabled(true);
        btnWaiting.setEnabled(true);
    }

    private void setStatus() {
        dragQR.setStateVoucher(evoucherModel.getState());
        frameActive.setVisibility(View.VISIBLE);
        switch (evoucherModel.getState()) {
            case USED:
            case USER_ACTIVE:
                if (evoucherModel.getRating() > 0) {
                    // đã dánh giá
                    btnActive.setVisibility(View.GONE);
                    btnWaiting.setVisibility(View.GONE);
                    btnMark.setVisibility(View.GONE);
                    linearRating.setVisibility(View.VISIBLE);
                    ratingBar.setCount(evoucherModel.getRating());
                } else {
                    // chưa đánh giá
                    btnActive.setVisibility(View.VISIBLE);
                    btnWaiting.setVisibility(View.GONE);
                    btnMark.setVisibility(View.GONE);
                    linearRating.setVisibility(View.GONE);
                    btnActive.setText(LanguageBinding.getString(R.string.bought_evoucher_rating_product, activity));
                }
                break;
            case CAN_ACTIVE:
                if (evoucherModel.isIs_lixi_code()) {
                    // do lixi phát hành
                    btnActive.setVisibility(View.VISIBLE);
                    btnWaiting.setVisibility(View.GONE);
                    btnMark.setVisibility(View.GONE);
                    linearRating.setVisibility(View.GONE);
                    btnActive.setText(LanguageBinding.getString(R.string.ev_active_voucher, activity));
                } else {
                    // không do lixi phát hành
                    btnActive.setVisibility(View.GONE);
                    btnWaiting.setVisibility(View.GONE);
                    btnMark.setVisibility(View.VISIBLE);
                    linearRating.setVisibility(View.GONE);
                }
                break;
            case WAITING:
                btnActive.setVisibility(View.GONE);
                btnWaiting.setVisibility(View.VISIBLE);
                btnMark.setVisibility(View.GONE);
                linearRating.setVisibility(View.GONE);
                if (evoucherModel.getActive_ttl() > 0) {
                    btnWaiting.setStatus(evoucherModel.getState(), evoucherModel.getActive_ttl());
                    btnWaiting.addOnFinishTimerListener(new OnFinishTimerListener() {
                        @Override
                        public void onFinish() {
                            cancelActiveCode();
                        }
                    });
                }
                break;
            case EXPIRED:
                frameActive.setVisibility(View.GONE);
                break;
        }
    }

    private void cancelActiveCode() {
        DataLoader.getCompleteVoucherCode((LixiActivity) activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                reloadStatusCode();
                if (isSuccess) {
                    CompletedVoucherModel model = (CompletedVoucherModel) object;
                    if (!model.isIs_voucher_complete()) {
                        if (!activity.isFinishing()) {
                            ExpiredRequirementDialog dialog = new ExpiredRequirementDialog((LixiActivity) activity, model.getStore_name(), model.getProduct_name(), evoucherModel);
                            try {
                                dialog.show();
                            } catch (Exception e) {
                                Toast.makeText(activity, String.format(LanguageBinding.getString(R.string.out_of_time_activate, activity), model.getProduct_name(), evoucherModel.getPin()), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } else {
                    Helper.showErrorDialog((LixiActivity) activity, object.toString());
                }
            }
        }, evoucherModel.getId());
    }

    public void reloadStatusCode() {
        setStatus();
    }

    private void checkLoadMore() {
        if (currentPosition != 0 && currentPosition != totalCanActive) {
            // get prev page
            if (minPage != 1) {
                if (getPositionInList(currentPosition) < 5) {
                    getVoucherList(-1);
                    return;
                }
            }

            // get next page
            if (minPage != getCurrentPage(totalCanActive - 1)) {
                if (getPositionInList(currentPosition) > listEvoucher.size() - 5) {
                    getVoucherList(1);
                    return;
                }
            }
        }
    }

    private void getVoucherList(final int move) {
        if (!isLoading) {
            if (!isOutOfVoucherPage) {
                progressBar.setVisibility(View.VISIBLE);
                dragQR.setCanSwipe(false);
                btnActive.setEnabled(false);
                btnMark.setEnabled(false);
                btnWaiting.setEnabled(false);
                constraintExpandLocation.setEnabled(false);
                DataLoader.getBoughtEVoucher(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            ApiWrapperForListModel<LixiShopEvoucherBoughtModel> oj = (ApiWrapperForListModel<LixiShopEvoucherBoughtModel>) object;

                            List<LixiShopEvoucherBoughtModel> list = oj.getRecords();
                            if (list == null || list.size() == 0 || listEvoucher == null) {
                                return;
                            } else {
                                if (move >= 0) {
                                    listEvoucher.addAll(list);
                                }

                                if (move < 0) {
                                    list.addAll(listEvoucher);
                                    listEvoucher.clear();
                                    listEvoucher.addAll(list);
                                    minPage--;
                                    totalList = listEvoucher.size();
                                }
                                try {
                                    load();
                                } catch (Exception e) {

                                }
                                checkLoadMore();
                            }

                            isLoading = false;
                        } else {
                            isLoading = true;
                        }
                    }
                }, getCurrentPage(currentPosition) + move, STATE_VOUCHER);
            }
        }
    }

    private int getCurrentPage(long pos) {
        long div = (long) (pos / DataLoader.record_per_page);
        return (int) (div + 1);
    }

    private int getPositionInList(long currentPosition) {
        return (int) (currentPosition - (minPage - 1) * DataLoader.record_per_page);
    }

    private long getCurrentPosition(long posInList) {
        return posInList + (minPage - 1) * DataLoader.record_per_page;
    }

    public void replaceFragment(LixiFragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped || manager.findFragmentByTag(backStateName) == null) { //fragment not in back stack, create it.
            constraintParentView.setVisibility(View.GONE);
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frameParentView, fragment, backStateName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                this.finish();
                break;
            case R.id.constraintExpandLocation:
                Bundle bundle = new Bundle();
                bundle.putString("ev", new Gson().toJson(evoucherModel));
                bundle.putBoolean("is_only_seen", true);
                StoreSelectionFragment fragment = new StoreSelectionFragment();
                fragment.setArguments(bundle);
                EVoucherAccountActivity.this.replaceFragment(fragment);
                break;
            case R.id.btnActive:
                if (!evoucherModel.getState().equals(CAN_ACTIVE)) {
                    // rating
                    Intent intent = new Intent(activity, RatingActivity.class);
                    intent.putExtra("rating product", evoucherModel);
                    intent.putExtra(ConstantValue.CURRENT_POSITION, currentPosition);
                    activity.startActivityForResult(intent, 1234);
                } else {
                    // active
                    checkLixiEvoucherStatus();
                }
                break;
            case R.id.btnMark:
                showConfirmDialog();
                break;
        }
    }

    private void showConfirmDialog() {
        ConfirmUsedVoucherDialog dialog = new ConfirmUsedVoucherDialog(activity);
        dialog.show();
        dialog.setOnConfirmListener(new OnConfirmListener() {
            @Override
            public void onConfirm(boolean result) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("evoucher_ids", evoucherModel.getId());
                DataLoader.postActiveEvoucher(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            MarkUsedResponse markUsedResponse = (MarkUsedResponse) object;
                            try {
                                load();
                            } catch (Exception e) {

                            }
                        } else {
                            Toast.makeText(activity, "Fail", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, jsonObject);
            }
        });
    }

    @Override
    public void onChanged(DragAction action) {
        switch (action) {
            case NEXT:
                currentPosition++;
                break;
            case PREV:
                currentPosition--;
                break;
            case CENTER:
                return;
        }
        ConstantValue.ID = getCurrentID(currentPosition);
        try {
            load();
        } catch (Exception e) {
            currentPosition++;
            ConstantValue.ID = getCurrentID(currentPosition);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(EVoucherAccountActivity.this, EVoucherAccountActivity.class);
            intent.putExtra(ConstantValue.CURRENT_POSITION, (int) currentPosition);
            intent.putExtra(ConstantValue.EVOUCHER_ID, evoucherModel.getId());
            intent.putExtra(ConstantValue.STATE_VOUCHER, STATE_VOUCHER);
            intent.putExtra(ConstantValue.TOTAL_CAN_ACTIVE, totalCanActive);
            if (listOrderCode != null) {
                intent.putExtra(ConstantValue.ORDER_LIST_CODE, listOrderCode);
            }
            finish();
            startActivity(intent);
        }
    }

    public long getCurrentID(long posInList) {
        try {
            if (listEvoucher != null) {
                return listEvoucher.get((int) posInList).getId();
            }

            if (listOrderCode != null) {
                return listOrderCode.get((int) posInList).getId();
            }

            if (listEvoucherInfo != null) {
                return listEvoucherInfo.get((int) posInList).getId();
            }
        } catch (IndexOutOfBoundsException e) {

        }
        return -1;
    }

    public void checkLixiEvoucherStatus() {
        if (!evoucherModel.is_lixi_code()) {
//            Helper.showErrorDialog(activity, getString(R.string.code_message) + " " +
//                    evoucherModel.getMerchant_info().getName() + " " + getString(R.string.invalid_code_message));
            InvalidLiXiCodeDialog dialog = new InvalidLiXiCodeDialog((LixiActivity) activity, false, true, false, evoucherModel.getMerchant_info().getName());
            dialog.show();
        } else if (btnActive.getText().toString().equalsIgnoreCase(activity.getString(R.string.ev_active_voucher))) { //can active (new code, unused, not expired)

            if (evoucherModel.getStore_apply().size() < 2) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DataLoader.getCashBoughtEvoucherStoreApply((LixiActivity) activity, new ApiCallBack() {
                            @Override
                            public void handleCallback(boolean isSuccess, Object object) {
                                if (isSuccess) {
                                    ArrayList<StoreApplyModel> storeApplyModels = (ArrayList<StoreApplyModel>) object;
                                    if (storeApplyModels == null || storeApplyModels.size() == 0) {
                                        return;
                                    }
                                    ConfirmActiveCodeFragment fragment = new ConfirmActiveCodeFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ev", new Gson().toJson(evoucherModel));
                                    bundle.putString("sa", new Gson().toJson(storeApplyModels.get(0)));
                                    bundle.putInt(ConstantValue.CURRENT_POSITION, (int) currentPosition);
                                    fragment.setArguments(bundle);
                                    ((EVoucherAccountActivity) activity).replaceFragment(fragment);
                                } else {
                                    Helper.showErrorToast((LixiActivity) activity, object.toString());
                                }
                            }
                        }, evoucherModel.getProduct());
                    }
                }, 300);
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("ev", new Gson().toJson(evoucherModel));
                bundle.putBoolean("is_only_seen", false);
                bundle.putInt(ConstantValue.CURRENT_POSITION, (int) currentPosition);
                StoreSelectionFragment fragment = new StoreSelectionFragment();
                fragment.setArguments(bundle);
                ((EVoucherAccountActivity) activity).replaceFragment(fragment);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (evoucherModel != null) {
            if (evoucherModel.getState().equals(WAITING)) {
                try {
                    load();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
