package com.opencheck.client.home.product.communicate;

import com.opencheck.client.home.product.model.BankCard;
import com.opencheck.client.home.product.model.TokenData;

public interface OnUseSavedCard {
    void onUse(TokenData tokenData);
}
