package com.opencheck.client.home.merchant;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.MerchantDetailActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.home.merchant.map.StoreMapActivity;
import com.opencheck.client.home.search.OnClickTagController;
import com.opencheck.client.home.search.SearchActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.FilterModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MenuModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.ProductImageModel;
import com.opencheck.client.models.merchant.PromotionModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.opencheck.client.utils.ConstantValue.ACTION_FAVORITE_PROMOTION;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class MerchantDetailActivity extends LixiActivity {

    public static void startMerchantDetailActivity(LixiActivity activity, String merchantId, String source) {
        Intent intent = new Intent(activity, MerchantDetailActivity.class);
        MerchantModel mc = new MerchantModel();
        mc.setId(merchantId);
        intent.putExtra(ConstantValue.MERCHANT_DETAIL_MODEL, (new Gson()).toJson(mc));
        intent.putExtra(GlobalTracking.SOURCE, source);
        activity.startActivity(intent);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_MERCHANT_DETAIL;
    }

    private RelativeLayout rlTop;
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private RelativeLayout rl_viewpager;
    private ViewPager pagerCover;
    private RelativeLayout rlIndicator;
    private CirclePageIndicator indicatorCover;
    private Button btnFavorite;
    private RelativeLayout rlMerchantTitle;
    private RelativeLayout rlLogoMC;
    private CircleImageView imgCircleMC;
    private TextView tvNameMC;
    private TextView tvHeaderTag;
    private LinearLayout ll_list_store;
    private LinearLayout ll_address_single;
    private TextView tv_address_single;
    private ImageView ic_time;
    private TextView tv_time_open;
    private TextView tv_time_open_value;
    private ImageView ic_price;
    private TextView tv_price;
    private TextView tv_price_value;
    private RelativeLayout rl_call;
    private TextView tv_phone;
    private TextView tv_num_store;
    private LinearLayout ll_merchant_info;
    private RelativeLayout rl_voucher_online;
    private TextView tv_count_voucher;
    private TextView tv_info;
    private ImageView iv_back;
    private View v_back;
    private NestedScrollView scroll_view;
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView rcv_voucher;
    private int PROMOTION_SHOW = 3;
    private int STORE_SHOW = 3;
    private boolean isRefreshing = true;
    private FilterModel hashTags = new FilterModel();
    MerchantPagerImageAdapter merchantPagerImageAdapter;
    private int heightWith169Aspect = 0;
    public MerchantModel merchantModel;
    public long currentProductID;
    private String source;
    public ArrayList<StoreModel> listStore;
    public ArrayList<PromotionModel> listPromotion;
    public ArrayList<LixiShopEvoucherModel> listVoucher;
    private static boolean showRatioable = true;
    private String screenName = "home-merchant";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private MerchantDetailActivityBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        currentProductID = getIntent().getLongExtra(ConstantValue.CURRENT_PRODUCT_ID, 0);
        String merchantJson = getIntent().getExtras().getString(ConstantValue.MERCHANT_DETAIL_MODEL, "");
        source = getIntent().getExtras().getString(GlobalTracking.SOURCE);
        if (merchantJson.equals(""))
            finish();
        merchantModel = new Gson().fromJson(merchantJson, MerchantModel.class);
        activity.id = Long.valueOf(merchantModel.getId());
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.MERCHANT,
                activity.id,
                TrackingConstant.Currency.VND,
                0,
                source
        );
        mBinding = DataBindingUtil.setContentView(this, R.layout.merchant_detail_activity);
        initView();
    }


    private void initView() {
        rlBack = findViewById(R.id.rlBack);
        tvTitle = findViewById(R.id.tvTitle);
        rl_viewpager = findViewById(R.id.rl_viewpager);
        pagerCover = findViewById(R.id.pagerCover);
        rlIndicator = findViewById(R.id.rlIndicator);
        indicatorCover = findViewById(R.id.indicatorCover);
        btnFavorite = findViewById(R.id.btnFavorite);
        rlMerchantTitle = findViewById(R.id.rlMerchantTitle);
        rlLogoMC = findViewById(R.id.rlLogoMC);
        imgCircleMC = findViewById(R.id.imgCircleMC);
        tvNameMC = findViewById(R.id.tvNameMC);
        tvHeaderTag = findViewById(R.id.tvHeaderTag);
        ll_address_single = findViewById(R.id.ll_address_single);
        tv_address_single = findViewById(R.id.tv_address_single);
        ic_time = findViewById(R.id.ic_time);
        tv_time_open = findViewById(R.id.tv_time_open);
        tv_time_open_value = findViewById(R.id.tv_time_open_value);
        tv_info = findViewById(R.id.tv_info);
        ic_price = findViewById(R.id.ic_price);
        tv_price = findViewById(R.id.tv_price);
        tv_price_value = findViewById(R.id.tv_price_value);
        rl_call = findViewById(R.id.rl_call);
        tv_phone = findViewById(R.id.ed_phone);
        ll_list_store = findViewById(R.id.ll_list_store);
        tv_num_store = findViewById(R.id.tv_num_store);
        ll_merchant_info = findViewById(R.id.ll_merchant_info);
        tv_count_voucher = findViewById(R.id.tv_count_voucher);
        swipeLayout = findViewById(R.id.swipeLayout);
        rcv_voucher = findViewById(R.id.rclv_voucher_online);
        rl_voucher_online = findViewById(R.id.rl_voucher_online);
        scroll_view = findViewById(R.id.scroll_view);
        v_back = findViewById(R.id.v_back);
        iv_back = findViewById(R.id.iv_back);
        findViewMenu();

        initData();
    }

    private void initData() {
        btnFavorite.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        rl_call.setOnClickListener(this);
        ll_address_single.setOnClickListener(this);
        tv_num_store.setOnClickListener(this);
        tv_count_voucher.setOnClickListener(this);

        listStore = new ArrayList<>();
        listPromotion = new ArrayList<>();
        listVoucher = new ArrayList<>();

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefreshing) {
                    isRefreshing = true;
                    listStore.clear();
                    ll_list_store.removeAllViews();
                    getMerchantDetail();
                }
                swipeLayout.setRefreshing(false);
            }
        });

        rl_viewpager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int availableHeight = rl_viewpager.getMeasuredHeight();
                if (availableHeight > 0) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        rl_viewpager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        rl_viewpager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
//                    rl_viewpager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    //save height here and do whatever you want with it
                    heightWith169Aspect = rl_viewpager.getWidth() * 9 / 16;

                    ViewGroup.LayoutParams params = rl_viewpager.getLayoutParams();
                    params.height = heightWith169Aspect;
                    rl_viewpager.setLayoutParams(params);
                }
            }
        });

        setViewObserver();
        getMerchantDetail();
    }


    private void getMerchantDetail() {

        DataLoader.getMerchantDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swipeLayout.setRefreshing(false);
                if (isSuccess) {
                    merchantModel = (MerchantModel) object;
                    setMerchantDetailData();
                    isRefreshing = false;
                } else {
                    String emptyStr = "";
                    if (emptyStr.equals(object)) {
                        isRefreshing = false;
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                        activity.onBackPressed();
                    }
                }
            }
        }, merchantModel.getId());

    }

    private AdapterVoucherOfMerchant voucherAdapter;

    private void getVoucherOfMerchant() {
        DataLoader.getMerchantEvoucherList(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listVoucher = (ArrayList<LixiShopEvoucherModel>) object;
                    if (listVoucher == null || listVoucher.size() == 0) {
                        rl_voucher_online.setVisibility(View.GONE);
                        return;
                    }

                    int realsize = listVoucher.size();
                    int totalShow = 3;
                    int size = Math.min(listVoucher.size(), totalShow);
                    while (listVoucher.size() > totalShow)
                        listVoucher.remove(totalShow);
                    rl_voucher_online.setVisibility(View.VISIBLE);
                    voucherAdapter = new AdapterVoucherOfMerchant(activity, listVoucher, R.layout.row_lixi_main_voucher_layout, activity.widthScreen, currentProductID);
                    rcv_voucher.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                    rcv_voucher.setAdapter(voucherAdapter);
                    rcv_voucher.setNestedScrollingEnabled(false);
                    if (realsize > size) {
                        tv_count_voucher.setVisibility(View.VISIBLE);
                        tv_count_voucher.setText(getString(R.string.merchant_detail_all) + " (" + realsize + ")");
                    } else tv_count_voucher.setVisibility(View.GONE);
                } else {
                    rl_voucher_online.setVisibility(View.GONE);
                }
            }
        }, 1, merchantModel.getId());
    }

    //region SHOW MERCHANT DATA
    public void setMerchantDetailData() {
        setCoverAdapter(merchantModel.getImage_list());

        //header title
        tvTitle.setText(merchantModel.getName());

        setFavorite();

        //slide photo
        setCoverAdapter(merchantModel.getImage_list());
        //avartar - name - tag
        ImageLoader.getInstance().displayImage(merchantModel.getLogo(), imgCircleMC, LixiApplication.getInstance().optionsNomal);
        tvNameMC.setText(merchantModel.getName());

        String listTagString = "";
        ArrayList<Integer> listOnClick = new ArrayList<Integer>();
        for (int i = 0; i < merchantModel.getTags().size(); i++) {
            if (i == 0) {
                listTagString = "#" + merchantModel.getTags().get(i).getName();
            } else {
                listTagString = listTagString + "  #" + merchantModel.getTags().get(i).getName();
            }

            listOnClick.add(listTagString.length());
        }

        SpannableString ss = new SpannableString(listTagString);
        int index = 0;
        for (int i = 0; i < listOnClick.size(); i++) {
            ss.setSpan(new OnClickTagController(activity, i, new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    FilterModel filterModel = new FilterModel();
                    filterModel.setKeyWord("#" + merchantModel.getTags().get(pos).getName());
                    Bundle data = new Bundle();
                    data.getBoolean("ISSEARCHCLICK", false);
                    data.putString(ConstantValue.FILTER_MODEL_MERCHANT, new Gson().toJson(filterModel));
                    Intent intent = new Intent(activity, SearchActivity.class);
                    intent.putExtras(data);
                    activity.startActivity(intent);
                }
            }), index, listOnClick.get(i), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            index = listOnClick.get(i);
        }

        tvHeaderTag.setText(ss);
        tvHeaderTag.setMovementMethod(LinkMovementMethod.getInstance());

        if (listTagString.equalsIgnoreCase("")) {
            tvHeaderTag.setVisibility(View.GONE);
        } else {

            tvHeaderTag.setVisibility(View.VISIBLE);
        }

        //get voucher
        getVoucherOfMerchant();

        //Menu
        getListMenu();

        //Thông tin địa điểm
        tv_time_open_value.setText(merchantModel.getOpening_time() + " - " + merchantModel.getClose_time());
        setOpenStatus();
        tv_price_value.setText(Helper.getVNCurrency(merchantModel.getMin_price()) + "đ - " + Helper.getVNCurrency(merchantModel.getMax_price()) + "đ");
        tv_phone.setText(merchantModel.getPhone());

        //Ds chi nhánh
        getListStore();

    }

    public void setOpenStatus() {
        String[] arro = merchantModel.getOpening_time().replace(" ", "").split(":");
        String[] arrc = merchantModel.getClose_time().replace(" ", "").split(":");

        int to = Integer.parseInt(arro[0]) * 60 + Integer.parseInt(arro[1]);
        int tc = Integer.parseInt(arrc[0]) * 60 + Integer.parseInt(arrc[1]);

        Calendar c = Calendar.getInstance();
        int tn = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);

        if (tc > to) {
            if (tn >= to && tn <= tc)
                tv_time_open.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_time_open, 0);
            else
                tv_time_open.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_time_close_gray, 0);
        } else {
            if (tn >= to && tn <= tc)
                tv_time_open.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_time_close_gray, 0);
            else
                tv_time_open.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_time_open, 0);
        }
    }

    private void setFavorite() {
        if (merchantModel.getIs_favorite().equals("true")) {
            btnFavorite.setBackgroundResource(R.drawable.ic_favorite_small_selected);
        } else {
            btnFavorite.setBackgroundResource(R.drawable.ic_dislike);
        }
    }

    private void setViewObserver() {

        scroll_view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int y = scroll_view.getScrollY();
                Helper.setScrollProrgess(y, v_back, tvTitle, iv_back);
                if (scroll_view.getChildAt(0).getBottom() <= (scroll_view.getHeight() + scroll_view.getScrollY())) {
                    //scroll view is at bottom
                    tvTitle.setTextColor(0xFF000000);
                    v_back.setBackgroundColor(0xFFFFFFFF);
                    tvTitle.setVisibility(View.VISIBLE);
                    v_back.setVisibility(View.VISIBLE);
                    iv_back.setColorFilter(Color.argb(255, 172, 30, 161));
                } else {
                    //scroll view is not at bottom
                }

            }
        });

    }

    private void setCoverAdapter(ArrayList<ProductImageModel> listItem) {
        merchantPagerImageAdapter = new MerchantPagerImageAdapter(activity, listItem);
        pagerCover.setAdapter(merchantPagerImageAdapter);
        indicatorCover.setViewPager(pagerCover);

        rlIndicator.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.value_10) * (merchantPagerImageAdapter.getCount() + 1);
        rlIndicator.requestLayout();
    }
    //endregion


    private void addFavoriteMerchant() {
        try {

            if (merchantModel.getIs_favorite().equals("true")) {
                merchantModel.setIs_favorite("false");
            } else {
                merchantModel.setIs_favorite("true");
            }
            setFavorite();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", merchantModel.getId());

            DataLoader.addFavouriteMerchant(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        setFavorite();
                    } else {
                        if (merchantModel.getIs_favorite().equals("true")) {
                            merchantModel.setIs_favorite("false");
                            Helper.showErrorDialog(activity, "Có lỗi xảy ra khi thêm vào danh sách quan tâm.");
                        } else {
                            merchantModel.setIs_favorite("true");
                            Helper.showErrorDialog(activity, "Có lỗi xảy ra khi bỏ ra khỏi danh sách quan tâm.");
                        }
                        setFavorite();
                    }
                }
            }, jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void sharePromotion(MerchantModel merchantModel) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_SUBJECT, merchantModel.getName());

        String extraText = "";

        extraText = extraText + merchantModel.getUrl() + "\n";

        intent.putExtra(Intent.EXTRA_TEXT, extraText);
        activity.startActivity(Intent.createChooser(intent, "Chia sẻ"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                handleLoginResultAction(data);
        }
    }

    private void handleLoginResultAction(Intent intent) {
        Bundle data = intent.getExtras();
        String action = data.getString(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        switch (action) {
            case ACTION_FAVORITE_PROMOTION:
                addFavoriteMerchant();
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btnFavorite:
                if (LoginHelper.isLoggedIn(activity)) {
                    addFavoriteMerchant();
                } else {
                    LoginHelper.startLoginActivityWithAction(activity, ACTION_FAVORITE_PROMOTION);
                }
                break;
            case R.id.ll_address_single:
                Intent map = new Intent(activity, StoreMapActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
                map.putExtras(bundle);
                startActivity(map);
                break;
            case R.id.tv_count_voucher:
                if (listVoucher.size() > 0) {
                    Intent intent = new Intent(activity, ListEvoucherOfMerchantActivity.class);
                    intent.putExtra("ID", merchantModel.getId());
                    intent.putExtra("NAMEMERCHANT", merchantModel.getName());
                    startActivity(intent);
                }
                break;
            case R.id.rl_call:
                Helper.callPhone(activity, merchantModel.getPhone());
                break;
            case R.id.tv_num_store:
                if (listStore.size() > STORE_SHOW) {
                    //   ll_list_store.removeAllViews();
                    for (int i = STORE_SHOW; i < listStore.size(); i++) {
                        new StoreController(activity, listStore.get(i), merchantModel, i, listStore.size(), ll_list_store);
                    }
                }
                tv_num_store.setVisibility(View.GONE);
                break;
        }
    }

    //region list STORE
    private void getListStore() {
        Helper.getGPSLocation(activity, new LocationCallBack() {
            @Override
            public void callback(LatLng position, ArrayList<String> addressInfo) {
                DataLoader.getMerchantStore(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            ArrayList<StoreModel> listItem = (ArrayList<StoreModel>) object;
                            listStore = listItem;
                            merchantModel.setListStore(listItem);
                            if (listItem.size() > 1) {
                                ll_list_store.setVisibility(View.VISIBLE);
                                ll_address_single.setVisibility(View.GONE);
                                int l = Math.min(STORE_SHOW, listItem.size());
                                for (int i = 0; i < l; i++) {
                                    new StoreController(activity, listItem.get(i), merchantModel, i, STORE_SHOW, ll_list_store);
                                }
                                if (listItem.size() > STORE_SHOW) {
                                    tv_num_store.setText(getString(R.string.merchant_detail_see_all) + " " + listItem.size() + " " + getString(R.string.merchant_detail_branch));
                                    tv_num_store.setVisibility(View.VISIBLE);
                                } else
                                    tv_num_store.setVisibility(View.GONE);
                            } else {
                                ll_list_store.setVisibility(View.GONE);
                                tv_num_store.setVisibility(View.GONE);
                                ll_address_single.setVisibility(View.VISIBLE);
                                try {
                                    tv_address_single.setText(listStore.get(0).getAddress());
                                } catch (IndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                    ll_address_single.setVisibility(View.GONE);
                                }
                            }
                            if (listPromotion != null)
                                for (int i = 0; i < listPromotion.size(); i++) {
                                    listPromotion.get(i).setListStore(listStore);
                                }
                        } else {
                            ll_list_store.setVisibility(View.GONE);
                            tv_num_store.setVisibility(View.GONE);
                            ll_address_single.setVisibility(View.GONE);
                        }
                    }
                }, merchantModel.getId(), position);
            }
        });
    }

    //endregion

    //region listMENU
    private RecyclerView rv_menu;
    private RelativeLayout rl_menu;
    private MerchantDetailMenuAdapter adapterMenu;
    private List<MenuModel> listMenu;
    private boolean isLoadingMenu;
    private boolean noRemainMenu = false;
    private int pageMenu = 1;
    private int pageSizemenu = DataLoader.record_per_page;
    private int totalMenu;

    private MerchantDetailMenuDialog dialogMenu;

    public void findViewMenu() {
        rl_menu = findViewById(R.id.rl_menu);
        rv_menu = findViewById(R.id.rv_menu);
        rv_menu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }


    private void getListMenu() {
        isLoadingMenu = true;
        DataLoader.getMerchantMenu(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ApiWrapperForListModel<MenuModel> result = (ApiWrapperForListModel<MenuModel>) object;
                    List<MenuModel> list = result.getRecords();
                    if (pageMenu == 1) {
                        totalMenu = result.get_meta().getTotal();
                        if (list.size() > 0) {
                            rl_menu.setVisibility(View.VISIBLE);

                            listMenu = list;
                            setMenuData();
                        } else {
                            rl_menu.setVisibility(View.GONE);
                            noRemainMenu = true;
                        }
                    } else {
                        if (list.size() > 0) {
                            listMenu.addAll(list);
                            adapterMenu.notifyDataSetChanged();
                            if (dialogMenu != null && dialogMenu.isShowing())
                                dialogMenu.upDateData();
                        } else {
                            noRemainMenu = true;
                        }
                    }
                    isLoadingMenu = false;
                    pageMenu++;
                } else {
                    if (pageMenu == 1) {
                        rl_menu.setVisibility(View.GONE);
                    } else {
                        noRemainMenu = true;
                    }
                    isLoadingMenu = false;
                }
            }
        }, pageMenu, merchantModel.getId());
    }

    public boolean isRvBottom() {
        if (listMenu == null || listMenu.size() == 0)
            return false;
        int totalItemCount = listMenu.size();
        int lastVisibleItem = ((LinearLayoutManager) rv_menu.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 5 && listMenu.size() >= pageSizemenu);
    }

    public void setMenuData() {
        adapterMenu = new MerchantDetailMenuAdapter(this, listMenu, totalMenu);
        rv_menu.setAdapter(adapterMenu);

        rv_menu.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isRvBottom() && !isLoadingMenu && !noRemainMenu)
                    getListMenu();
            }
        });

        adapterMenu.setIDidalogEventListener(new MerchantDetailMenuAdapter.IAdapterEvent() {
            @Override
            public void onShowSlide(int pos) {
                dialogMenu = new MerchantDetailMenuDialog(MerchantDetailActivity.this, false, false, false);
                dialogMenu.show();
                dialogMenu.setData(listMenu, pos, totalMenu);
                dialogMenu.setIDidalogEventListener(new MerchantDetailMenuDialog.IDidalogEvent() {
                    @Override
                    public void onLoadmore() {
                        if (!isLoadingMenu && !noRemainMenu)
                            getListMenu();
                    }

                    @Override
                    public void onCall() {
                        Helper.callPhone(activity, merchantModel.getPhone());
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setViews();
    }

    private void setViews() {
        tv_info.setText(getResources().getString(R.string.merchant_detail_place));
        tv_time_open.setText(getString(R.string.merchant_detail_open_time));
        tv_price.setText(getString(R.string.merchant_detail_average_price));
    }
    //endregion
}
