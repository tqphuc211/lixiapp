package com.opencheck.client.home.delivery.adapter;

import android.databinding.DataBindingUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.HeaderSectionLayoutBinding;
import com.opencheck.client.databinding.ItemCountQuantityLayoutBinding;
import com.opencheck.client.databinding.ItemMultiChoiceLayoutBinding;
import com.opencheck.client.databinding.ItemSingleChoiceLayoutBinding;
import com.opencheck.client.home.delivery.libs.sticky_recycler_view.SectioningAdapter;
import com.opencheck.client.home.delivery.model.AddonCategoryModel;
import com.opencheck.client.home.delivery.model.AddonModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class AddonCategoryAdapter extends SectioningAdapter {

    private final static int CIRCLE_ITEM = 0;
    private final static int SQUARE_ITEM = 1;
    private final static int NUMBER_ITEM = 2;

    private LixiActivity activity;
    private ArrayList<AddonCategoryModel> addonCategories;

    private HashMap<String, Integer> mMapClickedLastPosition = new HashMap<>();
    private HashMap<String, Boolean> mMapRequiredAddon = new HashMap<>();
    private HashMap<String, ArrayList<AddonModel>> mMapCurrentAddons = new HashMap<>();
    private int[] currentQuantityOfSection;
    private int[] lastPositionOfNumber;
    private boolean isClickedAddingToCart = false;

    private AddonCallback mAddonCallback;

    public AddonCategoryAdapter(LixiActivity activity, ArrayList<AddonCategoryModel> addonCategories) {
        this.activity = activity;
        this.addonCategories = addonCategories;
        currentQuantityOfSection = new int[addonCategories.size()];
        lastPositionOfNumber = new int[addonCategories.size()];
    }

    @Override
    public int getNumberOfSections() {
        return addonCategories.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        Log.d("section", "amount in section " + sectionIndex + "=" + addonCategories.get(sectionIndex).getList_addon().size());
        return addonCategories.get(sectionIndex).getList_addon().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        if (addonCategories.get(sectionIndex).getData_type().equals("boolean")) {
            if (addonCategories.get(sectionIndex).isMultichoice()) {
                return SQUARE_ITEM;
            }
            return CIRCLE_ITEM;
        }
        return NUMBER_ITEM;
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerUserType) {
        HeaderSectionLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.header_section_layout, null, false);
        return new HeaderSectionViewHolder(binding);
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        switch (itemType) {
            case CIRCLE_ITEM:
                ItemSingleChoiceLayoutBinding circleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_single_choice_layout, null, false);
                return new CircleViewHolder(circleBinding);
            case SQUARE_ITEM:
                ItemMultiChoiceLayoutBinding squareBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_multi_choice_layout, null, false);
                return new SquareViewHolder(squareBinding);
            case NUMBER_ITEM:
                ItemCountQuantityLayoutBinding numberBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_count_quantity_layout, null, false);
                return new NumberViewHolder(numberBinding);
        }
        throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int sectionIndex, int headerUserType) {
        ((HeaderSectionViewHolder) viewHolder).binding(sectionIndex);
    }

    @Override
    public void onBindItemViewHolder(ItemViewHolder viewHolder, int sectionIndex, int itemIndex, int itemType) {
        switch (itemType) {
            case CIRCLE_ITEM: {
                ((CircleViewHolder) viewHolder).binding(sectionIndex, itemIndex);
                break;
            }
            case SQUARE_ITEM: {
                ((SquareViewHolder) viewHolder).binding(sectionIndex, itemIndex);
                break;
            }
            case NUMBER_ITEM: {
                ((NumberViewHolder) viewHolder).binding(sectionIndex, itemIndex);
                break;
            }
            default:
                throw new IllegalArgumentException("Unrecognized itemType: " + itemType);

        }
    }

    class HeaderSectionViewHolder extends SectioningAdapter.HeaderViewHolder {
        HeaderSectionLayoutBinding mBinding;

        HeaderSectionViewHolder(HeaderSectionLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void binding(int sectionIndex) {
            mBinding.tvNameAddonCategory.setText(addonCategories.get(sectionIndex).getName());
            if (addonCategories.get(sectionIndex).isRequired()) {
                mBinding.tvRequired.setVisibility(View.VISIBLE);

                if (isClickedAddingToCart) {
                    if (checkRequireAddonCompleted(sectionIndex)) {
                        mBinding.tvNameAddonCategory.setTextColor(activity.getResources().getColor(R.color.color_header_topping_name));
                        mBinding.tvRequired.setTextColor(activity.getResources().getColor(R.color.color_required));
                    } else {
                        mBinding.tvNameAddonCategory.setTextColor(activity.getResources().getColor(R.color.red_point));
                        mBinding.tvRequired.setTextColor(activity.getResources().getColor(R.color.red_point));
                    }
                }
            } else {
                mBinding.tvNameAddonCategory.setTextColor(activity.getResources().getColor(R.color.color_header_topping_name));
                mBinding.tvRequired.setTextColor(activity.getResources().getColor(R.color.color_required));
                mBinding.tvRequired.setVisibility(View.GONE);
            }
        }

        boolean checkRequireAddonCompleted(int sectionIndex) {
            return addonCategories.get(sectionIndex).isRequired() && mMapRequiredAddon.containsKey(addonCategories.get(sectionIndex).getName());
        }
    }

    class CircleViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener {
        ItemSingleChoiceLayoutBinding mBinding;

        CircleViewHolder(ItemSingleChoiceLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mBinding.getRoot().setOnClickListener(this);
        }

        void binding(int sectionIndex, int itemIndex) {
            AddonModel model = addonCategories.get(sectionIndex).getList_addon().get(itemIndex);
            mBinding.tvName.setText(model.getName());

            showPausedAddon(model);

            Boolean defaultValue = (Boolean) model.getDefault_value();
            if (defaultValue != null) {
                model.setSelected(defaultValue);
                model.setDefault_value(null);
            }

            if (model.isSelected()) {
                mBinding.ivTick.setImageResource(R.drawable.ic_tick_violet);
            } else {
                mBinding.ivTick.setImageResource(R.drawable.bg_circle_grey);
            }

            if (itemIndex == addonCategories.get(sectionIndex).getList_addon().size() - 1) {
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.white));
            } else
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.gray));

            mAddonCallback.onUpdate();
        }

        void showPausedAddon(AddonModel model) {
            if (model.getState().equals("paused")) {
                mBinding.tvPrice.setVisibility(View.GONE);
                mBinding.tvPausedMessage.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvPausedMessage.setVisibility(View.GONE);
                mBinding.tvPrice.setVisibility(View.VISIBLE);
                if (model.getPrice_modifier() > 0) {
                    mBinding.tvPrice.setText(Helper.getVNCurrency(model.getPrice_modifier()) + "đ");
                } else mBinding.tvPrice.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            int sectionIndex = getSectionForAdapterPosition(position);
            int itemIndex = getPositionOfItemInSection(sectionIndex, position);

            AddonCategoryModel currentSection = addonCategories.get(sectionIndex);
            AddonModel currentAddon = currentSection.getList_addon().get(itemIndex);

            if (currentAddon.getState().equals("paused")) {
                Toast.makeText(activity, currentAddon.getName() + " đã hết, vui lòng chọn loại khác", Toast.LENGTH_SHORT).show();
            } else {
                if (mMapClickedLastPosition.containsKey(currentSection.getName())) {
                    int lastPosition = mMapClickedLastPosition.get(currentSection.getName());
                    if (currentSection.isRequired()) {
                        if (itemIndex != lastPosition) {
                            addonCategories.get(sectionIndex).getList_addon().get(lastPosition).setSelected(false);
                            addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setSelected(true);
                            mMapClickedLastPosition.put(currentSection.getName(), itemIndex);
                        }
                    } else {
                        currentAddon.setSelected(!currentAddon.isSelected());
                        if (itemIndex != lastPosition) {
                            addonCategories.get(sectionIndex).getList_addon().get(lastPosition).setSelected(false);
                            addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setSelected(true);

                        } else {
                            addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setSelected(currentAddon.isSelected());
                            addonCategories.get(sectionIndex).getList_addon().get(lastPosition).setSelected(currentAddon.isSelected());
                        }
                        mMapClickedLastPosition.put(currentSection.getName(), itemIndex);
                    }
                } else {
                    mMapClickedLastPosition.put(currentSection.getName(), itemIndex);
                    addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setSelected(true);
                }

                if (currentAddon.isSelected()) {
                    ArrayList<AddonModel> tempList = new ArrayList<>();
                    tempList.add(currentAddon);
                    mMapCurrentAddons.put(currentSection.getName(), tempList);
                } else mMapCurrentAddons.get(currentSection.getName()).remove(currentAddon);

                // Thêm các required addon vào list
                if (currentSection.isRequired())
                    mMapRequiredAddon.put(currentSection.getName(), true);

                notifySectionDataSetChanged(sectionIndex);
            }
        }
    }

    class SquareViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener {
        ItemMultiChoiceLayoutBinding mBinding;

        SquareViewHolder(ItemMultiChoiceLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mBinding.getRoot().setOnClickListener(this);
        }

        void binding(int sectionIndex, int itemIndex) {
            AddonModel model = addonCategories.get(sectionIndex).getList_addon().get(itemIndex);
            mBinding.tvName.setText(model.getName());

            showPausedAddon(model);

            if (model.isSelected())
                mBinding.cb.setChecked(true);
            else mBinding.cb.setChecked(false);

            if (itemIndex == addonCategories.get(sectionIndex).getList_addon().size() - 1) {
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.white));
            } else
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.gray));

        }

        void showPausedAddon(AddonModel model) {
            if (model.getState().equals("paused")) {
                mBinding.tvPrice.setVisibility(View.GONE);
                mBinding.tvPausedMessage.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvPausedMessage.setVisibility(View.GONE);
                mBinding.tvPrice.setVisibility(View.VISIBLE);
                if (model.getPrice_modifier() > 0) {
                    mBinding.tvPrice.setText(Helper.getVNCurrency(model.getPrice_modifier()) + "đ");
                } else mBinding.tvPrice.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            int sectionIndex = getSectionForAdapterPosition(position);
            int itemIndex = getPositionOfItemInSection(sectionIndex, position);

            AddonCategoryModel currentSection = addonCategories.get(sectionIndex);
            AddonModel currentAddon = currentSection.getList_addon().get(itemIndex);

            if (currentAddon.getState().equals("paused")) {
                Toast.makeText(activity, currentAddon.getName() + " đã hết, vui lòng chọn loại khác", Toast.LENGTH_SHORT).show();
            } else {
                currentAddon.setSelected(!currentAddon.isSelected());
                if (currentAddon.isSelected())
                    currentQuantityOfSection[sectionIndex]++;
                else currentQuantityOfSection[sectionIndex]--;

                //Kiểm tra số lượng hiện tại của section đã vượt max chưa
                if (currentSection.getMax_total_quantity() != null
                        && currentQuantityOfSection[sectionIndex] > currentSection.getMax_total_quantity()) {
                    currentQuantityOfSection[sectionIndex]--;
                    Toast.makeText(activity,
                            "Số lượng tối đa được chọn của " + currentSection.getName() + " là " + currentSection.getMax_total_quantity(), Toast.LENGTH_SHORT).show();
                } else {
                    if (currentAddon.isSelected()) {
                        mBinding.cb.setChecked(true);
                        if (mMapCurrentAddons.containsKey(currentSection.getName())) {
                            mMapCurrentAddons.get(currentSection.getName()).add(currentAddon);
                        } else {
                            ArrayList<AddonModel> tempList = new ArrayList<>();
                            tempList.add(currentAddon);
                            mMapCurrentAddons.put(currentSection.getName(), tempList);
                        }
                    } else {
                        mBinding.cb.setChecked(false);
                        mMapCurrentAddons.get(currentSection.getName()).remove(currentAddon);
                    }

                    if (mMapCurrentAddons.containsKey(currentSection.getName())) {
                        Collections.sort(mMapCurrentAddons.get(currentSection.getName()), new Comparator<AddonModel>() {
                            @Override
                            public int compare(AddonModel a1, AddonModel a2) {
                                return a1.getName().compareTo(a2.getName());
                            }
                        });
                    }
                    addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setSelected(currentAddon.isSelected());
                    mAddonCallback.onUpdate();
                }

                //Thêm các required addon vào list
                if (currentSection.isRequired()) {
                    if (currentQuantityOfSection[sectionIndex] > 0)
                        mMapRequiredAddon.put(currentSection.getName(), true);
                    else mMapRequiredAddon.remove(currentSection.getName());
                }
            }
        }
    }

    class NumberViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener {
        ItemCountQuantityLayoutBinding mBinding;

        NumberViewHolder(ItemCountQuantityLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;

            mBinding.getRoot().setOnClickListener(this);
            mBinding.ivSub.setOnClickListener(this);
        }

        void binding(int sectionIndex, int itemIndex) {
            AddonModel model = addonCategories.get(sectionIndex).getList_addon().get(itemIndex);
            mBinding.tvName.setText(model.getName());

            showPausedAddon(model);

            Double quantity = Double.valueOf(String.valueOf(model.getDefault_value()));
            if (quantity < 1.0) {
                mBinding.ivSub.setVisibility(View.GONE);
                mBinding.tvQuantity.setVisibility(View.GONE);
            } else {
                mBinding.ivSub.setVisibility(View.VISIBLE);
                mBinding.tvQuantity.setVisibility(View.VISIBLE);
                mBinding.tvQuantity.setText(quantity.intValue() + "x");
            }

            if (itemIndex == addonCategories.get(sectionIndex).getList_addon().size() - 1) {
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.white));
            } else
                mBinding.vLine.setBackgroundColor(activity.getResources().getColor(R.color.gray));
        }

        void showPausedAddon(AddonModel model) {
            if (model.getState().equals("paused")) {
                mBinding.tvPrice.setVisibility(View.GONE);
                mBinding.tvPausedMessage.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvPausedMessage.setVisibility(View.GONE);
                mBinding.tvPrice.setVisibility(View.VISIBLE);
                if (model.getPrice_modifier() > 0) {
                    mBinding.tvPrice.setText(Helper.getVNCurrency(model.getPrice_modifier()) + "đ");
                } else mBinding.tvPrice.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            int sectionIndex = getSectionForAdapterPosition(position);
            int itemIndex = getPositionOfItemInSection(sectionIndex, position);

            AddonCategoryModel currentSection = addonCategories.get(sectionIndex);
            AddonModel currentAddon = currentSection.getList_addon().get(itemIndex);

            if (view == mBinding.getRoot()) {
                if (currentAddon.getState().equals("paused")) {
                    Toast.makeText(activity, currentAddon.getName() + " đã hết, vui lòng chọn loại khác!", Toast.LENGTH_SHORT).show();
                } else {
                    // Nếu chỉ cho phép chọn 1 loại sản phẩm
                    if (!currentSection.isMultichoice()) {
                        //Làm nếu chưa chọn hoặc chọn đúng vị trí cũ
                        if (lastPositionOfNumber[sectionIndex] == -1 || lastPositionOfNumber[sectionIndex] == itemIndex) {
                            lastPositionOfNumber[sectionIndex] = itemIndex;
                        } else return;
                    }

                    Double count = Double.valueOf(String.valueOf(currentAddon.getDefault_value())) + 1;
                    currentQuantityOfSection[sectionIndex]++;

                    //Nếu tổng số topping chưa vượt max
                    if (currentQuantityOfSection[sectionIndex] <= currentSection.getMax_total_quantity()) {
                        if (count > currentAddon.getMax_number_value()) {
                            currentQuantityOfSection[sectionIndex]--;
                            Toast.makeText(activity,
                                    currentAddon.getName() + " chỉ chọn được tối đa " + currentAddon.getMax_number_value(), Toast.LENGTH_SHORT).show();
                        } else {
                            addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setDefault_value(count);
                            currentAddon.setDefault_value(count);
                            mBinding.ivSub.setVisibility(View.VISIBLE);
                            mBinding.tvQuantity.setVisibility(View.VISIBLE);
                            mBinding.tvQuantity.setText(count.intValue() + "x");

                            //Thêm addon vào danh sách
                            if (mMapCurrentAddons.containsKey(currentSection.getName())) {
                                if (mMapCurrentAddons.get(currentSection.getName()).contains(currentAddon)) {
                                    int index = mMapCurrentAddons.get(currentSection.getName()).indexOf(currentAddon);
                                    if (Double.valueOf(String.valueOf(currentAddon.getDefault_value())) == 0)
                                        mMapCurrentAddons.get(currentSection.getName()).remove(index);
                                    else
                                        mMapCurrentAddons.get(currentSection.getName()).get(index).setDefault_value(currentAddon.getDefault_value());
                                } else
                                    mMapCurrentAddons.get(currentSection.getName()).add(currentAddon);

                                Collections.sort(mMapCurrentAddons.get(currentSection.getName()), new Comparator<AddonModel>() {
                                    @Override
                                    public int compare(AddonModel a1, AddonModel a2) {
                                        return a1.getName().compareTo(a2.getName());
                                    }
                                });

                            } else {
                                ArrayList<AddonModel> tempList = new ArrayList<>();
                                tempList.add(currentAddon);
                                mMapCurrentAddons.put(currentSection.getName(), tempList);
                            }

                            mAddonCallback.onUpdate();
                        }
                    } else {
                        currentQuantityOfSection[sectionIndex]--;
                        Toast.makeText(activity,
                                "Tổng số lượng tối đa trong " + currentSection.getName() + " là " + currentSection.getMax_total_quantity(), Toast.LENGTH_SHORT).show();
                    }

                    if (currentSection.isRequired()) {
                        if (currentQuantityOfSection[sectionIndex] > 0)
                            mMapRequiredAddon.put(currentSection.getName(), true);
                        else mMapRequiredAddon.remove(currentSection.getName());
                    }
                }

            } else if (view == mBinding.ivSub) {
                currentQuantityOfSection[sectionIndex]--;
                Double count = Double.valueOf(String.valueOf(currentAddon.getDefault_value())) - 1;
                if (count < 1.0) {
                    lastPositionOfNumber[sectionIndex] = -1;
                    mMapCurrentAddons.get(currentSection.getName()).remove(currentAddon);
                    mBinding.tvQuantity.setVisibility(View.GONE);
                    mBinding.ivSub.setVisibility(View.GONE);
                } else {
                    mBinding.tvQuantity.setText(count.intValue() + "x");
                    int index = mMapCurrentAddons.get(currentSection.getName()).indexOf(currentAddon);
                    mMapCurrentAddons.get(currentSection.getName()).get(index).setDefault_value(count);
                }

                if (currentSection.isRequired()) {
                    if (currentQuantityOfSection[sectionIndex] > 0)
                        mMapRequiredAddon.put(currentSection.getName(), true);
                    else mMapRequiredAddon.remove(currentSection.getName());
                }

                addonCategories.get(sectionIndex).getList_addon().get(itemIndex).setDefault_value(count);
                mAddonCallback.onUpdate();
            }
        }
    }

    public void setAddingToCartState(boolean isClicked) {
        this.isClickedAddingToCart = isClicked;
    }

    public void checkValidProductQuantity() {
        boolean isCompleted = true;
        for (int i = 0; i < addonCategories.size(); i++) {
            AddonCategoryModel model = addonCategories.get(i);
            if (mMapCurrentAddons.containsKey(model.getName())) {
                if (model.getData_type().equals("boolean") && !model.isMultichoice())
                    continue;
                if (model.getMin_total_quantity() != null && model.getMin_total_quantity() > currentQuantityOfSection[i]) {
                    mAddonCallback.onError(model, i);
                    isCompleted = false;
                    break;
                }
            }
        }

        if (isCompleted) {
            mAddonCallback.onCompleted();
        }
    }

    public int getItemTotalBeforeSection(int sectionIndex) {
        int res = 1;
        for (int i = 0; i < sectionIndex; i++) {
            res += getNumberOfItemsInSection(i) + 1 + i;
        }
        Log.d("section", res + "");
        return res;
    }

    public int getPositionRequiredSection() {
        for (int i = 0; i < addonCategories.size(); i++) {
            if (addonCategories.get(i).isRequired() && !mMapRequiredAddon.containsKey(addonCategories.get(i).getName())) {
                Log.d("section", i + "");
                return getItemTotalBeforeSection(i);
            }
        }
        return -1;
    }

    public HashMap<String, ArrayList<AddonModel>> getMapCurrentAddons() {
        return mMapCurrentAddons;
    }

    public HashMap<String, Boolean> getMapRequiredAddon() {
        return mMapRequiredAddon;
    }

    public void setAddonCallback(AddonCallback addonCallback) {
        this.mAddonCallback = addonCallback;
    }

    public interface AddonCallback {
        void onUpdate();

        void onError(AddonCategoryModel model, int sectionIndex);

        void onCompleted();
    }
}
