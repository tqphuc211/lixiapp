package com.opencheck.client.home.account.evoucher.Model;

import com.opencheck.client.models.LixiModel;

import java.util.ArrayList;

public class GroupCode extends LixiModel {

    private ArrayList<Integer> evouchers_ids;
    private String group_uuid;
    private long id;
    private long merchant_id;
    private long user_id;

    public ArrayList<Integer> getEvouchers_ids() {
        return evouchers_ids;
    }

    public void setEvouchers_ids(ArrayList<Integer> evouchers_ids) {
        this.evouchers_ids = evouchers_ids;
    }

    public String getGroup_uuid() {
        return group_uuid;
    }

    public void setGroup_uuid(String group_uuid) {
        this.group_uuid = group_uuid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }
}
