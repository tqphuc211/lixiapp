package com.opencheck.client.home.LixiHomeV25;

import android.content.Intent;
import android.graphics.Paint;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.AutoScroll.AutoScrollViewPager;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.RowItemBannerBinding;
import com.opencheck.client.databinding.RowLixiMainCategoryLayoutBinding;
import com.opencheck.client.databinding.RowLixiMainFlashSaleBinding;
import com.opencheck.client.databinding.RowLixiMainThreeImageLayoutBinding;
import com.opencheck.client.databinding.RowLixiMainTopVoucherLayoutBinding;
import com.opencheck.client.databinding.RowLixiMainVoucherIndicatorLayoutBinding;
import com.opencheck.client.databinding.RowLixiMainVoucherLayoutBinding;
import com.opencheck.client.home.LixiHomeV25.CollectionContent.ActivityCollectionListProduct;
import com.opencheck.client.home.flashsale.Activity.FlashSaleDetailActivity;
import com.opencheck.client.home.flashsale.Adapter.FlashSaleHomeAdapter;
import com.opencheck.client.home.flashsale.Global.VoucherFlashSaleHolder;
import com.opencheck.client.home.flashsale.Interface.OnFinishTimerListener;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.View.TimerView;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.ProductCategoryModel;
import com.opencheck.client.models.ProductCollectionModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.BannerModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class LixiHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LixiActivity activity;

    public static int BANNER = 0;
    public static int CATEGORY = 1;
    public static int COLLECTION = 2;
    public static int FLASH_SALE = 3;
    public static int TOPVOUCHER = 4;
    public static int VOUCHER_INDICATOR = 5;
    public static int VOUCHER = 6;
    public static int VOUCHER_FS = -1;

    private ArrayList<BannerModel> list_banner;
    private ArrayList<ProductCategoryModel> list_category;
    private ArrayList<ProductCollectionModel> list_collection;
    private ArrayList<FlashSaleNow.ProductsBean> listFlashSale;
    private ArrayList<LixiShopEvoucherModel> list_voucher_top_hot;

    private int currentTab = 0;

    public ArrayList<LixiShopEvoucherModel> getList_voucher() {
        return list_voucher;
    }

    private ArrayList<LixiShopEvoucherModel> list_voucher;

    private AdapterBanner bannerAdapter;
    private AdapterCategory categoryAdapter;
    private FlashSaleHomeAdapter flashSaleAdapter;
    private AdapterVoucherTopHot adapterVoucherTopHot;

    private boolean hasFlashSale = false;
    private long currentTimeFS;
    private boolean isHappeningFS;
    // nếu ẩn item flashsale trước lần render đầu tiên thì timer trong flashSale ko hiện dc
    // ko rõ nguyên nhân flashSaleDataSeted đánh dấu đã có dử liệu để render
    private boolean flashSaleDataSeted = false;

    private BannerHolder bannerHolder;
    private CategoryHolder categoryHolder;
    private CollectionHolder collectionHolder;
    private FlashSaleHolder flashSaleHolder;
    private TopVoucherHolder topVoucherHolder;
    private VoucherIndicatorHolder voucherIndicatorHolder;

    public float getIndicatorPos() {
        if (voucherIndicatorHolder != null)
            return voucherIndicatorHolder.itemView.getY();
        return 0;
    }

    private ItemClickListener itemClickListener;

    public LixiHomeAdapter(LixiActivity lixiActivity, ArrayList<LixiShopEvoucherModel> list_voucher, ItemClickListener itemClickListener) {
        this.list_voucher = list_voucher;
        this.activity = lixiActivity;
        this.itemClickListener = itemClickListener;
    }

    //region BASE ADAPTER
    @Override
    public int getItemCount() {
        if (list_voucher == null || list_voucher.size() == 0)
            return VOUCHER;
        return list_voucher.size() + VOUCHER;

    }

    @Override
    public int getItemViewType(int position) {
        if (position < VOUCHER)
            return position;
        if (list_voucher != null && list_voucher.size() > (position - VOUCHER))
            if (list_voucher.get(position - VOUCHER).getFlash_sale_id() > 0)
                return VOUCHER_FS;
        return VOUCHER;
    }

//    int count = 0;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.d("mapi", "create view item: " + count++);
        if (viewType == BANNER) {
            RowItemBannerBinding rowItemBannerBinding =
                    RowItemBannerBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            bannerHolder = new BannerHolder(rowItemBannerBinding.getRoot());

            int height107 = (int) (activity.widthScreen * ConfigModel.getInstance(activity).getHome_banner_ratio());
            ViewGroup.LayoutParams layoutParams = bannerHolder.view_pager.getLayoutParams();
            layoutParams.height = height107;
            bannerHolder.view_pager.setLayoutParams(layoutParams);

            bannerAdapter = new AdapterBanner(activity, list_banner, activity.widthScreen);
            bannerHolder.view_pager.setAdapter(bannerAdapter);
            bannerHolder.view_pager.startAutoScroll();

            bannerHolder.indicator.setViewPager(bannerHolder.view_pager);
            bannerHolder.indicator.requestLayout();

            return bannerHolder;

        }
        if (viewType == CATEGORY) {
            RowLixiMainCategoryLayoutBinding rowLixiMainCategoryLayoutBinding =
                    RowLixiMainCategoryLayoutBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);
            categoryHolder = new CategoryHolder(rowLixiMainCategoryLayoutBinding.getRoot());

            categoryAdapter = new AdapterCategory(activity, list_category);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            categoryHolder.rvCategory.setLayoutManager(layoutManager);
            categoryHolder.rvCategory.setAdapter(categoryAdapter);

            return categoryHolder;

        }
        if (viewType == COLLECTION) {
            RowLixiMainThreeImageLayoutBinding mainThreeImageLayoutBinding =
                    RowLixiMainThreeImageLayoutBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);
            collectionHolder = new CollectionHolder(mainThreeImageLayoutBinding.getRoot());
            return collectionHolder;

        }

        if (viewType == FLASH_SALE) {
            RowLixiMainFlashSaleBinding lixiMainFlashSaleBinding =
                    RowLixiMainFlashSaleBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            flashSaleHolder = new FlashSaleHolder(lixiMainFlashSaleBinding.getRoot());

            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            flashSaleHolder.recListFlashSale.setLayoutManager(layoutManager);

            flashSaleHolder.txtShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, FlashSaleDetailActivity.class);
                    activity.startActivity(intent);
                }
            });

            flashSaleHolder.timerView.addOnFinishListener(new OnFinishTimerListener() {
                @Override
                public void onFinish() {
                    if (itemClickListener != null) {
                        itemClickListener.onItemCLick(-1, "End flash sale", null, 0L);
                    }
                }
            });


            if ((listFlashSale == null || listFlashSale.size() == 0) && flashSaleDataSeted) {
                flashSaleHolder.constraintParent.setVisibility(View.GONE);
            } else {
                flashSaleHolder.constraintParent.setVisibility(View.VISIBLE);
                flashSaleAdapter = new FlashSaleHomeAdapter(activity, listFlashSale, isHappeningFS);
                flashSaleHolder.recListFlashSale.setAdapter(flashSaleAdapter);

                flashSaleAdapter.onItemClickListener(new ItemClickListener() {
                    @Override
                    public void onItemCLick(int pos, String action, Object object, Long count) {
//                        Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                        intent.putExtra(PRODUCT_ID, (long) object);
//                        intent.putExtra(CashEvoucherDetailActivity.CURRENT_TIME, flashSaleHolder.timerView.getUntilFinishMillis());
//                        activity.startActivity(intent);
                        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, (Long) object, TrackingConstant.ContentSource.HOME);

                    }
                });
                flashSaleHolder.timerView.startTimer(currentTimeFS);
            }
            return flashSaleHolder;
        }

        if (viewType == TOPVOUCHER) {
            RowLixiMainTopVoucherLayoutBinding rowLixiMainTopVoucherLayoutBinding =
                    RowLixiMainTopVoucherLayoutBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);
            topVoucherHolder = new TopVoucherHolder(rowLixiMainTopVoucherLayoutBinding.getRoot());

//            topVoucherHolder.rvTopVoucher.setHasFixedSize(true);
//            topVoucherHolder.rvTopVoucher.setNestedScrollingEnabled(false);
            topVoucherHolder.rvTopVoucher.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            adapterVoucherTopHot = new AdapterVoucherTopHot(activity, list_voucher_top_hot);
            topVoucherHolder.rvTopVoucher.setAdapter(adapterVoucherTopHot);

            return topVoucherHolder;

        }

        if (viewType == VOUCHER_INDICATOR) {
            RowLixiMainVoucherIndicatorLayoutBinding rowLixiMainVoucherIndicatorLayoutBinding =
                    RowLixiMainVoucherIndicatorLayoutBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);
            voucherIndicatorHolder = new VoucherIndicatorHolder(
                    rowLixiMainVoucherIndicatorLayoutBinding.getRoot());

            voucherIndicatorHolder.lnHighLightVoucher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    isTabSelected(0);

                    if (eventListener != null)
                        eventListener.onSelectHighlightVoucher();
                }
            });

            voucherIndicatorHolder.lnNearbyVoucher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    isTabSelected(1);

                    if (eventListener != null)
                        eventListener.onSelectNearbyVoucher();
                }
            });

            voucherIndicatorHolder.lnGoodPriceVoucher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    isTabSelected(2);

                    if (eventListener != null)
                        eventListener.onSelectGoodpriceVoucher();
                }
            });

            return voucherIndicatorHolder;
        }

        if (viewType == VOUCHER) {
            RowLixiMainVoucherLayoutBinding rowLixiMainVoucherLayoutBinding =
                    RowLixiMainVoucherLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            rowLixiMainVoucherLayoutBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (pos < list_voucher.size()) {
                        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list_voucher.get(pos).getId(), TrackingConstant.ContentSource.HOME);
                    }
                }
            });
            return new VoucherHolder(rowLixiMainVoucherLayoutBinding.getRoot());
        }

        if (viewType == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherFsHolder = VoucherFlashSaleHolder.newHolder(activity, parent);
            return voucherFsHolder;
        }
        return null;
    }
    //endregion

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (position == BANNER) {
            bannerHolder = (BannerHolder) holder;
        }
        if (position == CATEGORY) {
            categoryHolder = (CategoryHolder) holder;
            if (list_category == null || list_category.size() < 5)
                categoryHolder.rl_root.setVisibility(View.GONE);
            else
                categoryHolder.rl_root.setVisibility(View.VISIBLE);
        }
        if (position == COLLECTION) {
            collectionHolder = (CollectionHolder) holder;
            if (list_collection != null) {
                if (list_collection.size() >= 3) {
                    if (list_collection.size() > 0) {
                        collectionHolder.ll_root.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(list_collection.get(0).getBanner(), collectionHolder.iv_1, LixiApplication.getInstance().optionsNomal);
                    } else {
                        collectionHolder.ll_root.setVisibility(View.GONE);
                    }
                    if (list_collection.size() > 1)
                        ImageLoader.getInstance().displayImage(list_collection.get(1).getBanner(), collectionHolder.iv_2, LixiApplication.getInstance().optionsNomal);
                    if (list_collection.size() > 2) {
                        collectionHolder.iv_3.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(list_collection.get(2).getBanner(), collectionHolder.iv_3, LixiApplication.getInstance().optionsNomal);
                    } else collectionHolder.iv_3.setVisibility(View.GONE);
                } else {
                    collectionHolder.ll_root.setVisibility(View.GONE);
                }
            } else {
                collectionHolder.ll_root.setVisibility(View.GONE);
            }
        }

        if (position == FLASH_SALE) {
            flashSaleHolder = (FlashSaleHolder) holder;
        }

        if (position == TOPVOUCHER) {
            topVoucherHolder = (TopVoucherHolder) holder;
            if (hasFlashSale) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) topVoucherHolder.rvTopVoucher.getLayoutParams();
                layoutParams.height = activity.getResources().getDimensionPixelSize(R.dimen.value_196);
                topVoucherHolder.rvTopVoucher.setLayoutParams(layoutParams);
            }
        }

        if (position == VOUCHER_INDICATOR) {
            final VoucherIndicatorHolder voucherIndicatorHolder = (VoucherIndicatorHolder) holder;
            isTabSelected(currentTab);
        }

        if (getItemViewType(position) == VOUCHER) {
            VoucherHolder voucherHolder = (VoucherHolder) holder;
            if (list_voucher.size() <= position - VOUCHER)
                return;
            LixiShopEvoucherModel voucher = list_voucher.get(position - VOUCHER);

            if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0) {
//                ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
                LixiImage.with(activity)
                        .load(voucher.getProduct_cover_image())
                        .size(ImageSize.SMALL)
                        .into(voucherHolder.imgLogoVoucher);
            } else {
                if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0) {
                    ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
                }
            }
            voucherHolder.tvVoucherName.setText(voucher.getName());

            voucherHolder.ll_cash_info.setVisibility(View.VISIBLE);
            voucherHolder.tvVoucherPrice.setText(Helper.getVNCurrency(voucher.getPayment_discount_price()) + "đ");
            if (voucher.getPayment_discount_price() != voucher.getPayment_price()) {
                voucherHolder.tvOldPrice.setVisibility(View.VISIBLE);
                voucherHolder.tvOldPrice.setText(Helper.getVNCurrency(voucher.getPayment_price()) + "đ");
                voucherHolder.tvOldPrice.setPaintFlags(voucherHolder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else voucherHolder.tvOldPrice.setVisibility(View.GONE);
            if (voucher.getCashback_price() > 0) {
                voucherHolder.tvLixiBonus.setVisibility(View.VISIBLE);
                voucherHolder.tvLixiBonus.setText("+" + Helper.getVNCurrency(voucher.getCashback_price()) + " Lixi");
            } else voucherHolder.tvLixiBonus.setVisibility(View.GONE);
            voucherHolder.tvMerchantInfo.setText(voucher.getMerchant_name() + " | " + voucher.getCount_store() + " cửa hàng");
            voucherHolder.tvCountAttend.setText(voucher.getQuantity_order_done() + " " + LanguageBinding.getString(R.string.quantity_bought_customer, activity));

            if (voucher.getPayment_price() != voucher.getPayment_discount_price()) {
                voucherHolder.tv_percent_discount.setVisibility(View.VISIBLE);
                int percent = 100 - (int) (voucher.getPayment_discount_price() * 100 / voucher.getPayment_price());
                voucherHolder.tv_percent_discount.setText("-" + percent + "%");
            } else voucherHolder.tv_percent_discount.setVisibility(View.GONE);

            if (currentTab == 1) {
                // gần bạn
                voucherHolder.tvCountAttend.setVisibility(View.GONE);
                // có thể null
                if (voucher.getStore_info() != null) {
                    voucherHolder.tvMerchantInfo.setText(voucher.getStore_info().getAddress());
                }
                voucherHolder.tvDistance.setVisibility(View.VISIBLE);
                if (voucher.getUser_distance() >= 1) {
                    voucherHolder.tvDistance.setText("(" + String.format("%.02f", voucher.getUser_distance()) + "km)");
                } else {
                    voucherHolder.tvDistance.setText("(" + (int) (voucher.getUser_distance() * 1000) + "m)");
                }
            } else {
                // còn lại
                voucherHolder.tvDistance.setVisibility(View.GONE);
                voucherHolder.tvCountAttend.setVisibility(View.VISIBLE);
            }

            voucherHolder.itemView.setTag(position - VOUCHER);
        }


        if (getItemViewType(position) == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherHolder = (VoucherFlashSaleHolder) holder;
            if (list_voucher.size() <= position - VOUCHER)
                return;
            LixiShopEvoucherModel voucher = list_voucher.get(position - VOUCHER);

            VoucherFlashSaleHolder.bindViewHolder(voucherHolder, voucher);

        }
    }

    //region interface CHANGE DATA
    public void setList_banner(ArrayList<BannerModel> list_banner) {
        this.list_banner = list_banner;
        if (bannerAdapter != null) {
            bannerAdapter.setItem(list_banner);
            notifyDataSetChanged();
        }
        if (bannerHolder != null) {
            bannerHolder.indicator.setViewPager(bannerHolder.view_pager);
            bannerHolder.indicator.requestLayout();
        }
    }

    public void setList_category(ArrayList<ProductCategoryModel> list_category) {
        this.list_category = list_category;
        if (this.list_category != null)
            while (this.list_category.size() > 5)
                this.list_category.remove(5);
        if (categoryAdapter != null)
            categoryAdapter.setList_category(list_category);
    }

    public void setList_collection(ArrayList<ProductCollectionModel> list_collection) {
        this.list_collection = list_collection;
        notifyDataSetChanged();
    }

    public void setList_flash_sale(ArrayList<FlashSaleNow.ProductsBean> listFlashSale, long currentTime, boolean isHappening) {
        this.listFlashSale = listFlashSale;
        currentTimeFS = currentTime;
        isHappeningFS = isHappening;
        flashSaleDataSeted = true;
        if (flashSaleHolder == null)
            return;
        if (listFlashSale == null || listFlashSale.size() == 0) {
            flashSaleHolder.constraintParent.setVisibility(View.GONE);
        } else {
            flashSaleHolder.constraintParent.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            flashSaleHolder.recListFlashSale.setLayoutManager(layoutManager);
            flashSaleAdapter = new FlashSaleHomeAdapter(activity, listFlashSale, isHappening);
            flashSaleHolder.recListFlashSale.setAdapter(flashSaleAdapter);

            flashSaleAdapter.onItemClickListener(new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, (Long) object, TrackingConstant.ContentSource.HOME);
                }
            });
            flashSaleHolder.timerView.startTimer(currentTime);
            notifyDataSetChanged();
        }
    }

    public void setList_voucher_tophot(ArrayList<LixiShopEvoucherModel> list_voucher_tophot) {
        list_voucher_top_hot = list_voucher_tophot;
        if (adapterVoucherTopHot != null)
            adapterVoucherTopHot.setList_voucher(list_voucher_tophot);

        hasFlashSale = false;
        if (list_voucher_top_hot != null) {
            for (int i = 0; i < list_voucher_top_hot.size(); i++) {
                if (list_voucher_top_hot.get(i).getFlash_sale_id() > 0) {
                    hasFlashSale = true;
                    break;
                }
            }
        }
    }

    public void setList_voucher(ArrayList<LixiShopEvoucherModel> list_voucher) {
        if (this.list_voucher == null)
            this.list_voucher = list_voucher;
        else
            this.list_voucher.addAll(list_voucher);
        notifyDataSetChanged();
    }

    public int getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(int currentTab) {
        this.currentTab = currentTab;
        if (voucherIndicatorHolder != null)
            isTabSelected(currentTab);
    }

    //endregion

    //region HOLDER
    class BannerHolder extends RecyclerView.ViewHolder {
        private AutoScrollViewPager view_pager;
        private CirclePageIndicator indicator;

        public BannerHolder(View itemView) {
            super(itemView);
            view_pager = itemView.findViewById(R.id.view_pager);
            indicator = itemView.findViewById(R.id.indicator);

        }
    }

    class CategoryHolder extends RecyclerView.ViewHolder {
        private LinearLayout rl_root;
        private RecyclerView rvCategory;

        public CategoryHolder(View itemView) {
            super(itemView);
            rl_root = itemView.findViewById(R.id.rl_root);
            rvCategory = itemView.findViewById(R.id.rvCategory);
            rvCategory.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    class CollectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        private LinearLayout ll_root;
        private ImageView iv_1;
        private ImageView iv_2;
        private ImageView iv_3;

        public CollectionHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            ll_root = itemView.findViewById(R.id.ll_root);
            iv_1 = itemView.findViewById(R.id.iv_1);
            iv_2 = itemView.findViewById(R.id.iv_2);
            iv_3 = itemView.findViewById(R.id.iv_3);

            iv_1.setOnClickListener(this);
            iv_2.setOnClickListener(this);
            iv_3.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ProductCollectionModel collection;
            switch (v.getId()) {
                case R.id.iv_1:
                    if (list_collection.size() > 0) {
                        collection = list_collection.get(0);
                        ActivityCollectionListProduct.startActivityCollectionListProduct(activity, collection.getId(), collection.getName(), collection.getBanner());
                    }
                    break;
                case R.id.iv_2:

                    if (list_collection.size() > 1) {
                        collection = list_collection.get(1);
                        ActivityCollectionListProduct.startActivityCollectionListProduct(activity, collection.getId(), collection.getName(), collection.getBanner());
                    }
                    break;
                case R.id.iv_3:
                    if (list_collection.size() > 2) {
                        collection = list_collection.get(2);
                        ActivityCollectionListProduct.startActivityCollectionListProduct(activity, collection.getId(), collection.getName(), collection.getBanner());
                    }
                    break;
            }
        }
    }

    class FlashSaleHolder extends RecyclerView.ViewHolder {
        TimerView timerView;
        TextView txtShowAll;
        RecyclerView recListFlashSale;

        ConstraintLayout constraintParent;

        public FlashSaleHolder(View itemView) {
            super(itemView);
            timerView = (TimerView) itemView.findViewById(R.id.timerView);
            txtShowAll = (TextView) itemView.findViewById(R.id.txtShowAll);
            recListFlashSale = (RecyclerView) itemView.findViewById(R.id.recListFlashSale);
            constraintParent = (ConstraintLayout) itemView.findViewById(R.id.constraintParent);
        }
    }

    class TopVoucherHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvTopVoucher;

        public TopVoucherHolder(View itemView) {
            super(itemView);
            rvTopVoucher = (RecyclerView) itemView.findViewById(R.id.rvTopVoucher);

        }
    }

    class VoucherIndicatorHolder extends RecyclerView.ViewHolder {
        private LinearLayout lnHighLightVoucher;
        private TextView tvHighLightVoucher;
        private View vHighLightVoucher;
        private LinearLayout lnNearbyVoucher;
        private TextView tvNearbyVoucher;
        private View vNearbyVoucher;
        private LinearLayout lnGoodPriceVoucher;
        private TextView tvGoodPriceVoucher;
        private View vGoodPriceVoucher;

        public VoucherIndicatorHolder(View itemView) {
            super(itemView);
            lnHighLightVoucher = (LinearLayout) itemView.findViewById(R.id.lnHighLightVoucher);
            tvHighLightVoucher = (TextView) itemView.findViewById(R.id.tvHighLightVoucher);
            vHighLightVoucher = (View) itemView.findViewById(R.id.vHighLightVoucher);
            lnNearbyVoucher = (LinearLayout) itemView.findViewById(R.id.lnNearbyVoucher);
            tvNearbyVoucher = (TextView) itemView.findViewById(R.id.tvNearbyVoucher);
            vNearbyVoucher = (View) itemView.findViewById(R.id.vNearbyVoucher);
            lnGoodPriceVoucher = (LinearLayout) itemView.findViewById(R.id.lnGoodPriceVoucher);
            tvGoodPriceVoucher = (TextView) itemView.findViewById(R.id.tvGoodPriceVoucher);
            vGoodPriceVoucher = (View) itemView.findViewById(R.id.vGoodPriceVoucher);
        }
    }

    class VoucherHolder extends RecyclerView.ViewHolder {
        private View rootView;
        private View v_line;
        private ImageView imgLogoVoucher;
        private TextView tv_percent_discount;
        private TextView tvVoucherName;
        private TextView tvVoucherPrice;
        private TextView tvOldPrice;
        private TextView tvLixiBonus;
        private TextView tvMerchantInfo;
        private TextView tvCountAttend;
        private TextView tvDistance;

        private LinearLayout ll_cash_info;

        public VoucherHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            v_line = itemView.findViewById(R.id.v_line);
            imgLogoVoucher = (ImageView) itemView.findViewById(R.id.imgLogoVoucher);
            tv_percent_discount = (TextView) itemView.findViewById(R.id.tv_percent_discount);
            tvVoucherName = (TextView) itemView.findViewById(R.id.tvVoucherName);

            ll_cash_info = (LinearLayout) itemView.findViewById(R.id.ll_cash_info);
            tvVoucherPrice = (TextView) itemView.findViewById(R.id.tvVoucherPrice);
            tvOldPrice = (TextView) itemView.findViewById(R.id.tvOldPrice);
            tvLixiBonus = (TextView) itemView.findViewById(R.id.tvLixiBonus);
            tvMerchantInfo = (TextView) itemView.findViewById(R.id.tvMerchantInfo);
            tvCountAttend = (TextView) itemView.findViewById(R.id.tvCountAttend);

            tvDistance = (TextView) itemView.findViewById(R.id.tvDistance);
        }

    }

    //endregion

    //region HANDLE ACTION
    public void isTabSelected(int pos) {
        switch (pos) {
            case 0:
                voucherIndicatorHolder.tvHighLightVoucher.setTextColor(activity.getResources().getColor(R.color.timmongmo));
                voucherIndicatorHolder.tvNearbyVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                voucherIndicatorHolder.tvGoodPriceVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));

                voucherIndicatorHolder.vHighLightVoucher.setVisibility(View.VISIBLE);
                voucherIndicatorHolder.vNearbyVoucher.setVisibility(View.GONE);
                voucherIndicatorHolder.vGoodPriceVoucher.setVisibility(View.GONE);
                break;

            case 1:
                voucherIndicatorHolder.tvHighLightVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                voucherIndicatorHolder.tvNearbyVoucher.setTextColor(activity.getResources().getColor(R.color.timmongmo));
                voucherIndicatorHolder.tvGoodPriceVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));

                voucherIndicatorHolder.vHighLightVoucher.setVisibility(View.GONE);
                voucherIndicatorHolder.vNearbyVoucher.setVisibility(View.VISIBLE);
                voucherIndicatorHolder.vGoodPriceVoucher.setVisibility(View.GONE);
                break;

            case 2:
                voucherIndicatorHolder.tvHighLightVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                voucherIndicatorHolder.tvNearbyVoucher.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                voucherIndicatorHolder.tvGoodPriceVoucher.setTextColor(activity.getResources().getColor(R.color.timmongmo));

                voucherIndicatorHolder.vHighLightVoucher.setVisibility(View.GONE);
                voucherIndicatorHolder.vNearbyVoucher.setVisibility(View.GONE);
                voucherIndicatorHolder.vGoodPriceVoucher.setVisibility(View.VISIBLE);
                break;

        }

    }

    //endregion

    //region EVENT CALLBACK
    public interface ILixiHomeAdapterListener {
        void onSelectHighlightVoucher();

        void onSelectNearbyVoucher();

        void onSelectGoodpriceVoucher();
    }

    public void setEventListener(ILixiHomeAdapterListener eventListener) {
        this.eventListener = eventListener;
    }

    private ILixiHomeAdapterListener eventListener;

    //endregion
}
