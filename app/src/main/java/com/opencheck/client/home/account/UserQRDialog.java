package com.opencheck.client.home.account;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.UserQrCodeDialogBinding;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class UserQRDialog extends LixiDialog {
    public UserQRDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ImageView iv_qr, img_close;
    private TextView tv_code;
    private TextView tv_title;
    private RelativeLayout rl_dismiss;
//    private String title = "Mã giao dịch";
    private String code;
    private UserQrCodeDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = UserQrCodeDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }


    private void findViews() {
        iv_qr = (ImageView) findViewById(R.id.iv_qr);
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_title = (TextView) findViewById(R.id.tv_title);
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        img_close = (ImageView) findViewById(R.id.img_close);
        img_close.setOnClickListener(this);
        rl_dismiss.setOnClickListener(this);
        showBarcode();

    }

    public void initDialogEvent() {

    }


    @Override

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_dismiss:
                dismiss();
                break;
            case R.id.img_close:
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface IDidalogEvent {

    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;

    }

    private void showBarcode() {

        UserModel userModel = UserModel.getInstance();

        // currentModel = userModel;

        new GeneralQRCodeTask(activity, userModel.getCode(), iv_qr, new ItemClickListener() {

            @Override

            public void onItemCLick(int pos, String action, Object object, Long count) {

                if (pos == 1) {
                    iv_qr.setImageBitmap((Bitmap) object);
                    // iv_evoucher.setImageBitmap((Bitmap) object);
                }

            }

        });
        tv_code.setText(userModel.getPhone());

    }
}
