package com.opencheck.client.home.account;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ChangeAvatarDialogBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/22/2018.
 */


public class ChangeAvatarDialog extends LixiDialog {

    private LinearLayout lnRoot;
    private TextView tvTitle;
    private Button btnNewImage;
    private Button btnSelect;
    private Button btnCancel;

    public ChangeAvatarDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ChangeAvatarDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ChangeAvatarDialogBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        tvTitle.setText(LanguageBinding.getString(R.string.avatar, activity));
    }

    private void findViews() {
        lnRoot = findViewById(R.id.lnRoot);
        tvTitle = findViewById(R.id.tvTitle);
        btnNewImage = findViewById(R.id.btnNewImage);
        btnSelect = findViewById(R.id.btnSelect);
        btnCancel = findViewById(R.id.btnCancel);

        btnNewImage.setOnClickListener(this);
        btnSelect.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNewImage:
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR);
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, ConstantValue.REQUEST_CODE_CAMERA_CHANGING_AVATAR);
                    }
                } else {
                    if (activity instanceof HomeActivity)
                        ((HomeActivity) activity).captureNewImage();
                }
                dismiss();
                break;
            case R.id.btnSelect:
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ConstantValue.REQUEST_CODE_WRITE_EXTERNAL_STORAGE_CHANGING_AVATAR);
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ConstantValue.REQUEST_CODE_WRITE_EXTERNAL_STORAGE_CHANGING_AVATAR);
                    }
                } else {
                    activity.startActivity(new Intent(activity, SelectAvatarActivity.class));
                }
                dismiss();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

}
