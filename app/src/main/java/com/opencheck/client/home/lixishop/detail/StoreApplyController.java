package com.opencheck.client.home.lixishop.detail;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ApplyStoreLayoutBinding;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class StoreApplyController {

    private Activity activity;
    private View viewChild;
    private LinearLayout lnRoot;
    private int position = 0;
    private int size = 0;

    private TextView tv_number;
    private TextView tv_name;
    private TextView tv_address;
    private TextView tv_phone;
    private View v_line;

    private StoreApplyModel storeModel;

    public StoreApplyController(Activity _activity, StoreApplyModel storeModel, int _position, LinearLayout _lnRoot, int size) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.storeModel = storeModel;
        this.size = size;
        ApplyStoreLayoutBinding mBinding = ApplyStoreLayoutBinding.inflate(
                LayoutInflater.from(activity), null, false);
        this.viewChild = mBinding.getRoot();

        this.initView();
        this.initData();

        lnRoot.addView(viewChild);
    }

    private void initView() {
        tv_number = (TextView) viewChild.findViewById(R.id.tv_number);
        tv_name = (TextView) viewChild.findViewById(R.id.tv_name);
        tv_address = (TextView) viewChild.findViewById(R.id.tv_address);
        tv_phone = (TextView) viewChild.findViewById(R.id.ed_phone);
        v_line = viewChild.findViewById(R.id.v_line);
    }

    private void initData() {
        if (position == size - 1) {
            v_line.setVisibility(View.GONE);
        }

        tv_number.setText((position + 1) + ".");
        tv_name.setText(storeModel.getName());
        tv_address.setText(storeModel.getAddress());
        tv_phone.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), storeModel.getPhone()));
    }

}
