package com.opencheck.client.home.merchant.map;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public interface OnDataPass {
    public void onDataPass(double curLat, double curLng);
}
