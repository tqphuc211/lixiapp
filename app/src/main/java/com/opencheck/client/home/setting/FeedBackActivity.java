package com.opencheck.client.home.setting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FeedbackActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;

import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class FeedBackActivity extends LixiActivity {

    private RelativeLayout rlTop;
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private RelativeLayout rlLine;
    private LinearLayout lnEmail;
    private EditText edEmail;
    private LinearLayout lnPhone;
    private EditText edPhone;
    private TextView btnSend;
    private EditText edContent;
    private RelativeLayout rlMessenger;

    private FeedbackActivityBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,  R.layout.feedback_activity);
        this.initView();
    }


    private void initView() {
        rlTop = findViewById(R.id.rlTop);
        rlBack = findViewById(R.id.rlBack);
        tvTitle = findViewById(R.id.tvTitle);
        rlLine = findViewById(R.id.rlLine);
        lnEmail = findViewById(R.id.lnEmail);
        edEmail = findViewById(R.id.edEmail);
        lnPhone = findViewById(R.id.lnPhone);
        edPhone = findViewById(R.id.edPhone);
        btnSend = findViewById(R.id.btnSend);
        edContent = findViewById(R.id.edContent);
        rlMessenger = findViewById(R.id.rlMessenger);
        this.initData();
    }

    private void initData() {
        UserModel userModel = UserModel.getInstance();

        rlBack.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        if (userModel != null) {
            edEmail.setText(userModel.getEmail());
            edPhone.setText(userModel.getPhone());
        }
        rlMessenger.setOnClickListener(this);
    }

    private void sendFeedback() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", edEmail.getText().toString());
            jsonObject.put("phone", edPhone.getText().toString());
            jsonObject.put("content", edContent.getText().toString());

            DataLoader.sendFeedback(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        Helper.showErrorDialog(activity, "Gửi yêu cầu, góp ý thành công");
                        activity.onBackPressed();
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                    }
                }
            }, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean checkInput() {
        if (edPhone.getText().toString().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, "Vui lòng nhập số điện thoại");
            return false;
        } else {
            if (!Helper.isValidPhone(edPhone.getText().toString())) {
                Helper.showErrorDialog(activity, "Vui lòng nhập số điện thoại đúng cấu trúc");
                return false;
            }
        }

        if (edEmail.getText().toString().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, "Vui lòng nhập email");
            return false;
        } else {
            if (!Helper.isValidEmail(edEmail.getText().toString())) {
                Helper.showErrorDialog(activity, "Vui lòng nhập email đúng cấu trúc");
                return false;
            }
        }

        if (edContent.getText().toString().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, "Vui lòng nhập nội dung góp ý");
            return false;
        }

        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            activity.onBackPressed();
        } else if (v == btnSend) {
            if (checkInput()) {
                sendFeedback();
            }
        } else if (v == rlMessenger) {
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
            }
            catch (Exception e) {
                Toast.makeText(activity, "Có lỗi, mong bạn gửi email", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
