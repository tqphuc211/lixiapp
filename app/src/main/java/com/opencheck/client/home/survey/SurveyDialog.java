package com.opencheck.client.home.survey;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.merchant.survey.SurveyAnswerResultModel;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;
import com.opencheck.client.models.merchant.survey.SurveyUserAnswerModel;
import com.opencheck.client.utils.Helper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SurveyDialog extends LixiDialog {

    public SurveyDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private SurveyDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = SurveyDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());

        findViews();
        initDialogEvent();
    }

    public final String QUEST_FREE = "free";
    public final String QUEST_CHOICE = "choice";

    private LinearLayout ll_root_popup_menu;

    protected LinearLayout ll_survey;
    private ImageView iv_logo;
    private LinearLayout ll_indicator;
    private TextView tv_count;
    private RelativeLayout rl_quest;
    private TextView tv_next;

    private SurveyModel surveyModel;
    private List<SurveyIndicatorController> listIndicator;
    private int crQuestion = 0;
    private QuestionAnswerController crQuestionController;

    private void findViews() {
        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        ll_survey = (LinearLayout) findViewById(R.id.ll_survey);
        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        ll_indicator = (LinearLayout) findViewById(R.id.ll_indicator);
        tv_count = (TextView) findViewById(R.id.tv_count);
        rl_quest = (RelativeLayout) findViewById(R.id.rl_quest);
        tv_next = (TextView) findViewById(R.id.tv_next);
    }

    public void initDialogEvent() {
        tv_next.setOnClickListener(this);
    }

    public void setData(SurveyModel surveyModel) {
        this.surveyModel = surveyModel;
        ImageLoader.getInstance().displayImage(surveyModel.getMerchant_logo(), iv_logo, LixiApplication.getInstance().optionsNomal);

        listIndicator = new ArrayList<SurveyIndicatorController>();
        for (int i = 0; i < surveyModel.getQuestions().size(); i++) {
            listIndicator.add(new SurveyIndicatorController(activity, ll_indicator, surveyModel.getQuestions().size()));
        }

        showQuest(surveyModel.getCurrent_question_index());
    }

    public void showQuest(int pos) {
        tv_count.setText("Khảo sát " + (pos + 1));
        SurveyQuestionModel questModel = getSurveyModel().getQuestions().get(pos);
        rl_quest.removeAllViews();

        for (int i = 0; i <= pos; i++) {
            listIndicator.get(i).setSelected(true);
        }

        switch (questModel.getQuestion_type()) {
            case QUEST_FREE:
                crQuestionController = new QuestionFreeController(activity, rl_quest, questModel, this);
                break;
            case QUEST_CHOICE:
                if (questModel.isShow_icon())
                    crQuestionController = new QuestionChoiceIconController(activity, rl_quest, questModel);
                else
                    crQuestionController = new QuestionChoiceController(activity, rl_quest, questModel);
                break;
        }

    }

    private void answerSurvey(long aswId, String aswVal, long questId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("answer_id", aswId);
            jsonObject.put("answer_value", aswVal);
            jsonObject.put("question_id", questId);
            jsonObject.put("survey_id", getSurveyModel().getId());
            tv_next.setOnClickListener(null);

            DataLoader.userAnswerSurvey(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    SurveyAnswerResultModel rs = (SurveyAnswerResultModel) object;
                    tv_next.setOnClickListener(SurveyDialog.this);
                    if (isSuccess) {
                        if (rs.is_done()) {
                            SurveyInviteDialog dialog = new SurveyInviteDialog(activity,false, true, false);
                            dialog.show();
                            dialog.setData(rs);
                            dismiss();
                        } else {
                            showQuest(rs.getNext_question_index());
                        }
                    } else {
                        Helper.showErrorDialog(activity, object.toString());
                    }
                }
            }, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
            tv_next.setOnClickListener(SurveyDialog.this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_next:
                SurveyUserAnswerModel asw = crQuestionController.getAnswer();
                if (asw == null) {
                    Toast.makeText(activity, "Vui lòng chọn câu trả lời", Toast.LENGTH_SHORT).show();
                    return;
                }
                answerSurvey(asw.getAnswer_id(), asw.getAnswer_value(), asw.getQuestion_id());
//
//                showQuest((surveyModel.getCurrent_question_index() + 1) % surveyModel.getQuestions().size());
//                surveyModel.setCurrent_question_index((surveyModel.getCurrent_question_index() + 1) % surveyModel.getQuestions().size());
                break;
            default:
                break;
        }
    }

    public void handleKeyboard(boolean isShow) {
        if (isShow) {
            if (crQuestionController instanceof QuestionFreeController) {
                ll_survey.setVisibility(View.GONE);
                tv_next.setVisibility(View.GONE);
            } else {
                ll_survey.setVisibility(View.VISIBLE);
                tv_next.setVisibility(View.VISIBLE);
            }
        } else {
            ll_survey.setVisibility(View.VISIBLE);
            tv_next.setVisibility(View.VISIBLE);
        }
    }

    public void onHideKeyBoard() {
        ll_survey.setVisibility(View.VISIBLE);
        tv_next.setVisibility(View.VISIBLE);
    }

    public void onShowKeyBoard() {
        ll_survey.setVisibility(View.GONE);
        tv_next.setVisibility(View.GONE);
    }

    public SurveyModel getSurveyModel() {
        return surveyModel;
    }

    public interface IDidalogEvent {
        public void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
