package com.opencheck.client.home.delivery.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivitySearchLocationBinding;
import com.opencheck.client.databinding.DialogRequireLoginBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.LocationAdapter;
import com.opencheck.client.home.delivery.controller.HistoryLocationController;
import com.opencheck.client.home.delivery.database.SQLiteDatabaseHandler;
import com.opencheck.client.home.delivery.dialog.EditOptionDialog;
import com.opencheck.client.home.delivery.enum_key.TypeLocation;
import com.opencheck.client.home.delivery.fragments.DeliveryHomeFragment;
import com.opencheck.client.home.delivery.interfaces.OnEdittingSelected;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.viewmodel.EdittingViewModel;
import com.opencheck.client.home.delivery.viewmodel.HomeLocationViewModel;
import com.opencheck.client.home.delivery.viewmodel.OfficeLocationViewModel;
import com.opencheck.client.home.merchant.map.LoadLocationAddress;
import com.opencheck.client.home.merchant.map.LocationCallBack;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LocationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class NewSearchLocationActivity extends LixiActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationHelper.LocationCallBack {

    private ImageView iv_back, ivMap;
    private EditText et_address;
    private RelativeLayout rl_delete;
    private RecyclerView rv_location;
    private LinearLayout lnNoResult, lnHistoryLocation;
    private ProgressBar progressBar;
    private NestedScrollView nestedScrollView;
    private LocationAdapter adapter;
    private GoogleApiClient mGoogleApiClient;

    // Location
    private LinearLayout linearLocation;
    private ConstraintLayout constraintPrivateHome, constraintOffice, constraintSaveLocation, constraintGpsLocation, constraintCurrentLocation;
    private TextView txtLocationHome, txtLocationOffice, tvCurrentLocation, tvFullCurrentLocation,
            tvSelectLocation, tvGpsLocation, tvFullGpsLocation, tvRequestPermission;
    private ImageView imgHomeOption, imgOfficeOption;

    private LatLngBounds latLngBounds = new LatLngBounds(new LatLng(10.349370, 106.363878), new LatLng(11.160214, 107.026577));
    private LatLngBounds mHaNoiBounds = new LatLngBounds(new LatLng(20.945, 105.742), new LatLng(21.098, 105.911));
    private AutocompleteFilter filter = new AutocompleteFilter.Builder()
            .setCountry("VN")
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE)
            .build();

    private boolean isLastLocation = false;
    private boolean isEditting = false;
    private HomeLocationViewModel homeLocationViewModel;
    private OfficeLocationViewModel officeLocationViewModel;
    private EdittingViewModel edittingViewModel;

    private final int REQUEST_MAP = 9865;
    private LocationHelper locationHelper;
    private ActivitySearchLocationBinding mBinding;

    public static void startSearchLocationActivity(LixiActivity activity, boolean isLastLocation) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("LAST_LOCATION", isLastLocation);
        Intent intent = new Intent(activity, NewSearchLocationActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideSoftKeyboard();
        super.onCreate(savedInstanceState);
        locationHelper = new LocationHelper(this);
        buildGoogleApiClient();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_location);
        isLastLocation = getIntent().getExtras().getBoolean("LAST_LOCATION", false);
        initViewModel();
        initViews();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.LOCATION_PICKER;
    }

    private void initViews() {
        iv_back = findViewById(R.id.iv_back);
        et_address = findViewById(R.id.et_address);
        rv_location = findViewById(R.id.rv_location);
        rl_delete = findViewById(R.id.rl_delete);
        ivMap = findViewById(R.id.ivMap);
        lnNoResult = findViewById(R.id.lnNoResult);
        progressBar = findViewById(R.id.progressBar);

        linearLocation = (LinearLayout) findViewById(R.id.linearLocation);
        lnHistoryLocation = (LinearLayout) findViewById(R.id.lnHistoryLocation);

        constraintPrivateHome = (ConstraintLayout) findViewById(R.id.constraintPrivateHome);
        constraintOffice = (ConstraintLayout) findViewById(R.id.constraintOffice);
        constraintSaveLocation = (ConstraintLayout) findViewById(R.id.constraintSaveLocation);
        constraintGpsLocation = (ConstraintLayout) findViewById(R.id.constraintGpsLocation);
        constraintCurrentLocation = (ConstraintLayout) findViewById(R.id.constraintCurrentLocation);

        txtLocationHome = (TextView) findViewById(R.id.txtLocationHome);
        txtLocationOffice = (TextView) findViewById(R.id.txtLocationOffice);
        tvCurrentLocation = (TextView) findViewById(R.id.tvCurrentLocation);
        tvFullCurrentLocation = (TextView) findViewById(R.id.tvFullCurrentLocation);
        tvSelectLocation = (TextView) findViewById(R.id.tvSelectLocation);
        tvGpsLocation = (TextView) findViewById(R.id.tvGpsLocation);
        tvFullGpsLocation = (TextView) findViewById(R.id.tvFullGpsLocation);
        tvRequestPermission = (TextView) findViewById(R.id.tvRequestPermission);

        imgHomeOption = (ImageView) findViewById(R.id.imgHomeOption);
        imgOfficeOption = (ImageView) findViewById(R.id.imgOfficeOption);
        nestedScrollView = findViewById(R.id.nestedScrollView);

        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int i, int i1, int i2) {
                if (s.toString().equals("")) {
                    nestedScrollView.setVisibility(View.VISIBLE);
                } else {
                    nestedScrollView.setVisibility(View.GONE);
                }
            }

            private Timer timer = new Timer();
            private final long DELAY = 500;

            @Override
            public void afterTextChanged(final Editable e) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mGoogleApiClient.isConnected()) {
                                            if (e.toString().equals("")) {
                                                adapter.clearData();
                                                rl_delete.setVisibility(View.GONE);
                                                et_address.clearFocus();
                                                hideSoftKeyboard();

                                            } else {
                                                rl_delete.setVisibility(View.VISIBLE);
                                            }
                                            adapter.getFilter().filter(e.toString());

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Disconnected", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }, DELAY
                );

            }
        });

        iv_back.setOnClickListener(this);
        rl_delete.setOnClickListener(this);
        ivMap.setOnClickListener(this);
        imgHomeOption.setOnClickListener(this);
        imgOfficeOption.setOnClickListener(this);
        constraintPrivateHome.setOnClickListener(this);
        constraintSaveLocation.setOnClickListener(this);
        constraintOffice.setOnClickListener(this);
        constraintGpsLocation.setOnClickListener(this);
        constraintCurrentLocation.setOnClickListener(this);

        setAdapter();

        if (AppPreferenceHelper.getInstance().getDefaultLocation() != null) {
            DeliAddressModel deliAddressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
            tvSelectLocation.setVisibility(View.GONE);
            tvCurrentLocation.setVisibility(View.VISIBLE);
            tvFullCurrentLocation.setVisibility(View.VISIBLE);

            tvCurrentLocation.setText(deliAddressModel.getAddress());
            tvFullCurrentLocation.setText(deliAddressModel.getFullAddress());
        } else {
            tvSelectLocation.setVisibility(View.VISIBLE);
            tvCurrentLocation.setVisibility(View.GONE);
            tvFullCurrentLocation.setVisibility(View.GONE);
        }

        if (LoginHelper.isLoggedIn(activity)) {
            getCompletedOrder();
        } else {
            addHistoryLocation();
        }
    }

    private void setAdapter() {
        DeliAddressModel addressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (addressModel != null) {
            if (addressModel.getFullAddress().contains("Hồ Chí Minh"))
                adapter = new LocationAdapter(activity, mGoogleApiClient, latLngBounds, filter, lnNoResult);
            else if (addressModel.getFullAddress().contains("Hà Nội"))
                adapter = new LocationAdapter(activity, mGoogleApiClient, mHaNoiBounds, filter, lnNoResult);
            else
                adapter = new LocationAdapter(activity, mGoogleApiClient, null, filter, lnNoResult);
        } else adapter = new LocationAdapter(activity, mGoogleApiClient, null, filter, lnNoResult);

        rv_location.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        rv_location.setAdapter(adapter);
        adapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, final Object object, Long count) {
                final DeliAddressModel addressModel = ((DeliAddressModel) object);
//                if (isEditting) {
//                    setUserLocation(editAction, addressModel);
//                    onBackPressed();
//                    return;
//                }
                selectLocation(addressModel);
            }
        });
    }

    private void selectLocation(final DeliAddressModel addressModel) {
        if (addressModel.getPlaceId() != null) {
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, addressModel.getPlaceId())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                Place place = places.get(0);
                                addressModel.setLat(place.getLatLng().latitude);
                                addressModel.setLng(place.getLatLng().longitude);

                                if (isEditting) {
                                    setUserLocation(editAction, addressModel);
                                    onBackPressed();
                                    return;
                                }

                                AppPreferenceHelper.getInstance().setDefaultLocation(addressModel);
                                DeliDataLoader.checkChangeProvinceHeader(activity);
                                DeliveryHomeFragment.isRefreshLocation = true;
                                if (!isLastLocation) {
                                    sendBroadCast(addressModel, place);
                                }
                                finish();
                            } else {
                                Toast.makeText(activity, "Địa điểm không tồn tại", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            if (isEditting) {
                setUserLocation(editAction, addressModel);
                onBackPressed();
                return;
            }

            AppPreferenceHelper.getInstance().setDefaultLocation(addressModel);
            DeliDataLoader.checkChangeProvinceHeader(activity);
            if (isLastLocation) {
                DeliveryHomeFragment.isRefreshLocation = true;
            } else {
                sendBroadCast(addressModel, null);
                DeliveryHomeFragment.isRefreshLocation = true;
            }
            finish();
        }
    }

    private void sendBroadCast(DeliAddressModel addressModel, Place place) {
        Intent intent = new Intent("LOCATION_CHANGE");
        Bundle data = new Bundle();
        data.putSerializable("LOCATION", new DeliAddressModel(place == null ? addressModel.getPlaceId() : place.getId(),
                addressModel.getFullAddress(), addressModel.getAddress(),
                place == null ? addressModel.getLat() : place.getLatLng().latitude,
                place == null ? addressModel.getLng() : place.getLatLng().longitude,
                addressModel.isGPS()));
        intent.putExtras(data);
        sendBroadcast(intent);
    }

    private DeliAddressModel currentLocation = null;

    private void addOrderedAddressList(String address) {
        List<DeliAddressModel> listItems = SQLiteDatabaseHandler.getInstance(activity).getAllAddress();
        if (listItems != null && listItems.size() > 0) {
            for (int i = 0; i < listItems.size(); i++) {
                if (address != null && address.equals(listItems.get(i).getFullAddress()))
                    continue;
                adapter.addItem(listItems.get(i));
            }
        }
    }

    private void addLastestLocation() {
        DeliAddressModel addressModel = SQLiteDatabaseHandler.getInstance(activity).getLastLocationOrder();
        if (addressModel != null) {
            adapter.addItem(addressModel);
        }
    }

    private void addHistoryLocation() {
        List<DeliAddressModel> historyLocationList = SQLiteDatabaseHandler.getInstance(activity).getAllAddress();
        if (historyLocationList != null && historyLocationList.size() > 0) {
            for (int i = 0; i < historyLocationList.size(); i++) {
                new HistoryLocationController(activity, historyLocationList.get(i), lnHistoryLocation, i, historyLocationList.size(), isLastLocation);
            }
        }
    }

    private String getAddressFromLocation(double lat, double lng) {
        String res = "";
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));
                    if (i != returnedAddress.getMaxAddressLineIndex())
                        strReturnedAddress.append(", ");
                }

//                String placeName = "";
//                if (returnedAddress.getSubThoroughfare() == null || returnedAddress.getThoroughfare() == null)
//                    placeName = returnedAddress.getFeatureName();
//                else
//                    placeName = returnedAddress.getSubThoroughfare() + " " + returnedAddress.getThoroughfare();
                res = strReturnedAddress.toString();
            } else {
                Log.w("mapi", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGpsStatus() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void loadGpsLocation() {
        if (checkGpsStatus()) {
            if (checkLocationPermission()) {
                if (tvGpsLocation.getText().toString().length() == 0) {
                    tvRequestPermission.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    locationHelper.setTimeOut(Integer.MAX_VALUE)
                            .requestLocationGoogle(this);
                }
            } else {
                tvRequestPermission.setVisibility(View.VISIBLE);
                tvGpsLocation.setVisibility(View.GONE);
                tvFullGpsLocation.setVisibility(View.GONE);
            }

        } else {
            tvRequestPermission.setVisibility(View.VISIBLE);
            tvGpsLocation.setVisibility(View.GONE);
            tvFullGpsLocation.setVisibility(View.GONE);
        }
    }

    //region Autocomplete API
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("Google API Callback", "Connection Done");
        loadGpsLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("Google API Callback", "Connection Suspended=" + String.valueOf(i));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    mGoogleApiClient.connect();
                    setAdapter();
                }
                break;
            case REQUEST_MAP:
                if (resultCode == RESULT_OK) {
                    DeliAddressModel addressModel = (DeliAddressModel) data.getExtras().getSerializable("LOCATION");
                    if (isEditting) {
                        addressModel.setType(editAction.toString());
                        setUserLocation(editAction, addressModel);
                        onBackPressed();
                    } else {
                        AppPreferenceHelper.getInstance().setDefaultLocation(addressModel);
                        DeliDataLoader.checkChangeProvinceHeader(activity);
                        DeliveryHomeFragment.isRefreshLocation = true;
                        if (!isLastLocation) {
                            sendBroadCast(addressModel, null);
                        }
                        finish();
                    }
                }
                break;
            case LocationHelper.LOCATION_PERMISSION:
            case LocationHelper.LOCATION_PERMISSION_GOOGLE:
                locationHelper.onActivityResult(requestCode, resultCode);
                break;
            case ConstantValue.LOGIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    getUserLocation();
                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static final int REQUEST_CODE = 1;

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google API Callback", "Connection Failed");
        Log.d("Error Code", String.valueOf(connectionResult.getErrorCode()));
        if (!connectionResult.hasResolution()) {
            GoogleApiAvailability.getInstance().getErrorDialog(this, connectionResult.getErrorCode(), 0).show();
            return;
        }

        try {
            connectionResult.startResolutionForResult(this, REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "disconnected", Toast.LENGTH_SHORT).show();
    }

    //endregion

    private void showLoginAlertDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        DialogRequireLoginBinding binding = DataBindingUtil.inflate(LayoutInflater.from(activity),
                R.layout.dialog_require_login, null, false);

        alertDialog.setView(binding.getRoot());

        binding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_SAVE_LOCATION);
                alertDialog.dismiss();
            }
        });

        binding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAction = null;
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private TypeLocation editAction = null;

    private void showEditOption(TypeLocation action) {
        final EditOptionDialog editOptionDialog = new EditOptionDialog(activity, false, false, false);
        editOptionDialog.show();
        editOptionDialog.setOnEdittingSelected(action, new OnEdittingSelected() {
            @Override
            public void onSelected(int type, TypeLocation action) {
                switch (type) {
                    case EditOptionDialog.RESET:
                        edittingViewModel.getIsEditing().setValue(true);
                        break;
                    case EditOptionDialog.REMOVE:
                        final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                        alertDialog.setTitle("Bạn có muốn xóa địa chỉ này không?");
                        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Không", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                editOptionDialog.cancel();
                            }
                        });

                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Có", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setUserLocation(editAction, null);
                                dialog.dismiss();
                                editOptionDialog.cancel();
                            }
                        });
                        alertDialog.show();
                        break;
                }
            }
        });
    }

    private void setUserLocation(TypeLocation action, DeliAddressModel addressModel) {
        if (action == TypeLocation.HOME || action == TypeLocation.WORK) {
            if (addressModel != null) {
                if (action == TypeLocation.HOME) {
                    mBinding.pbHome.setVisibility(View.VISIBLE);
                    if (homeLocationViewModel.getHomeLocation().getValue() == null) {
                        createLocation(addressModel);
                    } else {
                        addressModel.setId(homeLocationViewModel.getHomeLocation().getValue().getId());
                        editLocation(addressModel);
                    }
                } else {
                    mBinding.pbOffice.setVisibility(View.VISIBLE);
                    if (officeLocationViewModel.getOfficeLocation().getValue() == null) {
                        createLocation(addressModel);
                    } else {
                        addressModel.setId(officeLocationViewModel.getOfficeLocation().getValue().getId());
                        editLocation(addressModel);
                    }
                }
            } else {
                DeliAddressModel todeleteModel = null;
                if (action.equals(TypeLocation.HOME)) {
                    mBinding.pbHome.setVisibility(View.VISIBLE);
                    todeleteModel = homeLocationViewModel.getHomeLocation().getValue();
                } else {
                    mBinding.pbOffice.setVisibility(View.VISIBLE);
                    todeleteModel = officeLocationViewModel.getOfficeLocation().getValue();
                }
                if (todeleteModel != null) {
                    deleteLocation(todeleteModel.getId());
                }
            }

//            if (addressModel != null)
//                addressModel.setType(action.toString());
//
//            if (action == TypeLocation.HOME) {
//                homeLocationViewModel.getHomeLocation().setValue(addressModel);
//            } else {
//                officeLocationViewModel.getOfficeLocation().setValue(addressModel);
//            }
            //DatabaseUserLocation.getInstance(getBaseContext()).addLocation(addressModel, editAction.toString());
            return;
        }

        if (action == TypeLocation.LOCAL) {
            if (SQLiteDatabaseHandler.getInstance(activity).isExist(addressModel.getFullAddress())) {
                SQLiteDatabaseHandler.getInstance(activity).removeItem(addressModel.getFullAddress());
            }
            SQLiteDatabaseHandler.getInstance(activity).addAddress(addressModel);
            return;
        }

        // action == null
        if (addressModel == null) {
            homeLocationViewModel.getHomeLocation().setValue(null);
            officeLocationViewModel.getOfficeLocation().setValue(null);
        } else {
            if (addressModel.getType().equals(TypeLocation.HOME.toString())) {
                homeLocationViewModel.getHomeLocation().setValue(addressModel);
            } else {
                officeLocationViewModel.getOfficeLocation().setValue(addressModel);
            }
        }
    }

    private void initViewModel() {
        homeLocationViewModel = ViewModelProviders.of(this).get(HomeLocationViewModel.class);
        officeLocationViewModel = ViewModelProviders.of(this).get(OfficeLocationViewModel.class);
        edittingViewModel = ViewModelProviders.of(this).get(EdittingViewModel.class);

        // init observe
        homeLocationViewModel.getHomeLocation().observe(this, new Observer<DeliAddressModel>() {
            @Override
            public void onChanged(@Nullable DeliAddressModel deliAddressModel) {
                mBinding.pbHome.setVisibility(View.GONE);
                if (deliAddressModel == null) {
                    txtLocationHome.setText(getString(R.string.location_setting_home));
                    imgHomeOption.setVisibility(View.GONE);
                } else {
                    imgHomeOption.setVisibility(View.VISIBLE);
                    if (deliAddressModel.getFullAddress() == null || deliAddressModel.getFullAddress().equals("")) {
                        txtLocationHome.setText(deliAddressModel.getAddress());
                    } else {
                        txtLocationHome.setText(deliAddressModel.getFullAddress());
                    }
                }
            }
        });

        officeLocationViewModel.getOfficeLocation().observe(this, new Observer<DeliAddressModel>() {
            @Override
            public void onChanged(@Nullable DeliAddressModel deliAddressModel) {
                mBinding.pbOffice.setVisibility(View.GONE);
                if (deliAddressModel == null) {
                    txtLocationOffice.setText(getString(R.string.location_setting_office));
                    imgOfficeOption.setVisibility(View.GONE);
                } else {
                    imgOfficeOption.setVisibility(View.VISIBLE);
                    if (deliAddressModel.getFullAddress() == null || deliAddressModel.getFullAddress().equals("")) {
                        txtLocationOffice.setText(deliAddressModel.getAddress());
                    } else {
                        txtLocationOffice.setText(deliAddressModel.getFullAddress());
                    }
                }
            }
        });

        edittingViewModel.getIsEditing().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                isEditting = aBoolean;
                if (adapter.getList() != null) {
                    adapter.getList().clear();
                }
                DeliAddressModel deliAddressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
                if (deliAddressModel != null) {
                    deliAddressModel.setType("default");
                    adapter.addItem(deliAddressModel);
                }
                if (currentLocation != null) {
                    currentLocation.setType("gps");
                    adapter.addItem(currentLocation);
                }
                if (aBoolean) {
                    nestedScrollView.setVisibility(View.GONE);
                } else {
                    nestedScrollView.setVisibility(View.VISIBLE);
                }

                for (DeliAddressModel addressModel : SQLiteDatabaseHandler.getInstance(activity).getAllAddress()) {
                    addressModel.setType("history");
                    adapter.addItem(addressModel);
                }
            }
        });

        // get location from server
        getUserLocation();
    }

    private void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void getAddressFromLocation(final Location location) {
        LoadLocationAddress newTask = new LoadLocationAddress(new LocationCallBack() {
            @Override
            public void callback(LatLng position, ArrayList<String> addressInfo) {
                progressBar.setVisibility(View.GONE);
                String fullAddress = "";
                if (addressInfo != null && addressInfo.get(0) != null && addressInfo.get(0).length() > 0) {
                    fullAddress = addressInfo.get(0);
                    if (fullAddress.contains("Vietnam"))
                        fullAddress = fullAddress.replace("Vietnam", "Việt Nam");
                } else
                    fullAddress = getAddressFromLocation(location.getLatitude(), location.getLongitude());

                if (fullAddress != null && fullAddress.length() > 0) {
                    String headerAddress = fullAddress.substring(0, fullAddress.contains(",") ? fullAddress.indexOf(",") : 0);
                    currentLocation = new DeliAddressModel(fullAddress, headerAddress, location.getLatitude(), location.getLongitude(), true);
                    tvGpsLocation.setVisibility(View.VISIBLE);
                    tvFullGpsLocation.setVisibility(View.VISIBLE);
                    tvRequestPermission.setVisibility(View.GONE);

                    tvGpsLocation.setText(headerAddress);
                    tvFullGpsLocation.setText(fullAddress);
                }
            }
        });

        newTask.execute(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    // region API LOCATION
    private ArrayList<DeliAddressModel> listUserLocation;

    private void getUserLocation() {
        DeliDataLoader.getUserLocation(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listUserLocation = (ArrayList<DeliAddressModel>) object;
                    DeliAddressModel tempHome = null, tempOffice = null;
                    if (listUserLocation != null && listUserLocation.size() > 0) {
                        for (int i = 0; i < listUserLocation.size(); i++) {
                            String type = listUserLocation.get(i).getType();
                            if (type.equals(TypeLocation.HOME.toString())) {
                                tempHome = listUserLocation.get(i);
                                tempHome.setFullAddress(tempHome.getAddress());
                                if (tempHome.getAddress().contains(","))
                                    tempHome.setAddress(tempHome.getAddress().substring(0, tempHome.getAddress().contains(",") ? tempHome.getAddress().indexOf(",") : tempHome.getAddress().length()));
                            } else {
                                tempOffice = listUserLocation.get(i);
                                tempOffice.setFullAddress(tempOffice.getAddress());
                                if (tempOffice.getAddress().contains(","))
                                    tempOffice.setAddress(tempOffice.getAddress().substring(0, tempOffice.getAddress().contains(",") ? tempOffice.getAddress().indexOf(",") : tempOffice.getAddress().length()));
                            }
                        }

                        homeLocationViewModel.getHomeLocation().setValue(tempHome);
                        officeLocationViewModel.getOfficeLocation().setValue(tempOffice);

                        if ((editAction == TypeLocation.HOME && tempHome == null) || (editAction == TypeLocation.WORK && tempOffice == null)) {
                            edittingViewModel.getIsEditing().setValue(true);
                        }


//                        if (tempHome == null) {
//                            homeLocationViewModel.getHomeLocation().setValue(DatabaseUserLocation.getInstance(activity).getLocation(TypeLocation.HOME.toString()));
//                        } else {
//
//                        }
//
//                        if (tempOffice == null) {
//                            officeLocationViewModel.getOfficeLocation().setValue(DatabaseUserLocation.getInstance(activity).getLocation(TypeLocation.WORK.toString()));
//                        } else

                    }

                } else {
                    setUserLocation(null, null);
                    //Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    private void createLocation(final DeliAddressModel addressModel) {
        JsonObject params = new JsonObject();
        params.addProperty("type", editAction.toString());
        params.addProperty("lat", addressModel.getLat());
        params.addProperty("lng", addressModel.getLng());
        params.addProperty("address", addressModel.getFullAddress());

        DeliDataLoader.createUserLocation(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    int id = (int) object;
                    addressModel.setId(id);
                    addressModel.setType(editAction.toString());

                    if (editAction == TypeLocation.HOME) {
                        homeLocationViewModel.getHomeLocation().setValue(addressModel);
                    } else {
                        officeLocationViewModel.getOfficeLocation().setValue(addressModel);
                    }
                } else {
                    mBinding.pbHome.setVisibility(View.GONE);
                    mBinding.pbOffice.setVisibility(View.GONE);
//                    Toast.makeText(activity, "Lưu thất bại", Toast.LENGTH_SHORT).show();
                    //Helper.showErrorDialog(activity, (String) object);
                }


                // DatabaseUserLocation.getInstance(activity).addLocation(addressModel, editAction.toString());
            }
        }, params);
    }

    private void editLocation(final DeliAddressModel addressModel) {
        JsonObject params = new JsonObject();
        params.addProperty("id", addressModel.getId());
        params.addProperty("type", editAction.toString());
        params.addProperty("lat", addressModel.getLat());
        params.addProperty("lng", addressModel.getLng());
        params.addProperty("address", addressModel.getFullAddress());

        DeliDataLoader.editLocation(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    if (editAction == TypeLocation.HOME) {
                        homeLocationViewModel.getHomeLocation().setValue(addressModel);
                    } else {
                        officeLocationViewModel.getOfficeLocation().setValue(addressModel);
                    }
                } else {
                    mBinding.pbHome.setVisibility(View.GONE);
                    mBinding.pbOffice.setVisibility(View.GONE);
//                    Toast.makeText(activity, "Lưu thất bại", Toast.LENGTH_SHORT).show();
                    //Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void deleteLocation(int id) {
        DeliDataLoader.deleteLocation(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    //Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                    if (editAction == TypeLocation.HOME)
                        homeLocationViewModel.getHomeLocation().setValue(null);
                    else
                        officeLocationViewModel.getOfficeLocation().setValue(null);
                } else {
                    mBinding.pbHome.setVisibility(View.GONE);
                    mBinding.pbOffice.setVisibility(View.GONE);
//                    Toast.makeText(activity, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                    //Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, id);
    }

    private void getCompletedOrder() {
        DeliDataLoader.getUserOrderHistory(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<DeliOrderModel> list = (ArrayList<DeliOrderModel>) object;
                    if (list == null || list.size() == 0)
                        return;
                    for (int i = 0; i < list.size(); i++) {
                        DeliOrderModel orderModel = list.get(i);
                        SQLiteDatabaseHandler sqLiteDatabaseHandler = SQLiteDatabaseHandler.getInstance(activity);
                        if (!sqLiteDatabaseHandler.isExist(orderModel.getReceive_address())) {
                            sqLiteDatabaseHandler.addAddress(new DeliAddressModel(orderModel.getReceive_address(),
                                    orderModel.getReceive_address().substring(0, orderModel.getReceive_address().contains(",") ? orderModel.getReceive_address().indexOf(",") : 0),
                                    orderModel.getReceive_lat(), orderModel.getReceive_lng()));
                        }
                    }

                    addHistoryLocation();
                }
            }
        }, 1, 10, "completed", null, null);
    }
    // endregion

    //region BASE EVENT
    @Override
    protected void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            Log.v("Google API", "Connecting");
            mGoogleApiClient.connect();
        } else loadGpsLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            Log.d("Google API", "Dis-Connecting");
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == iv_back) {
            if (isEditting) {
                edittingViewModel.getIsEditing().setValue(false);
                if (!et_address.getText().toString().equals("")) {
                    et_address.setText("");
                }
            } else {
                super.onBackPressed();
            }
        } else if (view == rl_delete) {
            adapter.clearData();
            if (!et_address.getText().toString().equals(""))
                et_address.setText("");
        } else if (view == ivMap) {
            Intent intent = new Intent(activity, MapActivity.class);
            startActivityForResult(intent, REQUEST_MAP);
//            if (!isEditting) {
//                finish();
//            }
        } else if (view == imgHomeOption || view == imgOfficeOption) {
            if (view == imgHomeOption) {
                editAction = TypeLocation.HOME;
            }
            if (view == imgOfficeOption) {
                editAction = TypeLocation.WORK;
            }
            showEditOption(editAction);
        } else if (view == constraintPrivateHome) {
            if (LoginHelper.isLoggedIn(activity)) {
                if (homeLocationViewModel.getHomeLocation().getValue() != null) {
                    if (mBinding.pbHome.getVisibility() == View.GONE)
                        selectLocation(homeLocationViewModel.getHomeLocation().getValue());
                } else {
                    editAction = TypeLocation.HOME;
                    edittingViewModel.getIsEditing().setValue(true);
                    //showEditOption(editAction);
                }
            } else {
                editAction = TypeLocation.HOME;
                showLoginAlertDialog();
            }
        } else if (view == constraintOffice) {
            if (LoginHelper.isLoggedIn(activity)) {
                if (officeLocationViewModel.getOfficeLocation().getValue() != null) {
                    if (mBinding.pbOffice.getVisibility() == View.GONE)
                        selectLocation(officeLocationViewModel.getOfficeLocation().getValue());
                } else {
                    editAction = TypeLocation.WORK;
                    edittingViewModel.getIsEditing().setValue(true);
                    //showEditOption(editAction);
                }
            } else {
                editAction = TypeLocation.WORK;
                showLoginAlertDialog();
            }
        } else if (view == constraintSaveLocation) {
            editAction = TypeLocation.LOCAL;
            edittingViewModel.getIsEditing().setValue(true);
        } else if (view == constraintCurrentLocation) {
            if (tvSelectLocation.getVisibility() == View.GONE) {
                selectLocation(AppPreferenceHelper.getInstance().getDefaultLocation());
            }
        } else if (view == constraintGpsLocation) {
            if (tvRequestPermission.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.VISIBLE);
                locationHelper.setTimeOut(Integer.MAX_VALUE).requestLocationGoogle(this);
            } else {
                if (progressBar.getVisibility() == View.GONE && currentLocation != null)
                    selectLocation(currentLocation);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationHelper.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    public void onBackPressed() {
        if (isEditting) {
            edittingViewModel.getIsEditing().setValue(false);
            if (!et_address.getText().toString().equals("")) {
                et_address.setText("");
            }
        } else {
            super.onBackPressed();
        }
    }
    //endregion

    @Override
    public void onResponse(LocationHelper.Task task) {
        if (task.isSuccess()) {
            getAddressFromLocation(task.getLocation());
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}