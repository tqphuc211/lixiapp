package com.opencheck.client.home.delivery.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivitySupportOrderBinding;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.delivery.dialog.InfoOrderDialog;
import com.opencheck.client.home.delivery.fragments.DeleteOrderFragment;
import com.opencheck.client.home.delivery.interfaces.OnUpdatedOrder;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class SupportOrderActivity extends LixiActivity {
    private DeliOrderModel orderModel = null;
    private String mReceiveTimeChange;

    private ActivitySupportOrderBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_support_order);
        orderModel = (DeliOrderModel) getIntent().getExtras().getSerializable("order");
        initViews();
    }

    private void initViews() {
        mBinding.rlBack.setOnClickListener(this);
        mBinding.rlEditOrder.setOnClickListener(this);
        mBinding.rlEditAddress.setOnClickListener(this);
        mBinding.rlDeleteOrder.setOnClickListener(this);
        mBinding.rlDefault.setOnClickListener(this);

        mReceiveTimeChange = String.valueOf(orderModel.getReceive_date());
        getUserConfig();
    }

    private String reasons = "";

    private void getUserConfig() {
        ArrayList<DeliConfigModel> configModels = (ArrayList<DeliConfigModel>) DeliConfigModel.getData(this);
        for (DeliConfigModel model : configModels) {
            if (model.getKey().equals("lixi_phone")) {
                String numberPhone = model.getValue();
                (new AppPreferenceHelper(activity)).setDefaultPhoneDeli(numberPhone);
            }

            if (model.getKey().equals("cancel_order_reason_list")) {
                reasons = model.getValue() + "|" + LanguageBinding.getString(R.string.deli_support_other_reason, activity);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.rlBack) {
            finish();
        } else if (view == mBinding.rlEditOrder) {
            showDialog(LanguageBinding.getString(R.string.deli_support_edit_oder, activity));

        } else if (view == mBinding.rlEditAddress) {
            InfoOrderDialog dialog = new InfoOrderDialog(activity, false, false, false);
            dialog.show();
            dialog.setData(orderModel, true, mReceiveTimeChange);
            dialog.setIDialogEventListener(new InfoOrderDialog.IDialogEvent() {
                @Override
                public void onOk(DeliAddressModel deliAddressModel, String name, String phone, String time) {
                    mReceiveTimeChange = time;
                }
            });

        } else if (view == mBinding.rlDeleteOrder) {
            if (orderModel.getCurrent_tracking().getState().equals(DeliOrderModel.SUBMITTED) ||
                    orderModel.getCurrent_tracking().getState().equals(DeliOrderModel.PAYMENT)) {
                DeleteOrderFragment fragment = new DeleteOrderFragment();
                Bundle bundle = new Bundle();
                bundle.putString(DeleteOrderFragment.REASONS, reasons);
                bundle.putSerializable(DeleteOrderFragment.DELI_ORDER, orderModel);
                fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameParent, fragment).addToBackStack("delete").commit();
                fragment.setOnUpdatedOrderListener(new OnUpdatedOrder() {
                    @Override
                    public void onUpdated() {
                        finish();
                    }
                });
            } else {
                showDialog(LanguageBinding.getString(R.string.deli_support_cancel_order, activity), "delete");
            }
        } else if (view == mBinding.rlDefault) {
            showDialog(LanguageBinding.getString(R.string.deli_support_other, activity));
        }
    }

    private void showDialog(String title, String... delete) {
        ManualDismissDialog dialog = new ManualDismissDialog(activity, false, true, false);
        dialog.show();
        dialog.setTitle(title);
        if (delete.length > 0)
            dialog.setMessage(String.format(LanguageBinding.getString(R.string.deli_support_cannot_cancel, activity),
                    ConfigModel.getInstance(this).getDefaultSupportPhone()));
        else
            dialog.setMessage(String.format(LanguageBinding.getString(R.string.deli_support_contact_to_edit, activity),
                    ConfigModel.getInstance(this).getDefaultSupportPhone()));
        dialog.setTitleButton(LanguageBinding.getString(R.string.deli_support_call, activity));
        dialog.setIDidalogEventListener(new ManualDismissDialog.IDialogEvent() {
            @Override
            public void onOk() {
                Helper.callPhoneSupport(activity);
            }
        });
    }
}
