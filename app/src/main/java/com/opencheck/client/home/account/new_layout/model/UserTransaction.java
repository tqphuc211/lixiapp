package com.opencheck.client.home.account.new_layout.model;

import com.opencheck.client.models.LixiModel;

public class UserTransaction extends LixiModel {

    /**
     * _index : string
     * date_order : 0
     * id : 0
     * order_uuid : string
     * payment_method : string
     * product_info : {"id":0,"name":"string","price":0,"quantity":0}
     * status : string
     * total_cashback : 0
     * total_price : 0
     */

    private String _index;
    private long date_order;
    private int id;
    private String order_uuid;
    private String payment_method;
    private ProductInfo product_info;
    private String status;
    private int total_cashback;
    private int total_price;

    public String get_index() {
        return _index;
    }

    public void set_index(String _index) {
        this._index = _index;
    }

    public long getDate_order() {
        return date_order;
    }

    public void setDate_order(long date_order) {
        this.date_order = date_order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder_uuid() {
        return order_uuid;
    }

    public void setOrder_uuid(String order_uuid) {
        this.order_uuid = order_uuid;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public ProductInfo getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductInfo product_info) {
        this.product_info = product_info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotal_cashback() {
        return total_cashback;
    }

    public void setTotal_cashback(int total_cashback) {
        this.total_cashback = total_cashback;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }
}
