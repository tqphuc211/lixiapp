package com.opencheck.client.home.rating.control;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;

public class ColorFilterUtils {
    // Matrix
    private static final float[] MATRIX_VIOLET = {
            0.5f, 0, 0, 0, 0,
            0, 0.12f, 0, 0, 0,
            0, 0, 0.63f, 0, 0,
            0, 0, 0, 1, 0
    };

    private static final float[] MATRIX_DARK = {
            0.33f, 0.33f, 0.33f, 0, 0,
            0.33f, 0.33f, 0.33f, 0, 0,
            0.33f, 0.33f, 0.33f, 0, 0,
            0, 0, 0, 1, 0
    };

    // Constant
    public static final int FILTER_VIOLET = 0;
    public static final int FILTER_DARK = 1;

    public static ColorMatrixColorFilter getFilter(int filter){
        ColorMatrix matrix = new ColorMatrix();
        switch (filter){
            case FILTER_VIOLET:
                matrix.set(MATRIX_VIOLET);
                break;
            case FILTER_DARK:
                matrix.set(MATRIX_DARK);
                break;
        }
        return new ColorMatrixColorFilter(matrix);
    }
}
