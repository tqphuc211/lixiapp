package com.opencheck.client.home.product;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowDetailComboNewBinding;
import com.opencheck.client.models.lixishop.ComboItemModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class AdapterComboProduct extends RecyclerView.Adapter {
    LixiActivity activityLixiEvoucherDetail;
    ArrayList<ComboItemModel> listCombo = new ArrayList<>();
    int totalProduct = 1;

    public AdapterComboProduct(LixiActivity activityLixiEvoucherDetail, ArrayList<ComboItemModel> listCombo) {
        this.activityLixiEvoucherDetail = activityLixiEvoucherDetail;
        this.listCombo = listCombo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowDetailComboNewBinding rowDetailComboNewBinding =
                RowDetailComboNewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new HolderItem(rowDetailComboNewBinding.getRoot());
    }


    public void SetRatioContent(HolderItem holder) {
//        int height = activityLixiEvoucherDetail.getWindowManager().getDefaultDisplay().getHeight();
//        int with = activityLixiEvoucherDetail.getWindowManager().getDefaultDisplay().getWidth();
//
//        ViewGroup.LayoutParams layoutParams = holder.ivItem.getLayoutParams();
//        layoutParams.width = with / 3;
//        int withOftvName = layoutParams.width / 5 * 4;
//        layoutParams.height = layoutParams.width * 3 / 4;
//        holder.ivItem.setLayoutParams(layoutParams);
//        holder.tvName.setWidth(withOftvName);
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        lp.setMargins(activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_3), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5), activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_5));
//        lp.width = with / 3 + activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_15);
//        lp.height = with / 3 * 16 / 9 - activityLixiEvoucherDetail.getResources().getDimensionPixelSize(R.dimen.value_40);
//        holder.lnItem.setLayoutParams(lp);


    }

    public void setTotalProduct(int totalProduct) {
        this.totalProduct = totalProduct;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder _holder, final int position) {
        HolderItem holder = (HolderItem) _holder;
        try {
            if (position == 0)
                holder.v_line.setVisibility(View.GONE);
            holder.tv_name.setText(listCombo.get(position).getName());
            holder.tv_price.setText(Helper.getVNCurrency(listCombo.get(position).getPayment_discount_price() * listCombo.get(position).getQuantity() * totalProduct) + "đ");
            holder.tv_count.setText(listCombo.get(position).getQuantity() * totalProduct + "x");// + " voucher");
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(activityLixiEvoucherDetail, CashEvoucherDetailActivity.class);
//                intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, listCombo.get(position).getId());
//                intent.putExtra(ConstantValue.IS_COMBO_ITEM, true);
//                activityLixiEvoucherDetail.startActivity(intent);
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activityLixiEvoucherDetail, listCombo.get(position).getId(), null, ActivityProductCashEvoucherDetail.getParamIsComboItem(true));
            }
        });

    }

    @Override
    public int getItemCount() {
        return listCombo.size();
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        //        ImageView ivItem;
//        TextView tvName, tvPrice, tvQuantity;
//        LinearLayout lnItem;
        View rootView;
        View v_line;
        TextView tv_count;
        TextView tv_name;
        TextView tv_price;

        public HolderItem(final View itemView) {
            super(itemView);
            rootView = itemView;
            v_line = itemView.findViewById(R.id.v_line);
            tv_count = itemView.findViewById(R.id.tv_count);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
//            lnItem = (LinearLayout) itemView.findViewById(R.id.lnItem);
//            ivItem = (ImageView) itemView.findViewById(R.id.ivItem);
//            tvName = (TextView) itemView.findViewById(R.id.tvName);
//            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
//            tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);

        }
    }
}