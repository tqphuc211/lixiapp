package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FeedbackOrderLayoutBinding;
import com.opencheck.client.home.delivery.model.FeedbackItemModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class FeedbackItemAdapter extends RecyclerView.Adapter<FeedbackItemAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<FeedbackItemModel> list;
    private ItemClickListener itemClickListener;

    public FeedbackItemAdapter(LixiActivity activity, ArrayList<FeedbackItemModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FeedbackOrderLayoutBinding feedbackOrderLayoutBinding =
                FeedbackOrderLayoutBinding.inflate(LayoutInflater.from(
                        parent.getContext()),parent, false);
        return new ViewHolder(feedbackOrderLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_service.setText(list.get(position).getName());
        ImageLoader.getInstance().displayImage(list.get(position).getImage(), holder.iv_service, LixiApplication.getInstance().optionsNomal);
        if (list.get(position).isSelected()) {
            holder.iv_service.setBackground(activity.getResources().getDrawable(R.drawable.bg_ring_yellow));
            holder.iv_service.setColorFilter(activity.getResources().getColor(R.color.app_violet));
            holder.tv_service.setTextColor(activity.getResources().getColor(R.color.app_violet));
        } else {
            holder.iv_service.setBackground(activity.getResources().getDrawable(R.drawable.bg_ring_border));
            holder.iv_service.setColorFilter(activity.getResources().getColor(R.color.color_border));
            holder.tv_service.setTextColor(activity.getResources().getColor(R.color.color_text_feedback));
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_service = itemView.findViewById(R.id.tv_service);
            iv_service = itemView.findViewById(R.id.iv_service);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemCLick(getAdapterPosition(), "ITEM", list.get(getAdapterPosition()), 0L);
                }
            });
        }
    }
}
