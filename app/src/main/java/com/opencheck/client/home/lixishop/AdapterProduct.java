package com.opencheck.client.home.lixishop;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.lixishop.detail.ClassicProductDetail;
import com.opencheck.client.home.lixishop.detail.EvoucherDetail;
import com.opencheck.client.home.search.SearchEvoucherByLixiActivity;
import com.opencheck.client.models.lixishop.LixiProductModel;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/8/2018.
 */

public class AdapterProduct extends RecyclerView.Adapter {

    LixiActivity activity;
    ArrayList<LixiProductModel> listCode;
    int layoutId;

    public AdapterProduct(LixiActivity activity, ArrayList<LixiProductModel> listCode, int layoutId) {
        this.activity = activity;
        this.listCode = listCode;
        this.layoutId = layoutId;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutId, parent, false);

        return new HolderItem(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HolderItem viewHolder = (HolderItem) holder;
        LixiProductModel dto = listCode.get(position);
        viewHolder.rootItemView.setTag(position);

        viewHolder.tv_name.setText(dto.getName());
        viewHolder.tv_price.setText(Helper.getVNCurrency(dto.getDiscount_price()) + " " + activity.getResources().getString(R.string.lixi));
        viewHolder.tv_amount_people.setText("" + dto.getQuantity());
        if (layoutId == R.layout.lixi_shop_product_item) {
            int height = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_12)) / 32 * 11;
            ViewGroup.LayoutParams params = viewHolder.iv.getLayoutParams();
            params.height = height;
            params.width = 10 * height / 7;
            viewHolder.iv.setLayoutParams(params);
        }

        if (dto.getProduct_cover_image() == null || dto.getProduct_cover_image().equals(""))
            ImageLoader.getInstance().displayImage(dto.getProduct_image().get(0), viewHolder.iv, LixiApplication.getInstance().optionsNomal);
        else
            ImageLoader.getInstance().displayImage(dto.getProduct_cover_image(), viewHolder.iv, LixiApplication.getInstance().optionsNomal);

    }

    public class HolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        private View v_top;
        private View v_left;
        private LinearLayout ll_item;
        private ImageView iv;
        private TextView tv_name;
        private TextView tv_price;
        private TextView tv_promo;
        private TextView tv_amount_people;

        private String source;

        public HolderItem(View itemView) {
            super(itemView);
            rootItemView = itemView;
            v_top = itemView.findViewById(R.id.v_top);
            v_left = itemView.findViewById(R.id.v_left);
            ll_item = itemView.findViewById(R.id.ll_item);
            iv = itemView.findViewById(R.id.iv);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_promo = itemView.findViewById(R.id.tv_promo);
            tv_amount_people = itemView.findViewById(R.id.tv_amount_people);
            rootItemView.setOnClickListener(this);

            if (activity instanceof SearchEvoucherByLixiActivity) {
                source = TrackingConstant.ContentSource.SEARCH;
            }
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView) {
                Intent intent = new Intent(activity, EvoucherDetail.class);
                intent.putExtra(GlobalTracking.SOURCE, source);
                if (listCode.get((int) v.getTag()).getProduct_type().equals("evoucher")) {
                    intent.putExtra(EvoucherDetail.PRODUCT_ID, listCode.get((int) v.getTag()).getId());
                } else if (listCode.get((int) v.getTag()).getProduct_type().equals("combo")) {
                    intent.putExtra(EvoucherDetail.PRODUCT_ID, listCode.get((int) v.getTag()).getId());
                } else {
                    intent.putExtra(ClassicProductDetail.PRODUCT_ID, listCode.get((int) v.getTag()).getId());
                }
                activity.startActivity(intent);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listCode == null)
            return 0;
        return listCode.size();
    }
}