package com.opencheck.client.home.survey;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyQuestChoiceItemBinding;
import com.opencheck.client.models.merchant.survey.SurveyAnswerModel;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;

public class QuestionChoiceControllerItem {
    LixiActivity activity;
    View parent;
    View rootView;
    SurveyQuestionModel quest;
    private int pos;
    QuestionChoiceController parentController;

    private View v_space;
    private LinearLayout ll_check;
    private TextView tv_name;
    private TextView tv_check;

    public QuestionChoiceControllerItem(LixiActivity activity, ViewGroup parent, SurveyQuestionModel quest, int pos, QuestionChoiceController parentController) {
        this.activity = activity;
        this.parent = parent;
        this.quest = quest;
        this.pos = pos;
        this.parentController = parentController;
        SurveyQuestChoiceItemBinding surveyQuestChoiceItemBinding =
                SurveyQuestChoiceItemBinding.inflate(LayoutInflater.from(activity),
                        parent, false);
        rootView = surveyQuestChoiceItemBinding.getRoot();
        parent.addView(rootView);

        findView();
        setData();
    }

    public void findView() {
        v_space = rootView.findViewById(R.id.v_space);
        ll_check = (LinearLayout) rootView.findViewById(R.id.ll_check);
        tv_name = (TextView) rootView.findViewById(R.id.tv_name);
        tv_check = (TextView) rootView.findViewById(R.id.tv_check);

        if (getPos() == 0)
            v_space.setVisibility(View.GONE);
    }

    public void setData() {
        SurveyAnswerModel asw = quest.getAnswers().get(getPos());
        tv_name.setText(asw.getAnswer_name());
        tv_check.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_uncheck, 0, 0, 0);
//        tv_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_uncheck, 0, 0, 0);

        ll_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentController.selectAnswer(QuestionChoiceControllerItem.this);
            }
        });
    }

    public void setSelected(boolean selected) {
        if (selected)
            tv_check.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_check, 0, 0, 0);
//            tv_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_check, 0, 0, 0);
        else
            tv_check.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_uncheck, 0, 0, 0);
//            tv_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.survey_ic_uncheck, 0, 0, 0);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
