package com.opencheck.client.home.account.activecode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode.fragments.ActiveCodeFragment;
import com.opencheck.client.home.account.activecode.fragments.StoreSelectionFragment;
import com.opencheck.client.home.survey.SurveyDialog;
import com.opencheck.client.home.survey.SurveyInviteDialog;
import com.opencheck.client.models.merchant.survey.SurveyModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class ActiveCodeActivity extends LixiActivity {

    @Override
    public void onClick(View view) {

    }

    private RelativeLayout rlContent;
    public Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.active_code_activity);
        initView();
    }

    private void initView() {
        setupToolbar();
        this.initData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private long voucherId;
    private void initData() {
        ActiveCodeFragment activeCodeFragment = new ActiveCodeFragment();
        Bundle data = new Bundle();
        voucherId = getIntent().getExtras().getLong(ConstantValue.EVOUCHER_ID, -1);
        if (voucherId == -1) {
            Helper.showErrorToast(activity, "Không tìm thấy sản phẩm!");
            finish();
        }
        data.putLong(ConstantValue.EVOUCHER_ID, voucherId);
        activeCodeFragment.setArguments(data);
        replaceFragment(activeCodeFragment);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else
            super.onBackPressed();
    }

    public void setupToolbar() {
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void replaceFragment (LixiFragment fragment){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped || manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fr_main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_CAMERA_QR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LixiFragment currentFragment = getCurrentFragment();
                    if (currentFragment instanceof StoreSelectionFragment) {
                        StoreSelectionFragment selectionFragment = (StoreSelectionFragment) currentFragment;
                        selectionFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("ev", new Gson().toJson(selectionFragment.getEvoucherModel()));
//                        ScannerQRCodeFragment fragment = new ScannerQRCodeFragment();
//                        replaceFragment(fragment);
                    }
                } else {
                    Helper.showErrorDialog(activity, "Xin quyền không thành công!");
                }
                break;
            case ConstantValue.REQUEST_CODE_COARSE_LOCATION:
            case ConstantValue.REQUEST_CODE_FINE_LOCATION:
                getCurrentFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public LixiFragment getCurrentFragment() {
        FragmentManager manager = getSupportFragmentManager();
        int latesIndex = manager.getBackStackEntryCount() - 1;
        if (latesIndex < 0)
            return null;
        return (LixiFragment) manager.findFragmentByTag(manager.getBackStackEntryAt(latesIndex).getName());
    }

    private void setUpActiveReceiver() {
        final IntentFilter intentFilter = new IntentFilter(ConstantValue.PUSH_EVOUCHER_ACTIVE);
        registerReceiver(activeReceiver, intentFilter);
    }

    BroadcastReceiver activeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("ev_id").equals(String.valueOf(voucherId))) {
                if (getCurrentFragment() instanceof ActiveCodeFragment) {
                    ((ActiveCodeFragment) getCurrentFragment()).reloadStatusCode(LanguageBinding.getString(R.string.bought_evoucher_used_state, activity), getDrawable(R.drawable.bg_promotion_green_corner));
                    ((ActiveCodeFragment) getCurrentFragment()).cancelTimer();
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        setUpActiveReceiver();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }, 300);
        //getSurvey();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(activeReceiver);
    }

    public void getSurvey() {
        DataLoader.getUserSurvey(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if(isSuccess){
                    SurveyModel surveyModel = (SurveyModel) object;
                    SurveyInviteDialog surveyInviteDialog = new SurveyInviteDialog(activity, false, true, false);
                    surveyInviteDialog.setIDidalogEventListener(new SurveyInviteDialog.IDidalogEvent() {
                        @Override
                        public void onOk(SurveyModel surveyModel) {
                            startSurvey(surveyModel);
                        }
                    });
                    surveyInviteDialog.setCancelable(false);
                    surveyInviteDialog.show();
                    surveyInviteDialog.setData(surveyModel);
                }else{
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, "53");
    }

    public void startSurvey(SurveyModel surveyModel) {
        SurveyDialog dialogSurvey = new SurveyDialog(activity, false, true, false);
        dialogSurvey.show();
        dialogSurvey.setData(surveyModel);
    }
}
