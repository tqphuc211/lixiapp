package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class ProductMerchantHot implements Serializable {
    private Long id;
    private String state;
    private String name;
    private String image_link;
    private Long store_id;
    private Long merchant_id;
    private Long count_order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public Long getStore_id() {
        return store_id;
    }

    public void setStore_id(Long store_id) {
        this.store_id = store_id;
    }

    public Long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(Long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public Long getCount_order() {
        return count_order;
    }

    public void setCount_order(Long count_order) {
        this.count_order = count_order;
    }
}
