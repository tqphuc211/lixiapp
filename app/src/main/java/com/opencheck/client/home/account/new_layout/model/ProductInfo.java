package com.opencheck.client.home.account.new_layout.model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.lixishop.ComboItemModel;

import java.util.ArrayList;

public class ProductInfo {
    /**
     * id : 0
     * name : string
     * price : 0
     * product_type : string
     * quantity : 0
     */

    private ArrayList<ComboItemModel> combo_item;
    private int id;
    private String name;
    private int price;
    private String product_type;
    private int quantity;

    public ArrayList<ComboItemModel> getCombo_item() {
        return combo_item;
    }

    public void setCombo_item(ArrayList<ComboItemModel> combo_item) {
        this.combo_item = combo_item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
