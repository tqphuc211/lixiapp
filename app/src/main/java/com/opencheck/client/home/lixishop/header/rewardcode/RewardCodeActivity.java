package com.opencheck.client.home.lixishop.header.rewardcode;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RewardCodeActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.LixiCodeModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;
/**
 * Created by vutha_000 on 3/9/2018.
 */

public class RewardCodeActivity extends LixiActivity {

    @Override
    public void onClick(View view) {

    }

   private SwipeRefreshLayout swr;
   private RecyclerView rv_code;
   private TextView tvTitle;
   private LixiCodeAdapter adapter;
   private RelativeLayout rl_back;
   private int page = 1;
   private int record_per_page = DataLoader.record_per_page;
   private boolean isLoading = false;
   private boolean noRemain = false;

    private ArrayList<LixiCodeModel> listCode;
    private ArrayList<LixiCodeBarnerModel> listBarner;

    private RewardCodeActivityBinding mBinding;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.reward_code_activity);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.lixi_code));
        swr = (SwipeRefreshLayout) findViewById(R.id.swr);
        rv_code = (RecyclerView) findViewById(R.id.rv_code);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_code.setLayoutManager(layoutManager);
        adapter = new LixiCodeAdapter(this, null);
        rv_code.setAdapter(adapter);

        rv_code.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (isLoading || noRemain)
                    return;

                if (isRvBottom())
                    startLoadData();
            }
        });

        swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listCode = null;
                adapter.notifyDataSetChanged();
                page = 1;
                isLoading = false;
                noRemain = false;
                startLoadData();
            }
        });


        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        startLoadData();
        startLoadBarner();
        swr.setRefreshing(true);
    }

    public boolean isRvBottom() {
        if (listCode == null || listCode.size() == 0)
            return false;
        int totalItemCount = listCode.size();
        int lastVisibleItem = ((LinearLayoutManager) rv_code.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 5 && listCode.size() >= record_per_page);
    }

    private void startLoadData() {
        DataLoader.getLixiShopRewardCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    swr.setRefreshing(false);
                    if (page == 1) {
                        listCode = (ArrayList<LixiCodeModel>) object;
                        if (listCode == null || listCode.size() == 0) {
                            if (listCode == null) {
                                listCode = new ArrayList<LixiCodeModel>();
                            }
                            listCode.add(0, new LixiCodeModel());
                            showData();
                            noRemain = true;
                            return;
                        }
                        listCode.add(0, new LixiCodeModel());
                        showData();

                    } else {
                        ArrayList<LixiCodeModel> list = (ArrayList<LixiCodeModel>) object;
                        if (listCode == null || list == null || list.size() == 0) {
                            noRemain = true;
                            return;
                        }
                        listCode.addAll(list);
                        adapter.notifyDataSetChanged();
                    }

                    page++;
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
                isLoading = false;
            }
        }, page);
        isLoading = true;
    }

    public void showData() {
        adapter = new LixiCodeAdapter(this, listCode);
        if (listBarner != null && listBarner.size() > 0)
            adapter.setHeaderData(listBarner);
        rv_code.setAdapter(adapter);
    }


    private void startLoadBarner() {
        DataLoader.getLixiShopRewardCodeBanner(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listBarner = (ArrayList<LixiCodeBarnerModel>) object;
                    showBarner();
                } else {
                }
            }
        });
    }

    public void showBarner() {
        if (adapter != null)
            adapter.setHeaderData(listBarner);
    }
}
