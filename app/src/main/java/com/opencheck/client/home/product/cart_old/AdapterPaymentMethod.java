package com.opencheck.client.home.product.cart_old;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiEvoucherPaymentMethodBinding;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class AdapterPaymentMethod extends RecyclerView.Adapter {

    LixiActivity activity;
    List<PaymentMethodModel> listPaymentMethod;
    private int lastPost = -1;
    PaymentMethodModel currentPaymentMethod = null;

    AdapterPaymentMethod(LixiActivity activity, List<PaymentMethodModel> listPaymentMethod) {
        this.activity = activity;
        this.listPaymentMethod = listPaymentMethod;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case PaymentMethodModel.PAYMENT_COD: {
                LixiEvoucherPaymentMethodBinding evoucherPaymentMethodBinding =
                        LixiEvoucherPaymentMethodBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                return new CODViewItemHolder(evoucherPaymentMethodBinding.getRoot());
            }
            default: {
                LixiEvoucherPaymentMethodBinding evoucherPaymentMethodBinding =
                        LixiEvoucherPaymentMethodBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                return new HolderItem(evoucherPaymentMethodBinding.getRoot());
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case PaymentMethodModel.PAYMENT_COD: {
                ((CODViewItemHolder) holder).bind(position);
                break;
            }
            default: {
                ((HolderItem) holder).bind(position);
            }
        }
    }

    public class CODViewItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View rootItemView;
        private TextView content;
        private View v_line;
        private RadioButton cb;

        CODViewItemHolder(View itemView) {
            super(itemView);
            rootItemView = itemView;
            cb = itemView.findViewById(R.id.cb);
            v_line = itemView.findViewById(R.id.v_line);
            content = itemView.findViewById(R.id.payment_content);
            rootItemView.setOnClickListener(this);
            content.setVisibility(View.GONE);
            cb.setOnClickListener(this);
        }

        void bind(final int position) {
            PaymentMethodModel dto = listPaymentMethod.get(position);
            rootItemView.setTag(position);
            cb.setTag(position);
            cb.setText(" " + dto.getValue());

            if (dto.isSelected()) {
                lastPost = position;
                currentPaymentMethod = dto;
                cb.setChecked(true);
                content.setText(getCodFeeInfo(dto.getPaymentCodFee()));
                content.setVisibility(View.VISIBLE);
            } else {
                cb.setChecked(false);
                content.setVisibility(View.GONE);
            }

            if (position == listPaymentMethod.size() - 1) {
                v_line.setVisibility(View.GONE);
            }
            changeCheckBoxColor(cb);
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView || v == cb) {
                PaymentMethodModel currentMethod = listPaymentMethod.get((int) v.getTag());
                if (lastPost == -1) {
                    listPaymentMethod.get((int) v.getTag()).setSelected(!currentMethod.isSelected());
                    notifyDataSetChanged();
                } else {
                    listPaymentMethod.get((int) v.getTag()).setSelected(!currentMethod.isSelected());
                    if (lastPost != (int) v.getTag()) {
                        listPaymentMethod.get(lastPost).setSelected(false);
                    }
                    notifyDataSetChanged();
                }
                lastPost = (int) v.getTag();
                if (listPaymentMethod.get(lastPost).isSelected()) {
                    currentMethod = listPaymentMethod.get(lastPost);
                } else {
                    currentPaymentMethod = null;
                }
            }
        }

        private Spanned getCodFeeInfo(String codInfo) {
            try {
                JSONObject jsonObject = new JSONObject(codInfo);
                String minMaxInfo = String.format(activity.getString(R.string.cod_min_max_info),
                        Helper.getVNCurrency(jsonObject.getLong("min")),
                        Helper.getVNCurrency(jsonObject.getLong("max")));
                return Html.fromHtml(minMaxInfo);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return Html.fromHtml(activity.getString(R.string.cant_find_cod_payment_fee));
        }
    }

    public class HolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        private View v_line;
        private TextView content;
        private RadioButton cb;

        HolderItem(View itemView) {
            super(itemView);
            rootItemView = itemView;
            cb = (RadioButton) itemView.findViewById(R.id.cb);
            v_line = (View) itemView.findViewById(R.id.v_line);
            content = itemView.findViewById(R.id.payment_content);
            content.setVisibility(View.GONE);
            rootItemView.setOnClickListener(this);
            cb.setOnClickListener(this);
        }

        void bind(final int position) {
            PaymentMethodModel dto = listPaymentMethod.get(position);
            rootItemView.setTag(position);
            cb.setTag(position);

            cb.setText(" " + dto.getValue());

            if (dto.isSelected()) {
                lastPost = position;
                currentPaymentMethod = dto;
                cb.setChecked(true);
            } else {
                cb.setChecked(false);
            }

            if (position == listPaymentMethod.size() - 1) {
                v_line.setVisibility(View.GONE);
            }
            changeCheckBoxColor(cb);
        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView || v == cb) {
                PaymentMethodModel currentMethod = listPaymentMethod.get((int) v.getTag());
                if (lastPost == -1) {
                    listPaymentMethod.get((int) v.getTag()).setSelected(!currentMethod.isSelected());
                    notifyDataSetChanged();
                } else {
                    listPaymentMethod.get((int) v.getTag()).setSelected(!currentMethod.isSelected());
                    if (lastPost != (int) v.getTag()) {
                        listPaymentMethod.get(lastPost).setSelected(false);
                    }
                    notifyDataSetChanged();
                }
                lastPost = (int) v.getTag();
                if (listPaymentMethod.get(lastPost).isSelected()) {
                    currentMethod = listPaymentMethod.get(lastPost);
                } else {
                    currentPaymentMethod = null;
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listPaymentMethod.get(position).getType();
    }

    @Override
    public int getItemCount() {
        if (listPaymentMethod == null)
            return 0;
        return listPaymentMethod.size();
    }

    public PaymentMethodModel getCurrentPaymentMethod() {
        return currentPaymentMethod;
    }

    private void changeCheckBoxColor(RadioButton checkBox) {
        if (checkBox == null)
            return;
        if (!checkBox.isChecked()) {
            checkBox.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
        } else if (checkBox.isChecked()) {
            checkBox.setTextColor(activity.getResources().getColor(R.color.my_black));
        }
    }

    public void performClick(int position) {
        PaymentMethodModel currentMethod = listPaymentMethod.get(position);
        if (lastPost == -1) {
            listPaymentMethod.get(position).setSelected(!currentMethod.isSelected());
            notifyDataSetChanged();
        } else {
            listPaymentMethod.get(position).setSelected(!currentMethod.isSelected());
            if (lastPost != position) {
                listPaymentMethod.get(lastPost).setSelected(false);
            }
            notifyDataSetChanged();
        }
        lastPost = position;
        if (listPaymentMethod.get(lastPost).isSelected()) {
            currentMethod = listPaymentMethod.get(lastPost);
        } else {
            currentPaymentMethod = null;
        }
    }
}
