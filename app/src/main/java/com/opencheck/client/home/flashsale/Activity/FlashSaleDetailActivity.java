package com.opencheck.client.home.flashsale.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.databinding.ActivityFlashSaleDetailBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.flashsale.Adapter.FlashSaleListAdapter;
import com.opencheck.client.home.flashsale.Adapter.TimeSpanAdapter;
import com.opencheck.client.home.flashsale.Interface.OnFinishTimerListener;
import com.opencheck.client.home.flashsale.Model.FlashSaleNow;
import com.opencheck.client.home.flashsale.Model.FlashSaleTime;
import com.opencheck.client.home.flashsale.Service.AlarmFlashSale;
import com.opencheck.client.home.flashsale.View.TimerView;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class FlashSaleDetailActivity extends LixiActivity implements ItemClickListener, OnFinishTimerListener {
    // View
    TimerView timerView;
    TextView txtStatus;
    EditText edtSearch;
    RecyclerView recTimeSpan, recFlashSale;
    ImageView imgBack, imgVoiceSearch;

    // Adapter
    TimeSpanAdapter timeAdapter;
    LinearLayoutManager timeSpanLayoutManager;

    FlashSaleListAdapter flashSaleListAdapter;
    LinearLayoutManager flashSaleLayoutManager;

    // List
    ArrayList<FlashSaleTime> listTime;
    ArrayList<FlashSaleNow.ProductsBean> listProductsBean;

    // Var
    boolean isLoading = false;
    boolean isOutOfVoucherPage = false;
    int currentID = -1;
    int currentPos = 0;
    long flashSaleID = -1;
    long startTime = 0;
    boolean isHappening = false;

    private ActivityFlashSaleDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_flash_sale_detail);
        initView();
        initFlashSaleListAdapter();
        initAdapter(0);
        loadTimeFlashSale();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                loadNew(edtSearch.getText().toString());
            }
        });
    }

    private void initView() {
        timerView = (TimerView) findViewById(R.id.timerView);

        txtStatus = (TextView) findViewById(R.id.txtStatus);

        edtSearch = (EditText) findViewById(R.id.edtSearch);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgVoiceSearch = (ImageView) findViewById(R.id.imgVoiceSearch);

        recTimeSpan = (RecyclerView) findViewById(R.id.recTimeSpan);
        recFlashSale = (RecyclerView) findViewById(R.id.recFlashSaleNow);

        listTime = new ArrayList<>();
        listProductsBean = new ArrayList<>();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        recFlashSale.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recFlashSale.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount = flashSaleLayoutManager.getChildCount();
                    int totalItemCount = flashSaleLayoutManager.getItemCount();
                    int pastVisiblesItems = flashSaleLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 3) {
                        Helper.showLog("Load more");
                        loadFlashSaleNow(currentID, "", false);
                        isLoading = true;
                    }
                }
            }
        });

        imgBack.setOnClickListener(this);
        imgVoiceSearch.setOnClickListener(this);
    }

    private void initAdapter(long currentTime) {
        timeAdapter = new TimeSpanAdapter(this, listTime, currentTime, this);
        timeSpanLayoutManager = new LinearLayoutManager(this);
        timeSpanLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recTimeSpan.setLayoutManager(timeSpanLayoutManager);
        recTimeSpan.setAdapter(timeAdapter);
    }

    private void loadTimeFlashSale() {
        DataLoader.getListFlashSaleTime(this, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ApiWrapperForListModel<FlashSaleTime> result = (ApiWrapperForListModel<FlashSaleTime>) object;
                    listTime = result.getRecords();
                    long currentTime = result.get_meta().getCurrentTime() * 1000;
                    timeAdapter.setData(listTime, currentTime);

                    if (listTime.get(currentPos).getStart_time() * 1000 <= currentTime) {
                        txtStatus.setText("Thời gian còn lại");
                        timerView.startTimer(listTime.get(currentPos).getEnd_time() * 1000 - currentTime);
                        isHappening = true;
                    } else {
                        txtStatus.setText("Bắt đầu trong");
                        timerView.startTimer(listTime.get(currentPos).getStart_time() * 1000 - currentTime);
                        startTime = listTime.get(currentPos).getStart_time();
                        isHappening = false;
                    }

                    timerView.addOnFinishListener(FlashSaleDetailActivity.this);

                    page = 1;
                    isLoading = false;
                    isOutOfVoucherPage = false;
                    if (currentID < 0) {
                        currentID = listTime.get(0).getId();
                        loadFlashSaleNow(listTime.get(0).getId(), "", true);
                    } else {
                        loadFlashSaleNow((int) currentID, "", true);
                    }
                    flashSaleID = listTime.get(currentPos).getId();
                }
            }
        }, 1, 10);
    }

    private ItemClickListener flashSaleItemClick;

    private void initFlashSaleListAdapter() {
        flashSaleItemClick = new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (action.equals(FlashSaleListAdapter.ACTION_ITEM_CLICK)) {
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, (Long) object, null);
                    return;
                }

            if (action.equals(FlashSaleListAdapter.ACTION_REMIND)) {
                    AlarmFlashSale alarmFlashSale = new AlarmFlashSale(activity);
                    if (timerView.getUntilFinishMillis() <= AlarmFlashSale.TIME_ALARM) {
                        alarmFlashSale.start((Long) object, AlarmFlashSale.TIME_ALARM, startTime);
                    } else {
                        alarmFlashSale.start((Long) object, timerView.getUntilFinishMillis(), startTime);
                    }
                }
            }
        };

        flashSaleListAdapter = new FlashSaleListAdapter(this, listProductsBean, flashSaleItemClick);
        flashSaleLayoutManager = new LinearLayoutManager(this);
        flashSaleLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recFlashSale.setLayoutManager(flashSaleLayoutManager);
        recFlashSale.setAdapter(flashSaleListAdapter);
    }

    private int page = 1;
    private int record_per_page = 10;

    private void loadFlashSaleNow(final int id, String keyword, final boolean newList) {
        if (!isLoading) {
            if (!isOutOfVoucherPage) {
                DataLoader.getListFlashSaleDetail(this, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
//                            FlashSaleNow fsn = (FlashSaleNow) object;
                            ArrayList<FlashSaleNow.ProductsBean> listProducts = new ArrayList<>();
                            listProducts = (ArrayList<FlashSaleNow.ProductsBean>) object;

                            if (newList) {
                                listProductsBean.clear();
                            }
                            listProductsBean.addAll(listProducts);
                            flashSaleListAdapter.isHappening(isHappening, flashSaleID, startTime);
                            setOutOfPage(listProducts.size());
                            page++;
                            isLoading = false;
                        } else {
                            isLoading = true;
                            Log.d("TTTT", object.toString());
                        }
                    }
                }, page, record_per_page, id, keyword);
            }
        }
    }

    private void setOutOfPage(int size) {
        if (size < record_per_page) {
            isOutOfVoucherPage = true;
        }
    }


    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{"en-US", "vi-VN"});
        intent.putExtra("android.speech.extra.LANGUAGE", new String[]{"vi-VN", "en-US"});
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nói gì đó");
        startActivityForResult(intent, ConstantValue.VOICE_SEARCH_CODE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.imgVoiceSearch:
                startVoiceInput();
                break;
        }
    }

    @Override
    public void onItemCLick(int pos, String action, Object object, Long count) {
        FlashSaleTime fst = (FlashSaleTime) object;
        currentID = fst.getId();
        currentPos = pos;
        if (action.equals("INIT") || action.equals("CLICK")) {
            loadTimeFlashSale();
        }
    }

    private void loadNew(String keyword) {
        page = 1;
        isLoading = false;
        isOutOfVoucherPage = false;
        loadFlashSaleNow(currentID, keyword, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstantValue.VOICE_SEARCH_CODE && resultCode == RESULT_OK && null != data) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            edtSearch.setText(result.get(0));
        }
    }

    @Override
    public void onFinish() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listTime.clear();
                listProductsBean.clear();
                page = 1;
                currentID = -1;
                loadTimeFlashSale();
            }
        }, 1000);
    }
}
