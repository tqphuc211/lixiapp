package com.opencheck.client.home.product.cart_old;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopBuyFailedDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.history.detail.controller.OrderDetailInfoController;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.merchant.OrderInfoModel;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class FailedCashDialog extends LixiTrackingDialog {
    public FailedCashDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private LinearLayout ll_buy;
    private TextView tv_message;
    private LinearLayout ll_detail;
    private Button btnClose;

    SpannableStringBuilder spanMessage;
    String mss = "";
    String title = "";
    String okText = "";
    LixiShopBankTransactionResult transactionResult;
    OrderDetailModel orderDTO;

    public void setData(String title, String ms, String okText, LixiShopBankTransactionResult transactionResult) {
        mss = ms;
        this.title = title;
        this.okText = okText;
        this.transactionResult = transactionResult;

//        tv_title.setText(title);
//        if (spanMessage != null)
//            tv_message.setText(spanMessage);
//        else if (mss.length() > 0)
//            tv_message.setText(mss);

        if (okText.length() > 0)
            tv_message.setText(okText);

        btnClose.setOnClickListener(this);
        ll_buy.setOnClickListener(this);

        getOrderdetail();
    }

    private LixiShopBuyFailedDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopBuyFailedDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
    }


    private void findViews() {
        tv_message = (TextView) findViewById(R.id.tv_message);
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        ll_buy = (LinearLayout) findViewById(R.id.ll_buy);
        btnClose = (Button) findViewById(R.id.btnClose);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
                activity.finish();
                break;
            case R.id.ll_buy:
                if (eventListener != null)
                    eventListener.onBuyAgain();
                break;
            default:
                break;
        }
    }

    private void showOrderDetailInfo() {
        for (OrderInfoModel dto : orderDTO.getInfo())
            new OrderDetailInfoController(activity, 0, dto.getName(), dto.getValue(), Long.parseLong("FF" + dto.getColor(), 16), !dto.isBold(), ll_detail);
    }

    private void getOrderdetail() {
        DataLoader.getOrderDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    orderDTO = (OrderDetailModel) object;
                    showOrderDetailInfo();
                }
            }
        }, transactionResult.getOrder_id());
    }

    public interface IDidalogEvent {
        void onBuyAgain();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
