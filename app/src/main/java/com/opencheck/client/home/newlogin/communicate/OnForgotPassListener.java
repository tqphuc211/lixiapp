package com.opencheck.client.home.newlogin.communicate;

public interface OnForgotPassListener {
    void onForgotPass(String phone);
}
