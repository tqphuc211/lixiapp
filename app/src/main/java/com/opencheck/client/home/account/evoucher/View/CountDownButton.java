package com.opencheck.client.home.account.evoucher.View;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewCountdownButtonBinding;
import com.opencheck.client.home.flashsale.Interface.OnFinishTimerListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.concurrent.TimeUnit;

public class CountDownButton extends RelativeLayout{
    private Context context;

    public static final String USED = "used";
    public static final String EXPIRED = "expired";
    public static final String WAITING = "waiting";
    public static final String CAN_ACTIVE = "can_active";

    private OnFinishTimerListener onFinishTimerListener;

    public CountDownButton(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public CountDownButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public CountDownButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private ViewCountdownButtonBinding mBinding;

    private void initView(){
        mBinding = ViewCountdownButtonBinding.inflate(LayoutInflater.from(getContext()),
                this, true);
    }

    public Button getBtn() {
        return mBinding.btn;
    }

    public void setStatus(String stt, long time){
        if (stt.equals(USED)) {
            mBinding.btn.setText(LanguageBinding.getString(R.string.bought_evoucher_used_state, context));
            mBinding.btn.setBackground(context.getDrawable(R.drawable.bg_promotion_green_corner));
            mBinding.btn.setClickable(false);
        } else if (stt.equals(EXPIRED)) {
            mBinding.btn.setText(LanguageBinding.getString(R.string.bought_evoucher_expiry, context));
            mBinding.btn.setBackground(context.getDrawable(R.drawable.bg_signup_black));
            mBinding.btn.setClickable(false);
        } else if (stt.equals(WAITING)) {
            mBinding.btn.setClickable(false);
            if (time > 0) {
                startTimer(time * 1000);
            }
        } else if (stt.equals(CAN_ACTIVE)) {
            mBinding.btn.setText(context.getString(R.string.ev_active_voucher));
            mBinding.btn.setBackground(context.getDrawable(R.drawable.bg_promotion_red_corner_right));
        }
    }

    private static final String FORMAT = "%02d:%02d";
    private CountDownTimer countDownTimer = null;
    private void startTimer(long timer) {
        cancelTimer();
        countDownTimer = new CountDownTimer(timer, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                Log.d("mapi", "timer " + millisUntilFinished);
                mBinding.btn.setText(LanguageBinding.getString(R.string.bought_evoucher_waiting, context) + " (" + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))
                        , TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + ")");

                mBinding.btn.setBackground(context.getDrawable(R.drawable.bg_active_code_waiting_status_orange));
            }

            public void onFinish() {
                if (onFinishTimerListener != null){
                    onFinishTimerListener.onFinish();
                }
            }
        }.start();
    }

    public void cancelTimer() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    public void addOnFinishTimerListener(OnFinishTimerListener onFinishTimerListener){
        this.onFinishTimerListener = onFinishTimerListener;
    }
}
