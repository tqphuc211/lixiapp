package com.opencheck.client.home.delivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GateWayModel implements Serializable {
    @SerializedName("field")
    private String field;

    @SerializedName("type")
    private String type;

    @SerializedName("required")
    private boolean required;

    @SerializedName("enum")
    private String _enum;

    @SerializedName("description")
    private String description;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getEnum() {
        return _enum;
    }

    public void setEnumm(String _enum) {
        this._enum = _enum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
