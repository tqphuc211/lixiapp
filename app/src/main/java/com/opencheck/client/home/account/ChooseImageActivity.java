package com.opencheck.client.home.account;

import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityChooseImageBinding;
import com.opencheck.client.models.user.ImageModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

public class ChooseImageActivity extends LixiActivity {

    private ImageAdapter imageAdapter;
    private RelativeLayout rl_back;
    private TextView tv_title, tvAdd;
    private GridView gridView;
    private RecyclerView rv_image;
    private String albumName;
    private ProgressBar pb_image_selection;
    private ArrayList<ImageModel> arrImg = new ArrayList<>();
    private int lastPos = -1;
    private LoadImageTask loadImageTask;

    private ActivityChooseImageBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_image);
        albumName = getIntent().getStringExtra(ConstantValue.KEY_ALBUM);
        initViews();
    }

    private void initViews(){
        tv_title = (TextView) findViewById(R.id.tv_title);
        tvAdd = (TextView) findViewById(R.id.tvAdd);
        gridView = (GridView) findViewById(R.id.gv_select_image);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        pb_image_selection = (ProgressBar) findViewById(R.id.pb_image_selection);
        rv_image = (RecyclerView) findViewById(R.id.rv_image);

        rl_back.setOnClickListener(this);
        tvAdd.setOnClickListener(this);
        tv_title.setText(albumName);

        loadImageTask = new LoadImageTask();
        loadImageTask.execute();
    }

    AbsListView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            toggleSelection(position);
        }
    };

    private void toggleSelection(int pos){
        if(lastPos != -1) {
            if(lastPos != pos) {
                arrImg.get(pos).isSelected = !arrImg.get(pos).isSelected;
                arrImg.get(lastPos).isSelected = false;
            }
            else{
                if(arrImg.get(pos).isSelected)
                    tvAdd.setVisibility(View.VISIBLE);
                else tvAdd.setVisibility(View.GONE);
            }
        }
        else{
            arrImg.get(pos).isSelected = !arrImg.get(pos).isSelected;
            tvAdd.setVisibility(View.VISIBLE);
        }
        imageAdapter.notifyDataSetChanged();
        lastPos = pos;
    }

    @Override
    public void onClick(View v) {
        if(v == rl_back)
            activity.onBackPressed();
        else if(v == tvAdd){
            getImage();
            activity.onBackPressed();
        }
    }

    private void getImage(){
        for(int i = 0; i < arrImg.size(); i++){
            if(arrImg.get(i).isSelected) {
                if(ConstantValue.image.size() > 0)
                    ConstantValue.image.remove(0);
                ConstantValue.image.add(arrImg.get(i));
                break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        loadImageTask.cancel(true);
    }

    private class LoadImageTask extends AsyncTask<String, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            arrImg.clear();
            pb_image_selection.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... strings) {
            long id;
            String path = null;
            String name = null;
            String timestamp = null;
            Uri uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String projection[] = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED,
                    MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME };

            Cursor cursor = getContentResolver().query(uriExternal, projection, "bucket_display_name=\"" + albumName + "\"", null, null);

            while(cursor.moveToNext()){
                id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID));
                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
                name = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME));
                timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED));
                arrImg.add(new ImageModel(id, name, path, convertToTime(timestamp), false));
            }
            cursor.close();
            Collections.sort(arrImg, new ImageComparator("dsc"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pb_image_selection.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageAdapter = new ImageAdapter(activity, arrImg, R.layout.item_image_layout);
                    rv_image.setLayoutManager(new GridLayoutManager(activity, 3));
                    rv_image.setAdapter(imageAdapter);
                    imageAdapter.setItemClickListener(new ItemClickListener() {
                        @Override
                        public void onItemCLick(int pos, String action, Object object, Long count) {
                            toggleSelection(pos);
                        }
                    });
                }
            }, 100);

         //   gridView.setOnItemClickListener(onItemClickListener);
        }

        private HashMap<String, String> mappingInbox(String album, String path, String timestamp, String time, String count){
            HashMap<String, String> map = new HashMap<>();
            map.put(ConstantValue.KEY_ALBUM, album);
            map.put(ConstantValue.KEY_PATH, path);
            map.put(ConstantValue.KEY_TIMESTAMP, timestamp);
            map.put(ConstantValue.KEY_TIME, time);
            map.put(ConstantValue.KEY_COUNT, count);
            return map;
        }

        private String convertToTime(String timestamp){
            long datetime = Long.parseLong(timestamp);
            Date date = new Date(datetime);
            DateFormat formatter = new SimpleDateFormat("dd/MM HH:mm");
            return formatter.format(date);
        }
    }

    private class ImageComparator implements Comparator<ImageModel>{
        private String key, order;

        public ImageComparator(String order) {
            this.order = order;
        }
        @Override
        public int compare(ImageModel first, ImageModel second) {
            String firstValue = first.timestamp;
            String secondValue = second.timestamp;
            if(this.order.toLowerCase().contentEquals("asc"))
                return firstValue.compareTo(secondValue);
            else
                return secondValue.compareTo(firstValue);
        }
    }
}
