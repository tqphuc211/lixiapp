package com.opencheck.client.home.product.gateway.result;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowsVoucherLayoutBinding;
import com.opencheck.client.home.lixishop.detail.ClassicProductDetail;
import com.opencheck.client.home.lixishop.detail.EvoucherDetail;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by
 * Sugar on 3/7/2018.
 */


public class ProductHotAdapters extends RecyclerView.Adapter<ProductHotAdapters.ViewHolder>{

    private LixiActivity activity;
    private ArrayList<LixiHotProductModel> list_voucher;
    private ItemClickListener itemClickListener;

    public ProductHotAdapters(LixiActivity activity, ArrayList<LixiHotProductModel> list_voucher,ItemClickListener itemClickListener) {
        this.list_voucher = list_voucher;
        this.activity = activity;
        this.itemClickListener = itemClickListener;
    }
    public void loadData(ArrayList<LixiHotProductModel> list_voucher){
        this.list_voucher = list_voucher;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowsVoucherLayoutBinding rowsVoucherLayoutBinding =
                RowsVoucherLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ProductHotAdapters.ViewHolder(rowsVoucherLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.rootItemView.setTag(position);
        holder.tvName.setText(list_voucher.get(position).getName());
        int width = 0;
        try {
            width = (int) (((ActivityProductCashEvoucherDetail) activity).widthScreen  * 0.4);
        }catch (ClassCastException e){
//            width = (int) (((ActivityProductCashEvoucherDetail) activity).widthScreen  * 0.4);
        }

        ViewGroup.LayoutParams params = holder.ivHinh.getLayoutParams();
        params.width = width;
        params.height = 7 * holder.ivHinh.getLayoutParams().width /11;
        holder.ivHinh.setLayoutParams(params);
        holder.tvPrice.setText("" + Helper.getVNCurrency(list_voucher.get(position).getDiscount_price()) + " "+ activity.getResources().getString(R.string.lixi));
        holder.tvAmountPeople.setText("" + list_voucher.get(position).getQuantity_order_done());

        if (list_voucher.get(position).getProduct_cover_image() == null || list_voucher.get(position).getProduct_cover_image().equals("")){
            ImageLoader.getInstance().displayImage(list_voucher.get(position).getProduct_image().get(0), holder.ivHinh, LixiApplication.getInstance().optionsNomal);
        }else{
            ImageLoader.getInstance().displayImage(list_voucher.get(position).getProduct_cover_image(), holder.ivHinh, LixiApplication.getInstance().optionsNomal);
        }

    }

    @Override
    public int getItemCount() {
        if (list_voucher == null || list_voucher.size() == 0)
            return 0;
        return list_voucher.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivHinh;
        private TextView tvName, tvPrice, tvAmountPeople;
        private LinearLayout container;
        private View rootItemView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootItemView = itemView;
            ivHinh = itemView.findViewById(R.id.iv_hinh);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvAmountPeople = itemView.findViewById(R.id.tv_amount_people);
            container = itemView.findViewById(R.id.ll_item);

            rootItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view == rootItemView) {
                        if (list_voucher.get((int) view.getTag()).getProduct_type().equals("evoucher")) {
                            Intent intent = new Intent(activity, EvoucherDetail.class);
                            intent.putExtra(EvoucherDetail.PRODUCT_ID, list_voucher.get((int) view.getTag()).getId());
                            activity.startActivity(intent);
//                            itemClickListener.onItemCLick(0,"","",0L);
                        }
                        else if(list_voucher.get((int) view.getTag()).getProduct_type().equals("combo")){
                            Intent intent = new Intent(activity, EvoucherDetail.class);
                            intent.putExtra(EvoucherDetail.PRODUCT_ID, list_voucher.get((int) view.getTag()).getId());
                            activity.startActivity(intent);
//                            itemClickListener.onItemCLick(0,"","",0L);
                        }
                        else {
                            Intent intent = new Intent(activity, ClassicProductDetail.class);
                            intent.putExtra(ClassicProductDetail.PRODUCT_ID, list_voucher.get((int) view.getTag()).getId());
                            activity.startActivity(intent);
//                            itemClickListener.onItemCLick(0,"","",0L);
                        }
                    }
                }
            });
        }
    }
}