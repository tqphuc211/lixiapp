package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AddonCategoryModel implements Serializable {

    private long id;
    private String name;
    private String data_type;
    private boolean required;
    private boolean multichoice;
    private Integer min_total_quantity;
    private Integer max_total_quantity;
    private Integer current_total_quantity;
    private ArrayList<AddonModel> list_addon;
    private int type = 1;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getMin_total_quantity() {
        return min_total_quantity;
    }

    public void setMin_total_quantity(Integer min_total_quantity) {
        this.min_total_quantity = min_total_quantity;
    }

    public Integer getMax_total_quantity() {
        return max_total_quantity;
    }

    public void setMax_total_quantity(Integer max_total_quantity) {
        this.max_total_quantity = max_total_quantity;
    }

    public Integer getCurrent_total_quantity() {
        return current_total_quantity;
    }

    public void setCurrent_total_quantity(Integer current_total_quantity) {
        this.current_total_quantity = current_total_quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isMultichoice() {
        return multichoice;
    }

    public void setMultichoice(boolean multichoice) {
        this.multichoice = multichoice;
    }

    public ArrayList<AddonModel> getList_addon() {
        return list_addon;
    }

    public void setList_addon(ArrayList<AddonModel> list_addon) {
        this.list_addon = list_addon;
    }
}
