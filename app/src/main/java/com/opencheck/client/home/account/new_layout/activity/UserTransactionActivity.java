package com.opencheck.client.home.account.new_layout.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityUserTransactionBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.adapter.UserTransactionAdapter;
import com.opencheck.client.home.account.new_layout.fragment.bought_evoucher.ValidEvoucherFragment;
import com.opencheck.client.home.account.new_layout.model.UserTransaction;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class UserTransactionActivity extends LixiActivity {

    // View
    RelativeLayout relBack, relMessenger;
    RecyclerView recListOrder;
    ProgressBar progressBar;

    // Var
    private int page = 1;
    private Boolean isLoading = false;
    private boolean isOutOfVoucherPage = false;

    // Adapter
    UserTransactionAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<UserTransaction> listOrder;

    private ActivityUserTransactionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_transaction);
        initView();
        getData();
    }


    private void initView(){
        recListOrder = (RecyclerView) findViewById(R.id.recListOrder);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        relBack = (RelativeLayout) findViewById(R.id.rlBack);
        relMessenger = (RelativeLayout) findViewById(R.id.rlFacebookMessenger);

        listOrder = new ArrayList<>();

        relBack.setOnClickListener(this);
        relMessenger.setOnClickListener(this);

        recListOrder.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recListOrder.computeVerticalScrollOffset() > 0) {
                    int visibleItemCount  = linearLayoutManager.getChildCount();
                    int totalItemCount    = linearLayoutManager.getItemCount();
                    int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 3) {
                        Helper.showLog("Load more");
                        isLoading = true;
                        getData();
                    }
                }
            }
        });
    }

    private void setAdapter(){
        adapter = new UserTransactionAdapter(activity, listOrder);
        linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recListOrder.setLayoutManager(linearLayoutManager);
        recListOrder.setAdapter(adapter);
    }

    private void getData(){
        if (!isLoading) {
            if (!isOutOfVoucherPage) {
                progressBar.setVisibility(View.VISIBLE);
                DataLoader.getUserTransaction(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess){
                            if (page == 1){
                                listOrder = (ArrayList<UserTransaction>) object;
//                                setOutOfVoucherPage(listOrder);
                                setAdapter();
                            }else {
                                ArrayList<UserTransaction> listTemp = (ArrayList<UserTransaction>) object;
                                if (listTemp == null || listTemp.size() == 0){
                                    progressBar.setVisibility(View.GONE);
                                    return;
                                }else {
//                                    setOutOfVoucherPage(listTemp);
                                    listOrder.addAll(listTemp);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            page++;
                            isLoading = false;
                        }else {
                            isLoading = true;
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                }, page);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlFacebookMessenger:
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/lixiapp")));
                break;
        }
    }

    private void setOutOfVoucherPage(ArrayList<UserTransaction> list) {
        if (list.size() < DataLoader.record_per_page) {
            isOutOfVoucherPage = true;
        }
    }
}
