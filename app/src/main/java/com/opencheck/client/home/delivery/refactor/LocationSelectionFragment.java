package com.opencheck.client.home.delivery.refactor;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentLocationSelectionBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.activities.NewSearchLocationActivity;
import com.opencheck.client.home.delivery.fragments.DeliveryHomeFragment;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliConfigModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.game.GameActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.LocationHelper;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationSelectionFragment extends LixiFragment {

    private FragmentLocationSelectionBinding mBinding;
    private LocationHelper mLocationHelper;
    private boolean isLixiEnable = false;
    private List<StoreOfCategoryModel> stores;
    private DeliveryHomeFragment deliveryHomeFragment;
    public static boolean deliHomeShowing = false;
    private boolean isRequestLocation = false;
    private int countHandleResult = 0;
    private int countDeepLink = -1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_location_selection, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialiseView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLocationHelper = new LocationHelper(activity);
    }

    //region LOGIC
    private void initialiseView() {

        mBinding.tvRequireAddress.setOnClickListener(this);
        mBinding.searchBar.setOnClickListener(this);
        mBinding.llTurnOnGps.setOnClickListener(this);
        mBinding.llSearch.setOnClickListener(this);
        mBinding.rlChooseAddress.setOnClickListener(this);
        mBinding.rlLoading.setOnClickListener(this);
        mBinding.tvChooseOtherLocation.setOnClickListener(this);

        getDefaultUserConfiguration();
        try {
            mBinding.imgGame.setOnClickListener(this);

            if (ConfigModel.getInstance(activity).getGame_home_button_enable() == 0) {
                mBinding.imgGame.setVisibility(View.GONE);
            } else {
                mBinding.imgGame.setVisibility(View.VISIBLE);
            }

            LixiImage.with(activity)
                    .load(ConfigModel.getInstance(activity).getGame_home_button_icon())
                    .into(mBinding.imgGame);
        } catch (Exception ex) {
            Crashlytics.logException(ex);
        }
    }

    private boolean checkGpsState() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean checkLocationRequestPermission() {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission_group.LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private String convertLocationToAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            if (Geocoder.isPresent()) {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                if (addresses != null && addresses.size() > 0) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder builder = new StringBuilder("");

                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        builder.append(returnedAddress.getAddressLine(i));
                        if (i != returnedAddress.getMaxAddressLineIndex())
                            builder.append(", ");
                    }

                    return builder.toString();
                } else {
                    Log.w("GPS", "No Address returned!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        return null;
    }

    private void showUnavailableLocation() {
        mBinding.rlLoading.setVisibility(View.GONE);
        if (AppPreferenceHelper.getInstance().getDefaultLocation() != null) {
            mBinding.tvDetailMessage.setVisibility(View.VISIBLE);
            mBinding.tvChooseOtherLocation.setVisibility(View.VISIBLE);
            mBinding.tvRequireAddress.setVisibility(View.VISIBLE);
            mBinding.tvUnsupportLocation.setVisibility(View.VISIBLE);
            mBinding.tvUnsupportDeli.setVisibility(View.VISIBLE);
            mBinding.llGps.setVisibility(View.GONE);
            mBinding.tvMessage.setVisibility(View.GONE);
            mBinding.tvMessageFeedback.setVisibility(View.GONE);
            mBinding.tvUnsupportLocation.setText(AppPreferenceHelper.getInstance().getDefaultLocation().getFullAddress());
            mBinding.tvChooseLocation.setText(AppPreferenceHelper.getInstance().getDefaultLocation().getFullAddress());
        }
    }

    private void handleOnAddressResponse() {
        mBinding.rlLoading.setVisibility(View.VISIBLE);
        DeliAddressModel addressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (addressModel == null || addressModel.getFullAddress().isEmpty()) {
            if (AppPreferenceHelper.getInstance().isStartActivityFromDeepLink() && countHandleResult != 1) {
                loadGpsLocation();
            }
            return;
        }

        mBinding.tvChooseLocation.setText(addressModel.getFullAddress());
        getStoresByLocation(addressModel.getLat(), addressModel.getLng(), addressModel.getFullAddress());
    }

    private void requestOnChangeLocation() {
        if (DeliveryHomeFragment.isRefreshLocation) {
            handleOnAddressResponse();
            return;
        }

        if (checkLocationRequestPermission() && isRequestLocation) {
            handleOnAddressResponse();
        } else {
            if (countDeepLink == 0) {
                requestLocation();
                countDeepLink = -1;
            }
        }
    }

    private void requestLocation() {
        mBinding.rlLoading.setVisibility(View.VISIBLE);

        DeliAddressModel addressModel = AppPreferenceHelper.getInstance().getDefaultLocation();
        if (addressModel != null && !addressModel.getFullAddress().isEmpty()) {
            mBinding.tvActiveAddress.setText(addressModel.getFullAddress());
            mBinding.tvChooseLocation.setText(addressModel.getFullAddress());
            getStoresByLocation(addressModel.getLat(), addressModel.getLng(), addressModel.getFullAddress());
            return;
        }

        if (checkGpsState()) {
            loadGpsLocation();
        } else {
            showNoGpsLocation();
        }
    }

    private void showGPSLocation(double lat, double lng, @NonNull String fullAddress) {
        String addressTempStr = fullAddress;
        if (fullAddress.contains("Vietnam"))
            addressTempStr = fullAddress.replace("Vietnam", "Việt Nam");
        mBinding.rlInactiveDeli.setVisibility(View.GONE);
        mBinding.tvActiveAddress.setText(addressTempStr);
        mBinding.tvChooseLocation.setText(addressTempStr);
        String shortcutAddress = addressTempStr.substring(0, addressTempStr.contains(",") ? addressTempStr.indexOf(",") : addressTempStr.length());
        DeliAddressModel addressModel = new DeliAddressModel(addressTempStr, shortcutAddress, lat, lng, true);
        AppPreferenceHelper.getInstance().setDefaultLocation(addressModel);
        DeliDataLoader.checkChangeProvinceHeader(activity);
        getStoresByLocation(lat, lng, addressTempStr);
    }

    private void loadGpsLocation() {
        mLocationHelper.setTimeOut(Integer.MAX_VALUE)
                .requestLocationGoogle(new LocationHelper.LocationCallBack() {
                    @Override
                    public void onResponse(LocationHelper.Task task) {
                        if (task.isSuccess()) {
                            final double lat = task.getLocation().getLatitude();
                            final double lng = task.getLocation().getLongitude();

                            //get address with geocoder server
                            String fullAddress = convertLocationToAddress(lat, lng);
                            if (fullAddress != null && !fullAddress.isEmpty()) {
                                showGPSLocation(lat, lng, fullAddress);
                            } else { //geo coder server is unavailable, get address through api
                                LatLng currentPoint = new LatLng(lat, lng);
                                new LoadAddressTask(new AddressCallback() {
                                    @Override
                                    public void callback(LatLng position, String addressInfo) {
                                        if (addressInfo != null && !addressInfo.isEmpty()) {
                                            showGPSLocation(lat, lng, addressInfo);
                                        }
                                    }
                                }).execute(currentPoint);
                            }
                        } else {
                            showNoGpsLocation();
                        }
                        isRequestLocation = true;
                    }
                });
    }

    private void showNoGpsLocation() {
        isRequestLocation = true;
        mBinding.rlLoading.setVisibility(View.GONE);
        mBinding.rlInactiveDeli.setVisibility(View.GONE);
        if (mBinding.tvChooseLocation.getText().toString().equalsIgnoreCase("Chọn địa chỉ giao hàng")) {
            mBinding.llGps.setVisibility(View.VISIBLE);
            mBinding.tvRequireAddress.setVisibility(View.GONE);
            mBinding.tvDetailMessage.setVisibility(View.GONE);
            mBinding.tvMessage.setText(LanguageBinding.getString(R.string.delivery_choose_location_message_1, getContext()));
        } else {
            mBinding.llGps.setVisibility(View.GONE);
            mBinding.tvRequireAddress.setVisibility(View.VISIBLE);
            mBinding.tvDetailMessage.setVisibility(View.VISIBLE);
            mBinding.tvMessage.setText(LanguageBinding.getString(R.string.delivery_choose_location_message_2, getContext()));
        }

        if (AppPreferenceHelper.getInstance().isStartActivityFromDeepLink()) {
            NewSearchLocationActivity.startSearchLocationActivity(activity, true);
        }
    }

    private void showUnavailableLixiService(String disableMessage) {
        mBinding.rlLoading.setVisibility(View.GONE);
        mBinding.rlInactiveDeli.setVisibility(View.VISIBLE);
        mBinding.tvInactiveMessage.setText(disableMessage);
        mBinding.searchBar.setVisibility(View.GONE);
        mBinding.rlChooseAddress.setVisibility(View.GONE);
    }

    private void showAvailableLixiService() {
        mBinding.rlInactiveDeli.setVisibility(View.GONE);
        mBinding.searchBar.setVisibility(View.VISIBLE);
        DeliAddressModel model = AppPreferenceHelper.getInstance().getDefaultLocation();

        if (model != null) {
            //check available stores and navigate to deli home fragment
            mBinding.tvActiveAddress.setText(model.getFullAddress());
            mBinding.tvChooseLocation.setText(model.getFullAddress());
            getStoresByLocation(model.getLat(), model.getLng(), model.getFullAddress());
        } else {
            requestLocation();
        }
    }

    private void showDeliveryHomeFragment() {
        try {
            deliveryHomeFragment = new DeliveryHomeFragment();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.child_fragment_container, deliveryHomeFragment).commitAllowingStateLoss();
            getChildFragmentManager().executePendingTransactions();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    public DeliveryHomeFragment getDeliveryHomeFragment() {
        return deliveryHomeFragment;
    }
    //endregion

    //region LIFE CYCLE

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mLocationHelper != null)
            mLocationHelper.onRequestPermissionsResult(requestCode, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mLocationHelper != null)
            mLocationHelper.onActivityResult(requestCode, resultCode);
        countHandleResult++;
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);

        if (!deliHomeShowing)
            requestOnChangeLocation();
        else showUnavailableLocation();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (countHandleResult == 1)
            countHandleResult = 0;
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.tvRequireAddress) {
            mBinding.tvMessageFeedback.setVisibility(View.VISIBLE);
            mBinding.tvRequireAddress.setVisibility(View.GONE);
            if (stores != null && !stores.isEmpty()) {
                if (AppPreferenceHelper.getInstance().getDefaultLocation() != null) {
                    showDeliveryHomeFragment();
                }
            } else {
                Toast.makeText(activity, LanguageBinding.getString(R.string.delivery_choose_location_message_location_not_support_toast, getContext()), Toast.LENGTH_SHORT).show();
            }
        } else if (view == mBinding.llTurnOnGps) {
            mBinding.rlLoading.setVisibility(View.VISIBLE);
            loadGpsLocation();
        } else if (view == mBinding.searchBar || view == mBinding.llSearch || view == mBinding.rlChooseAddress || view == mBinding.tvChooseOtherLocation) {
            try {
                AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            NewSearchLocationActivity.startSearchLocationActivity(activity, true);
        } else if (view == mBinding.imgGame) {
            if ((new AppPreferenceHelper(activity)).getAccessToken().equals("")) {
                LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_PLAY_GAME);
            } else {
                Intent intentGame = new Intent(activity, GameActivity.class);
                intentGame.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.HOME);
                startActivity(intentGame);
            }
        }
    }
    //endregion

    //region API
    public void getDefaultUserConfiguration() {
        DeliDataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    AppPreferenceHelper.getInstance().setTimeResetConfig(System.currentTimeMillis());
                    ArrayList<DeliConfigModel> configs = (ArrayList<DeliConfigModel>) object;
                    String disableLixiMessage = "";
                    for (DeliConfigModel model : configs) {
                        if (model.getKey().equals("deli_disable_message")) {
                            disableLixiMessage = model.getValue();
                        } else if (model.getKey().equals("deli_enable") && model.getValue().equals("true")) {
                            isLixiEnable = true;
                        } else if (model.getKey().equals("lixi_phone")) {
                            (new AppPreferenceHelper(activity)).setDefaultPhoneDeli(model.getValue());
                        }
                    }

                    if (isLixiEnable) {
                        showAvailableLixiService();
                    } else {
                        showUnavailableLixiService(disableLixiMessage);
                    }
                } else {
                    mBinding.rlLoading.setVisibility(View.GONE);
                }
            }
        });
    }

    public void recheckUserConfiguration() {
        DeliDataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<DeliConfigModel> configs = (ArrayList<DeliConfigModel>) object;
                    boolean isLixiEnableRecheck = false;
                    String disableLixiMessage = "";

                    for (DeliConfigModel model : configs) {
                        if (model.getKey().equals("deli_disable_message")) {
                            disableLixiMessage = model.getValue();
                        } else if (model.getKey().equals("deli_enable") && model.getValue().equals("true")) {
                            isLixiEnableRecheck = true;
                        } else if (model.getKey().equals("lixi_phone")) {
                            (new AppPreferenceHelper(activity)).setDefaultPhoneDeli(model.getValue());
                        }
                    }

                    if (isLixiEnableRecheck == isLixiEnable)
                        return;

                    isLixiEnable = isLixiEnableRecheck;

                    if (isLixiEnable) {
                        showAvailableLixiService();
                    } else {
                        if (deliHomeShowing) {
                            getChildFragmentManager().beginTransaction().detach(deliveryHomeFragment).commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                            deliHomeShowing = false;
                        }
                        showUnavailableLixiService(disableLixiMessage);
                    }
                } else {
                    mBinding.rlLoading.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getStoresByLocation(double lat, double lng, String fullAddress) {
        DeliDataLoader.getListHot(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    stores = (List<StoreOfCategoryModel>) object;
                    if (stores == null || stores.isEmpty()) {
                        showUnavailableLocation();
                        return;
                    }
                    showDeliveryHomeFragment();
                } else {
                    mBinding.rlLoading.setVisibility(View.GONE);
                    if (mBinding.tvChooseLocation.getText().toString().equals("Chọn địa chỉ giao hàng"))
                        return;
                    showUnavailableLocation();
                }
            }
        }, 1, fullAddress, lat, lng);
    }
    //endregion
}
