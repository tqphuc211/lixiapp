package com.opencheck.client.home.rating.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.RowRatingBinding;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.RatingHolder> {
    private LixiActivity activity;
    private ArrayList<RatingModel> listRating;
    private boolean isLimit;

    public RatingAdapter(LixiActivity activity, ArrayList<RatingModel> listRating, boolean isLimit) {
        this.activity = activity;
        this.listRating = listRating;
        this.isLimit = isLimit;
    }


    @Override
    public RatingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowRatingBinding rowRatingBinding = RowRatingBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new RatingHolder(rowRatingBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(RatingHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        if (isLimit) {
            if (listRating == null) {
                return 0;
            } else {
                if (listRating.size() > 3) {
                    return 3;
                } else {
                    return listRating.size();
                }
            }
        } else {
            return listRating != null ? listRating.size() : 0;
        }
    }

    public class RatingHolder extends RecyclerView.ViewHolder {
        CircleImageView imgAvatar;
        TextView txtName, txtDate, txtContent;
        com.opencheck.client.home.delivery.libs.RatingBar ratingBar;

        public RatingHolder(View itemView) {
            super(itemView);
            imgAvatar = (CircleImageView) itemView.findViewById(R.id.imgAvatarAdapter);
            txtName = (TextView) itemView.findViewById(R.id.txtNameAdapter);
            txtDate = (TextView) itemView.findViewById(R.id.txtDateAdapter);
            txtContent = (TextView) itemView.findViewById(R.id.txtCommentAdapter);
            ratingBar = (com.opencheck.client.home.delivery.libs.RatingBar) itemView.findViewById(R.id.ratingBarAdapter);
        }

        public void bind() {
            int position = getAdapterPosition();
            RatingModel ratingModel = listRating.get(position);
            if (ratingModel != null) {
                ratingBar.setCount((int) ratingModel.getRating());
                txtContent.setText(ratingModel.getComment() == null || ratingModel.getComment().equals("") ? ratingModel.getRating_option_value() : ratingModel.getComment());
                txtDate.setText(Helper.getFormatDateEngISO("dd-MM-yyyy", ratingModel.getCreate_date() + "000"));
                if (ratingModel.getUser_info() != null) {
                    txtName.setText(ratingModel.getUser_info().getName());
                    if (ratingModel.getUser_info().getAvatar() == null || ratingModel.getUser_info().getAvatar().equals("")) {
                        imgAvatar.setImageResource(R.drawable.ic_user_rating_def);
                    } else {
                        ImageLoader.getInstance().displayImage(ratingModel.getUser_info().getAvatar(), imgAvatar);
                    }
                }
            }
        }
    }
}
