package com.opencheck.client.home.delivery.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.opencheck.client.home.delivery.model.DeliAddressModel;

public class HomeLocationViewModel extends ViewModel {
    private MutableLiveData<DeliAddressModel> homeLocation;

    public MutableLiveData<DeliAddressModel> getHomeLocation() {
        if (homeLocation == null){
            homeLocation = new MutableLiveData<>();
        }
        return homeLocation;
    }
}
