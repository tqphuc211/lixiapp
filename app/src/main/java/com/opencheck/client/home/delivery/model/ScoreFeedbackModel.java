package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class ScoreFeedbackModel implements Serializable {
    private long rating;
    private long amount;

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
