package com.opencheck.client.home.delivery.enum_key;

public enum FeeType {
    SAME_FEE, FREE_SHIP, NORMAL
}
