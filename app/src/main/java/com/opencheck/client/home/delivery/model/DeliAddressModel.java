package com.opencheck.client.home.delivery.model;

import com.opencheck.client.utils.Helper;

import java.io.Serializable;

public class DeliAddressModel implements Serializable {
    private String placeId;
    private String fullAddress;
    private String address;
    private double lat;
    private double lng;
    private boolean isGPS = false;
    private String type;
    private long create_date;
    private int user_id;
    private int id;

    public DeliAddressModel(String fullAddress, String address, double lat, double lng) {
        this.fullAddress = fullAddress;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public DeliAddressModel(String fullAddress, String address, double lat, double lng, boolean isGPS) {
        this.fullAddress = fullAddress;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.isGPS = isGPS;
    }

    public DeliAddressModel(String placeId, String fullAddress, String address, double lat, double lng, boolean isGPS) {
        this.placeId = placeId;
        this.fullAddress = fullAddress;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.isGPS = isGPS;
    }

    public DeliAddressModel(String placeId, String fullAddress, String address) {
        this.placeId = placeId;
        this.fullAddress = fullAddress;
        this.address = address;
    }

    public DeliAddressModel() {
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public boolean isGPS() {
        return isGPS;
    }

    public void setGPS(boolean GPS) {
        isGPS = GPS;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(long create_date) {
        this.create_date = create_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegion() {
        try {
            if (compare(fullAddress, "Hồ Chí Minh")) {
                return 65;
            } else if (compare(fullAddress, "Hà Nội")) {
                return 66;
            } else {
                return 0;
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public String getProvince() {
        if (compare(fullAddress, "Hồ Chí Minh")) {
            return PROVINCE_HCM;
        } else if (compare(fullAddress, "Hà Nội")) {
            return PROVINCE_HN;
        } else {
            return PROVINCE_ALL;
        }
    }

    private boolean compare(String source, String pattern) {
        String ct1 = Helper.removeAccent(source)
                .replace(" ", "")
                .toLowerCase();
        String ct2 = Helper.removeAccent(pattern)
                .replace(" ", "")
                .toLowerCase();
        return ct1.contains(ct2);
    }

    public static final String PROVINCE_ALL = "NW";
    public static final String PROVINCE_HCM = "HCM";
    public static final String PROVINCE_HN = "HaNoi";
}
