package com.opencheck.client.home.product.gateway;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.TimeOutLayoutBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;

/**
 * Created by I'm Sugar on 5/24/2018.
 */

public class DialogTransactionTimeout extends LixiTrackingDialog {

    private TextView tvCancel;
    private TextView tvCont;

    public DialogTransactionTimeout(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private TimeOutLayoutBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = TimeOutLayoutBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    private void findViews() {
        tvCancel = findViewById(R.id.tvCancel);
        tvCont = findViewById(R.id.tvCont);
        tvCancel.setOnClickListener(this);
        tvCont.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvCancel){
            onEventOK(true);
            dismiss();
        }
        if (view.getId() == R.id.tvCont){
            onEventOK(false);
            dismiss();
        }

    }


    private void onEventOK(boolean ok) {
        if (eventListener != null) {
            eventListener.onOk(ok);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onOk(boolean success);
    }

    protected DialogTransactionTimeout.IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            DialogTransactionTimeout.IDidalogEvent listener) {
        eventListener = listener;
    }

    @Override
    public void onBackPressed() {
        onEventOK(true);
        dismiss();
    }

}
