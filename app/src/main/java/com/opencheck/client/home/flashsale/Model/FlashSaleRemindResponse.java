package com.opencheck.client.home.flashsale.Model;

import com.opencheck.client.models.LixiModel;

public class FlashSaleRemindResponse extends LixiModel {

    /**
     * id : 0
     * status : string
     */

    private long id;
    private String status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
