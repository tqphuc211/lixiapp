package com.opencheck.client.home.flashsale.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.opencheck.client.R;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.home.flashsale.Model.RemindFlashSale;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.ArrayList;

public class RemindManager extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        long id = intent.getLongExtra(AlarmFlashSale.REMIND_ID, -1);
        long startTime = intent.getLongExtra(AlarmFlashSale.START_TIME, -1);
        String CHANNEL_ID = System.currentTimeMillis() + "";

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification noti;
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }

        ArrayList<RemindFlashSale> listTemp = appPreferenceHelper.getRemind();
        for (int i = 0; i < listTemp.size(); i++){
            try {
                long tempID = listTemp.get(i).getIdFlashSale() * listTemp.get(i).getIdProduct();
                if (id == tempID){
                    Intent intentTemp = new Intent(context, ActivityProductCashEvoucherDetail.class);
                    intentTemp.putExtra(ActivityProductCashEvoucherDetail.PRODUCT_ID, listTemp.get(i).getIdProduct());
                    intentTemp.putExtra(AlarmFlashSale.START_TIME, startTime);

                    Intent backItent = new Intent(context, FirstLaunchActivity.class);

                    Intent[] listItent = new Intent[]{backItent, intentTemp};
                    PendingIntent pIntent = PendingIntent.getActivities(context, (int) System.currentTimeMillis(), listItent, 0);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        noti = new Notification.Builder(context)
                                .setContentTitle("Nhắc nhở")
                                .setContentText("Sắp bắt đầu Flash sale")
                                .setContentIntent(pIntent)
                                .setSmallIcon(R.drawable.icon_app_small_trans)
                                .setAutoCancel(true)
                                .setChannelId(CHANNEL_ID)
                                .build();
                    }else {
                        noti = new Notification.Builder(context)
                                .setContentTitle("Nhắc nhở")
                                .setContentText("Sắp bắt đầu Flash sale")
                                .setContentIntent(pIntent)
                                .setSmallIcon(R.drawable.icon_app_small_trans)
                                .setAutoCancel(true)
                                .build();
                    }

                    notificationManager.notify((int) id, noti);
                    listTemp.remove(i);
                }
            }catch (Exception e){
                return;
            }
        }

        appPreferenceHelper.clearRemind();
        appPreferenceHelper.setRemind(listTemp);
    }
}
