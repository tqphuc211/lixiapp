package com.opencheck.client.home.account.evoucher.Model;

public class PaymentCodBody {

    private long id;

    private long order_id;

    private String payment_method;

    private String phone;

    private int quantity;

    private boolean user_discount;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isUser_discount() {
        return user_discount;
    }

    public void setUser_discount(boolean user_discount) {
        this.user_discount = user_discount;
    }
}
