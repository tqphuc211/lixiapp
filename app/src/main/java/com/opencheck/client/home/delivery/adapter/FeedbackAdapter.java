package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.databinding.FeedbackItemLayoutBinding;
import com.opencheck.client.databinding.HeaderFeedbackLayoutBinding;

import java.util.ArrayList;

public class FeedbackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<String> arrComment;
    private int HEADER = 0;
    private int BODY = 1;

    public FeedbackAdapter(LixiActivity activity, ArrayList<String> arrComment) {
        this.activity = activity;
        this.arrComment = arrComment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            HeaderFeedbackLayoutBinding headerFeedbackLayoutBinding =
                    HeaderFeedbackLayoutBinding.inflate(LayoutInflater.from(
                            parent.getContext()), parent, false);
            return new HeadHolder(headerFeedbackLayoutBinding.getRoot());
        } else if (viewType == BODY) {
            FeedbackItemLayoutBinding feedbackItemLayoutBinding =
                    FeedbackItemLayoutBinding.inflate(LayoutInflater.from(
                            parent.getContext()), parent, false);
            return new BodyHoler(feedbackItemLayoutBinding.getRoot());
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == HEADER) {

        } else if (getItemViewType(position) == BODY) {

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEADER;
        else
            return BODY;
    }

    @Override
    public int getItemCount() {
        return arrComment.size();
    }

    class HeadHolder extends RecyclerView.ViewHolder {

        public HeadHolder(View itemView) {
            super(itemView);
        }
    }

    class BodyHoler extends RecyclerView.ViewHolder {

        public BodyHoler(View itemView) {
            super(itemView);
        }
    }
}
