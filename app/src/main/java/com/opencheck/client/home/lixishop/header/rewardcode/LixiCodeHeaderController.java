package com.opencheck.client.home.lixishop.header.rewardcode;

import android.graphics.Typeface;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.user.LixiCodeBarnerModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class LixiCodeHeaderController implements View.OnClickListener {

    LixiActivity activity;
    View rootView;

    private RelativeLayout rl_logo;
    private ImageView iv;
    private RelativeLayout rl_input_code;
    private EditText et_code;
    private TextView tv_input;
    private TextView tv_zero_code;
    private TextView tv;
    private LinearLayout ll_input_code;
    private TextView tv_lixi_value;
    private boolean isEmpty;

    LixiCodeBarnerModel barner;

    LixiCodeHeaderController(LixiActivity activity, View rootView, boolean isEmpty) {
        this.activity = activity;
        this.rootView = rootView;
        this.isEmpty = isEmpty;
        findView();
    }

    private void findView() {
        rl_logo = (RelativeLayout) rootView.findViewById(R.id.rl_logo);
        iv = (ImageView) rootView.findViewById(R.id.iv);
        rl_input_code = (RelativeLayout) rootView.findViewById(R.id.rl_input_code);
        et_code = (EditText) rootView.findViewById(R.id.et_code);
        tv_input = (TextView) rootView.findViewById(R.id.tv_input);
        tv = (TextView) rootView.findViewById(R.id.tv);
        tv_zero_code = (TextView) rootView.findViewById(R.id.tv_zero_code);
        ll_input_code = (LinearLayout) rootView.findViewById(R.id.ll_input_code);
        tv_lixi_value = (TextView) rootView.findViewById(R.id.tv_lixi_value);

        if (UserModel.getInstance() != null)
            tv_lixi_value.setText(Helper.getVNCurrency(Long.parseLong(UserModel.getInstance().getAvailable_point())) + "đ");

        if (!isEmpty) {
            tv_zero_code.setVisibility(View.GONE);
        } else {
            tv_zero_code.setVisibility(View.VISIBLE);
        }

        et_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_code.getText().toString().length() > 0) {
                    tv_input.setEnabled(true);
                    tv_input.setOnClickListener(LixiCodeHeaderController.this);
                } else {
                    tv_input.setEnabled(false);
                    tv_input.setOnClickListener(null);
                }
            }
        });
    }

    public void setData(LixiCodeBarnerModel barner) {
        this.barner = barner;

        ImageLoader.getInstance().displayImage(barner.getImage(), iv, LixiApplication.getInstance().optionsNomal);
        tv.setText(barner.getText());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_input:
                inputCode();
                break;
        }
    }


    public void inputCode() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("reward_code", et_code.getText().toString());

            DataLoader.useRewardCode(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        JsonObject jsonRecords = (JsonObject) object;
                        String title = jsonRecords.get("title").getAsString();
                        String mess = jsonRecords.get("message").getAsString();
                        JsonArray data = null;
                        SpannableStringBuilder messageBuilder = new SpannableStringBuilder();
                        data = jsonRecords.get("data").getAsJsonArray();
                        if (data != null) {
                            int st = 0;
                            for (int i = 0; i < data.size(); i++) {
                                int pos = mess.indexOf("{" + i + "}");
                                if (pos > 0) {
                                    messageBuilder.append(mess.substring(st, pos));
                                    st = pos + 2 + (i > 9 ? 2 : 1);
                                    SpannableString redSpannable = new SpannableString(data.get(i).getAsString());
                                    redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.app_violet)), 0, data.get(i).getAsString().length(), 0);
                                    redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, data.get(i).getAsString().length(), 0);
                                    messageBuilder.append(redSpannable);
                                }
                            }
                            if (st < mess.length() - 1)
                                messageBuilder.append(mess.substring(st, mess.length()));
                        } else
                            messageBuilder.append(mess);
                        RewardDialog dialogss = new RewardDialog(activity, false, true, false);
                        dialogss.show();
                        dialogss.setData(title, "", messageBuilder);
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                    }
                }
            }, jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

