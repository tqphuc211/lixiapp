package com.opencheck.client.home.product.Control;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class OperateBase64 {
    public static String encode(String text){
        byte[] encrpt = new byte[0];
        try {
            encrpt = text.getBytes("UTF-8");
            return Base64.encodeToString(encrpt, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decode(String base64){
        byte[] decrypt= Base64.decode(base64, Base64.DEFAULT);
        try {
            String text = new String(decrypt, "UTF-8");
            return text;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
