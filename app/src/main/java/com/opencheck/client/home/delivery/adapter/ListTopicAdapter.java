package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowTopicBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.StoreTopicModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class ListTopicAdapter extends RecyclerView.Adapter<ListTopicAdapter.TopicHolder> {
    private LixiActivity activity;
    private ArrayList<StoreTopicModel> listTopic;

    public ListTopicAdapter(LixiActivity activity, ArrayList<StoreTopicModel> listTopic) {
        this.activity = activity;
        this.listTopic = listTopic;
    }

    @Override
    public TopicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowTopicBinding mBinding = RowTopicBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new TopicHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(TopicHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listTopic == null ? 0 : listTopic.size();
    }

    public void setData(ArrayList<StoreTopicModel> list) {
        this.listTopic = list;
        notifyDataSetChanged();
    }

    public class TopicHolder extends RecyclerView.ViewHolder {
        private RowTopicBinding mBinding;

        public TopicHolder(RowTopicBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StoreDetailActivity.startStoreDetailActivity(activity, listTopic.get(getAdapterPosition()).getId(), null);
                }
            });
        }

        public void bind() {
            final StoreTopicModel topic = listTopic.get(getAdapterPosition());
            LixiImage.with(activity)
                    .load(topic.getLogo_link())
                    .size(ImageSize.AUTO)
                    .into(mBinding.ivAvatar);

            mBinding.tvName.setText(topic.getName());
            mBinding.tvTime.setText(topic.getShipping_duration_min() + "-" + topic.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
            if (topic.getShipping_distance() >= 100) {
                double distance = (double) Math.round(((float) topic.getShipping_distance() / 1000) * 10) / 10;
                mBinding.tvDistance.setText(String.valueOf(distance) + " km");
            } else
                mBinding.tvDistance.setText(String.valueOf(topic.getShipping_distance()) + " m");

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < topic.getList_tag().size(); i++) {
                builder.append("#").append(topic.getList_tag().get(i).getName());
                if (i < topic.getList_tag().size() - 1)
                    builder.append(".");
            }
            mBinding.tvTags.setText(builder.toString());
            if (topic.getMin_money_to_free_ship_price() == null)
                mBinding.tvShippingFee.setVisibility(View.GONE);
            else {
                mBinding.tvShippingFee.setVisibility(View.VISIBLE);
                mBinding.tvShippingFee.setText(LanguageBinding.getString(R.string.store_detail_free_ship, activity));
            }

            if (topic.getPromotion_text() != null && topic.getPromotion_text().length() > 0) {
                mBinding.llPromotion.setVisibility(View.VISIBLE);
                mBinding.tvPromotion.setText(topic.getPromotion_text());
            } else {
                mBinding.llPromotion.setVisibility(View.GONE);
            }

            mBinding.vTop.setVisibility(View.GONE);
            mBinding.vLeft.setVisibility(View.GONE);
            mBinding.vRight.setVisibility(View.GONE);

            int height = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_1)) / 32 * 10;
            ViewGroup.LayoutParams params = mBinding.ivAvatar.getLayoutParams();
            params.height = height;
            params.width = 16 * height / 9;
            mBinding.ivAvatar.setLayoutParams(params);
        }
    }
}
