package com.opencheck.client.home.product.cart_new;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ProductPaymentPopupBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.flashsale.Global.FlashSaleTimerController;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.merchant.DiscountModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class PopupPayment extends LixiTrackingDialog {

    public PopupPayment(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_CART_STEP_2;
    }

    //region VIEW
    private ImageView iv_back;
    private LinearLayout flash_sale;
    private RecyclerView rv_payment_method;
    private TextView tv_lixi_exist;
    private CheckBox cb_use_lixi;
    private ImageView iv_tick_min;
    private TextView tv_min_message;
    private ImageView iv_tick_total;
    private TextView tv_total_message;
    private TextView tv_price;
    private TextView tv_lixi;
    private TextView tv_next;

    private void findViews() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        flash_sale = (LinearLayout) findViewById(R.id.flash_sale);
        rv_payment_method = (RecyclerView) findViewById(R.id.rv_payment_method);
        tv_lixi_exist = (TextView) findViewById(R.id.tv_lixi_exist);
        cb_use_lixi = (CheckBox) findViewById(R.id.cb_use_lixi);
        iv_tick_min = (ImageView) findViewById(R.id.iv_tick_min);
        tv_min_message = (TextView) findViewById(R.id.tv_min_message);
        iv_tick_total = (ImageView) findViewById(R.id.iv_tick_total);
        tv_total_message = (TextView) findViewById(R.id.tv_total_message);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_lixi = (TextView) findViewById(R.id.tv_lixi);
        tv_next = (TextView) findViewById(R.id.tv_next);

        mBinding.linearCart.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        cb_use_lixi.setOnClickListener(this);
        tv_next.setOnClickListener(this);
        cb_use_lixi.setEnabled(false);
    }


    //endregion

    private FlashSaleTimerController fsControler;
    private AdapterPaymentMethod adapter;

    private ProductCashVoucherDetailModel productModel;
    private int countProduct = 1;
    private DiscountModel discountModel;
    private boolean isUseLixiDiscount = false;

    private PaymentMethodModel paymentMethod;

    private ProductPaymentPopupBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ProductPaymentPopupBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    //region SET DATA
    public void updateFsTimer(int[] arrTime) {
        if (fsControler != null)
            fsControler.showTime(arrTime);
    }

    public void setData(ProductCashVoucherDetailModel productModel, int sl) {
        this.productModel = productModel;
        this.countProduct = sl;

        UserModel userModel = UserModel.getInstance();
        if (userModel != null) {
            tv_lixi_exist.setText(Helper.getVNCurrency(Long.parseLong(userModel.getAvailable_point())) + " Lixi");
            getLixiDiscount();
            showPrice();

            showPaymentMethod();
        }
        if (productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {
            fsControler = new FlashSaleTimerController(activity, flash_sale);
        }
        id = this.productModel.getId();
        trackScreen();
    }
    //endregion

    //region SHOW DATA
    private void showPaymentMethod() {
        adapter = new AdapterPaymentMethod(activity, (ArrayList<PaymentMethodModel>) productModel.getPayment_method());
        rv_payment_method.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_payment_method.setAdapter(adapter);
        rv_payment_method.setNestedScrollingEnabled(false);
    }

    public void showPrice() {
        long lixi_discount = 0;
        if (discountModel != null)
            lixi_discount = discountModel.getDiscount();

        long price = countProduct * productModel.getPayment_discount_price();
        long lixi = countProduct * productModel.getCashback_price();

        if (productModel.getFlash_sale_id() > 0) {
            price = countProduct * productModel.getFlash_sale_info().getPayment_discount_price();
            lixi = countProduct * productModel.getFlash_sale_info().getCashback_price();
        }

        tv_price.setText(Helper.getVNCurrency(price - (isUseLixiDiscount ? lixi_discount : 0)) + "đ");
        tv_lixi.setText("+" + Helper.getVNCurrency(lixi) + " Lixi");
        if (lixi > 0)
            tv_lixi.setVisibility(View.VISIBLE);
        else
            tv_lixi.setVisibility(View.GONE);
    }

    public void showDiscountInfo() {
        if (discountModel.getDiscount() > 0) {
            cb_use_lixi.setEnabled(true);
            cb_use_lixi.setTextColor(activity.getResources().getColor(R.color.black));
            cb_use_lixi.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_use_lixi_discount, activity), Helper.getVNCurrency(discountModel.getDiscount())));
        } else cb_use_lixi.setEnabled(false);

        if (discountModel.getInfo().get(0).isDone()) {
            iv_tick_min.setBackground(activity.getDrawable(R.drawable.icon_check_big_green));
            tv_min_message.setText(discountModel.getInfo().get(0).getText());
            tv_min_message.setTextColor(activity.getResources().getColor(R.color.black));
        } else {
            iv_tick_min.setBackground(activity.getDrawable(R.drawable.icon_check_big_grey));
            tv_min_message.setText(discountModel.getInfo().get(0).getText());
            tv_min_message.setTextColor(activity.getResources().getColor(R.color.gray));
        }

        if (discountModel.getInfo().get(1).isDone()) {
            iv_tick_total.setBackground(activity.getDrawable(R.drawable.icon_check_big_green));
            tv_total_message.setText(discountModel.getInfo().get(1).getText());
            tv_total_message.setTextColor(activity.getResources().getColor(R.color.black));
        } else {
            iv_tick_total.setBackground(activity.getDrawable(R.drawable.icon_check_big_grey));
            tv_total_message.setText(discountModel.getInfo().get(1).getText());
            tv_total_message.setTextColor(activity.getResources().getColor(R.color.gray));
        }

    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                dismiss();
                break;
            case R.id.cb_use_lixi:
                if (cb_use_lixi.isChecked()) {
                    isUseLixiDiscount = true;
                    cb_use_lixi.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_used_lixi_discount, activity), Helper.getVNCurrency(discountModel.getDiscount())));
                }
                else {
                    isUseLixiDiscount = false;
                    cb_use_lixi.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_use_lixi_discount, activity), Helper.getVNCurrency(discountModel.getDiscount())));
                }
                showPrice();
                break;
            case R.id.linearCart:
                dismiss();
                break;
            case R.id.tv_next:
                //TODO check payment method null
                if (eventListener != null) {
                    if (adapter.getPaymentMethod() == null) {
                        Toast.makeText(activity, LanguageBinding.getString(R.string.cash_evoucher_please_choose_payment, activity), Toast.LENGTH_SHORT).show();
                    } else {
                        long price = countProduct * productModel.getPayment_discount_price();
                        long lixi = countProduct * productModel.getCashback_price();

                        if (productModel.getFlash_sale_id() > 0) {
                            price = countProduct * productModel.getFlash_sale_info().getPayment_discount_price();
                            lixi = countProduct * productModel.getFlash_sale_info().getCashback_price();
                        }


                        eventListener.onNext(isUseLixiDiscount, discountModel == null ? 0 : discountModel.getDiscount(), adapter.getPaymentMethod(), price, lixi, adapter.isSaveCard());
                    }
                }
                break;
        }
    }

    //region Load Data
    public void getLixiDiscount() {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", productModel.getId());
        obj.addProperty("quantity", countProduct);

        DataLoader.postLixiDiscount(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    discountModel = (DiscountModel) object;
                    showDiscountInfo();
                }
            }
        }, obj);

    }
    //endregion

    public interface PaymentEventListener {
        void onNext(boolean isUserLixiDiscount, long lixiDiscount, PaymentMethodModel paymentMethod, long payPrice, long lixiCasback, boolean isSaveCard);
    }

    public void setPaymentEventListener(PaymentEventListener eventListener) {
        this.eventListener = eventListener;
    }

    private PaymentEventListener eventListener;

    public void notifyDataSetChanged(){
        adapter.notifyDataSetChanged();
    }
}
