package com.opencheck.client.home.delivery.adapter;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FavoriteStoreItemLayoutBinding;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class FavoriteStoreAdapter extends RecyclerView.Adapter<FavoriteStoreAdapter.ViewHolder>{

    private LixiActivity activity;
    private ArrayList<StoreOfCategoryModel> listStore;
    private ClickListener listener;

    public void setListStore(ArrayList<StoreOfCategoryModel> listStore) {
        if(listStore != null) {
            this.listStore.addAll(listStore);
            notifyDataSetChanged();
        }
    }

    public void deleteStore(int pos) {
        if(pos < listStore.size()) {
            listStore.remove(pos);
        }
    }

    public ArrayList<StoreOfCategoryModel> getListStore() {
        return listStore;
    }

    public FavoriteStoreAdapter(LixiActivity activity, ArrayList<StoreOfCategoryModel> listStore, ClickListener listener) {
        this.activity = activity;
        this.listStore = listStore;
        this.listener = listener;
        this.listStore = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FavoriteStoreItemLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.favorite_store_item_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final StoreOfCategoryModel store = listStore.get(position);

        LixiImage.with(activity)
                .load(store.getCover_link())
                .size(ImageSize.AUTO)
                .into(holder.binding.ivAvatar);

        holder.binding.tvName.setText(store.getName());
        holder.binding.tvTime.setText(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
        if (store.getShipping_distance() >= 100) {
            double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
            holder.binding.tvDistance.setText(String.valueOf(distance) + " km");
        } else
            holder.binding.tvDistance.setText(String.valueOf(store.getShipping_distance()) + " m");

        if (store.getPromotion_text() != null && store.getPromotion_text().length() > 0) {
            holder.binding.llPromotion.setVisibility(View.VISIBLE);
            holder.binding.tvPromotion.setText(store.getPromotion_text());
        } else {
            holder.binding.llPromotion.setVisibility(View.GONE);
        }

        StringBuilder builder = new StringBuilder();
        if(store.getList_tag().size() > 0) {
            holder.binding.tvTags.setVisibility(View.VISIBLE);
        } else {
            holder.binding.tvTags.setVisibility(View.GONE);
        }
        for (int i = 0; i < store.getList_tag().size(); i++) {
            builder.append("#").append(store.getList_tag().get(i).getName());
            if (i < store.getList_tag().size() - 1)
                builder.append(".");
        }
        holder.binding.tvTags.setText(builder.toString());

        if (store.isIs_opening()) {
            holder.binding.tvOpenTime.setVisibility(View.GONE);
            holder.binding.ivStoreClose.setVisibility(View.GONE);
            holder.binding.ivAvatar.setColorFilter(null);
            holder.binding.tvShippingFee.setVisibility(View.VISIBLE);
        } else {
            holder.binding.tvOpenTime.setText(String.format(LanguageBinding.getString(R.string.open_close_time, activity), store.getOpen_time_text()));
            holder.binding.tvOpenTime.setVisibility(View.VISIBLE);
            holder.binding.ivStoreClose.setVisibility(View.VISIBLE);
            holder.binding.ivAvatar.setColorFilter(Color.parseColor("#CC000000"));
            holder.binding.tvShippingFee.setVisibility(View.GONE);
        }

        if (store.getMin_money_to_free_ship_price() == null)
            holder.binding.tvShippingFee.setVisibility(View.GONE);
        else {
            holder.binding.tvShippingFee.setVisibility(View.VISIBLE);
            holder.binding.tvShippingFee.setText(LanguageBinding.getString(R.string.store_detail_free_ship, activity));
        }

        int width = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_1)) / 25 * 10;
        ViewGroup.LayoutParams params = holder.binding.ivAvatar.getLayoutParams();
        params.width = width;
        params.height = 3 * width / 4;
        holder.binding.ivAvatar.setLayoutParams(params);
        holder.binding.tvOpenTime.setMaxWidth(width);

        holder.binding.ivDeleteFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteItem(store, position);
            }
        });

        holder.binding.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(store, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listStore == null ? 0 : listStore.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        FavoriteStoreItemLayoutBinding binding;

        public ViewHolder(FavoriteStoreItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ClickListener {
        void onClick(StoreOfCategoryModel store, int pos);
        void onDeleteItem(StoreOfCategoryModel store, int pos);
    }
}
