package com.opencheck.client.home.account.new_layout.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowEvoucherItemComboBinding;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.account.new_layout.model.EvoucherInfo;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class EvoucherItemAdapter extends RecyclerView.Adapter<EvoucherItemAdapter.EvoucherItemHolder> {
    // Constructor
    private LixiActivity activity;
    private ArrayList<EvoucherInfo> listEvoucher;

    public EvoucherItemAdapter(LixiActivity activity, ArrayList<EvoucherInfo> listEvoucher) {
        this.activity = activity;
        this.listEvoucher = listEvoucher;
    }

    @Override
    public EvoucherItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowEvoucherItemComboBinding rowEvoucherItemComboBinding =
                RowEvoucherItemComboBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new EvoucherItemHolder(rowEvoucherItemComboBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(EvoucherItemHolder holder, int position) {
        EvoucherInfo evoucherInfo = listEvoucher != null ? listEvoucher.get(position) : null;
        if (evoucherInfo != null){
            if (evoucherInfo.isSend_pin()){
                holder.txtCode.setText(evoucherInfo.getPin());
            }else {
                holder.txtCode.setText(activity.getString(R.string.bought_evoucher_click_to_show_code));
            }
            holder.txtDate.setText(Helper.getFormatDateEngISO("dd/MM/yyyy", evoucherInfo.getExpired_time() * 1000 + ""));
            holder.txtPrice.setText(Helper.getVNCurrency(evoucherInfo.getPrice()) + activity.getString(R.string.p));
        }
    }

    private OnItemClickListener onItemClickListener;
    public void addOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return listEvoucher != null ? listEvoucher.size() : 0;
    }

    public class EvoucherItemHolder extends RecyclerView.ViewHolder {
        TextView txtCode, txtDate, txtPrice;

        public EvoucherItemHolder(View itemView) {
            super(itemView);
            txtCode = (TextView) itemView.findViewById(R.id.txtCode);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    Bundle data = new Bundle();
                    data.putLong(ConstantValue.EVOUCHER_ID, listEvoucher.get(getAdapterPosition()).getId());
                    data.putParcelableArrayList(ConstantValue.LIST_EVOUCHER_INFO,(ArrayList<? extends Parcelable>) listEvoucher);
                    data.putInt(ConstantValue.CURRENT_POSITION, getAdapterPosition());

                    if (ConstantValue.USER_MULTI_ACTIVE)
                        intent = new Intent(activity, EVoucherAccountActivity.class);
                    else
                        intent = new Intent(activity, ActiveCodeActivity.class);
                    intent.putExtras(data);
                    activity.startActivity(intent);
                }
            });
        }
    }
}
