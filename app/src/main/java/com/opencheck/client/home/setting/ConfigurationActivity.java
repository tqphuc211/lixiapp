package com.opencheck.client.home.setting;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ConfigurationActivityBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.home.newlogin.view.SwitchButton;
import com.opencheck.client.home.setting.dialog.ChooseLanguageDialog;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.LocaleHelper;

import org.json.JSONException;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class ConfigurationActivity extends LixiActivity implements SwitchButton.OnCheckChangeListener {

    private String currentLanguage = "vi";
    private String version = "0";
    private final int COLOR_ALPHA = Color.parseColor("#9b9b9b");
    private final int COLOR_BLACK = Color.parseColor("#000000");

    private ConfigurationActivityBinding mBinding;
    private AppPreferenceHelper appPreferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.configuration_activity);
        appPreferenceHelper = new AppPreferenceHelper(activity);
        this.initData();
    }

    private void initData() {
        setUpSetting();
        mBinding.rlBack.setOnClickListener(this);
        mBinding.linearActivePin.setOnClickListener(this);
        mBinding.linearLanguage.setOnClickListener(this);
        mBinding.linearFingerPrint.setOnClickListener(this);
        mBinding.linearLanguage.setOnClickListener(this);
        mBinding.switchPin.setOnCheckChangeListener(this);
        mBinding.switchFinger.setOnClickListener(this);
        try {
            version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
            mBinding.txtVersion.setText(String.format(LanguageBinding.getString(R.string.user_config_version, activity), version));
        }catch (Exception e){
            e.printStackTrace();
        }
        checkUpToDate();
    }

    private void setUpSetting() {
        mBinding.switchPin.post(new Runnable() {
            @Override
            public void run() {
                mBinding.switchPin.setCheck(appPreferenceHelper.isPinRequired());
                setTextColor(mBinding.switchPin.isCheck(), mBinding.txtPinContent);
            }
        });

        mBinding.switchFinger.post(new Runnable() {
            @Override
            public void run() {
                setTextColor(mBinding.switchFinger.isCheck(), mBinding.txtFingerContent);
            }
        });

        setTextColor(appPreferenceHelper.isPinRequired(), mBinding.txtActivePin);

        if (LanguageBinding.getString(R.string.current_language, activity).equals(ConstantValue.LANG_CONFIG_VN) ||
                LanguageBinding.getString(R.string.current_language, activity).equals(ConstantValue.LANG_CONFIG_DEF)){
            mBinding.txtLang.setText(LanguageBinding.getString(R.string.user_config_lang_vn, activity));
        }else {
            mBinding.txtLang.setText(LanguageBinding.getString(R.string.user_config_lang_en, activity));
        }
    }

    private void setTextColor(boolean check, TextView txt){
        if (check){
            txt.setTextColor(COLOR_BLACK);
        }else {
            txt.setTextColor(COLOR_ALPHA);
        }
    }

    private void checkUpToDate() {
//        try {
//            int minimumVersion;
//            minimumVersion = ConfigModel.getInstance(this).getAndroidLixiUpdate();
//            int currentVersion;
//            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            currentVersion = pInfo.versionCode;
//            if (minimumVersion <= currentVersion) {
//                mBinding.txtLastestVrs.setVisibility(View.VISIBLE);
//            }
//            else {
//                mBinding.txtLastestVrs.setVisibility(View.GONE);
//            }
//        } catch (Exception e) {
//            if (e instanceof JSONException) {
//                Helper.showErrorDialog(activity, "Cannot get minimum version");
//            }
//            if (e instanceof PackageManager.NameNotFoundException) {
//                Helper.showErrorDialog(activity, "Cannot get current version");
//            }
//        }
        // check latest version
        int latestVersion = ConfigModel.getInstance(activity).getAndroid_lastest_version();
        int currentVersion = BuildConfig.VERSION_CODE;

        if(currentVersion < latestVersion){
            mBinding.txtLastestVrs.setText(LanguageBinding.getString(R.string.user_config_already_have_new_version_please_update, activity));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        String str = LocaleHelper.getLanguage(activity);
        if(str == null)
            return;
        if(str.contains(currentLanguage))
            return;
        currentLanguage = str;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.linearActivePin:
                if (appPreferenceHelper.isPinRequired()) {
                    Bundle data = new Bundle();
                    data.putString(ConstantValue.ACTION_FOR_PIN, ConstantValue.ACTION_CHANGE_PIN);
                    Intent intent = new Intent(activity, LoginPinCodeActivity.class);
                    intent.putExtras(data);
                    startActivityForResult(intent, ConstantValue.PIN_REQUEST_CODE);
                }
                break;
            case R.id.switchFinger:
            case R.id.linearFingerPrint:
                Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.user_config_not_support, activity));
                break;
            case R.id.linearLanguage:
                showLanguageDialog();
                break;
        }

    }

    ChooseLanguageDialog dialog;
    private void showLanguageDialog(){
        if (dialog == null){
            dialog = new ChooseLanguageDialog(activity);
        }

        dialog.show();
        dialog.addOnItemClick(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (!action.equals(mBinding.txtLang.getText().toString())) {
                    mBinding.txtLang.setText(action);
                    DataLoader.initAPICall(activity, appPreferenceHelper.getUserToken(), null);
                    Intent intent = new Intent(ConfigurationActivity.this, FirstLaunchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void onChanged(View v, boolean check) {
        switch (v.getId()){
            case R.id.switchPin:
                if (!check){
                    Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.user_config_turn_off_pin, activity));
                    (new AppPreferenceHelper(activity)).setPinRequired(false);
                }else {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantValue.ACTION_FOR_PIN, ConstantValue.ACTION_TURN_ON_PIN);
                    Intent pinactivity = new Intent(activity, LoginPinCodeActivity.class);
                    pinactivity.putExtras(bundle);
                    startActivityForResult(pinactivity, ConstantValue.PIN_REQUEST_CODE);
                }
                setTextColor(check, mBinding.txtPinContent);
                setTextColor(appPreferenceHelper.isPinRequired(), mBinding.txtActivePin);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case ConstantValue.PIN_REQUEST_CODE:
                mBinding.switchPin.setCheck(appPreferenceHelper.isPinRequired());
                setTextColor(appPreferenceHelper.isPinRequired(), mBinding.txtPinContent);
                if (resultCode == RESULT_OK){
                    setTextColor(appPreferenceHelper.isPinRequired(), mBinding.txtActivePin);
                    Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.user_config_turn_on_pin, activity));
                }
                break;
        }
    }
}
