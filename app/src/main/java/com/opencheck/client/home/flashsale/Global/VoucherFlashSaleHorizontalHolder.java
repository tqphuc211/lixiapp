package com.opencheck.client.home.flashsale.Global;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

public class VoucherFlashSaleHorizontalHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static VoucherFlashSaleHorizontalHolder newHolder(LixiActivity activity, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_flash_sale_item_home, parent, false);
        VoucherFlashSaleHorizontalHolder flashSaleHolder = new VoucherFlashSaleHorizontalHolder(view);
        flashSaleHolder.activity = activity;
        flashSaleHolder.itemView.setOnClickListener(flashSaleHolder);
        return flashSaleHolder;
    }

    public static void bindViewHolder(VoucherFlashSaleHorizontalHolder voucherHolder, LixiShopEvoucherModel voucher) {
        voucherHolder.voucherModel = voucher;
        if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0)
            ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgBanner, LixiApplication.getInstance().optionsNomal);
        else {
            if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0)
                ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgBanner, LixiApplication.getInstance().optionsNomal);
        }
        voucherHolder.txtName.setText(voucher.getName());

        FlashSaleInfo voucherfs = voucher.getFlash_sale_info();
        voucherHolder.txtSalePrice.setText(Helper.getVNCurrency(voucherfs.getPayment_discount_price()) + "đ");

        if (voucherfs.getQuantity_order_done() >= voucherfs.getQuantity_limit()) {
            voucherHolder.txtSoldOut.setVisibility(View.VISIBLE);
            voucherHolder.progSold.setVisibility(View.GONE);
            voucherHolder.txtSold.setVisibility(View.GONE);
        } else {
            voucherHolder.txtSoldOut.setVisibility(View.GONE);
            voucherHolder.progSold.setVisibility(View.VISIBLE);
            voucherHolder.txtSold.setVisibility(View.VISIBLE);
            voucherHolder.txtSold.setText("ĐÃ BÁN " + voucherfs.getQuantity_order_done());
            voucherHolder.progSold.setProgress(voucherfs.getQuantity_order_done() * 100 / voucherfs.getQuantity_limit());
        }

        if (voucherfs.getPayment_price() != voucherfs.getPayment_discount_price()) {
            voucherHolder.frameSaleOff.setVisibility(View.VISIBLE);
            voucherHolder.frameLixi.setVisibility(View.GONE);
            int percent = 100 - (int) (voucherfs.getPayment_discount_price() * 100 / voucherfs.getPayment_price());
            voucherHolder.txtSaleOff.setText(percent + "%");
        } else voucherHolder.frameSaleOff.setVisibility(View.GONE);

        if (voucherfs.getCashback_price() > 0) {
            voucherHolder.frameLixi.setVisibility(View.VISIBLE);
            voucherHolder.frameSaleOff.setVisibility(View.GONE);

            voucherHolder.txtLixi.setText("+" + Helper.getVNCurrency(voucherfs.getCashback_price()));
        }
    }

    private LixiActivity activity;
    private LixiShopEvoucherModel voucherModel;

    private TextView txtName, txtSalePrice, txtSold, txtSoldOut, txtSaleOff, txtLixi, txtSaling;
    private ImageView imgBanner;
    private RelativeLayout frameSaleOff, frameLixi;
    private ProgressBar progSold;

    private String source = "";

    public VoucherFlashSaleHorizontalHolder(View itemView) {
        super(itemView);

        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtSalePrice = (TextView) itemView.findViewById(R.id.txtSalePrice);
        txtSold = (TextView) itemView.findViewById(R.id.txtSold);
        txtSoldOut = (TextView) itemView.findViewById(R.id.txtSoldOut);
        txtSaleOff = (TextView) itemView.findViewById(R.id.txtSaleOff);
        txtLixi = (TextView) itemView.findViewById(R.id.txtLixi);
        txtSaling = (TextView) itemView.findViewById(R.id.txtSaling);
        txtSaling.setVisibility(View.GONE);

        imgBanner = (ImageView) itemView.findViewById(R.id.imgBanner);
        frameSaleOff = (RelativeLayout) itemView.findViewById(R.id.frameSaleOff);
        frameLixi = (RelativeLayout) itemView.findViewById(R.id.frameLixi);

        progSold = (ProgressBar) itemView.findViewById(R.id.progSold);

        if (activity instanceof HomeActivity) {
            source = TrackingConstant.ContentSource.HOME;
        }
    }

    @Override
    public void onClick(View v) {
        if (voucherModel != null)
            ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, voucherModel.getId(), source);
    }
}
