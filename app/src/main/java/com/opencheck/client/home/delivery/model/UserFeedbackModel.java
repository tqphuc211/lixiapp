package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.LixiModel;

import java.io.Serializable;

public class UserFeedbackModel extends LixiModel implements Serializable {
    private String user_name;
    private String message;
    private long create_date;
    private String user_avatar;
    private long user_order_feedback_id;
    private long user_id;
    private long rating;

    public long getUser_order_feedback_id() {
        return user_order_feedback_id;
    }

    public void setUser_order_feedback_id(long user_order_feedback_id) {
        this.user_order_feedback_id = user_order_feedback_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(long create_date) {
        this.create_date = create_date;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }
}
