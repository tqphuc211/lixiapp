package com.opencheck.client.home.survey;

import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;
import com.opencheck.client.models.merchant.survey.SurveyUserAnswerModel;

public abstract class QuestionAnswerController {
    LixiActivity activity;
    View parent;
    View rootView;
    SurveyQuestionModel quest;

    abstract SurveyUserAnswerModel getAnswer();
}
