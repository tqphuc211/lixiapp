package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class OpenTimeModel implements Serializable{
    private int weekday;
    private long open_time;
    private long close_time;

    public OpenTimeModel() {
    }

    public OpenTimeModel(int weekday, long open_time, long close_time) {
        this.weekday = weekday;
        this.open_time = open_time;
        this.close_time = close_time;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public long getOpen_time() {
        return open_time;
    }

    public void setOpen_time(long open_time) {
        this.open_time = open_time;
    }

    public long getClose_time() {
        return close_time;
    }

    public void setClose_time(long close_time) {
        this.close_time = close_time;
    }
}
