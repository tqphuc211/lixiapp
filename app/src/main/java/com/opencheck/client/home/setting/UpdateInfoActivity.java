package com.opencheck.client.home.setting;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.UpdateInfoActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.newlogin.communicate.OnPickDateListener;
import com.opencheck.client.home.newlogin.dialog.DatePickerDialog;
import com.opencheck.client.home.setting.fragment.ChangePassFragment;
import com.opencheck.client.models.user.CityDistrictModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class UpdateInfoActivity extends LixiActivity {

    private UserModel userModel;

    private CityDistrictModel citySelected = null;
    private CityDistrictModel districtSelected = null;

    private UpdateInfoActivityBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.update_info_activity);
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                initData();
            }
        });
    }

    private void initData() {
        userModel = UserModel.getInstance();
        mBinding.rlBack.setOnClickListener(this);
        mBinding.btnUpdate.setOnClickListener(this);
        mBinding.txtChangePass.setOnClickListener(this);
        mBinding.edtBirth.setOnClickListener(this);

        setDefault();
    }

    private void setDefault() {
        mBinding.txtPhone.setText(userModel.getPhone());
        mBinding.edtEmail.setText(userModel.getEmail());
        mBinding.edtName.setText(userModel.getFullname());
        mBinding.edtBirth.setText(userModel.getBirthday());

        mBinding.switchGender.post(new Runnable() {
            @Override
            public void run() {
                if (userModel.getGender().equals("male")) {
                    mBinding.switchGender.setCheck(false);
                } else {
                    mBinding.switchGender.setCheck(true);
                }
            }
        });

        mBinding.edtEmail.setEnabled(false);

        citySelected = userModel.getCityModel();
        districtSelected = userModel.getDistrictModel();
        setCityView();
    }

    private void setCityView() {
        String textAddress = citySelected.getName();
        if (textAddress.equals("null")) {
            if (Integer.parseInt(districtSelected.getId()) >= 0) {
                textAddress = districtSelected.getName();
                if (textAddress.equals("null"))
                    textAddress = "Chưa cập nhật";
            }
        } else if (Integer.parseInt(districtSelected.getId()) >= 0) {
            if (!districtSelected.getName().equals("null"))
                textAddress = textAddress + ", " + districtSelected.getName();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                onBackPressed();
                break;
            case R.id.btnUpdate:
                updateProfile();
                break;
            case R.id.txtChangePass:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameContainer, new ChangePassFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.edtBirth:
                DatePickerDialog dialog = new DatePickerDialog(activity);
                dialog.show();
                dialog.setOnPickListener(new OnPickDateListener() {
                    @Override
                    public void onPicked(String date) {
                        mBinding.edtBirth.setText(date);
                    }
                });
                break;
        }
    }

    private void updateProfile() {
        String name = mBinding.edtName.getText().toString();
        String date = mBinding.edtBirth.getText().toString();
        String email = mBinding.edtEmail.getText().toString();
        String gender;
        if (mBinding.switchGender.isCheck()) {
            gender = "female";
        } else {
            gender = "male";
        }

        // check full input
        if (name.isEmpty() || date.isEmpty()) {
            toast(LanguageBinding.getString(R.string.login_error_miss_input_info, activity));
            return;
        }

        try {
            JsonObject param = new JsonObject();
            param.addProperty("birthday", date);
            if (!email.isEmpty()) {
//                param.addProperty("email", email);
            }
            param.addProperty("gender", gender);
            param.addProperty("name", name);

            DataLoader.putUpdateProfile(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        Helper.showErrorDialog(activity, "Cập nhật tài khoản thành công", new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                finish();
                            }
                        });
                    } else {
                        Helper.showErrorDialog(activity, (String) object);
                    }
                }
            }, param);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toast(String content) {
        Toast.makeText(activity, content, Toast.LENGTH_SHORT).show();
    }
}
