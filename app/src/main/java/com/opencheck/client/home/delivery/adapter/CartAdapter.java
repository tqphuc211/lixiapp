package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.CartItemLayoutBinding;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.logging.Handler;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {

    private LixiActivity activity;
    private ArrayList<ProductModel> productList;
    private ItemClickListener itemClickListener;

    public CartAdapter(LixiActivity activity, ArrayList<ProductModel> productList) {
        this.activity = activity;
        this.productList = productList;
    }

    public void setProductList(ArrayList<ProductModel> list) {
        this.productList = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (productList != null) {
            productList.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CartItemLayoutBinding layoutBinding = CartItemLayoutBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new CartHolder(layoutBinding.getRoot());
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(CartHolder holder, final int position) {
        final ProductModel productModel = productList.get(position);
        holder.tv_name.setText(productModel.getName());
        holder.tv_money_total.setText(Helper.getVNCurrency(productModel.getPrice()) + "đ");
        holder.tv_count.setText("" + productModel.getQuantity());
        //ImageLoader.getInstance().displayImage(productModel.getImage_link(), holder.iv_merchant, LixiApplication.getInstance().optionsNomal);

        if (productModel.getList_addon() != null && productModel.getList_addon().size() > 0) {
            holder.tv_addon.setVisibility(View.VISIBLE);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < productModel.getList_addon().size(); i++) {
                if (productModel.getList_addon().get(i).getDefault_value() != null) {
                    builder.append(Double.valueOf(String.valueOf(productModel.getList_addon().get(i).getDefault_value())).intValue())
                            .append("x");
                }
                builder.append(productModel.getList_addon().get(i).getName());

                if (i < productModel.getList_addon().size() - 1) {
                    builder.append(", ");
                }
                holder.tv_addon.setText(builder.toString());
            }
        } else {
            holder.tv_addon.setVisibility(View.GONE);
        }
    }

    public ProductModel getItemAtPos(int pos) {
        return productList.get(pos);
    }

    public void updateQuantity(int pos, int quantity) {
        productList.get(pos).setQuantity(quantity);
        notifyItemRemoved(pos);
    }

    public ArrayList<ProductModel> getProductList() {
        return productList;
    }

    public void removeItemAtPos(int pos) {
        productList.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    class CartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_name, tv_addon, tv_money_total, tv_count;
        private ImageView iv_add, iv_sub, iv_merchant;

        CartHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_addon = itemView.findViewById(R.id.tv_addon);
            tv_money_total = itemView.findViewById(R.id.tv_money_total);
            tv_count = itemView.findViewById(R.id.tv_count);
            iv_add = itemView.findViewById(R.id.iv_add);
            iv_sub = itemView.findViewById(R.id.iv_sub);
            iv_merchant = itemView.findViewById(R.id.iv_merchant);

            iv_add.setOnClickListener(this);
            iv_sub.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == iv_sub) {
                if (productList == null || productList.size() == 0)
                    return;
                if (productList.get(getAdapterPosition()) != null) {
                    int count = productList.get(getAdapterPosition()).getQuantity() - 1;
                    if (count > 0) {
                        productList.get(getAdapterPosition()).setQuantity(count);
                        tv_count.setText("" + count);
                    }
                    itemClickListener.onItemCLick(getAdapterPosition(), ConstantValue.SUB_CLICK, productList.get(getAdapterPosition()), (long) count);
                }
            } else if (view == iv_add) {
                if (productList != null && productList.get(getAdapterPosition()) != null) {
                    int count = productList.get(getAdapterPosition()).getQuantity() + 1;
                    productList.get(getAdapterPosition()).setQuantity(count);
                    tv_count.setText("" + count);
                    itemClickListener.onItemCLick(getAdapterPosition(), ConstantValue.ADD_CLICK, productList.get(getAdapterPosition()), (long) count);

                }
            }
        }
    }
}
