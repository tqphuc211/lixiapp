package com.opencheck.client.home.delivery.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentMenuBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.activities.ProductDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.adapter.PromotionAdapter;
import com.opencheck.client.home.delivery.controller.StorePolicyController;
import com.opencheck.client.home.delivery.dialog.AddonCategoryDialog;
import com.opencheck.client.home.delivery.interfaces.OnGetMenuToActivityListener;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.delivery.libs.MyCustomLayoutManager;
import com.opencheck.client.home.delivery.libs.MyScrollView;
import com.opencheck.client.home.delivery.model.CartModel;
import com.opencheck.client.home.delivery.model.MenuModel;
import com.opencheck.client.home.delivery.model.ProductModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.model.StorePolicyModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends LixiFragment implements StoreDetailActivity.OnTransferDataToFirstTabEvent {

    //Initialize views
    private RecyclerView rcvMenu, rcv_promotion;
    private FoodAdapter adapter;
    private TextView tv_copy_code, tv_promotion_title, tv_promotion_content;
    private TextView tvAddress;
    private LinearLayout lnNoResult;
    private MyScrollView scroll;
    private RelativeLayout rlPromotion;
    private View v_promotion;

    //Initialize variables
    private long storeId = 0L;
    private boolean isFirstTime = true;
    private StoreOfCategoryModel mStore;
    private ArrayList<PromotionModel> promotionList = new ArrayList<>();
    private ArrayList<Integer> headerList = new ArrayList<>();
    private ArrayList<MenuModel> menu = new ArrayList<>();
    private PromotionAdapter promotionAdapter;
    private StickyRecyclerHeadersDecoration headersDecor;
    private FragmentMenuBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_menu, container, false);
        storeId = activity.getIntent().getExtras().getLong("ID", 0L);
        ((StoreDetailActivity) activity).setReceiveStoreFirstEvent(this);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initViews(mBinding.getRoot());
    }

    @Override
    public void onStart() {
        super.onStart();
        if(isFirstTime && getActivity() != null && isAdded()) {
            isFirstTime = false;
            ((StoreDetailActivity) getActivity()).getStoreDetail();
        }
    }

    private void initViews(View view) {
        rcvMenu = view.findViewById(R.id.rcv_menu);
//        rcv_promotion = view.findViewById(R.id.rcv_promotion);
        tv_copy_code = view.findViewById(R.id.tv_copy_code);
        lnNoResult = view.findViewById(R.id.lnNoResult);
        scroll = view.findViewById(R.id.scroll);
        tv_promotion_title = view.findViewById(R.id.tv_promotion_title);
        tv_promotion_content = view.findViewById(R.id.tv_promotion_content);
        tvAddress = view.findViewById(R.id.tvAddress);
        v_promotion = view.findViewById(R.id.v_promotion);
        rlPromotion = view.findViewById(R.id.rlPromotion);

        tv_copy_code.setOnClickListener(this);

        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.setMyScrollChangeListener(new MyScrollView.OnMyScrollChangeListener() {
                    @Override
                    public void onScrollUp() {
                        if (adapter != null && headerList != null && headerList.size() > 0) {
                            for (int i = 1; i < headerList.size(); i++) {
                                if (scroll.getScrollY() >= headerList.get(i - 1) && scroll.getScrollY() < headerList.get(i)) {
                                    ((StoreDetailActivity) activity).toggleTitle(i - 1);
                                    ((MyCustomLayoutManager) ((StoreDetailActivity) activity)
                                            .getRv_menu_title()
                                            .getLayoutManager())
                                            .scrollToPositionWithOffset(i - 1, 0);
                                    break;
                                }
                            }
                        }
                    }

                    @Override
                    public void onScrollDown() {
                        if (adapter != null && headerList != null && headerList.size() > 0) {
                            for (int i = 0; i < headerList.size(); i++) {
                                if (scroll.getScrollY() >= headerList.get(i) - activity.getResources().getDimension(R.dimen.value_5)) {
                                    ((StoreDetailActivity) activity).toggleTitle(i);
                                    ((MyCustomLayoutManager) ((StoreDetailActivity) activity)
                                            .getRv_menu_title()
                                            .getLayoutManager())
                                            .scrollToPositionWithOffset(i, 0);
                                }
                            }
                        }
                    }
                });
            }
        });

        setAdapter();
        //setPromotionAdapter();
    }

    private void getProductDetail(final ProductModel product, final int pos, final View view) {
        DeliDataLoader.getProductDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    final ProductModel productModel = (ProductModel) object;
                    AddonCategoryDialog addonCategoryDialog = new AddonCategoryDialog(activity, false, true, false);
                    addonCategoryDialog.show();
                    addonCategoryDialog.setData(productModel);
                    addonCategoryDialog.setIDialogEventListener(new AddonCategoryDialog.IDialogEvent() {
                        @Override
                        public void onOk(int quantity) {
                            adapter.addProductOnClickEvent(product.getId());
                            adapter.updateQuantity(pos, quantity + product.getQuantity());
                            if(product.getIs_hot() == 1) {
                                adapter.updateViewForSameProduct(product.getId(), pos, ConstantValue.ADD_CLICK, product.getQuantity());
                            }
                            animationView(view, product.getQuantity());
                            ((StoreDetailActivity) activity).setCartData();
                        }
                    });
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, product.getId());
    }

    private void addProductToCart(ProductModel productModel, int quantity) {
        HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
        CartModel cartModel = null;
        // If store is existed in cart
        if (cart.containsKey("" + storeId)) {
            cartModel = cart.get("" + storeId);
            if (cartModel == null)
                cartModel = new CartModel();

            // If store contains this product after that update its quantity
            if (cartModel.getProduct_list() != null) {
                if (cartModel.getProduct_list().containsKey("" + productModel.getId())) {
                    if (cartModel.getProduct_list().get("" + productModel.getId()).size() > 0)
                        cartModel.getProduct_list().get("" + productModel.getId()).get(0).setQuantity(quantity);
                } else {
                    ArrayList<ProductModel> temp = new ArrayList<>();
                    temp.add(productModel);
                    cartModel.getProduct_list().put("" + productModel.getId(), temp);
                }
            } else { // add new product into cart
                ArrayList<ProductModel> temp = new ArrayList<>();
                temp.add(productModel);
                HashMap<String, ArrayList<ProductModel>> productList = new HashMap<>();
                productList.put("" + productModel.getId(), temp);
                cartModel.setProduct_list(productList);
            }
        } else {
            ArrayList<ProductModel> temp = new ArrayList<>();
            temp.add(productModel);

            HashMap<String, ArrayList<ProductModel>> product_list = new HashMap<>();
            product_list.put("" + productModel.getId(), temp);

            cartModel = new CartModel();
            cartModel.setId(storeId);
            cartModel.setProduct_list(product_list);
        }

        cart.put("" + storeId, cartModel);
        Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
    }

    private void animationView(final View view, int value) {
        int fromLoc[] = new int[2];
        ImageView imageView = view.findViewById(R.id.iv_add);
        imageView.getLocationOnScreen(fromLoc);
        float startX = fromLoc[0];
        float startY = fromLoc[1];

        int toLoc[] = new int[2];
        ((StoreDetailActivity) activity).iv_cart.getLocationOnScreen(toLoc);

        TextView tvCount = new TextView(activity);
        final RelativeLayout rlCount = new RelativeLayout(activity);
        rlCount.setLayoutParams(new ViewGroup.LayoutParams((int) activity.getResources().getDimension(R.dimen.value_30),
                (int) activity.getResources().getDimension(R.dimen.value_30)));
        rlCount.setBackgroundResource(R.drawable.bg_cirlce_violet);
        tvCount.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int padding = (int) activity.getResources().getDimension(R.dimen.value_2);
        rlCount.setPadding(padding, padding, padding, padding);
        tvCount.setTextSize(activity.getResources().getDimension(R.dimen.value_6));
        tvCount.setTextColor(getResources().getColor(R.color.white));
        rlCount.setGravity(Gravity.CENTER);
        tvCount.setText(String.valueOf(value));
        rlCount.addView(tvCount);
        ((StoreDetailActivity) activity).rootView.addView(rlCount);
        //rlRoot.addView(rlCount);

        final Path path = new Path();
        path.moveTo(startX, startY);
        path.quadTo(startX, startY - activity.getResources().getDimension(R.dimen.value_30),
                activity.widthScreen / 2, startY);

        path.quadTo(activity.widthScreen / 2 - 2 * ((StoreDetailActivity) activity).iv_cart.getWidth(), startY + ((StoreDetailActivity) activity).iv_cart.getHeight(),
                activity.getResources().getDimension(R.dimen.value_20), activity.heightScreen - activity.getResources().getDimension(R.dimen.value_50));

        path.close();

        ValueAnimator pathAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator.setDuration(400);
        pathAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path, true);
                pathMeasure.getPosTan(pathMeasure.getLength() / 2 * val, point, null);
                Log.d("mapi", "" + point[0] + "/" + point[1]);

                rlCount.setTranslationX(point[0]);
                rlCount.setTranslationY(point[1]);
            }
        });

        pathAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                Log.d("mapi", "cancel animation");
            }

            @Override
            public void onAnimationEnd(final Animator animation) {
                super.onAnimationEnd(animation);
                Log.d("mapi", "end animation");
                rlCount.setVisibility(View.GONE);
                rlCount.setDrawingCacheEnabled(false);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((StoreDetailActivity) activity).rootView.removeView(rlCount);
                        //rlRoot.removeView(rlCount);
                    }
                }, 100);

                ((StoreDetailActivity) activity).animateView();
                adapter.setClickEnable(true);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                Log.d("mapi", "start animation");
                rlCount.setDrawingCacheEnabled(true);
                adapter.setClickEnable(false);

            }

            @Override
            public void onAnimationPause(Animator animation) {
                super.onAnimationPause(animation);
                Log.d("mapi", "pause animation");
            }

            @Override
            public void onAnimationResume(Animator animation) {
                super.onAnimationResume(animation);
                Log.d("mapi", "resume animation");
            }
        });
        pathAnimator.start();
        rlCount.animate().scaleX(0.5f).scaleY(0.5f).setDuration(400).start();
    }

    private void setAdapter() {
        adapter = new FoodAdapter(activity, rcvMenu);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setItemPrefetchEnabled(true);
        rcvMenu.setLayoutManager(mLayoutManager);
        rcvMenu.setItemAnimator(new DefaultItemAnimator());
        rcvMenu.setAdapter(adapter);
        adapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(final View view, final int pos, final String action, Object object, final Long count) {
                final ProductModel product = (ProductModel) object;
                switch (action) {
                    case ConstantValue.ADD_CLICK:
                        if (product.isHave_addon()) {
                            getProductDetail(product, pos, view);
                        } else {
                            adapter.addProductOnClickEvent(product.getId());
                            addProductToCart(adapter.getItem(pos), count.intValue());
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    animationView(view, count.intValue());
                                }
                            }, 100);

                            ((StoreDetailActivity) activity).setCartData();
                        }

                        break;
                    case ConstantValue.SUB_CLICK:
                        if (product.isHave_addon()) {
                            ((StoreDetailActivity) activity).setSpecifyProductData(product);
                        } else {
                            HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);
                            if (cart.get("" + storeId) != null) {
                                if (count == 0) {
                                    if (cart.get("" + storeId).getProduct_list() != null) {
                                        cart.get("" + storeId).getProduct_list().remove("" + product.getId());
                                        adapter.removeProductInAddedList(product.getId());
                                    }

                                    if (cart.get("" + storeId).getProduct_list() == null ||
                                            (cart.get("" + storeId).getProduct_list() != null && cart.get("" + storeId).getProduct_list().size() == 0)) {
                                        cart.remove("" + storeId);
                                    }
                                } else
                                    cart.get("" + storeId).getProduct_list().get("" + adapter.getItem(pos).getId()).get(0).setQuantity(count.intValue());

                                Helper.saveProductToFile(cart, activity, ConstantValue.CART_FILE);
                                ((StoreDetailActivity) activity).setCartData();
                            }
                        }
                        break;
                    case ConstantValue.ON_ITEM_CLICK:
                        ProductDetailActivity.startProductDetailActivity(activity,
                                product.getId(),
                                product.getName(),
                                product.getQuantity(),
                                pos, product.isHave_addon());
                        break;
                }
            }
        });

        headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        rcvMenu.addItemDecoration(headersDecor);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                headersDecor.invalidateHeaders();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ProductDetailActivity.REQUEST_CODE_DETAIL_PRODUCT) {
            if (resultCode == RESULT_OK) {
                int tempCount = data.getIntExtra("COUNT", 0);
                boolean isAddon = data.getBooleanExtra("IS_ADDON", false);
                int position = data.getIntExtra("POSITION", -1);

                if (!isAddon) {
                    addProductToCart(adapter.getItem(position), tempCount);
                }
                adapter.updateQuantity(position, tempCount);
                if (adapter.getItem(position).getIs_hot() == 1) {
                    adapter.updateViewForSameProduct(adapter.getItem(position).getId(), position, ConstantValue.ADD_CLICK, tempCount);
                }
                ((StoreDetailActivity) activity).setCartData();
            }
        }
    }

    public FoodAdapter getAdapter() {
        return adapter;
    }

    public void getMenu() {
        activity.showProgressBar();
        DeliDataLoader.getMenu(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    menu = (ArrayList<MenuModel>) object;
                    if (menu == null || menu.size() == 0) {
                        activity.hideProgressBar();
                        lnNoResult.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (activity instanceof OnGetMenuToActivityListener)
                        ((OnGetMenuToActivityListener) activity).onOk(menu);
                    addItems(menu);
                    lnNoResult.setVisibility(View.GONE);

                    HashMap<String, CartModel> cart = Helper.getCartFromFile(activity, ConstantValue.CART_FILE);

                    if (cart.containsKey("" + storeId) && cart.get("" + storeId) != null && cart.get("" + storeId).getProduct_list() != null) {
                        Set<String> keySet = cart.get("" + storeId).getProduct_list().keySet();
                        for (String key : keySet) {
                            ArrayList<ProductModel> productList = new ArrayList<>(cart.get("" + storeId).getProduct_list().get(key));
                            int total = 0;
                            int pos = adapter.getPosOfItem(productList.get(0));
                            for (int j = 0; j < productList.size(); j++) {
                                if (adapter != null) {
                                    total += productList.get(j).getQuantity();
                                }
                            }

                            adapter.updateQuantity(pos, total);
                        }
                        ((StoreDetailActivity) activity).setCartData();
                    }

                    //save header y into list
                    rcvMenu.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                activity.hideProgressBar();

                                if (headerList != null && headerList.size() > 0)
                                    headerList.clear();

                                int heightHeaderStoreDetail = 0;

                                if (mStore.getMin_money_to_free_ship_price() != null) {
                                    heightHeaderStoreDetail += rlPromotion.getMeasuredHeight();
                                }

                                heightHeaderStoreDetail += mBinding.llPolicy.getMeasuredHeight();

//                                if (promotionList != null && promotionList.size() > 0) {
//                                    heightHeaderStoreDetail += (int) activity.getResources().getDimension(R.dimen.value_106) + 2 * (int) activity.getResources().getDimension(R.dimen.value_8);
//                                }

                                headerList.add(heightHeaderStoreDetail);
                                int index = 0;
                                int heightHeader = headersDecor.getHeaderView(rcvMenu, 0).getBottom();

                                for (int i = 0; i < menu.size(); i++) {
                                    int h = heightHeader + headerList.get(i);
                                    for (int j = 0; j < menu.get(i).getList_product().size(); j++) {
                                        RecyclerView.ViewHolder holder = rcvMenu.findViewHolderForLayoutPosition(index);
                                        if (holder != null) {
                                            h += holder.itemView.getHeight();
                                            ++index;
                                        } else break;
                                    }
                                    headerList.add(h);
                                }
                            } catch (Exception e) {
                                Crashlytics.logException(e);
                            }
                        }
                    });

                    // if see detail product in home, add this product and show cart
                    if (activity.getIntent().getExtras().getLong("PRODUCT_ID") != 0L) {
                        adapter.findProductAndClickAdd(activity.getIntent().getExtras().getLong("PRODUCT_ID"));
                        activity.getIntent().putExtra("PRODUCT_ID", 0L);
                    }

                    // find hot product and update view
                    rcvMenu.post(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < adapter.getItemList().size(); i++) {
                                ProductModel productModel = adapter.getItem(i);
                                if (productModel.getQuantity() > 0 && productModel.getIs_hot() == 1) {
                                    adapter.updateViewForSameProduct(productModel.getId(), i, ConstantValue.ADD_CLICK, productModel.getQuantity());
                                }
                            }
                        }
                    });
                } else {
                    activity.hideProgressBar();
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, storeId);
    }

    private void setPromotionAdapter() {
        promotionAdapter = new PromotionAdapter(activity, null);
        rcv_promotion.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        rcv_promotion.setAdapter(promotionAdapter);
    }

    public ArrayList<Integer> getHeaderList() {
        return headerList;
    }

    public MyScrollView getScroll() {
        return scroll;
    }

    @Override
    public void onClick(View view) {
        if (view == tv_copy_code) {
            Helper.copyCode(promotionList.get(0).getCode(), activity);
        }
    }

    public void addItems(ArrayList<MenuModel> menu) {
        adapter.addItems(menu);
    }

    @Override
    public void onDataReceivedOnFirstTab(StoreOfCategoryModel store) {
        this.mStore = store;
        if (mStore != null) {
            getMenu();

            tvAddress.setText(mStore.getAddress());
            String dot = LanguageBinding.getString(R.string.dot, activity);

            rlPromotion.setVisibility(View.VISIBLE);
            v_promotion.setVisibility(View.VISIBLE);

            switch (mStore.getFeeType()) {
                case SAME_FEE:
                    String priceSame = Helper.getVNCurrency(mStore.getFixed_ship_price()) + LanguageBinding.getString(R.string.p, activity);
                    String distance = mStore.getFixed_ship_price_range() + "Km";
                    String shipPrice = Helper.getVNCurrency(mStore.getShip_price()) + LanguageBinding.getString(R.string.p, activity);
                    String fixedShipPrice = Helper.getVNCurrency(mStore.getMin_money_to_fixed_ship_price()) + LanguageBinding.getString(R.string.p, activity);

                    String priceSameHtml = String.format(LanguageBinding.getString(R.string.store_detail_same_fee, activity), priceSame);
                    String title = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_title, activity), "<b>" + priceSame + "</b>", "<b>" + distance + "</b>");
                    String content = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_content, activity), "<b>" + shipPrice + "</b>");
                    String promotionConditionHtml = String.format(LanguageBinding.getString(R.string.deli_promotion_condition_apply, activity), "<b>" + fixedShipPrice + "</b>");

                    mBinding.tvPromotion.setText(Html.fromHtml(priceSameHtml));
                    mBinding.tvPromotionTitle.setText(Html.fromHtml(dot + title));
                    mBinding.tvPromotionContent.setText(Html.fromHtml(dot + content));
                    if (mStore.getFixed_ship_price() != 0) {
                        mBinding.txtDefaultTop.setText(Html.fromHtml("(" + promotionConditionHtml + ")"));
                        mBinding.txtDefaultBottom.setText(Html.fromHtml("(" + promotionConditionHtml + ")"));
                    } else {
                        mBinding.txtDefaultTop.setText(getString(R.string.store_detail_default_apply));
                        mBinding.txtDefaultBottom.setText(getString(R.string.store_detail_default_apply));
                    }

                    mBinding.txtDefaultTop.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mBinding.txtDefaultTop.getLineCount() > 1) {
                                mBinding.txtDefaultTop.setVisibility(View.INVISIBLE);
                                mBinding.txtDefaultBottom.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    break;
                case FREE_SHIP:
                    if (mStore.getMin_money_to_free_ship_price() != null && mStore.getMin_money_to_free_ship_price() != 0) {
                        String promotion = "(" + LanguageBinding.getString(R.string.store_detail_promotin_default, activity) + " <b>" + Helper.getVNCurrency(mStore.getMin_money_to_free_ship_price()) + "đ</b>)";
                        mBinding.txtDefaultTop.setText(Html.fromHtml(promotion));
                    }

                    if (mStore.getMax_free_ship_money() != null && mStore.getMax_free_ship_money() > 0) {
                        long dis = 0;
                        if (mStore.getShip_price() != 0) {
                            dis = mStore.getMax_free_ship_money() / mStore.getShip_price();
                            tv_promotion_title.setText(Html.fromHtml(dot + LanguageBinding.getString(R.string.store_detail_max_free_ship, activity) + " <b>" + dis + "km " + "(" + Helper.getVNCurrency(mStore.getMax_free_ship_money()) + "đ)" + "</b>"));
                        } else {
                            tv_promotion_title.setText(Html.fromHtml(dot + LanguageBinding.getString(R.string.store_detail_max_free_ship, activity) + " <b>" + Helper.getVNCurrency(mStore.getMax_free_ship_money()) + "đ</b>"));
                        }
                    } else {
                        tv_promotion_title.setText(Html.fromHtml(dot + LanguageBinding.getString(R.string.store_detail_free_delivery, activity)));
                    }

                    String nextKm = Helper.getVNCurrency(mStore.getShip_price()) + LanguageBinding.getString(R.string.p, activity);
                    String nextKmHtml = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_content, activity), "<b>" + nextKm + "</b>");
                    tv_promotion_content.setText(Html.fromHtml(dot + nextKmHtml));
                    break;
                case NORMAL:
                    rlPromotion.setVisibility(View.GONE);
                    break;
            }

            showStorePolicy(store, rlPromotion.getVisibility() == View.VISIBLE);

//            promotionList = store.getList_promotion_code();
//            if (promotionList == null || promotionList.size() == 0) {
//                rcv_promotion.setVisibility(View.GONE);
//                return;
//            }
//            promotionAdapter.setPromotionList(promotionList);
        }
    }

    private void showStorePolicy(StoreOfCategoryModel store, boolean isHaveDefault) {
        ArrayList<StorePolicyModel> list = store.getList_store_policy();

        for (PromotionModel promotionModel : store.getList_promotion_code()) {
            boolean isExist = false;
            for (StorePolicyModel storePolicyModel : store.getList_store_policy()) {
                if (storePolicyModel.getList_promotion_code() != null
                        && storePolicyModel.getList_promotion_code().size() > 0
                        && promotionModel.getCode().equals(storePolicyModel.getList_promotion_code().get(0))
                ) {
                    isExist = true;
                }
            }

            if (!isExist) {
                list.add(promotionModel.convertToStorePolicy());
            }
        }

        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator<StorePolicyModel>() {
                @Override
                public int compare(StorePolicyModel t1, StorePolicyModel t2) {
                    if (t1.getType().equals("promotion_code") && !t2.getType().equals("promotion_code"))
                        return 1;
                    else if (!t1.getType().equals("promotion_code") && t2.getType().equals("promotion_code"))
                        return -1;
                    else return 0;
                }
            });

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getType().equals("promotion_code")) {
                    if (list.get(i).getList_promotion_code() == null || list.get(i).getList_promotion_code().size() == 0) {
                        continue;
                    }
                }
                new StorePolicyController(activity, mBinding.llPolicy, list.get(i), i, isHaveDefault);
            }
        }
    }

    //region ADAPTER
    public abstract class RecyclerItemAdapter extends RecyclerView.Adapter {
        ArrayList<ProductModel> items = new ArrayList<>();

        RecyclerItemAdapter() {
            setHasStableIds(true);
        }

        public void add(ProductModel object) {
            items.add(object);
            notifyDataSetChanged();
        }

        public void add(int index, ProductModel object) {
            items.add(index, object);
            notifyDataSetChanged();
        }

        public void addAll(Collection collection) {
            if (collection != null) {
                items.addAll(collection);
                notifyDataSetChanged();
            }
        }

        public void addAll(ProductModel... items) {
            addAll(Arrays.asList(items));
        }

        public void clear() {
            items.clear();
            notifyDataSetChanged();
        }

        public void remove(ProductModel object) {
            items.remove(object);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return items == null ? 0 : items.size();
        }

        public ProductModel getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).hashCode();
        }

        public int getPosOfItem(ProductModel item) {
            int pos = 0;
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getId() == item.getId()) {
                    pos = i;
                    break;
                }
            }
            return pos;
        }

        public ArrayList<ProductModel> getItemList() {
            return items;
        }
    }

    public class FoodAdapter extends RecyclerItemAdapter implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {
        private Context context;
        private RecyclerView rcv;
        private OnItemClickListener itemClickListener;
        private int count = 0;
        private ArrayList<Long> addedList = new ArrayList<>();
        private long hotCategoryId = -1;
        private HashMap<Long, Pair<Long, Long>> sameProductIdMap;

        FoodAdapter(Context context, RecyclerView rcv) {
            this.context = context;
            this.rcv = rcv;
            sameProductIdMap = new HashMap<>();
        }

        void addItems(ArrayList<MenuModel> menu) {
            if (menu != null && menu.size() > 0 && menu.get(0).getList_product() != null && menu.get(0).getList_product().size() > 0 && menu.get(0).getList_product().get(0).getIs_hot() == 1) {
                hotCategoryId = menu.get(0).getCategory_id();
            }
            for (int i = 0; i < menu.size(); i++) {
                for (int j = 0; j < menu.get(i).getList_product().size(); j++) {
                    ProductModel productModel = menu.get(i).getList_product().get(j);
                    productModel.setCategory_name(menu.get(i).getCategory_name());
                    productModel.setCategory_id(menu.get(i).getCategory_id());
                    add(productModel);
                    if (productModel.getCategory_id() == hotCategoryId) {
                        sameProductIdMap.put(productModel.getId(), new Pair<Long, Long>((long) (items.size() - 1), -1L));
                    } else {
                        if (sameProductIdMap.containsKey(productModel.getId())) {
                            Long firstPos = sameProductIdMap.get(productModel.getId()).first;
                            sameProductIdMap.put(productModel.getId(), new Pair<Long, Long>(firstPos, (long) (items.size() - 1)));
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }

        private boolean isClickEnable = true;

        void setClickEnable(boolean canClick) {
            isClickEnable = canClick;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menu_item_layout, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.tv_name.setText(getItem(position).getName());
            viewHolder.tv_price.setText(Html.fromHtml("" + Helper.getVNCurrency(getItem(position).getPrice()) + "đ"));

            if (getItem(position).getDescription() != null && !getItem(position).getDescription().equals("")) {
                viewHolder.tv_description.setText(getItem(position).getDescription());
                viewHolder.tv_description.setVisibility(View.VISIBLE);
            } else viewHolder.tv_description.setVisibility(View.GONE);

            ImageLoader.getInstance().displayImage(getItem(position).getImage_link(), viewHolder.iv_food, LixiApplication.getInstance().optionsNomal);

            if (getItem(position).getPrice() < getItem(position).getOriginal_price()) {
                viewHolder.tv_original_price.setVisibility(View.VISIBLE);
                viewHolder.tv_original_price.setText(Html.fromHtml(Helper.getVNCurrency(getItem(position).getOriginal_price()) + "đ"));
                viewHolder.tv_original_price.setPaintFlags(viewHolder.tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                viewHolder.tv_original_price.setVisibility(View.GONE);
            }

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHolder.rl_content.getLayoutParams();
            if (getItem(position).getState().equals("paused")) {
                params.addRule(RelativeLayout.LEFT_OF, viewHolder.tv_no_result.getId());
                viewHolder.tv_no_result.setVisibility(View.VISIBLE);
                viewHolder.lnCount.setVisibility(View.GONE);
            } else {
                params.addRule(RelativeLayout.LEFT_OF, viewHolder.lnCount.getId());
                viewHolder.tv_no_result.setVisibility(View.GONE);
                viewHolder.lnCount.setVisibility(View.VISIBLE);
            }

            if (getItem(position).getQuantity() < 1) {
                viewHolder.tv_count.setVisibility(View.GONE);
                viewHolder.iv_sub.setVisibility(View.GONE);
            } else {
                viewHolder.tv_count.setVisibility(View.VISIBLE);
                viewHolder.iv_sub.setVisibility(View.VISIBLE);
                viewHolder.tv_count.setText(Html.fromHtml("" + getItem(position).getQuantity()));
            }

            if (getItem(position).getIs_hot() == 1) {
                viewHolder.tv_hot.setVisibility(View.VISIBLE);
                viewHolder.tv_name.setMaxWidth((int) activity.getResources().getDimension(R.dimen.value_140));
            } else {
                viewHolder.tv_hot.setVisibility(View.GONE);
                viewHolder.tv_name.setMaxWidth(Integer.MAX_VALUE);
            }

//            if (getItem(position).getIs_hot() == 1) {
//                if (getItem(position).getCategory_id() == hotCategoryId) {
//                    viewHolder.lnCount.setVisibility(View.VISIBLE);
//                } else {
//                    viewHolder.lnCount.setVisibility(View.GONE);
//                }
//            }
        }

        @Override
        public long getHeaderId(int position) {
            return getItem(position).getCategory_id();
        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_food_item, parent, false);
            return new HeaderHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof HeaderHolder) {
                if (getItem(position).getCategory_name() != null) {
                    ((HeaderHolder) holder).tvHeader.setText(getItem(position).getCategory_name());

                    if (getItem(position).getIs_hot() == 1 && position == 0) {
                        ((HeaderHolder) holder).ivHot.setVisibility(View.VISIBLE);
                        ((HeaderHolder) holder).tvHeader.setTextColor(activity.getResources().getColor(R.color.color_orange));
                    } else {
                        ((HeaderHolder) holder).ivHot.setVisibility(View.GONE);
                        ((HeaderHolder) holder).tvHeader.setTextColor(activity.getResources().getColor(R.color.black));
                    }
                }
            }
        }

        public void updateQuantity(int pos, int quantity) {
            getItem(pos).setQuantity(quantity);
            notifyItemChanged(pos);
        }

        public void addProductOnClickEvent(Long productId) {
            if (!addedList.contains(productId))
                addedList.add(productId);
        }

        public void findProductAndClickAdd(final long productId) {
            rcv.post(new Runnable() {
                @Override
                public void run() {
                    int pos = -1;
                    for (int i = 0; i < items.size(); i++) {
                        if (items.get(i).getId() == productId) {
                            pos = i;
                            break;
                        }
                    }
                    if (pos != -1) {
                        // TODO: scroll to pos
                    }
                    View view = rcv.getLayoutManager().findViewByPosition(pos);
                    if (view != null) {
                        view.findViewById(R.id.iv_add).callOnClick();
                    }
                }
            });
        }

        public void updateViewForSameProduct(Long productId, int currentPos, String type, int count) {
            Pair<Long, Long> pairPos = sameProductIdMap.get(productId);
            int pos = pairPos.first.intValue() == currentPos ? pairPos.second.intValue() : pairPos.first.intValue();
            FoodAdapter.ViewHolder viewHolder = (ViewHolder) rcv.findViewHolderForLayoutPosition(pos);
            if (viewHolder != null) {
                switch (type) {
                    case ConstantValue.ADD_CLICK:
                        viewHolder.tv_count.setVisibility(View.VISIBLE);
                        viewHolder.iv_sub.setVisibility(View.VISIBLE);
                        viewHolder.tv_count.setText(String.valueOf(count));
                        getItem(pos).setQuantity(count);
                        break;
                    case ConstantValue.SUB_CLICK:
                        viewHolder.tv_count.setText(String.valueOf(count));
                        getItem(pos).setQuantity(count);
                        if (count < 1) {
                            viewHolder.tv_count.setVisibility(View.GONE);
                            viewHolder.iv_sub.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        }

        public ArrayList<Long> getAddedList() {
            return addedList;
        }

        public HashMap<Long, Pair<Long, Long>> getSameProductIdMap() {
            return sameProductIdMap;
        }

        public void removeProductInAddedList(Long productId) {
            addedList.remove(productId);
        }

        class HeaderHolder extends RecyclerView.ViewHolder {
            TextView tvHeader;
            ImageView ivHot;
            View rootView;

            HeaderHolder(View itemView) {
                super(itemView);
                rootView = itemView;
                tvHeader = itemView.findViewById(R.id.tvHeader);
                ivHot = itemView.findViewById(R.id.ivHot);
                rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("height", rootView.getHeight() + " header");
                    }
                });
            }

        }

        public void setItemClickListener(OnItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView iv_food, iv_add, iv_sub;
            TextView tv_name, tv_price, tv_description, tv_count, tv_original_price, tv_no_result, tv_hot;
            View rootView;
            RelativeLayout rl_content;
            LinearLayout lnCount;

            ViewHolder(View itemView) {
                super(itemView);
                rootView = itemView;

                tv_name = itemView.findViewById(R.id.tv_name);
                tv_no_result = itemView.findViewById(R.id.tv_no_result);
                tv_price = itemView.findViewById(R.id.tv_price);
                tv_count = itemView.findViewById(R.id.tv_count);
                tv_description = itemView.findViewById(R.id.tv_description);
                tv_original_price = itemView.findViewById(R.id.tv_original_price);
                tv_hot = itemView.findViewById(R.id.tv_hot);
                iv_add = itemView.findViewById(R.id.iv_add);
                iv_sub = itemView.findViewById(R.id.iv_sub);
                iv_food = itemView.findViewById(R.id.iv_food);
                lnCount = itemView.findViewById(R.id.lnCount);
                rl_content = itemView.findViewById(R.id.rl_content);

                iv_sub.setOnClickListener(this);
                iv_add.setOnClickListener(this);
                rootView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                if (view == iv_sub) {
                    if (isClickEnable) {
                        ProductModel product = getItem(getAdapterPosition());
                        count = product.getQuantity() - 1;

                        if (!product.isHave_addon()) {
                            tv_count.setText(String.valueOf(count));
                            product.setQuantity(count);
                            if (count < 1) {
                                tv_count.setVisibility(View.GONE);
                                iv_sub.setVisibility(View.GONE);
                            }
                            if (product.getIs_hot() == 1) {
                                updateViewForSameProduct(product.getId(), getAdapterPosition(), ConstantValue.SUB_CLICK, count);
                            }
                        }
                        itemClickListener.onItemClickListener(rootView, getAdapterPosition(), ConstantValue.SUB_CLICK, getItem(getAdapterPosition()), (long) count);
                    }
                } else if (view == iv_add) {
                    if (isClickEnable) {
                        ProductModel product = getItem(getAdapterPosition());
                        //getItem(getAdapterPosition()).setViewCount(tv_count);
                        count = product.getQuantity() + 1;

                        if (!product.isHave_addon()) {
                            if (StoreDetailActivity.isWatch) {
                                Toast.makeText(activity, "Xin lỗi không thể thêm sản phẩm này. Vui lòng quay lại sau", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            tv_count.setVisibility(View.VISIBLE);
                            iv_sub.setVisibility(View.VISIBLE);
                            tv_count.setText(String.valueOf(count));
                            product.setQuantity(count);
                            if (product.getIs_hot() == 1) {
                                updateViewForSameProduct(product.getId(), getAdapterPosition(), ConstantValue.ADD_CLICK, count);
                            }
                        }
                        itemClickListener.onItemClickListener(rootView, getAdapterPosition(), ConstantValue.ADD_CLICK, getItem(getAdapterPosition()), (long) count);
                    }
                } else if (view == rootView) {
                    itemClickListener.onItemClickListener(rootView, getAdapterPosition(), ConstantValue.ON_ITEM_CLICK, getItem(getAdapterPosition()), (long) count);
                }
            }
        }
    }
    //endregion
}
