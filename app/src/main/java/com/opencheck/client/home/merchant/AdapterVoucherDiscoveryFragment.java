package com.opencheck.client.home.merchant;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DiscoveryRowVoucherFragmentBinding;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class AdapterVoucherDiscoveryFragment extends RecyclerView.Adapter<AdapterVoucherDiscoveryFragment.ViewHolder> {

    MerchantVoucherFragment newVoucherFragment;
    LixiActivity activity;
    ArrayList<LixiShopEvoucherModel> list;
    Boolean isMerchant;
    int cWith;


    public AdapterVoucherDiscoveryFragment(LixiActivity activity, ArrayList<LixiShopEvoucherModel> list, Boolean isMerchant, int cWith) {
        this.activity = activity;
        this.list = list;
        this.cWith = cWith;
        this.isMerchant = isMerchant;
    }

    public AdapterVoucherDiscoveryFragment(LixiActivity activity, MerchantVoucherFragment newVoucherFragment, ArrayList<LixiShopEvoucherModel> list, Boolean isMerchant, int cWith) {
        this.newVoucherFragment = newVoucherFragment;
        this.list = list;
        this.cWith = cWith;
        this.activity = activity;
        this.isMerchant = isMerchant;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DiscoveryRowVoucherFragmentBinding voucherFragmentBinding =
                DiscoveryRowVoucherFragmentBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);
        return new ViewHolder(voucherFragmentBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position == 0) {
            holder.viewTop.setVisibility(View.VISIBLE);
        } else {
            holder.viewTop.setVisibility(View.GONE);
        }

        if (position == list.size() - 1) {
            holder.viewBottom.setVisibility(View.GONE);
        } else {
            holder.viewBottom.setVisibility(View.VISIBLE);
        }

        ViewGroup.LayoutParams layoutParams = holder.img_voucher.getLayoutParams();
        layoutParams.width = cWith / 3 - 30;
        holder.img_voucher.setLayoutParams(layoutParams);

        if (list.get(position).getProduct_image() != null && list.get(position).getProduct_image_medium().size() > 0) {
            ImageLoader.getInstance().displayImage(list.get(position).getProduct_image_medium().get(0), holder.img_voucher, LixiApplication.getInstance().optionsNomal);
        } else
            ImageLoader.getInstance().displayImage(list.get(position).getProduct_logo(), holder.img_voucher, LixiApplication.getInstance().optionsNomal);

        holder.tv_name.setText(list.get(position).getName());
        if (list.get(position).getMerchant_name() != null)
            holder.tv_merchant_name.setText(list.get(position).getMerchant_name());
        else {
            holder.tv_merchant_name.setText(activity.getIntent().getStringExtra("NAMEMERCHANT"));
        }
        holder.tv_price.setText(Helper.getVNCurrency(list.get(position).getPayment_discount_price()) + "đ");
        holder.tv_lixi_caschback.setText("+" + Helper.getVNCurrency(list.get(position).getCashback_price()) + " Lixi");
        if (list.get(position).getQuantity_order_done() > 0)
            holder.tv_count_buyer.setText(Helper.getVNCurrency(list.get(position).getQuantity_order_done()) + " " + LanguageBinding.getString(R.string.quantity_bought_customer, activity));
        else
            holder.tv_count_buyer.setText(LanguageBinding.getString(R.string.first_customer, activity));

        if (!isMerchant) {

        } else {

        }

        holder.ll_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(activity, EvoucherDetail.class);
//                intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, list.get(position).getId());
//                intent.putExtra(ConstantValue.IS_COMBO,true);
//                activity.startActivity(intent);

                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list.get(position).getId(), null, ActivityProductCashEvoucherDetail.getParamIsComboItem(true));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_voucher;
        TextView tv_name;
        TextView tv_price;
        TextView tv_lixi_caschback;
        TextView tv_merchant_name;
        TextView tv_count_buyer;
        LinearLayout ll_count_buyer;
        LinearLayout ll_holder;
        ImageView ivPerson;
        View viewTop, viewBottom;

        public ViewHolder(View itemView) {
            super(itemView);

            img_voucher = itemView.findViewById(R.id.img_voucher);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_lixi_caschback = itemView.findViewById(R.id.tv_lixi_cashback);
            tv_merchant_name = itemView.findViewById(R.id.tv_merchant_name);
            tv_count_buyer = itemView.findViewById(R.id.tv_count_buyer);
            ll_count_buyer = itemView.findViewById(R.id.ll_count_buyer);
            ll_holder = itemView.findViewById(R.id.ll_holder);
            ivPerson = itemView.findViewById(R.id.img_view);
            viewTop = itemView.findViewById(R.id.view_line_top);
            viewBottom = itemView.findViewById(R.id.view_line_bottom);
        }


    }


}