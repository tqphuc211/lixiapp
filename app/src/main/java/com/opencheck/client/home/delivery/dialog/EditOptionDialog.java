package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogEditOptionBinding;
import com.opencheck.client.home.delivery.enum_key.TypeLocation;
import com.opencheck.client.home.delivery.interfaces.OnEdittingSelected;

public class EditOptionDialog extends LixiDialog {
    public static final int RESET = 0;
    public static final int REMOVE = 1;

    public EditOptionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private TextView txtReset, txtRemove, txtCancel;
    private DialogEditOptionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_edit_option,
                null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView() {
        txtReset = (TextView) findViewById(R.id.txtReset);
        txtRemove = (TextView) findViewById(R.id.txtRemove);
        txtCancel = (TextView) findViewById(R.id.txtCancel);

        txtReset.setOnClickListener(this);
        txtRemove.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtReset:
                if (onEdittingSelected != null){
                    onEdittingSelected.onSelected(RESET, action);
                    cancel();
                }
                break;
            case R.id.txtRemove:
                if (onEdittingSelected != null){
                    onEdittingSelected.onSelected(REMOVE, action);
                }
                break;
            case R.id.txtCancel:
                cancel();
                break;
        }
    }

    private OnEdittingSelected onEdittingSelected;
    private TypeLocation action = null;
    public void setOnEdittingSelected(TypeLocation action, OnEdittingSelected onEdittingSelected){
        this.onEdittingSelected= onEdittingSelected;
        this.action = action;
    }
}
