package com.opencheck.client.home.lixishop.detail;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.LixiShopEvoucherDetailActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.delivery.libs.RatingBar;
import com.opencheck.client.home.lixishop.payment.LixiShopConfirmBuyDialog;
import com.opencheck.client.home.rating.adapter.RatingAdapter;
import com.opencheck.client.home.rating.dialog.AllRatingDialog;
import com.opencheck.client.home.rating.dialog.ReviewRatingDialog;
import com.opencheck.client.home.rating.model.RatingModel;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.lixishop.ComboItemModel;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class EvoucherDetail extends LixiActivity {

    public static final String PRODUCT_ID = "PRODUCT_ID";
    private ImageView iv_share;
    private RelativeLayout rl_viewpager;
    private RelativeLayout rl_place;
    private RelativeLayout rlIndicator;
    private View v_back;
    private TextView tv_count_member;
    private TextView tv_payment_price;
    private TextView tv_name;
    private TextView tv_price;
    private TextView tv_promo;
    private TextView tv_promo_percent;
    private TextView tv_expire_day;
    private TextView tv_receive_content, tv_condition_content,
            tv_name_merchant, tv_time_open, tv_price_value, tv_store_address, tv_store_phone, tv_quantity_place;
    private ImageView iv_merchant;
    private LinearLayout ll_buy;
    private LinearLayout ll_current_point;
    private TextView tv_lixi;
    private TextView tv_buy;
    private ViewPager viewPager;
    private CirclePageIndicator indicator;
    private AdapterImageProductDetail adapterImageProductDetail;
    private ImageView iv_back;
    private ScrollView scroll_view;
    private TextView tv_chitietsanpham;
    private TextView tv_yuHave;
    private LinearLayout ll_receive, ll_condition, ll_merchant, linearRating, linearRatingList;
    private RatingBar ratingBar;
    private TextView txtRating;
    private RecyclerView recListRating;
    private TextView txtShowAll;
    private TextView txtReviewContent;
    private TextView txtShowDetail;

    private long idProduct;
    private LixiShopProductDetailModel productModel;
    private UserModel currentUser;
    private List<String> list;
    boolean isComboItem;
    private String source;
    private LinearLayout lnDetailCombo;
    private TextView tvListCombo;
    private RecyclerView rvListCombo;
    private AdapterDetailCombo adapterDetailCombo;
    private ArrayList<ComboItemModel> comboItemModelArrayList;
    private boolean isResume = false;

    private LixiShopEvoucherDetailActivityBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.lixi_shop_evoucher_detail_activity);
        idProduct = getIntent().getLongExtra(PRODUCT_ID, 0);
        isComboItem = getIntent().getBooleanExtra(ConstantValue.IS_COMBO_ITEM, false);
        source = getIntent().getStringExtra(GlobalTracking.SOURCE);
        findView();
        startLoadData();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //region Handle login

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                handleLoginResultAction(data);
                break;
        }
    }


    private void handleLoginResultAction(Intent intent) {
        Bundle data = intent.getExtras();
        String action = data.getString(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        switch (action) {
            case ConstantValue.ACTION_EXCHANGE_EVOUCHER:
                if (LoginHelper.isLoggedIn(activity)) {
                    loadUserInfoToExchange();
                } else {
                    ll_current_point.setVisibility(View.GONE);
                }
                break;
        }

    }

    //endregion

    private int heightWith169Aspect = 0;

    private void findView() {
        tv_yuHave = findViewById(R.id.tv_yuHave);
//        ll_info = findViewById(R.id.ll_info);
        ll_current_point = findViewById(R.id.ll_current_point);
        rlIndicator = findViewById(R.id.rlIndicator);
        rl_place = findViewById(R.id.rl_place);
        tv_count_member = findViewById(R.id.tv_count_member);
        tv_name = findViewById(R.id.tv_name);
        tv_price = findViewById(R.id.tv_price);
        tv_promo = findViewById(R.id.tv_promo);
        tv_payment_price = findViewById(R.id.tv_payment_price);
        tv_promo_percent = findViewById(R.id.tv_promo_percent);
        ll_buy = findViewById(R.id.ll_buy);
        tv_lixi = findViewById(R.id.tv_lixi);
        tv_buy = findViewById(R.id.tv_buy);
        viewPager = findViewById(R.id.viewpager);
        indicator = findViewById(R.id.indicator);
        iv_back = findViewById(R.id.iv_back);
        iv_share = findViewById(R.id.iv_share);
        rl_viewpager = findViewById(R.id.rl_viewpager);
        v_back = findViewById(R.id.v_back);
        scroll_view = findViewById(R.id.scroll_view);
        tv_chitietsanpham = findViewById(R.id.tv_chitietsanpham);
        tv_expire_day = findViewById(R.id.tv_expire_day);
        tv_receive_content = findViewById(R.id.tv_receive_content);
        tv_condition_content = findViewById(R.id.tv_condition_content);
        tv_name_merchant = findViewById(R.id.tv_name_merchant);
        tv_time_open = findViewById(R.id.tv_time_open);
        tv_price_value = findViewById(R.id.tv_price_value);
        tv_store_phone = findViewById(R.id.tv_store_phone);
        tv_store_address = findViewById(R.id.tv_store_address);
        tv_quantity_place = findViewById(R.id.tv_quantity_place);
        iv_merchant = findViewById(R.id.iv_merchant);
        ll_receive = findViewById(R.id.ll_receive);
        ll_condition = findViewById(R.id.ll_condition);
        ll_merchant = findViewById(R.id.ll_merchant);
        linearRating = findViewById(R.id.linearRating);
        linearRatingList = findViewById(R.id.linearRatingList);
        lnDetailCombo = findViewById(R.id.lnDetailCombo);
        tvListCombo = findViewById(R.id.tvListCombo);
        rvListCombo = findViewById(R.id.rvListCombo);
        ll_buy.setVisibility(View.VISIBLE);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        txtRating = (TextView) findViewById(R.id.txtRating);
        recListRating = (RecyclerView) findViewById(R.id.recListRating);
        txtShowAll = (TextView) findViewById(R.id.txtShowAll);
        txtReviewContent = (TextView) findViewById(R.id.txtReviewContent);
        txtShowDetail = (TextView) findViewById(R.id.txtShowDetail);

        //set ratio for viewpager
        rl_viewpager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int availableHeight = rl_viewpager.getMeasuredHeight();
                if (availableHeight > 0) {
                    rl_viewpager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    //save height here and do whatever you want with it
                    heightWith169Aspect = rl_viewpager.getWidth() * 9 / 16;

                    ViewGroup.LayoutParams params = rl_viewpager.getLayoutParams();
                    params.height = heightWith169Aspect;
                    rl_viewpager.setLayoutParams(params);
                }
            }
        });

        tv_promo.setPaintFlags(tv_promo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        tv_buy.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        iv_share.setOnClickListener(this);
        rl_place.setOnClickListener(this);
        txtShowDetail.setOnClickListener(this);
        txtShowAll.setOnClickListener(this);

        scroll_view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int y = scroll_view.getScrollY();
                Helper.setScrollProrgess(y, v_back, tv_chitietsanpham, iv_back, iv_share);
            }
        });

    }

    public void startLoadData() {
        DataLoader.getProductDetail(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    productModel = (LixiShopProductDetailModel) object;
                    GlobalTracking.trackShowDetail(
                            TrackingConstant.ContentType.EVOUCHER,
                            idProduct,
                            TrackingConstant.Currency.VND,
                            productModel.getPrice(),
                            source
                    );
                    showData();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, idProduct);
    }

    public void showData() {
        tv_yuHave.setText(LanguageBinding.getString(R.string.detail_product_own, activity));
        tv_name.setText(productModel.getName());
        tv_chitietsanpham.setText(productModel.getName());

        if (productModel.getProduct_type().equals("evoucher")) {
            linearRating.setVisibility(View.VISIBLE);
            ratingBar.setCount((int) productModel.getRating_info().getAverage_rating());
            txtRating.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_rating, activity), productModel.getRating_info().getTotal_rating()));
        } else {
            linearRating.setVisibility(View.GONE);
        }

        tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (productModel.getCount_attend() > 0)
            tv_count_member.setText(String.format(LanguageBinding.getString(R.string.quantity_bought_customer, activity), productModel.getCount_attend()));
        else
            tv_count_member.setText(LanguageBinding.getString(R.string.first_customer, activity));

        tv_promo.setText(Helper.getVNCurrency(productModel.getPrice()) + LanguageBinding.getString(R.string.p, activity));
        tv_price.setText(Helper.getVNCurrency(productModel.getDiscount_price()) + " " + LanguageBinding.getString(R.string.lixi, activity));
        tv_payment_price.setText(Helper.getVNCurrency(productModel.getPayment_discount_price()) + " VNĐ");
        tv_promo_percent.setText(" - " + productModel.getDiscount() + "%");

//        wv_product_detail.getSettings().setJavaScriptEnabled(true);

        if (productModel.getProduct_type().equalsIgnoreCase("combo")) {
            rl_place.setVisibility(View.GONE);
            ll_merchant.setVisibility(View.GONE);
            lnDetailCombo.setVisibility(View.VISIBLE);
        } else {
            rl_place.setVisibility(View.VISIBLE);
            ll_merchant.setVisibility(View.VISIBLE);
            lnDetailCombo.setVisibility(View.GONE);
        }

        if (LoginHelper.isLoggedIn(activity)) {
            currentUser = UserModel.getInstance();

            tv_lixi.setText(Helper.getVNCurrency(Long.parseLong(currentUser.getAvailable_point())) + " " + LanguageBinding.getString(R.string.lixi, activity));
            if (Long.parseLong(currentUser.getAvailable_point()) < productModel.getDiscount_price()) {
                tv_buy.setText(LanguageBinding.getString(R.string.detail_product_need, activity) + " " + Helper.getVNCurrency(productModel.getDiscount_price() - Integer.parseInt(currentUser.getAvailable_point())) + " " + LanguageBinding.getString(R.string.lixi, activity));
                tv_buy.setBackground(getResources().getDrawable(R.drawable.bg_signup_black));
                tv_buy.setClickable(false);
            } else {
                tv_buy.setText(LanguageBinding.getString(R.string.detail_product_receive, activity));
                tv_buy.setClickable(true);
            }
        } else {
            tv_buy.setText(LanguageBinding.getString(R.string.detail_product_receive, activity));
            tv_buy.setClickable(true);
        }

        if (productModel.getProduct_image() != null && productModel.getProduct_image().size() > 0)
            list = productModel.getProduct_image();
        else
            list = new ArrayList<>();
        //set pager

        adapterImageProductDetail = new AdapterImageProductDetail(this, list);
        adapterImageProductDetail.setData(EvoucherDetail.this, productModel.getName());
        viewPager.setAdapter(adapterImageProductDetail);
        rlIndicator.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.value_10) * (adapterImageProductDetail.getCount() + 1);
        rlIndicator.requestLayout();
        indicator.setViewPager(viewPager);

        tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.evoucher_detail_use_to, activity), Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(Long.parseLong(productModel.getContract_end_date()) * 1000))));
        if (productModel.getWhat_you_get() != null && !productModel.getWhat_you_get().equals("")) {
            tv_receive_content.setText(productModel.getWhat_you_get());
            ll_receive.setVisibility(View.VISIBLE);
        } else
            ll_receive.setVisibility(View.GONE);

        if (productModel.getUse_condition() != null && !productModel.getUse_condition().equals("")) {
            tv_condition_content.setText(productModel.getUse_condition());
            ll_condition.setVisibility(View.VISIBLE);
        } else {
            ll_condition.setVisibility(View.GONE);
        }

        setMerchant();

        comboItemModelArrayList = new ArrayList<>();
        comboItemModelArrayList.addAll(productModel.getCombo_item());

        adapterDetailCombo = new AdapterDetailCombo(this, comboItemModelArrayList);
        rvListCombo.setAdapter(adapterDetailCombo);

        rvListCombo.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        tvListCombo.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_list_product, activity), comboItemModelArrayList.size()));
        if (isComboItem) {
            tv_price.setText(Helper.getVNCurrency(productModel.getDiscount_price()) + " Lixi");
            ll_buy.setVisibility(View.GONE);
        }
        if (productModel.getExpired_text().length() == 0 || productModel.getExpired_text().equals("")) {
            String date = Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y, String.valueOf(Long.parseLong(productModel.getContract_end_date()) * 1000));
            if (productModel.getProduct_type().equals("combo")) {
                tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_apply_to, activity), date));
            } else
                tv_expire_day.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_expired, activity), date));
        } else tv_expire_day.setText(productModel.getExpired_text());

        setReview();
    }

    private void setReview() {
        txtReviewContent.setText(productModel.getHtml_strip_text()
                .replace("\n", "")
                .replace("\t", "")
                .trim()
        );

        DataLoader.getListRating(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<RatingModel> listRating = (ArrayList<RatingModel>) object;
                    if (listRating != null && listRating.size() != 0) {
                        if (listRating.size() > 3) {
                            txtShowAll.setVisibility(View.VISIBLE);
                        } else {
                            txtShowAll.setVisibility(View.GONE);
                        }
                        linearRatingList.setVisibility(View.VISIBLE);
                        RatingAdapter adapter = new RatingAdapter(activity, listRating, true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recListRating.setLayoutManager(layoutManager);
                        recListRating.setAdapter(adapter);
                    } else {
                        linearRatingList.setVisibility(View.GONE);
                    }
                }
            }
        }, (int) productModel.getId(), 1, 4);
    }

    private void setMerchant() {
        ImageLoader.getInstance().displayImage(productModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
        tv_name_merchant.setText(productModel.getMerchant_info().getName());
        tv_time_open.setText(LanguageBinding.getString(R.string.open_time_title,activity) + " " + productModel.getMerchant_info().getOpening_time() + " - " + productModel.getMerchant_info().getClose_time());
        tv_price_value.setText(LanguageBinding.getString(R.string.price_title,activity) + " " + Helper.getVNCurrency(productModel.getMerchant_info().getMin_price()) + " - " + Helper.getVNCurrency(productModel.getMerchant_info().getMax_price()));
        tv_quantity_place.setText(String.format(LanguageBinding.getString(R.string.evoucher_detail_apply_at, activity), productModel.getStore_apply().size()));
        if (productModel.getStore_apply().size() > 1) {
            rl_place.setVisibility(View.VISIBLE);
            tv_store_address.setVisibility(View.GONE);
            tv_store_phone.setVisibility(View.GONE);
        } else if (productModel.getStore_apply().size() == 1) {
            rl_place.setVisibility(View.GONE);
            if (!productModel.getStore_apply().get(0).getAddress().equals(""))
                tv_store_address.setText(productModel.getStore_apply().get(0).getAddress());
            else
                tv_store_address.setVisibility(View.GONE);
            if (!productModel.getStore_apply().get(0).getPhone().equals(""))
                tv_store_phone.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), productModel.getStore_apply().get(0).getPhone()));
            else
                tv_store_phone.setVisibility(View.GONE);
        }
    }

    //    private long getRemainingDate(String discount_end_date) {
//        long endDate = Long.parseLong(discount_end_date);
//        long diff =  endDate - System.currentTimeMillis();
//        long days = diff / (24 * 60 * 60 * 1000);
//        return days;
//    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_buy:
                if (LoginHelper.isLoggedIn(activity)) {
                    if (Long.parseLong(currentUser.getAvailable_point()) < productModel.getDiscount_price()) {

                    } else {
                        LixiShopConfirmBuyDialog popupLixiShopConfirmBuy = new LixiShopConfirmBuyDialog(EvoucherDetail.this, false, false, false);
                        popupLixiShopConfirmBuy.show();
                        popupLixiShopConfirmBuy.setData(-1, String.valueOf(productModel.getId()), productModel);
                        popupLixiShopConfirmBuy.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                reloadPoint();
                            }
                        });
                    }
                } else {
                    LoginHelper.startLoginActivityWithAction(activity, ConstantValue.ACTION_EXCHANGE_EVOUCHER);
                }
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_share:
                if (productModel.getShare_link() != null) {
                    ShareDialog shareDialog = new ShareDialog(this);
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(productModel.getShare_link()))
                                .build();
                        shareDialog.show(linkContent);
                    }
                } else
                    Toast.makeText(this, LanguageBinding.getString(R.string.cash_evoucher_cannot_share, activity), Toast.LENGTH_SHORT).show();
                break;
            case R.id.rl_place:
                Intent intent = new Intent(this, StoreListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(ConstantValue.STORE_LIST, new Gson().toJson(productModel));
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.txtShowDetail:
                ReviewRatingDialog dialog = new ReviewRatingDialog(activity, false, false, false);
                dialog.show();
                dialog.setData(productModel.getHtml_text());
                break;
            case R.id.txtShowAll:
                AllRatingDialog allRatingDialog = new AllRatingDialog(activity, false, false, false);
                allRatingDialog.show();
                allRatingDialog.setData(productModel.getId());
                break;
        }
    }

    @Override
    protected void onResume() {
        id = idProduct;
        super.onResume();
        if (LoginHelper.isLoggedIn(activity)) {
            loadUserInfo();
        } else {
            ll_current_point.setVisibility(View.GONE);
        }
    }

    private void loadUserInfo() {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    currentUser = UserModel.getInstance();
//                    try {
//                        createInSocket();
//                    } catch (URISyntaxException e) {
//                        e.printStackTrace();
//                    }
                    if (currentUser != null)
                        tv_lixi.setText(Helper.getVNCurrency(Long.parseLong(currentUser.getAvailable_point())) + " " + LanguageBinding.getString(R.string.lixi, activity));
                } else {
                    Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.error_user_info, activity));
                }
            }
        });
    }

    private void loadUserInfoToExchange() {
        DataLoader.getUserProfile(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {

                if (isSuccess) {
                    currentUser = UserModel.getInstance();

                    if (!LoginHelper.checkVerifyUser()) {
                        tv_buy.setText(LanguageBinding.getString(R.string.detail_product_receive, activity));
                        tv_buy.setClickable(true);
                        tv_buy.setBackground(getResources().getDrawable(R.drawable.bg_signup_gray));
                        return;
                    }

                    if (currentUser != null)
                        tv_lixi.setText(Helper.getVNCurrency(Long.parseLong(currentUser.getAvailable_point())) + " " + LanguageBinding.getString(R.string.lixi, activity));
                    ll_current_point.setVisibility(View.VISIBLE);

                    if (Long.parseLong(currentUser.getAvailable_point()) < productModel.getDiscount_price()) {
                        tv_buy.setText(LanguageBinding.getString(R.string.detail_product_need, activity) + " " + Helper.getVNCurrency(productModel.getDiscount_price() - Integer.parseInt(currentUser.getAvailable_point())) + " " + LanguageBinding.getString(R.string.lixi, activity));
                        tv_buy.setBackground(getResources().getDrawable(R.drawable.bg_signup_black));
                        tv_buy.setClickable(false);
                    } else {
                        LixiShopConfirmBuyDialog popupLixiShopConfirmBuy = new LixiShopConfirmBuyDialog(EvoucherDetail.this, false, false, false);
                        popupLixiShopConfirmBuy.show();
                        popupLixiShopConfirmBuy.setData(-1, String.valueOf(productModel.getId()), productModel);
                        popupLixiShopConfirmBuy.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                reloadPoint();
                            }
                        });
                    }
                } else {
                    Helper.showErrorDialog(activity, LanguageBinding.getString(R.string.error_user_info, activity));
                }

            }
        });
    }

    private void reloadPoint() {
        startLoadData();
        loadUserInfo();
    }

    //region global

//    public LixiShopProductDetailModel getProductModel() {
//        return productModel;
//    }

//    public UserModel getCurrentUser() {
//        return currentUser;
//    }
//
//
//    public List<Dialog> PopupList = new ArrayList<>();
//
//    public boolean dismissPopup(String PopupClassName) throws ClassNotFoundException {
//        Class popupClass = Class.forName(PopupClassName);
//        cleanPopupList();
//        for (int i = 0; i < PopupList.size(); i++) {
//            if (popupClass.isInstance(PopupList.get(i))) {
//                PopupList.get(i).dismiss();
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public int cleanPopupList() {
//        int nullPopup = 0;
//        for (int i = 0; i < PopupList.size(); i++) {
//            if (PopupList.get(i) == null) {
//                nullPopup++;
//                PopupList.remove(i);
//            }
//        }
//        return nullPopup;
//    }

    //endregion

    //region TRACKING LIXI

//    private void createInSocket() throws URISyntaxException {
//        try {
//            SocketMessageModel message = new SocketMessageModel();
//            message.setName("navigate");
//
//            SocketDataModel data = new SocketDataModel();
//            data.setAction(ConstantValue.SOCKET_IN_ACTION);
//            data.setName(getScreenName());
//
//            ParamsModel paramsModel = new ParamsModel();
//            paramsModel.setId(idProduct);
//            JSONObject id = new JSONObject();
//            id.put("id", idProduct);
//            data.setParams(paramsModel);
//
//            SocketMetaModel meta = new SocketMetaModel();
//            meta.setPlatform("android");
//            if (DataLoader.userModel != null)
//                meta.setUser_id(DataLoader.userModel.getId());
//
//            Long tsLong = System.currentTimeMillis();
//            meta.setTimestamp(tsLong);
//            SocketDeviceModel deviceModel = new SocketDeviceModel();
//            deviceModel.setDevice_id(Helper.getDeviceID(activity));
//            meta.setDevice(deviceModel);
//
//            message.setData(data);
//            message.setMeta(meta);
//
//
//            final JSONObject jsonMess = new JSONObject();
//            jsonMess.put("message", new JSONObject(new Gson().toJson(message)));
//
//            String tokenWithoutJWT = DataLoader.token;
//            tokenWithoutJWT = tokenWithoutJWT.replaceFirst("JWT ", "");
//            IO.Options opts = new IO.Options();
//            opts.forceNew = true;
//            opts.query = "token=" + tokenWithoutJWT;
//            opts.transports = new String[]{WebSocket.NAME};
//            final String ms = LZString.compressToUTF16(new Gson().toJson(message));
//            socket = IO.socket(BuildConfig.SOCKET, opts);
//            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    socket.emit("tracking", jsonMess);
//                    Helper.showLog("Socket Connected");
////                     socket.disconnect();
//                }
//
//            }).on("tracking", new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket Received Data");
//                    JSONObject obj = (JSONObject) args[0];
//                    Helper.showLog(obj.toString());
//                }
//
//            }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket timeout");
//                }
//
//            }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket ERROR");
//                }
//
//            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket CONNECT ERROR");
//                }
//
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket disconnected");
//                }
//
//            });
//            socket.connect();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

//    private void createOutSocket() throws URISyntaxException {
//        try {
//            SocketMessageModel message = new SocketMessageModel();
//            message.setName("navigate");
//
//            SocketDataModel data = new SocketDataModel();
//            data.setName(getActivityName());
//            data.setAction(ConstantValue.SOCKET_OUT_ACTION);
//            ParamsModel paramsModel = new ParamsModel();
//            paramsModel.setId(idProduct);
//            JSONObject id = new JSONObject();
//            id.put("id", idProduct);
//            data.setParams(paramsModel);
//            SocketMetaModel meta = new SocketMetaModel();
//            meta.setPlatform("android");
//            if (DataLoader.userModel != null)
//                meta.setUser_id(DataLoader.userModel.getId());
//
//            Long tsLong = System.currentTimeMillis();
//            meta.setTimestamp(tsLong);
//            SocketDeviceModel deviceModel = new SocketDeviceModel();
//            deviceModel.setDevice_id(Helper.getDeviceID(activity));
//            meta.setDevice(deviceModel);
//
//            message.setData(data);
//            message.setMeta(meta);
//
//
//            final JSONObject jsonMess = new JSONObject();
//            jsonMess.put("message", new JSONObject(new Gson().toJson(message)));
//            final String ms = LZString.compressToUTF16(new Gson().toJson(message));
//            if (socket.connected()) {
//                socket.emit("tracking", jsonMess);
//                return;
//            }
//
//            String tokenWithoutJWT = DataLoader.token;
//            tokenWithoutJWT = tokenWithoutJWT.replaceFirst("JWT ", "");
//            IO.Options opts = new IO.Options();
//            opts.forceNew = true;
//            opts.query = "token=" + tokenWithoutJWT;
//            opts.transports = new String[]{WebSocket.NAME};
//            socket = IO.socket(ApiEntity.SOCKET, opts);
//            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    socket.emit("tracking", jsonMess);
//                    Helper.showLog("Socket Connected");
////                     socket.disconnect();
//                }
//
//            }).on("tracking", new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket Received Data");
//                    JSONObject obj = (JSONObject) args[0];
//                    Helper.showLog(obj.toString());
//                }
//
//            }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket timeout");
//                }
//
//            }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket ERROR");
//                }
//
//            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket CONNECT ERROR");
//                }
//
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//
//                @Override
//                public void call(Object... args) {
//                    Helper.showLog("Socket disconnected");
//                }
//
//            });
//            socket.connect();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
    //endregion

    //region handle font for product webview
    // The class which loads the TTF file, parses it and returns the TTF font name
//    class TTFAnalyzer {
//        // This function parses the TTF file and returns the font name specified in the file
//        public String getTtfFontName(String fontFilename) {
//            try {
//                // Parses the TTF file format.
//                // See http://developer.apple.com/fonts/ttrefman/rm06/Chap6.html
//                m_file = new RandomAccessFile(fontFilename, "r");
//
//                // Read the version first
//                int version = readDword();
//
//                // The version must be either 'true' (0x74727565) or 0x00010000
//                if (version != 0x74727565 && version != 0x00010000)
//                    return null;
//
//                // The TTF file consist of several sections called "tables", and we need to know how many of them are there.
//                int numTables = readWord();
//
//                // Skip the rest in the header
//                readWord(); // skip searchRange
//                readWord(); // skip entrySelector
//                readWord(); // skip rangeShift
//
//                // Now we can read the tables
//                for (int i = 0; i < numTables; i++) {
//                    // Read the table entry
//                    int tag = readDword();
//                    readDword(); // skip checksum
//                    int offset = readDword();
//                    int length = readDword();
//
//                    // Now here' the trick. 'name' field actually contains the textual string name.
//                    // So the 'name' string in characters equals to 0x6E616D65
//                    if (tag == 0x6E616D65) {
//                        // Here's the name section. Read it completely into the allocated buffer
//                        byte[] table = new byte[length];
//
//                        m_file.seek(offset);
//                        read(table);
//
//                        // This is also a table. See http://developer.apple.com/fonts/ttrefman/rm06/Chap6name.html
//                        // According to Table 36, the total number of table records is stored in the second word, at the offset 2.
//                        // Getting the count and string offset - remembering it's big endian.
//                        int count = getWord(table, 2);
//                        int string_offset = getWord(table, 4);
//
//                        // Record starts from offset 6
//                        for (int record = 0; record < count; record++) {
//                            // Table 37 tells us that each record is 6 words -> 12 bytes, and that the nameID is 4th word so its offset is 6.
//                            // We also need to account for the first 6 bytes of the header above (Table 36), so...
//                            int nameid_offset = record * 12 + 6;
//                            int platformID = getWord(table, nameid_offset);
//                            int nameid_value = getWord(table, nameid_offset + 6);
//
//                            // Table 42 lists the valid name Identifiers. We're interested in 4 but not in Unicode encoding (for simplicity).
//                            // The encoding is stored as PlatformID and we're interested in Mac encoding
//                            if (nameid_value == 4 && platformID == 1) {
//                                // We need the string offset and length, which are the word 6 and 5 respectively
//                                int name_length = getWord(table, nameid_offset + 8);
//                                int name_offset = getWord(table, nameid_offset + 10);
//
//                                // The real name string offset is calculated by adding the string_offset
//                                name_offset = name_offset + string_offset;
//
//                                // Make sure it is inside the array
//                                if (name_offset >= 0 && name_offset + name_length < table.length)
//                                    return new String(table, name_offset, name_length);
//                            }
//                        }
//                    }
//                }
//
//                return null;
//            } catch (FileNotFoundException e) {
//                // Permissions?
//                return null;
//            } catch (IOException e) {
//                // Most likely a corrupted font file
//                return null;
//            }
//        }
//
//        // Font file; must be seekable
//        private RandomAccessFile m_file = null;
//
//        // Helper I/O functions
//        private int readByte() throws IOException {
//            return m_file.read() & 0xFF;
//        }
//
//        private int readWord() throws IOException {
//            int b1 = readByte();
//            int b2 = readByte();
//
//            return b1 << 8 | b2;
//        }
//
//        private int readDword() throws IOException {
//            int b1 = readByte();
//            int b2 = readByte();
//            int b3 = readByte();
//            int b4 = readByte();
//
//            return b1 << 24 | b2 << 16 | b3 << 8 | b4;
//        }
//
//        private void read(byte[] array) throws IOException {
//            if (m_file.read(array) != array.length)
//                throw new IOException();
//        }
//
//        // Helper
//        private int getWord(byte[] array, int offset) {
//            int b1 = array[offset] & 0xFF;
//            int b2 = array[offset + 1] & 0xFF;
//
//            return b1 << 8 | b2;
//        }
//    }

//    public String getDefaultFont() {
//        System.out.println("getFontList(): entry");
//        File configFilename = new File("/system/etc/system_fonts.xml");
//        String defaultFontName = "";
//        TTFAnalyzer analyzer = new TTFAnalyzer();
//
//        try {
//            FileInputStream fontsIn = new FileInputStream(configFilename);
//            XmlPullParser parser = Xml.newPullParser();
//            parser.setInput(fontsIn, null);
//            Boolean done = false;
//            Boolean getTheText = false;
//            int eventType;
//            String defaultFont = "";
//            while (!done) {
//                eventType = parser.next();
//                if (eventType == parser.START_TAG && parser.getName().equalsIgnoreCase("file")) {
//                    // the text is next up -- pull it
//                    getTheText = true;
//                }
//                if (eventType == parser.TEXT && getTheText == true) {
//                    // first name
//                    defaultFont = parser.getText();
//                    System.out.println("text for file tag:" + defaultFont);
//                    done = true;
//                }
//                if (eventType == parser.END_DOCUMENT) {
//                    System.out.println("hit end of system_fonts.xml document");
//                    done = true;
//                }
//            }
//
//            if (defaultFont.length() > 0) {
//                // found the font filename, most likely in /system/fonts. Now pull out the human-readable
//                // string from the font file
//                System.out.println("Figuring out default Font info");
//                String fontname = analyzer.getTtfFontName("/system/fonts/" + defaultFont);
//                if (fontname != null) {
//                    System.out.println("found font info: " + fontname);
//                    defaultFontName = fontname;
//                }
//            }
//
//        } catch (RuntimeException e) {
//            System.err.println("Didn't create default family (most likely, non-Minikin build)");
//        } catch (FileNotFoundException e) {
//            System.err.println("GetDefaultFont: config file Not found");
//        } catch (IOException e) {
//            System.err.println("GetDefaultFont: IO exception: " + e.getMessage());
//        } catch (XmlPullParserException e) {
//            System.err.println("getDefaultFont: XML parse exception " + e.getMessage());
//        }
//        return defaultFontName;
//    }
    //endregion


}
