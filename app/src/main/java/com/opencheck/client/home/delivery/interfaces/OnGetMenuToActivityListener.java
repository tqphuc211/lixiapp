package com.opencheck.client.home.delivery.interfaces;

import com.opencheck.client.home.delivery.model.MenuModel;

import java.util.ArrayList;

public interface OnGetMenuToActivityListener {
    void onOk(ArrayList<MenuModel> menu);
}
