package com.opencheck.client.home.delivery.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemStoreDetailRatingBinding;
import com.opencheck.client.home.delivery.model.UserFeedbackModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.List;

public class UserFeedbackAdapter extends RecyclerView.Adapter<UserFeedbackAdapter.ViewHolder> {

    private List<UserFeedbackModel> list;
    private Context context;

    public UserFeedbackAdapter(Context context, List<UserFeedbackModel> list) {
        this.list = list;
        this.context = context;
    }

    public void setData(List<UserFeedbackModel> list) {
        if(list != null && list.size() > 0){
            this.list.addAll(list);
            notifyDataSetChanged();
        }
    }

    public List<UserFeedbackModel> getList() {
        return list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemStoreDetailRatingBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_store_detail_rating, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        UserFeedbackModel feedback = list.get(position);

        holder.binding.tvName.setText(feedback.getUser_name());
        holder.binding.tvDate.setText(Helper.getFormatDateVN("dd/MM/yyyy", feedback.getCreate_date()));
        if(feedback.getMessage() == null || feedback.getMessage().isEmpty()){
            holder.binding.llContent.setVisibility(View.GONE);
        }else{
            holder.binding.tvContent.setText(Html.fromHtml(String.format(LanguageBinding.getString(R.string.content, context), feedback.getMessage())));
        }
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            boolean toggle = true;

            @Override
            public void onClick(View v) {
                if(toggle){
                    holder.binding.tvContent.setSingleLine(false);
                }else{
                    holder.binding.tvContent.setSingleLine(true);
                }
                toggle = !toggle;
            }
        });
        if(feedback.getUser_avatar() != null && !feedback.getUser_avatar().isEmpty()){
            ImageLoader.getInstance().displayImage(feedback.getUser_avatar(), holder.binding.cimgAvatar);
        }
        for(int i = 0; i < 5; i++){
            holder.binding.llStars.getChildAt(i).setVisibility(View.INVISIBLE);
        }
        for(int i = 0; i < feedback.getRating(); i++){
            holder.binding.llStars.getChildAt(i).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemStoreDetailRatingBinding binding;
        public ViewHolder(ItemStoreDetailRatingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
