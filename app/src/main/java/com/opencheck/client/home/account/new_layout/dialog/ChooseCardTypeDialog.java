package com.opencheck.client.home.account.new_layout.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogChooseCardTypeBinding;
import com.opencheck.client.home.account.new_layout.communicate.OnCreateCardResultListener;
import com.opencheck.client.home.account.new_layout.communicate.OnRefreshListener;
import com.opencheck.client.home.product.bank_card.SavedBankCardAdapter;
import com.opencheck.client.home.product.communicate.OnDeleteBankCard;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.ArrayList;

public class ChooseCardTypeDialog extends LixiDialog {

    public ChooseCardTypeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    ImageView imgBack;
    LinearLayout linearATM, linearVisa;

    private DialogChooseCardTypeBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogChooseCardTypeBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        linearATM = (LinearLayout) findViewById(R.id.linearATM);
        linearVisa = (LinearLayout) findViewById(R.id.linearVisa);

        imgBack.setOnClickListener(this);
        linearATM.setOnClickListener(this);
        linearVisa.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                cancel();
                break;
            case R.id.linearATM:
                createNewCard("napas_atm");
                break;
            case R.id.linearVisa:
                createNewCard("napas_cc");
                break;
        }
    }

    private void createNewCard(String key){
        CreateNewCardDialog createNewCardDialog = new CreateNewCardDialog(activity, false, false, false);
        createNewCardDialog.show();
        createNewCardDialog.setData(key);
        createNewCardDialog.addOnCreateCardResult(new OnCreateCardResultListener() {
            @Override
            public void onSuccess() {
                showNotifyDialog(activity.getString(R.string.notification_label), activity.getString(R.string.noti_add_card_success), true);
            }

            @Override
            public void onFail() {
                showNotifyDialog(activity.getString(R.string.noti_add_card_fail_title), activity.getString(R.string.noti_add_card_fail_content), false);
            }

            @Override
            public void onCardExisted() {
                showNotifyDialog(activity.getString(R.string.notification_label), activity.getString(R.string.noti_card_existed), true);
            }
        });
    }

    public void showNotifyDialog(String title, String content, final boolean cancel) {
        NotificationForCardDialog dialog = new NotificationForCardDialog(activity, false, false, false);
        dialog.show();
        dialog.setData(title, content);
        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (cancel){
                    onRefreshListener.onRefresh();
                    ChooseCardTypeDialog.this.cancel();
                }
            }
        });
    }

    private OnRefreshListener onRefreshListener;
    public void setOnRefreshListener(OnRefreshListener onRefreshListener){
        this.onRefreshListener = onRefreshListener;
    }
}
