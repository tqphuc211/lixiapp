package com.opencheck.client.home.account.new_layout.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityIntroCodeBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.account.new_layout.fragment.HistoryIntroFragment;
import com.opencheck.client.home.newlogin.communicate.OnLoginResult;
import com.opencheck.client.home.newlogin.dialog.UpdatePasswordDialog;
import com.opencheck.client.home.newlogin.dialog.UpdateUserInfoDialog;
import com.opencheck.client.home.setting.dialog.LearnMoreDialog;
import com.opencheck.client.login.LoginActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

public class IntroCodeActivity extends LixiActivity {

    public static void start(Context context) {
        if (LoginHelper.isLoggedIn(context)) {
            try {
                HomeActivity.getInstance().navigation.setSelectedItemId(R.id.navigation_user_info);
            } catch (Exception e) {
//                Log.d("RRRR", e.getMessage());
            }
        }
        Intent intent = new Intent(context, IntroCodeActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.PROFILE_INVITE_CODE;
    }

    private ActivityIntroCodeBinding mBinding;
    private AppPreferenceHelper appPreferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalTracking.trackScreenReferenceCodeStartsEvent(LoginHelper.isLoggedIn(activity));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_intro_code);
        appPreferenceHelper = new AppPreferenceHelper(activity);
        initView();
        initEvent();
    }

    private void initView() {
        if (UserModel.getInstance() != null) {
            mBinding.txtIntroCode.setText(UserModel.getInstance().getUser_code());
            try {
                GlobalTracking.trackShowDetail(
                        TrackingConstant.ContentType.REFERENCE_CODE,
                        Long.parseLong(UserModel.getInstance().getId()),
                        TrackingConstant.Currency.VND,
                        0,
                        null
                );
            } catch (Exception e) {

            }
        }
        mBinding.txtLearnMore.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        mBinding.txtRewardContent.setText(ConfigModel.getInstance(activity).getReference_code_lixi_text());

        if (!LoginHelper.isLoggedIn(activity)) {
            mBinding.txtIntroCode.setVisibility(View.GONE);
            mBinding.txtHintPress.setVisibility(View.GONE);
            mBinding.txtHistory.setVisibility(View.GONE);
            mBinding.txtIntroContent.setText(LanguageBinding.getString(R.string.intro_code_login_to_get, activity));
            mBinding.txtShare.setText(LanguageBinding.getString(R.string.login, activity));
            mBinding.linearIntroCode.setEnabled(false);
        } else {
            mBinding.txtIntroCode.setVisibility(View.VISIBLE);
            mBinding.txtHintPress.setVisibility(View.VISIBLE);
            mBinding.txtHistory.setVisibility(View.VISIBLE);
            mBinding.txtIntroContent.setText(LanguageBinding.getString(R.string.intro_code_owner_code, activity));
            mBinding.txtShare.setText(LanguageBinding.getString(R.string.intro_code_share, activity));
            mBinding.linearIntroCode.setEnabled(true);
        }
    }

    private void initEvent() {
        mBinding.imgBack.setOnClickListener(this);
        mBinding.txtHistory.setOnClickListener(this);
        mBinding.linearIntroCode.setOnClickListener(this);
        mBinding.txtShare.setOnClickListener(this);
        mBinding.txtLearnMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.txtHistory:
                showHistory();
                break;
            case R.id.linearIntroCode:
                Helper.copyCode(mBinding.txtIntroCode.getText().toString(), activity);
                break;
            case R.id.txtShare:
                if (LoginHelper.isLoggedIn(activity)) {
                    GlobalTracking.trackReferenceCodeShareEvent();
                    String content = ConfigModel.getInstance(activity)
                            .getReference_code_share_content()
                            .replace("<code>", mBinding.txtIntroCode.getText().toString());
                    Helper.shareLink(activity, content, true);
                } else {
                    // login
                    try {
                        new AppPreferenceHelper(this).setUpdateTokenInterval(0l);
                    } catch (Exception ex) {
                    }
                    Intent login = new Intent(this, LoginActivity.class);
                    login.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantValue.ACTION_AFTER_LOGIN, ConstantValue.ACTION_INTRO_CODE);
                    login.putExtras(bundle);
                    startActivityForResult(login, ConstantValue.LOGIN_REQUEST_CODE);
                }
                break;
            case R.id.txtLearnMore:
                showLearnMoreDialog();
                break;
        }
    }

    private void showHistory() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frameParent, new HistoryIntroFragment())
                .addToBackStack(null)
                .commit();
    }

    private void showLearnMoreDialog() {
        if (ConfigModel.getInstance(this).getReference_code_guide_link() == null || ConfigModel.getInstance(this).getReference_code_guide_link().equals("")) {
            return;
        }
        LearnMoreDialog dialog = new LearnMoreDialog(activity);
        dialog.show();
        dialog.setData(ConfigModel.getInstance(this).getReference_code_guide_link());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstantValue.LOGIN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String action = data.getStringExtra(ConstantValue.ACTION_AFTER_LOGIN);
                if (action != null) {
                    if (action.equals(ConstantValue.ACTION_INTRO_CODE)) {
                        checkUpdate();
                    }
                }
            }
        }
    }

    private void updateUserInfo() {
        UpdateUserInfoDialog dialog = new UpdateUserInfoDialog(activity, UserModel.getInstance().getPhone());
        dialog.show();
        dialog.setOnUpdateInfo(new OnLoginResult() {
            @Override
            public void onLogin(boolean result, Object obj) {
                IntroCodeActivity.start(activity);
                finish();
            }
        });
        UpdatePasswordDialog.getInstance(activity).setCanShow(false);
    }

    private void checkUpdate() {
        if (UserModel.getInstance() != null) {
            if (UserModel.getInstance().isNeed_update()) {
                updateUserInfo();
                return;
            }
        }
        IntroCodeActivity.start(activity);
        finish();
    }
}