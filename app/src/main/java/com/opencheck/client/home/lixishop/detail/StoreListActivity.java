package com.opencheck.client.home.lixishop.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityStoreListBinding;
import com.opencheck.client.models.lixishop.LixiShopProductDetailModel;
import com.opencheck.client.utils.ConstantValue;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class StoreListActivity extends LixiActivity {

    @Override
    public void onClick(View view) {

    }

    private LixiShopProductDetailModel productModel;
    private LinearLayout ll_place_container;
    private ImageView iv_back;
    private ActivityStoreListBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_store_list);
        ll_place_container = (LinearLayout) findViewById(R.id.ll_place_container);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        productModel = new Gson().fromJson(bundle.getString(ConstantValue.STORE_LIST), LixiShopProductDetailModel.class);
        for (int i = 0; i < productModel.getStore_apply().size(); i++) {
            new StoreApplyController(this, productModel.getStore_apply().get(i), i, ll_place_container, productModel.getStore_apply().size());
        }
    }
}
