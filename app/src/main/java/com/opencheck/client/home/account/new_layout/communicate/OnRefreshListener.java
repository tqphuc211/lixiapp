package com.opencheck.client.home.account.new_layout.communicate;

public interface OnRefreshListener {
    void onRefresh();
}
