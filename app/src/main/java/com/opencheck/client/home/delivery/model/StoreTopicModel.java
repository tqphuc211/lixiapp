package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.merchant.TagModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.List;

public class StoreTopicModel {

    /**
     * id : 22426
     * merchant_id : 1331
     * name : TocoToco Bubble Tea - Nguyễn Thái Bình
     * state : active
     * pause_reason_message : Quán quá tải. Vui lòng quay lại sau.
     * address : 245 Nguyễn Thái Bình, Phường 4,Quận Tân Bình,TP. Hồ Chí Minh
     * lat : 10.7977709
     * lng : 106.654373
     * is_opening : true
     * open_time_text : 08:00 đến 21:00
     * trusted_store : true
     * logo_link : https://media.foody.vn/res/g71/706950/prof/s/foody-mobile-24296450_15882724245.jpg
     * cover_link : https://static.lixiapp.com/oc_gallery/2018/11/16/c33a2afafe6048a05883f61bebf82f1c1d73a30a.png
     * service_price_type : fixed
     * min_money_to_go_shipping : 10000
     * ship_price_type : fixed
     * shipping_distance : 6335
     * shipping_duration_min : 40
     * shipping_duration_max : 50
     * list_tag : [{"id":175,"name":"Café/Dessert","image_link":null}]
     * promotion_text : Giảm 60%|Freeship
     * web_mobile_url : https://m.lixiapp.com/evoucher/deliverymerchant/2304
     * min_money_to_free_ship_price : 10000
     * max_free_ship_money : 20000
     * delay_after_open : 30
     * allow_order_in_pause_time : false
     * auto_pause_time : 30
     * max_order_in_pause : 50
     */

    private long id;
    private long merchant_id;
    private String name;
    private String state;
    private String pause_reason_message;
    private String address;
    private double lat;
    private double lng;
    private boolean is_opening;
    private String open_time_text;
    private boolean trusted_store;
    private String logo_link;
    private String cover_link;
    private String service_price_type;
    private long min_money_to_go_shipping;
    private String ship_price_type;
    private long shipping_distance;
    private long shipping_duration_min;
    private long shipping_duration_max;
    private String promotion_text;
    private String web_mobile_url;
    private Long min_money_to_free_ship_price;
    private long max_free_ship_money;
    private long delay_after_open;
    private boolean allow_order_in_pause_time;
    private int auto_pause_time;
    private int max_order_in_pause;
    private List<TagModel> list_tag;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPause_reason_message() {
        return pause_reason_message;
    }

    public void setPause_reason_message(String pause_reason_message) {
        this.pause_reason_message = pause_reason_message;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean isIs_opening() {
        return is_opening;
    }

    public void setIs_opening(boolean is_opening) {
        this.is_opening = is_opening;
    }

    public String getOpen_time_text() {
        return open_time_text;
    }

    public void setOpen_time_text(String open_time_text) {
        this.open_time_text = open_time_text;
    }

    public boolean isTrusted_store() {
        return trusted_store;
    }

    public void setTrusted_store(boolean trusted_store) {
        this.trusted_store = trusted_store;
    }

    public String getLogo_link() {
        if (logo_link == null || logo_link.equals("")) {
            return ConstantValue.DEFAULT_IMAGE[0];
        }
        return logo_link;
    }

    public void setLogo_link(String logo_link) {
        this.logo_link = logo_link;
    }

    public String getCover_link() {
        if (cover_link == null || cover_link.equals("")) {
            return getLogo_link();
        }
        return cover_link;
    }

    public void setCover_link(String cover_link) {
        this.cover_link = cover_link;
    }

    public String getService_price_type() {
        return service_price_type;
    }

    public void setService_price_type(String service_price_type) {
        this.service_price_type = service_price_type;
    }

    public long getMin_money_to_go_shipping() {
        return min_money_to_go_shipping;
    }

    public void setMin_money_to_go_shipping(long min_money_to_go_shipping) {
        this.min_money_to_go_shipping = min_money_to_go_shipping;
    }

    public String getShip_price_type() {
        return ship_price_type;
    }

    public void setShip_price_type(String ship_price_type) {
        this.ship_price_type = ship_price_type;
    }

    public long getShipping_distance() {
        return shipping_distance;
    }

    public void setShipping_distance(long shipping_distance) {
        this.shipping_distance = shipping_distance;
    }

    public long getShipping_duration_min() {
        return shipping_duration_min;
    }

    public void setShipping_duration_min(long shipping_duration_min) {
        this.shipping_duration_min = shipping_duration_min;
    }

    public long getShipping_duration_max() {
        return shipping_duration_max;
    }

    public void setShipping_duration_max(long shipping_duration_max) {
        this.shipping_duration_max = shipping_duration_max;
    }

    public String getPromotion_text() {
        return promotion_text;
    }

    public void setPromotion_text(String promotion_text) {
        this.promotion_text = promotion_text;
    }

    public String getWeb_mobile_url() {
        return web_mobile_url;
    }

    public void setWeb_mobile_url(String web_mobile_url) {
        this.web_mobile_url = web_mobile_url;
    }

    public Long getMin_money_to_free_ship_price() {
        return min_money_to_free_ship_price;
    }

    public void setMin_money_to_free_ship_price(Long min_money_to_free_ship_price) {
        this.min_money_to_free_ship_price = min_money_to_free_ship_price;
    }

    public long getMax_free_ship_money() {
        return max_free_ship_money;
    }

    public void setMax_free_ship_money(long max_free_ship_money) {
        this.max_free_ship_money = max_free_ship_money;
    }

    public long getDelay_after_open() {
        return delay_after_open;
    }

    public void setDelay_after_open(long delay_after_open) {
        this.delay_after_open = delay_after_open;
    }

    public boolean isAllow_order_in_pause_time() {
        return allow_order_in_pause_time;
    }

    public void setAllow_order_in_pause_time(boolean allow_order_in_pause_time) {
        this.allow_order_in_pause_time = allow_order_in_pause_time;
    }

    public int getAuto_pause_time() {
        return auto_pause_time;
    }

    public void setAuto_pause_time(int auto_pause_time) {
        this.auto_pause_time = auto_pause_time;
    }

    public int getMax_order_in_pause() {
        return max_order_in_pause;
    }

    public void setMax_order_in_pause(int max_order_in_pause) {
        this.max_order_in_pause = max_order_in_pause;
    }

    public List<TagModel> getList_tag() {
        return list_tag;
    }

    public void setList_tag(List<TagModel> list_tag) {
        this.list_tag = list_tag;
    }
}
