package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.LanguageBinding;

import java.util.List;

public class StoreOfMerchantAdapter extends RecyclerView.Adapter<StoreOfMerchantAdapter.ViewHolder> {

    private LixiActivity activity;
    private List<StoreOfCategoryModel> storeList;

    public StoreOfMerchantAdapter(LixiActivity activity, List<StoreOfCategoryModel> storeList) {
        this.activity = activity;
        this.storeList = storeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_store_of_merchant, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StoreOfCategoryModel store = storeList.get(position);
        holder.tv_name.setText(store.getName());
        holder.tv_time.setText(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
        if (store.getShipping_distance() >= 100) {
            double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
            holder.tv_distance.setText(String.valueOf(distance) + " km");
        } else
            holder.tv_distance.setText(String.valueOf(store.getShipping_distance()) + " m");

        if (store.getPromotion_text() != null && store.getPromotion_text().length() > 0) {
            holder.ll_promotion.setVisibility(View.VISIBLE);
            holder.tv_promotion.setText(store.getPromotion_text());
            holder.tv_name.setMaxLines(1);
        } else {
            holder.tv_name.setMaxLines(2);
            holder.ll_promotion.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return storeList == null ? 0 : storeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_distance, tv_time, tv_promotion;
        private ImageView iv_checked;
        private LinearLayout ll_promotion;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_promotion = itemView.findViewById(R.id.tv_promotion);
            iv_checked = itemView.findViewById(R.id.iv_checked);
            ll_promotion = itemView.findViewById(R.id.ll_promotion);
        }
    }
}
