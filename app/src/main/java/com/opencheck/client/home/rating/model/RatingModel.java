package com.opencheck.client.home.rating.model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.user.UserModel;

public class RatingModel extends LixiModel {

    /**
     * comment : string
     * create_date : 0
     * id : 0
     * product_id : 0
     * rating : 0
     * rating_option_id : 0
     * rating_option_value : string
     * status : string
     * user_id : 0
     * user_info : {"avatar": "string","name": "string"}
     */

    private String comment;
    private long create_date;
    private long id;
    private long product_id;
    private long rating;
    private long rating_option_id;
    private String rating_option_value;
    private String status;
    private long user_id;
    private UserModel user_info;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(long create_date) {
        this.create_date = create_date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public long getRating_option_id() {
        return rating_option_id;
    }

    public void setRating_option_id(long rating_option_id) {
        this.rating_option_id = rating_option_id;
    }

    public String getRating_option_value() {
        return rating_option_value;
    }

    public void setRating_option_value(String rating_option_value) {
        this.rating_option_value = rating_option_value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public UserModel getUser_info() {
        return user_info;
    }

    public void setUser_info(UserModel user_info) {
        this.user_info = user_info;
    }
}
