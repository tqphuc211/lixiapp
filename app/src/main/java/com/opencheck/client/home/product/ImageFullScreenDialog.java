package com.opencheck.client.home.product;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.LixiShopImageFullscreenDialogBinding;
import com.opencheck.client.dialogs.LixiTrackingDialog;

import java.util.List;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class ImageFullScreenDialog extends LixiTrackingDialog implements  View.OnTouchListener {
    public ImageFullScreenDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private boolean isChecked = false;

    private RelativeLayout rl_dismiss;
    private RelativeLayout rl_close;
    private LinearLayout ll_root_popup_menu;
    private ImageView iv;
    private TextView tv_title;
    private ViewPager viewPager;
    private int position;
    private List<String> listImage;
    private String image_url = "";
    private String title = "";
    private ImageGaleryAdapter adapter;
    private CirclePageIndicator indicator;
    private ViewGroup.LayoutParams layoutParams;
    //region setup dialog

    public void setData(String image_url, String title) {
        this.image_url = image_url;
        this.title = title;


        findViews();
        initViews();
    }

    public void setData(List<String> list_image_url, int position, String title) {

        this.listImage = list_image_url;
        this.title = title;
        this.position = position;


        findViews();
        initViews();
    }
    private LixiShopImageFullscreenDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopImageFullscreenDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

    }

    private void initViews() {
        if (!image_url.equals(""))
            ImageLoader.getInstance().displayImage(image_url, iv, LixiApplication.getInstance().optionsNomal);
        if (title != null)
            tv_title.setText(title);
    }

    public void setData(String image_url) {
        setViewData();
    }

    private void setViewData() {

    }



    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2017-10-06 15:23:44 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById( R.id.rl_dismiss );
        rl_close = (RelativeLayout)findViewById( R.id.rl_close );
        iv = (ImageView) findViewById(R.id.iv);
        tv_title = (TextView) findViewById(R.id.tv_title);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        adapter = new ImageGaleryAdapter(activity, listImage);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        viewPager.setCurrentItem(position);
        layoutParams = viewPager.getLayoutParams();
        int newHeight = activity.widthScreen*9/16;
        layoutParams.height = newHeight;
        viewPager.setLayoutParams(layoutParams);
        rl_close.setOnClickListener(this);
        ll_root_popup_menu.setOnTouchListener(this);
        ll_root_popup_menu.setOnClickListener(this);
    }

    //endregion


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_close:
                dismiss();
                break;
            case R.id.ll_root_popup_menu:
                dismiss();
                break;
            default:
                break;
        }
    }

    //region dialog callback

    @Override
    public void dismiss() {
        super.dismiss();
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
            dismiss();
        }
        return false;
    }

    public interface IDidalogEvent {
        void onOk(boolean success);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    //endregion
}
