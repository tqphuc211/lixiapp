package com.opencheck.client.home.survey;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyQuestChoiceBinding;
import com.opencheck.client.models.merchant.survey.SurveyQuestionModel;
import com.opencheck.client.models.merchant.survey.SurveyUserAnswerModel;

public class QuestionChoiceController extends QuestionAnswerController{

    private TextView tv_question;
    private LinearLayout ll_answer;
    private QuestionChoiceControllerItem crChoice;

    public QuestionChoiceController(LixiActivity activity, ViewGroup parent, SurveyQuestionModel quest) {
        this.activity = activity;
        this.parent = parent;
        this.quest = quest;

        SurveyQuestChoiceBinding surveyQuestChoiceBinding =
                SurveyQuestChoiceBinding.inflate(LayoutInflater.from(activity),
                        parent, false);
        rootView = surveyQuestChoiceBinding.getRoot();
        parent.addView(rootView);

        findView();
        setData();
    }

    public void findView() {
        tv_question = (TextView) rootView.findViewById(R.id.tv_question);
        ll_answer = (LinearLayout) rootView.findViewById(R.id.ll_answer);
    }

    public void setData() {
        tv_question.setText(quest.getQuestion_name());
        for (int i = 0; i < quest.getAnswers().size(); i++) {
            new QuestionChoiceControllerItem(activity, ll_answer, quest, i, this);
        }
    }

    public void selectAnswer(QuestionChoiceControllerItem item) {
        if (crChoice != null)
            crChoice.setSelected(false);
        crChoice = item;
        crChoice.setSelected(true);
    }

    @Override
    SurveyUserAnswerModel getAnswer() {
        if (crChoice == null)
            return null;
        SurveyUserAnswerModel rs = new SurveyUserAnswerModel();
        rs.setQuestion_id(quest.getId());
        rs.setAnswer_id(quest.getAnswers().get(crChoice.getPos()).getId());
        return rs;
    }
}
