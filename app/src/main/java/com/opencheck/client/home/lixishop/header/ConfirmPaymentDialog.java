package com.opencheck.client.home.lixishop.header;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ConfirmBuyCardDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.lixishop.PaymentModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class ConfirmPaymentDialog extends LixiDialog {
    public ConfirmPaymentDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    public static String PAYMENT_CONFIRM_SELECTED = "PAYMENT_CONFIRM_SELECTED";
    private ServiceCategoryModel serviceCategoryModel;
    private PaymentModel paymentModel = new PaymentModel();

    private boolean isTopup = false;
    private boolean isPrepay = false;
    private String type = "";

    //region Layout Variable
    private LinearLayout lnRoot;
    private RelativeLayout rlClose;
    private RelativeLayout rl_number;
    private RelativeLayout rl_name;
    private TextView tvSuplier, tvTitle, tvCustomerNumber, tvFee;
    private TextView tvSuplierValue;
    private TextView tvCustomerCode;
    private TextView tvCustomerCodeValue;
    private TextView tvCustomerNameValue;
    private TextView tvCustomerNumberValue;
    private TextView tvCustomerPrice;
    private TextView tvCustomerPriceValue;
    private TextView tvCustomerAvailable;
    private TextView tvCustomerAvailableValue;
    private TextView tvCustomerPay;
    private TextView tvCustomerPayValue;
    private TextView tvCustomerRemnant;
    private TextView tvCustomerRemnantValue;
    private Button btnConfirm;
    //endregion
    private ConfirmBuyCardDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ConfirmBuyCardDialogBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    private void findViews() {
        tvFee = findViewById(R.id.tvFee);
        tvCustomerNumber = findViewById(R.id.tvCustomerNumber);
        tvTitle = findViewById(R.id.tvTitle);
        lnRoot = findViewById(R.id.lnRoot);
        rlClose = findViewById(R.id.rlClose);
        rl_number = findViewById(R.id.rl_number);
        rl_name = findViewById(R.id.rl_name);
        tvSuplier = findViewById(R.id.tvSuplier);
        tvSuplierValue = findViewById(R.id.tvSuplierValue);
        tvCustomerCode = findViewById(R.id.tvCustomerCode);
        tvCustomerCodeValue = findViewById(R.id.tvCustomerCodeValue);
        tvCustomerPrice = findViewById(R.id.tvCustomerPrice);
        tvCustomerNameValue = findViewById(R.id.tvCustomerNameValue);
        tvCustomerNumberValue = findViewById(R.id.tvCustomerNumberValue);
        tvCustomerPriceValue = findViewById(R.id.tvCustomerPriceValue);
        tvCustomerAvailable = findViewById(R.id.tvCustomerAvailable);
        tvCustomerAvailableValue = findViewById(R.id.tvCustomerAvailableValue);
        tvCustomerPay = findViewById(R.id.tvCustomerPay);
        tvCustomerPayValue = findViewById(R.id.tvCustomerPayValue);
        tvCustomerRemnant = findViewById(R.id.tvCustomerRemnant);
        tvCustomerRemnantValue = findViewById(R.id.tvCustomerRemnantValue);
        btnConfirm = findViewById(R.id.btnConfirm);
    }

    public void setData(ServiceCategoryModel _serviceCategoryModel, PaymentModel _paymentModel, boolean _isTopup, boolean _isPrePay) {
        serviceCategoryModel = _serviceCategoryModel;
        paymentModel = _paymentModel;
        isPrepay = _isPrePay;
        isTopup = _isTopup;
        type = isTopup ? TrackingConstant.Service.TOPUP : TrackingConstant.Service.PHONE_CARD;
        initData();
        GlobalTracking.trackServiceBegin(
                type,
                Double.parseDouble(paymentModel.getMoney_payment()),
                TrackingConstant.Currency.VND);
    }

    private void initData() {
        rlClose.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        if (!isTopup) {
            rl_name.setVisibility(View.GONE);
            rl_number.setVisibility(View.GONE);
        } else {
            rl_name.setVisibility(View.VISIBLE);
            rl_number.setVisibility(View.VISIBLE);
        }

        tvTitle.setText(LanguageBinding.getString(R.string.popup_payment_confirm_bill_title, activity));
        tvCustomerNumber.setText(LanguageBinding.getString(R.string.info_update_phone, activity));
        tvSuplier.setText(LanguageBinding.getString(R.string.buy_card_provider, activity));
        tvCustomerPrice.setText(LanguageBinding.getString(R.string.buy_card_value, activity));
        tvCustomerAvailable.setText(LanguageBinding.getString(R.string.popup_payment_confirm_bill_available, activity));
        tvCustomerPay.setText(LanguageBinding.getString(R.string.popup_payment_confirm_bill_pay, activity));
        tvCustomerRemnant.setText(LanguageBinding.getString(R.string.popup_payment_confirm_bill_remnant, activity));

        if (serviceCategoryModel.getType().equalsIgnoreCase(ServiceCategoryModel.TYPE_CUSTOMER_CODE) || serviceCategoryModel.getType().equalsIgnoreCase(ServiceCategoryModel.TYPE_PHONE)) {
            //tvSuplier.setText("Nhà cung cấp");
            tvSuplierValue.setText(paymentModel.getPaymentPartnerModel().getName());
        } else if (serviceCategoryModel.getType().equalsIgnoreCase(ServiceCategoryModel.TYPE_CONTRACT)) {
            //tvSuplier.setText("Công ty tài chính");
            tvSuplierValue.setText(paymentModel.getPaymentPartnerModel().getName());
        } else if (serviceCategoryModel.getType().equalsIgnoreCase(ServiceCategoryModel.TYPE_BUY_CARD) || serviceCategoryModel.getType().equalsIgnoreCase(ServiceCategoryModel.TYPE_TOPUP)) {
            //tvSuplier.setText("Nhà mạng");
            String str = "";
            if (paymentModel.getTypeTopup().length() > 0)
                str += " - " + paymentModel.getTypeTopup();
            tvSuplierValue.setText(paymentModel.getPaymentPartnerModel().getName() + str);
        }

        //tvCustomerCode.setText("Mã khách hàng");
        tvCustomerCodeValue.setText(paymentModel.getContract_code());

        tvCustomerNameValue.setText(paymentModel.getFullname());
        tvCustomerNumberValue.setText(paymentModel.getPhone());

        //tvCustomerPrice.setText("Cước");
        tvCustomerPriceValue.setText(Helper.getVNCurrency(Long.parseLong(paymentModel.getMoney_payment())) + "đ");

        //tvCustomerAvailable.setText("Số dư khả dụng");
        tvCustomerAvailableValue.setText(Helper.getVNCurrency((long) Float.parseFloat(UserModel.getInstance().getAvailable_point())) + "đ");

        //tvCustomerPay.setText("Thanh toán");
        tvCustomerPayValue.setText(Helper.getVNCurrency(Long.parseLong(paymentModel.getMoney_payment())) + "đ");

        float remnant = Float.parseFloat(UserModel.getInstance().getAvailable_point()) - Float.parseFloat(paymentModel.getMoney_payment());

        //tvCustomerRemnant.setText("Số dư còn lại");
        tvCustomerRemnantValue.setText(Helper.getVNCurrency((long) remnant) + "đ");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm:
                GlobalTracking.trackServiceCheckedOut(type,
                        Double.parseDouble(paymentModel.getMoney_payment()),
                        TrackingConstant.Currency.VND,
                        Double.parseDouble(paymentModel.getMoney_payment()));
                setCancelable(false);
                rlClose.setEnabled(false);
                createPayment(paymentModel);
                break;
            case R.id.rlClose:
                dismiss();
                break;
        }
    }

    ConfirmPaymentDialog dialog = this;


    private void createPayment(final PaymentModel paymentModel) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("name", paymentModel.getName());
            jsonObject.put("service_id", paymentModel.getServiceCategoryModel().getId());
            jsonObject.put("suplier_id", paymentModel.getPaymentPartnerModel().getId());
            jsonObject.put("contract_code", paymentModel.getContract_code());
            jsonObject.put("fullname", paymentModel.getFullname());
            jsonObject.put("phone", paymentModel.getPhone());
            jsonObject.put("quantity", paymentModel.getQuantity());
            if (isTopup)
                jsonObject.put("type_topup", (isPrepay) ? "prepay" : "postpay"); //"type_topup": ("postpay", "prepay") }
            jsonObject.put("money_payment", paymentModel.getMoney_payment());
            jsonObject.put("platform", "android");


            DataLoader.createPaymentService(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        GlobalTracking.trackServicePurchased(type,
                                Double.parseDouble(paymentModel.getMoney_payment()),
                                TrackingConstant.Currency.VND,
                                Double.parseDouble(paymentModel.getMoney_payment()));

                        dialog.setCancelable(true);
                        rlClose.setEnabled(true);
                        dismiss();
                        CardPaymentDoneDialog doneDialog = new CardPaymentDoneDialog(activity, true, true, false);
                        doneDialog.show();
                        doneDialog.setData(paymentModel);
                    } else {
                        setCancelable(true);
                        Helper.showErrorDialog(activity, (String) object);
                    }
                }
            }, jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.SERVICE_CHECKOUT;
    }
}
