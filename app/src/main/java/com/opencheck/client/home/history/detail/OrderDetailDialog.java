package com.opencheck.client.home.history.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.OrderDetailDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.history.detail.controller.CardController;
import com.opencheck.client.home.history.detail.controller.OrderDetailInfoController;
import com.opencheck.client.models.lixishop.DetailHistoryCardModel;
import com.opencheck.client.models.lixishop.OrderDetailModel;
import com.opencheck.client.models.merchant.CardListModel;
import com.opencheck.client.models.merchant.OrderInfoModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderDetailDialog extends LixiDialog {
    public OrderDetailDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, false, _dimBackgroud, _titleBar);
    }


    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu, ll_root_card, ll_card_container;
    private RelativeLayout rlTop;
    private RelativeLayout rlBack;
    private TextView tvTitle;
    private RelativeLayout rlLine;
    private ImageView iv_icon;
    private TextView tv_name;
    private TextView tv_point;
    private TextView tv_time;
    private TextView tv_state;
    private LinearLayout ll_detail;
    private TextView tv_log;
    private RecyclerView rv_log;
    private SwipeRefreshLayout swiperefresh;
    ArrayList<DetailHistoryCardModel> list = new ArrayList<>();
    DetailSerialAdapter detailSerialAdapter;
    RecyclerView rvDetailSerial;

    private String orderId;
    private String orderType;
    private OrderDetailModel orderDTO;

    private OrderLogAdapter adapter;

    private OrderDetailDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = OrderDetailDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());

        findViews();
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);

        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        rlTop = (RelativeLayout) findViewById(R.id.rlTop);
        rlBack = (RelativeLayout) findViewById(R.id.rlBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rlLine = (RelativeLayout) findViewById(R.id.rlLine);
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_point = (TextView) findViewById(R.id.tv_point);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_state = (TextView) findViewById(R.id.tv_state);
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        tv_log = (TextView) findViewById(R.id.tv_log);
        rv_log = (RecyclerView) findViewById(R.id.rv_log);
        ll_root_card = (LinearLayout) findViewById(R.id.ll_card_root);
        ll_card_container = (LinearLayout) findViewById(R.id.ll_card_container);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        rvDetailSerial = (RecyclerView) findViewById(R.id.rvDetailSerial);

        rvDetailSerial.setLayoutManager(new LinearLayoutManager(getContext()));
        detailSerialAdapter = new DetailSerialAdapter(activity, list);
        rvDetailSerial.setAdapter(detailSerialAdapter);

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ll_detail.removeAllViews();
                ll_root_card.removeAllViews();
                getListResult();
            }
        });
        rlBack.setOnClickListener(this);
        mBinding.llSupport.setOnClickListener(this);

        tvTitle.setText(LanguageBinding.getString(R.string.trans_detail_title, activity));
        mBinding.tvSdt.setText(LanguageBinding.getString(R.string.trans_detail_call, activity));

        adapter = new OrderLogAdapter(null, activity);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_log.setLayoutManager(mLayoutManager);
        rv_log.setAdapter(adapter);
        rv_log.setNestedScrollingEnabled(false);
    }

    public void setData(String id, String type) {
        orderId = id;
        orderType = type;
        getListCard(id);
        getListResult();
    }

    public void setViewDataIn() {
        String str = orderDTO.getImage();
        ImageLoader.getInstance().displayImage(orderDTO.getImage(), iv_icon, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(orderDTO.getName());
        tv_point.setText("+" + Helper.getVNCurrency(orderDTO.getPoint()) + "đ");
        tv_time.setText(orderDTO.getDate());
        tv_state.setText(orderDTO.getStateVN());
        tv_state.setTextColor(orderDTO.getStateColor());

        for (int i = 0; i < orderDTO.getInfo().size(); i++) {
            OrderInfoModel dto = orderDTO.getInfo().get(i);
            new OrderDetailInfoController(activity, 0, dto.getName(), dto.getValue(), Long.parseLong("FF" + dto.getColor(), 16), dto.isBold(), ll_detail);
        }
        new OrderDetailInfoController(activity, 1, "", "", 0xff000000, false, ll_detail);
        new OrderDetailInfoController(activity, 0, orderDTO.getState(), LanguageBinding.getString(R.string.lixi_cash_back, activity), "+" + Helper.getVNCurrency(orderDTO.getPoint()) + "đ", orderDTO.getStateColor(), true, ll_detail);

        adapter.setArrData(orderDTO.getLog());
        adapter.notifyDataSetChanged();
    }

    public void setViewDataOut() {

        ImageLoader.getInstance().displayImage(orderDTO.getImage(), iv_icon, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(orderDTO.getName());
        tv_point.setText(Helper.getVNCurrency(orderDTO.getPoint()) + "đ");
        tv_time.setText(orderDTO.getDate());
        tv_state.setText(orderDTO.getStateVN());
        tv_state.setTextColor(orderDTO.getStateColor());

        tv_log.setVisibility(View.GONE);


        for (int i = 0; i < orderDTO.getInfo().size(); i++) {
            OrderInfoModel dto = orderDTO.getInfo().get(i);
            new OrderDetailInfoController(activity, 0, dto.getName(), dto.getValue(), Long.parseLong("FF" + dto.getColor(), 16), dto.isBold(), ll_detail);
        }

        if (orderDTO.getCardList() == null || orderDTO.getCardList().size() == 0) {
            ll_card_container.setVisibility(View.GONE);
            rvDetailSerial.setVisibility(View.GONE);
            mBinding.rlCall.setVisibility(View.VISIBLE);
        } else {
            mBinding.rlCall.setVisibility(View.GONE);
            ll_card_container.setVisibility(View.VISIBLE);
            rvDetailSerial.setVisibility(View.VISIBLE);
            for (int i = 0; i < orderDTO.getCardList().size(); i++) {
                CardListModel cardListModel = orderDTO.getCardList().get(i);
                new CardController(activity, cardListModel.getLabel(), cardListModel.getValue(), i, ll_root_card);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                dismiss();
                break;
            case R.id.ll_support:
                Helper.callPhoneSupport(activity);
                break;
            default:
                break;
        }
    }

    private void getListCard(String id) {
        DataLoader.getListCard(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArrayList<DetailHistoryCardModel> result = (ArrayList<DetailHistoryCardModel>) object;
                    // DetailHistoryCardModel detailHistoryCardModel = (DetailHistoryCardModel) objects;
                    list.clear();
                    list.addAll(result);
                    detailSerialAdapter.reLoadData(list);
                }
            }
        }, id);

    }

    private void getListResult() {
        if (orderType.equals("in") || orderId.indexOf("pointin") >= 0) {
            DataLoader.getOrderDetail(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    swiperefresh.setRefreshing(false);
                    if (isSuccess) {
                        orderDTO = (OrderDetailModel) object;
                        setViewDataIn();
                    } else {
                        //lnNoResult.setVisibility(View.VISIBLE);
                        //tvNoResult.setText("Bạn chưa có hoá đơn thanh toán nào");
                        tv_name.setText("Không tìm thấy giao dịch");
                        dismiss();
                        Toast.makeText(activity, "Không tìm thấy giao dịch", Toast.LENGTH_LONG).show();
                    }
                }
            }, orderId);
        } else {
            DataLoader.getOrderDetail(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    swiperefresh.setRefreshing(false);
                    if (isSuccess) {
                        orderDTO = (OrderDetailModel) object;
                        setViewDataOut();
                    } else {
                        //lnNoResult.setVisibility(View.VISIBLE);
                        //tvNoResult.setText("Bạn chưa có hoá đơn thanh toán nào");
                        tv_name.setText("Không tìm thấy giao dịch");
                        dismiss();
                        Toast.makeText(activity, "Không tìm thấy giao dịch", Toast.LENGTH_LONG).show();
                    }
                }
            }, orderId);
        }

    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.HISTORY_DETAIL;
    }
}
