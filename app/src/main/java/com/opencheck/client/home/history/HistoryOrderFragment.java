package com.opencheck.client.home.history;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.custom.PinnedSectionListView;
import com.opencheck.client.databinding.HistoryOrderFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.history.detail.HistoryReceiveLixiDialog;
import com.opencheck.client.home.history.detail.OrderDetailDialog;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.models.user.OrderHistoryModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class HistoryOrderFragment extends LixiFragment implements AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {
    private ViewGroup viewGroupHeader;

    private SwipeRefreshLayout swipeLayout;
    private PinnedSectionListView lvItem;
    private LinearLayout lnNoResult;
    private TextView tvNoResult;

    private HistoryOrderAdapter historyPurchantAdapter;

    private int record_per_page = DataLoader.record_per_page;
    private int indexPage = 1;
    private Boolean isLoading = false;

    private String typeHistory = ALL;

    public static final String IN = "in";
    public static final String OUT = "out";
    public static final String ALL = "all";

    private HistoryOrderFragmentBinding mBinding;
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mBinding = HistoryOrderFragmentBinding.inflate(inflater, container, false);
        this.viewGroupHeader = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        this.initView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initView(View view) {
        swipeLayout = view.findViewById(R.id.swipeLayout);
        lvItem = view.findViewById(R.id.lvItem);
        lnNoResult = view.findViewById(R.id.lnNoResult);
        tvNoResult = view.findViewById(R.id.tvNoResult);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initData();
            }
        }, 300);
    }

    private void initData() {

        lvItem.setOnScrollListener(this);
        swipeLayout.setOnRefreshListener(this);

        this.setAdapter();
        getListResult();
    }

    private void setAdapter() {
        this.historyPurchantAdapter = new HistoryOrderAdapter(activity, R.layout.history_order_item, itemClickListener);
        this.lvItem.setAdapter(historyPurchantAdapter);
    }

    private void addListItem(ArrayList<OrderHistoryModel> listItem) {
        historyPurchantAdapter.addListItems(listItem);
    }

    private void getListResult() {
        swipeLayout.setRefreshing(true);
        if (!isLoading) {
            isLoading = true;
            DataLoader.getHistory(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ArrayList<OrderHistoryModel> listItem = (ArrayList<OrderHistoryModel>) object;
                        Helper.showLog("History size: " + listItem.size());
                        addListItem(listItem);
                        isLoading = false;
                        indexPage++;
                    } else {
                        isLoading = true;
                    }
                    swipeLayout.setRefreshing(false);
                    if (historyPurchantAdapter.getListItems().size() <= 0) {
                        lnNoResult.setVisibility(View.VISIBLE);
                        tvNoResult.setText(getResources().getString(R.string.trans_no_result));
                    } else {
                        lnNoResult.setVisibility(View.GONE);
                    }
                }
            }, indexPage, typeHistory, null, null, null, null);
        }

    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemCLick(int pos, String action, Object object, Long count) {
            if (action.equalsIgnoreCase(HistoryPromotionStickyAdapter.ITEM_CLICK_ACTION)) {
                OrderHistoryModel dto = (OrderHistoryModel) object;
                if (dto.getId().indexOf("shopout") >= 0) {
                    OrderShopDetailDialog dialog = new OrderShopDetailDialog(activity, false, true, false);
                    dialog.show();
                    dialog.setData(dto.getId());
                } else {
                    if (dto.getType().equals("in")) {
                        HistoryReceiveLixiDialog dialog = new HistoryReceiveLixiDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData(dto.getId());
                    } else {
                        OrderDetailDialog dialog = new OrderDetailDialog(activity, false, true, false);
                        dialog.show();
                        dialog.setData(dto.getId(), dto.getType());
                    }
                }
            }
        }
    };

    private String actionSelect = "";


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onRefresh() {
        indexPage = 1;
        isLoading = false;
        setAdapter();
        getListResult();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!isLoading && totalItemCount > 0) {
            if (firstVisibleItem >= totalItemCount - visibleItemCount - 4) {
                getListResult();
            }
            if (firstVisibleItem >= totalItemCount - visibleItemCount) {
                //SHOW LOAD MORE
            }
        }
    }


    public String getTypeHistory() {
        return typeHistory;
    }

    public void setTypeHistory(String typeHistory) {
        this.typeHistory = typeHistory;
    }

    @Override
    public void onClick(View view) {

    }
}
