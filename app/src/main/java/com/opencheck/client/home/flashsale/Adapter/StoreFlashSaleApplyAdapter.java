package com.opencheck.client.home.flashsale.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowStoreFlashSaleApplyBinding;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class StoreFlashSaleApplyAdapter extends RecyclerView.Adapter<StoreFlashSaleApplyAdapter.StoreHolder> {
    private LixiActivity activity;
    private ArrayList<StoreApplyModel> listStore;

    public StoreFlashSaleApplyAdapter(LixiActivity activity, ArrayList<StoreApplyModel> listStore) {
        this.activity = activity;
        this.listStore = listStore;
    }

    @Override
    public StoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowStoreFlashSaleApplyBinding rowStoreFlashSaleApplyBinding =
                RowStoreFlashSaleApplyBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new StoreHolder(rowStoreFlashSaleApplyBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(StoreHolder holder, int position) {
        StoreApplyModel storeApply = listStore.get(position);

        holder.txtNumber.setText(position + 1 + ".");
        holder.txtName.setText(storeApply.getName());
        holder.txtPhone.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_phone, activity), storeApply.getPhone()));
        holder.txtAddress.setText(storeApply.getAddress());

        if (position == 0) {
            holder.v_line.setVisibility(View.GONE);
        }else {
            holder.v_line.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (listStore.size() <= 3){
            return listStore.size();
        }
        return 3;
    }

    public class StoreHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtNumber;
        private TextView txtName;
        private TextView txtAddress;
        private TextView txtPhone;
        private ImageView imgMap;
        private ImageView imgCall;
        private View v_line;

        public StoreHolder(View itemView) {
            super(itemView);
            txtNumber  = itemView.findViewById(R.id.tv_number);
            txtName    = itemView.findViewById(R.id.tv_name);
            txtAddress = itemView.findViewById(R.id.tv_address);
            txtPhone   = itemView.findViewById(R.id.ed_phone);

            imgMap     = itemView.findViewById(R.id.iv_map);
            imgCall    = itemView.findViewById(R.id.iv_call);
            v_line     = itemView.findViewById(R.id.v_line);

            imgCall.setVisibility(View.GONE);
            imgMap .setVisibility(View.GONE);

            imgCall.setOnClickListener(this);
            imgMap .setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_call:
//                    Helper.callPhone(activity, storeModel.getPhone());
                    break;
                case R.id.iv_map:
//                    if (merchantModel != null) {
//                        merchantModel.getListStore().remove(storeModel);
//                        merchantModel.getListStore().add(0, storeModel);
//                        Intent intent = new Intent(activity, StoreMapActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
//                        intent.putExtras(bundle);
//                        activity.startActivity(intent);
//                    }
                    break;
            }

//            if (view == itemView){
//                if (merchantModel != null) {
//                    merchantModel.getListStore().remove(storeModel);
//                    merchantModel.getListStore().add(0, storeModel);
//                    Intent intent = new Intent(activity, StoreMapActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ConstantValue.MERCHANT_MODEL, new Gson().toJson(merchantModel));
//                    intent.putExtras(bundle);
//                    activity.startActivity(intent);
//                }
//            }
        }
    }
}
