package com.opencheck.client.home.account.new_layout.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowListEvoucherBinding;
import com.opencheck.client.home.account.new_layout.model.EvoucherInfo;
import com.opencheck.client.models.lixishop.ComboItemModel;

import java.util.ArrayList;

public class EvoucherListAdapter extends RecyclerView.Adapter<EvoucherListAdapter.EvoucherHolder> {
    // Constructor
    private LixiActivity activity;
    private ArrayList<ComboItemModel> listCombo;
    private ArrayList<ArrayList<EvoucherInfo>> listEvoucherCombo;

    public EvoucherListAdapter(LixiActivity activity, ArrayList<ComboItemModel> listCombo, ArrayList<ArrayList<EvoucherInfo>> listEvoucherCombo) {
        this.activity = activity;
        this.listCombo = listCombo;
        this.listEvoucherCombo = listEvoucherCombo;
    }

    @Override
    public EvoucherListAdapter.EvoucherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowListEvoucherBinding rowListEvoucherBinding =
                RowListEvoucherBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new EvoucherHolder(rowListEvoucherBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(EvoucherListAdapter.EvoucherHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listCombo != null ? listCombo.size() : 0;
    }

    public class EvoucherHolder extends RecyclerView.ViewHolder {
        TextView txtEvoucherName, txtEvoucherQuant;
        RecyclerView recEvoucherInfo;
        View viewBorder;

        public EvoucherHolder(View itemView) {
            super(itemView);
            txtEvoucherName = (TextView) itemView.findViewById(R.id.txtEvoucherName);
            txtEvoucherQuant = (TextView) itemView.findViewById(R.id.txtEvoucherQuant);
            recEvoucherInfo = (RecyclerView) itemView.findViewById(R.id.recEvoucherInfo);
            viewBorder = (View) itemView.findViewById(R.id.viewBorder);
        }

        public void bind() {
            int position = getAdapterPosition();

            if (position == 0) {
                viewBorder.setVisibility(View.GONE);
            } else {
                viewBorder.setVisibility(View.VISIBLE);
            }

            ComboItemModel combo = listCombo.get(position);
            ArrayList<EvoucherInfo> listEvoucher = listEvoucherCombo.get(position);

            if (combo != null && listEvoucher != null) {
                txtEvoucherName.setText(combo.getName());
                txtEvoucherQuant.setText("(" + combo.getQuantity() + " voucher)");

                EvoucherItemAdapter adapter = new EvoucherItemAdapter(activity, listEvoucher);
                LinearLayoutManager manager = new LinearLayoutManager(activity);
                manager.setOrientation(LinearLayoutManager.VERTICAL);

                recEvoucherInfo.setAdapter(adapter);
                recEvoucherInfo.setLayoutManager(manager);
            }
        }
    }
}
