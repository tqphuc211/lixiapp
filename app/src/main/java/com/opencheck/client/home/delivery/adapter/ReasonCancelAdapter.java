package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.databinding.RowReasonCancelDeliBinding;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;

import java.util.List;

public class ReasonCancelAdapter extends RecyclerView.Adapter<ReasonCancelAdapter.ReasonHolder> {

    private LixiActivity activity;
    private List<String> listReason;
    public String reasonSelect = "";

    private int checkedItem = -1;

    public ReasonCancelAdapter(LixiActivity activity, List<String> listReason) {
        this.activity = activity;
        this.listReason = listReason;
    }

    @Override
    public ReasonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowReasonCancelDeliBinding mBinding = RowReasonCancelDeliBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ReasonHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(ReasonHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return listReason == null ? 0 : listReason.size();
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public int getReasonIndex(){
        return checkedItem;
    }

    public class ReasonHolder extends RecyclerView.ViewHolder{
        RowReasonCancelDeliBinding mBinding;
        public ReasonHolder(final RowReasonCancelDeliBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkedItem != getAdapterPosition()){
                        // check
                        reasonSelect = listReason.get(getAdapterPosition());
                        checkedItem = getAdapterPosition();
                        notifyDataSetChanged();

                        if (onItemClickListener != null){
                            onItemClickListener.onItemClickListener(null, checkedItem, null, reasonSelect, 0L);
                        }
                    }
                }
            });
        }

        void bind(){
            String reason = listReason.get(getAdapterPosition()) + "";
            mBinding.txtReason.setText(reason);
            if (checkedItem == getAdapterPosition()){
                mBinding.imgCheck.setVisibility(View.VISIBLE);
            }else {
                mBinding.imgCheck.setVisibility(View.GONE);
            }
        }
    }
}
