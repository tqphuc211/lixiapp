package com.opencheck.client.home.LixiHomeV25.CategoryContent;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.CategoryDetailActivityBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.LixiHomeV25.MerchantList.ActivityListMerchant;
import com.opencheck.client.models.ProductCategoryModel;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCategoryDetail extends LixiActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String CATEGORY_NAME = "CATEGORY_NAME";
    public static final String IS_CHILD_CATEGORY = "IS_CHILD_CATEGORY";
    public static final String IS_CHILD = "IS_CHILD";

    public static void startActivityCategoryDetail(LixiActivity activity, int categoryId, boolean isChild, String... params) {
        Intent intent = new Intent(activity, ActivityCategoryDetail.class);
        intent.putExtra(CATEGORY_ID, categoryId);
        intent.putExtra(IS_CHILD, isChild);
        for (int i = 0; i < params.length; i++) {
            String[] param = params[i].split(":");
            if (param.length > 1) {
                String key = param[0];
                String value = param[1];
                switch (key) {
                    case IS_CHILD_CATEGORY:
                        intent.putExtra(IS_CHILD_CATEGORY, value);
                        break;
                    case CATEGORY_NAME:
                        intent.putExtra(CATEGORY_NAME, value);
                        break;
                }
            }
        }
        activity.startActivity(intent);
    }

    private RelativeLayout rl_search;
    private ImageView iv_back;
    private ImageView iv_search;
    private EditText et_search;
    private ImageView iv_delete;
    private SwipeRefreshLayout swr;
    private RecyclerView rv;

    private String categoryName;
    private int categoryId;
    private String isChildCategory = "";

    AdapterCategoryContent adapter;

    private String keyWord = "";
    private String mScreenName = "home-category";

    private CategoryDetailActivityBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.category_detail_activity);
        categoryId = getIntent().getIntExtra(CATEGORY_ID, 0);
        categoryName = getIntent().getStringExtra(CATEGORY_NAME);
        isChildCategory = getIntent().getStringExtra(IS_CHILD_CATEGORY);
        boolean isChild = getIntent().getBooleanExtra(IS_CHILD, false);
        activity.id = (long) categoryId;
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.VOUCHER_CATEGORY,
                categoryId,
                TrackingConstant.Currency.VND,
                0,
                TrackingConstant.ContentSource.HOME
        );
        if (isChildCategory == null)
            isChildCategory = "";

        if (isChild)
            mScreenName = "home-subcategory";

        findViews();
        initView();

        startLoadData();
    }

    private void findViews() {
        iv_back = (ImageView) findViewById(R.id.iv_back);

        rl_search = (RelativeLayout) findViewById(R.id.rl_search);
        et_search = (EditText) findViewById(R.id.et_search);
        iv_search = (ImageView) findViewById(R.id.iv_search);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);

        swr = findViewById(R.id.swr);
        rv = (RecyclerView) findViewById(R.id.rv);

        iv_back.setOnClickListener(this);
        iv_delete.setOnClickListener(this);
    }

    private void initView() {
        swr.setOnRefreshListener(this);
        rv.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new AdapterCategoryContent(activity, null, isChildCategory);
        rv.setAdapter(adapter);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if (!isLoading && !noRemain && isRvBottom()) {
                    if (adapter == null)
                        return;
                    getVoucher();
                }
            }
        });

        adapter.setListener(new AdapterCategoryContent.IAdapterCategoryContetnListener() {
            @Override
            public void onClickViewAllMerchant() {
                Intent intent = new Intent(activity, ActivityListMerchant.class);
                intent.putExtra(ActivityListMerchant.EXTRA_CATEGORY_ID, categoryId);
                intent.putExtra(ActivityListMerchant.EXTRA_NAME, categoryName);
                activity.startActivity(intent);
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String key = et_search.getText().toString();
                if (key.length() > 2 || key.length() == 0)
                    if (!key.equals(keyWord)) {
                        keyWord = key;
                        startLoadData();
                    }
            }
        });
    }

    public void startLoadData() {

        isLoading = false;
        noRemain = false;
        page = 1;

        if (keyWord.length() == 0) {
//            if (adapter != null)
//                adapter.showSubCategory(true);
            if (isChildCategory == null || isChildCategory.length() == 0)
                getCategory();
        } else if (adapter != null) {
//            adapter.showSubCategory(false);
            adapter.setListCategory(null);
        }

        getMerchant();
        getVoucher();
    }

    //region handle ACTION
    @Override
    public void onRefresh() {
        startLoadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.rl_search:
//                SearchingActivity.startSearchActivity(activity, categoryId);
                break;
            case R.id.iv_delete:
                et_search.setText("");
                break;
        }
    }
    //endregion

    //region LoadData
    int page = 1;
    int pageSize = 20;
    boolean isLoading;
    boolean noRemain = false;

    private boolean isRvBottom() {
        if (adapter.getListVoucher() == null || adapter.getListVoucher().size() == 0)
            return false;
        int totalItemCount = rv.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && adapter.getListVoucher().size() >= pageSize);
//                && totalItemCount >= lixiHomeAdapter.getList_voucher().size());
    }

    public void resetList() {
        if (adapter == null || adapter.getListVoucher() == null) return;
        adapter.getListVoucher().clear();
        adapter.notifyDataSetChanged();
    }

    private void getCategory() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<ProductCategoryModel>> call = DataLoader.apiService._getCategory(1, 20, categoryId);

        call.enqueue(new Callback<ApiWrapperForListModel<ProductCategoryModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<ProductCategoryModel>> call,
                                   Response<ApiWrapperForListModel<ProductCategoryModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<ProductCategoryModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        adapter.setListCategory(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<ProductCategoryModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMerchant() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<MerchantModel>> call = DataLoader.apiService._getCategoryMerchant(1, 20, categoryId, keyWord);

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantModel>> call,
                                   Response<ApiWrapperForListModel<MerchantModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        adapter.setListMerchant(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getVoucher() {
        DataLoader.setupAPICall(activity, null);


        isLoading = true;
        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getCategoryVoucher(page, pageSize, categoryId, keyWord);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                swr.setRefreshing(false);
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                return;
                            }
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        adapter.setListVoucher(result.getRecords());
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //endregion
}
