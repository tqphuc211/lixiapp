package com.opencheck.client.home.search;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.databinding.SearchFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.FilterModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class SearchActivity extends LixiActivity {

    //region Layout Variable
    private EditText edt_search;
    private ImageView iv_delete;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swiperefresh;
    private RelativeLayout rl_back;
    //endregion

    //region Logic Variable
    private ArrayList<LixiShopEvoucherModel> listVoucher = new ArrayList<>();
    private ArrayList<MerchantModel> listPlace = new ArrayList<>();
    private Boolean isLoading = false;
    private int pageVoucher = 1;
    private LinearLayoutManager mLayoutManager;
    private FilterModel hashTags = new FilterModel();
    private Boolean isFilter = false;
    private boolean isSearchClick = false;
    // private static boolean isFirstLauch = true;
    private boolean isOutOfVoucherPage = false;
    private static final int RECORD_PER_PAGE = DataLoader.record_per_page;
    private boolean isTag = false;
    //endregion

    //region Adapter, Controller
    private AdapterSearching adapterSearching;

    //endregion

    //region Processing UI

    private SearchFragmentBinding searchFragmentBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchFragmentBinding = DataBindingUtil.setContentView(this, R.layout.search_fragment);
        Bundle data = getIntent().getExtras();
        String filterJson = data.getString(ConstantValue.FILTER_MODEL_MERCHANT, "");
        if (!filterJson.equals("")) {
            hashTags = new Gson().fromJson(filterJson, FilterModel.class);
        } else {
            hashTags = new FilterModel();
            isFilter = false;
        }
        isSearchClick = data.getBoolean("ISSEARCHCLICK");
        initView();
    }

    private void initView() {
        edt_search = findViewById(R.id.edt_search);
        recyclerView = findViewById(R.id.rclv_voucher);
        iv_delete = findViewById(R.id.iv_delete);
        swiperefresh = findViewById(R.id.swiperefresh);
        rl_back = findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        iv_delete.setOnClickListener(this);
        edt_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if(hasFocus){
                    imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                }
                else {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initData();
            }
        }, 300);

    }

    private void initData(){
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                pageVoucher = 1;
                isOutOfVoucherPage = false;
                isLoading = false;
                String str = charSequence.toString();
                if(charSequence.toString().contains("#")) {
                    for (int i = str.length() - 1; i >= 0; i--) {
                        if (str.charAt(i) == '#') {
                            str = str.substring(i+1, str.length());
                            break;
                        }
                    }
                    isTag = true;
                }
                else
                    isTag = false;
                loadData(str);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(recyclerView.computeVerticalScrollOffset() > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();


                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                        Helper.showLog("Load more");
                        getVoucher(edt_search.getText().toString());
                        isLoading = true;
                    }
                }
            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!listVoucher.isEmpty())
                {
                    listVoucher.clear();
                    adapterSearching.notifyDataSetChanged();
                }
                isLoading = false;
                pageVoucher = 1;
                listPlace.clear();
                isOutOfVoucherPage = false;
                final String str = edt_search.getText().toString();
//                if(edt_search.getText().toString().contains("#")) {
//                    str = str.replace("#", "");
//                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadData(str);
                    }
                }, 300);
                //             getListMerchant(str);
            }
        });

        if(isFilter) {

            if (!hashTags.getKeyWord().equalsIgnoreCase("")) {
                String keyword = hashTags.getKeyWord();
                if(!isSearchClick) {
                    edt_search.setText(keyword);

                }
                else{
//                    if(!isFirstLauch)
//                        edt_search.setText(keyword);
//                    else {
//                        isFirstLauch = false;
                    edt_search.setText("");
                    //}
                }
                edt_search.setSelection(edt_search.getText().length());
                //loadData(edt_search.getText().toString());
            }

        }
        else{
            loadData(edt_search.getText().toString());
            edt_search.setFocusable(true);
        }
    }

    //endregion

    //region Handle UI Logic
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_back:
                activity.onBackPressed();
                //isFirstLauch = true;
                break;
            case R.id.iv_delete:
                edt_search.setText("");
        }
    }

    private void setAdapter(){
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        adapterSearching = new AdapterSearching(listPlace, activity, listVoucher, hashTags, edt_search);
        recyclerView.setAdapter(adapterSearching);
    }

    //endregion

    //region Background Processing


    private void loadData(String key){
        if(key.contains("#")) {
            key = key.replace("#", "");
        }
        getVoucher(key);
        getListMerchant(key);
    }

    private void getListMerchant(String key) {
        DataLoader.searchListMerchant(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                handleGetListMerchantSuccess(object);
            }
        }, 1, key, isTag);
    }

    private void handleGetListMerchantSuccess(Object objects) {
        listPlace = (ArrayList<MerchantModel>) objects;
        if(adapterSearching != null) {
            adapterSearching.reloadPlace(listPlace);
        }
    }

    private void getVoucher(String key){
        if(key.contains("#")) {
            key = key.replace("#", "");
        }
        if(!isLoading){
            if (!isOutOfVoucherPage) {
                DataLoader.searchVoucher(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        swiperefresh.setRefreshing(false);
                        if (isSuccess) {
                            handleGetVoucherSuccess(object);
                        } else {
                            isLoading = true;
                        }
                    }
                }, pageVoucher, key);
            }
        }

    }

    private void handleGetVoucherSuccess(Object objects) {
        if (pageVoucher == 1) {
            handleIsFirstVoucherPage(objects);
        } else {
            handleLoadMoreVoucher(objects);
        }

        isLoading = false;
        pageVoucher++;
    }

    private void handleLoadMoreVoucher(Object objects) {
        ArrayList<LixiShopEvoucherModel> list = (ArrayList<LixiShopEvoucherModel>) objects;
        setOutOfVoucherPage(list);
        if (listVoucher == null || list == null || list.size() == 0) {

            return;
        }

        listVoucher.addAll(list);
        adapterSearching.notifyDataSetChanged();
    }

    private void setOutOfVoucherPage(ArrayList<LixiShopEvoucherModel> list) {
        if (list.size() < RECORD_PER_PAGE) {
            isOutOfVoucherPage = true;
        }
    }

    private void handleIsFirstVoucherPage(Object objects) {
        listVoucher = (ArrayList<LixiShopEvoucherModel>) objects;
        setOutOfVoucherPage(listVoucher);
        if (listVoucher == null || listVoucher.size() == 0) {
            return;
        }
        setAdapter();
    }



    //endregion
}
