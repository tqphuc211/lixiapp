package com.opencheck.client.home.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PromotionItemLayoutBinding;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<PromotionModel> promotionList;
    private ItemClickListener itemClickListener;

    public PromotionAdapter(LixiActivity activity, ArrayList<PromotionModel> promotionList) {
        this.activity = activity;
        this.promotionList = promotionList;
    }

    public void setPromotionList(ArrayList<PromotionModel> promotionList) {
        this.promotionList = promotionList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PromotionItemLayoutBinding promotionItemLayoutBinding =
                PromotionItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(promotionItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PromotionModel model = promotionList.get(position);
        holder.tv_code.setText(model.getCode());
        holder.tv_copy_code.setText(LanguageBinding.getString(R.string.copy, activity));
        ViewGroup.LayoutParams params = holder.rl_root.getLayoutParams();
        params.width = activity.widthScreen * 2 / 3;

        holder.rl_root.setLayoutParams(params);
        if (model.getDescription() != null && model.getDescription().length() > 0) {
            holder.tv_description.setVisibility(View.VISIBLE);
            holder.tv_description.setText(model.getDescription());
        } else
            holder.tv_description.setVisibility(View.GONE);

        holder.tv_timer.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_expired, activity), Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(model.getEnd_date()))));
        holder.tv_type.setText(model.getName());

        holder.rl_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.copyCode(model.getCode(), activity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promotionList == null ? 0 : promotionList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_type, tv_value, tv_description, tv_timer, tv_code, tv_copy_code;
        private RelativeLayout rl_root, rl_copy;
        private View v_line;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_type = itemView.findViewById(R.id.tv_type);
            tv_value = itemView.findViewById(R.id.tv_value);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_timer = itemView.findViewById(R.id.tv_timer);
            tv_code = itemView.findViewById(R.id.tv_code);
            rl_root = itemView.findViewById(R.id.rl_root);
            tv_copy_code = itemView.findViewById(R.id.tv_copy_code);
            v_line = itemView.findViewById(R.id.v_line);
            rl_copy = itemView.findViewById(R.id.rl_copy);
        }
    }
}
