package com.opencheck.client.home.account.activecode.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ConfirmActiveCodeFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.activecode.dialogs.NotifyActiveDialog;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.history.detail.GeneralQRCodeAndCropTask;
import com.opencheck.client.models.lixishop.ActiveCodeModel;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class ConfirmActiveCodeFragment extends LixiFragment {

    private Button btnClose;
    private TextView tv_title, tv_price, tv_code, tv_name, tv_address, tv_agree, tv_cancel, tv_note_message, tv_support_message;
    private ImageView iv_merchant;
    private ImageView iv_qr;
    private EvoucherDetailModel evoucherModel;
    private StoreApplyModel storeApplyModel;

    private ConfirmActiveCodeFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (activity instanceof EVoucherAccountActivity)
            ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.GONE);
        mBinding = ConfirmActiveCodeFragmentBinding.inflate(inflater, container, false);
        evoucherModel = new Gson().fromJson(getArguments().getString("ev"), EvoucherDetailModel.class);
        storeApplyModel = new Gson().fromJson(getArguments().getString("sa"), StoreApplyModel.class);
        initViews(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initViews(View v) {
        btnClose = v.findViewById(R.id.btnClose);
        tv_title = v.findViewById(R.id.tv_title);
        tv_price = v.findViewById(R.id.tv_price);
        tv_code = v.findViewById(R.id.tv_code);
        tv_name = v.findViewById(R.id.tv_name);
        tv_address = v.findViewById(R.id.tv_address);
        tv_agree = v.findViewById(R.id.tv_agree);
        tv_cancel = v.findViewById(R.id.tv_cancel);
        iv_merchant = v.findViewById(R.id.iv_merchant);
        iv_qr = v.findViewById(R.id.iv_qr);
        tv_note_message = v.findViewById(R.id.tv_note_message);
        tv_support_message = v.findViewById(R.id.tv_support_message);

        btnClose.setOnClickListener(this);
        tv_agree.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        initData();
    }

    private void initData() {
        tv_title.setText(evoucherModel.getName());
        tv_price.setText(Helper.getVNCurrency(evoucherModel.getPrice()) + activity.getString(R.string.p));
        new GeneralQRCodeAndCropTask(activity, evoucherModel.getPin(), iv_qr, new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (pos == 1) {
                    iv_qr.setImageBitmap((Bitmap) object);
                }
            }
        });
        tv_code.setText(evoucherModel.getPin());
        ImageLoader.getInstance().displayImage(evoucherModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(storeApplyModel.getName());
        tv_address.setText(storeApplyModel.getAddress());
        tv_note_message.setText(Html.fromHtml(activity.getResources().getString(R.string.note_message)));
        tv_support_message.setText(Html.fromHtml(activity.getResources().getString(R.string.support_message)));
    }

    private void submitActiveCode() {
        if (storeApplyModel.getStore_code() != null) {
            final JsonObject obj = new JsonObject();
            obj.addProperty("store_code", storeApplyModel.getStore_code());
            obj.addProperty("voucher_id", evoucherModel.getId());
            DataLoader.submitActiceCode(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        ActiveCodeModel activeCodeModel = (ActiveCodeModel) object;
                        NotifyActiveDialog popupNotifyActive = new NotifyActiveDialog(activity, false, true, false);
                        popupNotifyActive.setCancelable(false);
                        popupNotifyActive.show();
                        popupNotifyActive.setData(activity.getString(R.string.valid_code_message), evoucherModel, storeApplyModel, getArguments().getInt(ConstantValue.CURRENT_POSITION));
                        new AppPreferenceHelper(activity).setOnRefresh(true);
                    } else {
                        Helper.showErrorDialog(activity, object.toString());
                    }
                }
            }, obj);
        } else
            Toast.makeText(activity, "Không thể yêu cầu kích hoạt", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                activity.onBackPressed();
                break;
            case R.id.tv_agree:
                submitActiveCode();
                break;
            case R.id.tv_cancel:
                activity.onBackPressed();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        ((ActiveCodeActivity) activity).toolbar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        ((EVoucherAccountActivity) activity).constraintParentView.setVisibility(View.VISIBLE);
    }
}
