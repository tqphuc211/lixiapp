package com.opencheck.client.home.delivery.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PaymentItemLayoutBinding;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<PaymentModel> paymentList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int lastPost = -1;

    public PaymentAdapter(LixiActivity activity) {
        this.activity = activity;
    }

    public void setPaymentList(ArrayList<PaymentModel> paymentList) {
        this.paymentList = paymentList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentItemLayoutBinding paymentItemLayoutBinding =
                PaymentItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(paymentItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name.setText(paymentList.get(position).getName());
        ImageLoader.getInstance().displayImage(paymentList.get(position).getLogo_link(), holder.iv, LixiApplication.getInstance().optionsNomal);

        if (paymentList.get(position).isSelected()) {
            holder.iv_tick.setVisibility(View.VISIBLE);
        } else {
            holder.iv_tick.setVisibility(View.GONE);
        }

        if (paymentList.get(position).getState() == null)
            return;

        if (paymentList.get(position).getState().equals("active")) {
            holder.tv_status.setVisibility(View.GONE);
            holder.tvInfoPolicy.setVisibility(View.VISIBLE);
            holder.tv_name.setTextColor(activity.getResources().getColor(R.color.color_text_payment));
            if (paymentList.get(position).getStore_policy() != null)
                holder.tvInfoPolicy.setText(Html.fromHtml(paymentList.get(position).getStore_policy().getName() + " " + paymentList.get(position).getStore_policy().getDescription()));
        } else {
            holder.tvInfoPolicy.setVisibility(View.GONE);
            holder.tv_status.setVisibility(View.VISIBLE);
            holder.tv_name.setTextColor(activity.getResources().getColor(R.color.color_alpha_text_payment));
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return paymentList == null ? 0 : paymentList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_tick, iv;
        private TextView tv_name, tv_status, tvInfoPolicy;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            iv_tick = itemView.findViewById(R.id.iv_tick);
            iv = itemView.findViewById(R.id.iv);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tvInfoPolicy = itemView.findViewById(R.id.tvInfoPolicy);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemCLick(getAdapterPosition(), "", paymentList.get(getAdapterPosition()), 0L);
                }
            });
        }

    }

    private PaymentModel curPayment = null;

    public PaymentModel getPaymetMethod() {
        return curPayment;
    }

    public void performClick(int position) {
        PaymentModel currentMethod = paymentList.get(position);
        if (lastPost == -1) {
            paymentList.get(position).setSelected(!currentMethod.isSelected());
            notifyDataSetChanged();
        } else {
            if (lastPost != position) {
                paymentList.get(position).setSelected(!currentMethod.isSelected());
                paymentList.get(lastPost).setSelected(false);
                notifyDataSetChanged();
            }
        }

        curPayment = paymentList.get(position);
        lastPost = position;
    }
}
