package com.opencheck.client.home.delivery.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ReasonItemLayoutBinding;
import com.opencheck.client.home.delivery.model.ClaimReasonModel;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class SupportReasonAdapter extends RecyclerView.Adapter<SupportReasonAdapter.ViewHolder> {

    private LixiActivity activity;
    private List<ClaimReasonModel> reasons;
    private ItemClickListener itemClickListener;

    public SupportReasonAdapter(LixiActivity activity) {
        this.activity = activity;
        this.reasons = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        ReasonItemLayoutBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.reason_item_layout, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindingTo(reasons.get(position));
    }

    @Override
    public int getItemCount() {
        return reasons == null ? 0 : reasons.size();
    }

    public void setItems(List<ClaimReasonModel> items) {
        this.reasons.addAll(items);
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public ClaimReasonModel getItem(int position) {
        if (position < 0 || position >= reasons.size())
            return reasons.get(0);
        return reasons.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ReasonItemLayoutBinding mBinding;

        public ViewHolder(ReasonItemLayoutBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemCLick(getAdapterPosition(), "", null, 0L);
                }
            });
        }

        void bindingTo(ClaimReasonModel item) {
            mBinding.tvReason.setText(item.getMessage());
        }
    }
}
