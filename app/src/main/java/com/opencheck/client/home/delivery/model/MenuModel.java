package com.opencheck.client.home.delivery.model;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuModel implements Serializable {
    private long category_id;
    private String category_name;
    private int is_hot;
    private ArrayList<ProductModel> list_product;

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public ArrayList<ProductModel> getList_product() {
        return list_product;
    }

    public void setList_product(ArrayList<ProductModel> list_product) {
        this.list_product = list_product;
    }

    public int getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(int is_hot) {
        this.is_hot = is_hot;
    }
}
