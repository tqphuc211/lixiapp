package com.opencheck.client.home.delivery.controller;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ExtraInfoItemLayoutBinding;
import com.opencheck.client.databinding.RowHistoryLocationLayoutBinding;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.dialog.ShipFeeDialog;
import com.opencheck.client.home.delivery.fragments.DeliveryHomeFragment;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

public class HistoryLocationController {
    private LixiActivity activity;
    private LinearLayout lnRoot;
    private int position;
    private int size;
    private View viewChild;
    private TextView tvAddress, tvFullAddress;
    private ConstraintLayout constraintHistoryLocation;
    private DeliAddressModel mAddressModel;
    private RowHistoryLocationLayoutBinding mBinding;
    private boolean isLastLocation;

    public HistoryLocationController(LixiActivity _activity, DeliAddressModel _mAddressModel, LinearLayout _lnRoot, int _position, int _size, boolean _isLastLocation) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.size = _size;
        this.mAddressModel = _mAddressModel;
        this.isLastLocation = _isLastLocation;

        mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity),
                R.layout.row_history_location_layout, null, false);
        this.viewChild = mBinding.getRoot();
        this.initView();
        this.initData();
        this.lnRoot.addView(viewChild);
    }

    private void initView() {
        tvAddress = viewChild.findViewById(R.id.tvAddress);
        tvFullAddress = viewChild.findViewById(R.id.tvFullAddress);
        constraintHistoryLocation = viewChild.findViewById(R.id.constraintHistoryLocation);
    }

    private void initData() {

        tvFullAddress.setText(mAddressModel.getFullAddress());
        tvAddress.setText(mAddressModel.getAddress());

        constraintHistoryLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppPreferenceHelper.getInstance().setDefaultLocation(mAddressModel);
                DeliDataLoader.checkChangeProvinceHeader(activity);
                DeliveryHomeFragment.isRefreshLocation = true;
                if (!isLastLocation) {
                    sendBroadCast(mAddressModel);
                }
                activity.finish();
            }
        });
    }

    private void sendBroadCast(DeliAddressModel addressModel) {
        Intent intent = new Intent("LOCATION_CHANGE");
        Bundle data = new Bundle();
        data.putSerializable("LOCATION", new DeliAddressModel(addressModel.getPlaceId(),
                addressModel.getFullAddress(),
                addressModel.getAddress(),
                addressModel.getLat(),
                addressModel.getLng(),
                addressModel.isGPS()));
        intent.putExtras(data);
        activity.sendBroadcast(intent);
    }

}
