package com.opencheck.client.home.account.evoucher.Model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.opencheck.client.home.account.evoucher.Enum.EvoucherStatus;

public class MCActiveModel extends ViewModel {
    MutableLiveData<EvoucherStatus> status;

    public MutableLiveData<EvoucherStatus> getStatus(){
        if (status == null){
            status = new MutableLiveData<EvoucherStatus>();
        }
        return status;
    }
}
