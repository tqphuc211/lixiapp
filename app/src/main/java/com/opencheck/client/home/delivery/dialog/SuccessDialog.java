package com.opencheck.client.home.delivery.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DeliSuccessDialogBinding;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;

public class SuccessDialog extends LixiDialog {

    private TextView tv_done;

    public SuccessDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private DeliSuccessDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DeliSuccessDialogBinding.inflate(LayoutInflater
                .from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        tv_done = findViewById(R.id.tv_done);
        tv_done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == tv_done) {
            Bundle data = new Bundle();
            // data.putString("uuid", uuid);
            Intent intent = new Intent(activity, OrderTrackingActivity.class);
            intent.putExtras(data);
            activity.startActivity(intent);
        }
    }
}
