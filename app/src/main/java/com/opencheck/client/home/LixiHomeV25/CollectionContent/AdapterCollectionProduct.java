package com.opencheck.client.home.LixiHomeV25.CollectionContent;

import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.flashsale.Global.VoucherFlashSaleHolder;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/8/2018.
 */

public class AdapterCollectionProduct extends RecyclerView.Adapter {

    LixiActivity activity;

    public ArrayList<LixiShopEvoucherModel> getListCode() {
        return listCode;
    }

    public void setListCode(ArrayList<LixiShopEvoucherModel> listCode) {
        this.listCode = listCode;
        notifyDataSetChanged();
    }

    ArrayList<LixiShopEvoucherModel> listCode;

    public AdapterCollectionProduct(LixiActivity activity, ArrayList<LixiShopEvoucherModel> listCode) {
        this.activity = activity;
        this.listCode = listCode;
    }

    private int VOUCHER = 1;
    private int VOUCHER_FS = -1;

    @Override
    public int getItemViewType(int position) {
        if (listCode != null && listCode.size() > position)
            if (listCode.get(position).getFlash_sale_id() > 0)
                return VOUCHER_FS;
        return VOUCHER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherFsHolder = VoucherFlashSaleHolder.newHolder(activity, parent);
            return voucherFsHolder;
        }

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.row_lixi_main_voucher_layout, parent, false);

        return new HolderItem(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherHolder = (VoucherFlashSaleHolder) holder;
            if (listCode.size() <= position)
                return;
            LixiShopEvoucherModel voucher = listCode.get(position);

            VoucherFlashSaleHolder.bindViewHolder(voucherHolder, voucher);
            return;
        }

        HolderItem voucherHolder = (HolderItem) holder;
        LixiShopEvoucherModel voucher = listCode.get(position);

        if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0)
            ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        else {
            if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0)
                ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        }
        voucherHolder.tvVoucherName.setText(voucher.getName());

        voucherHolder.tvVoucherPrice.setText(Helper.getVNCurrency(voucher.getPayment_discount_price()) + "đ");
        if (voucher.getPayment_discount_price() != voucher.getPayment_price()) {
            voucherHolder.tvOldPrice.setVisibility(View.VISIBLE);
            voucherHolder.tvOldPrice.setText(Helper.getVNCurrency(voucher.getPayment_price()) + "đ");
            voucherHolder.tvOldPrice.setPaintFlags(voucherHolder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else voucherHolder.tvOldPrice.setVisibility(View.GONE);
        if (voucher.getCashback_price() > 0) {
            voucherHolder.tvLixiBonus.setVisibility(View.VISIBLE);
            voucherHolder.tvLixiBonus.setText(Helper.getVNCurrency(voucher.getCashback_price()) + " Lixi");
        } else voucherHolder.tvLixiBonus.setVisibility(View.GONE);

        voucherHolder.tvMerchantInfo.setText(voucher.getMerchant_name() + " | " + voucher.getCount_store() + " cửa hàng");
        voucherHolder.tvCountAttend.setText(voucher.getQuantity_order_done() + " " + LanguageBinding.getString(R.string.quantity_bought_customer, activity));

        if (voucher.getPayment_price() != voucher.getPayment_discount_price()) {
            voucherHolder.tv_percent_discount.setVisibility(View.VISIBLE);
            int percent = 100 - (int) (voucher.getPayment_discount_price() * 100 / voucher.getPayment_price());
            voucherHolder.tv_percent_discount.setText("-" + percent + "%");
        } else voucherHolder.tv_percent_discount.setVisibility(View.GONE);

        voucherHolder.tvDistance.setVisibility(View.GONE);
        voucherHolder.itemView.setTag(position);


    }

    class HolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        private View v_line;
        private ImageView imgLogoVoucher;
        private TextView tv_percent_discount;
        private TextView tvVoucherName;
        private TextView tvVoucherPrice;
        private TextView tvOldPrice;
        private TextView tvLixiBonus;
        private TextView tvMerchantInfo;
        private TextView tvCountAttend;
        private TextView tvDistance;

        public HolderItem(View itemView) {
            super(itemView);
            rootView = itemView;
            rootView.setOnClickListener(this);
            v_line = itemView.findViewById(R.id.v_line);
            imgLogoVoucher = (ImageView) itemView.findViewById(R.id.imgLogoVoucher);
            tv_percent_discount = (TextView) itemView.findViewById(R.id.tv_percent_discount);
            tvVoucherName = (TextView) itemView.findViewById(R.id.tvVoucherName);

            tvVoucherPrice = (TextView) itemView.findViewById(R.id.tvVoucherPrice);
            tvOldPrice = (TextView) itemView.findViewById(R.id.tvOldPrice);
            tvLixiBonus = (TextView) itemView.findViewById(R.id.tvLixiBonus);
            tvMerchantInfo = (TextView) itemView.findViewById(R.id.tvMerchantInfo);
            tvCountAttend = (TextView) itemView.findViewById(R.id.tvCountAttend);

            tvDistance = (TextView) itemView.findViewById(R.id.tvDistance);
        }

        @Override
        public void onClick(View v) {
            if (v == rootView) {
//                CashEvoucherDetailActivity.startCashVoucherDetail(activity, listCode.get((int) v.getTag()).getId());
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, listCode.get((int) v.getTag()).getId(), TrackingConstant.ContentSource.VOUCHER_COLLECTION);
            }
        }
    }


    @Override
    public int getItemCount() {
        if (listCode == null)
            return 0;
        return listCode.size();
    }
}