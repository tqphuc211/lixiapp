package com.opencheck.client.home.account.activecode2.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by Tony Tuan on 04/02/2018.
 */

public class UnusedVoucherAdapter extends RecyclerView.Adapter<UnusedVoucherAdapter.VoucherHolder>{
    private LixiActivity activity;
    private ArrayList<LixiShopEvoucherBoughtModel> voucherList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public UnusedVoucherAdapter(LixiActivity activity, ArrayList<LixiShopEvoucherBoughtModel> voucherList) {
        this.activity = activity;
        this.voucherList = voucherList;
    }

    @Override
    public VoucherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VoucherHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.e_voucher_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(VoucherHolder holder, final int position) {
        LixiShopEvoucherBoughtModel evoucherBoughtModel = voucherList.get(position);

        if(evoucherBoughtModel.getProduct_cover_image() == null || evoucherBoughtModel.getProduct_cover_image().equals(""))
            ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_image().get(0), holder.iv_hinh, LixiApplication.getInstance().optionsNomal);
        else
            ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_cover_image(), holder.iv_hinh, LixiApplication.getInstance().optionsNomal);

        holder.tv_name.setText(evoucherBoughtModel.getProduct_name());
        holder.tv_expire_date.setText(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_DATE,
                String.valueOf(Long.parseLong(evoucherBoughtModel.getExpired_time()) * 1000)));
        holder.tv_code.setText(evoucherBoughtModel.getPin());
        holder.tv_price.setText(Helper.getVNCurrency(evoucherBoughtModel.getPrice()) + "đ");


        if(evoucherBoughtModel.getState() != null && evoucherBoughtModel.getState().equals("waiting")){
            holder.tv_timer.setVisibility(View.VISIBLE);
            holder.tv_timer.setText(LanguageBinding.getString(R.string.state, activity));
        }
        else holder.tv_timer.setVisibility(View.GONE);

        if(evoucherBoughtModel.isSelected())
            holder.iv_tick.setVisibility(View.VISIBLE);
        else
            holder.iv_tick.setVisibility(View.GONE);

        holder.rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemCLick(position, ConstantValue.ON_ITEM_CLICK, voucherList.get(position), 0L);
            }
        });
    }

    @Override
    public int getItemCount() {
        return voucherList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class VoucherHolder extends RecyclerView.ViewHolder{
        private TextView tv_name, tv_code, tv_expire_date, tv_price, tv_timer;
        private ImageView iv_hinh;
        private View rlRoot, iv_tick;

        public VoucherHolder(View itemView) {
            super(itemView);
            rlRoot = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_code = itemView.findViewById(R.id.tv_code);
            tv_expire_date = itemView.findViewById(R.id.tv_expire_date);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_timer = itemView.findViewById(R.id.tv_timer);
            iv_hinh = itemView.findViewById(R.id.iv_hinh);
            iv_tick = itemView.findViewById(R.id.iv_tick);
        }

    }
}
