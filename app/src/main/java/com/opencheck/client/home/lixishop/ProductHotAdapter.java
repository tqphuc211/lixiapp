package com.opencheck.client.home.lixishop;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowVoucherLayoutBinding;
import com.opencheck.client.home.lixishop.detail.EvoucherDetail;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/7/2018.
 */


public class ProductHotAdapter extends RecyclerView.Adapter<ProductHotAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<LixiHotProductModel> list_voucher;

    public ProductHotAdapter(LixiActivity activity, ArrayList<LixiHotProductModel> list_voucher) {
        this.list_voucher = list_voucher;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowVoucherLayoutBinding rowVoucherLayoutBinding =
                RowVoucherLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ProductHotAdapter.ViewHolder(rowVoucherLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.rootItemView.setTag(position);
        holder.tvName.setText(list_voucher.get(position).getName());

//        int width = (int) (((HomeActivity) activity).widthScreen  * 0.4);
        int width = (int) activity.getResources().getDimension(R.dimen.value_160);
        ViewGroup.LayoutParams params = holder.ivHinh.getLayoutParams();
        params.width = width;
        params.height = 7 * holder.ivHinh.getLayoutParams().width / 10;
        holder.ivHinh.setLayoutParams(params);
        holder.tvPrice.setText("" + Helper.getVNCurrency(list_voucher.get(position).getDiscount_price()) + " " + activity.getResources().getString(R.string.lixi));
        holder.tvAmountPeople.setText("" + list_voucher.get(position).getQuantity_order_done());
        if (position == 0)
            holder.v_space.setVisibility(View.VISIBLE);
        else
            holder.v_space.setVisibility(View.GONE);

        if (list_voucher.get(position).getProduct_cover_image() == null || list_voucher.get(position).getProduct_cover_image().equals("")) {
            ImageLoader.getInstance().displayImage(list_voucher.get(position).getProduct_image().get(0), holder.ivHinh, LixiApplication.getInstance().optionsNomal);
        } else {
//            ImageLoader.getInstance().displayImage(list_voucher.get(position).getProduct_cover_image(), holder.ivHinh, LixiApplication.getInstance().optionsNomal);
            LixiImage.with(activity)
                    .load(list_voucher.get(position).getProduct_cover_image())
                    .size(ImageSize.AUTO)
                    .into(holder.ivHinh);
        }

    }

    @Override
    public int getItemCount() {
        return list_voucher.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivHinh;
        private TextView tvName, tvPrice, tvAmountPeople;
        private LinearLayout container;
        private View v_space;
        private View rootItemView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootItemView = itemView;
            ivHinh = itemView.findViewById(R.id.iv_hinh);
            v_space = itemView.findViewById(R.id.v_space);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvAmountPeople = itemView.findViewById(R.id.tv_amount_people);
            container = itemView.findViewById(R.id.ll_item);

            rootItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, EvoucherDetail.class);
                    intent.putExtra(EvoucherDetail.PRODUCT_ID, list_voucher.get((int) view.getTag()).getId());
                    intent.putExtra(GlobalTracking.SOURCE, TrackingConstant.ContentSource.REWARD_HOME);
                    activity.startActivity(intent);
                }
            });
        }
    }
}