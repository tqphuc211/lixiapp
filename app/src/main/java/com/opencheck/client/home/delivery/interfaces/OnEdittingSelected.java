package com.opencheck.client.home.delivery.interfaces;

import com.opencheck.client.home.delivery.enum_key.TypeLocation;

public interface OnEdittingSelected {
    void onSelected(int type, TypeLocation action);
}
