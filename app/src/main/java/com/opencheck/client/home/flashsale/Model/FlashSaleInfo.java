package com.opencheck.client.home.flashsale.Model;

public class FlashSaleInfo {

    private long server_current;
    private long my_start_time;
    private int cashback_price;
    private long end_time;
    private int payment_discount_price;
    private int payment_price;
    private int quantity_limit;
    private int quantity_order_done;
    private long start_time;

    public int getCashback_price() {
        return cashback_price;
    }

    public void setCashback_price(int cashback_price) {
        this.cashback_price = cashback_price;
    }

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public int getPayment_price() {
        return payment_price;
    }

    public void setPayment_price(int payment_price) {
        this.payment_price = payment_price;
    }

    public int getQuantity_limit() {
        return quantity_limit;
    }

    public void setQuantity_limit(int quantity_limit) {
        this.quantity_limit = quantity_limit;
    }

    public int getQuantity_order_done() {
        return quantity_order_done;
    }

    public void setQuantity_order_done(int quantity_order_done) {
        this.quantity_order_done = quantity_order_done;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    /**
     * cashback_price : 0
     * end_time : 1531902600
     * payment_discount_price : 14000
     * payment_price : 50000
     * quantity_limit : 100
     * quantity_order_done : 18
     * start_time : 1531890000
     */
    public long getServer_current() {
        return server_current;
    }

    public void setServer_current(long server_current) {
        this.server_current = server_current;
        this.my_start_time = System.currentTimeMillis();
    }

    public long getMy_start_time() {
        return my_start_time;
    }

    public void setMy_start_time(long my_start_time) {
        this.my_start_time = my_start_time;
    }
}
