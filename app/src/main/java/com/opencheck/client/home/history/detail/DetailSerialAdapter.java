package com.opencheck.client.home.history.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowDetailSerialMobileCardBinding;
import com.opencheck.client.models.lixishop.DetailHistoryCardModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class DetailSerialAdapter extends RecyclerView.Adapter<DetailSerialAdapter.ViewHolder> {
    LixiActivity activity;
    ArrayList<DetailHistoryCardModel> list;

    public DetailSerialAdapter(LixiActivity activity, ArrayList<DetailHistoryCardModel> list) {
        this.activity = activity;
        this.list = list;
    }

    public void reLoadData(ArrayList<DetailHistoryCardModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowDetailSerialMobileCardBinding rowDetailSerialMobileCardBinding =
                RowDetailSerialMobileCardBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(rowDetailSerialMobileCardBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            ImageLoader.getInstance().displayImage(list.get(position).getLogo().toString(), holder.imgLogo, LixiApplication.getInstance().optionsNomal);
            holder.tvPin.setText(list.get(position).getPin());
            holder.tvSerial.setText(LanguageBinding.getString(R.string.serial, activity) + ": " + list.get(position).getSerial());
            holder.tvPrice.setText(Helper.getVNCurrencyShort(list.get(position).getPrice()));
            holder.tvExpireDay.setText(LanguageBinding.getString(R.string.bought_evoucher_expiry, activity) + ": " + list.get(position).getExpired_date());

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MoreOptionDialog popupMoreOption = new MoreOptionDialog(activity, false, true, false);
                popupMoreOption.show();
                popupMoreOption.setData(list.get(position).getPin(), list.get(position).getSerial(), "");

            }
        });

        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dấu # phải encode uri
                Helper.useCardSerial(activity, "*100*" + list.get(position).getPin().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLogo;
        Button btnMore;
        Button btnCall;
        TextView tvPin, tvPrice;
        TextView tvSerial;
        TextView tvExpireDay;

        public ViewHolder(View itemView) {
            super(itemView);
            imgLogo = (ImageView) itemView.findViewById(R.id.imgLogo);
            btnMore = (Button) itemView.findViewById(R.id.btnMore);
            btnCall = (Button) itemView.findViewById(R.id.btnCall);
            tvPin = (TextView) itemView.findViewById(R.id.tvPin);
            tvSerial = (TextView) itemView.findViewById(R.id.tvSerial);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            tvExpireDay = (TextView) itemView.findViewById(R.id.tvExpireDay);


        }
    }
}
