package com.opencheck.client.home.LixiHomeV25.Search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.MyTagView;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.databinding.ActivitySearchingBinding;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.apiwrapper.ApiWrapperForListModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantHotBrandModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchingActivity extends LixiActivity {

    public static final String EXTRA_CATEGORY = "EXTRA_CATEGORY";

    public static void startSearchActivity(LixiActivity activity, int categoryId) {
        Intent intent = new Intent(activity, SearchingActivity.class);
        intent.putExtra(EXTRA_CATEGORY, categoryId);
        activity.startActivity(intent);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_SEARCH;
    }

    private RelativeLayout rl_top;
    private ImageView iv_back;
    private TextView tv_search;
    private LinearLayout ll_default;
    private RecyclerView rv_merchant;
    private LinearLayout ll_history;
    private LinearLayout ll_list_history;
    private LinearLayout ll_top_key;
    private LinearLayout ll_list_keyword;
    private RecyclerView rv;

    public MyTagView listHotKeyWord;
    public MyTagView listHistoryWord;
    public LogoMerchantAdapter adapterMerchant;

    public ArrayList<String> listKeyword;
    public ArrayList<String> listHistory;

    public AdapterSearchResult adapter;
    private String keyword = "";

    public int categoryId;

    private ActivitySearchingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_searching);
        categoryId = getIntent().getIntExtra(EXTRA_CATEGORY, 0);
        findViews();
        initView();
        startLoadDefaultData();
    }

    private void findViews() {
        rl_top = (RelativeLayout) findViewById(R.id.rl_top);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_search = (TextView) findViewById(R.id.tv_search);
        ll_default = (LinearLayout) findViewById(R.id.ll_default);
        rv_merchant = (RecyclerView) findViewById(R.id.rv_merchant);
        ll_history = (LinearLayout) findViewById(R.id.ll_history);
        ll_list_history = (LinearLayout) findViewById(R.id.ll_list_history);
        ll_top_key = (LinearLayout) findViewById(R.id.ll_top_key);
        ll_list_keyword = findViewById(R.id.ll_list_keyword);
        rv = (RecyclerView) findViewById(R.id.rv);

        tv_search.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    public void initView() {
        rv_merchant.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        adapterMerchant = new LogoMerchantAdapter(activity, null);
        rv_merchant.setAdapter(adapterMerchant);

        rv.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new AdapterSearchResult(activity, null);
        rv.setAdapter(adapter);

        mBinding.searchBar.setOnSearchEventListener(new SearchBar.OnSearchEventListener() {
            @Override
            public void onTextChange(@NonNull String text) {
                if (text.equals("")) {
                    ll_default.setVisibility(View.VISIBLE);
                    setListHistory();
                    rv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onComplete(@NonNull String text) {
                search(text);
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if (!isLoading && !noRemain && isRvBottom()) {
                    if (adapter == null)
                        return;

                    if (!isLoading) {
                        getVoucher();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_search:
                search(mBinding.searchBar.getText());
                break;
            case R.id.iv_back:
                hideKeyboard();
                onBackPressed();
                break;
        }
    }

    private void search(String text) {
        page = 1;
        ll_default.setVisibility(View.GONE);
        rv.setVisibility(View.VISIBLE);
        keyword = text;
        startSearch();
        storeKeywordHistory();
        hideKeyboard();
    }

    public void storeKeywordHistory() {
        keyword = keyword.trim();
        if (keyword.length() == 0)
            return;
        String history = (new AppPreferenceHelper(activity)).getHistorySearch();
        String[] listKey = history.split("\\|\\|");

        listHistory = new ArrayList<String>(Arrays.asList(listKey));
        for (int i = 0; i < listHistory.size(); i++)
            if (listHistory.get(i).equals(keyword))
                listHistory.remove(i);

        if (listHistory.size() == 10) {
            String str = keyword;
            for (int i = 0; i < 9; i++)
                str += "||" + listHistory.get(i);
            (new AppPreferenceHelper(activity)).setHistorySearch(str);
        } else {
            String str = keyword;
            for (int i = 0; i < listHistory.size(); i++)
                str += "||" + listHistory.get(i);
            (new AppPreferenceHelper(activity)).setHistorySearch(str);
        }
    }

    //region Set View Data
    public void setListHistory() {
        ll_list_history.removeAllViews();

        String history = (new AppPreferenceHelper(activity)).getHistorySearch();
        String[] listKey = history.split("\\|\\|");
        listHistory = new ArrayList<String>(Arrays.asList(listKey));
        if (listHistory.size() == 1 && listHistory.get(0).length() == 0)
            listHistory.remove(0);
//        listHistory.add("hai từ");
//        listHistory.add("ba từ nè");
//        listHistory.add("cáinày có từrấtlàdài");
//        listHistory.add("cái này bốn từ");
//        listHistory.add("cái này bốn từ");
//        listHistory.add("cái này bốn từ");
//        listHistory.add("cái này bốn từ");
//        listHistory.add("cáinày có từrấtlàdài");
//        listHistory.add("cáinày có từrấtlàdài");
//        listHistory.add("cái này bốn từ");

        listHistoryWord = new MyTagView(activity, ll_list_history, listHistory);
        listHistoryWord.setEventListener(new MyTagView.IMytagViewListener() {
            @Override
            public void onClick(TextView tv) {
                searchTag(tv.getText().toString());
            }
        });
    }

    public void setListKeyword() {
        ll_list_keyword.removeAllViews();
        if (listKeyword == null)
            return;

//        listKeyword.add("hai từ");
//        listKeyword.add("ba từ nè");
//        listKeyword.add("cáinày có từrấtlàdài");
//        listKeyword.add("cái này bốn từ");
//        listKeyword.add("cái này bốn từ");
//        listKeyword.add("cái này bốn từ");
//        listKeyword.add("cái này bốn từ");
//        listKeyword.add("cáinày có từrấtlàdài");
//        listKeyword.add("cáinày có từrấtlàdài");
//        listKeyword.add("cái này bốn từ");

        listHotKeyWord = new MyTagView(activity, ll_list_keyword, listKeyword);
        listHotKeyWord.setEventListener(new MyTagView.IMytagViewListener() {
            @Override
            public void onClick(TextView tv) {
                searchTag(tv.getText().toString());
            }
        });
    }

    private void searchTag(String tag) {
        mBinding.searchBar.setText(tag);
        ll_default.setVisibility(View.GONE);
        rv.setVisibility(View.VISIBLE);
        keyword = tag;
        startSearch();
        hideKeyboard();
        storeKeywordHistory();
    }
    //endregion

    //region LOAD DATA
    int page = 1;
    int pageSize = 20;
    boolean isLoading;
    boolean noRemain = false;

    public void resetList() {
        if (adapter == null || adapter.getListVoucher() == null) return;
        adapter.getListVoucher().clear();
        adapter.notifyDataSetChanged();
    }

    private boolean isRvBottom() {
        if (adapter.getListVoucher() == null || adapter.getListVoucher().size() == 0)
            return false;
        int totalItemCount = rv.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && adapter.getListVoucher().size() >= pageSize);
//                && totalItemCount >= lixiHomeAdapter.getList_voucher().size());
    }

    private void startLoadDefaultData() {
        getKeyWordHot();
        getMerchantHot();
        listHistory = new ArrayList<String>();
        setListHistory();
    }

    private void startSearch() {
        page = 1;
        isLoading = false;
        noRemain = false;
        getMerchant();
        if (!isLoading) {
            getVoucher();
        }
    }

    private void getKeyWordHot() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<String>> call = DataLoader.apiService._getKeywordhot();

        call.enqueue(new Callback<ApiWrapperForListModel<String>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<String>> call,
                                   Response<ApiWrapperForListModel<String>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<String> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        listKeyword = result.getRecords();
//                        (new Handler()).postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                setListKeyword();
//                            }
//                        }, 2000);
                        setListKeyword();
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<String>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMerchantHot() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<MerchantHotBrandModel>> call = DataLoader.apiService._getMerchantHotBranch(1, 10);

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantHotBrandModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantHotBrandModel>> call,
                                   Response<ApiWrapperForListModel<MerchantHotBrandModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantHotBrandModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        adapterMerchant.setListMerchant(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantHotBrandModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getMerchant() {
        DataLoader.setupAPICall(activity, null);

        Call<ApiWrapperForListModel<MerchantModel>> call = DataLoader.apiService._getSearchCategoryMerchant(1, 20, keyword);
        if (categoryId > 0)
            call = DataLoader.apiService._getCategoryMerchant(1, 20, categoryId, keyword);

        call.enqueue(new Callback<ApiWrapperForListModel<MerchantModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<MerchantModel>> call,
                                   Response<ApiWrapperForListModel<MerchantModel>> response) {
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<MerchantModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        adapter.setListMerchant(result.getRecords());
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<MerchantModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getVoucher() {
        DataLoader.setupAPICall(activity, null);

        isLoading = true;
        Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call = DataLoader.apiService._getSearchCategoryVoucher(page, pageSize, keyword);
        if (categoryId > 0)
            call = DataLoader.apiService._getCategoryVoucher(1, 20, categoryId, keyword);

        call.enqueue(new Callback<ApiWrapperForListModel<LixiShopEvoucherModel>>() {
            @Override
            public void onResponse(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call,
                                   Response<ApiWrapperForListModel<LixiShopEvoucherModel>> response) {
                isLoading = false;
                if (response.isSuccessful()) {
                    ApiWrapperForListModel<LixiShopEvoucherModel> result = response.body();
                    String status = result.get_meta().getStatus();
                    if (status.equals(ConstantValue.SUCCESS)) {
                        ArrayList<LixiShopEvoucherModel> list = result.getRecords();
                        if (page == 1) {
                            resetList();
                            if (list == null || list.size() == 0) {
                                //TODO show no result
                                GlobalTracking.trackEventSearch(TrackingConstant.ContentType.EVOUCHER, keyword, false);
                                return;
                            }
                            GlobalTracking.trackEventSearch(TrackingConstant.ContentType.EVOUCHER, keyword, true);
                            if (list.size() < pageSize)
                                noRemain = true;
                        } else {
                            if (list == null || list.size() == 0 || list.size() < pageSize)
                                noRemain = true;
                        }
                        adapter.setListVoucher(result.getRecords());
                        page++;
                    } else {
                        Toast.makeText(activity, "load errr", Toast.LENGTH_SHORT).show();
                        Helper.showErrorDialog(activity, "API CALL ERROR!");
                        GlobalTracking.trackEventSearch(TrackingConstant.ContentType.EVOUCHER, keyword, false);
                    }
                } else {
//                    handleUnsuccessfullRespond(activity, _callBack, response.errorBody(), response.message());
                    Toast.makeText(activity, "load reponese fail", Toast.LENGTH_SHORT).show();
                    GlobalTracking.trackEventSearch(TrackingConstant.ContentType.EVOUCHER, keyword, false);
                }
            }

            @Override
            public void onFailure(Call<ApiWrapperForListModel<LixiShopEvoucherModel>> call, Throwable t) {
//                handleUnexpectedCallError(activity, t);
                Toast.makeText(activity, "ex", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    //endregion
}