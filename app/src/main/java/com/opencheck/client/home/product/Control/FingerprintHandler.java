package com.opencheck.client.home.product.Control;

import android.content.Context;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;

import com.opencheck.client.home.product.communicate.OnCheckFingerPrintListener;

public class FingerprintHandler extends FingerprintManagerCompat.AuthenticationCallback {

    private CancellationSignal cancellationSignal;
    private Context context;
    private OnCheckFingerPrintListener onCheckFingerPrintListener;


    public FingerprintHandler(Context mContext) {
        context = mContext;
    }

    public void startAuth(FingerprintManagerCompat manager, FingerprintManagerCompat.CryptoObject cryptoObject, OnCheckFingerPrintListener onCheckFingerPrintListener) {
        cancellationSignal = new CancellationSignal();
        manager.authenticate(cryptoObject, 0, cancellationSignal, this, null);
        this.onCheckFingerPrintListener = onCheckFingerPrintListener;
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        super.onAuthenticationError(errMsgId, errString);
        onCheckFingerPrintListener.onError(errMsgId, errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        super.onAuthenticationHelp(helpMsgId, helpString);
        onCheckFingerPrintListener.onHelp(helpMsgId, helpString);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        onCheckFingerPrintListener.onSucceeded(result);
    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        onCheckFingerPrintListener.onFailed();
    }
}
