package com.opencheck.client.home.delivery.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityMapBinding;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.utils.Helper;

import java.util.List;
import java.util.Locale;

public class MapActivity extends LixiActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapLoadedCallback,
        LocationListener {

    private GoogleMap mMap;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private final static int LOCATION_PERMISSION_REQUEST = 1001;

    private Location mLastLocation;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LatLng mStartLatLng;


    private final static int UPDATE_INTERVAL = 1000;
    private final static int FATEST_INTERVAL = 1000;
    private final static int DISPLACEMENT = 10;

    private ActivityMapBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_map);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestRuntimePermission();
        } else {
            if (checkPlayService()) {
                buildGoogleApiClient();
                createLocationRequest();
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fr_map);
        mapFragment.getMapAsync(this);

        initView();
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    private void initView() {
        setMargins(mBinding.ivMarker, 0, activity.heightScreen / 2 + (int) activity.getResources().getDimension(R.dimen.value_8) - (mBinding.ivMarker.getLayoutParams().height + mBinding.ivMarker.getLayoutParams().height / 2), 0, 0);

        mBinding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.tvAddress.getText().toString().trim().length() > 0) {
                    Intent intentCallback = new Intent();
                    Bundle data = new Bundle();
                    data.putSerializable("LOCATION", new DeliAddressModel(fullAddress, namePlace, mStartLatLng.latitude, mStartLatLng.longitude, true));
                    intentCallback.putExtras(data);
                    setResult(RESULT_OK, intentCallback);
                    finish();
                } else
                    Helper.showErrorToast(activity, "Vị trí không khả dụng, xin vui lòng chọn địa điểm khác!");
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d("mapi", "This device is not support");
                finish();
            }
            return false;
        }
        return true;
    }

    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        }, LOCATION_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayService()) {
                        buildGoogleApiClient();
                        createLocationRequest();
                        displayLocation();
                    }
                }
                break;
        }
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestRuntimePermission();
            mStartLatLng = new LatLng(DeliDataLoader.shipping_receive_lat, DeliDataLoader.shipping_receive_lng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mStartLatLng, 17));
            fullAddress = getAddressFromLocation(mStartLatLng.latitude, mStartLatLng.longitude);
            mBinding.tvAddress.setText(fullAddress);
        } else {
            mMap.setMyLocationEnabled(true);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                mStartLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                mStartLatLng = new LatLng(DeliDataLoader.shipping_receive_lat, DeliDataLoader.shipping_receive_lng);
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mStartLatLng, 17));
            fullAddress = getAddressFromLocation(mStartLatLng.latitude, mStartLatLng.longitude);
            mBinding.tvAddress.setText(fullAddress);
        }
    }

    private String namePlace = "";
    private String fullAddress = "";

    private String getAddressFromLocation(double lat, double lng) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));
                }
                if (returnedAddress.getSubThoroughfare() == null || returnedAddress.getThoroughfare() == null)
                    namePlace = returnedAddress.getFeatureName();
                else
                    namePlace = returnedAddress.getSubThoroughfare() + " " + returnedAddress.getThoroughfare();
                strAdd = strReturnedAddress.toString();
                Log.d("mapi", strReturnedAddress.toString());
            } else {
                Log.d("mapi", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("mapi", "Cannot get Address!");
        }
        return strAdd;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private void startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        else mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        displayLocation();

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.d("mapi", "The camera has stopped moving.");
                mStartLatLng = mMap.getCameraPosition().target;
                fullAddress = getAddressFromLocation(mStartLatLng.latitude, mStartLatLng.longitude);
                mBinding.tvAddress.setText(fullAddress);

            }
        });

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    Log.d("mapi", "The user gestured on the map.");
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_API_ANIMATION) {
                    Log.d("mapi", "The user tapped something on the map.");
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_DEVELOPER_ANIMATION) {
                    Log.d("mapi", "The app moved the camera.");
                }
            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                Log.d("mapi", "The camera is moving.");
            }
        });

        mMap.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
            @Override
            public void onCameraMoveCanceled() {
                Log.d("mapi", "Camera movement canceled.");
            }
        });

        mMap.setOnMapLoadedCallback(this);

        //mMap.animateCamera(CameraUpdateFactory.zoomTo(1));

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayService();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onMapLoaded() {
        mStartLatLng = mMap.getCameraPosition().target;
    }

//    @Override
//    public void onCameraMove() {
////        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
////        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
////            @Override
////            public void onCameraIdle() {
////                LatLng center = mMap.getCameraPosition().target;
////                mStartLatLng = center;
////                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 15));
////                fullAddress = getAddressFromLocation(center.latitude, center.longitude);
////                tv_address.setText(fullAddress);
////            }
////        });
//    }

//    @Override
//    public void onCameraMoveStarted(int i) {
//
//    }
//
//    @Override
//    public void onCameraMove() {
//        LatLng location = mMap.getCameraPosition().target;
//        // MarkerOptions marker = new MarkerOptions().position(location).title("");
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16));
////        if(mMarker == null){
////            mMarker = mMap.addMarker(new MarkerOptions().position(location)
////                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_deliv_location)));
////            mMarker.showInfoWindow();
////        }else {
////            mMarker.setPosition(location);
////        }
//
//        mLastLocation.setLatitude(location.latitude);
//        mLastLocation.setLatitude(location.longitude);
//        Double[] lat_lng = new Double[] {location.latitude, location.longitude};
//        new ReverseGeocodingTask().execute(lat_lng);
//    }

}
