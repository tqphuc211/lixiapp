package com.opencheck.client.home.account;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivitySelectAvartarBinding;
import com.opencheck.client.utils.ConstantValue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by vutha_000 on 3/22/2018.
 */

public class SelectAvatarActivity extends LixiActivity {

    private GridView gridView;
    private RelativeLayout rl_back;
    private ArrayList<HashMap<String, String>> albumList = new ArrayList<HashMap<String, String>>();
    private AlbumAdapter albumAdapter;
    private ImageView iv;
    private TextView tv_done;
    private LoadAlbumTask loadAlbumTask;

    private ActivitySelectAvartarBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_avartar);
        initViews();
    }

    private void initViews() {
        gridView = (GridView) findViewById(R.id.gv_gallery);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        iv = (ImageView) findViewById(R.id.iv);
        tv_done = (TextView) findViewById(R.id.tv_done);

        tv_done.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        setColumnWidth();
        loadAlbumTask = new LoadAlbumTask();
        loadAlbumTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ConstantValue.image.size() > 0) {
            tv_done.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            //decodeImage(ConstantValue.image.get(0).path);
            ImageLoader.getInstance().displayImage("file://" + ConstantValue.image.get(0).path, iv, LixiApplication.getInstance().optionsNomal);
        } else {
            tv_done.setVisibility(View.GONE);
            iv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == rl_back)
            activity.onBackPressed();
        else if (view == tv_done) {
            activity.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        loadAlbumTask.cancel(true);
    }

    private void setColumnWidth() {
        int iDisplayWidth = getResources().getDisplayMetrics().widthPixels;
        Resources resources = getApplicationContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = iDisplayWidth / (metrics.densityDpi / 160f);

        if (dp < 360) {
            dp = (dp - 17) / 2;
            float px = convertDpToPixel(dp, getApplicationContext());
            gridView.setColumnWidth(Math.round(px));
        }
    }

    private float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    private class LoadAlbumTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            albumList.clear();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String path = "";
            String album = "";
            String timestamp;
            String countPhoto = "";

            Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String[] projection = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED};

            Cursor cursor = getContentResolver().query(uri, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name",
                    null, null);

            while (cursor.moveToNext()) {
                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
                album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED));
                countPhoto = getAmountPhoto(activity, album);
                albumList.add(mappingInbox(album, path, timestamp, convertToTime(timestamp), countPhoto));
            }
            cursor.close();
            Collections.sort(albumList, new AlbumComparator(ConstantValue.KEY_TIMESTAMP, "dsc"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AlbumAdapter albumAdapter = new AlbumAdapter(activity, albumList);
                    gridView.setAdapter(albumAdapter);
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(activity, ChooseImageActivity.class);
                            intent.putExtra(ConstantValue.KEY_ALBUM, albumList.get(position).get(ConstantValue.KEY_ALBUM));
                            startActivity(intent);
                        }
                    });
                }
            }, 500);

        }

        private String getAmountPhoto(Context c, String album_name) {
            Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String[] projection = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED};
            Cursor cursor = c.getContentResolver().query(uri, projection, "bucket_display_name = \"" + album_name + "\"", null, null);

            return cursor.getCount() + " Photos";
        }

        private HashMap<String, String> mappingInbox(String album, String path, String timestamp, String time, String count) {
            HashMap<String, String> map = new HashMap<>();
            map.put(ConstantValue.KEY_ALBUM, album);
            map.put(ConstantValue.KEY_PATH, path);
            map.put(ConstantValue.KEY_TIMESTAMP, timestamp);
            map.put(ConstantValue.KEY_TIME, time);
            map.put(ConstantValue.KEY_COUNT, count);
            return map;
        }

        private String convertToTime(String timestamp) {
            if (timestamp == null || timestamp.equals("null")) {
                return null;
            }
            long datetime = Long.parseLong(timestamp);
            Date date = new Date(datetime);
            DateFormat formatter = new SimpleDateFormat("dd/MM HH:mm");
            return formatter.format(date);
        }
    }

    private class AlbumComparator implements Comparator<HashMap<String, String>> {

        private String key, order;

        public AlbumComparator(String key, String order) {
            this.key = key;
            this.order = order;
        }

        @Override
        public int compare(HashMap<String, String> first, HashMap<String, String> second) {
            String firstValue = first.get(key);
            String secondValue = second.get(key);
            if (this.order.toLowerCase().contentEquals("asc"))
                return firstValue.compareTo(secondValue);
            else
                return secondValue.compareTo(firstValue);
        }
    }
}
