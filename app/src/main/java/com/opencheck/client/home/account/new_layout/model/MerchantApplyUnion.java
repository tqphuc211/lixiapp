package com.opencheck.client.home.account.new_layout.model;

public class MerchantApplyUnion {

    /**
     * id : 0
     * logo : string
     * name : string
     */

    private int id;
    private String logo;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        MerchantApplyUnion mau = (MerchantApplyUnion) obj;
        return (id == mau.getId());
    }

    public boolean equals(int id){
        return this.id == id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
