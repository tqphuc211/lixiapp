package com.opencheck.client.home.LixiHomeV25;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.databinding.DiscoveryRowBannerFragmentBinding;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.merchant.BannerModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/5/2018.
 */


public class AdapterBanner extends PagerAdapter {

    LixiActivity activity;
    public ArrayList<BannerModel> list;
    LayoutInflater inflater;
    int cWith;

    public AdapterBanner(LixiActivity _activity, ArrayList<BannerModel> list, int cWith) {
        this.activity = _activity;
        this.list = list;
        this.cWith = cWith;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        DiscoveryRowBannerFragmentBinding bannerFragmentBinding =
                DiscoveryRowBannerFragmentBinding.inflate(LayoutInflater.from(
                        container.getContext()), container, false);


//        int heightWith169Aspect = cWith * 9 / 16;
//        ViewGroup.LayoutParams params = imageView.getLayoutParams();
//        params.height = heightWith169Aspect;
//        imageView.setLayoutParams(params);

        ImageLoader.getInstance().displayImage(list.get(position).getImage_medium(), bannerFragmentBinding.imgBanner, LixiApplication.getInstance().optionsNomal);

        bannerFragmentBinding.imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getLink_app().equals("")) {
                    if (list.get(position).getLink().equals("")) {
                        return;
                    } else {
                        //Uri uri =
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getLink()));
                        activity.startActivity(intent);
                        //   customFragmentActivity.showLinkInApp(list.get(position).getLink());
                    }
                } else {
                    if (list.get(position).getLink_app().contains("merchant")) {
                        MerchantModel merchantModel = new MerchantModel();
                        String id = getIdFromString(list.get(position).getLink_app());
                        merchantModel.setId(id);
                        Intent merchant = new Intent(activity, MerchantDetailActivity.class);
                        Bundle data = new Bundle();
                        data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                        data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.HOME);
                        merchant.putExtras(data);
                        activity.startActivity(merchant);
                    }
                    if (list.get(position).getLink_app().contains("voucher")) {
                        long id = Long.parseLong(getIdFromString(list.get(position).getLink_app()));
                        ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, id, TrackingConstant.ContentSource.HOME);
//                        Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                        intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, id);
//                        activity.startActivity(intent);
                    }

                }
            }
        });

        container.addView(bannerFragmentBinding.getRoot());

        return bannerFragmentBinding.getRoot();
    }

    private String getIdFromString(String link_app) {
        String id = "0";
        int i = -1;
        int j = -1;
        for (i = 0; i < link_app.length(); i++) {
            if (link_app.charAt(i) == '=') {
                for (j = i; j < link_app.length(); j++) {
                    if (link_app.charAt(j) == '&') {
                        break;
                    }
                }
                break;
            }
        }
        if (j != -1) {
            id = link_app.substring(i + 1, j);
        }
        return id;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setItem(ArrayList<BannerModel> _list) {
        list = _list;
        notifyDataSetChanged();
    }

}
