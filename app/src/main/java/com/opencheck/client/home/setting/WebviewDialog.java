package com.opencheck.client.home.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiWebviewDialogBinding;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class WebviewDialog extends LixiDialog {
    public WebviewDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    //region Layout Variable
    private RelativeLayout rlRoot;
    private RelativeLayout rl_close;
    private RelativeLayout rl_refresh;
    private WebView webView;
    private TextView tv_title;
    private ProgressBar progressBar;
    //endregion

    //region Logic Variable

    private boolean isChecked = false;
    private String url;

    private String title = "";
    private String currentUrl = "";
    //endregion


    //region Processing UI

    private LixiWebviewDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiWebviewDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());

        findViews();
        initViews();
    }

    private void initViews() {

    }

    public void setData(String link, String title) {
        url = link;
        currentUrl = link;
        this.title = title;
        setViewData();
    }

    private void findViews() {
        rlRoot = (RelativeLayout) findViewById(R.id.rlRoot);
        rl_close = (RelativeLayout) findViewById(R.id.rl_close);
        rl_refresh = (RelativeLayout) findViewById(R.id.rl_refresh);
        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tv_title = (TextView) findViewById(R.id.tv_title);

        rl_close.setOnClickListener(this);
        rl_refresh.setOnClickListener(this);
    }

    //endregion

    //region Handle UI Logic
    private void setViewData() {
        tv_title.setText(title);

        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setDomStorageEnabled(true);

        String mime = "text/html";
        String encoding = "utf-8";
//        String testHtml = "<p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><img src=\"http://picture.lixiapp.com/oc_web_editor/2017/10/10/b6da4462049113d350111ee1018b149051639d33.jpeg\" class=\"pull-left\" style=\"width: 100%;\"><span style=\"font-weight: 700\">Ngày phát hành: 19/9/2017</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Hạn order đợt 1: 25-27/8</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">Tình trạng: ORDER 7-15 NGÀY SAU PHÁT HÀNH LÀ CÓ HÀNG</span></span><span style=\"font-weight: 700\"></span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Giá trên là giá nhận trực tiếp tại HCM, Ship sẽ tính lúc các bạn order</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Đợt 1 mới có poster và có khả năng trúng special poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Sau đợt 1 shop vẫn tiếp tục nhận order nhưng ko chắc chắn là còn poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Trọn bộ bao gồm:</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- 4 version: L.O.V.E (được chọn)- 1 CD + Photobook 100p- Mini Book (HYYH The Notes)- Random Photocard (có 28 mẫu tất cả)- Sticker Pack- Special Photocard (firstpress có khả năng trúng)<span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">- Quà firstpress (đợt 1): Poster</span></p>";
//        payment_url = "https://sandbox.payoo.com.vn/bank-simulate.php?vpc_AdditionData=990313&vpc_Amount=60000&vpc_Command=PAY&vpc_CurrencyCode=VND&vpc_Locale=VN&vpc_MerchTxnRef=PY7327000031&vpc_Merchant=OPPREPAID&vpc_OrderInfo=PY7327000031&vpc_TransactionNo=133088&vpc_Version=2&vpc_SecureHash=AAF5D28BAFD10023EE68DB9CC2E5868E4AD89DC4C9966EBD9B91C6625FFDD0CF&system=0&vpc_ReturnURL=https%3a%2f%2fnewsandbox.payoo.com.vn%2fv2%2fbank%2ffinish%3fiframe%3d0%26_token%3d89aa17ffdca0e9e04a5077f2b0a6a9ae%26bank%3dOCB";
//        webView.loadUrl(payment_url);
//        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(activity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                Toast.makeText(activity, "Http error " + errorResponse, Toast.LENGTH_LONG).show();
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                Toast.makeText(activity, "SSL error " + error , Toast.LENGTH_LONG).show();
//                super.onReceivedSslError(view, handler, error);
//                handler.proceed();
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        dismiss();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Here put your code
                currentUrl = url;
                Log.d("murl", url);
                return false; //allow loading (true orride it)
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl(url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_close:
                dismiss();
                break;
            case R.id.rl_refresh:
                webView.loadUrl(currentUrl);
                break;
            default:
                break;
        }
    }
    //endregion

    //region dialog callback

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onOk(boolean success);
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }

    //endregion
}
