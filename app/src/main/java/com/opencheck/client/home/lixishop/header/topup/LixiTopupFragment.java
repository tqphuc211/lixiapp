package com.opencheck.client.home.lixishop.header.topup;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.TopupLayoutBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.lixishop.header.ConfirmPaymentDialog;
import com.opencheck.client.home.lixishop.header.PaymentCardValueAdapter;
import com.opencheck.client.models.lixishop.PaymentModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.models.merchant.CardValueModel;
import com.opencheck.client.models.merchant.PaymentPartnerModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class LixiTopupFragment extends LixiFragment {

    public static final String[] viettel = {"086", "096", "097", "098", "0162", "0163", "0164", "0165", "0166", "0167", "0168", "0169"};
    public static final String[] mobile = {"090", "093", "0120", "0121", "0122", "0126", "0128"};
    public static final String[] vina = {"091", "094", "0123", "0124", "0125", "0127", "0129"};
    public static final String[] vnmobile = {"092", "0188", "0186"};
    public static final String[] gmobile = {"099", "0199"};

    private RelativeLayout rlBack;
    private TextView edInputContactName;
    private EditText edInputContactNumber;
    private RelativeLayout rlInputContactSelect;
    private RelativeLayout rlCheckPrepay;
    private ImageView imgCheckPrepay;
    private RelativeLayout rlCheckPostpaid;
    private ImageView imgCheckPostpaid;
    private TextView tvTopupInfo;
    private RecyclerView rl_card_value;
    private Button btnContinute;
    private TextView tvTitle, tvInputNumber, tvPrepay, tvPostpaid, tvSelectCount;

    private Boolean isPrepay = true;

    private ServiceCategoryModel serviceCategorySelected = new ServiceCategoryModel();
    private PaymentPartnerModel paymentPartnerSelected = new PaymentPartnerModel();
    private ArrayList<PaymentPartnerModel> listPaymentPartner = new ArrayList<PaymentPartnerModel>();
    private CardValueModel cardValuSelected = new CardValueModel();

    private ArrayList<CardValueModel> listCardValue = new ArrayList<CardValueModel>();

    private UserModel userModel = UserModel.getInstance();

    private PaymentCardValueAdapter adapterCardValue;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             final ViewGroup container, final Bundle savedInstanceState) {
        TopupLayoutBinding layoutBinding =
                TopupLayoutBinding.inflate(inflater, container, false);
        this.initView(layoutBinding.getRoot());
        initData();
        GlobalTracking.trackServiceStart(TrackingConstant.Service.TOPUP);
        getVariableStatic();
        getPaymentPartner();
        return layoutBinding.getRoot();
    }

    private void initView(View view) {
        tvSelectCount = view.findViewById(R.id.tvSelectCount);
        tvPostpaid = view.findViewById(R.id.tvPostpaid);
        tvPrepay = view.findViewById(R.id.tvPrepay);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvInputNumber = view.findViewById(R.id.tvInputNumber);
        rlBack = view.findViewById(R.id.rlBack);
        edInputContactName = view.findViewById(R.id.edInputContactName);
        edInputContactNumber = view.findViewById(R.id.edInputContactNumber);
        rlInputContactSelect = view.findViewById(R.id.rlInputContactSelect);
        rlCheckPrepay = view.findViewById(R.id.rlCheckPrepay);
        imgCheckPrepay = view.findViewById(R.id.imgCheckPrepay);
        rlCheckPostpaid = view.findViewById(R.id.rlCheckPostpaid);
        imgCheckPostpaid = view.findViewById(R.id.imgCheckPostpaid);
        tvTopupInfo = view.findViewById(R.id.tvTopupInfo);
        rl_card_value = view.findViewById(R.id.rl_card_value);
        btnContinute = view.findViewById(R.id.btnContinute);

        edInputContactName.setText(LanguageBinding.getString(R.string.not_in_contact, activity));

        GridLayoutManager mLayoutManager = new GridLayoutManager(activity, 3);
        rl_card_value.setLayoutManager(mLayoutManager);

        rlBack.setOnClickListener(this);
        btnContinute.setOnClickListener(this);
        rlInputContactSelect.setOnClickListener(this);
        rlCheckPrepay.setOnClickListener(this);
        rlCheckPostpaid.setOnClickListener(this);
        btnContinute.setOnClickListener(this);

        edInputContactNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Helper.isValidPhone(edInputContactNumber.getText().toString())) {
                    edInputContactName.setText("");
                } else {
                    if (edInputContactName.getText().toString().length() == 0)
                        edInputContactName.setText(LanguageBinding.getString(R.string.not_in_contact, activity));
                    paymentPartnerSelected = detectPaymentPartner(edInputContactNumber.getText().toString());
                    if (paymentPartnerSelected != null) {
                        edInputContactName.setText(edInputContactName.getText() + " - " + paymentPartnerSelected.getName());
                    } else {
                        edInputContactName.setText(LanguageBinding.getString(R.string.not_in_contact, activity));
                    }
                }
            }
        });
    }


    private void initData() {
        userModel = UserModel.getInstance();
        setPrepay(true);
        getCardValue();
    }

    private void setPrepay(Boolean _isPrepay) {
        this.isPrepay = _isPrepay;
        if (isPrepay) {
            imgCheckPrepay.setBackgroundResource(R.drawable.icon_radio_checked);
            imgCheckPostpaid.setBackgroundResource(R.drawable.icon_radio_uncheck);
        } else {
            imgCheckPostpaid.setBackgroundResource(R.drawable.icon_radio_checked);
            imgCheckPrepay.setBackgroundResource(R.drawable.icon_radio_uncheck);
        }
    }


    private void getVariableStatic() {
        String jsonServiceCategory;
        Bundle extras = getArguments();
        if (extras != null) {
            jsonServiceCategory = extras.getString(ConstantValue.SERVICE_CATEGORY_CHILD_MODEL);
            serviceCategorySelected = new Gson().fromJson(jsonServiceCategory, ServiceCategoryModel.class);
        } else
            serviceCategorySelected = new ServiceCategoryModel();
    }

    private Boolean checkInput() {
        if (edInputContactNumber.getText().toString().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.recharge_require_phone));
            return false;
        }

        if (!Helper.isValidPhone(edInputContactNumber.getText().toString())) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.recharge_require_phone_syntax));
            return false;
        }

        if (paymentPartnerSelected == null) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.recharge_require_telecom));
            return false;
        }

        if (cardValuSelected.getValue().equalsIgnoreCase("")) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.recharge_require_values));
            return false;
        }

        float remnant = Float.parseFloat(UserModel.getInstance().getAvailable_point()) - Float.parseFloat(cardValuSelected.getKey());

        if (remnant < 0) {
            Helper.showErrorDialog(activity, getResources().getString(R.string.recharge_require_not_enough));
            return false;
        }

        return true;
    }

    private void getCardValue() {
        DataLoader.getCardValue(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listCardValue = (ArrayList<CardValueModel>) object;
                    showCardValue();

                    cardValuSelected = listCardValue.get(0);

                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    public void showCardValue() {
        adapterCardValue = new PaymentCardValueAdapter(listCardValue, activity);
        rl_card_value.setAdapter(adapterCardValue);

        adapterCardValue.setOnClickListener(new PaymentCardValueAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                cardValuSelected = listCardValue.get(position);
            }
        });
    }

    private void getPaymentPartner() {
        DataLoader.getPaymentPartner(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listPaymentPartner = (ArrayList<PaymentPartnerModel>) object;

                    PaymentPartnerModel pn = detectPaymentPartner(userModel.getPhone());
                    edInputContactNumber.setText(userModel.getPhone());
                    edInputContactName.setText(userModel.getFullname() + (pn != null ? (" - " + pn.getName()) : ""));
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, serviceCategorySelected.getId());
    }

    private PaymentPartnerModel detectPaymentPartner(String sdt) {
        boolean found = false;
        PaymentPartnerModel rs = null;
        for (int i = 0; i < listPaymentPartner.size(); i++) {
            rs = listPaymentPartner.get(i);

            for (int j = 0; j < rs.getSuplier_prefix_phone().size(); j++) {
                if (sdt.indexOf(rs.getSuplier_prefix_phone().get(j)) == 0) {
                    if (sdt.length() - rs.getSuplier_prefix_phone().get(j).length() == 7) {
                        found = true;
                        break;
                    }
                }
            }
            if (found)
                break;
        }
        if (found)
            return rs;
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.GONE);
//            ((HomeActivity) activity).navigation.setVisibility(View.GONE);
//        tvTitle.setText(getResources().getString(R.string.recharge_title));
//        tvInputNumber.setText(getResources().getString(R.string.info_update_phone));
//        tvPrepay.setText(getResources().getString(R.string.recharge_before));
//        tvPostpaid.setText(getResources().getString(R.string.recharge_after));
//        tvSelectCount.setText(getResources().getString(R.string.buy_card_value));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == rlBack) {
            activity.onBackPressed();
        } else if (v == btnContinute) {
            if (checkInput()) {
                PaymentModel paymentModel = new PaymentModel();
                paymentModel.setContract_code(userModel.getCode());
                if (edInputContactNumber.getText().toString().indexOf(userModel.getPhone()) >= 0)
                    paymentModel.setFullname(userModel.getFullname());
                else {
                    String name = edInputContactName.getText().toString();
                    int l = name.lastIndexOf("-");
                    if (l < 0)
                        l = name.length();
                    paymentModel.setFullname(name.substring(0, l));
                }
                paymentModel.setMoney_payment(cardValuSelected.getKey());
                paymentModel.setName(serviceCategorySelected.getName());
                paymentModel.setPaymentPartnerModel(paymentPartnerSelected);
                paymentModel.setPhone(edInputContactNumber.getText().toString());
                paymentModel.setUserName(edInputContactName.getText().toString());
                paymentModel.setServiceCategoryModel(serviceCategorySelected);
                paymentModel.setQuantity("1");
                paymentModel.setTypeTopup(isPrepay ? LanguageBinding.getString(R.string.recharge_before, activity) : LanguageBinding.getString(R.string.recharge_after, activity));
                ConfirmPaymentDialog dialog = new ConfirmPaymentDialog(activity, false, true, false);
                dialog.show();
                dialog.setData(serviceCategorySelected, paymentModel, true, isPrepay);
            }
        } else if (v == rlCheckPrepay) {
            setPrepay(true);
        } else if (v == rlCheckPostpaid) {
            setPrepay(false);
        } else if (v == rlInputContactSelect) {
//            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CONTACTS)) {
//                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, ConstantValue.REQUEST_CODE_READ_CONTACTS_PERMISSION);
//                } else {
//                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, ConstantValue.REQUEST_CODE_READ_CONTACTS_PERMISSION);
//                }
//            } else {
                readContact();
//            }
        }
    }

    private void readContact() {
        try {
            Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
            pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
            activity.startActivityForResult(pickContactIntent, ConstantValue.REQUEST_CODE_READ_CONTACTS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_READ_CONTACTS:
                handleReadContact(resultCode, data);
                break;
        }
    }

    private void handleReadContact(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
            Cursor cursor = activity.getContentResolver()
                    .query(contactUri, projection, null, null, null);
            cursor.moveToFirst();

            int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String number = cursor.getString(column);
            int column1 = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String name = cursor.getString(column1);

            number = Helper.correctPhoneNumber(number);

            Helper.showLog("number: " + number);
            edInputContactName.setText(name);
            edInputContactNumber.setText(number);
        }
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.SERVICE_TOPUP;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_READ_CONTACTS_PERMISSION:
                readContact();
                break;

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
