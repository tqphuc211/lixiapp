package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class AddonGetModel implements Serializable{
    private long addon_id;
    private String value;
    private String addon_name;
    private long addon_category_id;
    private String addon_category_name;
    private String date_type;

    public long getAddon_id() {
        return addon_id;
    }

    public void setAddon_id(long addon_id) {
        this.addon_id = addon_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddon_name() {
        return addon_name;
    }

    public void setAddon_name(String addon_name) {
        this.addon_name = addon_name;
    }

    public long getAddon_category_id() {
        return addon_category_id;
    }

    public void setAddon_category_id(long addon_category_id) {
        this.addon_category_id = addon_category_id;
    }

    public String getAddon_category_name() {
        return addon_category_name;
    }

    public void setAddon_category_name(String addon_category_name) {
        this.addon_category_name = addon_category_name;
    }

    public String getDate_type() {
        return date_type;
    }

    public void setDate_type(String date_type) {
        this.date_type = date_type;
    }
}
