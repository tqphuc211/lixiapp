package com.opencheck.client.home.account.new_layout.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogCreateNewCardBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.communicate.OnCreateCardResultListener;
import com.opencheck.client.home.product.Control.OperateBase64;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.ArrayList;

public class CreateNewCardDialog extends LixiDialog {
    public CreateNewCardDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    // View
    ImageView imgBack;
    WebView webView;
    ProgressBar progressBar;

    // Var
    String payment_link = "";
    String key = "";

    private DialogCreateNewCardBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogCreateNewCardBinding.inflate(LayoutInflater
                .from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        cancel();
    }

    public void setData(String key) {
        this.key = key;

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("bank_code", key);
        jsonObject.addProperty("url_return", "");
        DataLoader.createNewCard(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    payment_link = ((JsonObject) object).get("payment_link").getAsString();
                    setViewData();
                }
            }
        }, jsonObject);
    }

    private void setViewData() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(activity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        dismiss();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                payment_link = url;

                saveBankCard(getQuery(url, "token_data"));
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl(payment_link);
    }

    private String getQuery(String url, String query) {
        String res = "";
        try {
            Uri uri = Uri.parse(url);
            res = uri.getQueryParameter(query);
            if (res == null) {
                res = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private void saveBankCard(String token) {
        if (token == null || token.equals("")) {
            if (onCreateCardResultListener != null) {
                onCreateCardResultListener.onFail();
                cancel();
            }
            return;
        }
        String decodeToken = OperateBase64.decode(token);
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
        TokenData tokenData = (new Gson()).fromJson(decodeToken, TokenData.class);

        String bankCode = tokenData.getCard().getBank_code();
        ArrayList<TokenData> listTemp = new ArrayList<>();
        listTemp = appPreferenceHelper.getTokenBankCard(bankCode);
        if (listTemp != null) {
            for (int i = 0; i < listTemp.size(); i++) {
                if (checkCardExisted(tokenData.getCard().getNumber(), listTemp.get(i).getCard().getNumber())) {
                    // thẻ đã tồn tại
                    if (onCreateCardResultListener != null) {
                        onCreateCardResultListener.onCardExisted();
                        cancel();
                    }
                    return;
                }
            }
        }
        appPreferenceHelper.setTokenBankCard(tokenData);
        if (onCreateCardResultListener != null) {
            onCreateCardResultListener.onSuccess();
        }
        cancel();
    }

    private boolean checkCardExisted(String n1, String n2) {
        return n1.equals(n2);
    }

    private OnCreateCardResultListener onCreateCardResultListener;

    public void addOnCreateCardResult(OnCreateCardResultListener onCreateCardResultListener) {
        this.onCreateCardResultListener = onCreateCardResultListener;
    }

    public void showNotifyDialog(String title, String content) {
        NotificationForCardDialog dialog = new NotificationForCardDialog(activity, false, false, false);
        dialog.show();
        dialog.setData(title, content);
        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                CreateNewCardDialog.this.cancel();
            }
        });
    }
}
