package com.opencheck.client.home.product.cart_old;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.models.lixishop.BankModel;
import com.opencheck.client.utils.Helper;

import java.util.List;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class AdapterBank extends RecyclerView.Adapter {

    LixiActivity activity;
    List<BankModel> listBank;
    private int lastPost = -1;
    BankModel currentUserBank = null;
    TextView chosenBank;
    private long price = 0;

    public AdapterBank(LixiActivity activity, List<BankModel> listBank, TextView chosenBank) {
        this.activity = activity;
        this.listBank = listBank;
        this.chosenBank = chosenBank;
    }

    public void setData(long price) {
        this.price = price;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.lixi_shop_bank_item, parent, false);

        return new HolderItem(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HolderItem viewHolder = (HolderItem) holder;
        BankModel dto = listBank.get(position);
        viewHolder.rootItemView.setTag(position);

        viewHolder.tv_name.setText(dto.getName());
        if (dto.getMin_value() > price) {
            viewHolder.tv_name.setTextColor((activity.getResources().getColor(R.color.dark_gray_light)));
        }
        if (dto.isSelected()) {
            if (dto.getMin_value() > price) {
                lastPost = -1;
                viewHolder.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                chosenBank.setText(activity.getString(R.string.popup_select_bank_unselect));
                chosenBank.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
            }
            lastPost = position;
            currentUserBank = dto;
            chosenBank.setText(dto.getName());
            chosenBank.setTextColor(activity.getResources().getColor(R.color.my_black));
            viewHolder.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_cab_done_mtrl_alpha, 0);
        } else {
            viewHolder.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        if (dto.getBank_logo() != null)
            ImageLoader.getInstance().displayImage(dto.getBank_logo(), viewHolder.iv_logo, LixiApplication.getInstance().optionsNomal);
    }

    public class HolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootItemView;
        private TextView tv_name;
        private ImageView iv_logo;
        private LinearLayout ll_bank;

        public HolderItem(View itemView) {
            super(itemView);
            rootItemView = itemView;
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
            ll_bank = (LinearLayout) itemView.findViewById(R.id.ll_bank);

            rootItemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v == rootItemView) {
                BankModel currentBank = listBank.get((int) v.getTag());
                if (currentBank.getMin_value() > price) {
                    Helper.showErrorDialog(activity, activity.getString(R.string.popup_select_bank_under_min_value)
                            + " " + Helper.getVNCurrency(currentBank.getMin_value()) + activity.getString(R.string.p));
                    return;
                }
                if (lastPost == -1) {
                    listBank.get((int) v.getTag()).setSelected(!currentBank.isSelected());
                    notifyDataSetChanged();
                } else {
                    listBank.get((int) v.getTag()).setSelected(!currentBank.isSelected());
                    if (lastPost != (int) v.getTag()) {
                        listBank.get(lastPost).setSelected(false);
                    }
                    notifyDataSetChanged();
                }
                lastPost = (int) v.getTag();
                if (listBank.get(lastPost).isSelected()) {
                    currentBank = listBank.get(lastPost);
                    chosenBank.setText(currentBank.getName());
                    chosenBank.setTextColor(activity.getResources().getColor(R.color.my_black));
                }
                else {
                    currentUserBank = null;
                    chosenBank.setTextColor(activity.getResources().getColor(R.color.dark_gray_light));
                    chosenBank.setText("Chưa chọn ngân hàng");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listBank == null)
            return 0;
        return listBank.size();
    }

    public BankModel getCurrentUserBank(){
        return currentUserBank;
    }
}