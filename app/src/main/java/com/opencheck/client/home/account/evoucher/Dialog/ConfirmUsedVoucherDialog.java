package com.opencheck.client.home.account.evoucher.Dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogConfirmUsedVoucherBinding;
import com.opencheck.client.home.account.evoucher.Interface.OnConfirmListener;

public class ConfirmUsedVoucherDialog extends LixiDialog {

    public ConfirmUsedVoucherDialog(@NonNull LixiActivity _activity) {
        super(_activity, false, true, false);
    }

    private DialogConfirmUsedVoucherBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogConfirmUsedVoucherBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());

        mBinding.txtCancel.setOnClickListener(this);
        mBinding.txtDone.setOnClickListener(this);
    }

    private OnConfirmListener onConfirmListener;
    public void setOnConfirmListener(OnConfirmListener onConfirmListener){
        this.onConfirmListener = onConfirmListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtDone:
                if (onConfirmListener != null){
                    onConfirmListener.onConfirm(true);
                }
                cancel();
                break;
            case R.id.txtCancel:
                cancel();
                break;
        }
    }
}
