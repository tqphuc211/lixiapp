package com.opencheck.client.home.delivery.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.opencheck.client.home.delivery.model.DeliAddressModel;

import java.util.LinkedList;
import java.util.List;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "AddressDB.db";
    private static final String TABLE_NAME = "Address";
    private static final String KEY_ID = "Id";
    private static final String KEY_PLACE_ID = "placeId";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_FULL_ADDRESS = "fullAddress";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LNG = "lng";
    private static final String KEY_GPS = "gps";
    private static final String KEY_TIME_STAMP = "timeStamp";
    private static final String KEY_TYPE = "type";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_LOCATION_ID = "loactionId";

    private static final String[] COLUMNS = {KEY_ID, KEY_ADDRESS, KEY_FULL_ADDRESS,
            KEY_LAT, KEY_LNG};

    private static SQLiteDatabaseHandler mInstance = null;

    private Context context;

    public SQLiteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static synchronized SQLiteDatabaseHandler getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new SQLiteDatabaseHandler(context.getApplicationContext());
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATION_TABLE = "CREATE TABLE Address ( "
                + "Id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "placeId TEXT , "
                + "address TEXT, "
                + "fullAddress TEXT, "
                + "lat REAL, "
                + "lng REAL, "
                + " gps INTEGER DEFAULT 0, "
                + " timeStamp TEXT, "
                + KEY_TYPE + " TEXT, "
                + KEY_USER_ID + " INTEGER ,"
                + KEY_LOCATION_ID + " INTEGER)";

        db.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public List<DeliAddressModel> getAllAddress() {

        List<DeliAddressModel> addresses = new LinkedList<>();
        DeliAddressModel address = null;

        try {
            String query = "SELECT * FROM " + TABLE_NAME;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            int count = 0;
            if (cursor.moveToLast()) {
                do {
                    address = new DeliAddressModel();
                    address.setPlaceId(cursor.getString(1));
                    address.setAddress(cursor.getString(2));
                    address.setFullAddress(cursor.getString(3));
                    address.setLat(Double.parseDouble(cursor.getString(4)));
                    address.setLng(Double.parseDouble(cursor.getString(5)));
                    address.setGPS((cursor.getInt(cursor.getColumnIndex("gps")) == 1));
                    address.setType(cursor.getString(8));
                    address.setUser_id(cursor.getInt(9));
                    address.setId(cursor.getInt(10));
                    addresses.add(address);
                    count++;
                } while (cursor.moveToPrevious() && count < 10);
            }

            cursor.close();
        } catch (SQLException e) {
            Log.d("VVVV", e.getMessage());
            e.printStackTrace();
        }
        return addresses;
    }

    public DeliAddressModel getLastLocationOrder(){
        // lấy row cuối
        DeliAddressModel addressModel = null;
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + KEY_ID + " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            addressModel = new DeliAddressModel();
            addressModel.setPlaceId(cursor.getString(1));
            addressModel.setAddress(cursor.getString(2));
            addressModel.setFullAddress(cursor.getString(3));
            addressModel.setLat(Double.parseDouble(cursor.getString(4)));
            addressModel.setLng(Double.parseDouble(cursor.getString(5)));
            addressModel.setGPS((cursor.getInt(cursor.getColumnIndex("gps")) == 1));
            addressModel.setCreate_date(Long.parseLong(cursor.getString(7)));
            addressModel.setType(cursor.getString(8));
            addressModel.setUser_id(cursor.getInt(9));
            addressModel.setId(cursor.getInt(10));
        }
        return addressModel;
    }

    public void addAddress(DeliAddressModel address) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_PLACE_ID, address.getPlaceId());
            values.put(KEY_ADDRESS, address.getAddress());
            values.put(KEY_FULL_ADDRESS, address.getFullAddress());
            values.put(KEY_LAT, address.getLat());
            values.put(KEY_LNG, address.getLng());
            values.put(KEY_GPS, address.isGPS());
            values.put(KEY_TIME_STAMP, address.getCreate_date() != 0 ? address.getCreate_date() + "" : System.currentTimeMillis() + "");
            values.put(KEY_TYPE, address.getType());
            values.put(KEY_USER_ID, address.getUser_id());
            values.put(KEY_LOCATION_ID, address.getId());
            // insert
            db.insertOrThrow(TABLE_NAME, null, values);
            db.close();
        } catch (SQLException e) {
            Log.d("VVVV", e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean isExist(String fullAddress) {
        String query = "SELECT " + KEY_FULL_ADDRESS + " FROM " + TABLE_NAME + " WHERE " + KEY_FULL_ADDRESS + "= '" + fullAddress + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        boolean isExist = cursor.getCount() > 0;
        cursor.close();
        return isExist;
    }

    public void deleteLastItem(){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_ID + "=10";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void deleteExpiredAddress() {
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_TIME_STAMP + "<= date('now','-3 day')";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void removeItem(String fullAddress){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_FULL_ADDRESS + "= '" + fullAddress + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
}
