package com.opencheck.client.home.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowItemMerchantBinding;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.FilterModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/6/2018.
 */

public class AdapterPlaceSearch extends RecyclerView.Adapter<AdapterPlaceSearch.ViewHolder> {

    private List<MerchantModel> listPlace;
    private LixiActivity activity;
    private FilterModel hashTags;
    private EditText edt_search;

    public AdapterPlaceSearch(LixiActivity activity, List<MerchantModel> listPlace, FilterModel filterSearchModel, EditText search) {
        this.activity = activity;
        this.listPlace = listPlace;
        this.hashTags = filterSearchModel;
        this.edt_search = search;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowItemMerchantBinding rowItemMerchantBinding =
                RowItemMerchantBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new AdapterPlaceSearch.ViewHolder(rowItemMerchantBinding.getRoot());

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
//        if (listPlace.get(position).getProduct_cover_image() == null || listPlace.get(position).getProduct_cover_image().equals(""))
        if (listPlace.get(position).getImage_list() != null && listPlace.get(position).getImage_list().size() > 0)
            ImageLoader.getInstance().displayImage(listPlace.get(position).getImage_list().get(0).getImage(),
                    holder.iv_hinh, LixiApplication.getInstance().optionsNomal);
//        else
//            ImageLoader.getInstance().displayImage(listPlace.get(position).getProduct_cover_image(),
//                    holder.iv_hinh, LixiApplication.getInstance().optionsNomal);

        holder.tv_name.setText(listPlace.get(position).getName());

        String listTagString = "";
        ArrayList<Integer> listOnClick = new ArrayList<Integer>();
        for (int i = 0; i < listPlace.get(position).getTags().size(); i++) {
            if (i == 0) {
                listTagString = "#" + listPlace.get(position).getTags().get(i).getName();
            } else {
                listTagString = listTagString + "  #" + listPlace.get(position).getTags().get(i).getName();
            }

            listOnClick.add(listTagString.length());
        }


        SpannableString ss = new SpannableString(listTagString);
        int index = 0;
        for (int i = 0; i < listOnClick.size(); i++) {
            ss.setSpan(new OnClickTagController(activity, i, new ItemClickListener() {
                @Override
                public void onItemCLick(int pos, String action, Object object, Long count) {
                    edt_search.setText("#" + listPlace.get(position).getTags().get(pos).getName());
                }
            }), index, listOnClick.get(i), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            index = listOnClick.get(i);
        }

        holder.tv_tag.setText(ss);
        holder.tv_tag.setMovementMethod(LinkMovementMethod.getInstance());

        holder.ll_merchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MerchantModel merchantModel = new MerchantModel();
                merchantModel.setId(String.valueOf(listPlace.get(position).getId()));
                Intent intent = new Intent(activity, MerchantDetailActivity.class);
                Bundle data = new Bundle();
                data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.SEARCH);
                intent.putExtras(data);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listPlace.size() > 3)
            return 3;
        else
            return listPlace.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_hinh;
        private TextView tv_name;
        private TextView tv_tag;
        private LinearLayout ll_merchant;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_hinh = itemView.findViewById(R.id.iv_promotion);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_tag = itemView.findViewById(R.id.tv_tag);
            ll_merchant = itemView.findViewById(R.id.ll_merchant);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
