package com.opencheck.client.home.delivery.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.transition.Slide;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.databinding.ActivityFavoriteStoreBinding;
import com.opencheck.client.databinding.ActivityPromotionCodeUserGroupBinding;
import com.opencheck.client.databinding.FragmentStoreBottomSheetDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.FavoriteStoreAdapter;
import com.opencheck.client.home.delivery.adapter.PlaceSmallAdapter;
import com.opencheck.client.home.delivery.adapter.PromotionListUserGroupAdapter;
import com.opencheck.client.home.delivery.fragments.DeliveryHomeFragment;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.refactor.LocationSelectionFragment;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class FavoriteStoreActivity extends LixiActivity implements SwipeRefreshLayout.OnRefreshListener {
    private FavoriteStoreAdapter mAdapter;
    private ActivityFavoriteStoreBinding mBinding;

    private DeliAddressModel mLocation;

    private int page = 1;
    private boolean isLoading = false;
    private boolean noRemain = false;

    public static void startFavoriteStoreActivity(LixiActivity context) {
        Intent intent = new Intent(context, FavoriteStoreActivity.class);
        context.startActivityForResult(intent, ConstantValue.REQUEST_FAVORITE_STORE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_favorite_store);
        mAdapter = new FavoriteStoreAdapter(activity, null, new FavoriteStoreAdapter.ClickListener() {
            @Override
            public void onClick(StoreOfCategoryModel store, int pos) {
                StoreDetailActivity.startStoreDetailActivity(activity, store.getId(), null);
            }

            @Override
            public void onDeleteItem(StoreOfCategoryModel store, int pos) {
                deleteFavoriteStore(store.getId());
                mAdapter.deleteStore(pos);
//                mBinding.rvFavoriteStore.getLayoutManager().removeViewAt(pos);
                mAdapter.notifyItemRemoved(pos);
                if(mAdapter.getListStore().size() == 0) {
                    mBinding.lnResult.setVisibility(View.GONE);
                    mBinding.lnNoResult.setVisibility(View.VISIBLE);
                }
            }
        });
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        initView();
    }



    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
        mBinding.swr.setOnRefreshListener(this);
        mBinding.swr.setRefreshing(true);
    }

    private void initView() {
        mBinding.rvFavoriteStore.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mBinding.rvFavoriteStore.setAdapter(mAdapter);
        mBinding.rvFavoriteStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading && !noRemain && isRvBottom()) {
                    Log.d("mapi", "load more");
                    getFavoriteStore();
                    isLoading = true;
                }
            }
        });
        mBinding.ivBack.setOnClickListener(this);
    }

    private void getFavoriteStore() {
        if(!isLoading) {
            mBinding.swr.setRefreshing(true);
            DeliDataLoader.getFavoriteStore(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    mBinding.swr.setRefreshing(false);
                    if(isSuccess) {
                        ArrayList<StoreOfCategoryModel> listItem = (ArrayList<StoreOfCategoryModel>) object;
                        Helper.showLog("huhuhu:" + new Gson().toJson(object));
                        if (page == 1) {
                            resetList();
                            if (listItem == null || listItem.size() == 0) {
                                mBinding.lnResult.setVisibility(View.GONE);
                                mBinding.lnNoResult.setVisibility(View.VISIBLE);
                                return;
                            }
                            if (listItem.size() < DeliDataLoader.record_per_page)
                                noRemain = true;
                        } else {
                            if (listItem == null || listItem.size() == 0 || listItem.size() < DeliDataLoader.record_per_page) {
                                if(listItem.size() < DeliDataLoader.record_per_page) {
                                    mAdapter.setListStore(listItem);
                                }
                                noRemain = true;
                                return;
                            }
                        }
                        mAdapter.setListStore(listItem);
                        isLoading = false;
                        page++;
                    } else {
                        isLoading = true;
                    }
                }
            }, page, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        }
    }

    private void deleteFavoriteStore(long storeId) {
        DeliDataLoader.deleteFavoriteStore(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                } else {
                }
            }
        }, storeId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                hideKeyboard();
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    private void loadData() {
        isLoading = false;
        noRemain = false;
        page = 1;

        getFavoriteStore();
    }

    private void resetList() {
        mAdapter.getListStore().clear();
    }

    private boolean isRvBottom() {
        if (mAdapter.getListStore() == null || mAdapter.getListStore().size() == 0)
            return false;
        int totalItemCount = mBinding.rvFavoriteStore.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) mBinding.rvFavoriteStore.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        return (lastVisibleItem >= totalItemCount - 7
                && mAdapter.getListStore().size() >= DeliDataLoader.record_per_page);
    }

    @Override
    public void onRefresh() {
        mBinding.lnResult.setVisibility(View.VISIBLE);
        mBinding.lnNoResult.setVisibility(View.GONE);
        loadData();
    }
}
