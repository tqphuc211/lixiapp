package com.opencheck.client.home.flashsale.Model;

public class RemindFlashSale {
    public static final String HEADER_REMIND = "REMIND";

    private long idFlashSale;
    private long idProduct;
    private long idRemind;
    private long timer;

    public long getIdFlashSale() {
        return idFlashSale;
    }

    public void setIdFlashSale(long idFlashSale) {
        this.idFlashSale = idFlashSale;
    }

    public long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(long idProduct) {
        this.idProduct = idProduct;
    }

    public long getIdRemind() {
        return idRemind;
    }

    public void setIdRemind(long idRemind) {
        this.idRemind = idRemind;
    }

    public long getTimer() {
        return timer;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }
}
