package com.opencheck.client.home.delivery.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FragmentDetailInfoBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.adapter.ImageDetailAdapter;
import com.opencheck.client.home.delivery.adapter.UserFeedbackAdapter;
import com.opencheck.client.home.delivery.model.ScoreFeedbackModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.home.delivery.model.UserFeedbackModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailInfoFragment extends LixiFragment implements StoreDetailActivity.OnTransferDataToSecondTabEvent {

    private RecyclerView rcv_menu;
    private ArrayList<String> arrImage = new ArrayList<>();
    private ImageDetailAdapter adapter;
    private TextView tv_time, tv_price, tv_address, tv_phone, tv_image;
    private StoreOfCategoryModel store;
    private UserFeedbackAdapter userFeedbackAdapter;
    private ArrayList<UserFeedbackModel> listUserFeedback = new ArrayList<>();
    private int page = 1;
    private int pageSize = DeliDataLoader.record_per_page;
    private boolean isLoading = false;
    private boolean noRemain = false;
    private boolean shouldMore = false;

    private FragmentDetailInfoBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_info, container, false);
        ((StoreDetailActivity) activity).setReceiveStoreSecondEvent(this);
        initViews(mBinding.getRoot());
        return mBinding.getRoot();
    }

    private void initViews(View view) {
        rcv_menu = view.findViewById(R.id.rcv_menu);
        tv_time = view.findViewById(R.id.tv_time);
        tv_price = view.findViewById(R.id.tv_price);
        tv_address = view.findViewById(R.id.tv_address);
        tv_phone = view.findViewById(R.id.tv_phone);
        tv_image = view.findViewById(R.id.tv_image);
        mBinding.btnMore.setOnClickListener(this);
    }

    private void initData() {
        Date d = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (store.getOpen_time_text() != null && store.getOpen_time_text().length() > 0)
            tv_time.setText(LanguageBinding.getString(R.string.open_time_title, activity) + " " + store.getOpen_time_text());
        else {
            if (store.getList_open_time() != null && store.getList_open_time().size() > 0) {
                long openTime = store.getList_open_time().get(dayOfWeek).getOpen_time();
                long closeTime = store.getList_open_time().get(dayOfWeek).getClose_time();

                tv_time.setText(LanguageBinding.getString(R.string.open_time_title, activity) + " " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(openTime))
                        + " - " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(closeTime)));
            }
        }

        tv_price.setText(LanguageBinding.getString(R.string.merchant_detail_average_price, activity) + ": " + Helper.getVNCurrency(store.getMin_price()) + "đ - " + Helper.getVNCurrency(store.getMax_price()) + "đ");
        tv_address.setText(LanguageBinding.getString(R.string.info_update_address, activity) + ": " + store.getAddress());
        tv_phone.setText("Hot line: " + store.getPhone());

        mBinding.tvNumberRating.setText(String.format(LanguageBinding.getString(R.string.store_detail_number_rating, activity), store.getTotal_amount_feedback()));
        if (store.getTotal_amount_feedback() <= 3) {
            mBinding.btnMore.setVisibility(View.INVISIBLE);
        }
        if (store.getTotal_amount_feedback() == 0) {
            mBinding.llRating.setVisibility(View.GONE);
        } else {
            mBinding.tvNoneRating.setVisibility(View.GONE);
            mBinding.tvAverageRating.setText(String.valueOf(store.getAverage_rating_feedback()));
            initRatingChart();
            setRatingAdapter();
        }

        setAdapter();
        checkRemainMoreFeedback();
    }

    private void setAdapter() {
        if (store.getList_image() == null || store.getList_image().size() == 0) {
            tv_image.setVisibility(View.GONE);
            return;
        }
        adapter = new ImageDetailAdapter(activity, store.getList_image());
        rcv_menu.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        rcv_menu.setAdapter(adapter);
    }

    private void setRatingAdapter() {
        listUserFeedback.addAll(store.getList_user_feedback());
        userFeedbackAdapter = new UserFeedbackAdapter(activity, listUserFeedback);

        mBinding.rcvRating.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mBinding.rcvRating.setAdapter(userFeedbackAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
        mBinding.rcvRating.addItemDecoration(dividerItemDecoration);
        mBinding.rcvRating.setNestedScrollingEnabled(false);
        mBinding.nsv.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = mBinding.nsv.getChildAt(mBinding.nsv.getChildCount() - 1);

                int diff = (view.getBottom() - (mBinding.nsv.getHeight() + mBinding.nsv
                        .getScrollY()));

                // height threshold for loading (same as 5-7 item)
                int threshold = 1200;

                if (shouldMore && !isLoading && !noRemain && diff <= threshold) {
                    Log.d("rum", "onScrollChanged: loading more");
                    if (userFeedbackAdapter == null)
                        return;
                    getUserFeedback();
                    isLoading = true;
                }
            }
        });
    }

    private void initRatingChart() {
        mBinding.pb5Star.setMax((int) store.getTotal_amount_feedback());
        mBinding.pb4Star.setMax((int) store.getTotal_amount_feedback());
        mBinding.pb3Star.setMax((int) store.getTotal_amount_feedback());
        mBinding.pb2Star.setMax((int) store.getTotal_amount_feedback());
        mBinding.pb1Star.setMax((int) store.getTotal_amount_feedback());
        for (ScoreFeedbackModel score : store.getList_score_feedback()
        ) {
            switch ((int) score.getRating()) {
                case 5:
                    mBinding.pb5Star.setProgress((int) score.getAmount());
                    break;
                case 4:
                    mBinding.pb4Star.setProgress((int) score.getAmount());
                    break;
                case 3:
                    mBinding.pb3Star.setProgress((int) score.getAmount());
                    break;
                case 2:
                    mBinding.pb2Star.setProgress((int) score.getAmount());
                    break;
                case 1:
                    mBinding.pb1Star.setProgress((int) score.getAmount());
                    break;
            }
        }
    }

    private void getUserFeedback() {
        if(store != null){
            if (!isLoading) {
                DeliDataLoader.getUserFeedback(activity, new ApiCallBack() {
                    @Override
                    public void handleCallback(boolean isSuccess, Object object) {
                        if (isSuccess) {
                            if (page == 1) {
                                listUserFeedback.clear();
                            }
                            ArrayList<UserFeedbackModel> listItem = (ArrayList<UserFeedbackModel>) object;
                            if (listItem == null || listItem.size() == 0 || listItem.size() < pageSize) {
                                noRemain = true;
                            }
                            userFeedbackAdapter.setData(listItem);
                            isLoading = false;
                            page++;
                        }
                    }
                }, page, store.getId());
            }
        }
    }

    private void checkRemainMoreFeedback(){
        DeliDataLoader.getUserFeedback(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if(isSuccess){
                    ArrayList<UserFeedbackModel> listItem = (ArrayList<UserFeedbackModel>) object;
                    if(listItem.size() <= 3){
                        mBinding.btnMore.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }, 1, store.getId());
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_more:
                mBinding.btnMore.setVisibility(View.INVISIBLE);
                shouldMore = true;
                getUserFeedback();
                isLoading = true;
                break;
        }
    }

    @Override
    public void onDataReceivedOnSecondTab(StoreOfCategoryModel store) {
        this.store = store;
        if (store != null)
            initData();
    }
}
