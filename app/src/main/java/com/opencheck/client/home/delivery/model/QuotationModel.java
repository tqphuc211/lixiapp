package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class QuotationModel implements Serializable {
    private String code;
    private String name;
    private long money;

    public QuotationModel(String code, String name, long money) {
        this.code = code;
        this.name = name;
        this.money = money;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }
}
