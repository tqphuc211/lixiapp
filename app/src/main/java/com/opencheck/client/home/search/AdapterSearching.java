package com.opencheck.client.home.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowPlaceBinding;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.FilterModel;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class AdapterSearching extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MerchantModel> listPlace = new ArrayList<>();
    private List<LixiShopEvoucherModel> listVoucher = new ArrayList<>();
    private LixiActivity activity;
    private final int PLACE = 0;
    private final int VOUCHER = 1;
    private AdapterPlaceSearch adapterPlace;
    private FilterModel filterSearchModel;
    private EditText edt_search;

    public AdapterSearching(List<MerchantModel> list, LixiActivity activity, List<LixiShopEvoucherModel> listVoucher, FilterModel filterSearchModel, EditText search) {
        this.listPlace = list;
        this.activity = activity;
        this.listVoucher = listVoucher;
        this.filterSearchModel = filterSearchModel;
        this.edt_search = search;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return PLACE;
        else if (position >= 1)
            return VOUCHER;
        else return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case PLACE: {
                RowPlaceBinding rowPlaceBinding = RowPlaceBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
                return new PlaceHolder(rowPlaceBinding.getRoot());
            }
            case VOUCHER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.discovery_row_voucher_fragment, parent, false);
                return new EvoucherHolder(view);
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.discovery_row_voucher_fragment, parent, false);
        return new EvoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == PLACE) {
            PlaceHolder placeHolder = (PlaceHolder) holder;
            adapterPlace = new AdapterPlaceSearch(activity, listPlace, filterSearchModel, edt_search);
            placeHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            placeHolder.recyclerView.setAdapter(adapterPlace);
            placeHolder.recyclerView.setNestedScrollingEnabled(false);

            if (listPlace == null || listPlace.size() == 0) {
                placeHolder.lnNoResult.setVisibility(View.VISIBLE);

            }
            if (listVoucher == null || listVoucher.size() == 0) {
                placeHolder.lnNoResultVoucher.setVisibility(View.VISIBLE);
            }
            if (listPlace != null && listPlace.size() > 0 && listVoucher != null && listVoucher.size() > 0) {
                placeHolder.lnNoResultVoucher.setVisibility(View.GONE);
                placeHolder.lnNoResult.setVisibility(View.GONE);
            }
        } else {
            if (listVoucher.size() > 0) {

                EvoucherHolder merchantHolder = (EvoucherHolder) holder;
                ViewGroup.LayoutParams layoutParams = merchantHolder.iv_hinh.getLayoutParams();
                layoutParams.width = (activity).widthScreen / 3 - 30;
                merchantHolder.iv_hinh.setLayoutParams(layoutParams);
                final LixiShopEvoucherModel model = listVoucher.get(position - 1);

                if (model.getProduct_cover_image() == null || model.getProduct_cover_image().equals(""))
                    ImageLoader.getInstance().displayImage(model.getProduct_image().get(0), merchantHolder.iv_hinh, LixiApplication.getInstance().optionsNomal);
                else
                    ImageLoader.getInstance().displayImage(model.getProduct_cover_image(), merchantHolder.iv_hinh, LixiApplication.getInstance().optionsNomal);

                merchantHolder.tv_name.setText(model.getName());
                merchantHolder.tv_price.setText(Helper.getVNCurrency(model.getPayment_discount_price()) + "đ");
                merchantHolder.tv_lixi_cashback.setText("+" + Helper.getVNCurrency(model.getCashback_price()) + " " + activity.getResources().getString(R.string.lixi));
                merchantHolder.tv_merchant_name.setText(model.getMerchant_name());

                if (model.getQuantity_order_done() == 0)
                    merchantHolder.tv_count_buyer.setText(LanguageBinding.getString(R.string.first_customer, activity));
                else
                    merchantHolder.tv_count_buyer.setText(Helper.getVNCurrency(listVoucher.get(position - 1).getQuantity_order_done()) + " khách tham gia");

                merchantHolder.ll_holder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (model.getProduct_type().equals("combo")) {
//                            long tmp = model.getId();
//                            Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                            intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, model.getId());
//                            intent.putExtra(ConstantValue.IS_COMBO, true);
//                            activity.startActivity(intent);

                            ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, model.getId(), TrackingConstant.ContentSource.SEARCH,
                                    ActivityProductCashEvoucherDetail.getParamIsComboItem(true));
                        } else {
//                            long tmp = model.getId();
//                            Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                            intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, model.getId());
//                            activity.startActivity(intent);

                            ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, model.getId(), TrackingConstant.ContentSource.SEARCH);
                        }

                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return listVoucher.size() + 1;
    }

    public class EvoucherHolder extends RecyclerView.ViewHolder {
        private ImageView iv_hinh;
        private TextView tv_name;
        private TextView tv_price;
        private TextView tv_lixi_cashback;
        private TextView tv_merchant_name;
        private TextView tv_count_buyer;
        private LinearLayout ll_holder;


        public EvoucherHolder(View itemView) {
            super(itemView);
            iv_hinh = itemView.findViewById(R.id.img_voucher);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_lixi_cashback = itemView.findViewById(R.id.tv_lixi_cashback);
            tv_merchant_name = itemView.findViewById(R.id.tv_merchant_name);
            tv_count_buyer = itemView.findViewById(R.id.tv_count_buyer);
            ll_holder = itemView.findViewById(R.id.ll_holder);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        private LinearLayout lnNoResult, lnNoResultVoucher;
        private RecyclerView recyclerView;

        public PlaceHolder(View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.rclv_place);
            lnNoResult = itemView.findViewById(R.id.lnNoResult);
            lnNoResultVoucher = itemView.findViewById(R.id.lnNoResultVoucher);
        }
    }

    public void reloadPlace(ArrayList<MerchantModel> _list) {
        if (listPlace.size() > 0)
            listPlace.clear();
        listPlace = _list;
        notifyDataSetChanged();
    }
}