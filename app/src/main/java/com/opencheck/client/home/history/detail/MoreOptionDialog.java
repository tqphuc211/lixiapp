package com.opencheck.client.home.history.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.McMoreLayoutDialogBinding;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class MoreOptionDialog extends LixiDialog {
    public MoreOptionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;

    private TextView tv_message, tvCopyMaThe, tvCopyMaPin, tvActionCancel;
    private TextView tv_title;
    private TextView bt_ok;
    private String maPin = "";
    private String maSerial = "";

    String mss = "";

    public void setData(String maPin, String maThe, String ms) {
        this.maPin = maPin;
        this.maSerial = maThe;
        mss = ms;
        pourData();
    }

    private void pourData() {
        tv_message.setText(mss);
        tv_title.setText(LanguageBinding.getString(R.string.notification_label, activity));
        tvCopyMaPin.setOnClickListener(this);
        tvCopyMaThe.setOnClickListener(this);
        tvActionCancel.setOnClickListener(this);
        rl_dismiss.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
    }

    private McMoreLayoutDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = McMoreLayoutDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
        initDialogEvent();
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        tvCopyMaThe = (TextView) findViewById(R.id.tvCopyMaThe);
        tvCopyMaPin = (TextView) findViewById(R.id.tvCopyMaPin);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tvActionCancel = (TextView) findViewById(R.id.tvActionCancel);


        bt_ok = (TextView) findViewById(R.id.bt_ok);


    }

    public void setOkButtonText(String txt) {
        bt_ok.setText(txt);
    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    boolean dismissAble = true;

    public void setDismissAble(boolean dismissAble) {
        this.dismissAble = dismissAble;
        setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rl_dismiss:
                if (dismissAble)
                    dismiss();
                break;
            case R.id.tvActionCancel:
                dismiss();
                break;
            case R.id.tvCopyMaPin:
                if (!maSerial.contains("-")) {
                    Helper.copyCode(maSerial, activity);
                    dismiss();
                }
                break;
            case R.id.tvCopyMaThe:
                if (!maPin.contains("-")) {
                    Helper.copyCode(maPin, activity);
                    dismiss();
                }
                break;
            case R.id.bt_ok:
                if (dismissAble)
                    dismiss();
                if (eventListener != null)
                    eventListener.onOk();
            default:

                break;
        }
    }

    public interface IDidalogEvent {
        void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
