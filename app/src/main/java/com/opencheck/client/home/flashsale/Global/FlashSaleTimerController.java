package com.opencheck.client.home.flashsale.Global;

import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FlashSaleTimerLayoutBinding;
import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;

public class FlashSaleTimerController {

    LixiActivity activity;
    View rootView;
    LinearLayout parantView;


    public FlashSaleTimerController(LixiActivity activity, LinearLayout parantView) {
        this.activity = activity;
        this.parantView = parantView;
        FlashSaleTimerLayoutBinding saleTimerLayoutBinding =
                FlashSaleTimerLayoutBinding.inflate(LayoutInflater.from(activity),
                        null, false);
        rootView = saleTimerLayoutBinding.getRoot();
        parantView.addView(rootView);
        findViews();
    }

    public void showTime(int[] arrTime) {
        tv_hours.setText(formatTimer(arrTime[0]));
        tv_minute.setText(formatTimer(arrTime[1]));
        tv_second.setText(formatTimer(arrTime[2]));
    }

    public static String formatTimer(int numb) {
        String result = "";
        if (numb < 10) {
            result = "0" + numb;
        } else {
            result = numb + "";
        }
        return result;
    }


    private TextView tv_hours;
    private TextView tv_minute;
    private TextView tv_second;

    private void findViews() {
        tv_hours = (TextView) rootView.findViewById(R.id.tv_hours);
        tv_minute = (TextView) rootView.findViewById(R.id.tv_minute);
        tv_second = (TextView) rootView.findViewById(R.id.tv_second);
    }

}
