package com.opencheck.client.home.delivery.activities;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityFeedbackBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.FeedbackItemAdapter;
import com.opencheck.client.home.delivery.model.FeedbackItemModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.willy.ratingbar.BaseRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeedbackActivity extends LixiActivity {
    private ArrayList<FeedbackItemModel> listFeedback;
    private FeedbackItemAdapter adapter;
    private String uuid = "";
    private float originRating = 0;
    public final static int REQUEST_FEEDBACK_CODE = 127;

    private ActivityFeedbackBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);
        uuid = getIntent().getStringExtra("uuid");
        initViews();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        mBinding.ratingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(BaseRatingBar baseRatingBar, float v) {
                if (originRating == 0) {
                    mBinding.etComment.setVisibility(View.VISIBLE);
                    mBinding.tvMessage.setVisibility(View.VISIBLE);
                }
                originRating = v;
                if (v > 3) {
                    mBinding.tvMessage.setText("Rất vui vì phục vụ bạn. Bạn còn \n" + "nhận xét gì về Lixi không ? ");
                    mBinding.rvFeedback.setVisibility(View.GONE);
                } else if (v > 0) {
                    mBinding.tvMessage.setText("Có thể cho chúng tôi biết cần cải thiện\n" + "điều gì để tốt hơn ? ");
                    mBinding.rvFeedback.setVisibility(View.VISIBLE);
                    scrollDown();
                } else {
                    mBinding.tvMessage.setVisibility(View.GONE);
                    mBinding.etComment.setVisibility(View.GONE);
                    mBinding.rvFeedback.setVisibility(View.GONE);
                }
            }
        });

        mBinding.ivClose.setOnClickListener(this);
        mBinding.tvSend.setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getFeedbackItem();
            }
        }, 300);

        mBinding.scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mBinding.etComment.getVisibility() == View.VISIBLE) {
                    View view = (View) mBinding.scrollView.getChildAt(mBinding.scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (mBinding.scrollView.getHeight() + mBinding.scrollView.getScrollY()));
                    if (diff == 0) {
                        mBinding.tvMessage.setVisibility(View.GONE);
                        scrollDown();
                    } else {
                        mBinding.tvMessage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void scrollDown() {
        mBinding.scrollView.post(new Runnable() {
            @Override
            public void run() {
                mBinding.scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void getFeedbackItem() {
        DeliDataLoader.getFeedbackItem(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listFeedback = (ArrayList<FeedbackItemModel>) object;
                    if (listFeedback == null || listFeedback.size() == 0)
                        return;
                    setAdapter();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    private void sendFeedback() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_order_uuid", uuid);
            params.put("rating", mBinding.ratingBar.getRating());

            if (mBinding.ratingBar.getRating() <= 3) {
                int countFeedback = 0;
                JSONArray type_code = new JSONArray();
                for (int i = 0; i < listFeedback.size(); i++) {
                    if (listFeedback.get(i).isSelected()) {
                        countFeedback++;
                        type_code.put(listFeedback.get(i).getCode());
                    }
                }

                if (countFeedback == 0) {
                    Toast.makeText(activity, "Vui lòng chọn tiêu chí đánh giá", Toast.LENGTH_SHORT).show();
                    return;
                }
                params.put("list_type_code", type_code);
                params.put("message", mBinding.etComment.getText().toString());
            } else params.put("message", mBinding.etComment.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        DeliDataLoader.sendFeedback(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    Helper.showSuccessDialog(activity, "Cảm ơn bạn đã đánh giá dịch vụ của chúng tôi. Hẹn gặp lại bạn");
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        }, params);
    }

    private void setAdapter() {
        adapter = new FeedbackItemAdapter(activity, listFeedback);
        mBinding.rvFeedback.setLayoutManager(new GridLayoutManager(activity, 3));
        mBinding.rvFeedback.setAdapter(adapter);
        adapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                toggle(pos);
            }
        });
    }

    private void toggle(int pos) {
        listFeedback.get(pos).setSelected(!listFeedback.get(pos).isSelected());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.ivClose) {
            finish();
        } else if (view == mBinding.tvSend) {
            if (mBinding.ratingBar.getRating() > 0)
                sendFeedback();
            else
                Toast.makeText(activity, "Xin hãy đánh giá dịch vụ của chúng tôi", Toast.LENGTH_SHORT).show();
        }
    }
}
