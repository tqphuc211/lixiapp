package com.opencheck.client.home.product.communicate;

public interface OnSettingListener {
    void onSetting(int option);
}
