package com.opencheck.client.home.delivery.model;

import com.opencheck.client.home.delivery.enum_key.FeeType;
import com.opencheck.client.models.merchant.TagModel;
import com.opencheck.client.utils.ConstantValue;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreOfCategoryModel implements Serializable {
    private long id;
    private long merchant_id;
    private String state;
    private String name;
    private String address;
    private String description;
    private String open_time_text;
    private String pause_reason_message;
    private String phone;
    private String logo_link;
    private long min_price;
    private long max_price;
    private String open_time;
    private String close_time;
    private String cashback_type;
    private float lat;
    private float lng;
    private long ship_price; // phí tiêu chuẩn cho đơn thường
    private long min_money_to_ship;
    private long service_fixed_price;
    private long service_percent_price;
    private long min_money_to_free_service_price;
    private long ship_price_fixed;
    private long ship_price_percent;
    private long cashback_price;
    private Long min_money_to_free_ship_price;
    private Long max_free_ship_money;
    private boolean service_price_by_merchant;
    private boolean trusted_store;
    private boolean is_opening;
    private long min_money_to_go_shipping; // phí tối thiếu của đơn bình thường
    private long shipping_distance;
    private long shipping_duration_min;
    private long shipping_duration_max;
    private String cover_link;
    private String promotion_text;
    private String web_mobile_url;
    private ArrayList<TagModel> list_tag;
    private ArrayList<ImageModel> list_image;
    private ArrayList<PromotionModel> list_promotion_code;
    private ArrayList<OpenTimeModel> list_open_time;
    private Long end_pause_time;
    private boolean isChecked = false;
    private boolean allow_order_in_pause_time;
    private int delay_after_open;
    private boolean apply_service_price;
    private ArrayList<StorePolicyModel> list_store_policy;
    private long min_money_to_fixed_ship_price;
    private double average_rating_feedback;
    private long total_amount_feedback;
    private ArrayList<ScoreFeedbackModel> list_score_feedback;
    private ArrayList<UserFeedbackModel> list_user_feedback;
    private boolean is_favorite;

    public boolean isIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public double getAverage_rating_feedback() {
        return average_rating_feedback;
    }

    public void setAverage_rating_feedback(double average_rating_feedback) {
        this.average_rating_feedback = average_rating_feedback;
    }

    public long getTotal_amount_feedback() {
        return total_amount_feedback;
    }

    public void setTotal_amount_feedback(long total_amount_feedback) {
        this.total_amount_feedback = total_amount_feedback;
    }

    public ArrayList<ScoreFeedbackModel> getList_score_feedback() {
        return list_score_feedback;
    }

    public void setList_score_feedback(ArrayList<ScoreFeedbackModel> list_score_feedback) {
        this.list_score_feedback = list_score_feedback;
    }

    public ArrayList<UserFeedbackModel> getList_user_feedback() {
        return list_user_feedback;
    }

    public void setList_user_feedback(ArrayList<UserFeedbackModel> list_user_feedback) {
        this.list_user_feedback = list_user_feedback;
    }
    private long min_order_use_lixi;
    private String x_factor;
    private boolean is_use_lixi;
    private boolean is_use_lixi_config;

    public long getMin_order_use_lixi() {
        return min_order_use_lixi;
    }

    public void setMin_order_use_lixi(long min_order_use_lixi) {
        this.min_order_use_lixi = min_order_use_lixi;
    }

    public String getX_factor() {
        return x_factor;
    }

    public void setX_factor(String x_factor) {
        this.x_factor = x_factor;
    }

    public boolean isIs_use_lixi() {
        return is_use_lixi;
    }

    public void setIs_use_lixi(boolean is_use_lixi) {
        this.is_use_lixi = is_use_lixi;
    }

    public boolean isIs_use_lixi_config() {
        return is_use_lixi_config;
    }

    public void setIs_use_lixi_config(boolean is_use_lixi_config) {
        this.is_use_lixi_config = is_use_lixi_config;
    }

    public long getMin_money_to_fixed_ship_price() {
        return min_money_to_fixed_ship_price;
    }

    public void setMin_money_to_fixed_ship_price(long min_money_to_fixed_ship_price) {
        this.min_money_to_fixed_ship_price = min_money_to_fixed_ship_price;
    }

    public ArrayList<StorePolicyModel> getList_store_policy() {
        return list_store_policy;
    }

    public void setList_store_policy(ArrayList<StorePolicyModel> list_store_policy) {
        this.list_store_policy = list_store_policy;
    }

    public int getDelay_after_open() {
        return delay_after_open;
    }

    public void setDelay_after_open(int delay_after_open) {
        this.delay_after_open = delay_after_open;
    }

    public boolean isAllow_order_in_pause_time() {
        return allow_order_in_pause_time;
    }

    public void setAllow_order_in_pause_time(boolean allow_order_in_pause_time) {
        this.allow_order_in_pause_time = allow_order_in_pause_time;
    }

    public boolean isApply_service_price() {
        return apply_service_price;
    }

    public void setApply_service_price(boolean apply_service_price) {
        this.apply_service_price = apply_service_price;
    }

    private Long fixed_ship_price;

    private Long fixed_ship_price_range;

    public Long getEnd_pause_time() {
        return end_pause_time;
    }

    public void setEnd_pause_time(Long end_pause_time) {
        this.end_pause_time = end_pause_time;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isIs_opening() {
        return is_opening;
    }

    public void setIs_opening(boolean is_opening) {
        this.is_opening = is_opening;
    }

    public String getPause_reason_message() {
        return pause_reason_message;
    }

    public void setPause_reason_message(String pause_reason_message) {
        this.pause_reason_message = pause_reason_message;
    }

    public Long getMax_free_ship_money() {
        return max_free_ship_money;
    }

    public void setMax_free_ship_money(Long max_free_ship_money) {
        this.max_free_ship_money = max_free_ship_money;
    }

    public ArrayList<PromotionModel> getList_promotion_code() {
        return list_promotion_code;
    }

    public void setList_promotion_code(ArrayList<PromotionModel> list_promotion_code) {
        this.list_promotion_code = list_promotion_code;
    }

    public ArrayList<OpenTimeModel> getList_open_time() {
        return list_open_time;
    }

    public void setList_open_time(ArrayList<OpenTimeModel> list_open_time) {
        this.list_open_time = list_open_time;
    }

    public String getOpen_time_text() {
        return open_time_text;
    }

    public void setOpen_time_text(String open_time_text) {
        this.open_time_text = open_time_text;
    }

    public String getPromotion_text() {
        return promotion_text;
    }

    public void setPromotion_text(String promotion_text) {
        this.promotion_text = promotion_text;
    }

    public String getWeb_mobile_url() {
        return web_mobile_url;
    }

    public void setWeb_mobile_url(String web_mobile_url) {
        this.web_mobile_url = web_mobile_url;
    }

    public String getCashback_type() {
        return cashback_type;
    }

    public void setCashback_type(String cashback_type) {
        this.cashback_type = cashback_type;
    }

    public long getCashback_price() {
        return cashback_price;
    }

    public void setCashback_price(long cashback_price) {
        this.cashback_price = cashback_price;
    }

    public ArrayList<ImageModel> getList_image() {
        return list_image;
    }

    public void setList_image(ArrayList<ImageModel> list_image) {
        this.list_image = list_image;
    }

    public String getCover_link() {
        if (cover_link != null && cover_link.length() > 0)
            if (!cover_link.equals(ConstantValue.DEFAULT_IMAGE[0]))
                return cover_link;
        return getLogo_link();
    }

    public void setCover_link(String cover_link) {
        this.cover_link = cover_link;
    }

    public ArrayList<TagModel> getList_tag() {
        return list_tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setList_tag(ArrayList<TagModel> list_tag) {
        this.list_tag = list_tag;
    }

    private long service_price;

    public long getService_price() {
        return service_price;
    }

    public void setService_price(long service_price) {
        this.service_price = service_price;
    }

    public long getMin_money_to_free_service_price() {
        return min_money_to_free_service_price;
    }

    public void setMin_money_to_free_service_price(long min_money_to_free_service_price) {
        this.min_money_to_free_service_price = min_money_to_free_service_price;
    }

    public long getShip_price_fixed() {
        return ship_price_fixed;
    }

    public void setShip_price_fixed(long ship_price_fixed) {
        this.ship_price_fixed = ship_price_fixed;
    }

    public long getShip_price_percent() {
        return ship_price_percent;
    }

    public void setShip_price_percent(long ship_price_percent) {
        this.ship_price_percent = ship_price_percent;
    }

    public Long getMin_money_to_free_ship_price() {
        return min_money_to_free_ship_price;
    }

    public void setMin_money_to_free_ship_price(Long min_money_to_free_ship_price) {
        this.min_money_to_free_ship_price = min_money_to_free_ship_price;
    }

    public long getMin_money_to_go_shipping() {
        return min_money_to_go_shipping;
    }

    public void setMin_money_to_go_shipping(long min_money_to_go_shipping) {
        this.min_money_to_go_shipping = min_money_to_go_shipping;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogo_link() {
        if (logo_link == null || logo_link.length() == 0)
            return ConstantValue.DEFAULT_IMAGE[0];
        return logo_link;
    }

    public void setLogo_link(String logo_link) {
        this.logo_link = logo_link;
    }

    public long getMin_price() {
        return min_price;
    }

    public void setMin_price(long min_price) {
        this.min_price = min_price;
    }

    public long getMax_price() {
        return max_price;
    }

    public void setMax_price(long max_price) {
        this.max_price = max_price;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public long getShip_price() {
        return ship_price;
    }

    public void setShip_price(long ship_price) {
        this.ship_price = ship_price;
    }

    public long getMin_money_to_ship() {
        return min_money_to_ship;
    }

    public void setMin_money_to_ship(long min_money_to_ship) {
        this.min_money_to_ship = min_money_to_ship;
    }

    public long getService_fixed_price() {
        return service_fixed_price;
    }

    public void setService_fixed_price(long service_fixed_price) {
        this.service_fixed_price = service_fixed_price;
    }

    public long getService_percent_price() {
        return service_percent_price;
    }

    public void setService_percent_price(long service_percent_price) {
        this.service_percent_price = service_percent_price;
    }

    public boolean isService_price_by_merchant() {
        return service_price_by_merchant;
    }

    public void setService_price_by_merchant(boolean service_price_by_merchant) {
        this.service_price_by_merchant = service_price_by_merchant;
    }

    public boolean isTrusted_store() {
        return trusted_store;
    }

    public void setTrusted_store(boolean trusted_store) {
        this.trusted_store = trusted_store;
    }

    public long getShipping_distance() {
        return shipping_distance;
    }

    public void setShipping_distance(long shipping_distance) {
        this.shipping_distance = shipping_distance;
    }

    public long getShipping_duration_min() {
        return shipping_duration_min;
    }

    public void setShipping_duration_min(long shipping_duration_min) {
        this.shipping_duration_min = shipping_duration_min;
    }

    public long getShipping_duration_max() {
        return shipping_duration_max;
    }

    public void setShipping_duration_max(long shipping_duration_max) {
        this.shipping_duration_max = shipping_duration_max;
    }

    public Long getFixed_ship_price() {
        return fixed_ship_price;
    }

    public void setFixed_ship_price(long fixed_ship_price) {
        this.fixed_ship_price = fixed_ship_price;
    }

    public Long getFixed_ship_price_range() {
        return fixed_ship_price_range;
    }

    public void setFixed_ship_price_range(long fixed_ship_price_range) {
        this.fixed_ship_price_range = fixed_ship_price_range;
    }

    public FeeType getFeeType() {
        if (getFixed_ship_price() != null
                && getFixed_ship_price_range() != null
                && getFixed_ship_price_range() * getFixed_ship_price_range() != 0) {
            return FeeType.SAME_FEE;
        } else if (getMin_money_to_free_ship_price() != null) {// && getMin_money_to_free_ship_price() > 0) {
            return FeeType.FREE_SHIP;
        } else {
            return FeeType.NORMAL;
        }
    }
}
