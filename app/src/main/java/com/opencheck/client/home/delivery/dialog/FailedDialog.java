package com.opencheck.client.home.delivery.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DeliFailedDialogBinding;

public class FailedDialog extends LixiDialog {

    public FailedDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private Button btnClose;
    private LinearLayout ll_buy;
    private TextView tv_uuid;
    private String uuid;

    private DeliFailedDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DeliFailedDialogBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        btnClose = findViewById(R.id.btnClose);
        ll_buy = findViewById(R.id.ll_buy);
        tv_uuid = findViewById(R.id.tv_uuid);

        btnClose.setOnClickListener(this);
        ll_buy.setOnClickListener(this);
    }

    public void setData(String uuid) {
        this.uuid = uuid;
        tv_uuid.setText("#Mã Đơn Hàng: " + uuid);
    }

    @Override
    public void onClick(View view) {
        if (view == btnClose) {
            dismiss();
        } else if (view == ll_buy) {

        }
    }
}
