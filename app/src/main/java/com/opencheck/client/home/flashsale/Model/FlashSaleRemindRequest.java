package com.opencheck.client.home.flashsale.Model;

import com.opencheck.client.models.LixiModel;

public class FlashSaleRemindRequest extends LixiModel {

    /**
     * flash_sale : 3
     * product : 48
     */

    private long flash_sale;
    private long product;

    public long getFlash_sale() {
        return flash_sale;
    }

    public void setFlash_sale(long flash_sale) {
        this.flash_sale = flash_sale;
    }

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }
}
