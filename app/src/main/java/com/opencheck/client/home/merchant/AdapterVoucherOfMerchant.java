package com.opencheck.client.home.merchant;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.flashsale.Global.VoucherFlashSaleHolder;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class AdapterVoucherOfMerchant extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    MerchantVoucherFragment newVoucherFragment;
    LixiActivity activity;
    ArrayList<LixiShopEvoucherModel> list;
    int layoutId;
    int cWith;
    long productID;

    public AdapterVoucherOfMerchant(LixiActivity activity, ArrayList<LixiShopEvoucherModel> list, int layoutId, int cWith, long productID) {
        this.activity = activity;
        this.list = list;
        this.layoutId = layoutId;
        this.cWith = cWith;
        this.productID = productID;
    }

    public AdapterVoucherOfMerchant(LixiActivity activity, ArrayList<LixiShopEvoucherModel> list, int layoutId, int cWith) {
        this.activity = activity;
        this.list = list;
        this.layoutId = layoutId;
        this.cWith = cWith;

    }

    public AdapterVoucherOfMerchant(LixiActivity activity, MerchantVoucherFragment newVoucherFragment, ArrayList<LixiShopEvoucherModel> list, int cWith) {
        this.newVoucherFragment = newVoucherFragment;
        this.list = list;
        this.cWith = cWith;
        this.activity = activity;

    }


    private int VOUCHER = 1;
    private int VOUCHER_FS = -1;

    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > position)
            if (list.get(position).getFlash_sale_id() > 0)
                return VOUCHER_FS;
        return VOUCHER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VOUCHER_FS) {
            VoucherFlashSaleHolder voucherFsHolder = VoucherFlashSaleHolder.newHolder(activity, parent);
            return voucherFsHolder;
        }

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(layoutId, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                if (pos < list.size()) {
//                    CashEvoucherDetailActivity.startCashVoucherDetail(activity, list.get(pos).getId());
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list.get(pos).getId(), null);
                }
            }
        });
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder voucherHolder1, final int position) {

        if (getItemViewType(position) == VOUCHER_FS) {
            VoucherFlashSaleHolder holder = (VoucherFlashSaleHolder) voucherHolder1;
            if (list.size() <= position)
                return;
            LixiShopEvoucherModel voucher = list.get(position);

            VoucherFlashSaleHolder.bindViewHolder(holder, voucher);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (productID == list.get(position).getId()){
                        ActivityProductCashEvoucherDetail.finishCashVoucherDetail();
                    }
                    ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list.get(position).getId(), null);
                }
            });
            return;
        }

        AdapterVoucherOfMerchant.ViewHolder voucherHolder = (AdapterVoucherOfMerchant.ViewHolder) voucherHolder1;
        LixiShopEvoucherModel voucher = list.get(position);
        voucherHolder.itemView.setTag(position);

        if (position == 0)
            voucherHolder.v_line.setVisibility(View.GONE);
        else
            voucherHolder.v_line.setVisibility(View.VISIBLE);
        if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0)
            ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        else {
            if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0)
                ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        }
        voucherHolder.tvVoucherName.setText(voucher.getName());

        voucherHolder.tvVoucherPrice.setText(Helper.getVNCurrency(voucher.getPayment_discount_price()) + "đ");
        if (voucher.getPayment_discount_price() != voucher.getPayment_price()) {
            voucherHolder.tvOldPrice.setVisibility(View.VISIBLE);
            voucherHolder.tvOldPrice.setText(Helper.getVNCurrency(voucher.getPayment_price()) + "đ");
            voucherHolder.tvOldPrice.setPaintFlags(voucherHolder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else voucherHolder.tvOldPrice.setVisibility(View.GONE);
        if (voucher.getCashback_price() > 0) {
            voucherHolder.tvLixiBonus.setVisibility(View.VISIBLE);
            voucherHolder.tvLixiBonus.setText(Helper.getVNCurrency(voucher.getCashback_price()) + " Lixi");
        } else voucherHolder.tvLixiBonus.setVisibility(View.GONE);

//        voucherHolder.tvMerchantInfo.setText(voucher.getMerchant_name() + " | " + voucher.getCount_store() + " cửa hàng");
        voucherHolder.tvMerchantInfo.setVisibility(View.GONE);
        voucherHolder.tvCountAttend.setText(voucher.getQuantity_order_done() + " " + LanguageBinding.getString(R.string.quantity_bought_customer, activity));

        if (voucher.getPayment_price() != voucher.getPayment_discount_price()) {
            voucherHolder.tv_percent_discount.setVisibility(View.VISIBLE);
            int percent = 100 - (int) (voucher.getPayment_discount_price() * 100 / voucher.getPayment_price());
            voucherHolder.tv_percent_discount.setText("-" + percent + "%");
        } else voucherHolder.tv_percent_discount.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        private View v_line;
        private ImageView imgLogoVoucher;
        private TextView tv_percent_discount;
        private TextView tvVoucherName;
        private TextView tvVoucherPrice;
        private TextView tvOldPrice;
        private TextView tvLixiBonus;
        private TextView tvMerchantInfo;
        private TextView tvCountAttend;
        private TextView tvDistance;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            rootView.setOnClickListener(this);
            v_line = itemView.findViewById(R.id.v_line);
            imgLogoVoucher = (ImageView) itemView.findViewById(R.id.imgLogoVoucher);
            tv_percent_discount = (TextView) itemView.findViewById(R.id.tv_percent_discount);
            tvVoucherName = (TextView) itemView.findViewById(R.id.tvVoucherName);

            tvVoucherPrice = (TextView) itemView.findViewById(R.id.tvVoucherPrice);
            tvOldPrice = (TextView) itemView.findViewById(R.id.tvOldPrice);
            tvLixiBonus = (TextView) itemView.findViewById(R.id.tvLixiBonus);
            tvMerchantInfo = (TextView) itemView.findViewById(R.id.tvMerchantInfo);
            tvCountAttend = (TextView) itemView.findViewById(R.id.tvCountAttend);

            tvDistance = (TextView) itemView.findViewById(R.id.tvDistance);
        }

        @Override
        public void onClick(View v) {
            if (v == rootView) {
                if (productID == list.get((int) v.getTag()).getId()){
                    ActivityProductCashEvoucherDetail.finishCashVoucherDetail();
                }
//                CashEvoucherDetailActivity.startCashVoucherDetail(activity, list.get((int) v.getTag()).getId());
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, list.get((int) v.getTag()).getId(), TrackingConstant.ContentSource.MERCHANT);
            }
        }
    }

}