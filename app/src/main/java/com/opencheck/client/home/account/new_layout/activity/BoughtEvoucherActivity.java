package com.opencheck.client.home.account.new_layout.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ActivityBoughtEvoucherBinding;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.evoucher.EVoucherAccountActivity;
import com.opencheck.client.home.account.evoucher.Fragment.MultiActiveFragment;
import com.opencheck.client.home.account.new_layout.adapter.EvoucherPagerAdapter;
import com.opencheck.client.home.account.new_layout.adapter.ListEvoucherAdapter;
import com.opencheck.client.home.account.new_layout.communicate.OnRefreshListener;
import com.opencheck.client.home.account.new_layout.control.OperateListCode;
import com.opencheck.client.home.account.new_layout.dialog.NotificationForCardDialog;
import com.opencheck.client.home.account.new_layout.fragment.bought_evoucher.InvalidEvoucherFragment;
import com.opencheck.client.home.account.new_layout.fragment.bought_evoucher.ValidEvoucherFragment;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

public class BoughtEvoucherActivity extends LixiActivity {


    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.PROFILE_VOUCHER_LIST;
    }

    // View
    private ImageView imgBack;
    private TextView txtTotalActive, txtTotalPrice;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout relActive;

    private EvoucherPagerAdapter adapter;

    private long totalPrice = 0;
    private long totalActiveVoucher = 0;

    private ActivityBoughtEvoucherBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bought_evoucher);
        // clear Instance list
        OperateListCode.getInstance().clearData();
        initView();
    }

    private void initView() {
        imgBack = (ImageView) findViewById(R.id.imgBack);

        txtTotalActive = (TextView) findViewById(R.id.txtTotalActive);
        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        relActive = (RelativeLayout) findViewById(R.id.relActive);

        adapter = new EvoucherPagerAdapter(getSupportFragmentManager(), activity);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        imgBack.setOnClickListener(this);
        relActive.setOnClickListener(this);

        ((ValidEvoucherFragment) adapter.getItem(0)).addOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                LixiShopEvoucherBoughtModel evoucher = (LixiShopEvoucherBoughtModel) object;
                Intent intent;
                if (action.equals(ListEvoucherAdapter.ACTION_CLICK)) {
                    if (ConstantValue.USER_MULTI_ACTIVE) {
                        intent = new Intent(BoughtEvoucherActivity.this, EVoucherAccountActivity.class);
                        intent.putExtra(ConstantValue.EVOUCHER_ID, evoucher.getId());
                        intent.putExtra(ConstantValue.CURRENT_POSITION, pos);
                        intent.putExtra(ConstantValue.TOTAL_CAN_ACTIVE, count);
                        intent.putExtra(ConstantValue.STATE_VOUCHER, "selled");
                    } else {
                        intent = new Intent(BoughtEvoucherActivity.this, ActiveCodeActivity.class);
                        intent.putExtra(ConstantValue.EVOUCHER_ID, evoucher.getId());
                    }
                    startActivity(intent);
                }

                if (action.equals(ListEvoucherAdapter.ACTION_CHECK)) {
                    if (evoucher.isSelected()) {
                        addVoucher(evoucher.getPrice());
                        OperateListCode.getInstance().getListEvoucher().add(evoucher);
                    } else {
                        subVoucher(evoucher.getPrice());
                        OperateListCode.getInstance().getListEvoucher().remove(evoucher);
                    }
                }
            }
        });

        ((ValidEvoucherFragment) adapter.getItem(0)).addOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                relActive.setVisibility(View.GONE);
                totalActiveVoucher = 0;
                totalPrice = 0;
            }
        });

        ((InvalidEvoucherFragment) adapter.getItem(1)).addOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                LixiShopEvoucherBoughtModel evoucher = (LixiShopEvoucherBoughtModel) object;
                Intent intent;
                if (action.equals(ListEvoucherAdapter.ACTION_CLICK)) {
                    if (ConstantValue.USER_MULTI_ACTIVE) {
                        intent = new Intent(BoughtEvoucherActivity.this, EVoucherAccountActivity.class);
                        intent.putExtra(ConstantValue.EVOUCHER_ID, evoucher.getId());
                        intent.putExtra(ConstantValue.CURRENT_POSITION, pos);
                        intent.putExtra(ConstantValue.TOTAL_CAN_ACTIVE, count);
                        intent.putExtra(ConstantValue.STATE_VOUCHER, "unavailable");
                    } else {
                        intent = new Intent(BoughtEvoucherActivity.this, ActiveCodeActivity.class);
                        intent.putExtra(ConstantValue.EVOUCHER_ID, evoucher.getId());
                    }
                    startActivity(intent);
                }
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(adapter.getPageTitle(1))) {
                    relActive.setVisibility(View.GONE);
                } else {
                    if (totalActiveVoucher > 0) {
                        relActive.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void addVoucher(long price) {
        totalPrice += price;
        totalActiveVoucher++;

        txtTotalPrice.setText(Helper.getVNCurrency(totalPrice) + getString(R.string.p_under));
        txtTotalActive.setText(getString(R.string.bought_evoucher_active) + " " + totalActiveVoucher + " voucher");
        if (totalActiveVoucher > 0) {
            relActive.setVisibility(View.VISIBLE);
        }
    }

    private void subVoucher(long price) {
        if (totalPrice >= price) {
            totalPrice -= price;
        }
        totalActiveVoucher--;

        txtTotalPrice.setText(Helper.getVNCurrency(totalPrice) + getString(R.string.p_under));
        txtTotalActive.setText(getString(R.string.bought_evoucher_active) + " " + totalActiveVoucher + " voucher");
        if (totalActiveVoucher <= 0) {
            relActive.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.relActive:
                // kích hoạt
                // Check group code
                if (OperateListCode.getInstance().checkMerchant()) {
                    // goto EVoucherAccount
                    MultiActiveFragment multiActiveFragment = new MultiActiveFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("layout active", true);
                    multiActiveFragment.setArguments(bundle);
                    replaceFragment(multiActiveFragment);
                } else {
                    showNotification(getString(R.string.active_voucher), getString(R.string.noti_active_multi_content));
                }
                break;
        }
    }

    private void showNotification(String title, String content) {
        NotificationForCardDialog dialog = new NotificationForCardDialog(activity, false, false, false);
        dialog.show();
        dialog.setData(title, content);
    }

    public void replaceFragment(LixiFragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped || manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frameGroup, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();

        }
    }

    public void reset() {
        totalPrice = 0;
        totalActiveVoucher = 0;
        OperateListCode.getInstance().clearData();
        relActive.setVisibility(View.GONE);
        ((ValidEvoucherFragment) adapter.getItem(0)).reset();
    }
}
