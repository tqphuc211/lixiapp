package com.opencheck.client.home.account.evoucher.Model;

import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionMessage;

import java.util.List;

public class PaymentCodModel {

    private String billing_code;
    private int count_attend;
    private int discount;
    private int discount_price;
    private List<EvoucherDetailModel> evouchers;
    private long id;
//    private LixiShopBankTransactionMessage message;
    private long payment_expired;
    private String payment_link;
    private String payment_method;
    private String platform;
    private long price;
    private long product;
    private String product_type;
    private int quantity;
    private long reduce_point;
    private String shipping_info;
    private String state;
    private String transaction_id;
    private long user;

    public String getBilling_code() {
        return billing_code;
    }

    public void setBilling_code(String billing_code) {
        this.billing_code = billing_code;
    }

    public int getCount_attend() {
        return count_attend;
    }

    public void setCount_attend(int count_attend) {
        this.count_attend = count_attend;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public List<EvoucherDetailModel> getEvouchers() {
        return evouchers;
    }

    public void setEvouchers(List<EvoucherDetailModel> evouchers) {
        this.evouchers = evouchers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public LixiShopBankTransactionMessage getMessage() {
//        return message;
//    }
//
//    public void setMessage(LixiShopBankTransactionMessage message) {
//        this.message = message;
//    }

    public long getPayment_expired() {
        return payment_expired;
    }

    public void setPayment_expired(long payment_expired) {
        this.payment_expired = payment_expired;
    }

    public String getPayment_link() {
        return payment_link;
    }

    public void setPayment_link(String payment_link) {
        this.payment_link = payment_link;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getReduce_point() {
        return reduce_point;
    }

    public void setReduce_point(long reduce_point) {
        this.reduce_point = reduce_point;
    }

    public String getShipping_info() {
        return shipping_info;
    }

    public void setShipping_info(String shipping_info) {
        this.shipping_info = shipping_info;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }
}
