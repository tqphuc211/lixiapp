package com.opencheck.client.home.account.activecode2.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.home.account.activecode.ActiveCodeActivity;
import com.opencheck.client.home.account.activecode2.ActiveCode2Activity;
import com.opencheck.client.home.account.activecode2.fragment.ConfirmSuccessFragment;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

/**
 * Created by Tony Tuan on 03/26/2018.
 */

public class VoucherController {
    private LixiActivity activity;
    private LinearLayout lnRoot;
    private View viewChild, iv_tick;
    private int position;
    private TextView tv_name, tv_code, tv_expire_date, tv_price, tv_timer;
    private ImageView iv_hinh;
    private RelativeLayout rlRoot;
    private RelativeLayout rl_item;
    private View v_line;
    private int size;
    private LixiShopEvoucherBoughtModel evoucherBoughtModel;
    private boolean isConfirm;

    public VoucherController(LixiActivity _activity, LixiShopEvoucherBoughtModel _evoucherBoughtModel, int _position, int _size, boolean _isConfirm, LinearLayout _lnRoot){
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.position = _position;
        this.size = _size;
        this.evoucherBoughtModel = _evoucherBoughtModel;
        this.isConfirm = _isConfirm;
        this.viewChild = View.inflate(activity, R.layout.e_voucher_item_layout, null);
        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initView(){
        tv_name = (TextView) viewChild.findViewById(R.id.tv_name);
        tv_expire_date = (TextView) viewChild.findViewById(R.id.tv_expire_date);
        tv_price = (TextView) viewChild.findViewById(R.id.tv_price);
        tv_timer = (TextView) viewChild.findViewById(R.id.tv_timer);
        tv_code = (TextView) viewChild.findViewById(R.id.tv_code);
        iv_hinh = (ImageView) viewChild.findViewById(R.id.iv_hinh);
        iv_tick = (View) viewChild.findViewById(R.id.iv_tick);
        v_line = (View) viewChild.findViewById(R.id.v_line);
        rl_item = (RelativeLayout) viewChild.findViewById(R.id.ll_item);

        rl_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LixiFragment l = ((ActiveCode2Activity) activity).getCurrentFragment();
                if(((ActiveCode2Activity) activity).getCurrentFragment() instanceof ConfirmSuccessFragment) {
                    Bundle bundle = new Bundle();
                    bundle.putLong(ConstantValue.EVOUCHER_ID, evoucherBoughtModel.getId());
                    Intent intent = new Intent(activity, ActiveCodeActivity.class);
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            }
        });

    }

    private void initData(){
        if (position == size - 1) {
            v_line.setVisibility(View.GONE);
        }
        if(!isConfirm) {
            iv_tick.setVisibility(View.VISIBLE);
            tv_timer.setVisibility(View.GONE);
        }
        else{
            iv_tick.setVisibility(View.GONE);
            tv_timer.setVisibility(View.VISIBLE);
        }
        if (evoucherBoughtModel.getProduct_cover_image() == null || evoucherBoughtModel.getProduct_cover_image().equals(""))
            ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_image().get(0), iv_hinh, LixiApplication.getInstance().optionsNomal);
        else
            ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_cover_image(), iv_hinh, LixiApplication.getInstance().optionsNomal);

        tv_name.setText(evoucherBoughtModel.getProduct_name());
        tv_price.setText(Helper.getVNCurrency(evoucherBoughtModel.getPrice()) + activity.getString(R.string.p));
        tv_code.setText(evoucherBoughtModel.getPin());
        tv_expire_date.setText(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_DATE, String.valueOf(Long.parseLong(evoucherBoughtModel.getExpired_time()) * 1000)));

    }
}
