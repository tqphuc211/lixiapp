package com.opencheck.client.home.product.gateway;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopTransactionDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.ApiEntity;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.dialogs.OptionDialog;
import com.opencheck.client.home.product.Control.OperateBase64;
import com.opencheck.client.home.product.model.TokenData;
import com.opencheck.client.models.lixishop.LixiShopBankTransactionResult;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

public class DialogTransactions extends LixiTrackingDialog {

    public DialogTransactions(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private boolean isChecked = false;

    private RelativeLayout rlRoot;
    private RelativeLayout rl_close;
    private RelativeLayout rl_refresh;
    private WebView webView;
    private String payment_url;
    private ProgressBar progressBar;

    private String provider = "";
    private String currentUrl = "";

    private boolean isWebViewLoadSuccess = false;

    boolean isSaveCard;
    TokenData tokenData;

    //region setup dialog
    private LixiShopTransactionDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopTransactionDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }


    public void setData(String payment_link, boolean isSaveCard, Long orderId) {
        this.isSaveCard = isSaveCard;
        payment_url = payment_link;
        currentUrl = payment_url;
        setListener();
        setViewData();
        startCountDownTimer();
        id = orderId;
        trackScreen();
    }

    private void startCountDownTimer() {
        CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long l) {
                if (l == 0) {
                    if (!isWebViewLoadSuccess) {
                        DialogTransactionTimeout dialogTransactionTimeout = new DialogTransactionTimeout(activity, false, false, false);
                        dialogTransactionTimeout.show();
                        dialogTransactionTimeout.setIDidalogEventListener(new DialogTransactionTimeout.IDidalogEvent() {
                            @Override
                            public void onOk(boolean success) {
                                if (success)
                                    dismiss();
                                else {
                                    webView.clearView();
                                    setViewData();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        };
        countDownTimer.start();

    }

    private void setListener() {
        rl_close.setOnClickListener(this);
        rl_refresh.setOnClickListener(this);
    }

    private void setViewData() {
        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setDomStorageEnabled(true);

        String mime = "text/html";
        String encoding = "utf-8";

//        String testHtml = "<p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><img src=\"http://picture.lixiapp.com/oc_web_editor/2017/10/10/b6da4462049113d350111ee1018b149051639d33.jpeg\" class=\"pull-left\" style=\"width: 100%;\"><span style=\"font-weight: 700\">Ngày phát hành: 19/9/2017</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Hạn order đợt 1: 25-27/8</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">Tình trạng: ORDER 7-15 NGÀY SAU PHÁT HÀNH LÀ CÓ HÀNG</span></span><span style=\"font-weight: 700\"></span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Giá trên là giá nhận trực tiếp tại HCM, Ship sẽ tính lúc các bạn order</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Đợt 1 mới có poster và có khả năng trúng special poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- Sau đợt 1 shop vẫn tiếp tục nhận order nhưng ko chắc chắn là còn poster</p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\"><span style=\"font-weight: 700\">Trọn bộ bao gồm:</span></p><p style=\"line-height: 24px;color: rgb(51, 51, 51)\">- 4 version: L.O.V.E (được chọn)- 1 CD + Photobook 100p- Mini Book (HYYH The Notes)- Random Photocard (có 28 mẫu tất cả)- Sticker Pack- Special Photocard (firstpress có khả năng trúng)<span data-mce-style=\"color: #ff0000;\" style=\"color: rgb(255, 0, 0)\"><span style=\"font-weight: 700\">- Quà firstpress (đợt 1): Poster</span></p>";
//        payment_url = "https://sandbox.payoo.com.vn/bank-simulate.php?vpc_AdditionData=990313&vpc_Amount=60000&vpc_Command=PAY&vpc_CurrencyCode=VND&vpc_Locale=VN&vpc_MerchTxnRef=PY7327000031&vpc_Merchant=OPPREPAID&vpc_OrderInfo=PY7327000031&vpc_TransactionNo=133088&vpc_Version=2&vpc_SecureHash=AAF5D28BAFD10023EE68DB9CC2E5868E4AD89DC4C9966EBD9B91C6625FFDD0CF&system=0&vpc_ReturnURL=https%3a%2f%2fnewsandbox.payoo.com.vn%2fv2%2fbank%2ffinish%3fiframe%3d0%26_token%3d89aa17ffdca0e9e04a5077f2b0a6a9ae%26bank%3dOCB";
//        webView.loadUrl(payment_url);
//        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(activity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                Toast.makeText(activity, "SSL error " + error , Toast.LENGTH_LONG).show();
//                super.onReceivedSslError(view, handler, error);
//                handler.proceed();
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        dismiss();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                currentUrl = url;
//                Log.d("SSSS", url);

                String state = getQuery(url, "state");
                if (state != null && state.equals("complete")) {
                    // thanh toán thành công
                    saveBankCard(getQuery(url, "token_data"));
                } else {
                    // khác
                }

                if (payment_url.equals(url))
                    return false;//Allow WebView to load url
                else {
//                    String rootUrl = getRootUrl(url);
                    String rootUrl = getPath(url);

                    if (rootUrl != null) {
                        if (rootUrl.contains(ApiEntity.PAYMENT_CALLBACK_PATH_NAPAS)) {
                            String urlParams = getUrlParams(url);
                            provider = getUrlProvider(url);
                            checkShoppingStatus(urlParams, "napas");
//                            webView.setWebViewClient(null);
                            return true;
                        }
                    } else {
                        return false;
                    }

                    int lastindex = rootUrl.lastIndexOf("/");
                    if (lastindex > 0)
                        rootUrl = rootUrl.substring(0, lastindex);

                    if (rootUrl.equals(ApiEntity.PAYMENT_CALLBACK_PATH)
                            || rootUrl.contains(ApiEntity.PAYMENT_CALLBACK_PATH_NAPAS)) {
                        String urlParams = getUrlParams(url);
                        provider = getUrlProvider(url);
                        checkShoppingStatus(urlParams);
//                        webView.setWebViewClient(null);
                        return true; //Indicates WebView to NOT load the url;
                    } else {
                        Log.d("Wrong redirect url", url);
                        return false;
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
//                Log.d("SSSS", url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hd.removeCallbacks(rRefresh);
                progressBar.setVisibility(View.GONE);
                isWebViewLoadSuccess = true;
                super.onPageFinished(view, url);
//                Log.d("SSSS", url);
            }
        });
        webView.loadUrl(payment_url);
        startTimeout();
    }

    private String getPath(String url) {
        try {
            Uri uri = Uri.parse(url);
            String protocol = uri.getScheme();
            String server = uri.getAuthority();
            String path = uri.getPath();
            return path;
        } catch (Exception e) {

        }
        return "";
    }

    private String getQuery(String url, String query) {
        String res = "";
        try {
            Uri uri = Uri.parse(url);
            res = uri.getQueryParameter(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private void saveBankCard(String token) {
        if (token == null || token.equals("")) {
            return;
        }
        String decodeToken = OperateBase64.decode(token);
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
        tokenData = (new Gson()).fromJson(decodeToken, TokenData.class);

        String bankCode = tokenData.getCard().getBank_code();
        ArrayList<TokenData> listTemp = new ArrayList<>();
        listTemp = appPreferenceHelper.getTokenBankCard(bankCode);
        if (listTemp != null) {
            for (int i = 0; i < listTemp.size(); i++) {
                if (checkCardExisted(tokenData.getCard().getNumber(), listTemp.get(i).getCard().getNumber())) {
                    iGetTokenData.onResult(null);
                    return;
                }
            }
        }

        if (isSaveCard) {
            appPreferenceHelper.setTokenBankCard(tokenData);
            iGetTokenData.onResult(tokenData);
        } else {
            iGetTokenData.onResult(null);
        }
    }

    private boolean checkCardExisted(String n1, String n2) {
        return n1.equals(n2);
    }

    private void findViews() {
        rlRoot = (RelativeLayout) findViewById(R.id.rlRoot);
        rl_close = (RelativeLayout) findViewById(R.id.rl_close);
        rl_refresh = (RelativeLayout) findViewById(R.id.rl_refresh);
        webView = (WebView) findViewById(R.id.wv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    private String getMessage(String ms, List<String> par) {
        String res = ms;

        if (par != null) {
            for (int i = 0; i < par.size(); i++) {
                String holder = "{" + String.valueOf(i) + "}";

                if (res.contains(holder)) {
                    res = res.replace(holder, "<b>" + par.get(i) + "</b>");
                }
            }
        }

        return res;
    }

    //endregion

    //region Handle Shopping
    private void checkShoppingStatus(String params, String... napas) {
        DataLoader.checkShoppingStatus(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    if (!isChecked) {
                        LixiShopBankTransactionResult transactionResult = (LixiShopBankTransactionResult) object;
                        handleTransactionResult(transactionResult);
                    }
                } else {
                    showErrorToast((String) object);
                }
                isChecked = true;
            }
        }, params + (napas.length > 0 ? "" : ("&provider=" + provider)), napas);
    }

    private void handleTransactionResult(LixiShopBankTransactionResult transactionResult) {
        String ms = transactionResult.getMessage().get(0).getText();
        if (!transactionResult.getStatus()) {
            if (eventListener != null)
                eventListener.onPayResult(false, false, false, transactionResult.getOrder_id(), transactionResult);
        } else {
            if (eventListener != null)
                eventListener.onPayResult(true, false, false, transactionResult.getOrder_id(), transactionResult);
        }

        dismiss();
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    //endregion

    //region get incoming url and its params

    private String getRootUrl(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(0, j);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }

    private String getUrlParams(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                return url.substring(i, url.length());
            }
        }
        return "";
    }


    private String getUrlProvider(String url) {
        int i;
        for (i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '?') {
                for (int j = i; j > 0; j--) {
                    if (url.charAt(j) == '/') {
                        return url.substring(j + 1, i);
                    }
                }
//                return url.substring(0, i);
            }
        }
        return "";
    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_close:
                DialogCancelTransaction dialogCancelTransaction = new DialogCancelTransaction(activity, false, false, false);
                dialogCancelTransaction.show();
                dialogCancelTransaction.setData(this);
                break;
            case R.id.rl_refresh:
                webView.loadUrl(currentUrl);
                break;
            default:
                break;
        }
    }

    //region Reload
    private int TIMEOUT = 30000;

    public void startTimeout() {
        try {
            hd.removeCallbacks(rRefresh);
        } catch (Exception ex) {
        }
        hd.postDelayed(rRefresh, TIMEOUT);
    }

    OptionDialog dialogReload;
    Handler hd = new Handler();
    Runnable rRefresh = new Runnable() {
        @Override
        public void run() {
            showReload();
        }
    };

    public void showReload() {
        if (dialogReload != null && dialogReload.isShowing())
            return;
        int color = activity.getResources().getColor(R.color.app_violet);
        dialogReload = new OptionDialog(activity, false, true, false);
        dialogReload.setDismissAble(false);
        dialogReload.show();
        dialogReload.setData("Lỗi kết nối", "Có lỗi trong quá trình kết nối cổng thanh toán, xin kiểm tra đường truyền và thử lại...",
                "Bỏ qua", "Thử lại", color, color, true, false);
        dialogReload.setIDidalogEventListener(new OptionDialog.IDialogEvent() {
            @Override
            public void onLeftClick() {
                dialogReload.dismiss();
            }

            @Override
            public void onRightClick() {
                dialogReload.dismiss();
                webView.loadUrl(currentUrl);
                startTimeout();
            }
        });
    }

    //endregion

    //region dialog callback

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public interface IDidalogEvent {
        void onPayResult(boolean success, boolean isCancel, boolean isTimeout, String orderId, LixiShopBankTransactionResult transactionResult);
    }

    protected DialogTransactions.IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            DialogTransactions.IDidalogEvent listener) {
        eventListener = listener;
    }

    public interface IGetTokenData {
        void onResult(TokenData tokenData);
    }

    private IGetTokenData iGetTokenData;

    public void getTokenData(IGetTokenData iGetTokenData) {
        this.iGetTokenData = iGetTokenData;
    }
    //endregion
}
