package com.opencheck.client.home.rating.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityRatingBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.delivery.libs.RatingBar;
import com.opencheck.client.home.rating.adapter.RatingGridOptionAdapter;
import com.opencheck.client.home.rating.dialog.NotificationDialog;
import com.opencheck.client.home.rating.model.RatingOption;
import com.opencheck.client.home.rating.model.RatingResponse;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class RatingActivity extends LixiActivity {

    // View
    private ImageView imgLogo;
    private TextView txtVoucherName;
    private EditText edtComment;
    private RatingBar ratingBar;
    private GridView recOption;
    private RelativeLayout relBack;
    private ConstraintLayout constraintDone;
    private FrameLayout frameComment;
    private ProgressBar progressBar;

    // Var
    private EvoucherDetailModel evoucherDetailModel;
    private RatingGridOptionAdapter adapter;

    private ActivityRatingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_rating);
        Intent intent = getIntent();
        if (intent != null) {
            evoucherDetailModel = (EvoucherDetailModel) intent.getSerializableExtra("rating product");
        }
        initView();
        getOption();
    }

    private void initView() {
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        txtVoucherName = (TextView) findViewById(R.id.txtVoucherName);
        edtComment = (EditText) findViewById(R.id.edtComment);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        recOption = (GridView) findViewById(R.id.recOption);
        relBack = (RelativeLayout) findViewById(R.id.rlClose);
        frameComment = (FrameLayout) findViewById(R.id.frameComment);
        constraintDone = (ConstraintLayout) findViewById(R.id.constraintDone);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        operateRatingBar();
        ratingBar.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar ratingBar, int preCount, int curCount) {
                operateRatingBar();
            }
        });

        ImageLoader.getInstance().displayImage(evoucherDetailModel.getMerchant_info().getLogo(), imgLogo);
        txtVoucherName.setText(evoucherDetailModel.getName());

        constraintDone.setOnClickListener(this);
        relBack.setOnClickListener(this);
    }

    private void getOption() {
        DataLoader.getRatingOption(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    final ArrayList<RatingOption> listOption = (ArrayList<RatingOption>) object;
                    adapter = new RatingGridOptionAdapter(activity, listOption);
                    recOption.setAdapter(adapter);
                    adapter.addOnItemClick(new OnItemClickListener() {
                        @Override
                        public void onItemClickListener(View view, int pos, String action, Object object, Long count) {
                            frameComment.setVisibility(View.VISIBLE);
                            recOption.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }, 1);
    }

    private void operateRatingBar() {
        if (ratingBar.getCount() < 4) {
            if (adapter != null) {
                if (adapter.getOption() != null) {
                    if (adapter.getOption().getKey().equals("OTHER_OPTION")) {
                        adapter.resetOption();
                    }
                }
            }
            frameComment.setVisibility(View.GONE);
            recOption.setVisibility(View.VISIBLE);
        } else {
            frameComment.setVisibility(View.VISIBLE);
            recOption.setVisibility(View.GONE);
        }
    }

    private void postRating() {
        JsonObject jsonObject = new JsonObject();
        RatingOption option = adapter.getOption();
        if (ratingBar.getCount() < 4) {
            if (option != null) {
                if (option.getKey().equals("OTHER_OPTION")) {
                    if (edtComment.getText().toString().equals("")) {
                        showNotificationDialog(LanguageBinding.getString(R.string.notification_label, activity), "Bạn chưa nhập đánh giá");
                        return;
                    }
                }
                jsonObject.addProperty("comment", edtComment.getText().toString());
                jsonObject.addProperty("product_id", evoucherDetailModel.getProduct());
                jsonObject.addProperty("rating", ratingBar.getCount());
                jsonObject.addProperty("rating_option_id", option.getId());
            } else {
                // chưa chọn
                showNotificationDialog(LanguageBinding.getString(R.string.notification_label, activity), "Bạn chưa chọn đánh giá");
                return;
            }
        } else {
            jsonObject.addProperty("comment", edtComment.getText().toString().trim());
            jsonObject.addProperty("product_id", evoucherDetailModel.getProduct());
            jsonObject.addProperty("rating", ratingBar.getCount());
        }

        progressBar.setVisibility(View.VISIBLE);
        DataLoader.postRating(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                progressBar.setVisibility(View.GONE);
                if (isSuccess) {
                    RatingResponse response = (RatingResponse) object;
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            RatingActivity.this.setResult(RESULT_OK, null);
//                            RatingActivity.this.finish();
//                        }
//                    }, 2000);
                    RatingActivity.this.setResult(RESULT_OK, null);
                    RatingActivity.this.finish();
                } else {
                    // rating thất bại
                    showNotificationDialog(LanguageBinding.getString(R.string.notification_label, activity), "Đánh giá thất bại. Vui lòng đánh giá lại");
                }
            }
        }, jsonObject);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.constraintDone:
                postRating();
                break;
            case R.id.rlClose:
                finish();
                break;
        }
    }

    private void showNotificationDialog(String title, String content) {
        NotificationDialog dialog = new NotificationDialog(activity, false, false, false);
        dialog.show();
        dialog.setData(title, content);
    }
}
