package com.opencheck.client.home.account.evoucher.Model;

public class ActiveCodeApiBody {
//    "evoucher_ids": "string"

    private String evoucher_ids;

    public String getEvoucher_ids() {
        return evoucher_ids;
    }

    public void setEvoucher_ids(String evoucher_ids) {
        this.evoucher_ids = evoucher_ids;
    }
}
