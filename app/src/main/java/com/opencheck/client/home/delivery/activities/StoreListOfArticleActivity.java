package com.opencheck.client.home.delivery.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.custom.FlingBehavior;
import com.opencheck.client.databinding.ActivityCollectionDetailBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.PlaceAdapter;
import com.opencheck.client.home.delivery.libs.SpacesItemDecoration;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class StoreListOfArticleActivity extends LixiActivity {

    private PlaceAdapter mAdapter;
    private ArrayList<StoreOfCategoryModel> storeList;
    private int page = 1;
    private boolean isLoading = false;
    private long collectionId;
    private long storeId;
    private String title = "", imageLink = "", source;
    private boolean noRemain = false;
    private int height = 0;
    private DeliAddressModel mLocation;

    private ActivityCollectionDetailBinding mBinding;

    public final static String TITLE = "TITLE";
    public final static String IMAGE_LINK = "IMAGE_LINK";
    public final static String ID = "ID";
    public final static String OBJECT_ID = "OBJECT_ID";

    public static void startArticleActivity(Activity activity, long storeId, long collectionId, String imageLink, String title, String source) {
        Bundle bundle = new Bundle();
        bundle.putLong(OBJECT_ID, storeId);
        bundle.putLong(ID, collectionId);
        bundle.putString(IMAGE_LINK, imageLink);
        bundle.putString(TITLE, title);
        bundle.putString(GlobalTracking.SOURCE, source);
        Intent intent = new Intent(activity, StoreListOfArticleActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static Intent getCallingIntent(Context context, long storeId, long collectionId, String imageLink, String title, String source) {
        Bundle bundle = new Bundle();
        bundle.putLong(OBJECT_ID, storeId);
        bundle.putLong(ID, collectionId);
        bundle.putString(IMAGE_LINK, imageLink);
        bundle.putString(TITLE, title);
        bundle.putString(GlobalTracking.SOURCE, source);
        Intent intent = new Intent(context, StoreListOfArticleActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_collection_detail);
        title = getIntent().getExtras().getString("TITLE", "");
        imageLink = getIntent().getExtras().getString("IMAGE_LINK", "");
        collectionId = getIntent().getExtras().getLong("ID", 0L);
        storeId = getIntent().getExtras().getLong("OBJECT_ID", 0L);
        source = getIntent().getExtras().getString(GlobalTracking.SOURCE);
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        GlobalTracking.trackShowDetail(
                TrackingConstant.ContentType.DELI_COLLECTION,
                collectionId,
                TrackingConstant.Currency.VND,
                0,
                source
        );
        initViews();
        getStoreOfArticle();
    }

    private void initViews() {
        setSupportActionBar(mBinding.toolbar);
        int space = (int) getResources().getDimension(R.dimen.value_8);
        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(1, space, true);
        ViewGroup.LayoutParams layoutParams = mBinding.appbar.getLayoutParams();
        layoutParams.height = 9 * activity.widthScreen / 16;
        mBinding.appbar.setLayoutParams(layoutParams);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mBinding.appbar.getLayoutParams();
        params.setBehavior(new FlingBehavior(getBaseContext(), null));

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mBinding.ivBanner.getLayoutParams();
        lp.setMargins(0, getStatusBarHeight(), 0, 0);

        if (title.isEmpty() || imageLink.isEmpty()) {
            getArticleDetail();
        } else {
            mBinding.tvTitle.setText(title);
            ImageLoader.getInstance().displayImage(imageLink, mBinding.ivBanner, LixiApplication.getInstance().optionsNomal);
        }

        mBinding.rlBack.setOnClickListener(this);
        mBinding.ivShare.setOnClickListener(this);

        mBinding.swr.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                isLoading = false;
                noRemain = false;
                if (mAdapter != null) {
                    storeList.clear();
                    mAdapter.notifyDataSetChanged();
                }
                getStoreOfArticle();
            }
        });

        mBinding.rv.setLayoutManager(new GridLayoutManager(activity, 1));
//        mBinding.rv.addItemDecoration(spacesItemDecoration);

        mBinding.appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());
                mBinding.ivBanner.setAlpha(1 - (offsetAlpha * -1));
                Log.d("ddd", verticalOffset + " " + appBarLayout.getTotalScrollRange());
                if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) < height + 10) {
                    //collapse
                    mBinding.tvTitle.setText(title);
                    mBinding.tvTitle.setVisibility(View.VISIBLE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 172, 30, 161));
                    mBinding.ivShare.setColorFilter(Color.argb(255, 172, 30, 161));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(appBarLayout.getTotalScrollRange() - (height + 10));
                } else {
                    //expands
                    mBinding.tvTitle.setVisibility(View.GONE);
                    mBinding.ivBack.setColorFilter(Color.argb(255, 255, 255, 255));
                    mBinding.ivShare.setColorFilter(Color.argb(255, 255, 255, 255));
                    mBinding.collapsing.setScrimVisibleHeightTrigger(5);
                }
            }
        });
    }

    private void setAdapter() {
        mAdapter = new PlaceAdapter(activity, storeList);
        mBinding.rv.setAdapter(mAdapter);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void getStoreOfArticle() {
        try {
            isLoading = true;
            mBinding.swr.setRefreshing(true);
            DeliDataLoader.getStoreOfArticle(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    mBinding.swr.setRefreshing(false);
                    if (isSuccess) {
                        if (page == 1) {
                            storeList = (ArrayList<StoreOfCategoryModel>) object;
                            if (storeList == null || storeList.size() == 0) {
                                mBinding.lnNoResult.setVisibility(View.VISIBLE);
                                noRemain = true;
                                return;
                            }
                            setAdapter();
                        } else {
                            ArrayList<StoreOfCategoryModel> list = (ArrayList<StoreOfCategoryModel>) object;
                            if (storeList == null || list == null || list.size() == 0) {
                                noRemain = true;
                                return;
                            }
                            storeList.addAll(list);
                            mAdapter.notifyDataSetChanged();
                        }
                        page++;
                        isLoading = false;

                        try {
                            if (storeId > 0) {
                                StoreDetailActivity.startStoreDetailActivity(activity, storeId, TrackingConstant.ContentSource.DELI_COLLECTION);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        mBinding.lnNoResult.setVisibility(View.VISIBLE);
                        Helper.showLog((String) object);
                    }
                }
            }, page, collectionId, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void getArticleDetail() {
        DeliDataLoader.getDetailArticle(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    ArticleModel model = (ArticleModel) object;
                    title = model.getName();
                    mBinding.tvTitle.setText(title);
                    ImageLoader.getInstance().displayImage(model.getCover_link(), mBinding.ivBanner, LixiApplication.getInstance().optionsNomal);
                }
            }
        }, 1, collectionId);
    }

    private boolean isRvBottom() {
        if (storeList == null || storeList.size() == 0)
            return false;
        int totalItemCount = storeList.size();
        int lastVisibleItem = ((LinearLayoutManager) mBinding.rv.getLayoutManager()).findLastCompletelyVisibleItemPosition();

        int record_per_page = DeliDataLoader.record_per_page;
        return (lastVisibleItem >= totalItemCount - 7 && storeList.size() >= record_per_page);
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.DELIVERY_COLLECTION;
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.rlBack) {
            finish();
        }
        if (view == mBinding.ivShare) {
            Helper.shareLink(activity, "https://li-xi.app.link/article?id=" + collectionId, false);
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        if(getResources() == null) {
            return result;
        }
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Socket mSocket = LixiApplication.getInstance().getSocket();
//        new LixiTrackingHelper(activity, mSocket).createNavigateTrackingSocket("detail-article", collectionId);
//    }
}
