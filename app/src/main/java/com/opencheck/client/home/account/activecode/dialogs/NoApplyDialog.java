package com.opencheck.client.home.account.activecode.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.NoApplyStoreDialogBinding;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;

/**
 * Created by Tony Tuan on 04/11/2018.
 */

public class NoApplyDialog extends LixiDialog {

    private TextView tv_name_store, tv_done;
    private CircleImageView iv_merchant;

    public NoApplyDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private NoApplyStoreDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = NoApplyStoreDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
    }

    private void findViews() {
        tv_name_store = (TextView) findViewById(R.id.tv_name_store);
        tv_done = (TextView) findViewById(R.id.tv_done);
        iv_merchant = (CircleImageView) findViewById(R.id.iv_merchant);

        tv_done.setOnClickListener(this);
    }

    public void setData(StoreApplyModel storeModel, EvoucherDetailModel evoucherModel) {
        tv_name_store.setText(storeModel.getName());
        ImageLoader.getInstance().displayImage(evoucherModel.getMerchant_info().getLogo(), iv_merchant, LixiApplication.getInstance().optionsNomal);
    }

    @Override
    public void onClick(View v) {
        if (v == tv_done)
            dismiss();
    }
}
