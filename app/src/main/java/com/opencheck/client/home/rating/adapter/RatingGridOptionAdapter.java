package com.opencheck.client.home.rating.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.RowRatingOptionBinding;
import com.opencheck.client.home.delivery.interfaces.OnItemClickListener;
import com.opencheck.client.home.rating.control.ColorFilterUtils;
import com.opencheck.client.home.rating.model.RatingOption;

import java.util.ArrayList;

public class RatingGridOptionAdapter extends BaseAdapter {
    private LixiActivity activity;
    private ArrayList<RatingOption> listOption;

    public RatingGridOptionAdapter(LixiActivity activity, ArrayList<RatingOption> listOption) {
        this.activity = activity;
        this.listOption = listOption;
    }

    private int checkedItem = -1;
    private int lastChecked = -1;
    private RatingOption option = null;

    @Override
    public int getCount() {
        return listOption != null ? listOption.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            RowRatingOptionBinding rowRatingOptionBinding =
                    RowRatingOptionBinding.inflate(LayoutInflater.from(activity),
                            null, false);
            view = rowRatingOptionBinding.getRoot();
        }

        final CircleImageView imgOption = (CircleImageView) view.findViewById(R.id.imgOption);
        final TextView txtContent = (TextView) view.findViewById(R.id.txtContent);
        final FrameLayout frameOption = (FrameLayout) view.findViewById(R.id.frameOption);

        final RatingOption option = listOption.get(i);
        ImageLoader.getInstance().displayImage(option.getImage(), imgOption);
        txtContent.setText(option.getValue());
        if (checkedItem == i) {
            imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_VIOLET));
            frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating_violet));
            txtContent.setTextColor(activity.getResources().getColor(R.color.app_violet));
        } else {
            imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_DARK));
            frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating));
            txtContent.setTextColor(Color.parseColor("#AEAEAE"));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingGridOptionAdapter.this.option = listOption.get(i);
                if (checkedItem == i) {
                    return;
                }

                if (option.getKey().equals("OTHER_OPTION")) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClickListener(null, i, null, null, 0L);
                    }
                    return;
                }

                checkedItem = i;
                imgOption.setColorFilter(ColorFilterUtils.getFilter(ColorFilterUtils.FILTER_VIOLET));
                frameOption.setBackground(activity.getResources().getDrawable(R.drawable.circle_user_rating_violet));
                txtContent.setTextColor(activity.getResources().getColor(R.color.app_violet));
                if (lastChecked >= 0) {
                    notifyDataSetChanged();
                }
                lastChecked = checkedItem;
            }
        });
        return view;
    }

    public RatingOption getOption() {
        return option;
    }

    public void resetOption() {
        this.option = null;
    }

    private OnItemClickListener onItemClickListener;

    public void addOnItemClick(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
