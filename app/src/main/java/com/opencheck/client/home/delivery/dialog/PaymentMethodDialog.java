package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ChoosePaymentMethodDialogBinding;
import com.opencheck.client.home.delivery.adapter.PaymentAdapter;
import com.opencheck.client.home.delivery.model.PaymentModel;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class PaymentMethodDialog extends LixiDialog {
    private PaymentAdapter mAdapter;
    private ArrayList<PaymentModel> mList;
    private int currentPos = -1;

    public PaymentMethodDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackground, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackground, _titleBar);
    }

    private ChoosePaymentMethodDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.choose_payment_method_dialog, null, false);
        setContentView(mBinding.getRoot());
        mBinding.rlDismiss.setOnClickListener(this);
        setAdapter();
    }

    public void setData(ArrayList<PaymentModel> list, String code) {
        this.mList = list;

        mAdapter.setPaymentList(mList);
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getCode() != null && mList.get(i).getCode().equals(code)) {
                currentPos = i;
                break;
            }
        }
        if (currentPos != -1)
            toggle(currentPos);
    }

    private void setAdapter() {
        mAdapter = new PaymentAdapter(activity);
        mBinding.rcvMethod.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mBinding.rcvMethod.setAdapter(mAdapter);
        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (mList.get(pos).getState().equals("active")) {
                    toggle(pos);
                    eventListener.onOk(mList.get(pos));
                    dismiss();
                } else {
                    Toast.makeText(activity, LanguageBinding.getString(R.string.method_not_support, activity), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void toggle(int pos) {
        if (mList.get(pos).isSelected()) {
            return;
        }
        mList.get(pos).setSelected(!mList.get(pos).isSelected());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.rlDismiss)
            dismiss();
    }

    public interface IDialogEvent {
        void onOk(PaymentModel paymentModel);
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(IDialogEvent listener) {
        eventListener = listener;
    }
}
