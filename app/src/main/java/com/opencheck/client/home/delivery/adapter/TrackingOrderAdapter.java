package com.opencheck.client.home.delivery.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.OrderTrackingItemLayoutBinding;
import com.opencheck.client.home.delivery.activities.OrderTrackingActivity;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class TrackingOrderAdapter extends RecyclerView.Adapter<TrackingOrderAdapter.ViewHolder> {

    private LixiActivity activity;
    private ArrayList<DeliOrderModel> list;

    public TrackingOrderAdapter(LixiActivity activity, ArrayList<DeliOrderModel> list) {
        this.activity = activity;
        this.list = list;
    }

    public void setOrderData(ArrayList<DeliOrderModel> data) {
        if (data == null || data.size() == 0)
            return;
        this.list = data;
        notifyDataSetChanged();
    }

    public void clearOrderData() {
        this.list.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        OrderTrackingItemLayoutBinding orderTrackingItemLayoutBinding =
                OrderTrackingItemLayoutBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(orderTrackingItemLayoutBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        DeliOrderModel model = list.get(position);
        holder.root.getLayoutParams().width = activity.widthScreen;
        holder.tv_name_store.setText(model.getStore_name());
        if (model.getCurrent_tracking() != null && model.getCurrent_tracking().getStart_date() != null && model.getCurrent_tracking().getEnd_date() != null)
            holder.tv_time.setText(Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(model.getCurrent_tracking().getStart_date())) + " - " + Helper.getFormatDateISO(ConstantValue.FORMAT_TIME_TRACKING, String.valueOf(model.getCurrent_tracking().getEnd_date())));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView iv;
        private TextView tv_name_store, tv_time;
        private RelativeLayout root;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_name_store = itemView.findViewById(R.id.tv_name_store);
            iv = itemView.findViewById(R.id.iv);
            root = itemView.findViewById(R.id.root);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putString("uuid", list.get(getAdapterPosition()).getUuid().substring(list.get(getAdapterPosition()).getUuid().indexOf("O") + 1,
                            list.get(getAdapterPosition()).getUuid().length()));
                    Intent intent = new Intent(activity, OrderTrackingActivity.class);
                    intent.putExtras(data);
                    activity.startActivity(intent);
                }
            });
        }
    }
}
