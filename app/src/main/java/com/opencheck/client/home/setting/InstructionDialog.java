package com.opencheck.client.home.setting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiInstructionDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.FAQModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class InstructionDialog extends LixiDialog implements   AdapterView.OnItemClickListener{
    public InstructionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    //region Layout Variable
    protected RelativeLayout rl_dismiss;

    private LinearLayout ll_root_popup_menu;
    private LinearLayout ll_faq;
    private RelativeLayout rlTop;
    private RelativeLayout rl_back;
    private TextView tvTitle;
    //endregion

    //region Logic Variable
    private ArrayList<FAQModel> listItemFAQ;

    //endregion


    //region Processing UI
    private LixiInstructionDialogBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiInstructionDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findView();
        setData();
    }


    public void findView() {
        ll_root_popup_menu = (LinearLayout) findViewById(R.id.ll_root_popup_menu);
        ll_faq = (LinearLayout) findViewById(R.id.ll_faq);
        rlTop = (RelativeLayout) findViewById(R.id.rlTop);
        rlTop = (RelativeLayout) findViewById(R.id.rlTop);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        rl_back.setOnClickListener(this);
    }

    //endregion

    //region Handle UI Logic
    private void setData() {
        getFAQ();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }


    //endregion

    //region Handle background processing

    public void getFAQ() {
        if (listItemFAQ == null || listItemFAQ.size() == 0) {
            DataLoader.getFAQ(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    if (isSuccess) {
                        listItemFAQ = (ArrayList<FAQModel>) object;
                        ll_faq.removeAllViews();
                        for (int i = 0; i < listItemFAQ.size(); i++) {
                            FAQModel dto = listItemFAQ.get(i);
                            new AddFAQController(activity, InstructionDialog.this, dto.getQuestion(), dto.getAnswer(), i, listItemFAQ.size(), ll_faq);
                        }
                    } else {
                    }
                }
            });
        } else {
            if (ll_faq.getChildCount() == listItemFAQ.size())
                return;
            ll_faq.removeAllViews();
            for (int i = 0; i < listItemFAQ.size(); i++) {
                FAQModel dto = listItemFAQ.get(i);
                new AddFAQController(activity, InstructionDialog.this, dto.getQuestion(), dto.getAnswer(), i, listItemFAQ.size(), ll_faq);
            }
        }
    }
    //endregion


}
