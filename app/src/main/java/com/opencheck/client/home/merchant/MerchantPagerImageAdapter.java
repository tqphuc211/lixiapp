package com.opencheck.client.home.merchant;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ItemImageMerchantDetailLayoutBinding;
import com.opencheck.client.models.merchant.ProductImageModel;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class MerchantPagerImageAdapter extends PagerAdapter {


    private ArrayList<ProductImageModel> listItem;
    private Activity activity;
    private final LayoutInflater inflater;

    public MerchantPagerImageAdapter(Activity _activity, ArrayList<ProductImageModel> _listItem) {
        this.activity = _activity;
        this.listItem = _listItem;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public void destroyItem(final View container, final int position, final Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public void finishUpdate(final View container) {
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object instantiateItem(final View view, final int position) {
        ItemImageMerchantDetailLayoutBinding detailLayoutBinding =
                ItemImageMerchantDetailLayoutBinding.inflate(inflater, null, false);
        final View convertView = detailLayoutBinding.getRoot();

        RelativeLayout rlItem = (RelativeLayout) convertView.findViewById(R.id.rlItem);
        final ImageView imgNone = (ImageView) convertView.findViewById(R.id.imgNone);
        final ImageView imgItem = (ImageView) convertView.findViewById(R.id.imgItem);

        imgItem.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageLoader.getInstance().displayImage(listItem.get(position).getImage_medium(), imgNone, LixiApplication.getInstance().optionsNomal, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                imgNone.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null) {
                    imgItem.setImageBitmap(loadedImage);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        ((ViewPager) view).addView(convertView, 0);
        return convertView;
    }


    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(final Parcelable state, final ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(final View container) {
    }

}
