package com.opencheck.client.home.account.evoucher.Adapter;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.custom.SelectableRoundedImageView;
import com.opencheck.client.databinding.RowDetailGroupVoucherBinding;
import com.opencheck.client.databinding.RowListGroupVoucherBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.evoucher.Model.ActiveCodeApiBody;
import com.opencheck.client.home.account.evoucher.Model.EVoucherUnused;
import com.opencheck.client.home.account.evoucher.Model.GroupCode;
import com.opencheck.client.home.history.detail.GeneralQRCodeAndCropTask;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

public class MultiActiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int DETAIL = 0;
    private final int LIST_GROUP = 1;
    private final String UN_ACTIVE_ALL = "----------";

    private LixiActivity activity;
    private ArrayList<EVoucherUnused> listEvoucherUnused;
    private GroupCode groupCode;
    private EvoucherDetailModel evoucherDetailModel;

    DetailViewHolder detailViewHolder;
    ListVoucherViewHolder listVoucherViewHolder;

    private static final String CAN_ACTIVE = "can_active";
    private static final String UN_CHECKED = "un checked";
    private static final String WAITING = "waiting";

    public MultiActiveAdapter(LixiActivity activity, ArrayList<EVoucherUnused> listEvoucherUnused, GroupCode groupCode, EvoucherDetailModel evoucherDetailModel) {
        this.activity = activity;
        this.listEvoucherUnused = listEvoucherUnused;
        this.groupCode = groupCode;
        this.evoucherDetailModel = evoucherDetailModel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case DETAIL:
                RowDetailGroupVoucherBinding rowDetailGroupVoucherBinding =
                        RowDetailGroupVoucherBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                return new DetailViewHolder(rowDetailGroupVoucherBinding.getRoot());
            case LIST_GROUP:
                RowListGroupVoucherBinding rowListGroupVoucherBinding =
                        RowListGroupVoucherBinding.inflate(
                                LayoutInflater.from(parent.getContext()), parent, false);
                rowListGroupVoucherBinding.executePendingBindings();
                return new ListVoucherViewHolder(rowListGroupVoucherBinding.getRoot());
        }
        return null;
    }

    public void unCheckAll() {

        for (int i = 0; i < listEvoucherUnused.size(); i++) {
            listEvoucherUnused.get(i).setState(UN_CHECKED);
        }

        ActiveCodeApiBody activeCodeApiBody = new ActiveCodeApiBody();
        activeCodeApiBody.setEvoucher_ids("");
        loadGroupCode(activeCodeApiBody);

        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == DETAIL) {
            // layout detail
            detailViewHolder = (DetailViewHolder) holder;
            showDataDetail(detailViewHolder);
        }

        if (holder.getItemViewType() == LIST_GROUP) {
            // layout list group
            listVoucherViewHolder = (ListVoucherViewHolder) holder;
            showDataList(listVoucherViewHolder, position);
        }
    }

    private void showDataDetail(final DetailViewHolder holder) {
        holder.txtTotalStore.setText("Voucher khả dụng tại cửa hàng(" + listEvoucherUnused.size() + ")");

        holder.txtCode.setText(groupCode.getGroup_uuid());
        ImageLoader.getInstance().displayImage(evoucherDetailModel.getMerchant_info().getLogo(), holder.imgLogo);
        new GeneralQRCodeAndCropTask(activity, groupCode.getGroup_uuid(), holder.imgQR, new ItemClickListener() {
            @Override
            public void onItemCLick(int pos, String action, Object object, Long count) {
                if (pos == 1) {
                    holder.imgQR.setImageBitmap((Bitmap) object);
                }
            }
        });
        long count = 0;
        long total = 0;
        for (int i = 0; i < listEvoucherUnused.size(); i++) {
            if (listEvoucherUnused.get(i).getState().equals(CAN_ACTIVE)) {
                count++;
                total = total + listEvoucherUnused.get(i).getPrice();
            }
        }
        holder.txtMulti.setText("x" + count);
        holder.txtTotal.setText(Helper.getVNCurrency(total) + activity.getString(R.string.p));
    }

    private void showDataList(final ListVoucherViewHolder holder, final int position) {
        final EVoucherUnused e = listEvoucherUnused.get(position - 1);
        holder.imgChecked.setVisibility(View.VISIBLE);
        holder.txtConfirming.setVisibility(View.INVISIBLE);

        holder.txtName.setText(e.getName());
        holder.txtExpried.setText(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_Y,
                String.valueOf(e.getExpired_time() * 1000)));
        holder.txtPrice.setText(Helper.getVNCurrency(e.getPrice()) + activity.getString(R.string.p_under));
        ImageLoader.getInstance().displayImage(e.getProduct_cover_image(), holder.imgPic);
        if (e.isSend_pin()) {
            holder.txtCode.setText(e.getPin());
            holder.txtCode.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        } else {
            holder.txtCode.setText("(Quét QR để kích hoạt mã)");
            holder.txtCode.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
        }

        if (e.getState().equals(CAN_ACTIVE)) {
            holder.imgChecked.setVisibility(View.VISIBLE);
            holder.txtConfirming.setVisibility(View.INVISIBLE);
            holder.txtPrice.setTextColor(activity.getResources().getColor(R.color.green_price));
            holder.itemView.setClickable(true);
        } else {
            if (e.getState().equals(WAITING)) {
                holder.imgChecked.setVisibility(View.INVISIBLE);
                holder.txtConfirming.setVisibility(View.VISIBLE);
                holder.txtPrice.setTextColor(activity.getResources().getColor(R.color.color_gray));
                holder.itemView.setClickable(false);
            } else {
                holder.imgChecked.setVisibility(View.INVISIBLE);
                holder.txtConfirming.setVisibility(View.INVISIBLE);
                holder.txtPrice.setTextColor(activity.getResources().getColor(R.color.green_price));
                holder.itemView.setClickable(true);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (e.getState().equals(UN_CHECKED)) {
                    // uncheck
                    e.setState(CAN_ACTIVE);
                    holder.imgChecked.setVisibility(View.VISIBLE);
//                    holder.txtConfirming.setVisibility(View.INVISIBLE);
                } else {
                    if (e.getState().equals(CAN_ACTIVE)) {
                        // check
                        e.setState(UN_CHECKED);
                        holder.imgChecked.setVisibility(View.INVISIBLE);
//                    holder.txtConfirming.setVisibility(View.VISIBLE);
                    } else {
                        return;
                    }
                }

                long count = 0;
                long total = 0;

                listEvoucherUnused.remove(position - 1);
                listEvoucherUnused.add(position - 1, e);
                String strList = "";
                for (int i = 0; i < listEvoucherUnused.size(); i++) {
                    if (listEvoucherUnused.get(i).getState().equals(CAN_ACTIVE)) {
                        strList = strList + listEvoucherUnused.get(i).getId() + " ";
                        count++;
                        total = total + listEvoucherUnused.get(i).getPrice();
                    }
                }

                ActiveCodeApiBody activeCodeApiBody = new ActiveCodeApiBody();
                activeCodeApiBody.setEvoucher_ids(strList.trim().replace(" ", ","));
                loadGroupCode(activeCodeApiBody);

                detailViewHolder.txtMulti.setText("x" + count);
                detailViewHolder.txtTotal.setText(Helper.getVNCurrency(total) + activity.getString(R.string.p));
            }
        });
    }

    private void loadGroupCode(ActiveCodeApiBody activeCodeApiBody) {
        DataLoader.getActiveCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    groupCode = (GroupCode) object;
                    detailViewHolder.txtCode.setText(groupCode.getGroup_uuid());
                    new GeneralQRCodeAndCropTask(activity, groupCode.getGroup_uuid(), detailViewHolder.imgQR, new ItemClickListener() {
                        @Override
                        public void onItemCLick(int pos, String action, Object object, Long count) {
                            if (pos == 1) {
                                detailViewHolder.imgQR.setImageBitmap((Bitmap) object);
                            }
                        }
                    });
                } else {
                    detailViewHolder.txtCode.setText("");
                    new GeneralQRCodeAndCropTask(activity, UN_ACTIVE_ALL, detailViewHolder.imgQR, new ItemClickListener() {
                        @Override
                        public void onItemCLick(int pos, String action, Object object, Long count) {
                            if (pos == 1) {
                                detailViewHolder.imgQR.setImageBitmap((Bitmap) object);
                            }
                        }
                    });
                }
            }
        }, groupCode.getId(), activeCodeApiBody);
    }

    @Override
    public int getItemCount() {
        if (listEvoucherUnused == null) {
            return 1;
        }
        return listEvoucherUnused.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return DETAIL;
        } else {
            return LIST_GROUP;
        }
    }

    public class DetailViewHolder extends RecyclerView.ViewHolder {
        SelectableRoundedImageView imgQR;
        TextView txtCode, txtTotalStore, txtMulti, txtTotal;
        CircleImageView imgLogo;

        public DetailViewHolder(View itemView) {
            super(itemView);
            imgQR = (SelectableRoundedImageView) itemView.findViewById(R.id.iv_qr);
            imgLogo = (CircleImageView) itemView.findViewById(R.id.img_logo);
            txtCode = (TextView) itemView.findViewById(R.id.tv_code);
            txtTotalStore = (TextView) itemView.findViewById(R.id.txt_total_store);
            txtMulti = (TextView) itemView.findViewById(R.id.txt_multi);
            txtTotal = (TextView) itemView.findViewById(R.id.txt_total);
        }
    }

    public class ListVoucherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgPic, imgChecked;
        TextView txtName, txtCode, txtExpried, txtPrice, txtConfirming;

        public ListVoucherViewHolder(View itemView) {
            super(itemView);
            imgPic = (ImageView) itemView.findViewById(R.id.iv_hinh);
            imgChecked = (ImageView) itemView.findViewById(R.id.img_check_purple);
            txtName = (TextView) itemView.findViewById(R.id.tv_name);
            txtCode = (TextView) itemView.findViewById(R.id.tv_code);
            txtPrice = (TextView) itemView.findViewById(R.id.tv_price);
            txtExpried = (TextView) itemView.findViewById(R.id.tv_expire_date);
            txtConfirming = (TextView) itemView.findViewById(R.id.txt_confirming);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
