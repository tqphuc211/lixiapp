package com.opencheck.client.home.product.model;

import com.opencheck.client.models.lixishop.BankModel;

public class BankCard {
    /**
     * bank_code : napas_atm
     * logo_url : https://png.icons8.com/metro/1600/bank-cards.png
     * gateway_code : napas
     * number : 970400xxxxxx0018
     * name : NGUYEN VAN A
     * issue_date : 0307
     * bank_name: master
     */

    private String bank_code;
    private String logo_url;
    private String gateway_code;
    private String number;
    private String name;
    private String issue_date;
    private String bank_name;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getGateway_code() {
        return gateway_code;
    }

    public void setGateway_code(String gateway_code) {
        this.gateway_code = gateway_code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }
}
