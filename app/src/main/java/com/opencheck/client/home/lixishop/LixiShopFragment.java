package com.opencheck.client.home.lixishop;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.opencheck.client.LixiFragment;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.LixiShopFragmentBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.lixishop.header.buycard.LixiBuyCardFragment;
import com.opencheck.client.home.lixishop.header.rewardcode.RewardCodeActivity;
import com.opencheck.client.home.lixishop.header.topup.LixiTopupFragment;
import com.opencheck.client.home.search.SearchEvoucherByLixiActivity;
import com.opencheck.client.login.LoginHelper;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.models.lixishop.LixiHotProductModel;
import com.opencheck.client.models.lixishop.LixiShopCategoryModel;
import com.opencheck.client.models.lixishop.ServiceCategoryModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.opencheck.client.utils.ConstantValue.ACTION_BUY_CARD;
import static com.opencheck.client.utils.ConstantValue.ACTION_SHOW_REWARD_CODE;
import static com.opencheck.client.utils.ConstantValue.ACTION_TOPUP;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class LixiShopFragment extends LixiFragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rcvShopCategory;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private int pageCategory = 1;
    private int pageHot = 1;
    boolean loading = false;
    private boolean noRemain = false;
    private ArrayList<LixiShopCategoryModel> list_category = new ArrayList<>();
    private ArrayList<LixiHotProductModel> list_product = new ArrayList<>();
    private CategoryAdapter adapterCategory;
    private ArrayList<ServiceCategoryModel> listService = new ArrayList<ServiceCategoryModel>();
    private boolean serviceEnable;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager layoutManager;
    private boolean isOutOfVoucherPage = false;
    private ImageView imgLogo;

    private LixiTopupFragment topupFragment = null;

    private LixiShopFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (isResume && rootview != null) {
            isResume = false;
            return rootview;
        }
        mBinding = LixiShopFragmentBinding.inflate(inflater, container, false);
        rootview = mBinding.getRoot();
        super.onCreateView(inflater, container, savedInstanceState);
        pageCategory = 1;
        initView(mBinding.getRoot());
        //getServiceCategory();
        return mBinding.getRoot();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        rcvShopCategory = view.findViewById(R.id.rclv_category);
        initData();
    }

    private void initData() {
        mBinding.searchBar.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        mBinding.linearTopup.setOnClickListener(this);
        mBinding.linearBuyCard.setOnClickListener(this);
        mBinding.linearLixiCode.setOnClickListener(this);

        if (LoginHelper.isLoggedIn(activity))
            startLoadNumCode();
        else {
            mBinding.tvNumCode.setVisibility(View.GONE);
        }

        rcvShopCategory.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        rcvShopCategory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (rcvShopCategory.computeVerticalScrollOffset() > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (!loading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startLoadData();
                            }
                        }, 1000);

                        loading = true;
                    }
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoadData();
            }
        }, 500);
    }

    private void startLoadNumCode() {
        DataLoader.getNumLixiCode(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    mBinding.tvNumCode.setText((int) object + "");
                    if ((int) object == 0) {
                        mBinding.tvNumCode.setVisibility(View.GONE);
                    } else {
                        mBinding.tvNumCode.setVisibility(View.VISIBLE);
                    }
                } else {
                }
            }
        });
    }

    private void startLoadData() {
        getCategory();
    }

    private void getCategory() {
        swipeRefreshLayout.setRefreshing(true);
        DataLoader.getLixiShopCategory(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                swipeRefreshLayout.setRefreshing(false);
                if (isSuccess) {
                    if (pageCategory == 1) {
                        list_category = (ArrayList<LixiShopCategoryModel>) object;
                        if (list_category == null || list_category.size() == 0) {
                            return;
                        }
                        setOutOfVoucherPage(list_category);
                        setAdapter();
                    } else {
                        List<LixiShopCategoryModel> list = (List<LixiShopCategoryModel>) object;
                        if (list_category == null || list == null || list.size() == 0) {
                            return;
                        }
                        setOutOfVoucherPage(list);
                        list_category.addAll(list);
                        adapterCategory.notifyDataSetChanged();
                    }
                    loading = false;
                    pageCategory++;
                } else {
                    loading = true;
                }
            }
        }, pageCategory);

    }

    private void setAdapter() {
        adapterCategory = new CategoryAdapter(activity, list_category, listService, ((HomeActivity) activity).widthScreen);
        layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rcvShopCategory.setLayoutManager(layoutManager);
        rcvShopCategory.setAdapter(adapterCategory);
    }

    private void getServiceCategory() {
        DataLoader.getLixiShopServiceCategory(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                serviceEnable = isSuccess;
                if (isSuccess) {
                    listService = (ArrayList<ServiceCategoryModel>) object;
                    if (adapterCategory != null)
                        adapterCategory.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getServiceCategory();
        if (activity instanceof HomeActivity)
            ((HomeActivity) activity).visibleBottomNavigationView(View.VISIBLE);
//            ((HomeActivity) activity).navigation.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.searchBar:
                Intent intent = new Intent(activity, SearchEvoucherByLixiActivity.class);
                activity.startActivity(intent);
                break;
            case R.id.linearTopup:
                goToTopUpFragment();
                break;
            case R.id.linearBuyCard:
                goToBuyCardFragment();
                break;
            case R.id.linearLixiCode:
                goToRewardCodeActivity();
                break;
        }
    }

    public ServiceCategoryModel checkService(String type) {
        if (listService != null) {
            for (int i = 0; i < listService.size(); i++) {
                ServiceCategoryModel service = listService.get(i);
                String stype = service.getType();
                boolean isTheSameType = service.getType().equals(type);
                if (isTheSameType) {
                    return service;
                }
            }
        }
        return null;
    }

    private String ACTION = "";

    public void getServiceCategoryToExchange() {
        DataLoader.getLixiShopServiceCategory(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    listService = (ArrayList<ServiceCategoryModel>) object;
                    if (ACTION.equalsIgnoreCase(ACTION_BUY_CARD)) {
                        goToBuyCardFragment();
                    } else if (ACTION.equalsIgnoreCase(ACTION_TOPUP)) {
                        goToTopUpFragment();
                    } else if (ACTION.equalsIgnoreCase(ACTION_SHOW_REWARD_CODE)) {
                        Intent intent = new Intent(activity, RewardCodeActivity.class);
                        activity.startActivity(intent);
                    }

                }
            }
        });
    }

    private void goToRewardCodeActivity() {
        if (LoginHelper.isLoggedIn(activity)) {
            ACTION = ACTION_SHOW_REWARD_CODE;
            if (listService == null || listService.size() == 0) {
                getServiceCategoryToExchange();
            } else {
                Intent intent = new Intent(activity, RewardCodeActivity.class);
                activity.startActivity(intent);
            }
        } else {
            LoginHelper.startLoginActivityWithAction(activity, ACTION_SHOW_REWARD_CODE);

        }
    }

    private void goToBuyCardFragment() {
        if (LoginHelper.isLoggedIn(activity)) {
            if (listService == null || listService.size() == 0) {
                ACTION = ACTION_BUY_CARD;
                getServiceCategoryToExchange();
            } else {
                ServiceCategoryModel service = checkService(ServiceCategoryModel.TYPE_BUY_CARD);
                if (service != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantValue.SERVICE_CATEGORY_CHILD_MODEL, new Gson().toJson(service));
                    LixiBuyCardFragment fragment = new LixiBuyCardFragment();
                    fragment.setArguments(bundle);
                    if (activity instanceof HomeActivity) {
                        Analytics.trackScreenView(fragment.getScreenName(), null);
                        ((HomeActivity) activity).replaceFragment(fragment);
                    }
                } else
                    Helper.showErrorDialog(activity, "Dịch vụ chưa sẵn sàng!");
            }
        } else {
            LoginHelper.startLoginActivityWithAction(activity, ACTION_BUY_CARD);
        }
    }

    public void goToTopUpFragment() {
        if (LoginHelper.isLoggedIn(activity)) {
            if (listService == null || listService.size() == 0) {
                ACTION = ACTION_TOPUP;
                getServiceCategoryToExchange();
            } else {
                ServiceCategoryModel service = checkService(ServiceCategoryModel.TYPE_TOPUP);
                if (service != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantValue.SERVICE_CATEGORY_CHILD_MODEL, new Gson().toJson(service));
                    topupFragment = new LixiTopupFragment();
                    topupFragment.setArguments(bundle);
                    if (activity instanceof HomeActivity) {
                        Analytics.trackScreenView(topupFragment.getScreenName(), null);
                        ((HomeActivity) activity).replaceFragment(topupFragment);
                    }
                } else
                    Helper.showErrorDialog(activity, "Dịch vụ chưa sẵn sàng!");
            }
        } else {
            LoginHelper.startLoginActivityWithAction(activity, ACTION_TOPUP);
        }
    }

    private void setOutOfVoucherPage(List<LixiShopCategoryModel> list) {
        if (list.size() < RECORD_PER_PAGE) {
            isOutOfVoucherPage = true;
        }
    }

    @Override
    public void onRefresh() {
        list_category.clear();
        loading = false;
        adapterCategory.notifyDataSetChanged();
        pageCategory = 1;
        isOutOfVoucherPage = false;
        startLoadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                if (resultCode != RESULT_OK)
                    return;
                handleLoginResultAction(data);
                break;
            case ConstantValue.REQUEST_CODE_READ_CONTACTS:
                onActivityResultTopup(requestCode, resultCode, data);
                break;
        }
    }

    private void handleLoginResultAction(Intent intent) {
        Bundle data = intent.getExtras();
        String action = data.getString(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        switch (action) {
            case ConstantValue.ACTION_TOPUP:
                goToTopUpFragment();
                break;
            case ConstantValue.ACTION_BUY_CARD:

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantValue.REQUEST_CODE_READ_CONTACTS:
            case ConstantValue.REQUEST_CODE_READ_CONTACTS_PERMISSION:
                onRequestPermissionsResultTopup(requestCode, permissions, grantResults);
                break;

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void onRequestPermissionsResultTopup(int requestCode, String[] permissions, int[] grantResults) {
        if (topupFragment != null && topupFragment.isVisible()) {
            topupFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void onActivityResultTopup(int requestCode, int resultCode, Intent data) {
        if (topupFragment != null && topupFragment.isVisible()) {
            topupFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (ConfigModel.getInstance(activity).getShop_services_enable() == 1) {
                mBinding.linearHeader.setVisibility(View.VISIBLE);
            }else {
                mBinding.linearHeader.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.REWARD_HOME;
    }
}
