package com.opencheck.client.home.account.new_layout.dialog;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogDetailTransactionBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.adapter.ComboItemAdapter;
import com.opencheck.client.home.account.new_layout.adapter.EvoucherListAdapter;
import com.opencheck.client.home.account.new_layout.adapter.EvoucherNullComboAdapter;
import com.opencheck.client.home.account.new_layout.control.OperateObject;
import com.opencheck.client.home.account.new_layout.model.DetailTransaction;
import com.opencheck.client.home.account.new_layout.model.EvoucherInfo;
import com.opencheck.client.models.lixishop.ComboItemModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

public class DetailTransactionDialog extends LixiDialog {
    private int COLOR_COMPLETE, COLOR_FAIL, COLOR_WATING, COLOR_BLACK, COLOR_STRIKE;
    private final String STATUS_COMPLETE = "complete";
    private final String STATUS_FAIL = "payment_expired";
    private final String STATUS_CANCEL = "user_cancel";
    private final String STATUS_WAITING = "waiting";
    private String UNION, UNION_UNDER;
    private final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

    public DetailTransactionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
        COLOR_COMPLETE = activity.getResources().getColor(R.color.colorSuccess);
        COLOR_FAIL = activity.getResources().getColor(R.color.colorFail);
        COLOR_WATING = activity.getResources().getColor(R.color.colorWaiting);
        COLOR_BLACK = activity.getResources().getColor(R.color.black);
        COLOR_STRIKE = activity.getResources().getColor(R.color.colorStrike);

        UNION = activity.getResources().getString(R.string.p);
        UNION_UNDER = activity.getResources().getString(R.string.p_under);
    }

    // View
    private RelativeLayout relBack;
    private TextView txtCode, txtDate, txtStatus, txtDetail, txtVoucherName, txtPrice, txtQuant;
    private RecyclerView recListCombo;
    private TextView txtTempPrice, txtLixiDiscount, txtTotalPrice, txtLixi, txtLixiTitle;
    private ImageView imgPayoo;
    private LinearLayout linearPaymentMethod, linearListCombo;
    private ConstraintLayout constraintPaymentAtStore;
    private TextView txtPaymentMethod, txtDeadline, txtPaymentAtStore, txtPaymentCode, txtPaymentCodeText, txtDeadlineAtStore;
    private RecyclerView recEvoucherCombo;
    private LinearLayoutManager layoutManager;

    private DialogDetailTransactionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogDetailTransactionBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView() {
        relBack = (RelativeLayout) findViewById(R.id.rlBack);

        txtCode = (TextView) findViewById(R.id.txtCode);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtDetail = (TextView) findViewById(R.id.txtDetail);
        txtVoucherName = (TextView) findViewById(R.id.txtVoucherName);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtQuant = (TextView) findViewById(R.id.txtQuant);
        txtTempPrice = (TextView) findViewById(R.id.txtTempPrice);
        txtLixiDiscount = (TextView) findViewById(R.id.txtLixiDiscount);
        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);
        txtLixi = (TextView) findViewById(R.id.txtLixi);
        txtLixiTitle = (TextView) findViewById(R.id.txtLixiTitle);
        txtPaymentMethod = (TextView) findViewById(R.id.txtPaymentMethod);
        txtDeadline = (TextView) findViewById(R.id.txtDeadline);
        txtPaymentAtStore = (TextView) findViewById(R.id.txtPaymentAtStore);
        txtPaymentCode = (TextView) findViewById(R.id.txtPaymentCode);
        txtPaymentCodeText = (TextView) findViewById(R.id.txtPaymentCodeText);
        txtDeadlineAtStore = (TextView) findViewById(R.id.txtDeadlineAtStore);

        imgPayoo = (ImageView) findViewById(R.id.imgPayoo);

        recListCombo = (RecyclerView) findViewById(R.id.recListCombo);
        recEvoucherCombo = (RecyclerView) findViewById(R.id.recEvoucherCombo);

        linearPaymentMethod = (LinearLayout) findViewById(R.id.linearPaymentMethod);
        linearListCombo = (LinearLayout) findViewById(R.id.linearListCombo);
        constraintPaymentAtStore = (ConstraintLayout) findViewById(R.id.constraintPaymentAtStore);

        relBack.setOnClickListener(this);
        layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    }

    public void setData(int id, String index) {
        DataLoader.getDetailTransaction(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    DetailTransaction detailTransaction = (DetailTransaction) object;
                    showData(detailTransaction);
                } else {
                    DetailTransactionDialog.this.cancel();
                }
            }
        }, id, index);
    }

    private void showData(DetailTransaction data) {
        txtCode.setText("#" + data.getOrder_uuid());
        txtDate.setText(Helper.getFormatDateEngISO(DATE_FORMAT, data.getDate_order() * 1000 + ""));
        txtQuant.setText("x" + data.getProduct_info().getQuantity());
        txtVoucherName.setText(data.getProduct_info().getName());

        if (data.getStatus().equals(STATUS_FAIL) || data.getStatus().equals(STATUS_CANCEL)) {
            txtDetail.setVisibility(View.VISIBLE);
            txtStatus.setText(activity.getString(R.string.voucher_transaction_fail));
            txtStatus.setTextColor(COLOR_FAIL);

            txtPrice.setTextColor(COLOR_COMPLETE);
            txtPrice.setTextSize(20f);
            txtPrice.setText(Helper.getVNCurrency(data.getProduct_info().getPrice()) + UNION_UNDER);

            if (data.getStatus().equals(STATUS_CANCEL)) {
                txtDetail.setText(activity.getString(R.string.voucher_transaction_user_cancel));
            } else {
                txtDetail.setText(activity.getString(R.string.voucher_transaction_expired_payment));
            }
        } else {
            txtDetail.setVisibility(View.GONE);
            if (data.getStatus().equals(STATUS_COMPLETE)) {
                txtStatus.setText(activity.getString(R.string.voucher_transaction_success));
                txtStatus.setTextColor(COLOR_COMPLETE);
            } else {
                txtStatus.setText(activity.getString(R.string.waiting_for_payment));
                txtStatus.setTextColor(COLOR_WATING);
            }

            txtPrice.setTextColor(COLOR_BLACK);
            txtPrice.setTextSize(15f);
            txtPrice.setText(Helper.getVNCurrency(data.getProduct_info().getPrice()) + UNION);
        }

        // Combo
        ArrayList<ComboItemModel> listCombo = data.getProduct_info().getCombo_item();
        if (listCombo == null || listCombo.size() == 0) {
            linearListCombo.setVisibility(View.GONE);
        } else {
            // set adapter list combo
            layoutManager = new LinearLayoutManager(activity);
            linearListCombo.setVisibility(View.VISIBLE);
            ComboItemAdapter comboItemAdapter = new ComboItemAdapter(listCombo);
            recListCombo.setLayoutManager(new LinearLayoutManager(activity));
            recListCombo.setAdapter(comboItemAdapter);
        }

        // Price
        long tempPrice = data.getProduct_info().getPrice() * data.getProduct_info().getQuantity();
        long totalLixi = data.getTotal_cashback();
        long totalPrice = data.getTotal_price();
        long totalDiscount = data.getTotal_discount();
        txtTempPrice.setText(Helper.getVNCurrency(tempPrice) + UNION);
        txtLixiDiscount.setText("-" + Helper.getVNCurrency(totalDiscount) + UNION);
        txtTotalPrice.setText(Helper.getVNCurrency(totalPrice) + UNION);

        if (data.getStatus().equals(STATUS_FAIL) || data.getStatus().equals(STATUS_CANCEL)) {
            txtLixi.setVisibility(View.GONE);
            txtLixiTitle.setVisibility(View.GONE);
        } else {
            if (data.getTotal_cashback() == 0) {
                txtLixi.setVisibility(View.GONE);
                txtLixiTitle.setVisibility(View.GONE);
            } else {
                txtLixi.setVisibility(View.VISIBLE);
                txtLixiTitle.setVisibility(View.VISIBLE);
                txtLixi.setText("+" + Helper.getVNCurrency(totalLixi) + UNION);
            }
        }

        if (data.getPayment_key().equals("at_store") || data.getPayment_key().equals("cod")) {
            linearPaymentMethod.setVisibility(View.GONE);
            constraintPaymentAtStore.setVisibility(View.VISIBLE);
            txtPaymentAtStore.setText(data.getPayment_method());
            txtPaymentCode.setText(data.getBilling_code());

            if (data.getStatus().equals(STATUS_FAIL) || data.getStatus().equals(STATUS_CANCEL)) {
                txtPaymentCode.setTextColor(COLOR_STRIKE);
                txtPaymentCodeText.setTextColor(COLOR_STRIKE);
                txtPaymentCode.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                txtPaymentCode.setPaintFlags(TextPaint.STRIKE_THRU_TEXT_FLAG);
                txtPaymentCodeText.setPaintFlags(TextPaint.STRIKE_THRU_TEXT_FLAG);
            }

            if (data.getPayment_key().equals("cod")) {
                imgPayoo.setVisibility(View.GONE);
            }

            txtDeadlineAtStore.setText(activity.getString(R.string.title_expired_date) + ": " + Helper.getFormatDateEngISO(DATE_FORMAT, data.getPayment_expired_time() * 1000 + ""));
        } else {
            linearPaymentMethod.setVisibility(View.VISIBLE);
            constraintPaymentAtStore.setVisibility(View.GONE);
            txtPaymentMethod.setText(data.getPayment_method());
            txtDeadline.setText(activity.getString(R.string.title_expired_date) + ": " + Helper.getFormatDateEngISO(DATE_FORMAT, data.getPayment_expired_time() * 1000 + ""));
        }

        // set adapter list Evoucher
        ArrayList<EvoucherInfo> listEvoucherComboData = data.getEvoucher_info();
        if (data.getStatus().equals(STATUS_CANCEL) || data.getStatus().equals(STATUS_FAIL) || data.getStatus().equals(STATUS_WAITING)) {
            // thanh toán thất bại
            recEvoucherCombo.setVisibility(View.GONE);
        } else {
            // thanh toán thành công
            recEvoucherCombo.setVisibility(View.VISIBLE);
            if (data.getProduct_info().getCombo_item() == null || data.getProduct_info().getCombo_item().size() == 0) {
                // không phải combo
                layoutManager = new LinearLayoutManager(activity);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                EvoucherNullComboAdapter evoucherNullComboAdapter = new EvoucherNullComboAdapter(activity, data.getEvoucher_info(), data.getProduct_info());
                recEvoucherCombo.setLayoutManager(layoutManager);
                recEvoucherCombo.setAdapter(evoucherNullComboAdapter);
            } else {
                // có combo
                // set adapter list combo
                ArrayList<ArrayList<EvoucherInfo>> listEvoucherCombo = new ArrayList<>();
                for (int i = 0; i < listCombo.size(); i++) {
                    listEvoucherCombo.add(OperateObject.getInstance().sort(listEvoucherComboData, listCombo.get(i).getId()));
                }

                layoutManager = new LinearLayoutManager(activity);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                EvoucherListAdapter evoucherListAdapter = new EvoucherListAdapter(activity, listCombo, listEvoucherCombo);
                recEvoucherCombo.setAdapter(evoucherListAdapter);
                recEvoucherCombo.setLayoutManager(layoutManager);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                cancel();
                break;
        }
    }
}
