package com.opencheck.client.home.delivery.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.RowHotCategoryDeliBinding;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class CategorySearchAdapter extends RecyclerView.Adapter<CategorySearchAdapter.CategoryHolder> {
    private LixiActivity activity;
    private ArrayList<ArticleModel> listArticle;

    private final int limit = 4;

    public CategorySearchAdapter(LixiActivity activity, ArrayList<ArticleModel> listArticle) {
        this.activity = activity;
        this.listArticle = listArticle;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowHotCategoryDeliBinding mBinding = RowHotCategoryDeliBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false );
        return new CategoryHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        if (listArticle == null){
            return 0;
        }else {
            if (listArticle.size() < limit){
                return listArticle.size();
            }else {
                return limit;
            }
        }
    }

    public class CategoryHolder extends RecyclerView.ViewHolder{
        RowHotCategoryDeliBinding mBinding;
        public CategoryHolder(RowHotCategoryDeliBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Bundle bundle = new Bundle();
//                    bundle.putLong("ID", listArticle.get(getAdapterPosition()).getId());
//                    bundle.putString("TITLE", listArticle.get(getAdapterPosition()).getName());
//                    bundle.putString("IMAGE_LINK", listArticle.get(getAdapterPosition()).getImage_link());
//                    bundle.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.SEARCH);
//                    Intent intent = new Intent(activity, StoreListOfArticleActivity.class);
//                    intent.putExtras(bundle);
//                    activity.startActivity(intent);
                    StoreListOfArticleActivity.startArticleActivity(activity,
                            0L, listArticle.get(getAdapterPosition()).getId(),
                            listArticle.get(getAdapterPosition()).getName(),
                            listArticle.get(getAdapterPosition()).getImage_link(),
                            TrackingConstant.ContentSource.SEARCH);
                }
            });
        }

        public void bind(){
            ArticleModel articleModel = listArticle.get(getAdapterPosition());
            if (articleModel != null) {
                ImageLoader.getInstance().displayImage(articleModel.getImage_link(), mBinding.imgBackground);
                mBinding.txtName.setText(articleModel.getName());
                mBinding.txtQuant.setText(String.format(LanguageBinding.getString(R.string.deli_search_quant_category, activity),
                        articleModel.getList_subcategory() == null ? 0 : articleModel.getList_subcategory().size()));
            }
        }
    }
}
