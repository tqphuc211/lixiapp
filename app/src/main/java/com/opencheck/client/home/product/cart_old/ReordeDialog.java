package com.opencheck.client.home.product.cart_old;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LixiShopReorderAndViewDetailDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.LixiTrackingDialog;
import com.opencheck.client.home.history.detail.OrderShopDetailDialog;
import com.opencheck.client.models.lixishop.OrderCheckModel;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class ReordeDialog extends LixiTrackingDialog {


    public ReordeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected RelativeLayout rl_dismiss;
    private OrderCheckModel orderCheckModel;

    private TextView tv_view_log, tv_cancel, tv_total_price, tv_name, tv_amount, tv_method, tv_mess;
    private ImageView iv_product;
    private String imageUrl, name;


    // private static PopupReorder instance;

    private LixiShopReorderAndViewDetailDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = LixiShopReorderAndViewDetailDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }


    private void findViews() {
        tv_method = (TextView) findViewById(R.id.tv_method);
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        tv_view_log = (TextView) findViewById(R.id.tv_view_log);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_total_price = (TextView) findViewById(R.id.tv_total_price);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        iv_product = (ImageView) findViewById(R.id.iv_product);
        tv_mess = (TextView) findViewById(R.id.tv_message);

        try {
            if (orderCheckModel.getPayment_method_value().equals("null"))
                tv_method.setVisibility(View.GONE);
            else
                tv_method.setText("(" + orderCheckModel.getPayment_method_value() + ")");
            tv_mess.setText(Html.fromHtml(orderCheckModel.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            tv_method.setVisibility(View.GONE);
        }


        ImageLoader.getInstance().displayImage(imageUrl, iv_product, LixiApplication.getInstance().optionsNomal);
        tv_name.setText(name);
        tv_amount.setText(activity.getString(R.string.count) + ": " + orderCheckModel.getQuantity());
        tv_total_price.setText(Helper.getVNCurrency(orderCheckModel.getDiscount_price()) + activity.getString(R.string.p));

        tv_view_log.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    public void setData(OrderCheckModel orderCheckModel, String imageUrl, String name) {
        this.orderCheckModel = orderCheckModel;
        this.imageUrl = imageUrl;
        this.name = name;
    }

    @Override
    public void onClick(View view) {
        if (view == tv_view_log) {
            OrderShopDetailDialog dialog = new OrderShopDetailDialog(activity, false, true, false);
            dialog.show();
            dialog.setData(String.valueOf(orderCheckModel.getHistory_id()));
        } else if (view == tv_cancel) {
            if (isClicked)
                return;
            handleDeleteOrder(orderCheckModel.getId());
        } else if (view == rl_dismiss) {
            Intent intent = new Intent().setAction("reLoadLoadQuantityAllow");
            activity.sendBroadcast(intent);
            dismiss();
        }
    }

    private boolean isClicked = false;

    private void handleDeleteOrder(long id) {
        isClicked = true;
        DataLoader.deletePendingOrder(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    JsonObject records = (JsonObject) object;
                    boolean status = records.get("status").getAsBoolean();
                    if (status) {
                        dismiss();
                        if (eventListener != null)
                            eventListener.onCancel();
                        Toast.makeText(activity, "Hủy đơn hàng thành công", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent().setAction("reLoadLoadQuantityAllow");
                        activity.sendBroadcast(intent);
                    } else {
                        showErrorToast("Đã xảy ra lỗi");
                    }
                } else {
                    showErrorToast(object.toString());
                }
                isClicked = false;
            }
        }, id);
    }

    private void showErrorToast(String objects) {
        Toast toast = Toast.makeText(activity, objects, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
    }

    public interface IDidalogEvent {
        public void onOk();

        public void onCancel();
    }

    protected ReordeDialog.IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            ReordeDialog.IDidalogEvent listener) {
        eventListener = listener;
    }
}
