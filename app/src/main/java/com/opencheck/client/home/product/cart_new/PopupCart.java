package com.opencheck.client.home.product.cart_new;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.ProductCartPopupBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.ManualDismissDialog;
import com.opencheck.client.home.flashsale.Global.FlashSaleTimerController;
import com.opencheck.client.home.product.AdapterComboProduct;
import com.opencheck.client.models.Product.ProductCashVoucherDetailModel;
import com.opencheck.client.models.lixishop.QuantityProductModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.CrashModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class PopupCart extends LixiDialog {

    public PopupCart(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.VOUCHER_CART_STEP_1;
    }

    //region VIEW
    private ImageView iv_back;
    private LinearLayout flash_sale;
    private ImageView iv_logo_product;
    private TextView tv_name_product;
    private TextView tv_price_count;
    private TextView tv_lixi_count;
    private EditText et_count;
    private ImageView iv_sub;
    private ImageView iv_add;
    private RecyclerView rv_combo;
    private TextView tv_limit_but_product;
    private EditText et_phone;
    private TextView tv_price;
    private TextView lb_lixi;
    private TextView tv_lixi;
    private TextView tv_next;

    private void findViews() {
        iv_back = findViewById(R.id.iv_back);
        flash_sale = findViewById(R.id.flash_sale);
        iv_logo_product = findViewById(R.id.iv_logo_product);
        tv_name_product = findViewById(R.id.tv_name_product);
        tv_price_count = findViewById(R.id.tv_price_count);
        tv_lixi_count = findViewById(R.id.tv_lixi_count);
        et_count = findViewById(R.id.et_count);
        iv_sub = findViewById(R.id.iv_sub);
        iv_add = findViewById(R.id.iv_add);
        rv_combo = findViewById(R.id.rv_combo);
        tv_limit_but_product = findViewById(R.id.tv_limit_but_product);
        et_phone = findViewById(R.id.et_phone);
        tv_price = findViewById(R.id.tv_price);
        lb_lixi = findViewById(R.id.lb_lixi);
        tv_lixi = findViewById(R.id.tv_lixi);
        tv_next = findViewById(R.id.tv_next);

        iv_back.setOnClickListener(this);
        iv_sub.setOnClickListener(this);
        iv_add.setOnClickListener(this);
        tv_next.setOnClickListener(this);
    }
    //endregion

    private FlashSaleTimerController fsControler;
    private ProductCashVoucherDetailModel productModel;
    private int countProduct = 1;
    AdapterComboProduct adapterCombo;

    private QuantityProductModel quantityProductModel = new QuantityProductModel();
    private int rangeQuantity = 100;

    private ProductCartPopupBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ProductCartPopupBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    //region SET DATA
    public void setData(ProductCashVoucherDetailModel productModel) {
        this.productModel = productModel;
        loadRangeQuantity();
        setProductData();
        UserModel userModel = UserModel.getInstance();
        if (userModel != null && userModel.getPhone() != null)
            et_phone.setText(userModel.getPhone());
        Analytics.trackScreenView(getScreenName(), this.productModel.getId());
    }

    public void updateFsTimer(int[] arrTime) {
        if (fsControler != null)
            fsControler.showTime(arrTime);
    }
    //endregion

    //region SHOW DATA
    public void setProductData() {
        ImageLoader.getInstance().displayImage(productModel.getProduct_image_medium().get(0), iv_logo_product);
        tv_name_product.setText(productModel.getName());

        if (productModel.getCombo_item() != null && productModel.getCombo_item().size() > 0) {
            rv_combo.setVisibility(View.VISIBLE);
            rv_combo.setLayoutManager(new LinearLayoutManager(activity));
            rv_combo.setNestedScrollingEnabled(false);
            adapterCombo = new AdapterComboProduct(activity, productModel.getCombo_item());
            rv_combo.setAdapter(adapterCombo);
        } else {
            rv_combo.setVisibility(View.GONE);
        }

        setCountProduct(1);
        if (productModel.getFlash_sale_id() > 0 && productModel.getFlash_sale_info() != null) {
            fsControler = new FlashSaleTimerController(activity, flash_sale);
        }

        et_count.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null) {
                    if (!editable.toString().equals("")) {
                        try {
                            countProduct = Integer.valueOf(editable.toString());
                        } catch (NumberFormatException e) {
                            countProduct = 0;
                            et_count.setText("");
                            Toast.makeText(activity, LanguageBinding.getString(R.string.count_unreal, activity), Toast.LENGTH_SHORT).show();
                            Crashlytics.logException(new CrashModel("input: " + editable.toString() + " - " + UserModel.getInstance().getFullname() + ", id: " + UserModel.getInstance().getId()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        countProduct = 0;
                    }
                    setCountData();
                }
            }
        });
    }

    public void setCountData() {
        long price = countProduct * productModel.getPayment_discount_price();
        long lixi = countProduct * productModel.getCashback_price();

        if (productModel.getFlash_sale_id() > 0) {
            price = countProduct * productModel.getFlash_sale_info().getPayment_discount_price();
            lixi = countProduct * productModel.getFlash_sale_info().getCashback_price();
        }

        tv_price.setText(Helper.getVNCurrency(price) + LanguageBinding.getString(R.string.p, activity));
        tv_price_count.setText(Helper.getVNCurrency(price) + LanguageBinding.getString(R.string.p, activity));
        tv_lixi.setText("+" + Helper.getVNCurrency(lixi) + " Lixi");
        tv_lixi_count.setText("+" + Helper.getVNCurrency(lixi) + " Lixi");
        if (adapterCombo != null) {
            adapterCombo.setTotalProduct(countProduct);
        }

        if (lixi > 0) {
            tv_lixi.setVisibility(View.VISIBLE);
            tv_lixi_count.setVisibility(View.VISIBLE);
            lb_lixi.setVisibility(View.VISIBLE);
        } else {
            tv_lixi.setVisibility(View.GONE);
            tv_lixi_count.setVisibility(View.GONE);
            lb_lixi.setVisibility(View.GONE);
        }
    }

    public void setCountProduct(int countProduct) {
        this.countProduct = countProduct;
        et_count.setText(countProduct + "");
        setCountData();
    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                dismiss();
                break;
            case R.id.iv_sub:
                setCountProduct(Math.max(1, countProduct - 1));
                break;
            case R.id.iv_add:
                setCountProduct(Math.min(rangeQuantity, countProduct + 1));
                break;
            case R.id.tv_next:
                //TODO check sdt hợp lệ
                if (!Helper.isValidPhone(et_phone.getText().toString().replace(" ", ""))) {
                    ManualDismissDialog dialog = new ManualDismissDialog(activity, false, true, false);
                    dialog.show();
                    dialog.setTitle(LanguageBinding.getString(R.string.cash_evoucher_warning, activity));
                    dialog.setMessage(LanguageBinding.getString(R.string.login_error_phone_number, activity));
                    return;
                }
                if (eventListener != null)
                    eventListener.onNext(countProduct, et_phone.getText().toString());
                break;
        }
    }

    //region Load Data
    private void loadRangeQuantity() {
        DataLoader.getProductQuantity(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    quantityProductModel = new QuantityProductModel();
                    quantityProductModel = (QuantityProductModel) object;
                    if (quantityProductModel.getLimit_cash() > 0) {
                        rangeQuantity = quantityProductModel.getLimit_cash();
                        tv_limit_but_product.setVisibility(View.VISIBLE);
                        tv_limit_but_product.setText(String.format(LanguageBinding.getString(R.string.cash_evoucher_limit_product, activity), quantityProductModel.getLimit_cash()));
                        if (countProduct > rangeQuantity)
                            setCountProduct(rangeQuantity);
                    } else tv_limit_but_product.setVisibility(View.GONE);
                }
            }
        }, productModel.getId());
    }
    //endregion

    public interface CartEventListener {
        void onNext(int sl, String sdt);
    }

    public void setCartEventListener(CartEventListener eventListener) {
        this.eventListener = eventListener;
    }

    private CartEventListener eventListener;
}