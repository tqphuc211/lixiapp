package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;

public class ArticleModel extends LixiModel {
    private long id;
    private String name;
    private String image_link;
    private String cover_link;
    private ArrayList<SubCategoryModel> list_subcategory;

    public ArrayList<SubCategoryModel> getList_subcategory() {
        return list_subcategory;
    }

    public void setList_subcategory(ArrayList<SubCategoryModel> list_subcategory) {
        this.list_subcategory = list_subcategory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_link() {
        if (image_link == null || image_link.equals("")) {
            return ConstantValue.DEFAULT_IMAGE[0];
        }
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getCover_link() {
        if (cover_link == null || cover_link.isEmpty()){
            return getImage_link();
        }
        return cover_link;
    }

    public void setCover_link(String cover_link) {
        this.cover_link = cover_link;
    }
}
