package com.opencheck.client.home.product;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.home.product.ImageFullScreenDialog;

import java.util.List;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class AdapterImageEvoucherDetail extends PagerAdapter {

    Context context;
    List<String> list ;
    LayoutInflater layoutInflater;
    private String productName = "";

    private LixiActivity activity;

    public AdapterImageEvoucherDetail(Context context, List<String> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.lixi_shop_row_image_detail_product,container,false);
        ImageView imageView = (ImageView) view.findViewById(R.id.img);
        ImageLoader.getInstance().displayImage(list.get(position),imageView, LixiApplication.getInstance().optionsNomal);
        container.addView(view);
        final int pos = position;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFullScreenDialog popupImageFullscreen = new ImageFullScreenDialog(activity, false, true, false);
                popupImageFullscreen.setCancelable(true);
                popupImageFullscreen.show();
                popupImageFullscreen.setData(list, pos, productName);
            }
        });
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setData(LixiActivity activity, String productName){
        this.activity = activity;
        this.productName = productName;
    }
}