package com.opencheck.client.home.delivery.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FreeShipDialogBinding;
import com.opencheck.client.utils.Helper;

public class FreeShipDialog extends LixiDialog {

    private TextView tvValue, tvMessage;
    private LinearLayout ll_container;
    private RelativeLayout rlDismiss;

    public FreeShipDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private FreeShipDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = FreeShipDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        tvValue = findViewById(R.id.tv_value);
        tvMessage = findViewById(R.id.tv_message);
        ll_container = findViewById(R.id.ll_container);
        rlDismiss = findViewById(R.id.rl_dismiss);

        rlDismiss.setOnClickListener(this);
        ll_container.setOnClickListener(this);

        ViewGroup.LayoutParams params = ll_container.getLayoutParams();
        params.height = activity.heightScreen / 2;
        ll_container.setLayoutParams(params);
    }

    public void setData(Long mMinMoney, Long mMaxFee) {
        if (mMaxFee != null)
            tvValue.setText(Html.fromHtml("* Miễn phí ship tối đa <b>" + Helper.getVNCurrency(mMaxFee) + "đ</b>"));

        if (mMinMoney != null && mMinMoney > 0)
            tvMessage.setText(Html.fromHtml("* Áp dụng với đơn hàng có giá trị lớn hơn <b>" + Helper.getVNCurrency(mMinMoney) + "đ</b>"));
    }

    @Override
    public void onClick(View view) {
        if (view == rlDismiss) {
            dismiss();
        }
    }
}
