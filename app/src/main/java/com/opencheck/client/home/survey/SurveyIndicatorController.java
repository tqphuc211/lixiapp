package com.opencheck.client.home.survey;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.SurveyIndicatorItemBinding;

public class SurveyIndicatorController {
    static int width = 0;
    LixiActivity activity;
    View rootView;

    View v_indicator;

    public SurveyIndicatorController(LixiActivity activity, ViewGroup rootView, int size) {
        int w = (int) activity.getResources().getDimension(R.dimen.value_68);
        if (width == 0) {
            width = rootView.getWidth() / 5;
        }
        SurveyIndicatorItemBinding surveyIndicatorItemBinding =
                SurveyIndicatorItemBinding.inflate(LayoutInflater.from(activity), rootView,
                        false);
        rootView.addView(surveyIndicatorItemBinding.getRoot());

        v_indicator = surveyIndicatorItemBinding.getRoot().findViewById(R.id.v_indicator);
    }
    public void setSelected(boolean selected) {
        v_indicator.setSelected(selected);
    }
}
