package com.opencheck.client.home.merchant.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Address;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.PickerSelectedLayoutBinding;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class CustomGoogleMapFragment extends Fragment implements GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {
    public GoogleMap mMap;
    public static LatLng mCurrentPoint;
    public static Boolean isSetMaker = false;
    protected CustomInfoWindowAdapter mapAdapter;
    protected SupportMapFragment mMapFragment;
    protected MapView mapView;
    protected ArrayList<Marker> listMarker;
    protected Marker mCurrentMarker;
    protected View mVIew;
    private Marker mClickedMarker;
    public static Boolean isFilter = false;

    // public String addressLocal = null;
    // public LatLng latlogLocal = null;

    public double crLat = 1.d;
    public double crLng = 1.d;

    @Override
    public void onResume() {

        super.onResume();
    }

    public void initMarkerListener() {

    }

    @Override
    public void onPause() {

        super.onPause();
//        ((LinearLayout) mVIew.findViewById(R.id.mapviewLinearLayout)).removeAllViews();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        releaseMap();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        crLat = marker.getPosition().latitude;
        crLng = marker.getPosition().longitude;

        return false;
    }

    public void focusStore(int pos) {
        listMarker.get(pos).showInfoWindow();
        crLat = listMarker.get(pos).getPosition().latitude;
        crLng = listMarker.get(pos).getPosition().longitude;
    }

    public void moveToPoints(final List<LatLng> listBounds, final Boolean isMark, final ArrayList<String> listAddress) {
        final View mapView = mMapFragment.getView();
        // try {
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressWarnings("deprecation")
                @SuppressLint("NewApi")
                // We check which build version we are using.
                @Override
                public void onGlobalLayout() {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (int i = 0; i < listBounds.size(); i++) {
                        if (isMark) {
                            mClickedMarker = mMap.addMarker(new MarkerOptions().title(listAddress.get(i)).position(listBounds.get(i)));
                            mClickedMarker.showInfoWindow();
                            crLat = mClickedMarker.getPosition().latitude;
                            crLng = mClickedMarker.getPosition().longitude;
                        }
                        builder.include(listBounds.get(i));
                    }
                    if (listBounds.size() > 0) {
                        LatLngBounds bounds = builder.build();
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                    }
                }
            });
        }
    }

    /**
     * Map will be release when not use
     */
    public void releaseMap() {
        try {
            FragmentManager manager = this.getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            SupportMapFragment frag = ((SupportMapFragment) getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.mapRoot));
            if (null != frag) {
                trans.remove(frag);
            }
            trans.commit();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /*
     * handle google api
     */
    public String makeFindLocationUrl(String key) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.google.com/maps/api/geocode/json");
        urlString.append("?address=");// from
        urlString.append(key);
        urlString.append("&sensor=false");
        Log.d("xxx", "URL=" + urlString.toString());
        return urlString.toString().trim().replace(" ", "%20");
    }

    public void showListLocation(final String key, final LatLng mCurrentPoint, final String srcAddress) {
        try {
//            Helper.showProgressBar((LixiActivity) getActivity());
            ((LixiActivity) getActivity()).showProgressBar();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {

                getListLocation(key, mCurrentPoint, false, srcAddress);
            }
        }).start();
    }

    public void getListLocation(String key, LatLng mCurrentPoint, Boolean isSingleAddress, String srcAddress) {
        // connect to map web service
        ArrayList<Address> listLocation = new ArrayList<Address>();
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(makeFindLocationUrl(key))
                    .build();

            Response response = client.newCall(request).execute();

            JSONObject jsonObject = new JSONObject(response.body().string());
            JSONArray routeArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < routeArray.length(); i++) {
                Address modelNote = new Address(Locale.getDefault());
                JSONObject note = routeArray.getJSONObject(i);
                modelNote.setAddressLine(0, note.getString("formatted_address"));
                JSONObject geometry = note.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                modelNote.setLongitude(location.getDouble("lng"));
                modelNote.setLatitude(location.getDouble("lat"));
                listLocation.add(modelNote);
            }
            if (!isSingleAddress) {
                showLocationPicker(listLocation, mCurrentPoint, srcAddress);
            } else {
                getLatLongFromFristAddress(listLocation);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
//            Helper.hideProgressBar();
            ((LixiActivity)getActivity()).hideProgressBar();
        }
    }

    public void getListAddress(final String key, final ItemClickListener callback) {
        try {
//            Helper.showProgressBar((LixiActivity) getActivity());
            ((LixiActivity) getActivity()).showProgressBar();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Address> listLocation = new ArrayList<Address>();
                try {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(makeFindLocationUrl(key))
                            .build();

                    Response response = client.newCall(request).execute();

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray routeArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < routeArray.length(); i++) {
                        Address modelNote = new Address(Locale.getDefault());
                        JSONObject note = routeArray.getJSONObject(i);
                        modelNote.setAddressLine(0, note.getString("formatted_address"));
                        JSONObject geometry = note.getJSONObject("geometry");
                        JSONObject location = geometry.getJSONObject("location");
                        modelNote.setLongitude(location.getDouble("lng"));
                        modelNote.setLatitude(location.getDouble("lat"));
                        listLocation.add(modelNote);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    callback.onItemCLick(0, ConstantValue.SEARCH_ADDRESS_CALLBACK, listLocation, 0L);
//                    Helper.hideProgressBar();
                    ((LixiActivity)getActivity()).hideProgressBar();
                }
            }
        }).start();
    }

    onSetAddressCallBack setAddressCalback;
    Boolean isDrawPath = false;

    public ArrayList<Address> getListAddress(String key, LatLng mCurrentPoint, String srcAddress, onSetAddressCallBack _setAddressCalback, Boolean _isDrawPath) {
        // connect to map web service
        setAddressCalback = _setAddressCalback;
        isDrawPath = _isDrawPath;
        ArrayList<Address> listLocation = new ArrayList<Address>();
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(makeFindLocationUrl(key))
                    .build();

            Response response = client.newCall(request).execute();

            JSONObject jsonObject = new JSONObject(response.body().string());
            JSONArray routeArray = jsonObject.getJSONArray("results");
            for (int i = 0; i < routeArray.length(); i++) {
                Address modelNote = new Address(Locale.getDefault());
                JSONObject note = routeArray.getJSONObject(i);
                modelNote.setAddressLine(0, note.getString("formatted_address"));
                JSONObject geometry = note.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                modelNote.setLongitude(location.getDouble("lng"));
                modelNote.setLatitude(location.getDouble("lat"));
                listLocation.add(modelNote);
            }
            showLocationPicker(listLocation, mCurrentPoint, srcAddress);
        } catch (Throwable e) {
            e.printStackTrace();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Helper.showErrorDialog((LixiActivity) getActivity(), "Cannot find path to your location!!");
                }
            });
        } finally {
//            Helper.hideProgressBar();
            ((LixiActivity)getActivity()).hideProgressBar();
        }
        return listLocation;
    }

    public void showLocationPicker(final List<Address> results, final LatLng mCurrentPoint, final String srcAddress) {
        final ArrayList<String> listAddressPoint = new ArrayList<String>();
        listAddressPoint.add(srcAddress);
        this.setMakerMap(srcAddress, mCurrentPoint);
        // Check input
        if (results.size() == 0) {
            Helper.showErrorDialog((LixiActivity) getActivity(), "Address not math");
            return;
        }
        // Do not build dialog if the activity is finishing
        if (getActivity().isFinishing()) {
            return;
        }
        // Convert the list of results to display strings
        final String[] items = getAddressStringArray(results);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Display alert
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Did you mean:");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int itemIndex) {
                        // Display the position
                        if (isDrawPath) {
                            listAddressPoint.add(items[itemIndex]);
                            displayLocation(results.get(itemIndex), mCurrentPoint, listAddressPoint);
                        }
                        LatLng latlogChoise = new LatLng(results.get(itemIndex).getLatitude(), results.get(itemIndex).getLongitude());
                        setAddressCalback.setTextEdit(items[itemIndex], latlogChoise);
                        dialogInterface.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    public interface onSetAddressCallBack {
        void setTextEdit(String address, LatLng position);
    }

    public LatLng getLatLongFromFristAddress(final List<Address> results) {
        LatLng point = new LatLng(results.get(0).getLatitude(), results.get(0).getLongitude());
        doAfterGetLatLongFromAddress(point);
        return point;
    }

    public void doAfterGetLatLongFromAddress(LatLng point) {
    }

    public String[] getAddressStringArray(List<Address> results) {
        // Declare
        ArrayList<String> result = new ArrayList<String>();
        String[] resultType = new String[0];
        // Iterate over addresses
        for (int i = 0; i < results.size(); i++) {
            // Get the data
            String formattedAddress = results.get(i).getAddressLine(0);
            if (formattedAddress == null)
                formattedAddress = "";
            try {
                formattedAddress = new String(formattedAddress.getBytes("ISO-8859-1"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e("utf8", "conversion", e);
            }
            result.add(formattedAddress);
        }
        // Return
        if (result.size() == 0) {
            return null;
        } else {
            return (String[]) result.toArray(resultType);
        }
    }

    public void displayLocation(Address address, final LatLng mCurrentPoint, final ArrayList<String> listAddress) {
//        Helper.showProgressBar((LixiActivity) getActivity());
        ((LixiActivity) getActivity()).showProgressBar();
        final LatLng destGeoPoint = new LatLng(address.getLatitude(), address.getLongitude());

        if (null != destGeoPoint) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    DrawPath(mCurrentPoint, destGeoPoint, listAddress);
                }
            }).start();
        }
    }

    @SuppressWarnings("unused")
    protected void DrawPath(final LatLng srcPoint, final LatLng destPoint, final ArrayList<String> listAddress) {
        // connect to map web service
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(this.makeUrl(srcPoint, destPoint))
                    .build();

            Response response = client.newCall(request).execute();

            JSONObject jsonObject = new JSONObject(response.body().string());
            JSONArray routeArray = jsonObject.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            // demo get direction
            JSONArray legs = routes.getJSONArray("legs");
            // Grab first leg
            JSONObject leg = legs.getJSONObject(0);
            JSONObject durationObject = leg.getJSONObject("duration");
            JSONArray steps = leg.getJSONArray("steps");
            // ///
            final int numSteps = steps.length();
            for (int i = 0; i < numSteps; i++) {
                // Get the individual step
                final JSONObject step = steps.getJSONObject(i);
                // Strip html from google directions and set as turn instruction
            }
            // end demo
            final List<LatLng> pointToDraw = this.decodePoly(encodedString);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    drawPath(pointToDraw);
                    List<LatLng> listPoint = new ArrayList<LatLng>();
                    listPoint.add(srcPoint);
                    listPoint.add(destPoint);
                    moveToPoints(listPoint, true, listAddress);
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Helper.showErrorDialog((LixiActivity) getActivity(), "Cannot find path to your location!!");
                }
            });
        } finally {
//            Helper.hideProgressBar();
            ((LixiActivity)getActivity()).hideProgressBar();
        }
    }

    public void drawPath(List<LatLng> listPoint) {
        // List<LatLng> list = new ArrayList<LatLng>();
        // for (int i = 0; i < listPoint.size(); i++) {
        // list.add(new LatLng(listPoint.get(i).getLatitudeE6() / 1E6,
        // listPoint.get(i).getLongitudeE6() / 1E6));
        // }
        mMap.addPolyline(new PolylineOptions().addAll(listPoint).color(Color.BLUE).width(6));
    }

    public List<LatLng> decodePoly(final String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            // GeoPoint p = new GeoPoint((int) ((lat / 1E5) * 1E6), (int) ((lng
            // / 1E5) * 1E6));
            LatLng a = new LatLng((lat / 1E5), (lng / 1E5));
            poly.add(a);
        }
        return poly;
    }

    public String makeUrl(final LatLng src, final LatLng dest) {

        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(src.latitude));
        urlString.append(",");
        urlString.append(Double.toString(src.longitude));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(dest.latitude));
        urlString.append(",");
        urlString.append(Double.toString(dest.longitude));
        urlString.append("&sensor=false");
        Log.d("xxx", "URL=" + urlString.toString());
        return urlString.toString();
    }

    public void startLocationDetail(final int position, final LatLng point) {
    }

    public void initDefaultPoint(LatLng point, Boolean isMove) {
        try {
            mCurrentMarker = mMap.addMarker(new MarkerOptions().position(point).title("Current Position").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));
            if (isMove) {

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, mMap.getCameraPosition().zoom));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void setMakerMap(final String address, final LatLng position) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.clear();
                mClickedMarker = mMap.addMarker(new MarkerOptions().title(address).position(position));
                mClickedMarker.showInfoWindow();
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 11));
                crLat = mClickedMarker.getPosition().latitude;
                crLng = mClickedMarker.getPosition().longitude;
            }
        });
    }

    /**
     * Set multi point on map
     */
    private ItemClickListener itemMapClick;

    public void setMapOverlay(final ArrayList<StoreModel> _listItem, ItemClickListener _itemMapClick) {
        itemMapClick = _itemMapClick;
        ArrayList<StoreModel> listItem = _listItem;
        ArrayList<LatLng> listBounds = new ArrayList<LatLng>();
        listMarker = new ArrayList<Marker>();
        if (listItem.size() > 0) {
            // first overlay
            LatLng point;
            for (int i = 0; i < listItem.size(); i++) {
                try {
                    point = new LatLng(Double.parseDouble(listItem.get(i).getLat()), Double.parseDouble(listItem.get(i).getLng()));
                    listMarker.add(mMap.addMarker(new MarkerOptions().position(point).icon(
                            BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(getActivity(), R.drawable.map_pin, "")))));

//                        listMarker.add(mMap.addMarker(new MarkerOptions().position(point).icon(
//                                BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(getActivity(), R.drawable.icon_map_point, " " + iCount++)))));
                    listBounds.add(point);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        mapAdapter = new CustomInfoWindowAdapter(getActivity(), listItem, true);
        mMap.setInfoWindowAdapter(mapAdapter);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);
        if (listMarker.size() > 0) {
            listMarker.get(0).showInfoWindow();
            crLat = listMarker.get(0).getPosition().latitude;
            crLng = listMarker.get(0).getPosition().longitude;
            mMap.getUiSettings().setMapToolbarEnabled(false);
            //((View) listMarker.get(0).get).performClick();

//            Projection projection = mMap.getProjection();
//            LatLng markerLocation = listMarker.get(0).getPosition();
//            Point screenPosition = projection.toScreenLocation(markerLocation);
//            long downTime = SystemClock.uptimeMillis();
//            long eventTime = SystemClock.uptimeMillis() + 100;
//            final MotionEvent motionEvent = MotionEvent.obtain(
//                    downTime,
//                    eventTime,
//                    MotionEvent.ACTION_DOWN,
//                    screenPosition.x,
//                    screenPosition.y,
//                    0
//            );
//
//            final MotionEvent motionEvent_up = MotionEvent.obtain(
//                    downTime,
//                    eventTime,
//                    MotionEvent.ACTION_UP,
//                    screenPosition.x,
//                    screenPosition.y,
//                    0
//            );
//
//            final View mapView = mMapFragment.getView();
//
//            (new Handler()).postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mapView.dispatchTouchEvent(motionEvent);
//                }
//            }, 2000);
//
//            (new Handler()).postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mapView.dispatchTouchEvent(motionEvent_up);
//                }
//            }, 2286);
        }
//        moveToPoints(listBounds, false, null);
    }

    /**
     * view window custom when click point on map
     */
    class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        // These a both viewgroups containing an ImageView with id "badge" and
        // two
        // TextViews with id
        // "title" and "snippet".
        private final View mWindow;
        private Activity mContext;
        private ArrayList<StoreModel> mData;

        public ArrayList<StoreModel> getDatas() {
            return mData;
        }

        @SuppressWarnings("unused")
        private Boolean isEnableDetailView = false;
        int position;

        CustomInfoWindowAdapter(Activity context, ArrayList<StoreModel> _data, Boolean isEnableDetailView) {
            this.mContext = context;
            this.mData = _data;
            this.isEnableDetailView = isEnableDetailView;
            PickerSelectedLayoutBinding pickerSelectedLayoutBinding =
                    PickerSelectedLayoutBinding.inflate(mContext.getLayoutInflater(),
                            null, false);
            mWindow = pickerSelectedLayoutBinding.getRoot();
        }

        @Override
        public View getInfoWindow(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        StoreModel note;

        private void render(final Marker marker, View view) {
            note = null;
            for (int i = 0; i < mData.size(); i++) {
                if (marker.equals(listMarker.get(i))) {
                    note = mData.get(i);
                    position = i;
                    break;
                }
            }

            LinearLayout ll_maker = (LinearLayout) view.findViewById(R.id.ll_maker);
            TextView tvItem = (TextView) view.findViewById(R.id.tvItem);
            TextView tvName = (TextView) view.findViewById(R.id.tvName);

            ll_maker.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

//                    Toast.makeText(getActivity(), "OnClick", Toast.LENGTH_SHORT).show();

                    Helper.getGPSLocation(getActivity(), new LocationCallBack() {
                        @Override
                        public void callback(LatLng position, ArrayList<String> address) {
                            float lat = (float) position.latitude;
                            float lng = (float) position.longitude;

                            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + lat + "," + lng + "&daddr=" + note.getLat() + "," + note.getLng()));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        }
                    });
                }
            });

            if (null != note && !marker.equals(mCurrentMarker)) {
                tvItem.setText(note.getAddress());
                tvName.setText(note.getName());
                ll_maker.setVisibility(View.VISIBLE);
            } else {
                ll_maker.setVisibility(View.GONE);
            }
        }
    }

    private class FetchImageTask extends AsyncTask<String, Integer, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            Bitmap b = null;
            try {
                b = BitmapFactory.decodeStream((InputStream) new URL(arg0[0]).getContent());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return b;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        StoreModel note = null;
        for (int i = 0; i < mapAdapter.getDatas().size(); i++) {
            if (marker.equals(listMarker.get(i))) {

                note = mapAdapter.getDatas().get(i);

                itemMapClick.onItemCLick(i, ConstantValue.MAP_MARKER_CLICK, note, 0L);

//                ((CustomFragmentActivity) getActivity()).setPassValue(ConstantValue.ID_SHOP, note.getId());
//                ((CustomFragmentActivity) getActivity()).replaceFragment(ConstantValue.SHOP_DETAIL_FRAGMENT);

                break;
            }
        }

//        Toast.makeText(getActivity(), "Macker click", Toast.LENGTH_SHORT).show();

    }


    public String makeUrl(LatLng point) {
        StringBuilder urlString = new StringBuilder();

        urlString.append("http://maps.googleapis.com/maps/api/geocode/json?latlng=");
        urlString.append(point.latitude + "," + point.longitude);
        urlString.append("&sensor=false");
        Log.d("xxx", "URL=" + urlString.toString());
        return urlString.toString().trim().replace(" ", "%20");
    }

    public String updateLocation(String location) {
        try {
            location = new String(location.getBytes("ISO-8859-1"), "UTF-8");
        } catch (Throwable e) {
            location = "";
            e.printStackTrace();
        }
        return location;
    }

    public LatLng getCurrentLocation() {
        // check if GPS enabled
        GPSTracker gps = new GPSTracker(getActivity());
        LatLng srcGeoPoint;
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            srcGeoPoint = new LatLng(latitude, longitude);
        } else {
            // LocationFragment.gps.showSettingsAlert();
            double latitude = 10.77306;
            double longitude = 106.69833;
            srcGeoPoint = new LatLng(latitude, longitude);
        }
        return srcGeoPoint;
    }

    /**
     * draw text into bitmap and return bitmap painted
     *
     * @param mContext
     * @param resourceId
     * @param mText
     * @return
     */
    public Bitmap drawTextToBitmap(Context mContext, int resourceId, String mText) {
        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);
            Bitmap.Config bitmapConfig = bitmap.getConfig();
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888;
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true);
            Canvas canvas = new Canvas(bitmap);
            // new antialised Paint
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            // text color - #3D3D3D
            paint.setColor(Color.WHITE);
            // text size in pixels
            paint.setTextSize((int) (12 * scale));
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
            // draw text to the Canvas center
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);
            int x = (bitmap.getWidth() - bounds.width()) / 6;
            int y = (bitmap.getHeight() + bounds.height()) / 4;
            canvas.drawText(mText, x * scale, y * scale, paint);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public void moveToPoint(LatLng location, final int zoom) {
//        final CameraUpdate center = CameraUpdateFactory.newLatLng(location);
//        CameraUpdate cameraZoom = CameraUpdateFactory.zoomTo(zoom);
//        mMap.moveCamera(center);
//        mMap.animateCamera(cameraZoom);

        CameraPosition camPosition = mMap.getCameraPosition();
        if (!((Math.floor(camPosition.target.latitude * 100) / 100) == (Math.floor(location.latitude * 100) / 100) && (Math.floor(camPosition.target.longitude * 100) / 100) == (Math.floor(location.longitude * 100) / 100))) {
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.animateCamera(CameraUpdateFactory.newLatLng(location), new GoogleMap.CancelableCallback() {

                @Override
                public void onFinish() {
                    mMap.getUiSettings().setScrollGesturesEnabled(true);
                    CameraUpdate cameraZoom = CameraUpdateFactory.zoomTo(zoom);
                    mMap.animateCamera(cameraZoom);
                }

                @Override
                public void onCancel() {
                    mMap.getUiSettings().setAllGesturesEnabled(true);
                }
            });
        }
    }
}