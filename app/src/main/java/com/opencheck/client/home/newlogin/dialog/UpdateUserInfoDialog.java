package com.opencheck.client.home.newlogin.dialog;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.DialogUpdateUserInfoBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.home.account.new_layout.fragment.UserFragment;
import com.opencheck.client.home.newlogin.communicate.OnLoginResult;
import com.opencheck.client.home.newlogin.communicate.OnPickDateListener;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;

public class UpdateUserInfoDialog extends LixiDialog {

    private String phone;

    public UpdateUserInfoDialog(@NonNull LixiActivity _activity, String phone) {
        super(_activity, false, true, false);

        this.phone = phone;
    }

    @Override
    protected String getScreenName() {
        return TrackEvent.SCREEN.LOGIN_UPDATE_INFOR;
    }

    private DialogUpdateUserInfoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogUpdateUserInfoBinding.inflate(LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        initData();

        mBinding.btnBack.setOnClickListener(this);
        mBinding.cbShowPass.setOnClickListener(this);
        mBinding.txtConfirm.setOnClickListener(this);
        mBinding.imgCalendar.setOnClickListener(this);
        mBinding.edtBirthbay.setOnClickListener(this);

        Helper.setAllCapFilter(mBinding.edtIntroCode);
    }

    private void initData() {
        mBinding.txtPhone.setText(phone);
        mBinding.txtIntroCode.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        if (UserModel.getInstance() != null) {
            if (UserModel.getInstance().getSource().equals("facebook")) {
                mBinding.edtName.setText(UserModel.getInstance().getFullname() + "");
                mBinding.edtEmail.setText(UserModel.getInstance().getEmail() + "");
                if (UserModel.getInstance().getGender() != null) {
                    mBinding.switchGender.post(new Runnable() {
                        @Override
                        public void run() {
                            if (UserModel.getInstance().getGender().equals("male")) {
                                mBinding.switchGender.setCheck(false);
                            } else {
                                mBinding.switchGender.setCheck(true);
                            }
                        }
                    });
                }

                mBinding.edtName.setSelection(mBinding.edtName.getText().length());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.cbShowPass:
                if (mBinding.cbShowPass.isChecked()) {
                    // show
                    mBinding.edtPass.setInputType(InputType.TYPE_CLASS_TEXT);
                    mBinding.edtPass.setTransformationMethod(new SingleLineTransformationMethod());
                    mBinding.edtPass.setSelection(mBinding.edtPass.getText().length());
                    mBinding.cbShowPass.setText(activity.getString(R.string.login_hide_pass));
                } else {
                    // hide
                    mBinding.edtPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mBinding.edtPass.setTransformationMethod(new PasswordTransformationMethod());
                    mBinding.edtPass.setSelection(mBinding.edtPass.getText().length());
                    mBinding.cbShowPass.setText(activity.getString(R.string.login_show_pass));
                }
                mBinding.edtPass.setFocusable(true);
                break;
            case R.id.txtConfirm:
                checkInfo();
                break;
            case R.id.imgCalendar:
            case R.id.edtBirthbay:
                DatePickerDialog dialog = new DatePickerDialog(activity);
                dialog.show();
                dialog.setOnPickListener(new OnPickDateListener() {
                    @Override
                    public void onPicked(String date) {
                        mBinding.edtBirthbay.setText(date);
                    }
                });
                break;
        }
    }

    private void checkInfo() {
        final String name = mBinding.edtName.getText().toString().trim() + "";
        final String email = mBinding.edtEmail.getText().toString().trim() + "";
        final String pass = mBinding.edtPass.getText().toString() + "";
        final String date = mBinding.edtBirthbay.getText().toString().trim() + "";
        final String code = mBinding.edtIntroCode.getText().toString().trim();
        final String gender = mBinding.switchGender.isCheck() ? "female" : "male";

        // check full input
        if (name.isEmpty() || pass.isEmpty() || date.isEmpty()) {
//            Toast.makeText(activity, LanguageBinding.getString(R.string.login_error_miss_input_info, activity), Toast.LENGTH_SHORT).show();
            if (name.isEmpty()) {
                Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_name_null, activity), Toast.LENGTH_SHORT).show();
                return;
            } else if (pass.isEmpty()) {
                Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_pass_null, activity), Toast.LENGTH_SHORT).show();
                return;
            } else {
                Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_birth_null, activity), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        // check valid pass
        int length = pass.length();
        if (length < 6 || length > 32) {
            Toast.makeText(activity, LanguageBinding.getString(R.string.update_error_invalid_pass, activity), Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            JsonObject param = new JsonObject();
            param.addProperty("birthday", date);
            if (code.length() > 0) {
                param.addProperty("code", code);
                GlobalTracking.trackReferenceCodeSubmitEvent();
            }
//            param.addProperty("email", email);
            param.addProperty("gender", gender);
            param.addProperty("name", name);
            param.addProperty("password", pass);

            GlobalTracking.trackSubmitUpdateInfoEvent();
            DataLoader.putUpdateUserInfo(activity, new ApiCallBack() {
                @Override
                public void handleCallback(final boolean isSuccess, final Object object) {
                    if (isSuccess) {
                        GlobalTracking.trackUpdateInfoSuccess();
                        if (code.length() > 0) {
                            GlobalTracking.trackReferenceCodeSuccessEvent();
                        }
                        UpdateSuccessDialog updateSuccessDialog = new UpdateSuccessDialog(activity, phone);
                        updateSuccessDialog.show();
//                        (new AppPreferenceHelper(activity)).getRemindUpdateTime()
                        updateSuccessDialog.setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                if (onLoginResult != null) {
                                    onLoginResult.onLogin(true, object);
                                    UpdatePasswordDialog.getInstance(activity).cancel();
                                    UserFragment.UPDATE_USER_INFO = true;
                                } else {
                                    Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                                }
                                cancel();
                            }
                        });
                    } else {
                        Toast.makeText(activity, object.toString() + "", Toast.LENGTH_SHORT).show();
                    }
                }
            }, param);
        } catch (NullPointerException e) {
            Log.d("error", e.getMessage());
        }
    }

    private OnLoginResult onLoginResult;

    public void setOnUpdateInfo(OnLoginResult onLoginResult) {
        this.onLoginResult = onLoginResult;
    }

    @Override
    public void onBackPressed() {
        if (onLoginResult != null) {
            onLoginResult.onLogin(false, null);
        }
        activity.hideKeyboard();
        super.onBackPressed();
    }

    @Override
    public void cancel() {
        super.cancel();
        UpdatePasswordDialog.getInstance(activity).setCanShow(true);
    }

    @Override
    public void show() {
        super.show();
        GlobalTracking.trackStartUpdateInfo();
    }
}
