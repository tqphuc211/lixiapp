package com.opencheck.client.home.delivery.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.CirclePagerIndicator.CirclePageIndicator;
import com.opencheck.client.databinding.ItemBannerLayoutBinding;
import com.opencheck.client.databinding.ItemCategoryLayoutBinding;
import com.opencheck.client.databinding.ItemCollectionLayoutBinding;
import com.opencheck.client.databinding.ItemProductMerchantHotLayoutBinding;
import com.opencheck.client.databinding.ItemRecommendStoreLayoutBinding;
import com.opencheck.client.databinding.ItemTabLayoutBinding;
import com.opencheck.client.databinding.PlaceItemLayoutBinding;
import com.opencheck.client.home.delivery.activities.ProductMerchantHotActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.RecommendActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.libs.CustomViewPager;
import com.opencheck.client.home.delivery.model.ArticleModel;
import com.opencheck.client.home.delivery.model.DeliBannerModel;
import com.opencheck.client.home.delivery.model.ProductMerchantHot;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.load_image.ImageSize;
import com.opencheck.client.utils.load_image.LixiImage;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class DeliveryHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LixiActivity activity;

    //Initialize arrays
    private ArrayList<StoreOfCategoryModel> listMainStore;
    private ArrayList<StoreOfCategoryModel> listRecommend;
    private ArrayList<ProductMerchantHot> listProductMerchantHot;
    private ArrayList<DeliBannerModel> listBanner;
    private ArrayList<ArticleModel> listArticle;
    private ArrayList<ArticleModel> listCategory;

    //Initialize adapter
    private CategoryAdapter categoryAdapter;
    private DeliBannerAdapter bannerAdapter;
    private RecommendStoreAdapter recommendStoreAdapter;
    private ProductMerchantHotAdapter productMerchantHotAdapter;

    //Initialize view holder
    private BannerHolder bannerHolder;
    private CategoryHolder categoryHolder;
    private ArticleHolder articleHolder;
    private RecommendStoreHolder recommendStoreHolder;
    private ProductMerchantHotHolder productMerchantHotHolder;
    private TabHolder tabHolder;

    private final int BANNER = 0;
    private final int CATEGORY = 1;
    private final int ARTICLE = 2;
    private final int RECOMMEND_STORE = 3;
    private final int PRODUCT_MERCHANT_HOT = 4;
    private final int TAB_LAYOUT = 5;
    private final int MAIN_STORE = 6;

    private int curTab = 0;
    private int count = 0;
    private boolean isClickedTab = false;

    //Initialize event listener
    private OnTabClickListener onTabClickListener;

    public DeliveryHomeAdapter(LixiActivity activity, ArrayList<StoreOfCategoryModel> listMainStore) {
        this.activity = activity;
        this.listMainStore = listMainStore;
        notifyDataSetChanged();
    }

    public ArrayList<StoreOfCategoryModel> getListItem() {
        return listMainStore;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("mapi", "create view item: " + count++);
        switch (viewType) {
            case BANNER: {
                ItemBannerLayoutBinding itemBannerLayoutBinding =
                        ItemBannerLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                                null, false);

                bannerHolder = new BannerHolder(itemBannerLayoutBinding.getRoot());
                int height31 = activity.widthScreen * 4 / 15;
                ViewGroup.LayoutParams layoutParams = bannerHolder.mViewPager.getLayoutParams();
                layoutParams.height = height31;
                bannerHolder.mViewPager.setLayoutParams(layoutParams);
                bannerAdapter = new DeliBannerAdapter(activity, listBanner);
                bannerHolder.mViewPager.setAdapter(bannerAdapter);
                bannerHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        bannerCurrentPage = position;
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                bannerHolder.mIndicator.setViewPager(bannerHolder.mViewPager);
                bannerHolder.mIndicator.requestLayout();
                return bannerHolder;
            }
            case CATEGORY: {
                ItemCategoryLayoutBinding itemCategoryLayoutBinding =
                        ItemCategoryLayoutBinding.inflate(LayoutInflater
                                .from(parent.getContext()), null, false);
                categoryHolder = new CategoryHolder(itemCategoryLayoutBinding.getRoot());
                categoryAdapter = new CategoryAdapter(activity, listCategory);
                categoryHolder.rvCategory.setAdapter(categoryAdapter);
                return categoryHolder;

            }
            case ARTICLE: {
                ItemCollectionLayoutBinding collectionLayoutBinding =
                        ItemCollectionLayoutBinding.inflate(LayoutInflater.from(
                                parent.getContext()), null, false);
                articleHolder = new ArticleHolder(collectionLayoutBinding.getRoot());
                return articleHolder;
            }
            case RECOMMEND_STORE: {
                ItemRecommendStoreLayoutBinding itemRecommendStoreLayoutBinding =
                        ItemRecommendStoreLayoutBinding.inflate(
                                LayoutInflater.from(parent.getContext()), null, false);
                recommendStoreHolder = new RecommendStoreHolder(
                        itemRecommendStoreLayoutBinding.getRoot());
                recommendStoreAdapter = new RecommendStoreAdapter(activity, listRecommend);
                recommendStoreHolder.rvRecommendStore.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                recommendStoreHolder.rvRecommendStore.setAdapter(recommendStoreAdapter);

                recommendStoreHolder.llAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, RecommendActivity.class);
                        activity.startActivity(intent);
                    }
                });
                return recommendStoreHolder;
            }
            case PRODUCT_MERCHANT_HOT:
                ItemProductMerchantHotLayoutBinding itemProductMerchantHotLayoutBinding =
                        ItemProductMerchantHotLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), null, false);
                productMerchantHotHolder = new ProductMerchantHotHolder(itemProductMerchantHotLayoutBinding.getRoot());
                productMerchantHotAdapter = new ProductMerchantHotAdapter(activity, listProductMerchantHot);
                productMerchantHotHolder.rvProductMerchantHot.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                productMerchantHotHolder.rvProductMerchantHot.setAdapter(productMerchantHotAdapter);

                productMerchantHotHolder.llAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, ProductMerchantHotActivity.class);
                        activity.startActivity(intent);
                    }
                });
                return productMerchantHotHolder;
            case TAB_LAYOUT: {
                ItemTabLayoutBinding itemTabLayoutBinding = ItemTabLayoutBinding
                        .inflate(LayoutInflater.from(parent.getContext()),
                                null, false);
                tabHolder = new TabHolder(itemTabLayoutBinding.getRoot());

                tabHolder.llHotStore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onTabClickListener != null)
                            onTabClickListener.onHotTabClick();
                    }
                });

                tabHolder.llFastestStore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onTabClickListener != null)
                            onTabClickListener.onFastestTabClick();
                    }
                });

                tabHolder.llBestPriceStore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onTabClickListener != null)
                            onTabClickListener.onBestPriceTabClick();
                    }
                });
                return tabHolder;
            }
            case MAIN_STORE: {
                PlaceItemLayoutBinding placeItemLayoutBinding =
                        PlaceItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                                null, false);
                return new MainStoreHolder(placeItemLayoutBinding.getRoot());
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == BANNER) {
            bannerHolder = (BannerHolder) holder;
            if (listBanner != null && listBanner.size() > 0)
                bannerHolder.rlRoot.setVisibility(View.VISIBLE);
            else
                bannerHolder.rlRoot.setVisibility(View.GONE);
        }

        if (position == CATEGORY) {
            categoryHolder = (CategoryHolder) holder;
            if (listCategory == null || listCategory.size() == 0)
                categoryHolder.llRoot.setVisibility(View.GONE);
            else
                categoryHolder.llRoot.setVisibility(View.VISIBLE);
        }

        if (position == ARTICLE) {
            articleHolder = (ArticleHolder) holder;

            if (listArticle != null) {
                Log.d("size", listArticle.size() + "");
                if (listArticle.size() > 0) {
                    articleHolder.llRoot.setVisibility(View.VISIBLE);
                    LixiImage.with(activity)
                            .load(listArticle.get(0).getImage_link())
                            .size(ImageSize.AUTO)
                            .into(articleHolder.iv_1);
                } else
                    articleHolder.llRoot.setVisibility(View.GONE);

                if (listArticle.size() > 1) {
                    LixiImage.with(activity)
                            .load(listArticle.get(1).getImage_link())
                            .size(ImageSize.AUTO)
                            .into(articleHolder.iv_2);
                }
                if (listArticle.size() > 2) {
                    articleHolder.iv_3.setVisibility(View.VISIBLE);
                    LixiImage.with(activity)
                            .load(listArticle.get(2).getImage_link())
                            .size(ImageSize.AUTO)
                            .into(articleHolder.iv_3);
                } else articleHolder.iv_3.setVisibility(View.GONE);

            } else {
                articleHolder.llRoot.setVisibility(View.GONE);
            }
        }

        if (position == RECOMMEND_STORE) {
            recommendStoreHolder = (RecommendStoreHolder) holder;
        }

        if(position == PRODUCT_MERCHANT_HOT) {
            productMerchantHotHolder = (ProductMerchantHotHolder) holder;
            if(listProductMerchantHot != null && listProductMerchantHot.size() > 0) {
                productMerchantHotHolder.llRoot.setVisibility(View.VISIBLE);
                productMerchantHotHolder.llRoot.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            } else {
                productMerchantHotHolder.llRoot.setVisibility(View.GONE);
                productMerchantHotHolder.llRoot.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            }
        }

        if (position == TAB_LAYOUT) {
            final TabHolder tabHolder = (TabHolder) holder;
            tabSelected(curTab);
        }

        if (position >= MAIN_STORE) {
            MainStoreHolder mainStoreHolder = (MainStoreHolder) holder;
            if (listMainStore.size() <= position - MAIN_STORE)
                return;
            final StoreOfCategoryModel store = listMainStore.get(position - MAIN_STORE);

//            ImageLoader.getInstance().displayImage(store.getCover_link(), mainStoreHolder.iv_avatar, LixiApplication.getInstance().optionsNomal);
            LixiImage.with(activity)
                    .load(store.getCover_link())
                    .size(ImageSize.AUTO)
                    .into(mainStoreHolder.iv_avatar);

            mainStoreHolder.tv_name.setText(store.getName());
            mainStoreHolder.tv_time.setText(store.getShipping_duration_min() + "-" + store.getShipping_duration_max() + " " + LanguageBinding.getString(R.string.minute, activity));
            if (store.getShipping_distance() >= 100) {
                double distance = (double) Math.round(((float) store.getShipping_distance() / 1000) * 10) / 10;
                mainStoreHolder.tv_distance.setText(String.valueOf(distance) + " km");
            } else
                mainStoreHolder.tv_distance.setText(String.valueOf(store.getShipping_distance()) + " m");

            if (store.getPromotion_text() != null && store.getPromotion_text().length() > 0) {
                mainStoreHolder.ll_promotion.setVisibility(View.VISIBLE);
                mainStoreHolder.tv_promotion.setText(store.getPromotion_text());
                mainStoreHolder.tv_name.setMaxLines(1);
            } else {
                mainStoreHolder.tv_name.setMaxLines(2);
                mainStoreHolder.ll_promotion.setVisibility(View.GONE);
            }

            StringBuilder builder = new StringBuilder();
            if(store.getList_tag().size() > 0) {
                mainStoreHolder.tv_tags.setVisibility(View.VISIBLE);
            } else {
                mainStoreHolder.tv_tags.setVisibility(View.GONE);
            }
            for (int i = 0; i < store.getList_tag().size(); i++) {
                builder.append("#").append(store.getList_tag().get(i).getName());
                if (i < store.getList_tag().size() - 1)
                    builder.append(".");
            }
            mainStoreHolder.tv_tags.setText(builder.toString());

            if (store.isIs_opening()) {
                mainStoreHolder.tv_open_time.setVisibility(View.GONE);
                mainStoreHolder.iv_store_close.setVisibility(View.GONE);
                mainStoreHolder.iv_avatar.setColorFilter(null);
                mainStoreHolder.tv_shipping_fee.setVisibility(View.VISIBLE);
            } else {
                mainStoreHolder.tv_open_time.setText(String.format(LanguageBinding.getString(R.string.open_close_time, activity), store.getOpen_time_text()));
                mainStoreHolder.tv_open_time.setVisibility(View.VISIBLE);
                mainStoreHolder.iv_store_close.setVisibility(View.VISIBLE);
                mainStoreHolder.iv_avatar.setColorFilter(Color.parseColor("#CC000000"));
                mainStoreHolder.tv_shipping_fee.setVisibility(View.GONE);
            }

            if (store.getMin_money_to_free_ship_price() == null)
                mainStoreHolder.tv_shipping_fee.setVisibility(View.GONE);
            else {
                mainStoreHolder.tv_shipping_fee.setVisibility(View.VISIBLE);
                mainStoreHolder.tv_shipping_fee.setText(LanguageBinding.getString(R.string.store_detail_free_ship, activity));
            }

            int width = (int) (activity.widthScreen - 4 * activity.getResources().getDimension(R.dimen.value_1)) / 25 * 10;
            ViewGroup.LayoutParams params = mainStoreHolder.iv_avatar.getLayoutParams();
            params.width = width;
            params.height = 3 * width / 4;
            mainStoreHolder.iv_avatar.setLayoutParams(params);
            mainStoreHolder.tv_open_time.setMaxWidth(width);

            mainStoreHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StoreDetailActivity.startStoreDetailActivity(activity, store.getId(), TrackingConstant.ContentSource.HOME);
                    isClickedTab = true;
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position > TAB_LAYOUT ? MAIN_STORE : position;
    }

    @Override
    public int getItemCount() {
        return listMainStore == null ? 6 : listMainStore.size() + 6;
    }

    //region SET CHANGE DATA
    public void setListBanner(final ArrayList<DeliBannerModel> listBanner) {
        this.listBanner = listBanner;
        if (bannerAdapter != null) {
            bannerAdapter.setListBanner(listBanner);
            bannerHolder.rlRoot.setVisibility(View.VISIBLE);
            bannerAdapter.notifyDataSetChanged();
        }

        if (bannerHolder != null) {
            bannerHolder.mIndicator.setViewPager(bannerHolder.mViewPager);
            bannerHolder.mIndicator.requestLayout();
        }

        if (listBanner != null && listBanner.size() > 0) {
            if (listBanner.size() == 1) {
                if (bannerHolder != null) {
                    bannerHolder.mIndicator.setVisibility(View.INVISIBLE);
                }
            } else {
                if (bannerHolder != null) {
                    bannerHolder.mIndicator.setVisibility(View.VISIBLE);
                }
                setUpChangingBannerPeriod();
            }
        }
        notifyDataSetChanged();
    }

    public void setListCategory(ArrayList<ArticleModel> listCategory) {
        Log.d("mapi", "set up list=" + listCategory.size());
        this.listCategory = listCategory;
        if (categoryAdapter != null) {
            categoryAdapter.setListCategory(listCategory);
            categoryAdapter.notifyDataSetChanged();
        }
        notifyDataSetChanged();
    }

    public void setListArticle(ArrayList<ArticleModel> listArticle) {
        this.listArticle = listArticle;
        notifyDataSetChanged();
    }

    public void setListRecommend(ArrayList<StoreOfCategoryModel> listRecommend) {
        this.listRecommend = listRecommend;
        if (recommendStoreAdapter != null) {
            recommendStoreAdapter.setListRecommendStore(this.listRecommend);
        }
        notifyDataSetChanged();
    }

    public void setListProductMerchantHot(ArrayList<ProductMerchantHot> listProductMerchantHot) {
        this.listProductMerchantHot = listProductMerchantHot;
        if (productMerchantHotAdapter != null) {
            productMerchantHotAdapter.setListProductMerchantHot(this.listProductMerchantHot);
        }
        notifyDataSetChanged();
    }

    public void setListMainStore(ArrayList<StoreOfCategoryModel> listMainStore) {
        if (this.listMainStore == null)
            this.listMainStore = listMainStore;
        else
            this.listMainStore.addAll(listMainStore);
        notifyDataSetChanged();
    }

    public int getCurrentTab() {
        return curTab;
    }

    public void setCurrentTab(int curTab) {
        this.curTab = curTab;
        tabSelected(curTab);
    }

    //endregion

    private int bannerCurrentPage = 0;
    private Handler bannerHandler = new Handler();
    private Runnable bannerRunnable = new Runnable() {
        @Override
        public void run() {
            if (bannerHolder != null && listBanner.size() > 1) {
                if (bannerCurrentPage >= bannerAdapter.getCount()) {
                    bannerCurrentPage = 0;
                }
                if (!bannerHolder.isTouching) {
                    bannerHolder.mViewPager.setCurrentItem(bannerCurrentPage);
                    bannerCurrentPage++;
                }
                bannerHolder.isTouching = false;
                bannerHandler.postDelayed(this, 5000);
            }
        }
    };

    private void setUpChangingBannerPeriod() {
        bannerHandler.removeCallbacks(bannerRunnable);
        bannerHandler.post(bannerRunnable);
    }

    //region SET VIEW HOLDER
    class BannerHolder extends RecyclerView.ViewHolder {
        private CustomViewPager mViewPager;
        private CirclePageIndicator mIndicator;
        private RelativeLayout rlRoot;
        private boolean isTouching;

        public BannerHolder(View itemView) {
            super(itemView);
            mViewPager = itemView.findViewById(R.id.view_pager);
            mIndicator = itemView.findViewById(R.id.indicator);
            rlRoot = itemView.findViewById(R.id.rlRoot);
            isTouching = false;
            mViewPager.setListener(new CustomViewPager.OnSwipeOutListener() {
                @Override
                public void onSwipeOut() {
                    isTouching = true;
                }
            });
        }
    }

    class CategoryHolder extends RecyclerView.ViewHolder {
        private LinearLayout llRoot;
        private RecyclerView rvCategory;
        private View v_sapce;

        public CategoryHolder(View itemView) {
            super(itemView);
            llRoot = itemView.findViewById(R.id.llRoot);
            rvCategory = itemView.findViewById(R.id.rvCategory);
            v_sapce = itemView.findViewById(R.id.v_sapce);
            rvCategory.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    class ArticleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout llRoot;
        private View rootView;
        private ImageView iv_1;
        private ImageView iv_2;
        private ImageView iv_3;

        public ArticleHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            llRoot = itemView.findViewById(R.id.llRoot);
            iv_1 = itemView.findViewById(R.id.iv_1);
            iv_2 = itemView.findViewById(R.id.iv_2);
            iv_3 = itemView.findViewById(R.id.iv_3);

            iv_1.setOnClickListener(this);
            iv_2.setOnClickListener(this);
            iv_3.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String imageLink = "", title = "";
            long id = 0L;
            if (v == iv_1) {
                id = listArticle.get(0).getId();
                imageLink = listArticle.get(0).getCover_link();
                title = listArticle.get(0).getName();
            } else if (v == iv_2) {
                id = listArticle.get(1).getId();
                imageLink = listArticle.get(1).getCover_link();
                title = listArticle.get(1).getName();
            } else if (v == iv_3) {
                id = listArticle.get(2).getId();
                imageLink = listArticle.get(2).getCover_link();
                title = listArticle.get(2).getName();
            }
            StoreListOfArticleActivity.startArticleActivity(activity, 0L, id, imageLink, title, TrackingConstant.ContentSource.HOME);
        }
    }

    class RecommendStoreHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvRecommendStore;
        private LinearLayout llAll;

        public RecommendStoreHolder(View itemView) {
            super(itemView);
            llAll = itemView.findViewById(R.id.llAll);
            rvRecommendStore = itemView.findViewById(R.id.rvRecommendStore);
        }
    }

    class ProductMerchantHotHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvProductMerchantHot;
        private LinearLayout llAll;
        private LinearLayout llRoot;

        public ProductMerchantHotHolder(View itemView) {
            super(itemView);
            rvProductMerchantHot = itemView.findViewById(R.id.rvProductMerchantHot);
            llAll = itemView.findViewById(R.id.llAll);
            llRoot = itemView.findViewById(R.id.ll_root);
        }
    }

    class TabHolder extends RecyclerView.ViewHolder {
        private LinearLayout llHotStore, llFastestStore, llBestPriceStore;
        private TextView tvHotStore, tvFastestStore, tvBestPriceStore;
        private View vHotStore, vFastestStore, vBestPriceStore;

        public TabHolder(View itemView) {
            super(itemView);
            llHotStore = itemView.findViewById(R.id.llHotStore);
            llFastestStore = itemView.findViewById(R.id.llFastestStore);
            llBestPriceStore = itemView.findViewById(R.id.llBestPriceStore);
            tvHotStore = itemView.findViewById(R.id.tvHotStore);
            tvFastestStore = itemView.findViewById(R.id.tvFastestStore);
            tvBestPriceStore = itemView.findViewById(R.id.tvBestPriceStore);
            vHotStore = itemView.findViewById(R.id.vHotStore);
            vFastestStore = itemView.findViewById(R.id.vFastestStore);
            vBestPriceStore = itemView.findViewById(R.id.vBestPriceStore);
        }
    }

    class MainStoreHolder extends RecyclerView.ViewHolder {
        private ImageView iv_avatar, iv_store_close;
        private TextView tv_name, tv_time, tv_distance, tv_tags, tv_shipping_fee, tv_promotion, tv_open_time;
        private LinearLayout ll_promotion;
        private View rootView;
        private CardView card_view;

        public MainStoreHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            iv_avatar = itemView.findViewById(R.id.iv_avatar);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_tags = itemView.findViewById(R.id.tv_tags);
            tv_shipping_fee = itemView.findViewById(R.id.tv_shipping_fee);
            tv_promotion = itemView.findViewById(R.id.tv_promotion);
            ll_promotion = itemView.findViewById(R.id.ll_promotion);
            iv_store_close = itemView.findViewById(R.id.iv_store_close);
            tv_open_time = itemView.findViewById(R.id.tv_open_time);
            card_view = itemView.findViewById(R.id.card_view);
        }
    }

    //endregion

    //region SET EVENT LISTENER
    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        this.onTabClickListener = onTabClickListener;
    }

    //endregion

    //region SET CHANGE TAB LAYOUT
    public void tabSelected(int mPosition) {
        switch (mPosition) {
            case 0:
                if (tabHolder != null) {
                    tabHolder.tvHotStore.setTextColor(activity.getResources().getColor(R.color.app_violet));
                    tabHolder.tvFastestStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                    tabHolder.tvBestPriceStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));

                    tabHolder.vHotStore.setVisibility(View.VISIBLE);
                    tabHolder.vFastestStore.setVisibility(View.GONE);
                    tabHolder.vBestPriceStore.setVisibility(View.GONE);
                }
                break;

            case 1:
                tabHolder.tvHotStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                tabHolder.tvFastestStore.setTextColor(activity.getResources().getColor(R.color.app_violet));
                tabHolder.tvBestPriceStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));

                tabHolder.vHotStore.setVisibility(View.GONE);
                tabHolder.vFastestStore.setVisibility(View.VISIBLE);
                tabHolder.vBestPriceStore.setVisibility(View.GONE);
                break;

            case 2:
                tabHolder.tvHotStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                tabHolder.tvFastestStore.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                tabHolder.tvBestPriceStore.setTextColor(activity.getResources().getColor(R.color.app_violet));

                tabHolder.vHotStore.setVisibility(View.GONE);
                tabHolder.vFastestStore.setVisibility(View.GONE);
                tabHolder.vBestPriceStore.setVisibility(View.VISIBLE);
                break;

        }
    }
    //endregion

    public interface OnTabClickListener {
        void onHotTabClick();

        void onFastestTabClick();

        void onBestPriceTabClick();
    }
}
