package com.opencheck.client.home.rating.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.opencheck.client.R;
import com.opencheck.client.databinding.ViewStateVoucherBinding;

public class StateVoucherView extends FrameLayout {
    public static final String STATE_CAN_ACTIVE = "can_active";
    public static final String STATE_USED = "used";
    public static final String STATE_EXPIRED = "expired";
    public static final String STATE_WAITING = "wating";

    // Var
    String state = "can_active";

    public StateVoucherView(@NonNull Context context) {
        super(context);
        initView(context);
    }

    public StateVoucherView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
        initData(context, attrs);
    }

    public StateVoucherView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
        initData(context, attrs);
    }

    private ViewStateVoucherBinding mBinding;

    private void initView(Context context){
        mBinding = ViewStateVoucherBinding.inflate(LayoutInflater.from(getContext()),
                this, true);
    }

    private void initData(Context context, AttributeSet attrs){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StateVoucherView);
        state = a.getString(a.getIndex(0));
        if (state == null || state.equals("")){
            state = STATE_CAN_ACTIVE;
        }
        operateState();
    }

    public void setState(String state){
        this.state = state;
        operateState();
    }

    public String getState(){
        return state;
    }

    private void operateState(){
        switch (state){
            case STATE_CAN_ACTIVE:
            case STATE_WAITING:
                mBinding.parentView.setVisibility(GONE);
                break;
            case STATE_EXPIRED:
                mBinding.parentView.setVisibility(VISIBLE);
                mBinding.imgExpired.setVisibility(VISIBLE);
                mBinding.imgUsed.setVisibility(GONE);
                break;
            case STATE_USED:
                mBinding.parentView.setVisibility(VISIBLE);
                mBinding.imgExpired.setVisibility(GONE);
                mBinding.imgUsed.setVisibility(VISIBLE);
                break;
        }
    }
}
