package com.opencheck.client.home.account;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.models.user.ImageModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by Tony Tuan on 04/02/2018.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private LixiActivity activity;
    private ArrayList<ImageModel> arrImg;
    private int layoutItem;
    private int widthScreen = 0;
    private ItemClickListener itemClickListener;
    private DisplayImageOptions optionsNomal;

    public ImageAdapter(LixiActivity _activity, ArrayList<ImageModel> arrImg, int resource) {
        this.activity = _activity;
        this.layoutItem = resource;
        this.arrImg = arrImg;
        this.widthScreen = activity.widthScreen;
        this.optionsNomal = new DisplayImageOptions.Builder().showStubImage(R.drawable.photo_tran).cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(300))
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(layoutItem, parent, false);
        return new ViewHolder(view);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ImageModel item = arrImg.get(position);

        holder.imageView.getLayoutParams().width = widthScreen / 3;
        holder.imageView.getLayoutParams().height = widthScreen / 3 + 50;
        holder.imageView.requestFocus();

        if (item.isSelected) {
            holder.imageView.setAlpha((float) 0.5);
            holder.fr.setForeground(activity.getResources().getDrawable(R.drawable.ic_cab_done_mtrl_alpha));

        } else {
            holder.imageView.setAlpha((float) 1.0);
            holder.fr.setForeground(null);
        }

        ImageLoader.getInstance().displayImage("file://" + item.path, holder.imageView, optionsNomal);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemCLick(position, ConstantValue.ON_ITEM_CLICK, null, 0L);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrImg.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private FrameLayout fr;
        private View rootView;
        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.image_view_image_select);
            fr = (FrameLayout) itemView.findViewById(R.id.fr);
        }

    }
}
