package com.opencheck.client.home.flashsale.Service;

import android.content.Context;

import com.opencheck.client.utils.AppPreferenceHelper;

import java.util.Calendar;

public class OffsetTimer {
    public static long offsetTime = -1;
    public static String OFFSET_TIME = "offset time";

    public static void initOffset(long serverTime, Context context){
        offsetTime = serverTime * 1000 - Calendar.getInstance().getTimeInMillis();
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(context);
        appPreferenceHelper.setOffsetTime(offsetTime);
    }

    public static long getCurrentTimeMillis(){
        return Calendar.getInstance().getTimeInMillis() + offsetTime;
    }

    public static long getCurrentTimeMillis(long ost){
        return Calendar.getInstance().getTimeInMillis() + ost;
    }
}
