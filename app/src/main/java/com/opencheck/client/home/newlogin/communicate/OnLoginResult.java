package com.opencheck.client.home.newlogin.communicate;

public interface OnLoginResult {
    void onLogin(boolean result, Object obj);
}
