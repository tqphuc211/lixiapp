package com.opencheck.client.home.delivery.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.transition.Slide;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.custom.SearchBar;
import com.opencheck.client.databinding.ActivityPromotionCodeUserGroupBinding;
import com.opencheck.client.databinding.FragmentStoreBottomSheetDialogBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.delivery.adapter.PlaceSmallAdapter;
import com.opencheck.client.home.delivery.adapter.PromotionListUserGroupAdapter;
import com.opencheck.client.home.delivery.model.DeliAddressModel;
import com.opencheck.client.home.delivery.model.PromotionModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

public class PromotionCodeUserGroupActivity extends LixiActivity {
    private PromotionListUserGroupAdapter mAdapter;
    private ArrayList<PromotionModel> list = new ArrayList<>();
    private ActivityPromotionCodeUserGroupBinding mBinding;
    private PlaceSmallAdapter placeSmallAdapter;
    private DeliAddressModel mLocation;

    private PromotionModel currentPromotion = new PromotionModel();

    public static final String USER_PROMOTION_CODE = "USER_PROMOTION_CODE";
    private final double HEIGHT_BOTTOM_SHEET_RATIO = 0.7; // <= 1
    private final double MINIMUM_HEIGHT_BOTTOM_SHEET_RATIO = 0.5; // <= 1

    public static void startPromotionCodeUserGroupActivity(LixiActivity context) {
        Intent intent = new Intent(context, PromotionCodeUserGroupActivity.class);
        context.startActivityForResult(intent, ConstantValue.REQUEST_LIST_PROMOTION);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_promotion_code_user_group);
        placeSmallAdapter = new PlaceSmallAdapter(activity, null, new PlaceSmallAdapter.ClickListener() {
            @Override
            public void onClick(View view, StoreOfCategoryModel store) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(activity, StoreDetailActivity.class);
                bundle.putLong(StoreDetailActivity.ID, store.getId());
                Helper.showLog("pro group:" + currentPromotion.getCode());
                bundle.putString(USER_PROMOTION_CODE, currentPromotion.getCode());
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });
        mLocation = AppPreferenceHelper.getInstance().getDefaultLocation();
        initView();
    }

    private void initView() {
        mBinding.ivBack.setOnClickListener(this);

//        mBinding.searchBar.setOnSearchEventListener(new SearchBar.OnSearchEventListener() {
//            @Override
//            public void onTextChange(@NonNull String text) {
//                if (text.equals("")) {
//                    search(text);
//                }
//            }
//
//            @Override
//            public void onComplete(@NonNull String text) {
//                search(text);
//            }
//        });

        getListPromotionCode(null);
    }

    private String operateTextSearch(String text) {
        if (text.equals("")) {
            return null;
        }
        return text.trim().toUpperCase();
    }

    private void search(String text) {
        list.clear();
        getListPromotionCode(operateTextSearch(text));
        hideKeyboard();
    }

    private void setAdapter() {
        mBinding.rvPromotionCode.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mAdapter = new PromotionListUserGroupAdapter(activity, list);
        mBinding.rvPromotionCode.setAdapter(mAdapter);

        mAdapter.setItemClickListener(new PromotionListUserGroupAdapter.ItemClickListener() {

            @Override
            public void onClick(PromotionModel model, int position) {
                currentPromotion = model;

                if(model.getList_store().size() == 0 && model.getList_province().size() > 0) {
                    setResult(RESULT_OK);
                    finish();
                    return;
                }

                activity.hideKeyboard();
                final FragmentStoreBottomSheetDialogBinding bottomSheetDialogBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_store_bottom_sheet_dialog, null, false);
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
                bottomSheetDialogBinding.rv.setAdapter(placeSmallAdapter);
                bottomSheetDialogBinding.rv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                bottomSheetDialogBinding.spacer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });
                ViewGroup.LayoutParams params = bottomSheetDialogBinding.spacer.getLayoutParams();
                params.height = (int) (activity.heightScreen * (1 - HEIGHT_BOTTOM_SHEET_RATIO));
                bottomSheetDialogBinding.spacer.setLayoutParams(params);
                bottomSheetDialog.setContentView(bottomSheetDialogBinding.getRoot());
                final View bottomSheetInternal = bottomSheetDialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                if (bottomSheetInternal != null) {
                    bottomSheetInternal.setBackgroundColor(ContextCompat.getColor(activity, R.color.transparent));
                    bottomSheetInternal.setMinimumHeight((int) (activity.heightScreen));
                    BottomSheetBehavior.from(bottomSheetInternal).setSkipCollapsed(true);
                }
                bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        placeSmallAdapter.setArrStore(new ArrayList<StoreOfCategoryModel>());
//                        bottomSheetDialogBinding.rv.scrollToPosition(0);
                    }
                });
                bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        if (bottomSheetInternal != null && hasMoreItem(bottomSheetInternal)) {
                            BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);

                        }
                    }
                });
                placeSmallAdapter.setArrStore(model.getList_store());
                bottomSheetDialog.show();
            }
        });

        mAdapter.setOnAvailableData(new PromotionListUserGroupAdapter.OnAvailableData() {
            @Override
            public void onHaveData(boolean isHave) {
                if (!isHave) {
                    mBinding.lnNoResult.setVisibility(View.VISIBLE);
                    mBinding.lnResult.setVisibility(View.GONE);
                } else {
                    mBinding.lnNoResult.setVisibility(View.GONE);
                    mBinding.lnResult.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean hasMoreItem(View view) {
        int height = (int) (activity.heightScreen * HEIGHT_BOTTOM_SHEET_RATIO - 30); // 30 is offset'
        return view.getHeight() >= height;
    }

    private void getListPromotionCode(final String search) {
        DeliDataLoader.getPromotionListUserGroup(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    list = (ArrayList<PromotionModel>) object;
                    if (list == null || list.size() == 0) {
                        mBinding.lnNoResult.setVisibility(View.VISIBLE);
                        mBinding.lnResult.setVisibility(View.GONE);
                        mBinding.tv.setText(LanguageBinding.getString(R.string.deli_promotion_waiting_code, activity));
                        mBinding.iv.setImageResource(R.drawable.ic_wait_promotion);
                    } else {
                        mBinding.lnNoResult.setVisibility(View.GONE);
                        mBinding.lnResult.setVisibility(View.VISIBLE);
                        setAdapter();
                    }
                }
            }
        }, search, mLocation.getFullAddress(), mLocation.getLat(), mLocation.getLng());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                hideKeyboard();
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }
}
