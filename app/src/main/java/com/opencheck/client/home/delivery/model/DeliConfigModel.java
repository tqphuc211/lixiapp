package com.opencheck.client.home.delivery.model;

import android.content.Context;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.utils.AppPreferenceHelper;

import java.io.Serializable;
import java.util.ArrayList;

public class DeliConfigModel implements Serializable {

    public final static String DELI_WORKING_TIME_DEFAULT = "08:30 - 21:30";
    public final static String DELI_WORKING_TIME_KEY = "deli_working_time";
    public final static String DELI_ENABLE_KEY = "deli_enable";
    public final static String DELI_LIMIT_DISTANCE = "default_shipping_real_limit_distance";
    public final static String DEFAULT_PAYMENT_METHOD = "default_payment_method";
    public final static String PAYMENT_BY_ATM = "atm";
    public final static String PAYMENT_BY_CC = "cc";
    public final static String PAYMENT_BY_COD = "cod";
    public final static String PAYMENT_AT_STORE = "store";
    public final static String PAYMENT_BY_MOMO = "wallet_momo";
    public final static String PAYMENT_BY_ZALO = "wallet_zalo";

    private String key = "";
    private String description = "";
    private String value = "";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static void SaveConfig(LixiActivity activity, final ArrayList<DeliConfigModel> list) {
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
        appPreferenceHelper.setDeliConfig(list);
        data = list;
    }

    private static ArrayList<DeliConfigModel> data = null;

    public static ArrayList<DeliConfigModel> getData(Context context) {
        if (data == null) {
            data = new AppPreferenceHelper(context).getDeliConfig();
        }

        return data;
    }
}
