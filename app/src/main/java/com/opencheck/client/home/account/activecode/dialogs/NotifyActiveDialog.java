package com.opencheck.client.home.account.activecode.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.NotifyActiveDialogBinding;
import com.opencheck.client.models.lixishop.EvoucherDetailModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.utils.ConstantValue;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class NotifyActiveDialog extends LixiDialog {
    public NotifyActiveDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }


    private TextView tv_done;
    private TextView tv_name, tv_address, tv_message, tv_name_store, tv_title;
    private RelativeLayout rl_dimiss;

    //region Logic Variable
    private EvoucherDetailModel productModel;
    private StoreApplyModel storeApplyModel;
    private String msg;
    private int currentPosition;
    //endregion

    private NotifyActiveDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = NotifyActiveDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());
        findViews();
    }

    public void setData(String _msg, EvoucherDetailModel productModel, StoreApplyModel storeApplyModel, int currentPosition) {
        msg = _msg;
        this.productModel = productModel;
        this.storeApplyModel = storeApplyModel;
        this.currentPosition = currentPosition;
        setViewData();
    }

    private void setViewData() {
        tv_name.setText(productModel.getName());
        tv_name_store.setText(storeApplyModel.getName());
        tv_address.setText(storeApplyModel.getAddress());
        tv_message.setText(msg);
    }

    private void findViews() {
        tv_name = findViewById(R.id.tv_name);
        tv_address = findViewById(R.id.tv_address);
        tv_name_store = findViewById(R.id.tv_name_store);
        tv_message = findViewById(R.id.tv_message);
        tv_done = findViewById(R.id.tv_done);
        rl_dimiss = findViewById(R.id.rl_dismiss);

        tv_done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_done) {
            dismiss();
            Intent intent = activity.getIntent();
            intent.putExtra(ConstantValue.EVOUCHER_ID, ConstantValue.ID);
            intent.putExtra(ConstantValue.CURRENT_POSITION, currentPosition);
            activity.finish();
            activity.startActivity(intent);
        }
    }

    public interface IDidalogEvent {
        void onOk();
    }

    protected IDidalogEvent eventListener;

    public void setIDidalogEventListener(
            IDidalogEvent listener) {
        eventListener = listener;
    }
}
