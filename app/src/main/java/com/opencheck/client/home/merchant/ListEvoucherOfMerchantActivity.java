package com.opencheck.client.home.merchant;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ListEvoucherOfMerchantActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class ListEvoucherOfMerchantActivity extends LixiActivity {

    //regionregion Layout Variable
    private RelativeLayout rl_back;
    private RecyclerView recyclerView;
    private TextView tv_name;
    private LinearLayout lnNoResult;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    //endregion

    //region Logic Variable
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int page = 1;
    private int RECORD_PER_PAGE = DataLoader.record_per_page;
    private Boolean isLoading = false;
    private boolean isOutOfVoucherPage = false;
    private String name, id;
    private ArrayList<LixiShopEvoucherModel> listVoucher;
    //endregion

    //region Adapter, Controller
    private AdapterVoucherOfMerchant adapterVoucherDiscoveryFragment;
    //endregion

    private ListEvoucherOfMerchantActivityBinding mBinding;
    //region Processing UI
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.list_evoucher_of_merchant_activity);
        activity = this;
        initView();
    }

    private void initView(){
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        tv_name = (TextView) findViewById(R.id.tv_name);
        recyclerView = (RecyclerView) findViewById(R.id.rclv_evoucher);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        lnNoResult = (LinearLayout) findViewById(R.id.lnNoResult);
        initData();
    }
    //endregion

    //region Handle UI Logic
    private void initData(){
        name = getIntent().getStringExtra("NAMEMERCHANT");
        id = getIntent().getStringExtra("ID");
        tv_name.setText(name);
        rl_back.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listVoucher = null;
                adapterVoucherDiscoveryFragment.notifyDataSetChanged();
                page = 1;
                isLoading = false;
                isOutOfVoucherPage = false;
                getVoucherOfMerchant();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(recyclerView.computeVerticalScrollOffset() > 0){
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if(!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount - 2){
                        Helper.showLog("Load more");
                        getVoucherOfMerchant();
                        isLoading = true;
                    }
                }
            }
        });

        getVoucherOfMerchant();
    }

    private void setAdapter(){
        adapterVoucherDiscoveryFragment = new AdapterVoucherOfMerchant(activity, listVoucher, R.layout.row_lixi_main_voucher_layout, this.widthScreen);
        layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterVoucherDiscoveryFragment);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_back:
                onBackPressed();
                break;
        }
    }
    //endregion

    //region Background Processing

    private void getVoucherOfMerchant(){
        if(!isOutOfVoucherPage) {
            DataLoader.getMerchantEvoucherList(activity, new ApiCallBack() {
                @Override
                public void handleCallback(boolean isSuccess, Object object) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (isSuccess) {
                        if (page == 1) {
                            listVoucher = (ArrayList<LixiShopEvoucherModel>) object;
                            if ( listVoucher == null || listVoucher.size() == 0) {
                                lnNoResult.setVisibility(View.VISIBLE);
                                return;
                            }
                            lnNoResult.setVisibility(View.GONE);
                            setOutOfVoucherPage(listVoucher);
                            setAdapter();
                        } else {
                            ArrayList<LixiShopEvoucherModel> list = (ArrayList<LixiShopEvoucherModel>) object;
                            if (list == null || list.size() == 0 || list == null) {
                                lnNoResult.setVisibility(View.VISIBLE);
                                return;
                            }
                            setOutOfVoucherPage(list);
                            lnNoResult.setVisibility(View.GONE);
                            listVoucher.addAll(list);
                            adapterVoucherDiscoveryFragment.notifyDataSetChanged();
                        }
                        isLoading = false;
                        page++;
                    } else {
                        isLoading = true;
                    }
                }
            }, page, id);
        }
    }

    private void setOutOfVoucherPage(ArrayList<LixiShopEvoucherModel> list) {
        if (list.size() < RECORD_PER_PAGE) {
            isOutOfVoucherPage = true;
        }
    }

    //endregion
}
