package com.opencheck.client.home.delivery.model;

import com.opencheck.client.utils.ConstantValue;

import java.io.Serializable;

public class DeliBannerModel implements Serializable {
    private long id;
    private String name;
    private String image_link;
    private String object_type; //  store, article, topic
    private long object_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_link() {
        if (image_link == null || image_link.equals("")) {
            return ConstantValue.DEFAULT_IMAGE[0];
        }
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public long getObject_id() {
        return object_id;
    }

    public void setObject_id(long object_id) {
        this.object_id = object_id;
    }

    public enum Type {
        STORE, ARTICLE, TOPIC;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
}
