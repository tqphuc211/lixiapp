package com.opencheck.client.home.account.activecode2.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.custom.CircleImageView;
import com.opencheck.client.databinding.HeaderVoucherAtStoreLayoutBinding;
import com.opencheck.client.databinding.UnusedVoucherLayoutBinding;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.lixishop.LixiShopEvoucherBoughtModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.ItemClickListener;
import com.opencheck.client.utils.LanguageBinding;

import java.util.ArrayList;

/**
 * Created by Tony Tuan on 03/26/2018.
 */

public class VoucherAtStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private LixiActivity activity;
    private int HEADER = 0;
    private int BODY_UNUSED = 1;
    private int BODY_USED = 2;
    private ArrayList<LixiShopEvoucherBoughtModel> usedList = new ArrayList<>();
    private ArrayList<LixiShopEvoucherBoughtModel> unusedList = new ArrayList<>();
    private NearestStoreModel storeApplyModel;
    private LinearLayout ll_buy;
    private LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
    private int num_voucher = 0;

    public VoucherAtStoreAdapter(LixiActivity activity, ArrayList<LixiShopEvoucherBoughtModel> unusedList, ArrayList<LixiShopEvoucherBoughtModel> usedList, NearestStoreModel storeApplyModel, LinearLayout ll_buy, int num_voucher) {
        this.activity = activity;
        this.usedList = usedList;
        this.unusedList = unusedList;
        this.storeApplyModel = storeApplyModel;
        this.ll_buy = ll_buy;
        this.num_voucher = num_voucher;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if(viewType == HEADER) {
            HeaderVoucherAtStoreLayoutBinding voucherAtStoreLayoutBinding =
                    HeaderVoucherAtStoreLayoutBinding.inflate(
                            LayoutInflater.from(parent.getContext()), parent, false);
            return new HeaderViewHolder(voucherAtStoreLayoutBinding.getRoot());
        }
        else if(viewType == BODY_UNUSED){
            UnusedVoucherLayoutBinding unusedVoucherLayoutBinding =
                    UnusedVoucherLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),
                            parent, false);
            return new UnusedViewHolder(unusedVoucherLayoutBinding.getRoot());
        }
        else if(viewType == BODY_USED){
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.e_voucher_item_layout, parent, false);
            return new UsedViewHolder(itemView);
        }

        return null;
    }

    public void setUsedList(ArrayList<LixiShopEvoucherBoughtModel> usedList) {
        this.usedList = usedList;
    }

    public void setUnusedList(ArrayList<LixiShopEvoucherBoughtModel> unusedList) {
        this.unusedList = unusedList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == HEADER){
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            if(unusedList.size() > 0){
                headerViewHolder.tv_quantity_voucher.setText("Voucher khả dụng tại cửa hàng (" + unusedList.get(0).getEvoucher_selled_count() + ")");
                headerViewHolder.lnNoResult.setVisibility(View.GONE);
            }
            else {
                headerViewHolder.lnNoResult.setVisibility(View.VISIBLE);
                headerViewHolder.tv_buy_voucher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MerchantModel merchantModel = new MerchantModel();
                        merchantModel.setId(String.valueOf(storeApplyModel.getMerchant_info().getId()));
                        Intent detailIntent = new Intent(activity, MerchantDetailActivity.class);
                        Bundle data = new Bundle();
                        data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                        detailIntent.putExtras(data);
                        activity.startActivity(detailIntent);
                    }
                });
            }

            headerViewHolder.tv_name.setText(storeApplyModel.getName());
            headerViewHolder.tv_address.setText(storeApplyModel.getAddress());
            ImageLoader.getInstance().displayImage(storeApplyModel.getMerchant_info().getLogo(),
                    headerViewHolder.iv_merchant, LixiApplication.getInstance().optionsNomal);
            headerViewHolder.tv_title.setText("Chọn voucher bạn muốn sử dụng tại " + storeApplyModel.getName());
        }
        else if(holder.getItemViewType() == BODY_UNUSED){
            UnusedViewHolder viewHolder = (UnusedViewHolder) holder;
            if(unusedList.size() > 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                viewHolder.rv_unused.setNestedScrollingEnabled(false);
                viewHolder.rv_unused.setHasFixedSize(true);
                viewHolder.rv_unused.setLayoutManager(layoutManager);
                final UnusedVoucherAdapter adapter = new UnusedVoucherAdapter(activity, unusedList);
                viewHolder.rv_unused.setAdapter(adapter);
                adapter.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onItemCLick(int pos, String action, Object object, Long count) {
                        if(action.equalsIgnoreCase(ConstantValue.ON_ITEM_CLICK)){
                            LixiShopEvoucherBoughtModel model = (LixiShopEvoucherBoughtModel) object;
                            if(!model.getState().equals("waiting"))
                                toggle(pos, adapter);
                        }

                    }
                });

                viewHolder.tv_used_title.setText("Voucher không khả dụng (" + (num_voucher - unusedList.get(0).getEvoucher_selled_count()) + ")");
            }

            if(usedList != null && usedList.size() > 0){
                viewHolder.ll_title.setVisibility(View.VISIBLE);
            }else{
                viewHolder.ll_title.setVisibility(View.GONE);
            }

        }
        else if(holder.getItemViewType() == BODY_USED) {
            UsedViewHolder voucherHolder = (UsedViewHolder) holder;
            voucherHolder.rlRoot.setBackgroundColor(activity.getResources().getColor(R.color.white));
            if(unusedList.size() + position - 2 < usedList.size()) {
                LixiShopEvoucherBoughtModel evoucherBoughtModel = usedList.get(unusedList.size() + position - 2);

                if (evoucherBoughtModel.getProduct_cover_image() == null || evoucherBoughtModel.getProduct_cover_image().equals(""))
                    ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_image().get(0), voucherHolder.iv_hinh, LixiApplication.getInstance().optionsNomal);
                else
                    ImageLoader.getInstance().displayImage(evoucherBoughtModel.getProduct_cover_image(), voucherHolder.iv_hinh, LixiApplication.getInstance().optionsNomal);

                voucherHolder.tv_name.setText(evoucherBoughtModel.getProduct_name());
                //voucherHolder.tv_expired_title.setTypeface(voucherHolder.tv_expired_title.getTypeface(), Typeface.ITALIC);
                voucherHolder.tv_expire_date.setText(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_DATE,
                        String.valueOf(evoucherBoughtModel.getActivate_date() * 1000)));
                voucherHolder.tv_code.setText(evoucherBoughtModel.getPin());
                voucherHolder.tv_price.setTextColor(activity.getResources().getColor(R.color.color_gray_price));
                voucherHolder.tv_price.setText(Helper.getVNCurrency(evoucherBoughtModel.getPrice()) + "đ");
                voucherHolder.tv_quantity_day.setVisibility(View.VISIBLE);
                voucherHolder.tv_quantity_day.setTextColor(activity.getResources().getColor(R.color.color_gray));
                if (evoucherBoughtModel.getState().equals("used")) {
                    voucherHolder.tv_expired_title.setText(LanguageBinding.getString(R.string.bought_evoucher_actived_day, activity) + " ");
                    voucherHolder.tv_quantity_day.setText(LanguageBinding.getString(R.string.bought_evoucher_used, activity));
                } else if (evoucherBoughtModel.getState().equals("expired")) {
                    voucherHolder.tv_expired_title.setText(LanguageBinding.getString(R.string.expire_day, activity) + " ");
                    voucherHolder.tv_quantity_day.setText(LanguageBinding.getString(R.string.bought_evoucher_expired, activity));
                } else if (evoucherBoughtModel.getState().equals("can_active")) {
                    final long countLimitDay = Helper.getDaysBetweenTimestamp(Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_REMAIN, String.valueOf(Long.parseLong(Helper.getTimestamp()) * 1000)), Helper.getFormatDateEngISO(ConstantValue.FORMAT_TIME_REMAIN, String.valueOf(Long.parseLong(evoucherBoughtModel.getExpired_time()) * 1000)), ConstantValue.FORMAT_TIME_REMAIN);
                    if (countLimitDay < 0) {
                        voucherHolder.tv_expired_title.setText(LanguageBinding.getString(R.string.expire_day, activity) + " ");
                        voucherHolder.tv_quantity_day.setText(activity.getString(R.string.bought_evoucher_expired));
                    }
                }
            }
        }
    }

    public ArrayList<LixiShopEvoucherBoughtModel> getChosenVoucherList(){
        ArrayList<LixiShopEvoucherBoughtModel> res = new ArrayList<>();
        for(int i = 0; i < unusedList.size(); i++){
            if(unusedList.get(i).isSelected())
                res.add(unusedList.get(i));
        }
        return res;
    }

    private int countSelected = 0;
    long total = 0;
    private void toggle(int pos, UnusedVoucherAdapter adapter){
        unusedList.get(pos).setSelected(!unusedList.get(pos).isSelected());
        if(unusedList.get(pos).isSelected()) {
            countSelected++;
            total += unusedList.get(pos).getPrice();
        }
        else{
            countSelected--;
            total -= unusedList.get(pos).getPrice();
        }

        if(countSelected > 0) {
            ll_buy.setVisibility(View.VISIBLE);
            TextView tv_money = ll_buy.getChildAt(0).findViewById(R.id.tv_scan);
            TextView tv_amount = ll_buy.getChildAt(1).findViewById(R.id.tv_buy);
            tv_money.setText(LanguageBinding.getString(R.string.use, activity) + " " + Helper.getVNCurrency(total) + "đ");
            tv_amount.setText("(" + countSelected + " voucher)");
        }
        else
            ll_buy.setVisibility(View.GONE);
        adapter.notifyItemChanged(pos);
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return HEADER;
        else if(position == 1)
            return BODY_UNUSED;
        return BODY_USED;
    }

    @Override
    public int getItemCount() {
        return usedList.size() + 2;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView iv_merchant;
        private TextView tv_name, tv_address, tv_quantity_voucher, tv_buy_voucher, tv_title;
        private LinearLayout lnNoResult, llRoot;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            iv_merchant = itemView.findViewById(R.id.iv_merchant);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_quantity_voucher = itemView.findViewById(R.id.tv_quantity_voucher);
            tv_buy_voucher = itemView.findViewById(R.id.tv_buy_voucher);
            tv_title = itemView.findViewById(R.id.tv_title);
            lnNoResult = itemView.findViewById(R.id.lnNoResult);
            llRoot = itemView.findViewById(R.id.llRoot);
        }

    }

    public class UnusedViewHolder extends RecyclerView.ViewHolder{
        private RecyclerView rv_unused;
        private LinearLayout ll_title;
        private TextView tv_used_title;

        public UnusedViewHolder(View itemView) {
            super(itemView);
            rv_unused = (RecyclerView) itemView.findViewById(R.id.rv_unused);
            ll_title = (LinearLayout) itemView.findViewById(R.id.ll_title);
            tv_used_title = (TextView) itemView.findViewById(R.id.tv_used_title);
        }
    }

    public class UsedViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name, tv_code, tv_expire_date, tv_price, tv_expired_title, tv_quantity_day;
        private ImageView iv_hinh;
        private RelativeLayout rlRoot;

        public UsedViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_code = itemView.findViewById(R.id.tv_code);
            tv_expire_date = itemView.findViewById(R.id.tv_expire_date);
            tv_expired_title = itemView.findViewById(R.id.tv_expired_title);
            tv_quantity_day = itemView.findViewById(R.id.tv_quantity_day);
            tv_price = itemView.findViewById(R.id.tv_price);
            iv_hinh = itemView.findViewById(R.id.iv_hinh);
            rlRoot = itemView.findViewById(R.id.rlRoot);
        }

    }
}
