package com.opencheck.client.home.search;

import android.app.Activity;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.opencheck.client.R;
import com.opencheck.client.utils.ItemClickListener;

/**
 * Created by vutha_000 on 3/6/2018.
 */

public class OnClickTagController extends ClickableSpan {

    public static String ONCLICK_SPAN = "ONCLICK_SPAN";
    private int pos;
    private ItemClickListener itemClickListener;
    private Activity activity;

    public OnClickTagController(Activity activity, int position, ItemClickListener _itemClickListener) {
        this.pos = position;
        this.activity = activity;
        this.itemClickListener = _itemClickListener;
    }

    @Override
    public void onClick(View widget) {
        itemClickListener.onItemCLick(pos, ONCLICK_SPAN, "", 0L);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(activity.getResources().getColor(R.color.color_hash_tag));
        ds.setUnderlineText(false);
    }
}
