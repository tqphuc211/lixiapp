package com.opencheck.client.home.delivery.interfaces;

public interface OnChangeOffsetListener {
    void onChangeOffsetListener(int height);
}
