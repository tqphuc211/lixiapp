package com.opencheck.client.home.delivery.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ShipFeeDialogBinding;
import com.opencheck.client.home.delivery.model.DeliOrderModel;
import com.opencheck.client.home.delivery.model.StoreOfCategoryModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;

public class ShipFeeDialog extends LixiDialog {

    private TextView tv_shipping_fee, tv_value, tv_message;
    private LinearLayout ll_container;
    private RelativeLayout rl_dismiss;

    public ShipFeeDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ShipFeeDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomToTop;
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.ship_fee_dialog, null, false);
        setContentView(mBinding.getRoot());
        tv_shipping_fee = findViewById(R.id.tv_shipping_fee);
        tv_value = findViewById(R.id.tv_value);
        tv_message = findViewById(R.id.tv_message);
        ll_container = findViewById(R.id.ll_container);
        rl_dismiss = findViewById(R.id.rl_dismiss);

        rl_dismiss.setOnClickListener(this);
        ll_container.setOnClickListener(this);

        ViewGroup.LayoutParams params = ll_container.getLayoutParams();
        params.height = activity.heightScreen / 2;
        ll_container.setLayoutParams(params);
    }

    public void setData(StoreOfCategoryModel mStore, DeliOrderModel orderModel, int index) {
        String fee;
        if (orderModel.getList_quotation().get(index).getMoney() == 0) {
            fee = LanguageBinding.getString(R.string.store_detail_free, activity);
        } else {
            fee = Helper.getVNCurrency(orderModel.getList_quotation().get(index).getMoney()) + "đ";
        }

        tv_shipping_fee.setText(String.format(LanguageBinding.getString(R.string.store_detail_delivery_fee, activity), fee));

        String dot = LanguageBinding.getString(R.string.dot, activity);

        switch (mStore.getFeeType()) {
            case SAME_FEE:
                String priceSame = Helper.getVNCurrency(mStore.getFixed_ship_price()) + LanguageBinding.getString(R.string.p, activity);
                String distanceSame = mStore.getFixed_ship_price_range() + "Km";
                String shipPrice = Helper.getVNCurrency(mStore.getShip_price()) + LanguageBinding.getString(R.string.p, activity);

                String title = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_title, activity), "<b>" + priceSame + "</b>", "<b>" + distanceSame + "</b>");
                String content = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_content, activity), "<b>" + shipPrice + "</b>");

                tv_value.setText(Html.fromHtml(dot + title));
                tv_message.setText(Html.fromHtml(dot + content));
                break;
            case FREE_SHIP:
                if (mStore.getMin_money_to_free_ship_price() != null && mStore.getMin_money_to_free_ship_price() != 0) {
                    String promotion = dot + LanguageBinding.getString(R.string.store_detail_promotin_default_long, activity) + " <b>" + Helper.getVNCurrency(mStore.getMin_money_to_free_ship_price()) + "đ</b>";
                    mBinding.tvPromotionDefault.setText(Html.fromHtml(promotion));
                }

                if (mStore.getMax_free_ship_money() != null && mStore.getMax_free_ship_money() > 0) {
                    long distance = 0;
                    if (mStore.getShip_price() != 0) {
                        distance = mStore.getMax_free_ship_money() / mStore.getShip_price();
                        tv_value.setText(Html.fromHtml(dot + LanguageBinding.getString(R.string.store_detail_max_free_ship, activity) + " <b>" + Helper.getVNCurrency(mStore.getMax_free_ship_money()) + "đ " + "(" + distance + "km)" + "</b>"));
                    }else {
                        tv_value.setText(Html.fromHtml(dot + LanguageBinding.getString(R.string.store_detail_max_free_ship, activity) + " <b>" + Helper.getVNCurrency(mStore.getMax_free_ship_money()) + "đ</b>"));
                    }
                } else {
                    tv_value.setText(dot + LanguageBinding.getString(R.string.store_detail_free_delivery, activity));
                }

                String nextKm = Helper.getVNCurrency(mStore.getShip_price()) + LanguageBinding.getString(R.string.p, activity);
                String nextKmHtml = String.format(LanguageBinding.getString(R.string.store_detail_same_fee_content, activity), "<b>" + nextKm + "</b>");
                tv_message.setText(Html.fromHtml(dot + nextKmHtml));
                break;
            case NORMAL:
                mBinding.txtPlan.setVisibility(View.GONE);
                String minShip = Helper.getVNCurrency(orderModel.getMin_ship_money()) + LanguageBinding.getString(R.string.p, activity);
                String standardShip = Helper.getVNCurrency(mStore.getShip_price()) + LanguageBinding.getString(R.string.p, activity) + "/Km";

                String minShipHtml = String.format(LanguageBinding.getString(R.string.store_detail_min_ship_fee, activity), "<b>" + minShip + "</b>");
                String standardShipHtml = String.format(LanguageBinding.getString(R.string.store_detail_standard_ship_fee, activity), "<b>" + standardShip + "</b>");

                tv_value.setText(Html.fromHtml(dot + minShipHtml));
                tv_message.setText(Html.fromHtml(dot + standardShipHtml));
                break;
        }
    }


    @Override
    public void onClick(View view) {
        if (view == rl_dismiss) {
            dismiss();
        }
    }
}
