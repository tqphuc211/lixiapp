package com.opencheck.client.home.history.detail.controller;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opencheck.client.R;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderDetailInfoController implements View.OnClickListener {

    private Activity activity;
    private View viewChild;
    private LinearLayout lnRoot;

    private LinearLayout ll_content;
    private TextView tv_name;
    private TextView tv_value;
    private View viewLine;

    private int type = 0;
    private String name = "";
    private String value = "";
    private long textColor = 0xff000000;
    private String strTextColor = "#ff000000";
    private String state = "";
    private boolean isBolt = false;


    public OrderDetailInfoController(Activity _activity, int type, String _name, String _value, long textColor, boolean isBolt, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.type = type;
        this.name = _name;
        this.value = _value;
        this.textColor = textColor;
        this.isBolt = isBolt;

        this.viewChild = View.inflate(activity, R.layout.item_order_info_layout, null);

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    public OrderDetailInfoController(Activity _activity, int type, String state, String _name, String _value, String strTextColor, boolean isBolt, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.type = type;
        this.name = _name;
        this.value = _value;
        this.strTextColor = strTextColor;
        this.isBolt = isBolt;
        this.state = state;

        this.viewChild = View.inflate(activity, R.layout.item_order_info_layout, null);

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    public OrderDetailInfoController(Activity _activity, int type, String state, String _name, String _value, long TextColor, boolean isBolt, LinearLayout _lnRoot) {
        this.activity = _activity;
        this.lnRoot = _lnRoot;
        this.type = type;
        this.name = _name;
        this.value = _value;
        this.textColor = TextColor;
        this.isBolt = isBolt;
        this.state = state;

        this.viewChild = View.inflate(activity, R.layout.item_order_info_layout, null);

        this.initView();
        this.initData();

        this.lnRoot.post(new Runnable() {
            @Override
            public void run() {
                lnRoot.addView(viewChild);
            }
        });
    }

    private void initView() {
        ll_content = (LinearLayout) viewChild.findViewById(R.id.ll_content);
        tv_name = (TextView) viewChild.findViewById(R.id.tv_name);
        tv_value = (TextView) viewChild.findViewById(R.id.tv_value);
        viewLine = (View) viewChild.findViewById(R.id.viewLine);
    }

    private void initData() {
        if (type == 0) {
            tv_name.setText(name);
            tv_value.setText(value);
//            tv_value.setTextColor((int) textColor);
            tv_value.setTextColor(Color.parseColor(strTextColor));
            if (name.equals(LanguageBinding.getString(R.string.lixi_cash_back, activity))) {
                tv_value.setTextColor(getColor());
            }
            if (isBolt)
                tv_value.setTypeface(null, Typeface.BOLD);
            ll_content.setVisibility(View.VISIBLE);
            viewLine.setVisibility(View.GONE);
        } else {
            viewLine.setVisibility(View.VISIBLE);
            ll_content.setVisibility(View.GONE);
        }
    }

    public int getColor() {
        if (state.equals("wait"))
            return 0xFFEB8509;
        else if (state.equals("done"))
            return 0xFF64B408;
        else if (state.equals("cancel"))
            return 0xffac1ea1;
        else if (state.equals("payment_expired"))
            return 0xffac1ea1;
        return 0xFF000000;
    }

    @Override
    public void onClick(View v) {
    }
}