package com.opencheck.client.home.delivery.model;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.apiwrapper.MetaDataModel;

import java.util.ArrayList;

public class DeliApiWrapperForListModel<T> extends LixiModel {
    private MetaDataModel meta;
    private ArrayList<T> data;
    private ErrorModel error;

    public MetaDataModel get_meta() {
        return meta;
    }

    public void set_meta(MetaDataModel meta) {
        this.meta = meta;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel error) {
        this.error = error;
    }
}
