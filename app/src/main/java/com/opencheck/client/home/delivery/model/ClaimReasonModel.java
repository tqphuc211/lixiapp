package com.opencheck.client.home.delivery.model;

import java.io.Serializable;

public class ClaimReasonModel implements Serializable {

    private Long id;
    private String type;
    private String state;
    private Long report_date;
    private String reason;
    private String message;

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getState() {
        return state;
    }

    public Long getReport_date() {
        return report_date;
    }

    public String getReason() {
        return reason;
    }

    public String getMessage() {
        return message;
    }
}
