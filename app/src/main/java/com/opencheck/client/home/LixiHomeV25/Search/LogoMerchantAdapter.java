package com.opencheck.client.home.LixiHomeV25.Search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.databinding.LogoHotTrendMerchantLayoutBinding;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.models.merchant.MerchantHotBrandModel;
import com.opencheck.client.utils.tracking.TrackingConstant;

import java.util.ArrayList;

public class LogoMerchantAdapter extends RecyclerView.Adapter<LogoMerchantAdapter.ViewHolder> {

    LixiActivity lixiActivity;

    public void setListMerchant(ArrayList<MerchantHotBrandModel> listMerchant) {
        this.listMerchant = listMerchant;
        notifyDataSetChanged();
    }

    ArrayList<MerchantHotBrandModel> listMerchant;

    public LogoMerchantAdapter(LixiActivity lixiActivity, ArrayList<MerchantHotBrandModel> strings) {
        this.lixiActivity = lixiActivity;
        this.listMerchant = strings;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LogoHotTrendMerchantLayoutBinding logoHotTrendMerchantLayoutBinding =
                LogoHotTrendMerchantLayoutBinding.inflate(LayoutInflater.from(
                        parent.getContext()), parent, false);

        final ViewHolder holder = new ViewHolder(logoHotTrendMerchantLayoutBinding.getRoot());
        holder.imgLogoMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MerchantDetailActivity.startMerchantDetailActivity(
                        lixiActivity,
                        listMerchant.get((int) holder.imgLogoMerchant.getTag()).getId(),
                        TrackingConstant.ContentSource.SEARCH
                );
                lixiActivity.hideKeyboard();
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage(listMerchant.get(position).getLogo(), holder.imgLogoMerchant);
        holder.imgLogoMerchant.setTag(position);
    }

    @Override
    public int getItemCount() {
        if (listMerchant == null)
            return 0;
        return listMerchant.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLogoMerchant;

        public ViewHolder(View itemView) {
            super(itemView);
            imgLogoMerchant = itemView.findViewById(R.id.imgLogoMerchant);
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.width = lixiActivity.widthScreen / 5;
            itemView.setLayoutParams(layoutParams);
        }
    }

}
