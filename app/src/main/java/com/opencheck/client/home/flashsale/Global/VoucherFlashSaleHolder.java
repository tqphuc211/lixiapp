package com.opencheck.client.home.flashsale.Global;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.FlashSaleVoucherItemLayoutBinding;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.LixiHomeV25.CategoryContent.ActivityCategoryDetail;
import com.opencheck.client.home.LixiHomeV25.CollectionContent.ActivityCollectionListProduct;
import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.lixishop.LixiShopEvoucherModel;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.TrackingConstant;

public class VoucherFlashSaleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static VoucherFlashSaleHolder newHolder(LixiActivity activity, ViewGroup parent) {

        FlashSaleVoucherItemLayoutBinding voucherItemLayoutBinding =
                FlashSaleVoucherItemLayoutBinding.inflate(
                        LayoutInflater.from(parent.getContext()), parent, false);
        VoucherFlashSaleHolder flashSaleHolder = new VoucherFlashSaleHolder(
                voucherItemLayoutBinding.getRoot());
        flashSaleHolder.activity = activity;
        flashSaleHolder.itemView.setOnClickListener(flashSaleHolder);
        return flashSaleHolder;
    }

    public static void bindViewHolder(VoucherFlashSaleHolder voucherHolder, LixiShopEvoucherModel voucher) {
        voucherHolder.voucherModel = voucher;
        if (voucher.getProduct_cover_image() != null && voucher.getProduct_cover_image().length() > 0)
            ImageLoader.getInstance().displayImage(voucher.getProduct_cover_image(), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        else {
            if (voucher.getProduct_image() != null && voucher.getProduct_image().size() > 0)
                ImageLoader.getInstance().displayImage(voucher.getProduct_image().get(0), voucherHolder.imgLogoVoucher, LixiApplication.getInstance().optionsNomal);
        }
        voucherHolder.tvVoucherName.setText(voucher.getName());

        FlashSaleInfo voucherfs = voucher.getFlash_sale_info();
        voucherHolder.rl_flashsale_info.setVisibility(View.VISIBLE);
        voucherHolder.tvBuyFS.setVisibility(View.VISIBLE);
        voucherHolder.tvVoucherPriceFS.setText(Helper.getVNCurrency(voucherfs.getPayment_discount_price()) + "đ");
        if (voucherfs.getPayment_discount_price() != voucherfs.getPayment_price()) {
            voucherHolder.tvOldPriceFS.setVisibility(View.VISIBLE);
            voucherHolder.tvOldPriceFS.setText(Helper.getVNCurrency(voucherfs.getPayment_price()) + "đ");
            voucherHolder.tvOldPriceFS.setPaintFlags(voucherHolder.tvOldPriceFS.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else voucherHolder.tvOldPriceFS.setVisibility(View.GONE);

        if (voucherfs.getQuantity_order_done() >= voucherfs.getQuantity_limit()) {
            voucherHolder.txtSoldOut.setVisibility(View.VISIBLE);
            voucherHolder.progSold.setVisibility(View.GONE);
            voucherHolder.txtSold.setVisibility(View.GONE);
            voucherHolder.tvBuyFS.setVisibility(View.GONE);
        } else {
            voucherHolder.txtSoldOut.setVisibility(View.GONE);
            voucherHolder.progSold.setVisibility(View.VISIBLE);
            voucherHolder.txtSold.setVisibility(View.VISIBLE);
            voucherHolder.tvBuyFS.setVisibility(View.VISIBLE);
            voucherHolder.txtSold.setText("ĐÃ BÁN " + voucherfs.getQuantity_order_done());
            voucherHolder.progSold.setProgress(voucherfs.getQuantity_order_done() * 100 / voucherfs.getQuantity_limit());
        }

        if (voucherfs.getPayment_price() != voucherfs.getPayment_discount_price()) {
            voucherHolder.rlSaleOff.setVisibility(View.VISIBLE);
            voucherHolder.ivBgSaleoff.setVisibility(View.VISIBLE);
            voucherHolder.ivBgLixi.setVisibility(View.GONE);
            int percent = 100 - (int) (voucherfs.getPayment_discount_price() * 100 / voucherfs.getPayment_price());
            voucherHolder.txtSaleOffValue.setText(percent + "%");
            voucherHolder.txtSaleOffNote.setText("GIẢM");
        } else voucherHolder.rlSaleOff.setVisibility(View.GONE);

        if (voucherfs.getCashback_price() > 0) {
            voucherHolder.rlSaleOff.setVisibility(View.VISIBLE);
            voucherHolder.ivBgSaleoff.setVisibility(View.GONE);
            voucherHolder.ivBgLixi.setVisibility(View.VISIBLE);

            voucherHolder.txtSaleOffValue.setText("+" + Helper.getVNCurrency(voucherfs.getCashback_price()));
            voucherHolder.txtSaleOffNote.setText(LanguageBinding.getString(R.string.give_lixi, voucherHolder.activity));
        }
    }

    private LixiActivity activity;
    private LixiShopEvoucherModel voucherModel;

    private View v_line;
    private RelativeLayout rl_info;
    private ImageView imgLogoVoucher;
    private RelativeLayout rlSaleOff;
    private ImageView ivBgSaleoff;
    private ImageView ivBgLixi;
    private TextView txtSaleOffValue;
    private TextView txtSaleOffNote;
    private TextView tvVoucherName;
    private RelativeLayout rl_flashsale_info;
    private TextView tvOldPriceFS;
    private TextView tvVoucherPriceFS;
    private ProgressBar progSold;
    private TextView txtSold;
    private TextView txtSoldOut;
    private TextView tvBuyFS;

    private String source = "";

    public VoucherFlashSaleHolder(final View itemView) {
        super(itemView);

        v_line = (View) itemView.findViewById(R.id.v_line);
        rl_info = (RelativeLayout) itemView.findViewById(R.id.rl_info);
        imgLogoVoucher = (ImageView) itemView.findViewById(R.id.imgLogoVoucher);
        rlSaleOff = (RelativeLayout) itemView.findViewById(R.id.rlSaleOff);
        ivBgSaleoff = (ImageView) itemView.findViewById(R.id.ivBgSaleoff);
        ivBgLixi = (ImageView) itemView.findViewById(R.id.ivBgLixi);
        txtSaleOffValue = (TextView) itemView.findViewById(R.id.txtSaleOffValue);
        txtSaleOffNote = (TextView) itemView.findViewById(R.id.txtSaleOffNote);
        tvVoucherName = (TextView) itemView.findViewById(R.id.tvVoucherName);
        rl_flashsale_info = (RelativeLayout) itemView.findViewById(R.id.rl_flashsale_info);
        tvOldPriceFS = (TextView) itemView.findViewById(R.id.tvOldPriceFS);
        tvVoucherPriceFS = (TextView) itemView.findViewById(R.id.tvVoucherPriceFS);
        progSold = (ProgressBar) itemView.findViewById(R.id.progSold);
        txtSold = (TextView) itemView.findViewById(R.id.txtSold);
        txtSoldOut = (TextView) itemView.findViewById(R.id.txtSoldOut);
        tvBuyFS = (TextView) itemView.findViewById(R.id.tvBuyFS);

        if (activity instanceof HomeActivity) {
            source = TrackingConstant.ContentSource.HOME;
        } else if (activity instanceof ActivityCategoryDetail) {
            source = TrackingConstant.ContentSource.VOUCHER_CATEGORY;
        } else if (activity instanceof ActivityCollectionListProduct) {
            source = TrackingConstant.ContentSource.VOUCHER_COLLECTION;
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, voucherModel.getId(), source);
            }
        });
        tvBuyFS.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (voucherModel != null) {
            if (v == tvBuyFS)
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, voucherModel.getId(), source,
                        FlashSaleDetailController.getParamBuyNow(true));
            else
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, voucherModel.getId(), source);
        }
    }
}
