package com.opencheck.client.analytics;

import android.util.Log;

import com.google.gson.Gson;
import com.opencheck.client.analytics.models.TrackingConfigModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import io.socket.client.Ack;
import io.socket.client.Socket;

public class TrackQueue extends LinkedBlockingQueue<TrackQueue.Task> {

    private static final String TAG = "Analytics_track_queue";

    private int queueId = 0;
    private String singleEvent;
    private String multiEvent;
    private AtomicBoolean isPushing = new AtomicBoolean(false);
    private TrackingConfigModel trackingConfigModel;

    TrackQueue(TrackingConfigModel trackingConfigModel, String singleEvent, String multiEvent) {
        this.multiEvent = multiEvent;
        this.singleEvent = singleEvent;
        this.trackingConfigModel = trackingConfigModel;
    }

    void setTrackingConfigModel(TrackingConfigModel trackingConfigModel, String strJson) {
        if (this.trackingConfigModel == null)
            this.trackingConfigModel = trackingConfigModel;
        else
            this.trackingConfigModel.update(strJson);
    }

    public TrackingConfigModel getTrackingConfigModel() {
        return trackingConfigModel;
    }

    void addToQueue(final Object... objects) {
        if (objects != null && objects.length > 0) {
            for (int i = 0; i < objects.length; i++) {
                final int finalI = i;
                add(new Task() {{
                    data = objects[finalI];
                }});
            }
        }
    }

    public synchronized void enqueue(final SocketTrackBuilder socketTrackBuilder) {
        if (!isEmpty() && isPushing.compareAndSet(false, true)) {
            if (size() > 0 && size() >= trackingConfigModel.getMin_hit_queue_length()) {
                final List<Task> tasks = new ArrayList<>();
                final List<Object> objects = new ArrayList<>();
                while (size() > 0) {
                    Task task = poll();
                    if (task != null) {
                        //Retry count down
                        if (--task.retry > 0) {
                            tasks.add(task);
                            objects.add(task.data);
                        }
                    }
                    if (objects.size() == trackingConfigModel.getMax_hit_queue_length()) {
                        break;
                    }
                }
                if (objects.size() == 1) {
                    Socket socket = socketTrackBuilder.getSocket();
                    if (socket == null) {
                        isPushing.set(false);
                        return;
                    }
                    Log.i(TAG, singleEvent + " " + new Gson().toJson(objects.get(0)));
                    socket.emit(singleEvent, objects.get(0), new Ack() {
                        @Override
                        public void call(Object... args) {
                            if (args[0] != null && args[0] instanceof Integer) {
                                switch ((int) args[0]) {
                                    case 0: {
                                        Log.i(TAG, "Send success: " + new Gson().toJson(objects.get(0)));
                                        break;
                                    }
                                    case 1: {
                                        add(tasks.get(0));
                                        Log.i(TAG, "Retry send: " + new Gson().toJson(objects.get(0)));
                                        break;
                                    }
                                    case 2: {
                                        Log.i(TAG, "Cancel send: " + new Gson().toJson(objects.get(0)));
                                        break;
                                    }
                                }
                            }
                            isPushing.set(false);
                            if (socketTrackBuilder.isSocketAvailable() && size() > 0)
                                enqueue(socketTrackBuilder);
                        }
                    });
                } else if (objects.size() > 1) {
                    Socket socket = socketTrackBuilder.getSocket();
                    if (socket == null) {
                        isPushing.set(false);
                        return;
                    }
                    JSONArray jsonArray = new JSONArray(objects);
                    Log.i(TAG, multiEvent + " " + jsonArray.toString());
                    socket.emit(multiEvent, jsonArray, new Ack() {
                        @Override
                        public void call(Object... args) {
                            if (args[0] != null && args[0] instanceof Integer) {
                                switch ((int) args[0]) {
                                    case 0: {
                                        Log.i(TAG, "Sends success: " + new Gson().toJson(objects));
                                        break;
                                    }
                                    case 1: {
                                        addAll(tasks);
                                        Log.i(TAG, "Retry sends: " + new Gson().toJson(objects));
                                        break;
                                    }
                                    case 2: {
                                        Log.i(TAG, "Cancel sends: " + new Gson().toJson(objects));
                                        break;
                                    }
                                }
                            } else if (args[0] != null && args[0] instanceof JSONArray) {
                                JSONArray array = (JSONArray) args[0];
                                List<Integer> argsList = new ArrayList<>();
                                for (int i = 0; i < array.length(); i++) {
                                    try {
                                        argsList.add(array.getInt(i));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (argsList.size() == objects.size()) {
                                    for (int j = 0; j < objects.size(); j++) {
                                        switch (j) {
                                            case 0: {
                                                Log.i(TAG, "Send success: " + new Gson().toJson(objects.get(j)));
                                                break;
                                            }
                                            case 1: {
                                                add(tasks.get(j));
                                                Log.i(TAG, "Retry send: " + new Gson().toJson(objects.get(j)));
                                                break;
                                            }
                                            case 2: {
                                                Log.i(TAG, "Cancel send: " + new Gson().toJson(objects.get(j)));
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            isPushing.set(false);
                            if (socketTrackBuilder.isSocketAvailable() && size() > 0)
                                enqueue(socketTrackBuilder);
                        }
                    });
                } else {
                    isPushing.set(false);
                    if (socketTrackBuilder.isSocketAvailable() && size() > 0)
                        enqueue(socketTrackBuilder);
                }
            } else {
                isPushing.set(false);
            }
        }
    }

    public void setIsPushing(boolean isPushing) {
        this.isPushing.set(isPushing);
    }

    class Task {

        int id;

        Object data;

        int retry;

        Task() {
            this.id = ++queueId;
            this.retry = trackingConfigModel.getMax_emit_retry_count();
        }
    }
}
