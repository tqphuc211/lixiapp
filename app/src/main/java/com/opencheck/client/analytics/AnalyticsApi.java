package com.opencheck.client.analytics;

import com.google.gson.JsonObject;
import com.opencheck.client.analytics.models.PostLogModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AnalyticsApi {

    @POST("log/connection")
    Call<JsonObject> postLog(@Body PostLogModel postLogModel);
}
