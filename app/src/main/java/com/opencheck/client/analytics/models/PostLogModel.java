package com.opencheck.client.analytics.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PostLogModel implements Parcelable {

    private String tracker_id;
    private String event;
    private String param;
    private int hit_queue_length;

    public String getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(String tracker_id) {
        this.tracker_id = tracker_id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public int getHit_queue_length() {
        return hit_queue_length;
    }

    public void setHit_queue_length(int hit_queue_length) {
        this.hit_queue_length = hit_queue_length;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tracker_id);
        dest.writeString(this.event);
        dest.writeString(this.param);
        dest.writeInt(this.hit_queue_length);
    }

    public PostLogModel() {
    }

    private PostLogModel(Parcel in) {
        this.tracker_id = in.readString();
        this.event = in.readString();
        this.param = in.readString();
        this.hit_queue_length = in.readInt();
    }

    public static final Creator<PostLogModel> CREATOR = new Creator<PostLogModel>() {
        @Override
        public PostLogModel createFromParcel(Parcel source) {
            return new PostLogModel(source);
        }

        @Override
        public PostLogModel[] newArray(int size) {
            return new PostLogModel[size];
        }
    };
}
