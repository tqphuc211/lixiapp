package com.opencheck.client.analytics.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencheck.client.utils.AppPreferenceHelper;

@SuppressWarnings("all")
public class TrackingConfigModel implements Parcelable {

    private String tracker_id;
    private int max_emit_retry_count;
    private int max_hit_queue_length;
    private int min_hit_queue_length;

    public void update(TrackingConfigModel model) {
        //TODO chổ này coi lại update
        max_emit_retry_count = model.max_emit_retry_count;
        min_hit_queue_length = model.min_hit_queue_length;
        max_hit_queue_length = model.max_hit_queue_length;
        tracker_id = model.tracker_id;
    }

    public void update(String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(json);
        if (jo.has("max_emit_retry_count"))
            max_emit_retry_count = jo.get("max_emit_retry_count").getAsInt();
        if (jo.has("min_hit_queue_length"))
            min_hit_queue_length = jo.get("min_hit_queue_length").getAsInt();
        if (jo.has("max_hit_queue_length"))
            max_hit_queue_length = jo.get("max_hit_queue_length").getAsInt();
        if (jo.has("tracker_id"))
            tracker_id = jo.get("tracker_id").getAsString();
    }

    public String getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(String tracker_id) {
        this.tracker_id = tracker_id;
    }

    public int getMax_emit_retry_count() {
        return max_emit_retry_count;
    }

    public void setMax_emit_retry_count(int max_emit_retry_count) {
        this.max_emit_retry_count = max_emit_retry_count;
    }

    public int getMax_hit_queue_length() {
        return max_hit_queue_length;
    }

    public void setMax_hit_queue_length(int max_hit_queue_length) {
        this.max_hit_queue_length = max_hit_queue_length;
    }

    public int getMin_hit_queue_length() {
        return min_hit_queue_length;
    }

    public void setMin_hit_queue_length(int min_hit_queue_length) {
        this.min_hit_queue_length = min_hit_queue_length;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tracker_id);
        dest.writeInt(this.max_emit_retry_count);
        dest.writeInt(this.max_hit_queue_length);
        dest.writeInt(this.min_hit_queue_length);
    }

    public TrackingConfigModel() {
    }

    protected TrackingConfigModel(Parcel in) {
        this.tracker_id = in.readString();
        this.max_emit_retry_count = in.readInt();
        this.max_hit_queue_length = in.readInt();
        this.min_hit_queue_length = in.readInt();
    }

    public static final Creator<TrackingConfigModel> CREATOR = new Creator<TrackingConfigModel>() {
        @Override
        public TrackingConfigModel createFromParcel(Parcel source) {
            return new TrackingConfigModel(source);
        }

        @Override
        public TrackingConfigModel[] newArray(int size) {
            return new TrackingConfigModel[size];
        }
    };
}
