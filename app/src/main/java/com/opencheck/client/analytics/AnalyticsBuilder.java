package com.opencheck.client.analytics;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.opencheck.client.analytics.models.PostLogModel;

import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnalyticsBuilder {

    private static final String TAG = "AnalyticsBuilder";

    private AnalyticsApi analyticsApi;
    private static AnalyticsBuilder mInstance;
    private SocketTrackBuilder socketTrackBuilder;

    public static AnalyticsBuilder get() {
        if (mInstance == null)
            throw new NullPointerException("Build AnalyticsBuilder in application");
        return mInstance;
    }


    private AnalyticsBuilder(Builder builder) {
        Uri baseUrl = builder.getBaseUrl();
        Retrofit.Builder factory = new Retrofit.Builder().baseUrl(baseUrl.toString())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient().create()));
        analyticsApi = factory.build().create(AnalyticsApi.class);

        Uri.Builder builderUrl = baseUrl.buildUpon();
        String[] paths = builder.paths;
        if (paths != null && paths.length > 0) {
            for (String path : paths) {
                builderUrl.appendPath(path);
            }
        }
        socketTrackBuilder = new SocketTrackBuilder(builder.context, builderUrl) {{
            setSocketEvent(new SocketTrackBuilder.SocketEvent() {

                private void post(final String event, final @Nullable String param) {
                    final String tracking_id = SocketTrackBuilder.trackingConfigModel.getTracker_id();

                    PostLogModel postLogModel = new PostLogModel();
                    postLogModel.setTracker_id(tracking_id);
                    postLogModel.setParam(param);
                    postLogModel.setEvent(event);
                    Log.i(TAG, "PostLog: " + (new Gson().toJson(postLogModel)));
                    analyticsApi.postLog(postLogModel).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            JsonObject object = response.body();
                            if (object != null && object.getAsJsonObject("meta") != null &&
                                    object.getAsJsonObject("meta").get("success").getAsBoolean()) {
                                Log.i(TAG, "Post log success");
                            } else {
                                Log.i(TAG, "Post log failed");
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            Log.i(TAG, "Post log failed");
                        }
                    });
                }

                @Override
                public void callBack(String event, Object... args) {
                    switch (event) {
                        case Socket.EVENT_CONNECT: {
                            Log.i(TAG, "Socket connected");
                            break;
                        }
                        case Socket.EVENT_CONNECT_ERROR: {
                            post(event, (args != null && args.length > 0 ? String.valueOf(args[0]) : null));
                            break;
                        }
                        case Socket.EVENT_CONNECT_TIMEOUT: {
                            post(event, (args != null && args.length > 0 ? String.valueOf(args[0]) : null));
                            break;
                        }
                        case Socket.EVENT_ERROR: {
                            post(event, (args != null && args.length > 0 ? String.valueOf(args[0]) : null));
                            break;
                        }
                        case Socket.EVENT_RECONNECT_ERROR: {
                            post(event, (args != null && args.length > 0 ? String.valueOf(args[0]) : null));
                            break;
                        }
                        case Socket.EVENT_RECONNECT_FAILED: {
                            post(event, (args != null && args.length > 0 ? String.valueOf(args[0]) : null));
                            break;
                        }
                    }
                }
            });
        }};
    }

    public SocketTrackBuilder getSocketTrackBuilder() {
        return socketTrackBuilder;
    }

    public AnalyticsApi getApi() {
        return analyticsApi;
    }

    public static class Builder {

        private Context context;
        private String baseUrl;
        private String[] paths;
        private String schema = "https";

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setSchema(String schema) {
            this.schema = schema;
            return this;
        }

        public Builder setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public Builder setPaths(String[] paths) {
            this.paths = paths;
            return this;
        }

        Uri getBaseUrl() {
            Uri.Builder builder = new Uri.Builder()
                    .scheme(schema).authority(baseUrl);
            return builder.build();
        }

        public void build() {
            mInstance = new AnalyticsBuilder(this);
        }
    }
}
