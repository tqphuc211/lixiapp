package com.opencheck.client.analytics;

public class TrackEvent {

    public static class SCREEN {
        //Deli
        public static final String DELIVERY_HOME = "Delivery Home";
        public static final String DELIVERY_SEARCH = "Delivery Search";
        public static final String DELIVERY_STORE_DETAIL = "Delivery Store Detail";
        public static final String DELIVERY_COLLECTION = "Delivery Collection";
        public static final String DELIVERY_CATEGORY = "Delivery Category";
        public static final String DELIVERY_CHECKOUT = "Delivery Checkout";
        public static final String DELIVERY_ORDER_TRACKING = "Delivery Order Tracking";

        //History
        public static final String HISTORY_HOME = "History Home";
        public static final String HISTORY_DETAIL = "History Detail";

        //Reward
        public static final String REWARD_HOME = "Reward Home";
        public static final String REWARD_CATEGORY = "Reward Category";
        public static final String REWARD_SEARCH = "Reward Search";

        //Topup + Card Serivce
        public static final String SERVICE_TOPUP = "Service Topup";
        public static final String SERVICE_CARD = "Service Card";
        public static final String SERVICE_CHECKOUT = "Service Checkout";

        // voucher
        public static final String VOUCHER_HOME = "Voucher Home ";
        public static final String VOUCHER_DETAIL = "Voucher Detail";
        public static final String VOUCHER_SEARCH = "Voucher Search";
        public static final String VOUCHER_MERCHANT_DETAIL = "Voucher Merchant Detail";
        public static final String VOUCHER_CART_STEP_1 = "Voucher Cart Step 1";
        public static final String VOUCHER_CART_STEP_2 = "Voucher Cart Step 2";

        // login
        public static final String LOGIN = "Login";
        public static final String LOGIN_UPDATE_INFOR = "Login Update Infor";
        public static final String LOGIN_PHONE_GA = "Login Phone";
        public static final String LOGIN_PHONE_PASSWORD = "Login Phone Password";

        // profile
        public static final String PROFILE_HOME = "Profile Home";
        public static final String PROFILE_VOUCHER_LIST = "Profile Voucher List";
        public static final String PROFILE_VOUCHER_DETAIL = "Profile Voucher Detail";
        public static final String PROFILE_SETTINGS = "Profile Settings";
        public static final String PROFILE_INVITE_CODE = "Profile Invite Code";

        // other
        public static final String NOTIFICATION_HOME = "Notification Home";
        public static final String LOCATION_PICKER = "Location Picker";
    }
}
