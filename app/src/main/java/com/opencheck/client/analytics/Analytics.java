package com.opencheck.client.analytics;

import android.support.annotation.IdRes;

import com.opencheck.client.R;
import com.opencheck.client.home.delivery.refactor.LocationSelectionFragment;

import org.json.JSONObject;

public class Analytics {

    private static final String LOGIN_EVENT = "login";
    private static final String LOGOUT_EVENT = "logout";
    private static final String LOCATION_EVENT = "location";
    private static final String SCREEN_EVENT = "screen_view";
    private static final String ORDER_EVOUCHER = "order_evoucher";
    private static final String ORDER_DELIVERY = "order_delivery";

    public static void trackLogin(int lixi_id) {
        try {
            JSONObject object = new JSONObject();
            object.put("name", LOGIN_EVENT);
            object.put("lixi_id", lixi_id);
            AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackLogout() {
        AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(LOGOUT_EVENT);
    }

    public static void trackScreenView(String screenName, Long id) {
        if (screenName == null) {
            return;
        }
        try {
            JSONObject object = new JSONObject();
            object.put("id", id);
            object.put("name", SCREEN_EVENT);
            object.put("screen", screenName + "Screen");
            AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackOrderEVoucher(String orderId) {
        try {
            JSONObject object = new JSONObject();
            object.put("order_id", orderId);
            object.put("name", ORDER_EVOUCHER);
            AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackOrderDelivery(String orderUuId) {
        try {
            JSONObject object = new JSONObject();
            object.put("order_uuid", orderUuId);
            object.put("name", ORDER_DELIVERY);
            AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackLocation(Double lat, Double lng) {
        try {
            JSONObject object = new JSONObject();
            object.put("lat", lat);
            object.put("lng", lng);
            object.put("name", LOCATION_EVENT);
            AnalyticsBuilder.get().getSocketTrackBuilder().enqueue(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void homeActivityScreenTrack(@IdRes int menuId) {
        switch (menuId) {
            case R.id.navigation_delivery:
                if (LocationSelectionFragment.deliHomeShowing) {
                    trackScreenView(TrackEvent.SCREEN.DELIVERY_HOME, null);
                }
                break;
            case R.id.navigation_home:
                trackScreenView(TrackEvent.SCREEN.VOUCHER_HOME, null);
                break;
            case R.id.navigation_history:
                trackScreenView(TrackEvent.SCREEN.HISTORY_HOME, null);
                break;
            case R.id.navigation_shop:
                trackScreenView(TrackEvent.SCREEN.REWARD_HOME, null);
                break;
            case R.id.navigation_user_info:
                trackScreenView(TrackEvent.SCREEN.PROFILE_HOME, null);
                break;
        }
    }
}
