package com.opencheck.client.analytics;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencheck.client.analytics.models.TrackingConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

public class SocketTrackBuilder {

    private static final String TAG = "Analytics_socket";
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private static final String EVENT_CONFIG = "config";
    private Socket socket;
    private SocketEvent socketEvent;
    private final IO.Options opts = new IO.Options() {{
        forceNew = true;
        reconnection = true;
        reconnectionDelay = 3000;
        reconnectionAttempts = 10;
        reconnectionDelayMax = 5000;
        transports = new String[]{WebSocket.NAME};
    }};
    private final Uri.Builder uriBuilder;
    static TrackingConfigModel trackingConfigModel = new TrackingConfigModel();
    private static final TrackQueue trackQueue = new TrackQueue(trackingConfigModel, "hit", "hits");

    SocketTrackBuilder(Context context, Uri.Builder uriBuilder) {
        this.context = context;
        this.uriBuilder = uriBuilder;
    }

    @Nullable
    public Socket getSocket() {
        return socket;
    }

    public void enqueue(Object object) {
        if (object == null) return;
        trackQueue.addToQueue(object);
        if (isSocketAvailable())
            trackQueue.enqueue(this);
    }

    public void connect(@Nullable Uri uri) {
        uriBuilder.clearQuery();

        if (!TextUtils.isEmpty(trackingConfigModel.getTracker_id()))
            uriBuilder.appendQueryParameter("tracker_id", trackingConfigModel.getTracker_id());
        else {
            String rs = AppPreferenceHelper.getInstance().getLixiTrackerId();
            if (rs.length() > 0) {
                trackingConfigModel = (new Gson().fromJson(rs, TrackingConfigModel.class));
                uriBuilder.appendQueryParameter("tracker_id", trackingConfigModel.getTracker_id());
            }
        }

        if (uri != null) {
            String utm_medium = uri.getQueryParameter("utm_medium");
            if (!TextUtils.isEmpty(utm_medium))
                uriBuilder.appendQueryParameter("utm_medium", utm_medium);
            String utm_source = uri.getQueryParameter("utm_source");
            if (!TextUtils.isEmpty(utm_source))
                uriBuilder.appendQueryParameter("utm_source", utm_source);
            String utm_content = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(utm_content))
                uriBuilder.appendQueryParameter("utm_content", utm_content);
            String utm_campaign = uri.getQueryParameter("utm_campaign");
            if (!TextUtils.isEmpty(utm_campaign))
                uriBuilder.appendQueryParameter("utm_campaign", utm_campaign);
        }
        this.socket = IO.socket(URI.create(uriBuilder.build().toString()), opts);
        socket.connect();
        socket.on(EVENT_CONFIG, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    if (args[0] != null && args[0] instanceof JSONObject) {
                        trackingConfigModel = new Gson().fromJson(args[0].toString(),
                                new TypeToken<TrackingConfigModel>() {
                                }.getType());
                        trackQueue.setTrackingConfigModel(trackingConfigModel, args[0].toString());
                        AppPreferenceHelper.getInstance().setLixiTrackerId((new Gson()).toJson(trackQueue.getTrackingConfigModel()));
                        Log.i(TAG, EVENT_CONFIG + ": " + new Gson().toJson(trackingConfigModel));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "Connected: " + uriBuilder.build().toString());
                trackQueue.setIsPushing(false);
                trackQueue.enqueue(SocketTrackBuilder.this);
                //Send metadata
                sendMetadata();
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_CONNECT, args);
                }
            }
        }).on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "Connecting: " + uriBuilder.build().toString());
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_CONNECTING, args);
                }
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "Disconnected: " + uriBuilder.build().toString());
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_DISCONNECT, args);
                }
            }
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_ERROR, args);
                }
            }
        }).on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_MESSAGE, args);
                }
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_CONNECT_ERROR, args);
                }
            }
        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_CONNECT_TIMEOUT, args);
                }
            }

        }).on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_RECONNECT, args);
                }
            }
        }).on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_RECONNECT_ERROR, args);
                }
            }
        }).on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_RECONNECT_FAILED, args);
                }
            }
        }).on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_RECONNECT_ATTEMPT, args);
                }
            }
        }).on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_RECONNECTING, args);
                }
            }
        }).on(Socket.EVENT_PING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_PING, args);
                }
            }
        }).on(Socket.EVENT_PONG, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (socketEvent != null) {
                    socketEvent.callBack(Socket.EVENT_PONG, args);
                }
            }
        });
    }

    public void disconnect() {
        if (socket != null && socket.connected()) {
            socket.disconnect();
        }
    }

    void setSocketEvent(SocketEvent socketEvent) {
        this.socketEvent = socketEvent;
    }

    boolean isSocketAvailable() {
        return socket != null && socket.connected();
    }

    @SuppressLint("HardwareIds")
    private void sendMetadata() {
        JSONObject device = new JSONObject();
        final JSONObject meta = new JSONObject();
        try {
            device.put("platform", "android");
            device.put("device_id", Settings.Secure.getString(context
                    .getContentResolver(), Settings.Secure.ANDROID_ID));
            device.put("model", (Build.MANUFACTURER.substring(0, 1)
                    .toUpperCase() + Build.MANUFACTURER.substring(1) + " " + Build.MODEL.substring(0, 1)
                    .toUpperCase() + Build.MODEL.substring(1)).trim());
            device.put("screen_size", Resources.getSystem()
                    .getDisplayMetrics().widthPixels + "," + Resources.getSystem().getDisplayMetrics().heightPixels);
            device.put("os_version", Build.VERSION.RELEASE);
            try {
                device.put("app_version", context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0).versionName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            try {
                List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface nif : all) {
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                    byte[] macBytes = nif.getHardwareAddress();
                    if (macBytes != null) {
                        StringBuilder res1 = new StringBuilder();
                        for (byte b : macBytes) {
                            res1.append(String.format("%02X:", b));
                        }
                        if (res1.length() > 0) {
                            res1.deleteCharAt(res1.length() - 1);
                        }
                        device.put("mac_address", res1.toString());
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            meta.putOpt("device", device);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Sending metadata: " + meta.toString());
        if (isSocketAvailable()) {
            socket.emit("metadata", meta, new Ack() {
                @Override
                public void call(Object... args) {
                    if (args[0] instanceof Integer) {
                        switch ((int) args[0]) {
                            case 0: {
                                Log.i(TAG, "send metadata success");
                                break;
                            }
//                            case 1: {
//                                Log.i(TAG, "retry send metadata");
//                                break;
//                            }
//                            case 2: {
//                                Log.i(TAG, "cancel send metadata");
//                                break;
//                            }
                        }
                    }
                }
            });
        }
    }


    public interface SocketEvent {

        void callBack(String event, Object... args);
    }
}
