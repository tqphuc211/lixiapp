package com.opencheck.client.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.home.account.new_layout.activity.IntroCodeActivity;
import com.opencheck.client.home.delivery.activities.StoreDetailActivity;
import com.opencheck.client.home.delivery.activities.StoreListOfArticleActivity;
import com.opencheck.client.home.delivery.activities.TopicActivity;
import com.opencheck.client.home.merchant.MerchantDetailActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class DeepLinkHelper {

    private static JSONObject installParams;
    private static LixiActivity activity;

    public static void initDeepLink(LixiActivity _activity, final DeepLinkCallBack _callBack) {
        activity = _activity;
        try {
            // Branch init
            Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(JSONObject referringParams, BranchError error) {
                    if (error == null) {
                        Helper.showLog("Deep link data: " + referringParams.toString());
                        getDeepLinkData(activity);
                    } else {
                        Helper.showLog("Deep link error: " + error.getMessage());
                    }
                    _callBack.handleCallBack();
                }
            }, activity.getIntent().getData(), activity);
        } catch (NullPointerException e) {
            _callBack.handleCallBack();
            e.printStackTrace();
        }
    }

    private static void getDeepLinkData(LixiActivity _activity) {
        activity = _activity;
        installParams = Branch.getInstance().getLatestReferringParams();
        Helper.showLog("Get deep link data:" + installParams.toString());

        if (!installParams.toString().equals("{}")) {
            try {
                saveUtm(installParams);
                String objectId = "";
                if (installParams.has("object_id")) {
                    objectId = installParams.getString("object_id");
                    fireDeepLink(installParams.getString("type"), installParams.getString("id"), objectId);
                } else
                    fireDeepLink(installParams.getString("type"), installParams.getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Helper.showLog("Empty Deep Link!");
        }
    }

    private static void saveUtm(JSONObject params) {
        SocketUtmModel socketUtmModel = new SocketUtmModel();
        try {
            String url = params.getString("~referring_link");
            String subUrl = url.substring(url.indexOf("&") + 1, url.length());

            String[] arr = subUrl.split("&");
            HashMap<String, String> map = new HashMap<>();

            for (String temp : arr) {
                String[] subArr = temp.split("=");
                if (subArr.length > 1) {
                    map.put(subArr[0], subArr[1]);
                }
            }

            socketUtmModel.setSource(map.get("utm_source"));
            socketUtmModel.setCampaign(map.get("utm_campaign"));
            socketUtmModel.setMedium(map.get("utm_medium"));
            socketUtmModel.setTerm(map.get("utm_term"));
            socketUtmModel.setContent(map.get("utm_content"));

            ConstantValue.SOCKET_UTM_MODEL = socketUtmModel;
            if (ConstantValue.SOCKET_UTM_MODEL != null) {
                Log.d("TTTT", "ok");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void fireDeepLink(String key, String id, String... obj) {
        switch (key) {
            case "merchant":
                MerchantModel merchantModel = new MerchantModel();
                merchantModel.setId(id);
                Intent merchant = new Intent(activity, MerchantDetailActivity.class);
                Bundle data = new Bundle();
                data.putString(ConstantValue.MERCHANT_DETAIL_MODEL, new Gson().toJson(merchantModel));
                data.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
                merchant.putExtras(data);
                activity.startActivity(merchant);
                break;
            case "evoucher":
                //TODO ADD COMBO CHECK
                long product_id = Long.parseLong(id);
//                Intent intent = new Intent(activity, CashEvoucherDetailActivity.class);
//                intent.putExtra(CashEvoucherDetailActivity.PRODUCT_ID, product_id);
//                activity.startActivity(intent);
                ActivityProductCashEvoucherDetail.startActivityProductCashEvoucherDetail(activity, product_id, TrackingConstant.ContentSource.DEEP_LINK);
                break;
            case "deli_store":
                //Intent storeIntent = new Intent(activity, StoreDetailActivity.class);
                long store_id = Long.parseLong(id);
//                Bundle bundle = new Bundle();
//                bundle.putLong("ID", store_id);
//                bundle.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
//                storeIntent.putExtras(bundle);
//                activity.startActivity(storeIntent);
                StoreDetailActivity.startStoreDetailActivity(activity, store_id, TrackingConstant.ContentSource.DEEP_LINK);
                AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(true);
                break;
            case "deli_article":
//                Intent articleIntent = new Intent(activity, StoreListOfArticleActivity.class);
//
//                Bundle bundle1 = new Bundle();
//                bundle1.putString(GlobalTracking.SOURCE, TrackingConstant.ContentSource.DEEP_LINK);
//                bundle1.putLong("ID", article_id);
                long article_id = Long.parseLong(id);
                long objectId = 0L;
                if (obj.length > 0) {
                    objectId = Long.parseLong(obj[0]);
                }
                //articleIntent.putExtras(bundle1);

                StoreListOfArticleActivity.startArticleActivity(activity, objectId, article_id, "", "", TrackingConstant.ContentSource.DEEP_LINK);
                //activity.startActivity(articleIntent);
                AppPreferenceHelper.getInstance().setStartActivityFromDeepLink(true);
                break;
            case "reference_code":
                IntroCodeActivity.start(activity);
                break;
            case "deli_topic":
                Bundle dataTopic = new Bundle();
                dataTopic.putLong("TOPIC_ID", Long.valueOf(id));
                Intent intentTopic = new Intent(activity, TopicActivity.class);
                intentTopic.putExtras(dataTopic);
                activity.startActivity(intentTopic);
                break;
        }
        Branch.getInstance().logout();
    }

    public static void setForegroundActivity(LixiActivity _activity) {
        activity = _activity;
//        try {
//            getDeepLinkData(activity);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
