package com.opencheck.client.firstlaunch;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiApplication;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ActivityFirstLaunchBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.home.setting.LoginPinCodeActivity;
import com.opencheck.client.models.ConfigModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;

import java.util.Locale;

import static com.opencheck.client.login.LoginHelper.isLoggedIn;
import static com.opencheck.client.login.LoginHelper.startHomeActivity;

public class FirstLaunchActivity extends LixiActivity {

    //region Layout Variable
    private RelativeLayout rlContent;
    private RelativeLayout rlSplashScreen;
    private FrameLayout fr_main;
    //endregion

    private ActivityFirstLaunchBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            finish();
        }
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_first_launch);
        setUpApp();
        setLanguageConfig((new AppPreferenceHelper(activity).getLanguageSource()));
    }

    //region Setup app

    private void setUpApp() {
        //Mark that App is running
        if (LixiApplication.getInstance() != null)
            LixiApplication.isAppRunning = true;
        DataLoader.initAPICall(activity, new AppPreferenceHelper(activity).getUserToken(), null);
        DeliDataLoader.initAPICall(activity, new AppPreferenceHelper(activity).getUserToken());
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkUpToDate();
    }

    public static boolean isPush = false;

    @Override
    public void receiveNotificationData() {
        if (isLoggedIn(activity)) {
            isPush = true;
            return;
        }
        super.receiveNotificationData();
    }

    private void checkUpToDate() {
        DataLoader.getUserConfig(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
                    appPreferenceHelper.setTimeResetConfig(System.currentTimeMillis());
                    try {
                        int minimumVersion;
                        minimumVersion = ((ConfigModel) object).getAndroidLixiUpdate();
                        int currentVersion;
                        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        currentVersion = pInfo.versionCode;
                        if (minimumVersion <= currentVersion)
                            initView();
                        else {
                            forceUpdate();
                        }
                    } catch (Exception e) {
                        if (e instanceof JSONException) {
                            Helper.showErrorDialog(activity, "Cannot get minimum version");
                        }
                        if (e instanceof PackageManager.NameNotFoundException) {
                            Helper.showErrorDialog(activity, "Cannot get current version");
                        }
                    }
                } else {
                    Helper.showErrorDialog(activity, (String) object);
                }
            }
        });
    }

    private void forceUpdate() {
        ForceUpdateDialog dialog = new ForceUpdateDialog(activity, false, true, false);
        dialog.setCancelable(false);
        dialog.setIDialogEventListener(new ForceUpdateDialog.IDialogEvent() {
            @Override
            public void onOk() {
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    String vs = pInfo.packageName;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + vs + "&hl=en"));
                    startActivity(browserIntent);
                } catch (PackageManager.NameNotFoundException ex) {
                    Helper.showErrorDialog(activity, "Cannot get app id!");
                }
            }
        });
        dialog.show();

    }

    //endregion

    //region Init UI

    private void initView() {
        this.rlContent = findViewById(R.id.rl_content);
        this.rlSplashScreen = findViewById(R.id.rl_splash_screen);
        this.fr_main = findViewById(R.id.fr_main);
        initData();
//        startLaunchScreen();
    }

    private void initData() {
        checkUserLogined();
    }

    private void startLaunchScreen() {
        rlContent.setVisibility(View.VISIBLE);
        rlSplashScreen.setVisibility(View.GONE);
//        startFirstLaunchFragment();
        startHomeActivity(activity);
        finish();
    }

    //endregion

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    //region handle User state
    private boolean hasEnterPin = false;

    private void checkUserLogined() {
        if (isLoggedIn(activity)) {
            if (!new AppPreferenceHelper(activity).isPinRequired()) {
                Helper.showLog(new AppPreferenceHelper(activity).getUserToken());
                startHomeActivity(activity);
                finish();
            } else if (!hasEnterPin) {
                Bundle data = new Bundle();
                data.putString(ConstantValue.ACTION_FOR_PIN, ConstantValue.ACTION_OPEN_APP);
                Intent intent = new Intent(activity, LoginPinCodeActivity.class);
                intent.putExtras(data);
                startActivityForResult(intent, ConstantValue.PIN_REQUEST_CODE);
            }
        } else {
            handleUserNotLogin();
        }
    }

    private void handleUserNotLogin() {
        startLaunchScreen();
    }
    //endregion


    //region background thread

    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    startHomeActivity(activity);
                    finish();
                }
                break;
            case ConstantValue.PIN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    hasEnterPin = true;
                    startHomeActivity(activity);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void setLanguageConfig(String lang) {
        if (lang.equals("")) {
            return;
        }
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
