package com.opencheck.client.firstlaunch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ForceUpdateDialogBinding;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/2/2018.
 */

public class ForceUpdateDialog extends LixiDialog {

    //region Layout Variable
    protected RelativeLayout rl_dismiss;

    private TextView tv_message;
    private TextView bt_ok;
    //endregion

    public ForceUpdateDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    private ForceUpdateDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ForceUpdateDialogBinding.inflate(
                getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());
        findViews();
        initDialogEvent();
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        tv_message = (TextView) findViewById(R.id.tv_message);
        bt_ok = (TextView) findViewById(R.id.bt_ok);

        tv_message.setText(LanguageBinding.getString(R.string.update_new_version, activity));
    }


    private void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
        rl_dismiss.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_dismiss:
                break;
            case R.id.bt_ok:
                if (eventListener != null)
                    eventListener.onOk();
            default:
                break;
        }
    }


    public interface IDialogEvent {
        void onOk();
    }

    protected IDialogEvent eventListener;

    public void setIDialogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
