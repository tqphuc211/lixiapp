package com.opencheck.client.parent;

import android.support.v7.app.AppCompatActivity;

public abstract class LixiParentActivity extends AppCompatActivity {
    public abstract String getScreenName();
}
