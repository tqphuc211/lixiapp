package com.opencheck.client.parent;

import android.support.v4.app.Fragment;

public abstract class LixiParentFragment extends Fragment {
    public abstract String getScreenName();
}
