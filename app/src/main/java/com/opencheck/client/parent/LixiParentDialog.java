package com.opencheck.client.parent;

import android.app.Dialog;
import android.support.annotation.NonNull;

import com.opencheck.client.LixiActivity;

public abstract class LixiParentDialog extends Dialog {
    public LixiParentDialog(@NonNull LixiActivity _activity) {
        super(_activity);
    }
    protected abstract String getScreenName();
}
