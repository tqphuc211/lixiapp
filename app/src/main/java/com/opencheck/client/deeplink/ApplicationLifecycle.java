package com.opencheck.client.deeplink;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.analytics.Analytics;
import com.opencheck.client.analytics.AnalyticsBuilder;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.product.ActivityProductCashEvoucherDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class ApplicationLifecycle implements Application.ActivityLifecycleCallbacks {

    public static final String DEEPLINK = "my_deeplink";
    public static String UTM_QUERY = null;
    public static boolean raw_reference = false;
    private static final String TAG = "ApplicationLifecycle";
    //10 seconds
    private static final long MIN_INTERVAL = 10000;
    //5 min
    private static final long MAX_INTERVAL = 300000;

    private Context context;
    private Uri uriDeeplink = null;
    private int activityReferences = 0;
    //    private LixiAnalytics.SocketQueryBuilder socketQueryBuilder;
    private boolean isActivityChangingConfigurations = false;
    //Checking location with interval
    private final Runnable location = new Runnable() {
        @Override
        public void run() {
            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                final FusedLocationProviderClient providerClient = LocationServices.getFusedLocationProviderClient(context);
                providerClient.getLocationAvailability().addOnCompleteListener(new OnCompleteListener<LocationAvailability>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onComplete(@NonNull Task<LocationAvailability> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isLocationAvailable()) {
                                providerClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Location> task) {
                                        if (task.isSuccessful()) {
                                            Location location = task.getResult();
                                            if (location != null)
                                                Analytics.trackLocation(location.getLatitude(), location.getLongitude());
                                            new Handler().postDelayed(ApplicationLifecycle.this.location, MAX_INTERVAL);
                                        } else {
                                            Log.i(TAG, "Location not available");
                                            new Handler().postDelayed(location, MIN_INTERVAL);
                                        }
                                    }
                                });
                            } else {
                                Log.i(TAG, "LocationAvailability not granted");
                                new Handler().postDelayed(location, MIN_INTERVAL);
                            }
                        } else {
                            //Permission not granted
                            Log.i(TAG, "LocationAvailability not granted");
                            new Handler().postDelayed(location, MIN_INTERVAL);
                        }
                    }
                });
            } else {
                //Permission not granted
                Log.i(TAG, "Location permission not granted");
                new Handler().postDelayed(this, MIN_INTERVAL);
            }
        }
    };

    /**
     * Constructor
     *
     * @param context the context needed
     */
    public ApplicationLifecycle(final Context context) {
        this.context = context;
        new Handler().post(location);
    }

    @Override
    public void onActivityCreated(final Activity activity, Bundle savedInstanceState) {
        try {
            Log.d(DEEPLINK, "onActivityCreated: " + activity.getLocalClassName());
        } catch (Exception ex) {

        }
        registerFragmentLifecycle(activity);
    }

    @Override
    public void onActivityStarted(final Activity activity) {
        try {
            Log.d(DEEPLINK, "onActivityStarted: " + activity.getLocalClassName());
            Log.d(DEEPLINK, "activity reference: " + activityReferences);
        } catch (Exception ex) {

        }
        if ((++activityReferences == 1 && !isActivityChangingConfigurations) || raw_reference) {
            raw_reference = false;
            // App enters foreground
            Log.i(TAG, "App enters foreground");
            final Uri uri = activity.getIntent().getData();
            if (uri != null) {
                //Deep link
                Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
                    @Override
                    public void onInitFinished(JSONObject referringParams, BranchError error) {
                        uriDeeplink = createUriFromBranch(uri, referringParams);
                        if (uriDeeplink != null) {
                            UTM_QUERY = uriDeeplink.getQuery();
                            Log.i(TAG, "Deep-link detected: " + uriDeeplink.getQuery());
                        }
                        if (activity.isTaskRoot()) {
                            Intent intent = new Intent(activity, FirstLaunchActivity.class);
                            activity.startActivity(intent);
                        }
                        AnalyticsBuilder.get().getSocketTrackBuilder().connect(uriDeeplink);
                        activity.finish();
                        Branch.getInstance().logout();
                    }
                }, uri, activity);
            } else {
                // Launcher
                Log.d(DEEPLINK, "uri null");
                AnalyticsBuilder.get().getSocketTrackBuilder().connect(null);
            }
            try {
                JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();
                Log.d(DEEPLINK, "sessionParams: " + sessionParams.toString());
            } catch (Exception ex) {
                Log.d(DEEPLINK, "sessionParams e: " + ex.getMessage());
            }


            try {
                JSONObject installParams = Branch.getInstance().getFirstReferringParams();
                Log.d(DEEPLINK, "installParams: " + installParams.toString());
            } catch (Exception ex) {
                Log.d(DEEPLINK, "installParams e: " + ex.getMessage());
            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        //Lixi-Track
        if (activity instanceof ActivityProductCashEvoucherDetail) {
            //Handle in this activity
            return;
        }
        if (activity instanceof HomeActivity) {
            //Set deep-link-data
            activity.getIntent().setData(uriDeeplink);
            //Remove deep-link data
            uriDeeplink = null;
            //Track home
            Analytics.homeActivityScreenTrack(((HomeActivity)
                    activity).navigation.getSelectedItemId());
        } else if (activity instanceof LixiActivity) {
            Analytics.trackScreenView(((LixiActivity) activity).getScreenName(),
                    ((LixiActivity) activity).id);
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            // App enters background
            Log.i(TAG, "App enters background");
            AnalyticsBuilder.get().getSocketTrackBuilder().disconnect();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    private void registerFragmentLifecycle(Activity activity) {
        if (activity instanceof AppCompatActivity) {
            ((AppCompatActivity) activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(
                    new FragmentManager.FragmentLifecycleCallbacks() {
                        @Override
                        public void onFragmentPreAttached(FragmentManager fm, Fragment f, Context context) {
                            super.onFragmentPreAttached(fm, f, context);
                        }

                        @Override
                        public void onFragmentAttached(FragmentManager fm, Fragment f, Context context) {
                            super.onFragmentAttached(fm, f, context);
                        }

                        @Override
                        public void onFragmentPreCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
                            super.onFragmentPreCreated(fm, f, savedInstanceState);
                        }

                        @Override
                        public void onFragmentCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
                            super.onFragmentCreated(fm, f, savedInstanceState);
                        }

                        @Override
                        public void onFragmentActivityCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
                            super.onFragmentActivityCreated(fm, f, savedInstanceState);
                        }

                        @Override
                        public void onFragmentViewCreated(FragmentManager fm, Fragment f, View v, Bundle savedInstanceState) {
                            super.onFragmentViewCreated(fm, f, v, savedInstanceState);
                        }

                        @Override
                        public void onFragmentStarted(FragmentManager fm, Fragment f) {
                            super.onFragmentStarted(fm, f);
                        }

                        @Override
                        public void onFragmentResumed(FragmentManager fm, Fragment f) {
                            super.onFragmentResumed(fm, f);
                        }

                        @Override
                        public void onFragmentPaused(FragmentManager fm, Fragment f) {
                            super.onFragmentPaused(fm, f);
                        }

                        @Override
                        public void onFragmentStopped(FragmentManager fm, Fragment f) {
                            super.onFragmentStopped(fm, f);
                        }

                        @Override
                        public void onFragmentSaveInstanceState(FragmentManager fm, Fragment f, Bundle outState) {
                            super.onFragmentSaveInstanceState(fm, f, outState);
                        }

                        @Override
                        public void onFragmentViewDestroyed(FragmentManager fm, Fragment f) {
                            super.onFragmentViewDestroyed(fm, f);
                        }

                        @Override
                        public void onFragmentDestroyed(FragmentManager fm, Fragment f) {
                            super.onFragmentDestroyed(fm, f);
                        }

                        @Override
                        public void onFragmentDetached(FragmentManager fm, Fragment f) {
                            super.onFragmentDetached(fm, f);
                        }
                    }, true);
        }
    }

    @Nullable
    private Uri createUriFromBranch(Uri uri, JSONObject referringParams) {
        if (referringParams == null) {
            return null;
        }
        Uri.Builder builder = new Uri.Builder();
        if (uri != null)
            builder.scheme(uri.getScheme())
                    .authority(uri.getAuthority())
                    .appendEncodedPath(uri.getPath());

        Map<String, Object> objectHashMap = jsonObjectToMap(referringParams);
        for (Map.Entry<String, Object> item : objectHashMap.entrySet()
                ) {
            try {
                builder.appendQueryParameter(item.getKey(), String.valueOf(item.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return builder.build();
    }

    private static Map<String, Object> jsonObjectToMap(JSONObject json) {
        Map<String, Object> retMap = new HashMap<>();
        try {
            if (json != JSONObject.NULL) {
                retMap = toMap(json);
            }
        } catch (JSONException e) {
            Log.i(TAG, "Cannot cast json to object: " + e);
        }
        return retMap;
    }


    private static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
        HashMap<String, Object> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
