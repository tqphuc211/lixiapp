package com.opencheck.client.deeplink;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.net.URLDecoder;

public class InstallReferrerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        Log.d(ApplicationLifecycle.DEEPLINK, "app referrer: " + referrer);
        //Use the referrer
        if (referrer != null && referrer.indexOf("link_click_id") >= 0) {
            try {
                referrer = URLDecoder.decode(referrer, "UTF-8");
                Log.d(ApplicationLifecycle.DEEPLINK, "app referrer 1: ");
                if (referrer.indexOf("&") >= 0)
                    referrer = "lixi://open?" + referrer.substring(0, referrer.indexOf("&"));
                Log.d(ApplicationLifecycle.DEEPLINK, "app referrer 2: " + referrer);
                Intent intent1 = new Intent(context, DeepLinkActivity.class);
                intent1.setData(Uri.parse(referrer));
                ApplicationLifecycle.raw_reference = true;
                context.startActivity(intent1);
            } catch (Exception ex) {
                Log.d(ApplicationLifecycle.DEEPLINK, "rfr err: " + ex.getMessage());
            }
        }
    }
}