package com.opencheck.client;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.opencheck.client.analytics.AnalyticsBuilder;
import com.opencheck.client.deeplink.ApplicationLifecycle;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.utils.LocaleHelper;
import com.opencheck.client.utils.tracking.FacebookTracker;
import com.opencheck.client.utils.tracking.FirebaseTracker;

import java.util.concurrent.Executor;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class LixiApplication extends Application {

    public DisplayImageOptions optionsNomal;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader;

    //region Handle Application Lifecycle

    private static LixiApplication mInstance;

    public static boolean isAppRunning = false;

    public static synchronized LixiApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setUpApplication();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });

        CrashlyticsCore core = new CrashlyticsCore.Builder()
                .build();
        Fabric.with(new Fabric.Builder(this)
                .kits(new Crashlytics.Builder().core(core).build())
                .build());
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        //Log.e("Có lỗi đột xuất", e.getMessage());
        Crashlytics.log(e.getMessage());
        e.printStackTrace();

        Intent intent = new Intent(this, FirstLaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        System.exit(1); // kill off the crashed app*/
    }

    //endregion SOCKET

    @Override
    public void onTerminate() {
        isAppRunning = false;
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context, "vi"));
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleHelper.onAttach(this, "vi");
    }
    //endregion

    //region Handle Oncreate application

    private void setUpApplication() {
        mInstance = this;
        isAppRunning = true;
        FirebaseTracker.initialize(this);
        FacebookTracker.initialize(this);
        setUpGoogleAnalytics();
        setUpDeepLink();
        setUpImageLoader();
        setUpAnalytics();
    }

    private void setUpAnalytics() {
        new AnalyticsBuilder.Builder(this)
                .setSchema("https")
                .setPaths(new String[]{"tracking"})
                .setBaseUrl(BuildConfig.TRACKING).build();
        registerActivityLifecycleCallbacks(new ApplicationLifecycle(this));
    }

    private void setUpImageLoader() {
        this.optionsNomal = new DisplayImageOptions.Builder().showStubImage(R.drawable.image_placeholder).showImageForEmptyUri(R.drawable.image_placeholder).showImageOnFail(R.drawable.image_placeholder).showImageOnLoading(R.drawable.image_placeholder).cacheInMemory().cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
        this.imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        this.initImageLoader(this, imageLoader);
    }

    public void initImageLoader(Context context, com.nostra13.universalimageloader.core.ImageLoader imageLoader) {

        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_app)
                .resetViewBeforeLoading(true)
                .delayBeforeLoading(1000)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13)
                .diskCache(new UnlimitedDiskCache(getCacheDir()))
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .imageDownloader(new BaseImageDownloader(context))
                .imageDecoder(new BaseImageDecoder(true))
                .defaultDisplayImageOptions(imageOptions)
                .writeDebugLogs()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        imageLoader.init(config);
//        imageLoader.clearDiskCache();
//        imageLoader.clearMemoryCache();
    }

    private void setUpDeepLink() {
        // Branch logging for debugging
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(this);
    }

    private void setUpGoogleAnalytics() {
        analytics = GoogleAnalytics.getInstance(this);
        setUpTracker();
    }

    private void setUpTracker() {
//        tracker = analytics.newTracker("UA-68637074-1");
        // tracker = analytics.newTracker("UA-68647432-1");

        // Provide unhandled exceptions reports. ALWAYS do this first after creating the tracker
//        tracker.enableExceptionReporting(true);

        // Enable Remarketing, Demographics & Interests reports
        // https://developers.google.com/analytics/devguides/collection/android/display-features
//        tracker.enableAdvertisingIdCollection(true);

        // Enable automatic activity tracking
//        tracker.enableAutoActivityTracking(false);
    }

    //endregion

    //region GOOGLE ANALYTICS
    /**
     * The Analytics singleton. The field is set in onCreate method override when the application
     * class is initially created.
     */
    private static GoogleAnalytics analytics;

    /**
     * The default app tracker. The field is from onCreate callback when the application is
     * initially created.
     */
    private static Tracker tracker;

    /**
     * Access to the global Analytics singleton. If this method returns null you forgot to either
     * set android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.analytics field in onCreate method override.
     */
    public static GoogleAnalytics analytics() {
        return analytics;
    }

    /**
     * The default app tracker. If this method returns null you forgot to either set
     * android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.tracker field in onCreate method override.
     */
    public static Tracker tracker() {
        return tracker;
    }

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
    //endregion
}
