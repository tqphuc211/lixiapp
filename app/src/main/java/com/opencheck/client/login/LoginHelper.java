package com.opencheck.client.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dataloaders.DeliDataLoader;
import com.opencheck.client.firebase.SendPushTokenController;
import com.opencheck.client.firstlaunch.FirstLaunchActivity;
import com.opencheck.client.home.HomeActivity;
import com.opencheck.client.home.delivery.fragments.DeliveryHomeFragment;
import com.opencheck.client.home.delivery.refactor.LocationSelectionFragment;
import com.opencheck.client.models.NotificationModel;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

/**
 * Created by vutha_000 on 2/28/2018.
 */

public class LoginHelper {
    public static void startHomeActivity(LixiActivity activity) {
        Intent home = new Intent(activity, HomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        activity.startActivity(home);

        if (activity instanceof FirstLaunchActivity && isLoggedIn(activity) && FirstLaunchActivity.isPush) {
            Helper.showLog("OK BEDE");
            try {
                NotificationModel notificationModel = Helper.readNotidfyFromFile(activity, ConstantValue.NOTIFY_MODEL_FILE);
                if (notificationModel != null
                        && notificationModel.getType() != null
                        && (notificationModel.getType().equals(NotificationModel.ADMIN_SURVEY)
                        || notificationModel.getType().equals(NotificationModel.SURVEY)))
                    sendMessage(activity, ConstantValue.PUSH_SURVEY, true);
                else
                    sendMessage(activity, ConstantValue.PUSH_SYSTEM_INTENT, true);
            } catch (Exception e) {
            }
        }
    }

    private static void sendMessage(Context cont, String Action, Boolean isSuccess) {
        Intent intent = new Intent(Action);
        intent.putExtra(ConstantValue.IS_SUCCESS, isSuccess);
        cont.sendBroadcast(intent);
    }

    public static void Logout(LixiActivity activity) {
        new SendPushTokenController(activity, new AppPreferenceHelper(activity).getPushToken(), true);
        DataLoader.clearDeviceToken();
        Helper.clearCart(activity);
        DataLoader.initAPICall(activity, "JWT ", null);
        DeliDataLoader.initAPICall(activity, "JWT ");
        LoginManager.getInstance().logOut();
//        AppPreferenceHelper.getInstance().setLixiTrackerId((new Gson()).toJson(trackQueue.getTrackingConfigModel()));
        new AppPreferenceHelper(activity).clear();
        UserModel.clearAll();
        LocationSelectionFragment.deliHomeShowing = false;
        DeliveryHomeFragment.isRefreshLocation = false;
        if (HomeActivity.getInstance() == null)
            return;

        if (HomeActivity.getInstance().active) {
            HomeActivity.getInstance().recreateFragment();
            HomeActivity.getInstance().navigation.setSelectedItemId(R.id.navigation_delivery);
        } else {
            HomeActivity.getInstance().isLogout = true;
            startHomeActivity(activity);
        }
        activity.finish();
    }

    public static boolean isLoggedIn(Context context) {
        String token = new AppPreferenceHelper(context).getUserToken();
        if (!token.equalsIgnoreCase("JWT ")) {
            return true;
        } else {
            return false;
        }
    }

    public static void startLoginActivity(LixiActivity activity) {
        try {
            new AppPreferenceHelper(activity).setUpdateTokenInterval(0l);
        } catch (Exception ex) {
        }
        Intent login = new Intent(activity, LoginActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        login.putExtra(ConstantValue.ACTION_AFTER_LOGIN, "NONE");
        activity.startActivityForResult(login, ConstantValue.LOGIN_REQUEST_CODE);
    }

    public static void startLoginActivityWithAction(Activity activity, String action) {
        try {
            new AppPreferenceHelper(activity).setUpdateTokenInterval(0l);
        } catch (Exception ex) {
        }
        Intent login = new Intent(activity, LoginActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        Bundle bundle = new Bundle();
        bundle.putString(ConstantValue.ACTION_AFTER_LOGIN, action);
        login.putExtras(bundle);
        activity.startActivityForResult(login, ConstantValue.LOGIN_REQUEST_CODE);
    }

    public static boolean checkVerifyUser() {
        return  (UserModel.getInstance() != null && UserModel.getInstance().isIs_phone_verified());
    }
}
