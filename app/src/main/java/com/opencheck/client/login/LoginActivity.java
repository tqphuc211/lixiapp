package com.opencheck.client.login;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.JsonObject;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.R;
import com.opencheck.client.analytics.TrackEvent;
import com.opencheck.client.databinding.LoginActivityBinding;
import com.opencheck.client.dataloaders.ApiCallBack;
import com.opencheck.client.dataloaders.DataLoader;
import com.opencheck.client.dialogs.AutoDismissNotifyDialog;
import com.opencheck.client.home.newlogin.activity.InputPhoneActivity;
import com.opencheck.client.models.user.UserModel;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;
import com.opencheck.client.utils.LanguageBinding;
import com.opencheck.client.utils.tracking.GlobalTracking;
import com.opencheck.client.utils.tracking.TrackingConstant;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by vutha_000 on 2/28/2018.
 */

public class LoginActivity extends LixiActivity {

    //region Layout Variable
    private Button btn_close;
    private LinearLayout ln_login_facebook, ln_login_phone;
    //endregion

    private BroadcastReceiver broadcastReceiver;
    public CallbackManager callbackManager;

    private LoginActivityBinding mBinding;
    private String facebook_token;
    public static final String VERIFY_FACEBOOK = "verify_facebook";
    public static final String FACEBOOK_TOKEN = "facebook_token";
    private String ACTION_AFTER_LOGIN = "NONE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.login_activity);
        ACTION_AFTER_LOGIN = getIntent().getStringExtra(ConstantValue.ACTION_AFTER_LOGIN);
        initView();
    }

    @Override
    public String getScreenName() {
        return TrackEvent.SCREEN.LOGIN;
    }

    private void initView() {
        btn_close = findViewById(R.id.btn_close);
        ln_login_facebook = findViewById(R.id.ln_login_facebook);
        ln_login_phone = findViewById(R.id.linearLoginPhone);

        setupFacebookButton();
        initUI();
    }

    private void initUI() {
        btn_close.setOnClickListener(this);
        ln_login_phone.setOnClickListener(this);
        ln_login_facebook.setOnClickListener(this);

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                break;
            case R.id.linearLoginPhone:
//                phoneLogin();
                GlobalTracking.trackLoginStartEvent(TrackingConstant.Login.ACCOUNT_KIT);
                Intent intent = new Intent(LoginActivity.this, InputPhoneActivity.class);
                intent.putExtra(ConstantValue.ACTION_AFTER_LOGIN, ACTION_AFTER_LOGIN);
                startActivityForResult(intent, ConstantValue.LOGIN_REQUEST_CODE);
                break;
            case R.id.ln_login_facebook:
                GlobalTracking.trackLoginStartEvent(TrackingConstant.Login.FACEBOOK);
                new AppPreferenceHelper(activity).setReceivedReward(false);
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantValue.LOGIN_REQUEST_CODE:
                if (data != null) {
                    if (data.getBooleanExtra(ConstantValue.LOGIN_RESULT, false)) {
                        gotoHome();
                    }
                }
                break;
        }
    }

    //region Get Token Method

    private void setupFacebookButton() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                requestAccessToken(loginResult);
            }

            @Override
            public void onCancel() {
                showNotifyDialog(LanguageBinding.getString(R.string.login_error_cannot_login_facebook, activity));
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                showNotifyDialog(LanguageBinding.getString(R.string.login_error_login_facebook, activity));
            }
        });
    }

    private void requestAccessToken(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Helper.showLog("Facebook object: " + object);
                        if (object != null) {
                            facebook_token = AccessToken.getCurrentAccessToken().getToken();
                            handleFacebookToken(AccessToken.getCurrentAccessToken().getToken());
                        } else {
                            showNotifyDialog(LanguageBinding.getString(R.string.login_error_account_facebook, activity));
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,first_name,last_name, gender, middle_name, link, name_format");//link,picture{url},
        request.setParameters(parameters);
        request.executeAsync();
    }

    //endregion

    private void showNotifyDialog(String message) {
        AutoDismissNotifyDialog dialog = new AutoDismissNotifyDialog(activity, false, true, false);
        dialog.show();
        dialog.setMessage(message);
    }

    private void handleFacebookToken(String token) {
        // call api facebook
        JsonObject params = new JsonObject();
        params.addProperty("facebook_token", token);
        DataLoader.postLoginFacebook(activity, new ApiCallBack() {
            @Override
            public void handleCallback(boolean isSuccess, Object object) {
                if (isSuccess) {
                    UserModel userModel = (UserModel) object;
                    if (userModel.isIs_phone_verified()) {
                        // vào home
                        GlobalTracking.trackLoggedInEvent(TrackingConstant.Login.FACEBOOK);
                        gotoHome();
                    } else {
                        Intent intentFacebook = new Intent(LoginActivity.this, InputPhoneActivity.class);
                        intentFacebook.putExtra(VERIFY_FACEBOOK, true);
                        intentFacebook.putExtra(FACEBOOK_TOKEN, facebook_token);
                        startActivityForResult(intentFacebook, ConstantValue.LOGIN_REQUEST_CODE);
                    }
                } else {
                    Toast.makeText(activity, object.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, params);
    }

    private void gotoHome() {
        Intent returnIntent = new Intent();
        Bundle returnBundle = getIntent().getExtras();
        returnIntent.putExtras(returnBundle);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideKeyboard();
    }

    @Override
    protected void onDestroy() {
        try {
            new AppPreferenceHelper(activity).setOnRefresh(true);
        } catch (Exception ex) {
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
