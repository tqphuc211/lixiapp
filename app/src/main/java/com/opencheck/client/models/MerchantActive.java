package com.opencheck.client.models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Đồng bộ khi merchant kích hoạt
 */
public class MerchantActive extends ViewModel {
    MutableLiveData<Boolean> active;

    public MutableLiveData<Boolean> getActive() {
        if (active == null){
            active = new MutableLiveData<>();
        }
        return active;
    }
}
