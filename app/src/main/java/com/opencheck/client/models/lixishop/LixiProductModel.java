package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vutha_000 on 3/8/2018.
 */

public class LixiProductModel extends LixiModel {
    private int discount;
    private int discount_price;
    private int evouchers;
    private long id;
    private String name;
    private int orders;
    private int price;
    private int priority;
    private List<String> product_image;
    private List<String> product_image_medium;
    private String product_cover_image;
    private String product_logo;
    private String product_type;
    private int quantity;
    private int cashback_percent;
    private int payment_discount_price;
    private String state;
    private boolean hot;
    private int discount_end_date;

    public int getCashback_percent() {
        return cashback_percent;
    }

    public void setCashback_percent(int cashback_percent) {
        this.cashback_percent = cashback_percent;
    }

    public String getProduct_cover_image() {
        return product_cover_image;
    }

    public void setProduct_cover_image(String product_cover_image) {
        this.product_cover_image = product_cover_image;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public int getDiscount_end_date() {
        return discount_end_date;
    }

    public void setDiscount_end_date(int discount_end_date) {
        this.discount_end_date = discount_end_date;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public int getEvouchers() {
        return evouchers;
    }

    public void setEvouchers(int evouchers) {
        this.evouchers = evouchers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(List<String> product_image) {
        this.product_image = product_image;
    }

    public List<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(List<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
