package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderHistoryModel extends LixiModel {
    private String date;
    private String id;
    private String image;
    private String name;
    private int order_id;
    private int point;
    private String state;
    private String state_text;
    private String type;

    private int typeItem;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getState() {
        return state;
    }

    public String getStateVN() {
        return state_text;
    }

    public int getStateColor() {
        if (state.equals("wait"))
            return 0xFFEB8509;
        else if (state.equals("done"))
            return 0xFF64B408;
        else if (state.equals("cancel"))
            return 0xffac1ea1;
        else if (state.equals("payment_expired"))
            return 0xffac1ea1;
        return 0xFF000000;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeItem() {
        return typeItem;
    }

    public void setTypeItem(int typeItem) {
        this.typeItem = typeItem;
    }

    public String getState_text() {
        return state_text;
    }

    public void setState_text(String state_text) {
        this.state_text = state_text;
    }
}
