package com.opencheck.client.models.apiwrapper;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class ApiWrapperModel<T> implements Serializable {
    private MetaDataModel _meta;
    private T records;

    public MetaDataModel get_meta() {
        return _meta;
    }

    public void set_meta(MetaDataModel _meta) {
        this._meta = _meta;
    }

    public T getRecords() {
        return records;
    }

    public void setRecords(T records) {
        this.records = records;
    }
}
