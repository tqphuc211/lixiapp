package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class SimpleLocation extends LixiModel {
    private double lat;
    private double lon;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {

        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
