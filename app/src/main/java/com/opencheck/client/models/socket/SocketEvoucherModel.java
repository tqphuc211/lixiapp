package com.opencheck.client.models.socket;

import java.io.Serializable;

public class SocketEvoucherModel implements Serializable {
    private int id;
    private boolean is_combo;

    public boolean isIs_combo() {
        return is_combo;
    }

    public void setIs_combo(boolean is_combo) {
        this.is_combo = is_combo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
