package com.opencheck.client.models.merchant.survey;

import com.opencheck.client.models.LixiModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class SurveyModel extends LixiModel {
    private int current_question_index;
    private long id;
    private String merchant_logo;
    private String merchant_name;
    private List<SurveyQuestionModel> questions;
    private List<String> welcome_data;
    private String welcome_message;

    public int getCurrent_question_index() {
        return current_question_index;
    }

    public void setCurrent_question_index(int current_question_index) {
        this.current_question_index = current_question_index;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public List<SurveyQuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SurveyQuestionModel> questions) {
        this.questions = questions;
    }

    public List<String> getWelcome_data() {
        return welcome_data;
    }

    public void setWelcome_data(List<String> welcome_data) {
        this.welcome_data = welcome_data;
    }

    public String getWelcome_message() {
        return welcome_message;
    }

    public void setWelcome_message(String welcome_message) {
        this.welcome_message = welcome_message;
    }
}
