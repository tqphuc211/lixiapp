package com.opencheck.client.models.game;

public class GameDataModel {

    /**
     * id : 4939710
     * user_id : 42861
     * turn : 1
     * type : add_facebook_share
     * object_id : null
     * create_date : 1548301917
     * out_date : 1548349199
     */

    private int id;
    private int user_id;
    private int turn;
    private String type;
    private Object object_id;
    private int create_date;
    private int out_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getObject_id() {
        return object_id;
    }

    public void setObject_id(Object object_id) {
        this.object_id = object_id;
    }

    public int getCreate_date() {
        return create_date;
    }

    public void setCreate_date(int create_date) {
        this.create_date = create_date;
    }

    public int getOut_date() {
        return out_date;
    }

    public void setOut_date(int out_date) {
        this.out_date = out_date;
    }
}
