package com.opencheck.client.models.Product;

import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.home.rating.model.RatingInfo;
import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.PaymentTypeModel;
import com.opencheck.client.models.lixishop.BankModel;
import com.opencheck.client.models.lixishop.ComboItemModel;
import com.opencheck.client.models.lixishop.PaymentMethodModel;
import com.opencheck.client.models.lixishop.StoreApplyModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class ProductCashVoucherDetailModel extends LixiModel {
    private long id;
    private String name;
    private List<String> product_image;
    private List<String> product_image_medium;
    private String share_link;
    private int count_attend;
    private int quantity;
    private int quantity_order_done;// y chang count_attend
    private String contract_end_date;
    private String expired_text;

    private MerchantInfoModel merchant_info;
    private ArrayList<StoreApplyModel> store_apply;

    private String product_type;
    private ArrayList<ComboItemModel> combo_item;

    private String use_condition;
    private String html_strip_text;
    private String html_text;


    private long flash_sale_id;
    private FlashSaleInfo flash_sale_info;


    private List<PaymentTypeModel> payment_type;

    private String exchange_method;
    private String order_method;

    private String shipping_info;
    private RatingInfo rating_info;

    private long payment_discount_price;
    private long payment_price;
    private int cashback_price;
    private List<PaymentMethodModel> payment_method;

    private List<BankModel> card_international;
    private List<BankModel> bank;
    private List<BankModel> payoo_bank;
    private List<BankModel> payoo_card_international;
    private List<BankModel> zalo_bank;
    private List<BankModel> zalo_card_international;

    //    private int orders;
//    private int priority;
//    private String state;
//
//    private List<LixiShopBankTransactionMessage> message;
//    private int evouchers;
//
//    private String condition;
//    private int price;
//    private int discount;
//    private int discount_price;
//    private int discount_end_date;
//    private String description;
//    private int cashback_percent;
//    private long exprired_day;
//    private String what_you_get;

    //region usefull
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(List<String> product_image) {
        this.product_image = product_image;
    }

    public List<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(List<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public String getShare_link() {
        return share_link;
    }

    public void setShare_link(String share_link) {
        this.share_link = share_link;
    }

    public int getCount_attend() {
        return count_attend;
    }

    public void setCount_attend(int count_attend) {
        this.count_attend = count_attend;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity_order_done() {
        return quantity_order_done;
    }

    public void setQuantity_order_done(int quantity_order_done) {
        this.quantity_order_done = quantity_order_done;
    }

    public String getContract_end_date() {
        return contract_end_date;
    }

    public void setContract_end_date(String contract_end_date) {
        this.contract_end_date = contract_end_date;
    }

    public String getExpired_text() {
        return expired_text;
    }

    public void setExpired_text(String expired_text) {
        this.expired_text = expired_text;
    }

    public long getFlash_sale_id() {
        return flash_sale_id;
    }

    public void setFlash_sale_id(long flash_sale_id) {
        this.flash_sale_id = flash_sale_id;
    }

    public FlashSaleInfo getFlash_sale_info() {
        return flash_sale_info;
    }

    public void setFlash_sale_info(FlashSaleInfo flash_sale_info) {
        this.flash_sale_info = flash_sale_info;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public ArrayList<StoreApplyModel> getStore_apply() {
        return store_apply;
    }

    public void setStore_apply(ArrayList<StoreApplyModel> store_apply) {
        this.store_apply = store_apply;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public ArrayList<ComboItemModel> getCombo_item() {
        return combo_item;
    }

    public void setCombo_item(ArrayList<ComboItemModel> combo_item) {
        this.combo_item = combo_item;
    }

    public String getUse_condition() {
        return use_condition;
    }

    public void setUse_condition(String use_condition) {
        this.use_condition = use_condition;
    }

    public String getOrder_method() {
        return order_method;
    }

    public void setOrder_method(String order_method) {
        this.order_method = order_method;
    }

    public String getShipping_info() {
        return shipping_info;
    }

    public void setShipping_info(String shipping_info) {
        this.shipping_info = shipping_info;
    }

    public RatingInfo getRating_info() {
        return rating_info;
    }

    public void setRating_info(RatingInfo rating_info) {
        this.rating_info = rating_info;
    }

    public long getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(long payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public long getPayment_price() {
        return payment_price;
    }

    public void setPayment_price(long payment_price) {
        this.payment_price = payment_price;
    }

    public int getCashback_price() {
        return cashback_price;
    }

    public void setCashback_price(int cashback_price) {
        this.cashback_price = cashback_price;
    }

    public List<PaymentMethodModel> getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(List<PaymentMethodModel> payment_method) {
        this.payment_method = payment_method;
    }

    public List<BankModel> getCard_international() {
        return card_international;
    }

    public void setCard_international(List<BankModel> card_international) {
        this.card_international = card_international;
    }

    public List<BankModel> getBank() {
        return bank;
    }

    public void setBank(List<BankModel> bank) {
        this.bank = bank;
    }

    public List<BankModel> getPayoo_bank() {
        return payoo_bank;
    }

    public void setPayoo_bank(List<BankModel> payoo_bank) {
        this.payoo_bank = payoo_bank;
    }

    public List<BankModel> getPayoo_card_international() {
        return payoo_card_international;
    }

    public void setPayoo_card_international(List<BankModel> payoo_card_international) {
        this.payoo_card_international = payoo_card_international;
    }

    public List<BankModel> getZalo_bank() {
        return zalo_bank;
    }

    public void setZalo_bank(List<BankModel> zalo_bank) {
        this.zalo_bank = zalo_bank;
    }

    public List<BankModel> getZalo_card_international() {
        return zalo_card_international;
    }

    public void setZalo_card_international(List<BankModel> zalo_card_international) {
        this.zalo_card_international = zalo_card_international;
    }

    public String getHtml_text() {
        return html_text;
    }

    public void setHtml_text(String html_text) {
        this.html_text = html_text;
    }

    public String getHtml_strip_text() {
        return html_strip_text;
    }

    public void setHtml_strip_text(String html_strip_text) {
        this.html_strip_text = html_strip_text;
    }

    public List<PaymentTypeModel> getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(List<PaymentTypeModel> payment_type) {
        this.payment_type = payment_type;
    }

    public String getExchange_method() {
        return exchange_method;
    }

    public void setExchange_method(String exchange_method) {
        this.exchange_method = exchange_method;
    }
    //endregion
}
