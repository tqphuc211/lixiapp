package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class EvoucherDetailModel extends LixiModel {
    private long id;
    private long expired_time;
    private long active_ttl;
    private boolean is_lixi_code; // check
    private boolean is_show_serial;
    private boolean send_pin;
    private MerchantInfoModel merchant_info;
    private String name;
    private String pin;
    private int rating;
    private String serial;
    private long price;
    private long product;
    private String state;
    private ArrayList<StoreApplyModel> store_apply;
    private String use_condition;
    private String what_you_get;
    private String transform; // default, qr_code, bar_code

    public boolean isIs_show_serial() {
        return is_show_serial;
    }

    public void setIs_show_serial(boolean is_show_serial) {
        this.is_show_serial = is_show_serial;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getActive_ttl() {
        return active_ttl;
    }

    public void setActive_ttl(long active_ttl) {
        this.active_ttl = active_ttl;
    }

    public boolean isIs_lixi_code() {
        return is_lixi_code;
    }

    public long getExpired_time() {
        return expired_time;
    }

    public void setExpired_time(long expired_time) {
        this.expired_time = expired_time;
    }

    public boolean is_lixi_code() {
        return is_lixi_code;
    }

    public void setIs_lixi_code(boolean is_lixi_code) {
        this.is_lixi_code = is_lixi_code;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<StoreApplyModel> getStore_apply() {
        return store_apply;
    }

    public void setStore_apply(ArrayList<StoreApplyModel> store_apply) {
        this.store_apply = store_apply;
    }

    public String getUse_condition() {
        return use_condition;
    }

    public void setUse_condition(String use_condition) {
        this.use_condition = use_condition;
    }

    public String getWhat_you_get() {
        return what_you_get;
    }

    public void setWhat_you_get(String what_you_get) {
        this.what_you_get = what_you_get;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean isSend_pin() {
        return send_pin;
    }

    public void setSend_pin(boolean send_pin) {
        this.send_pin = send_pin;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getTransform() {
        return transform;
    }

    public void setTransform(String transform) {
        this.transform = transform;
    }
}
