package com.opencheck.client.models.game;

public class GameErrorModel {

    /**
     * code : user_added_turn
     * message : Bạn vượt quá số lần nhận trong ngày
     * data : null
     * exception : null
     */

    private String code;
    private String message;
    private Object data;
    private Object exception;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getException() {
        return exception;
    }

    public void setException(Object exception) {
        this.exception = exception;
    }
}
