package com.opencheck.client.models.user;

import android.support.annotation.Nullable;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.AppPreferenceHelper;

public class UserModel extends LixiModel {

    private String id = "";
    private String avatar = "";
    private String username = "";
    private String phone = "";
    private boolean is_phone_verified = false;
    private String available_point = "";
    private String code = "";
    private String fullname = "";
    private String name = "";
    private String datecreated = "";

    private String address = "";
    private String email = "";
    private String identification = "";
    private String user_code = "";

    private CityDistrictModel cityModel = new CityDistrictModel();
    private CityDistrictModel districtModel = new CityDistrictModel();
    private CityDistrictModel wardModel = new CityDistrictModel();

    /**
     * access_token : string
     * gender : string
     * level : string
     * merchant_id : 0
     * need_update : true
     * user_id : 0
     */

    private String birthday = "";
    private String gender = "";
    private String level = "";
    private int merchant_id = 0;
    private boolean need_update = true;
    private int user_id = 0;
    private int evoucher_bought_count = 0;
    private String source = "";

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isIs_phone_verified() {
        return is_phone_verified;
    }

    public void setIs_phone_verified(boolean is_phone_verified) {
        this.is_phone_verified = is_phone_verified;
        SaveProfile(this);
    }

    public String getAvailable_point() {
        return available_point;
    }

    public void setAvailable_point(String available_point) {
        this.available_point = available_point;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public CityDistrictModel getCityModel() {
        return cityModel;
    }

    public void setCityModel(CityDistrictModel cityModel) {
        this.cityModel = cityModel;
    }

    public CityDistrictModel getDistrictModel() {
        return districtModel;
    }

    public void setDistrictModel(CityDistrictModel districtModel) {
        this.districtModel = districtModel;
    }

    public CityDistrictModel getWardModel() {
        return wardModel;
    }

    public void setWardModel(CityDistrictModel wardModel) {
        this.wardModel = wardModel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public boolean isNeed_update() {
        return need_update;
    }

    public void setNeed_update(boolean need_update) {
        this.need_update = need_update;
        SaveProfile(this);
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getEvoucher_bought_count() {
        return evoucher_bought_count;
    }

    public void setEvoucher_bought_count(int evoucher_bought_count) {
        this.evoucher_bought_count = evoucher_bought_count;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    //    private String can_update_ref = "";
//
//    private String merchant_id = "";
//    private String count_expense = "";
//    private String count_order = "";
//
//
//    private String count_exp = "";
//    private String pending_point = "";
//    private String star = "";
//    private String status = "";
//
//    private String star_name = "";
//
//    private int merchant_favorite_count = 0;
//
//    private String required_old_password = "";

    private static UserModel instance;

    public static void SaveProfile(UserModel userModel) {
        AppPreferenceHelper.getInstance().setUserModel(userModel);
        instance = userModel;
    }

    public static void clearInstance(){
        instance = null;
    }

    public static void clearAll(){
        clearInstance();
        AppPreferenceHelper.getInstance().clearUserModel();
    }

    @Nullable
    public static UserModel getInstance() {
        synchronized (UserModel.class) {
            if (instance == null) {
                instance = AppPreferenceHelper.getInstance().getUserModel();
            }
        }
        return instance;
    }
}
