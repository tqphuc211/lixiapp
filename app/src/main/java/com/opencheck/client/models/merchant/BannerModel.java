package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class BannerModel extends LixiModel {
    private String id = "";

    private String alt = "";
    private String caption = "";
    private String image = "";
    private String image_medium = "";
    private String image_small = "";
    private String link = "";
    private String link_app = "";
    private String name = "";
    private String sequence = "";

    public String getLink_app() {
        return link_app;
    }

    public void setLink_app(String link_app) {
        this.link_app = link_app;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_medium() {
        return image_medium;
    }

    public void setImage_medium(String image_medium) {
        this.image_medium = image_medium;
    }

    public String getImage_small() {
        return image_small;
    }

    public void setImage_small(String image_small) {
        this.image_small = image_small;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}


