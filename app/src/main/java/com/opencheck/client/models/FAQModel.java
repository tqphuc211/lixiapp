package com.opencheck.client.models;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/20/2018.
 */
public class FAQModel extends LixiModel {
    private String answer;
    private int id;
    private boolean is_active;
    private String question;
    private int sequence;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }
}