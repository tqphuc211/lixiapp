package com.opencheck.client.models;

import com.opencheck.client.models.apierror.ExceptionModel;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/1/2018.
 */
/**
 * This model class is the base model class every API model must extend
 * */
public class LixiModel implements Serializable{
    private ExceptionModel exception;
    private String message_code;
    private int status_code;

    public ExceptionModel getException() {
        return exception;
    }

    public void setException(ExceptionModel exception) {
        this.exception = exception;
    }

    public String getMessage_code() {
        return message_code;
    }

    public void setMessage_code(String message_code) {
        this.message_code = message_code;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }
}
