package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class ProductInfoModel extends LixiModel {
    private long id;
    private String name;
    private long price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
