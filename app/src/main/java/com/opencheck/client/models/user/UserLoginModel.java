package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;

public class UserLoginModel extends LixiModel {

    /**
     * access_token : string
     * email : string
     * fullname : string
     * gender : string
     * is_phone_verified : true
     * level : string
     * merchant_id : 0
     * need_update : true
     * user_id : 0
     */

    private String access_token;
    private String email;
    private String fullname;
    private String gender;
    private boolean is_phone_verified;
    private String level;
    private int merchant_id;
    private boolean need_update;
    private int user_id;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isIs_phone_verified() {
        return is_phone_verified;
    }

    public void setIs_phone_verified(boolean is_phone_verified) {
        this.is_phone_verified = is_phone_verified;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public boolean isNeed_update() {
        return need_update;
    }

    public void setNeed_update(boolean need_update) {
        this.need_update = need_update;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
