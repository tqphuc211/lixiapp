package com.opencheck.client.models.lixishop;

import com.opencheck.client.home.flashsale.Model.FlashSaleInfo;
import com.opencheck.client.utils.ConstantValue;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class LixiShopEvoucherModel implements Serializable {

    private int cashback_percent;
    private int cashback_price;
    private int discount_price;
    private int count_store;
    private long id;
    private int merchant;
    private String merchant_name;
    private String name;
    private int payment_discount_price;
    private int payment_price;
    private int price;
    private List<String> product_image = null;
    private List<String> product_image_medium = null;
    private String product_cover_image;
    private String product_logo;
    private int quantity_order_done;
    private String product_type;
    private StoreApplyModel store_info;

    private int flash_sale_id;
    private FlashSaleInfo flash_sale_info;

    public float getUser_distance() {
        return user_distance;
    }

    public void setUser_distance(float user_distance) {
        this.user_distance = user_distance;
    }

    private float user_distance;

    public int getCashback_percent() {
        return cashback_percent;
    }

    public void setCashback_percent(int cashback_percent) {
        this.cashback_percent = cashback_percent;
    }

    public int getCount_store() {
        return count_store;
    }

    public void setCount_store(int count_store) {
        this.count_store = count_store;
    }

    public String getProduct_cover_image() {
        if (product_cover_image == null)
            return "";
        return product_cover_image;
    }

    public void setProduct_cover_image(String product_cover_image) {
        this.product_cover_image = product_cover_image;
    }

    public int getCashback_price() {
        return cashback_price;
    }

    public void setCashback_price(int cashback_price) {
        this.cashback_price = cashback_price;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMerchant() {
        return merchant;
    }

    public void setMerchant(int merchant) {
        this.merchant = merchant;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public int getPayment_price() {
        return payment_price;
    }

    public void setPayment_price(int payment_price) {
        this.payment_price = payment_price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(List<String> product_image) {
        this.product_image = product_image;
    }

    public List<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(List<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public int getQuantity_order_done() {
        return quantity_order_done;
    }

    public void setQuantity_order_done(int quantity_order_done) {
        this.quantity_order_done = quantity_order_done;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public int getFlash_sale_id() {
        return flash_sale_id;
//        return 0;
    }

    public void setFlash_sale_id(int flash_sale_id) {
        this.flash_sale_id = flash_sale_id;
    }

    public FlashSaleInfo getFlash_sale_info() {
        return flash_sale_info;
    }

    public void setFlash_sale_info(FlashSaleInfo flash_sale_info) {
        this.flash_sale_info = flash_sale_info;
    }

    public StoreApplyModel getStore_info() {
        return store_info;
    }

    public void setStore_info(StoreApplyModel store_info) {
        this.store_info = store_info;
    }
}