package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class LixiCodeBarnerModel extends LixiModel {
    private String link;
    private String name;
    private int priority;

    public String getImage() {
        return link;
    }

    public void setImage(String link) {
        this.link = link;
    }

    public String getText() {
        return name;
    }

    public void setText(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
