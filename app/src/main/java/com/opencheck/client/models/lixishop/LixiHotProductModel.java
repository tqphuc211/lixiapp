package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class LixiHotProductModel extends LixiModel {
    private int cashback_percent;
    private int cashback_price;
    private String cashback_end_date;
    private int discount_price;
    private long id;
    private String name;
    private int payment_discount_price;
    private int payment_price;
    private int price;
    private ArrayList<String> product_image;
    private ArrayList<String> product_image_medium;
    private String product_cover_image;
    private String product_logo;
    private String product_type;
    private int quantity_order_done;

    public int getCashback_percent() {
        return cashback_percent;
    }

    public void setCashback_percent(int cashback_percent) {
        this.cashback_percent = cashback_percent;
    }

    public String getProduct_cover_image() {
        return product_cover_image;
    }

    public void setProduct_cover_image(String product_cover_image) {
        this.product_cover_image = product_cover_image;
    }

    public int getCashback_price() {
        return cashback_price;
    }

    public void setCashback_price(int cashback_price) {
        this.cashback_price = cashback_price;
    }

    public String getCashback_end_date() {
        return cashback_end_date;
    }

    public void setCashback_end_date(String cashback_end_date) {
        this.cashback_end_date = cashback_end_date;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public int getPayment_price() {
        return payment_price;
    }

    public void setPayment_price(int payment_price) {
        this.payment_price = payment_price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return (ArrayList<String>) Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(ArrayList<String> product_image) {
        this.product_image = product_image;
    }

    public ArrayList<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return (ArrayList<String>) Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(ArrayList<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public int getQuantity_order_done() {
        return quantity_order_done;
    }

    public void setQuantity_order_done(int quantity_order_done) {
        this.quantity_order_done = quantity_order_done;
    }
}
