package com.opencheck.client.models.lixishop;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class BankModel implements Serializable {
    private String bank_logo = "";
    private String code = "";
    private String name = "";
    private int min_value = 0;
    private boolean isSelected = false;

    public int getMin_value() {
        return min_value;
    }

    public void setMin_value(int min_value) {
        this.min_value = min_value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getBank_logo() {
        return bank_logo;
    }

    public void setBank_logo(String bank_logo) {
        this.bank_logo = bank_logo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
