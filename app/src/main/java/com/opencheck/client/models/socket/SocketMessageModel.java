package com.opencheck.client.models.socket;

import com.opencheck.client.models.LixiModel;

import java.io.Serializable;

public class SocketMessageModel implements Serializable {
    private String name;
    private SocketMetaModel meta;
    private SocketDataModel data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SocketMetaModel getMeta() {
        return meta;
    }

    public void setMeta(SocketMetaModel meta) {
        this.meta = meta;
    }

    public SocketDataModel getData() {
        return data;
    }

    public void setData(SocketDataModel data) {
        this.data = data;
    }
}
