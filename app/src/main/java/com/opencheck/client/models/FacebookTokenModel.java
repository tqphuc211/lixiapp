package com.opencheck.client.models;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class FacebookTokenModel implements Serializable {
    private String platform = "android";
    private String source;
    private String token;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
