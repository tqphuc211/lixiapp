package com.opencheck.client.models.socket;

import org.json.JSONObject;

import java.io.Serializable;

public class SocketDataModel implements Serializable {
    private String action;
    private String name;
    private Boolean first_login;
    private Long product_id;
    private String transaction_id;
    private Long id;
    private String type;
    private String screen;
    private String order_uuid;
    private ParamsModel params;

    public String getOrder_uuid() {
        return order_uuid;
    }

    public void setOrder_uuid(String order_uuid) {
        this.order_uuid = order_uuid;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParamsModel getParams() {
        return params;
    }

    public void setParams(ParamsModel params) {
        this.params = params;
    }

    public Boolean getFirst_login() {
        return first_login;
    }

    public void setFirst_login(Boolean first_login) {
        this.first_login = first_login;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }
}
