package com.opencheck.client.models.merchant.survey;

import java.io.Serializable;
import java.util.List;

public class SurveyAnswerResultModel implements Serializable{
    private boolean is_done;
    private String merchant_logo;
    private String merchant_name;
    private int next_question_index;
    private List<String> response_data;
    private String response_message;
    private String reward_code;

    public boolean is_done() {
        return is_done;
    }

    public void setIs_done(boolean is_done) {
        this.is_done = is_done;
    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public int getNext_question_index() {
        return next_question_index;
    }

    public void setNext_question_index(int next_question_index) {
        this.next_question_index = next_question_index;
    }

    public List<String> getResponse_data() {
        return response_data;
    }

    public void setResponse_data(List<String> response_data) {
        this.response_data = response_data;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }

    public String getReward_code() {
        return reward_code;
    }

    public void setReward_code(String reward_code) {
        this.reward_code = reward_code;
    }
}
