package com.opencheck.client.models.merchant;

import android.content.Context;

import com.opencheck.client.R;
import com.opencheck.client.utils.LanguageBinding;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class PromotionModel implements Serializable {
    private String id ;

    //    private String company_name ;
    private String apply_text ;
    private String condition ;
    private String count_join ;
    private String count_joined ;
    private String is_coupon ;
    private String description ;
    private String discount_rate ;
    private String duration ;
    private String end_date ;
    private String is_delivery ;
    private String is_favorite ;
    private String is_hot ;
    private String name ;
    private String start_date ;
    private String denominator_point ;
    private String image ;

    private String count_receive ;
    private String is_receive ;
    private Boolean never_expire = false;
    private Boolean is_expired = false;
    private String point_percent ;
    private String ratio_receive_point ;

    private boolean isDefault = false;

    private String promotion_type ;
    private String fixed_value ;


    private ArrayList<StoreModel> listStore = new ArrayList<StoreModel>();

    private ArrayList<ImageModel> listImage = new ArrayList<ImageModel>();
    private MerchantModel merchantModel = new MerchantModel();

    private int total = 0;

    public String getPromotion_type() {
        return promotion_type;
    }


    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }


    public static PromotionModel getDefaultPromotionOfMerchant(MerchantModel merchantModel, Context context) {
        PromotionModel rs = new PromotionModel();

        rs.setName(LanguageBinding.getString(R.string.tab_promotion, context) + " " + merchantModel.getDiscount_rate() + "% " + LanguageBinding.getString(R.string.total_bill, context));
        rs.setApply_text(LanguageBinding.getString(R.string.total_bill, context));
        rs.setDiscount_rate(merchantModel.getDiscount_rate());
        rs.setNever_expire(true);
        rs.setEnd_date(merchantModel.getContract_end_date());
        if (merchantModel.getContract_end_date() == null || merchantModel.getContract_end_date().length() == 0
                || merchantModel.getContract_end_date().equals("null")) {
            rs.setEnd_date(LanguageBinding.getString(R.string.unknown, context));
        }

        ArrayList<ImageModel> listItem = new ArrayList<ImageModel>();
        for (int i = 0; i < merchantModel.getImage_list().size(); i++) {
            ImageModel im = new ImageModel();
            im.setImage(merchantModel.getImage_list().get(i).getImage_medium());
            im.setImage_medium(merchantModel.getImage_list().get(i).getImage_medium());
            listItem.add(im);
        }
        rs.setListImage(listItem);
        rs.setDescription(merchantModel.getInfo_promotion());
        rs.setPromotion_type("percent");
//        rs.setDescription("Khuyến mãi " + merchantModel.getDiscount_rate() + "% trên tổng hóa đơn");
        if (merchantModel.getInfo_condition() == null || merchantModel.getInfo_condition().length() == 0)
            rs.setCondition(LanguageBinding.getString(R.string.total_bill, context));
        else
            rs.setCondition(merchantModel.getInfo_condition());
//        rs.setCondition(" trên tổng hóa đơn");

        rs.setMerchantModel(merchantModel);
        rs.setListStore(merchantModel.getListStore());
        rs.setDefault(true);

        return rs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(String discount_rate) {
        this.discount_rate = discount_rate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getIs_delivery() {
        return is_delivery;
    }

    public void setIs_delivery(String is_delivery) {
        this.is_delivery = is_delivery;
    }

    public String getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(String is_hot) {
        this.is_hot = is_hot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getDenominator_point() {
        return denominator_point;
    }

    public void setDenominator_point(String denominator_point) {
        this.denominator_point = denominator_point;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<ImageModel> getListImage() {
        return listImage;
    }

    public void setListImage(ArrayList<ImageModel> listImage) {
        this.listImage = listImage;
    }

    public void setMerchantModel(MerchantModel merchantModel) {
        this.merchantModel = merchantModel;
    }

    public MerchantModel getMerchantModel() {
        return merchantModel;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public String getCount_join() {
        return count_join;
    }

    public void setCount_join(String count_join) {
        this.count_join = count_join;
    }

    public String getCount_joined() {
        return count_joined;
    }

    public void setCount_joined(String count_joined) {
        this.count_joined = count_joined;
    }

    public String getIs_coupon() {
        return is_coupon;
    }

    public void setIs_coupon(String is_coupon) {
        this.is_coupon = is_coupon;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getCount_receive() {
        return count_receive;
    }

    public void setCount_receive(String count_receive) {
        this.count_receive = count_receive;
    }

    public String getIs_receive() {
        return is_receive;
    }

    public void setIs_receive(String is_receive) {
        this.is_receive = is_receive;
    }

    public Boolean getNever_expire() {
        return never_expire;
    }

    public void setNever_expire(Boolean never_expire) {
        this.never_expire = never_expire;
    }

    public String getPoint_percent() {
        return point_percent;
    }

    public void setPoint_percent(String point_percent) {
        this.point_percent = point_percent;
    }

    public String getRatio_receive_point() {
        return ratio_receive_point;
    }

    public void setRatio_receive_point(String ratio_receive_point) {
        this.ratio_receive_point = ratio_receive_point;
    }

    public ArrayList<StoreModel> getListStore() {
        return listStore;
    }

    public void setListStore(ArrayList<StoreModel> listStore) {
        this.listStore = listStore;
    }

    public Boolean getIs_expired() {
        return is_expired;
    }

    public void setIs_expired(Boolean is_expired) {
        this.is_expired = is_expired;
    }

    public String getApply_text() {
        return apply_text;
    }

    public void setApply_text(String apply_text) {
        this.apply_text = apply_text;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getFixed_value() {
        return fixed_value;
    }

    public void setFixed_value(String fixed_value) {
        this.fixed_value = fixed_value;
    }

}
