package com.opencheck.client.models.merchant;

import com.google.gson.annotations.SerializedName;
import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class SearchResultModel extends LixiModel {
    public static String STATUS_ENABLE = "1";
    public static String STATUS_CONFIRM = "2";
    public static String STATUS_DISABLE = "3";
    public static String STATUS_REJECT = "5";
    public static String STATUS_DRAFT = "6";


    private String id = "";
    private String total = "0";
    private String recordPerPage = "0";
    private String udid = "";
    private String uid = "";
    private String cid = "";
    private String title = "";
    private String description = "";
    private String image = "";
    private String smallimage = "";
    private String price = "";
    private String sold = ""; //SOLD_SELL = 1 - SOLD_BUY = 3
    private String cityid = "";
    private String districtid = "";
    private String contactname = "";
    private String contactemail = "";
    private String contactphone = "";
    private String contactaddress = "";
    private String countview = "";
    private String countfavorite = "";
    private String status = "1";
    private String statusname = "";
    private String lat = "0";
    @SerializedName("long")
    private String longtitute = "0";
    private String datecreated = "";
    private String datemodified = "";
    private String isfavorite = "0";
    private String zoom = "11";
    private String countupdate = "0";
    private String url = "";
    private String slug = "";
    private String slugForward = "";
    private String shortdescription = "";

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String
    getCid() {
        return cid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        if (price.equalsIgnoreCase("")) {
            return "0";
        }
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getContactphone() {
        return contactphone;
    }

    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }

    public String getContactaddress() {
        return contactaddress;
    }

    public void setContactaddress(String contactaddress) {
        this.contactaddress = contactaddress;
    }

    public String getCountview() {
        return countview;
    }

    public void setCountview(String countview) {
        this.countview = countview;
    }

    public String getCountfavorite() {
        return countfavorite;
    }

    public void setCountfavorite(String countfavorite) {
        this.countfavorite = countfavorite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLattitut() {
        return lat;
    }

    public void setLattitut(String lattitut) {
        this.lat = lattitut;
    }

    public String getLongtitute() {
        return longtitute;
    }

    public void setLongtitute(String longtitute) {
        this.longtitute = longtitute;
    }

    public String getDatecreated() {
        if (datecreated.equalsIgnoreCase("")) {
            return "0";
        }
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public void setIsfavorite(String isfavorite) {
        this.isfavorite = isfavorite;
    }

    public String getIsfavorite() {
        if (isfavorite.equalsIgnoreCase("")) {
            return "0";
        }
        return isfavorite;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getZoom() {
        if (zoom == null || zoom.equalsIgnoreCase("") || zoom.equalsIgnoreCase("0")) {
            return "11";
        }
        return zoom;
    }

    public void setCountupdate(String countupdate) {
        this.countupdate = countupdate;
    }

    public String getCountupdate() {
        if (countupdate.equalsIgnoreCase("")) {
            return "0";
        }
        return countupdate;
    }

    public void setDatemodified(String datemodified) {
        this.datemodified = datemodified;
    }

    public String getDatemodified() {
        return datemodified;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(String recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlugForward(String slugForward) {
        this.slugForward = slugForward;
    }

    public String getSlugForward() {
        return slugForward;
    }

    public void setShortDesc(String shortDesc) {
        this.shortdescription = shortDesc;
    }

    public String getShortDesc() {
        return shortdescription;
    }

    public void setSmallimage(String smallimage) {
        this.smallimage = smallimage;
    }

    public String getSmallimage() {
        return smallimage;
    }
}
