package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderDetailLogModel extends LixiModel {
    private String action;
    private String date;
    private int point;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getStateColor() {
        if (action.equals("wait"))
            return 0xFFEB8509;
        else if (action.equals("done"))
            return 0xFF64B408;
        else if (action.equals("cancel"))
            return 0xffac1ea1;
        return 0xFF000000;
    }
}
