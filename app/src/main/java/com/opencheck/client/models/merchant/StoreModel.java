package com.opencheck.client.models.merchant;

import com.opencheck.client.models.user.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class StoreModel implements Serializable {
    private String id ;
    private String address ;
    private String opening_time ;
    private String closing_time ;
    private String city_id ;
    private String code ;
    private String merchant_id ;
    private String count_current_point ;
    private String count_point ;
    private String count_point_transaction ;
    private String count_promotion ;
    private String district_id ;
    private String email ;
    private String is_store ;
    private String lat ;
    private String lng ;
    private String name ;
    private String parent_id ;
    private String partner_id ;
    private String point_percent ;
    private String phone ;
    private String status ;
    private String type_customer ;
    private String ward_id ;
    private String ratio_receive_point ;
    private String denominator_point ;
    private ArrayList<ImageModel> listImage = new ArrayList<ImageModel>();
    private ArrayList<UserModel> listManager = new ArrayList<UserModel>();
//    private UserModel manager = new UserModel();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getCount_current_point() {
        return count_current_point;
    }

    public void setCount_current_point(String count_current_point) {
        this.count_current_point = count_current_point;
    }

    public String getCount_point() {
        return count_point;
    }

    public void setCount_point(String count_point) {
        this.count_point = count_point;
    }

    public String getCount_point_transaction() {
        return count_point_transaction;
    }

    public void setCount_point_transaction(String count_point_transaction) {
        this.count_point_transaction = count_point_transaction;
    }

    public String getCount_promotion() {
        return count_promotion;
    }

    public void setCount_promotion(String count_promotion) {
        this.count_promotion = count_promotion;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIs_store() {
        return is_store;
    }

    public void setIs_store(String is_store) {
        this.is_store = is_store;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPoint_percent() {
        return point_percent;
    }

    public void setPoint_percent(String point_percent) {
        this.point_percent = point_percent;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType_customer() {
        return type_customer;
    }

    public void setType_customer(String type_customer) {
        this.type_customer = type_customer;
    }

    public String getWard_id() {
        return ward_id;
    }

    public void setWard_id(String ward_id) {
        this.ward_id = ward_id;
    }

    public String getRatio_receive_point() {
        if (ratio_receive_point.equalsIgnoreCase("")) {
            return "0";
        }
        return ratio_receive_point;
    }

    public void setRatio_receive_point(String ratio_receive_point) {
        this.ratio_receive_point = ratio_receive_point;
    }

    public ArrayList<ImageModel> getListImage() {
        return listImage;
    }

    public void setListImage(ArrayList<ImageModel> listImage) {
        this.listImage = listImage;
    }

    public void setDenominator_point(String denominator_point) {
        this.denominator_point = denominator_point;
    }

    public String getDenominator_point() {
        if (denominator_point.equalsIgnoreCase("")) {
            return "0";
        }
        return denominator_point;
    }

    public void setListManager(ArrayList<UserModel> listManager) {
        this.listManager = listManager;
    }

    public ArrayList<UserModel> getListManager() {
        return listManager;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }
}