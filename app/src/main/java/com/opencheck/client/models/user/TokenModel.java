package com.opencheck.client.models.user;

import com.google.gson.annotations.SerializedName;
import com.opencheck.client.models.LixiModel;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class TokenModel extends LixiModel {
    private String access_token = "";
    @SerializedName("level")
    private String userLevel = "";

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }
}
