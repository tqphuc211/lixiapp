package com.opencheck.client.models.merchant.survey;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class SurveyAnswerModel extends LixiModel {
    private String answer_name;
    private String check_icon;
    private long id;
    private String uncheck_icon;

    public String getAnswer_name() {
        return answer_name;
    }

    public void setAnswer_name(String answer_name) {
        this.answer_name = answer_name;
    }

    public String getCheck_icon() {
        return check_icon;
    }

    public void setCheck_icon(String check_icon) {
        this.check_icon = check_icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUncheck_icon() {
        return uncheck_icon;
    }

    public void setUncheck_icon(String uncheck_icon) {
        this.uncheck_icon = uncheck_icon;
    }
}
