package com.opencheck.client.models.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tony Tuan on 04/02/2018.
 */

public class ImageModel implements Parcelable{

    public long id;
    public String name;
    public String path;
    public boolean isSelected;
    public String timestamp;

    public ImageModel(long id, String name, String path, String timestamp, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.isSelected = isSelected;
        this.timestamp = timestamp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(path);
        dest.writeString(timestamp);
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel source) {
            return new ImageModel(source);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    private ImageModel(Parcel in) {
        id = in.readLong();
        name = in.readString();
        path = in.readString();
        timestamp = in.readString();
    }
}
