package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class OrderCheckModel extends LixiModel {
    private long id;
    private long price;
    private long discount_price;
    private int quantity;
    private String payment_method;
    private String history_id;
    private String payment_method_value;
    private String head_message;


    public OrderCheckModel() {
    }

    public OrderCheckModel(int id, long price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public String getMessage() {
        return head_message;
    }

    public void setMessage(String message) {
        this.head_message = message;
    }

    public String getPayment_method_value() {
        return payment_method_value;
    }

    public void setPayment_method_value(String payment_method_value) {
        this.payment_method_value = payment_method_value;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getHistory_id() {
        return history_id;
    }

    public void setHistory_id(String history_id) {
        this.history_id = history_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(long discount_price) {
        this.discount_price = discount_price;
    }
}
