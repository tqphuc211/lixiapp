package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/20/2018.
 */

public class MenuModel extends LixiModel {
    private String link;
    private String link_medium;
    private String link_small;
    private String name;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink_medium() {
        return link_medium;
    }

    public void setLink_medium(String link_medium) {
        this.link_medium = link_medium;
    }

    public String getLink_small() {
        return link_small;
    }

    public void setLink_small(String link_small) {
        this.link_small = link_small;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
