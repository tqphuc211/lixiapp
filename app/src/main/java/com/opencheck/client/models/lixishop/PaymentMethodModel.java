package com.opencheck.client.models.lixishop;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class PaymentMethodModel implements Serializable {

    public static final String KEY_PAYMENT_COD = "cod";
    public static final String KEY_PAYMENT_AT_STORE = "at_store";
    public static final String KEY_PAYMENT_MOMO = "wallet_momo";
    public static final String KEY_PAYMENT_ZALO_WALLET = "wallet_zalo";
    public static final String KEY_PAYMENT_NAPAS = "napas_atm";
    public static final String KEY_PAYMENT_NAPAS_CC = "napas_cc";
    public static final String KEY_PAYMENT_PAYOO = "payoo";
    public static final String KEY_PAYMENT_VTC = "";

    public static final int PAYMENT_COD = 1;
    public static final int PAYMENT = 0;

    //dafault is payment
    private int type = PAYMENT;
    private String key = "";
    private String value = "";
    private String paymentCodFee = "";

    private boolean selected = false;
    private BankModel bankSelected = new BankModel();


    public BankModel getBankSelected() {
        return bankSelected;
    }

    public void setBankSelected(BankModel bankSelected) {
        this.bankSelected = bankSelected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPaymentCodFee() {
        return paymentCodFee;
    }

    public void setPaymentCodFee(String paymentCodFee) {
        this.paymentCodFee = paymentCodFee;
    }
}

