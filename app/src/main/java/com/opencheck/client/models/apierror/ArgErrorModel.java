package com.opencheck.client.models.apierror;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/1/2018.
 */

class ArgErrorModel implements Serializable {
    private String message;
    private ArrayList<String> args;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<String> getArgs() {
        return args;
    }

    public void setArgs(ArrayList<String> args) {
        this.args = args;
    }
}
