package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderDetailUserModel extends LixiModel {
    private int id;
    private String name;
    private String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
