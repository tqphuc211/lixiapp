package com.opencheck.client.models;

import android.content.Context;
import android.os.Build;

import com.opencheck.client.BuildConfig;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.utils.AppPreferenceHelper;

/**
 * Created by vutha_000 on 3/2/2018.
 */

public class ConfigModel extends LixiModel {
    private int androidLixiUpdate = 50;
    private int payment_cod_enable = 0;
    private String payment_cod_fee = "{\"min\" : 15000, \"max\": 25000}";
    private int napas_add_card_enable = 0;
    private int location_change_enable = 1;
    private String socket_url = BuildConfig.DELI_SOCKET;
    private int lixi_pay_new = 1;
    private int lixi_select_bank_inapp_for_payoo = 1;
    private int order_payment_expired_at_store = 90;
    private float home_banner_ratio = 0.5625f;
    private String defaultSupportPhone = "0901 863 639";
    private int remind_update_info_interval = 30; // second
    private String reference_code_guide_link = "";
    private String reference_code_share_content = "";
    private int reference_code_profile_enable = 1;
    private String reference_code_lixi_text = "";
    private int shop_services_enable = 1;
    private String reward_tab_title = "";
    private String shop_tabbar_icon = "";
    private String shop_tabbar_title = "";


//    "game_home_button_enable": 1,
//            "game_home_button_icon": "https://static.lixiapp.com/oc_gallery/2019/01/23/9796f6ed41c2b5c1675597f8894f84715ee098a2.png",
//            "game_main_url": "https://dev.lixiapp.com/game/eventlixi",

    private int game_home_button_enable = 0;

    private String game_home_button_icon = "";

    private String game_main_url = "";

    private String game_api_url = "";

    private String game_share_url = "";

    private int android_lastest_version = BuildConfig.VERSION_CODE;

    public int getAndroid_lastest_version() {
        return android_lastest_version;
    }

    public void setAndroid_lastest_version(int android_lastest_version) {
        this.android_lastest_version = android_lastest_version;
    }

    public int getLixi_pay_new() {
        return lixi_pay_new;
    }

    public void setLixi_pay_new(int lixi_pay_new) {
        this.lixi_pay_new = lixi_pay_new;
    }

    public int getLixi_select_bank() {
        return lixi_select_bank_inapp_for_payoo;
    }

    public void setLixi_select_bank(int lixi_select_bank) {
        this.lixi_select_bank_inapp_for_payoo = lixi_select_bank;
    }

    public int getOrder_payment_expired_at_store() {
        return order_payment_expired_at_store;
    }

    public void setOrder_payment_expired_at_store(int order_payment_expired_at_store) {
        this.order_payment_expired_at_store = order_payment_expired_at_store;
    }

    public String getPayment_cod_fee() {
        if (payment_cod_fee == null || payment_cod_fee.length() == 0)
            return "{\"min\" : 15000, \"max\": 25000}";
        return payment_cod_fee;
    }

    public void setPayment_cod_fee(String payment_cod_fee) {
        this.payment_cod_fee = payment_cod_fee;
    }

    public String getDefaultSupportPhone() {
        return defaultSupportPhone;
    }

    public void setDefaultSupportPhone(String defaultSupportPhone) {
        this.defaultSupportPhone = defaultSupportPhone;
    }

    public int getPayment_cod_enable() {
        return payment_cod_enable;
    }

    public void setPayment_cod_enable(int payment_cod_enable) {
        this.payment_cod_enable = payment_cod_enable;
    }

    public int getLixi_select_bank_inapp_for_payoo() {
        return lixi_select_bank_inapp_for_payoo;
    }

    public void setLixi_select_bank_inapp_for_payoo(int lixi_select_bank_inapp_for_payoo) {
        this.lixi_select_bank_inapp_for_payoo = lixi_select_bank_inapp_for_payoo;
    }

    public int getNapas_add_card_enable() {
        return napas_add_card_enable;
    }

    public void setNapas_add_card_enable(int napas_add_card_enable) {
        this.napas_add_card_enable = napas_add_card_enable;
    }

    public int getLocation_change_enable() {
        return location_change_enable;
    }

    public void setLocation_change_enable(int location_change_enable) {
        this.location_change_enable = location_change_enable;
    }

    public String getSocket_url() {
        return socket_url;
    }

    public void setSocket_url(String socket_url) {
        this.socket_url = socket_url;
    }

    public int getAndroidLixiUpdate() {
        return androidLixiUpdate;
    }

    public void setAndroidLixiUpdate(int androidLixiUpdate) {
        this.androidLixiUpdate = androidLixiUpdate;
    }

    public int getRemind_update_info_interval() {
        return remind_update_info_interval * 1000;
    }

    public void setRemind_update_info_interval(int remind_update_info_interval) {
        this.remind_update_info_interval = remind_update_info_interval;
    }

    public String getReference_code_guide_link() {
        return reference_code_guide_link;
    }

    public void setReference_code_guide_link(String reference_code_guide_link) {
        this.reference_code_guide_link = reference_code_guide_link;
    }

    public String getReference_code_share_content() {
        return reference_code_share_content;
    }

    public void setReference_code_share_content(String reference_code_share_content) {
        this.reference_code_share_content = reference_code_share_content;
    }

    public int getReference_code_profile_enable() {
        return reference_code_profile_enable;
    }

    public void setReference_code_profile_enable(int reference_code_profile_enable) {
        this.reference_code_profile_enable = reference_code_profile_enable;
    }

    public String getReference_code_lixi_text() {
        return reference_code_lixi_text;
    }

    public void setReference_code_lixi_text(String reference_code_lixi_text) {
        this.reference_code_lixi_text = reference_code_lixi_text;
    }

    public float getHome_banner_ratio() {
        return home_banner_ratio;
    }

    public void setHome_banner_ratio(float home_banner_ratio) {
        this.home_banner_ratio = home_banner_ratio;
    }

    public int getShop_services_enable() {
        return shop_services_enable;
    }

    public void setShop_services_enable(int shop_services_enable) {
        this.shop_services_enable = shop_services_enable;
    }

    public String getReward_tab_title() {
        return reward_tab_title;
    }

    public void setReward_tab_title(String reward_tab_title) {
        this.reward_tab_title = reward_tab_title;
    }

    public String getShop_tabbar_icon() {
        return shop_tabbar_icon;
    }

    public void setShop_tabbar_icon(String shop_tabbar_icon) {
        this.shop_tabbar_icon = shop_tabbar_icon;
    }

    public String getShop_tabbar_title() {
        return shop_tabbar_title;
    }

    public void setShop_tabbar_title(String shop_tabbar_title) {
        this.shop_tabbar_title = shop_tabbar_title;
    }

    public int getGame_home_button_enable() {
        return game_home_button_enable;
    }

    public void setGame_home_button_enable(int game_home_button_enable) {
        this.game_home_button_enable = game_home_button_enable;
    }

    public String getGame_home_button_icon() {
        return game_home_button_icon;
    }

    public void setGame_home_button_icon(String game_home_button_icon) {
        this.game_home_button_icon = game_home_button_icon;
    }

    public String getGame_main_url() {
        return game_main_url;
    }

    public void setGame_main_url(String game_main_url) {
        this.game_main_url = game_main_url;
    }

    public String getGame_api_url() {
        return game_api_url;
    }

    public void setGame_api_url(String game_api_url) {
        this.game_api_url = game_api_url;
    }

    public String getGame_share_url() {
        return game_share_url;
    }

    public void setGame_share_url(String game_share_url) {
        this.game_share_url = game_share_url;
    }

    public static void SaveConfig(LixiActivity activity, final ConfigModel model) {
        AppPreferenceHelper appPreferenceHelper = new AppPreferenceHelper(activity);
        appPreferenceHelper.setUserConfig(model);
        instance = model;
    }

    private static ConfigModel instance = null;

    public static ConfigModel getInstance(Context context) {
        if (instance == null) {
            instance = new AppPreferenceHelper(context).getUserConfig();
        }

        return instance;
    }
}
