package com.opencheck.client.models.lixishop;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class LixiShopBankTransactionMessage implements Serializable {
    private List<String> params;
    private String text;

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
