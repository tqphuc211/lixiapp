package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;
import com.opencheck.client.models.merchant.NearestStoreModel;
import com.opencheck.client.models.merchant.StoreModel;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class StoreApplyModel extends LixiModel {
    private String address;
    private String closing_time;
    private String opening_time;
    private String phone;
    private int id;
    private String name;
    private MerchantInfoModel merchant_info;
    private String store_code;
    private double lat;
    private double lng;
    private boolean allow_user_send_request_active;
    private boolean isSelected = false;

    public StoreModel convertToStoreModel() {
        StoreModel storeModel = new StoreModel();
        storeModel.setName(getName());
        storeModel.setLat(String.valueOf(getLat()));
        storeModel.setLng(String.valueOf(getLng()));
        storeModel.setAddress(getAddress());
        storeModel.setPhone(getPhone());
        return storeModel;
    }

    public NearestStoreModel convertToNearestStoreModel() {
        NearestStoreModel model = new NearestStoreModel();
        model.setName(getName());
        model.setMerchant_info(getMerchant_info());
        return model;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public boolean isAllow_user_send_request_active() {
        return allow_user_send_request_active;
    }

    public void setAllow_user_send_request_active(boolean allow_user_send_request_active) {
        this.allow_user_send_request_active = allow_user_send_request_active;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}