package com.opencheck.client.models;

import java.io.Serializable;

/**
 * Created by vutha_000 on 2/27/2018.
 */

public class NotificationModel implements Serializable {
    public static final String ADMIN_SURVEY = "admin.notification.survey";
    public static final String SURVEY = "survey";
    public static final String SURVEY_CODE = "survey.code";
    public static final String ORDER_CODE = "admin.code.order";

    public static final String POS_POINT_ADD_WAIT = "point.add.waitting";
    public static final String POS_POINT_ADD_DONE = "point.add.done";
    public static final String POS_POINT_ADD_CANCEL = "point.add.cancel";

    public static final String POS_POINT_ADD_WAIT_OLD = "order.add.waitting";
    public static final String POS_POINT_ADD_DONE_OLD = "order.add.done";

    //New code

    public static final String SALE_ORDER_CHANGE = "sale.order.change";
    public static final String SALE_ORDER_CANCEL = "sale.order.cancel";
    public static final String ORDER_ADD = "order.add";
    public static final String PAYMENT_ADD = "payment.add";
    public static final String PAYMENT_ADD_TOPUP = "payment.add.topup";
    public static final String PAYMENT_ADD_BUY_CARD = "payment.add.buycard";
    public static final String PROMOTION_ADD = "promotion.add";
    public static final String ADMIN_MERCHANT_ADD = "admin.merchant.add";
    public static final String ADMIN_PROMOTION_ADD = "admin.promotion.add";
    public static final String ADMIN_LINK_ADD = "admin.link.add";
    public static final String PROMOTION_EXPIRE = "promotion.expire";
    public static final String SHOP_BUY_SUCCESS = "shop.buy.success";
    public static final String SHOP_BUY_FAILED = "shop.buy.failed";
    public static final String ADMIN_ADD_PUSH = "admin.add.push";

    public static final String VOUCHER_DETAIL_ACTIVE = "voucher.detail.active";
    public static final String USER_ORDER_CONFIRMED = "user_order_confirmed";
    public static final String USER_ORDER_PICKED = "user_order_picked";
    public static final String USER_ORDER_CANCEL = "user_order_cancel";
    public static final String USER_PAYMENT_ORDER_COMPLETED = "payment_order_completed";
    public static final String USER_PAYMENT_ORDER_CANCELED = "payment_order_canceled";

    public static final String INTRO_CODE = "reference_code";

    //
    public static final String DELI_STORE_DETAIL = "deli_store_detail";
    public static final String DELI_ARTICLE_DETAIL = "deli_article_detail";
    public static final String DELI_PROMOTION_DETAIL = "deli_promotion_detail";
    public static final String DELI_TOPIC_DETAIL = "deli_topic_detail";
    public static final String DELI_USER_ORDER = "deli_user_order";

    public static final String WEB_GAME = "game_detail";

    public static final String HOME_LIXI_SHOP = "open_tabbar";

    //for voucher
    public static final String VOUCHER_DETAIL = "voucher.detail";

//    public static final String PROMOTION_EXPIRE = "promotion.expire";

    private String id = "";
    private String public_id = ""; // Dung cho ApiEntity.READ_NOTIFICATION (readerNotification)
    private String body = "";
    private String date_created = "";
    private String date_read = "";
    private String is_read = "";
    private String object_id = "";
    private String detail_id = "";// cho hóa đơn từ POS
    private String title = "";
    private String url = "";
    private String to_partner_id = ""; // hien tai khong dung
    private String click_action = "";
    private String image = "";
    private String type = "";
    private String total = "";
    private long date_created_timestamp = 0;
    private int typeItem;

    public int getTypeItem() {
        return typeItem;
    }

    public void setTypeItem(int typeItem) {
        this.typeItem = typeItem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_read() {
        return date_read;
    }

    public void setDate_read(String date_read) {
        this.date_read = date_read;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getObject_id() {
        return object_id;
    }

    public void setObject_id(String object_id) {
        this.object_id = object_id;
    }

    public String getDetail_id() {
        return detail_id;
    }

    public void setDetail_id(String detail_id) {
        this.detail_id = detail_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTo_partner_id() {
        return to_partner_id;
    }

    public void setTo_partner_id(String to_partner_id) {
        this.to_partner_id = to_partner_id;
    }

    public String getClick_action() {
        return click_action;
    }

    public void setClick_action(String click_action) {
        this.click_action = click_action;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public long getDate_created_timestamp() {
        return date_created_timestamp;
    }

    public void setDate_created_timestamp(long date_created_timestamp) {
        this.date_created_timestamp = date_created_timestamp;
    }
}
