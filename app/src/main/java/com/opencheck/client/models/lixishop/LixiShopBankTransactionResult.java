package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/15/2018.
 */

public class LixiShopBankTransactionResult extends LixiModel {
    private List<LixiShopBankTransactionMessage> message;
    private String order_id;
    private String order_uuid;
    private int product_id;
    private String product_type;
    private Boolean status;
    private String title;

    public List<LixiShopBankTransactionMessage> getMessage() {
        return message;
    }

    public void setMessage(List<LixiShopBankTransactionMessage> message) {
        this.message = message;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_uuid() {
        return order_uuid;
    }

    public void setOrder_uuid(String order_uuid) {
        this.order_uuid = order_uuid;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
