package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class PaymentPartnerModel extends LixiModel {
    private String id = "";

    private String avatar = "";
    private String website = "";
    private String suplier_code = "";
    private List<String> suplier_prefix_phone;
    private String address = "";
    private String birthday = "";
    private String email = "";
    private String fax = "";
    private String identification = "";
    private String is_installment = "";
    private String is_service_suplier = "";
    private String is_service_vendor = "";
    private String mobile = "";
    private String name = "";
    private String phone = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getIs_installment() {
        return is_installment;
    }

    public void setIs_installment(String is_installment) {
        this.is_installment = is_installment;
    }

    public String getIs_service_suplier() {
        return is_service_suplier;
    }

    public void setIs_service_suplier(String is_service_suplier) {
        this.is_service_suplier = is_service_suplier;
    }

    public String getIs_service_vendor() {
        return is_service_vendor;
    }

    public void setIs_service_vendor(String is_service_vendor) {
        this.is_service_vendor = is_service_vendor;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSuplier_code() {
        return suplier_code;
    }

    public void setSuplier_code(String suplier_code) {
        this.suplier_code = suplier_code;
    }

    public List<String> getSuplier_prefix_phone() {
        return suplier_prefix_phone;
    }

    public void setSuplier_prefix_phone(List<String> suplier_prefix_phone) {
        this.suplier_prefix_phone = suplier_prefix_phone;
    }
}
