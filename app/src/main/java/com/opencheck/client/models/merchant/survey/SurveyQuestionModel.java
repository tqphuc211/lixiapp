package com.opencheck.client.models.merchant.survey;

import com.opencheck.client.models.LixiModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class SurveyQuestionModel extends LixiModel {

    public static final String QUESTION_CHOICE = "choice";
    public static final String QUESTION_TEXT = "free";
    private List<SurveyAnswerModel> answers;
    private long id;
    private String question_name;
    private String question_type;
    private boolean show_icon;

    public List<SurveyAnswerModel> getAnswers() {
        return answers;
    }

    public void setAnswers(List<SurveyAnswerModel> answers) {
        this.answers = answers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public boolean isShow_icon() {
        return show_icon;
    }

    public void setShow_icon(boolean show_icon) {
        this.show_icon = show_icon;
    }
}
