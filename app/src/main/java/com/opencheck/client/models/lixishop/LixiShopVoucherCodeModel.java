package com.opencheck.client.models.lixishop;

import android.os.Parcel;
import android.os.Parcelable;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class LixiShopVoucherCodeModel extends LixiModel implements Parcelable {
    private String expired_time;
    private long id;
    private String pin;
    private String serial;
    private long product_id;
    private boolean send_pin;

    protected LixiShopVoucherCodeModel(Parcel in) {
        expired_time = in.readString();
        id = in.readLong();
        pin = in.readString();
        serial = in.readString();
        product_id = in.readLong();
    }

    public static final Creator<LixiShopVoucherCodeModel> CREATOR = new Creator<LixiShopVoucherCodeModel>() {
        @Override
        public LixiShopVoucherCodeModel createFromParcel(Parcel in) {
            return new LixiShopVoucherCodeModel(in);
        }

        @Override
        public LixiShopVoucherCodeModel[] newArray(int size) {
            return new LixiShopVoucherCodeModel[size];
        }
    };

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public String getExpired_time() {
        return expired_time;
    }

    public void setExpired_time(String expired_time) {
        this.expired_time = expired_time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(expired_time);
        dest.writeLong(id);
        dest.writeString(pin);
        dest.writeString(serial);
        dest.writeLong(product_id);
    }

    public boolean isSend_pin() {
        return send_pin;
    }

    public void setSend_pin(boolean send_pin) {
        this.send_pin = send_pin;
    }
}
