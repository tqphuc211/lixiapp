package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/5/2018.
 */


public class MerchantHotBrandModel extends LixiModel{
    private String id;
    private String logo;
    private String name;

    public MerchantHotBrandModel() {
    }

    public MerchantHotBrandModel(String id, String logo, String name) {
        this.id = id;
        this.logo = logo;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
