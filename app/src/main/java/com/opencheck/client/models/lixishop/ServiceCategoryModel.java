package com.opencheck.client.models.lixishop;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class ServiceCategoryModel extends LixiModel {

    public static final String TYPE_BUY_CARD = "buycard";
    public static final String TYPE_TOPUP = "topup";
    public static final String TYPE_BUY_HAS_CHILD = "has_child";
    public static final String TYPE_CONTRACT = "contract";
    public static final String TYPE_PHONE = "phone";
    public static final String TYPE_CUSTOMER_CODE = "customercode";
//    public static String TYPE_BUY_CARD = "TYPE_BUY_CARD";

    private String id = "";
    private String color = "";
    private String custom_field = "";
    private String description = "";
    private String icon = "";
    private String name = "";
    private String parent_id = "";
    private String sequence = "";
    private String supplier_ids = "";
    private String type = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCustom_field() {
        return custom_field;
    }

    public void setCustom_field(String custom_field) {
        this.custom_field = custom_field;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getSupplier_ids() {
        return supplier_ids;
    }

    public void setSupplier_ids(String supplier_ids) {
        this.supplier_ids = supplier_ids;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
