package com.opencheck.client.models.socket;

import java.io.Serializable;

public class SocketDeviceModel implements Serializable {
    private String device_id;
    private String device_model;
    private String os_version;
    private String app_version;
    private String device_token;
    private double lat;
    private double lng;
    private float battery_percent;
    private String network_connection;
    private String mac_address;
    private String screen_resolution;
    private String ip_address;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_model() {
        return device_model;
    }

    public void setDevice_model(String device_model) {
        this.device_model = device_model;
    }

    public String getOs_version() {
        return os_version;
    }

    public void setOs_version(String os_version) {
        this.os_version = os_version;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getBattery_percent() {
        return battery_percent;
    }

    public void setBattery_percent(float battery_percent) {
        this.battery_percent = battery_percent;
    }

    public String getNetwork_connection() {
        return network_connection;
    }

    public void setNetwork_connection(String network_connection) {
        this.network_connection = network_connection;
    }

    public String getMac_address() {
        return mac_address;
    }

    public void setMac_address(String mac_address) {
        this.mac_address = mac_address;
    }

    public String getScreen_resolution() {
        return screen_resolution;
    }

    public void setScreen_resolution(String screen_resolution) {
        this.screen_resolution = screen_resolution;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }
}
