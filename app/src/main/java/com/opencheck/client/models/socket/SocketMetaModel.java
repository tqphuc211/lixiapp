package com.opencheck.client.models.socket;

import java.io.Serializable;

public class SocketMetaModel implements Serializable {
    private String user_id;
    private SocketDeviceModel device;
    private String platform;
    private long timestamp;
    private SocketUtmModel utm;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public SocketDeviceModel getDevice() {
        return device;
    }

    public void setDevice(SocketDeviceModel device) {
        this.device = device;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public SocketUtmModel getUtm() {
        return utm;
    }

    public void setUtm(SocketUtmModel utm) {
        this.utm = utm;
    }
}
