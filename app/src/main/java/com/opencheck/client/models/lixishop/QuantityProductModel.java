package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class QuantityProductModel extends LixiModel{
    private int limit_cash;
    private int limit_gift;
    private int max_cash_quantity_allowed;
    private int max_lixi_quantity_allowed;

    public int getLimit_cash() {
        return limit_cash;
    }

    public void setLimit_cash(int limit_cash) {
        this.limit_cash = limit_cash;
    }

    public int getLimit_gift() {
        return limit_gift;
    }

    public void setLimit_gift(int limit_gift) {
        this.limit_gift = limit_gift;
    }

    public int getMax_cash_quantity_allowed() {
        return max_cash_quantity_allowed;
    }

    public void setMax_cash_quantity_allowed(int max_cash_quantity_allowed) {
        this.max_cash_quantity_allowed = max_cash_quantity_allowed;
    }

    public int getMax_lixi_quantity_allowed() {
        return max_lixi_quantity_allowed;
    }

    public void setMax_lixi_quantity_allowed(int max_lixi_quantity_allowed) {
        this.max_lixi_quantity_allowed = max_lixi_quantity_allowed;
    }
}
