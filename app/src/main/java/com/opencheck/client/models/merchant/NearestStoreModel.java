package com.opencheck.client.models.merchant;

import android.location.Location;

import com.opencheck.client.models.LixiModel;

public class NearestStoreModel extends LixiModel {
    private String address;
    private long id;
    private Location location;
    private double lat;
    private double lng;
    private MerchantInfoModel merchant_info;
    private String name;
    private String phone;
    private String store_code;
    private double user_distance;
    private boolean allow_user_send_request_active;

    public StoreModel convertToStoreModel(){
        StoreModel storeModel = new StoreModel();
        storeModel.setName(getName());
        storeModel.setLat(String.valueOf(lat));
        storeModel.setLng(String.valueOf(lng));
        storeModel.setAddress(getAddress());
        return storeModel;
    }

    public boolean isAllow_user_send_request_active() {
        return allow_user_send_request_active;
    }

    public void setAllow_user_send_request_active(boolean allow_user_send_request_active) {
        this.allow_user_send_request_active = allow_user_send_request_active;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public double getUser_distance() {
        return user_distance;
    }

    public void setUser_distance(double user_distance) {
        this.user_distance = user_distance;
    }
}
