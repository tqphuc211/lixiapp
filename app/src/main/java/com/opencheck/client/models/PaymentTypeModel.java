package com.opencheck.client.models;

public class PaymentTypeModel {
    public static final String PAY_BY_CASH = "cash";
    public static final String PAY_BY_LIXI = "lixi";

    private String name;
    private String payment_type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }
}
