package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class CategoryVendorModel extends LixiModel {
    private String id = "";
    private String image = "";
    private String name = "";
    private String color = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
