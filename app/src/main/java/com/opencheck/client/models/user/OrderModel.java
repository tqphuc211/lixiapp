package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.MerchantModel;
import com.opencheck.client.models.merchant.PromotionModel;
import com.opencheck.client.models.merchant.StoreModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderModel extends LixiModel {


//    state : Nhóm tạo thành đơn hàng : [draft, sent] Nhóm đã tạo thành đơn hàng : ['sale','done']

    public static String STATE_DRAFT = "draft";
    public static String STATE_SENT = "sent";
    public static String STATE_SALE = "sale";
    public static String STATE_DONE = "done";


    public static String typeOpencheck = "Ưu đãi từ OPENCHECK";
    public static String typePending = "Giao dịch chờ xác thực";

    private String typeCash = "Ưu đãi từ OPENCHECK";

    ///////////////////////////////////////////////////////
    public static final int ITEM = 0;
    public static final int SECTION = 1;
    public int type;

    private String idDb = "-1";
    private String timeStamp = "0";

    private String idOrder = "0";
    private String can_edit = "";
    private String coupon_code = "";
    private String date = "";
    private String order_code = "";
    private String point = "";
    private String price = "";
    private String state = "";
    private String time = "";
    private String verification_code = "";

    private String warehouse_id = "";
    private String write_date = "";

    private String bill_price = "";
    private String real_point = "";
    private String cost_point = "";

    private MerchantModel company = new MerchantModel();
    private PromotionModel promotion = new PromotionModel();
    private StoreModel store = new StoreModel();
    private UserModel creator = new UserModel();
    private UserModel customer = new UserModel();

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(String verification_code) {
        this.verification_code = verification_code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCan_edit() {
        return can_edit;
    }

    public void setCan_edit(String can_edit) {
        this.can_edit = can_edit;
    }

    public MerchantModel getCompany() {
        return company;
    }

    public void setCompany(MerchantModel company) {
        this.company = company;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public UserModel getCreator() {
        return creator;
    }

    public void setCreator(UserModel creator) {
        this.creator = creator;
    }

    public UserModel getCustomer() {
        return customer;
    }

    public void setCustomer(UserModel customer) {
        this.customer = customer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrder_code() {
        return order_code;
    }

    public void setOrder_code(String order_code) {
        this.order_code = order_code;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public PromotionModel getPromotion() {
        return promotion;
    }

    public void setPromotion(PromotionModel promotion) {
        this.promotion = promotion;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public StoreModel getStore() {
        return store;
    }

    public void setStore(StoreModel store) {
        this.store = store;
    }

    public String getTimeStamp() {
        timeStamp = String.valueOf(Helper.getTimeStampFromString(date + " " + time, ConstantValue.FORMAT_TIME_ORDER));
        timeStamp = String.valueOf(Long.parseLong(timeStamp) / 1000L);
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public static int getITEM() {
        return ITEM;
    }

    public static int getSECTION() {
        return SECTION;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public void setIdDb(String idDb) {
        this.idDb = idDb;
    }

    public String getIdDb() {
        return idDb;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getWrite_date() {
        return  String.valueOf(Helper.getTimeStampFromString(write_date, ConstantValue.FORMAT_TIME_ORDER) / 1000);
    }

    public void setWrite_date(String write_date) {
        this.write_date = write_date;
    }

    public void setTypeCash(String typeCash) {
        this.typeCash = typeCash;
    }

    public String getTypeCash() {
        return typeCash;
    }

    public String getCost_point() {
        if (cost_point.equalsIgnoreCase("")) {
            return "0";
        }
        return cost_point;
    }

    public void setCost_point(String cost_price) {
        this.cost_point = cost_price;
    }

    public String getBill_price() {
        if (bill_price.equalsIgnoreCase("")) {
            return "0";
        }
        return bill_price;
    }

    public void setBill_price(String bill_price) {
        this.bill_price = bill_price;
    }

    public String getReal_point() {
        if (real_point.equalsIgnoreCase("")) {
            return "0";
        }
        return real_point;
    }

    public void setReal_point(String real_price) {
        this.real_point = real_price;
    }
}
