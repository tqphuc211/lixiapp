package com.opencheck.client.models.game;

public class GameModel {
    public GameMetaModel meta;

    public GameDataModel data;

    public GameErrorModel error;

    public GameMetaModel getMeta() {
        return meta;
    }

    public void setMeta(GameMetaModel meta) {
        this.meta = meta;
    }

    public GameDataModel getData() {
        return data;
    }

    public void setData(GameDataModel data) {
        this.data = data;
    }

    public GameErrorModel getError() {
        return error;
    }

    public void setError(GameErrorModel error) {
        this.error = error;
    }
}
