package com.opencheck.client.models.socket;

import java.io.Serializable;

public class ParamsModel implements Serializable {
    private long id;
    private String message;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
