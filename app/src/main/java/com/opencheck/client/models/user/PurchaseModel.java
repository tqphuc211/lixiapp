package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.CategoryVendorModel;
import com.opencheck.client.models.merchant.VendorModel;
import com.opencheck.client.utils.ConstantValue;
import com.opencheck.client.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class PurchaseModel extends LixiModel {

    public static String typeOpencheck = "Ưu đãi từ OPENCHECK";
    public static String typePending = "Giao dịch chờ xác thực";

    private String typeCash = "Ưu đãi từ OPENCHECK";

    ///////////////////////////////////////////////////////
    public static final int ITEM = 0;
    public static final int SECTION = 1;
    public int type;

    private String id = "";
    private String can_edit = "";
    private String coupon_code = "";
    private UserModel creator = new UserModel();
    private String date = "";
    private String merchant_id = "";
    private String point = "";
    private String price = "";
    private VendorModel service_vendor = new VendorModel();
    private VendorModel service_suplier = new VendorModel();
    private CategoryVendorModel service = new CategoryVendorModel();
    private String state = "";
    private String store_id = "";
    private String time = "";
    private UserModel vendor = new UserModel();


    private String timeStamp = "0";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCan_edit() {
        return can_edit;
    }

    public void setCan_edit(String can_edit) {
        this.can_edit = can_edit;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public UserModel getCreator() {
        return creator;
    }

    public void setCreator(UserModel creator) {
        this.creator = creator;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public VendorModel getService_vendor() {
        return service_vendor;
    }

    public void setService_vendor(VendorModel service_vendor) {
        this.service_vendor = service_vendor;
    }

    public CategoryVendorModel getService() {
        return service;
    }

    public void setService(CategoryVendorModel service) {
        this.service = service;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserModel getVendor() {
        return vendor;
    }

    public void setVendor(UserModel vendor) {
        this.vendor = vendor;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        timeStamp = String.valueOf(Helper.getTimeStampFromString(date + " " + time, ConstantValue.FORMAT_TIME_ORDER_OLD));
        timeStamp = String.valueOf(Long.parseLong(timeStamp));// / 1000L);
        return timeStamp;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public VendorModel getService_suplier() {
        return service_suplier;
    }

    public void setService_suplier(VendorModel service_suplier) {
        this.service_suplier = service_suplier;
    }
}
