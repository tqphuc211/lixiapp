package com.opencheck.client.models.user;

import android.content.Context;

import com.opencheck.client.R;
import com.opencheck.client.utils.LanguageBinding;

/**
 * Created by vutha_000 on 3/2/2018.
 */

public class CityDistrictModel {
    private String id = "0";
    private String lat = "";
    private String lng = "";
    private String name = "";
    private String parent_id = "";

    public static CityDistrictModel getCityDefault(Context context) {
        CityDistrictModel cityDistrictModel = new CityDistrictModel();
        cityDistrictModel.setId("-1");
        cityDistrictModel.setName(LanguageBinding.getString(R.string.nationwide, context));
        cityDistrictModel.setParent_id("-2");
        return cityDistrictModel;
    }

    public static CityDistrictModel getDistrictDefault(Context context) {
        CityDistrictModel cityDistrictModel = new CityDistrictModel();
        cityDistrictModel.setId("-1");
        cityDistrictModel.setName(LanguageBinding.getString(R.string.all, context));
        cityDistrictModel.setParent_id("-2");
        return cityDistrictModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
