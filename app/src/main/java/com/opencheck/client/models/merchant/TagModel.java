package com.opencheck.client.models.merchant;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class TagModel implements Serializable {
    private String id;
    private String name;
    private String image_link;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }
}

