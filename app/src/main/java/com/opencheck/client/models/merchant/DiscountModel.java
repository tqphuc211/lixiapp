package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import java.io.Serializable;
import java.util.ArrayList;

public class DiscountModel extends LixiModel {
    private long discount;
    private ArrayList<InfoDiscountModel> info;

    public long getDiscount() {
        return discount;
    }

    public void setDiscount(long discount) {
        this.discount = discount;
    }

    public ArrayList<InfoDiscountModel> getInfo() {
        return info;
    }

    public void setInfo(ArrayList<InfoDiscountModel> info) {
        this.info = info;
    }
}
