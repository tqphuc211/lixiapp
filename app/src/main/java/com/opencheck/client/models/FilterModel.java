package com.opencheck.client.models;

import com.opencheck.client.models.user.CategoryModel;
import com.opencheck.client.models.user.CityDistrictModel;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class FilterModel implements Serializable {

    private String keyWord = "";
    private CategoryModel categoryModel;
    private CategoryModel categoryModelSelected;
    private CityDistrictModel cityModel;
    private CityDistrictModel districtModel;
    private CityDistrictModel cityModelSelected;
    private CityDistrictModel districtModelSelected;
    private CategoryModel sortModel;
    private String lat = "";
    private String lng = "";

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public CityDistrictModel getCityModel() {
        return cityModel;
    }

    public void setCityModel(CityDistrictModel cityModel) {
        this.cityModel = cityModel;
    }

    public CityDistrictModel getDistrictModel() {
        return districtModel;
    }

    public void setDistrictModel(CityDistrictModel districtModel) {
        this.districtModel = districtModel;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setSortModel(CategoryModel sortModel) {
        this.sortModel = sortModel;
    }

    public CategoryModel getSortModel() {
        return sortModel;
    }

    public void setCategoryModelSelected(CategoryModel categoryModelSelected) {
        this.categoryModelSelected = categoryModelSelected;
    }

    public CategoryModel getCategoryModelSelected() {
        return categoryModelSelected;
    }

    public CityDistrictModel getCityModelSelected() {
        return cityModelSelected;
    }

    public void setCityModelSelected(CityDistrictModel cityModelSelected) {
        this.cityModelSelected = cityModelSelected;
    }

    public CityDistrictModel getDistrictModelSelected() {
        return districtModelSelected;
    }

    public void setDistrictModelSelected(CityDistrictModel districtModelSelected) {
        this.districtModelSelected = districtModelSelected;
    }

}