package com.opencheck.client.models.lixishop;

import com.opencheck.client.utils.ConstantValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class ComboItemModel implements Serializable {
    private long id;
    private String name;
    private int payment_discount_price;
    private int price;
    private int discount_price;
    private ArrayList<String> product_image;
    private ArrayList<String> product_image_medium;
    private int quantity;

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment_discount_price() {
        return payment_discount_price;
    }

    public void setPayment_discount_price(int payment_discount_price) {
        this.payment_discount_price = payment_discount_price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return (ArrayList<String>) Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(ArrayList<String> product_image) {
        this.product_image = product_image;
    }

    public ArrayList<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return (ArrayList<String>) Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(ArrayList<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
