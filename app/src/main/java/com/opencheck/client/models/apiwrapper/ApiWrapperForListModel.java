package com.opencheck.client.models.apiwrapper;

import com.opencheck.client.models.LixiModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class ApiWrapperForListModel<T> extends LixiModel {
    private MetaDataModel _meta;
    private ArrayList<T> records;

    public MetaDataModel get_meta() {
        return _meta;
    }

    public void set_meta(MetaDataModel _meta) {
        this._meta = _meta;
    }

    public ArrayList<T> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<T> records) {
        this.records = records;
    }
}
