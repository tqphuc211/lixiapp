package com.opencheck.client.models.lixishop;

/**
 * Created by I'm Sugar on 5/21/2018.
 */

public class SingleMethodPaymentModel  {

    private PaymentMethodModel payment_method = new PaymentMethodModel();
    private BankModel bankModel = new BankModel();


    public PaymentMethodModel getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(PaymentMethodModel payment_method) {
        this.payment_method = payment_method;
    }

    public BankModel getBankModel() {
        return bankModel;
    }

    public void setBankModel(BankModel bankModel) {
        this.bankModel = bankModel;
    }
}
