package com.opencheck.client.models.apierror;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/1/2018.
 */

public class ExceptionModel implements Serializable {
    private String name;
    private String type;
    private ArgErrorModel arg;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArgErrorModel getArg() {
        return arg;
    }

    public void setArg(ArgErrorModel arg) {
        this.arg = arg;
    }
}
