package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/14/2018.
 */

public class LixiShopTransactionResultModel extends LixiModel {
    private int count_attend;
    private int discount;
    private int discount_price;
    private List<LixiShopBankTransactionMessage> message;
    private List<LixiShopVoucherCodeModel> evouchers;
    private long id;
    private int price;
    private int product;
    private String product_type;
    private long quantity;
    private String shipping_info;
    private String state;
    private String transaction_id;
    private long user;

    public List<LixiShopBankTransactionMessage> getMessage() {
        return message;
    }

    public void setMessage(List<LixiShopBankTransactionMessage> message) {
        this.message = message;
    }

    public int getCount_attend() {
        return count_attend;
    }

    public void setCount_attend(int count_attend) {
        this.count_attend = count_attend;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public List<LixiShopVoucherCodeModel> getEvouchers() {
        if (evouchers == null) {
            return new ArrayList<LixiShopVoucherCodeModel>();
        }
        return evouchers;
    }

    public void setEvouchers(List<LixiShopVoucherCodeModel> evouchers) {
        this.evouchers = evouchers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getShipping_info() {
        return shipping_info;
    }

    public void setShipping_info(String shipping_info) {
        this.shipping_info = shipping_info;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }
}
