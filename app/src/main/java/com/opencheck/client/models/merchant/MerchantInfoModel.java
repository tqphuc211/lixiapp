package com.opencheck.client.models.merchant;

import com.opencheck.client.home.account.new_layout.model.MerchantApplyUnion;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/12/2018.
 */

public class MerchantInfoModel implements Serializable {
    private String close_time;
    private String opening_time;
    private String name;
    private String logo;
    private int max_price;
    private int min_price;
    private int id;

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getMax_price() {
        return max_price;
    }

    public void setMax_price(int max_price) {
        this.max_price = max_price;
    }

    public int getMin_price() {
        return min_price;
    }

    public void setMin_price(int min_price) {
        this.min_price = min_price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
