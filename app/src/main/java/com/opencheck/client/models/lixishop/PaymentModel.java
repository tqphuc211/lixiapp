package com.opencheck.client.models.lixishop;

import com.opencheck.client.LixiFragment;
import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.PaymentPartnerModel;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class PaymentModel extends LixiModel {
    private String id = "";

    private String name = "";
    private ServiceCategoryModel serviceCategoryModel = new ServiceCategoryModel();
    private PaymentPartnerModel paymentPartnerModel = new PaymentPartnerModel();
    private String contract_code = "";
    private String fullname = "";
    private String phone = "";
    private String money_payment = "";
    private String quantity = "";
    private String userName = "";

    private String typeTopup = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceCategoryModel getServiceCategoryModel() {
        return serviceCategoryModel;
    }

    public void setServiceCategoryModel(ServiceCategoryModel serviceCategoryModel) {
        this.serviceCategoryModel = serviceCategoryModel;
    }

    public PaymentPartnerModel getPaymentPartnerModel() {
        return paymentPartnerModel;
    }

    public void setPaymentPartnerModel(PaymentPartnerModel paymentPartnerModel) {
        this.paymentPartnerModel = paymentPartnerModel;
    }

    public String getContract_code() {
        return contract_code;
    }

    public void setContract_code(String contract_code) {
        this.contract_code = contract_code;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMoney_payment() {
        return money_payment;
    }

    public void setMoney_payment(String money_payment) {
        this.money_payment = money_payment;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTypeTopup() {
        return typeTopup;
    }

    public void setTypeTopup(String typeTopup) {
        this.typeTopup = typeTopup;
    }
}
