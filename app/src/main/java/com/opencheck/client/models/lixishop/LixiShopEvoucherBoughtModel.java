package com.opencheck.client.models.lixishop;

import android.os.Parcel;
import android.os.Parcelable;

import com.opencheck.client.home.account.new_layout.model.MerchantApplyUnion;
import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;
import com.opencheck.client.utils.ConstantValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vutha_000 on 3/21/2018.
 */

public class LixiShopEvoucherBoughtModel extends LixiModel implements Parcelable {
    private long evoucher_order;
    private String expired_time;
    private long id;
    private Boolean is_generated;
    private Boolean send_pin;
    private String pin;
    private int product;
    private List<String> product_image = null;
    private List<String> product_image_medium = null;
    private String product_cover_image;
    private String product_logo;
    private String product_name;
    private String serial;
    private String state;
    private int user;
    private boolean isSelected = false;
    private long price;
    private int total;
    private int evoucher_selled_count;
    private long activate_date;
    private MerchantInfoModel merchant_info;
    private ArrayList<MerchantApplyUnion> merchant_apply_union;

    protected LixiShopEvoucherBoughtModel(Parcel in) {
        evoucher_order = in.readLong();
        expired_time = in.readString();
        id = in.readLong();
        byte tmpIs_generated = in.readByte();
        is_generated = tmpIs_generated == 0 ? null : tmpIs_generated == 1;
        byte tmpSend_pin = in.readByte();
        send_pin = tmpSend_pin == 0 ? null : tmpSend_pin == 1;
        pin = in.readString();
        product = in.readInt();
        product_image = in.createStringArrayList();
        product_image_medium = in.createStringArrayList();
        product_cover_image = in.readString();
        product_logo = in.readString();
        product_name = in.readString();
        serial = in.readString();
        state = in.readString();
        user = in.readInt();
        isSelected = in.readByte() != 0;
        price = in.readLong();
        total = in.readInt();
        evoucher_selled_count = in.readInt();
        activate_date = in.readLong();
    }

    public static final Creator<LixiShopEvoucherBoughtModel> CREATOR = new Creator<LixiShopEvoucherBoughtModel>() {
        @Override
        public LixiShopEvoucherBoughtModel createFromParcel(Parcel in) {
            return new LixiShopEvoucherBoughtModel(in);
        }

        @Override
        public LixiShopEvoucherBoughtModel[] newArray(int size) {
            return new LixiShopEvoucherBoughtModel[size];
        }
    };

    public long getActivate_date() {
        return activate_date;
    }

    public void setActivate_date(long activate_date) {
        this.activate_date = activate_date;
    }

    public int getEvoucher_selled_count() {
        return evoucher_selled_count;
    }

    public void setEvoucher_selled_count(int evoucher_selled_count) {
        this.evoucher_selled_count = evoucher_selled_count;
    }

    public String getProduct_cover_image() {
        return product_cover_image;
    }

    public void setProduct_cover_image(String product_cover_image) {
        this.product_cover_image = product_cover_image;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public long getEvoucher_order() {
        return evoucher_order;
    }

    public void setEvoucher_order(long evoucher_order) {
        this.evoucher_order = evoucher_order;
    }

    public String getExpired_time() {
        return expired_time;
    }

    public void setExpired_time(String expired_time) {
        this.expired_time = expired_time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getIs_generated() {
        return is_generated;
    }

    public void setIs_generated(Boolean is_generated) {
        this.is_generated = is_generated;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public List<String> getProduct_image() {
        if (product_image == null || product_image.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image;
    }

    public void setProduct_image(List<String> product_image) {
        this.product_image = product_image;
    }

    public List<String> getProduct_image_medium() {
        if (product_image_medium == null || product_image_medium.size() == 0){
            return Arrays.asList(ConstantValue.DEFAULT_IMAGE);
        }
        return product_image_medium;
    }

    public void setProduct_image_medium(List<String> product_image_medium) {
        this.product_image_medium = product_image_medium;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public Boolean getSend_pin() {
        return send_pin;
    }

    public void setSend_pin(Boolean send_pin) {
        this.send_pin = send_pin;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public ArrayList<MerchantApplyUnion> getMerchant_apply_union() {
        return merchant_apply_union;
    }

    public void setMerchant_apply_union(ArrayList<MerchantApplyUnion> merchant_apply_union) {
        this.merchant_apply_union = merchant_apply_union;
    }

    @Override
    public boolean equals(Object obj) {
        LixiShopEvoucherBoughtModel model = (LixiShopEvoucherBoughtModel) obj;
        return serial.equals(model.getSerial());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(evoucher_order);
        parcel.writeString(expired_time);
        parcel.writeLong(id);
        parcel.writeByte((byte) (is_generated == null ? 0 : is_generated ? 1 : 2));
        parcel.writeByte((byte) (send_pin == null ? 0 : send_pin ? 1 : 2));
        parcel.writeString(pin);
        parcel.writeInt(product);
        parcel.writeStringList(product_image);
        parcel.writeStringList(product_image_medium);
        parcel.writeString(product_cover_image);
        parcel.writeString(product_logo);
        parcel.writeString(product_name);
        parcel.writeString(serial);
        parcel.writeString(state);
        parcel.writeInt(user);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
        parcel.writeLong(price);
        parcel.writeInt(total);
        parcel.writeInt(evoucher_selled_count);
        parcel.writeLong(activate_date);
    }
}
