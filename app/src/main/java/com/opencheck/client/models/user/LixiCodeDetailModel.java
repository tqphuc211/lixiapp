package com.opencheck.client.models.user;

import com.opencheck.client.models.LixiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vutha_000 on 3/9/2018.
 */

public class LixiCodeDetailModel extends LixiModel {
    private String code;
    private String desc;
    private List<String> desc_data;
    private ArrayList<LixiCodeBarnerModel> list_banner;
    private String end_date;
    private String end_time;
    private long id;
    private long merchant_id;
    private String merchant_logo;
    private String merchant_name;
    private String start_date;
    private String start_time;
    private String text_detail;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getDesc_data() {
        return desc_data;
    }

    public void setDesc_data(List<String> desc_data) {
        this.desc_data = desc_data;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getText_detail() {
        return text_detail;
    }

    public void setText_detail(String text_detail) {
        this.text_detail = text_detail;
    }

    public ArrayList<LixiCodeBarnerModel> getList_banner() {
        return list_banner;
    }

    public void setList_banner(ArrayList<LixiCodeBarnerModel> list_banner) {
        this.list_banner = list_banner;
    }
}
