package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/26/2018.
 */

public class CompletedVoucherModel extends LixiModel {
    private boolean is_voucher_complete;
    private long merchant_id;
    private String merchant_logo;
    private String merchant_name;
    private long product_id;
    private String product_name;
    private long store_id;
    private String store_name;
    private long voucher_id;

    public boolean isIs_voucher_complete() {
        return is_voucher_complete;
    }

    public void setIs_voucher_complete(boolean is_voucher_complete) {
        this.is_voucher_complete = is_voucher_complete;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_logo() {
        return merchant_logo;
    }

    public void setMerchant_logo(String merchant_logo) {
        this.merchant_logo = merchant_logo;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public long getStore_id() {
        return store_id;
    }

    public void setStore_id(long store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public long getVoucher_id() {
        return voucher_id;
    }

    public void setVoucher_id(long voucher_id) {
        this.voucher_id = voucher_id;
    }
}
