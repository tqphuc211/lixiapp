package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class CardListModel extends LixiModel {
    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
