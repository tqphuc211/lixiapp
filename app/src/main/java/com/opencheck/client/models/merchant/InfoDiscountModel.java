package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

import java.io.Serializable;

public class InfoDiscountModel extends LixiModel {
    private boolean done;
    private String text;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
