package com.opencheck.client.models.lixishop;

import android.os.Parcel;
import android.os.Parcelable;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.CardListModel;
import com.opencheck.client.models.merchant.OrderDetailLogModel;
import com.opencheck.client.models.merchant.OrderDetailStoreModel;
import com.opencheck.client.models.merchant.OrderInfoModel;
import com.opencheck.client.models.user.OrderDetailUserModel;

import java.util.List;

/**
 * Created by vutha_000 on 3/19/2018.
 */

public class OrderDetailModel extends LixiModel implements Parcelable {

    private OrderDetailUserModel user;
    private OrderDetailStoreModel suplier;
    private String order_id;

    private int bill_price;
    private String date;
    private String id;
    private String image;
    private List<CardListModel> card_list;
    private List<OrderDetailLogModel> log;
    private List<OrderInfoModel> info;
    private String name;
    private String order_code;
    private int percent;
    private int point;
    private String state;
    private String state_text;
    private String type;
    private OrderDetailStoreModel store;

    private long product_id;
    private String shipping_info;
    private List<LixiShopVoucherCodeModel> code_list;

    //new params
    private String product_type; // evoucher - classic
    private String payment_method; // cc - bank - at_store - lixi (diem, shop)
    private String payment_type; //cash - lixi (diem - shop)

    protected OrderDetailModel(Parcel in) {
        order_id = in.readString();
        bill_price = in.readInt();
        date = in.readString();
        id = in.readString();
        image = in.readString();
        name = in.readString();
        order_code = in.readString();
        percent = in.readInt();
        point = in.readInt();
        state = in.readString();
        state_text = in.readString();
        type = in.readString();
        product_id = in.readLong();
        shipping_info = in.readString();
        product_type = in.readString();
        payment_method = in.readString();
        payment_type = in.readString();
        price = in.readInt();
    }

    public static final Creator<OrderDetailModel> CREATOR = new Creator<OrderDetailModel>() {
        @Override
        public OrderDetailModel createFromParcel(Parcel in) {
            return new OrderDetailModel(in);
        }

        @Override
        public OrderDetailModel[] newArray(int size) {
            return new OrderDetailModel[size];
        }
    };

    public List<CardListModel> getCardList() {
        return card_list;
    }

    public void setCardList(List<CardListModel> cardList) {
        this.card_list = cardList;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public int getBill_price() {
        return bill_price;
    }

    public void setBill_price(int bill_price) {
        this.bill_price = bill_price;
    }

    public List<OrderDetailLogModel> getLog() {
        return log;
    }

    public void setLog(List<OrderDetailLogModel> log) {
        this.log = log;
    }

    public String getOrder_code() {
        return order_code;
    }

    public void setOrder_code(String order_code) {
        this.order_code = order_code;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public OrderDetailStoreModel getStore() {
        return store;
    }

    public void setStore(OrderDetailStoreModel store) {
        this.store = store;
    }

    private int price;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrderDetailUserModel getUser() {
        return user;
    }

    public void setUser(OrderDetailUserModel user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getState() {
        return state;
    }

    public String getStateVN() {
//        if (state.equals("wait"))
//            return "Chờ xác thực";
//        else if (state.equals("done"))
//            return "Thành công";
//        else if (state.equals("cancel"))
//            return "Hủy";
//        return state;

        return state_text;
    }

    public int getStateColor() {
        if (state.equals("wait"))
            return 0xFFEB8509;
        else if (state.equals("done"))
            return 0xFF64B408;
        else if (state.equals("cancel"))
            return 0xffac1ea1;
        else if (state.equals("payment_expired"))
            return 0xffac1ea1;
        return 0xFF000000;
    }

    public void setState(String state) {
        this.state = state;
    }

    public OrderDetailStoreModel getSuplier() {
        return suplier;
    }

    public void setSuplier(OrderDetailStoreModel suplier) {
        this.suplier = suplier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<OrderInfoModel> getInfo() {
        return info;
    }

    public void setInfo(List<OrderInfoModel> info) {
        this.info = info;
    }

    public String getState_text() {
        return state_text;
    }

    public void setState_text(String state_text) {
        this.state_text = state_text;
    }

    public String getShipping_info() {
        return shipping_info;
    }

    public void setShipping_info(String shipping_info) {
        this.shipping_info = shipping_info;
    }

    public List<LixiShopVoucherCodeModel> getCode_list() {
        return code_list;
    }

    public void setCode_list(List<LixiShopVoucherCodeModel> code_list) {
        this.code_list = code_list;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(order_id);
        dest.writeInt(bill_price);
        dest.writeString(date);
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(order_code);
        dest.writeInt(percent);
        dest.writeInt(point);
        dest.writeString(state);
        dest.writeString(state_text);
        dest.writeString(type);
        dest.writeLong(product_id);
        dest.writeString(shipping_info);
        dest.writeString(product_type);
        dest.writeString(payment_method);
        dest.writeString(payment_type);
        dest.writeInt(price);
    }
}
