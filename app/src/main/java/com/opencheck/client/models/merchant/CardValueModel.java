package com.opencheck.client.models.merchant;

import com.opencheck.client.models.LixiModel;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class CardValueModel extends LixiModel {
    private String id = "";
    private String key = "";
    private String value = "";


    public void setData(String _key, String _value) {
        this.key = _key;
        this.value = _value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
