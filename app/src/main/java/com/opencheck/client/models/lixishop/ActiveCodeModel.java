package com.opencheck.client.models.lixishop;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.merchant.MerchantInfoModel;
import com.opencheck.client.models.merchant.StoreInfoModel;

/**
 * Created by vutha_000 on 3/23/2018.
 */

public class ActiveCodeModel extends LixiModel {
    private long active_ttl;
    private long evoucher_id;
    private int id;
    private int merchant_id;
    private MerchantInfoModel merchant_info;
    private int product_id;
    private ProductInfoModel product_info;
    private long user_id;
    private String state;
    private StoreInfoModel store_info;
    private String phone;
    private String name;
    private String store_code;

    public ActiveCodeModel() {
    }

    public ActiveCodeModel(int evoucher_id, int id, int merchant_id, int product_id, long user_id, String state) {
        this.evoucher_id = evoucher_id;
        this.id = id;
        this.merchant_id = merchant_id;
        this.product_id = product_id;
        this.user_id = user_id;
        this.state = state;
    }

    public long getActive_ttl() {
        return active_ttl;
    }

    public void setActive_ttl(long active_ttl) {
        this.active_ttl = active_ttl;
    }

    public MerchantInfoModel getMerchant_info() {
        return merchant_info;
    }

    public void setMerchant_info(MerchantInfoModel merchant_info) {
        this.merchant_info = merchant_info;
    }

    public ProductInfoModel getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductInfoModel product_info) {
        this.product_info = product_info;
    }

    public StoreInfoModel getStore_info() {
        return store_info;
    }

    public void setStore_info(StoreInfoModel store_info) {
        this.store_info = store_info;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public long getEvoucher_id() {
        return evoucher_id;
    }

    public void setEvoucher_id(long evoucher_id) {
        this.evoucher_id = evoucher_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
