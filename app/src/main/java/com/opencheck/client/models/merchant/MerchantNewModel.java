package com.opencheck.client.models.merchant;

import android.view.View;

import com.opencheck.client.models.LixiModel;
import com.opencheck.client.models.user.CategoryModel;

import java.util.ArrayList;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class MerchantNewModel extends LixiModel {

    private String id;
    private String address;
    private CategoryModel categoryModel;
    private String close_time;
    private String email;
    private String info_condition;
    private String info_delivery;
    private String info_merchant;
    private String info_promotion;
    private String is_delivery;
    private String lat;
    private String lng;
    private String logo;
    private String name;
    private String opening_time;
    private String phone;
    private String point_percent;
    private String website;
    private String ratio_receive_point;
    private String denominator_point;
    private String avg_bill;
    private String avg_price_bill;
    private String city_id;
    private String count_attend;
    private String count_current_point;
    private String count_favorite;
    private String count_order_point;
    private String count_point;
    private String count_point_transaction;
    private String count_promotion;
    private String count_purchase_point;
    private String discount_rate;
    private String district_id;
    private String is_favorite = "false";
    private String percent_use_point;
    private String ward_id;
    private String count_store;
    private String tranfer_date;
    private String url;
    private int min_price;
    private int max_price;
    private String contract_end_date;

    private String promotion_type;
    private String fixed_value;

    private ArrayList<TagModel> listTag = new ArrayList<TagModel>();

    private ArrayList<ProductImageModel> listImage = new ArrayList<ProductImageModel>();

    private ArrayList<PromotionModel> listPromotion = new ArrayList<PromotionModel>();
    private ArrayList<StoreModel> listStore = new ArrayList<StoreModel>();

    private View viewFavorite = null;

    public String getPromotion_type() {
        return promotion_type;
    }

    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }

    public String getFixed_value() {
        return fixed_value;
    }

    public void setFixed_value(String fixed_value) {
        this.fixed_value = fixed_value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public CategoryModel getCategoryModel() {
        if (categoryModel == null)
            categoryModel = new CategoryModel();
        return categoryModel;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfo_condition() {
        return info_condition;
    }

    public void setInfo_condition(String info_condition) {
        this.info_condition = info_condition;
    }

    public String getInfo_delivery() {
        return info_delivery;
    }

    public void setInfo_delivery(String info_delivery) {
        this.info_delivery = info_delivery;
    }

    public String getInfo_merchant() {
        return info_merchant;
    }

    public void setInfo_merchant(String info_merchant) {
        this.info_merchant = info_merchant;
    }

    public String getInfo_promotion() {
        return info_promotion;
    }

    public void setInfo_promotion(String info_promotion) {
        this.info_promotion = info_promotion;
    }

    public String getIs_delivery() {
        return is_delivery;
    }

    public void setIs_delivery(String is_delivery) {
        this.is_delivery = is_delivery;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPoint_percent(String point_percent) {
        this.point_percent = point_percent;
    }

    public String getPoint_percent() {

        if (point_percent == null || point_percent.equalsIgnoreCase("")) {
            return "0";
        }
        return point_percent;
    }

    public ArrayList<TagModel> getListTag() {
        return listTag;
    }

    public void setListTag(ArrayList<TagModel> listTag) {
        this.listTag = listTag;
    }

    public void setListImage(ArrayList<ProductImageModel> listImage) {
        this.listImage = listImage;
    }

    public ArrayList<ProductImageModel> getListImage() {
        return listImage;
    }

    public String getRatio_recive_point() {
        if (ratio_receive_point.equalsIgnoreCase("")) {
            return "0";
        }
        return ratio_receive_point;
    }

    public void setRatio_recive_point(String ratio_receive_point) {
        this.ratio_receive_point = ratio_receive_point;
    }

    public String getDenominator_point() {
        if (denominator_point.equalsIgnoreCase("")) {
            return "0";
        }
        return denominator_point;
    }

    public void setDenominator_point(String denominator_point) {
        this.denominator_point = denominator_point;
    }

    public String getRatio_receive_point() {
        if (ratio_receive_point.equalsIgnoreCase("")) {
            return "0";
        }
        return ratio_receive_point;
    }

    public void setRatio_receive_point(String ratio_receive_point) {
        this.ratio_receive_point = ratio_receive_point;
    }

    public String getAvg_bill() {
        if (avg_bill.equalsIgnoreCase("")) {
            return "0";
        }
        return avg_bill;
    }

    public void setAvg_bill(String avg_bill) {
        this.avg_bill = avg_bill;
    }

    public String getAvg_price_bill() {
        if (avg_price_bill.equalsIgnoreCase("")) {
            return "0";
        }
        return avg_price_bill;
    }

    public void setAvg_price_bill(String avg_price_bill) {
        this.avg_price_bill = avg_price_bill;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCount_attend() {
        return count_attend;
    }

    public void setCount_attend(String count_attend) {
        this.count_attend = count_attend;
    }

    public String getCount_current_point() {
        return count_current_point;
    }

    public void setCount_current_point(String count_current_point) {
        this.count_current_point = count_current_point;
    }

    public String getCount_favorite() {
        return count_favorite;
    }

    public void setCount_favorite(String count_favorite) {
        this.count_favorite = count_favorite;
    }

    public String getCount_order_point() {
        if (count_order_point.equalsIgnoreCase("")) {
            return "0";
        }
        return count_order_point;
    }

    public void setCount_order_point(String count_order_point) {
        this.count_order_point = count_order_point;
    }

    public String getCount_point() {
        return count_point;
    }

    public void setCount_point(String count_point) {
        this.count_point = count_point;
    }

    public String getCount_point_transaction() {
        if (count_point_transaction.equalsIgnoreCase("")) {
            return "0";
        }
        return count_point_transaction;
    }

    public void setCount_point_transaction(String count_point_transaction) {
        this.count_point_transaction = count_point_transaction;
    }

    public String getCount_promotion() {
        return count_promotion;
    }

    public void setCount_promotion(String count_promotion) {
        this.count_promotion = count_promotion;
    }

    public String getCount_purchase_point() {
        return count_purchase_point;
    }

    public void setCount_purchase_point(String count_purchase_point) {
        this.count_purchase_point = count_purchase_point;
    }

    public String getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(String discount_rate) {
        this.discount_rate = discount_rate;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getPercent_use_point() {
        return percent_use_point;
    }

    public void setPercent_use_point(String percent_use_point) {
        this.percent_use_point = percent_use_point;
    }

    public String getWard_id() {
        return ward_id;
    }

    public void setWard_id(String ward_id) {
        this.ward_id = ward_id;
    }

    public int getHourOpen() {
        String[] listString = getOpening_time().split(":");
        return Integer.parseInt(listString[0]);
    }

    public int getMinOpen() {
        String[] listString = getOpening_time().split(":");
        return Integer.parseInt(listString[1]);
    }

    public int getHourClose() {
        String[] listString = getClose_time().split(":");
        return Integer.parseInt(listString[0]);
    }

    public int getHourMin() {
        String[] listString = getClose_time().split(":");
        return Integer.parseInt(listString[1]);
    }

    public String getCount_store() {
        if (count_store.equalsIgnoreCase("")) {
            return "1";
        }
        return count_store;
    }

    public void setTranfer_date(String tranfer_date) {
        this.tranfer_date = tranfer_date;
    }

    public String getTranfer_date() {
        return tranfer_date.replace(".0", "");
    }

    public void setCount_store(String count_store) {
        this.count_store = count_store;
    }

    public void setListPromotion(ArrayList<PromotionModel> listPromotion) {
        this.listPromotion = listPromotion;
    }

    public ArrayList<PromotionModel> getListPromotion() {
        return listPromotion;
    }

    public void setViewFavorite(View viewFavorite) {
        this.viewFavorite = viewFavorite;
    }

    public View getViewFavorite() {
        return viewFavorite;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getMin_price() {
        return min_price;
    }

    public void setMin_price(int min_price) {
        this.min_price = min_price;
    }

    public int getMax_price() {
        return max_price;
    }

    public void setMax_price(int max_price) {
        this.max_price = max_price;
    }

    public ArrayList<StoreModel> getListStore() {
        return listStore;
    }

    public void setListStore(ArrayList<StoreModel> listStore) {
        this.listStore = listStore;
    }

    public String getContract_end_date() {
        return contract_end_date;
    }

    public void setContract_end_date(String contract_end_date) {
        this.contract_end_date = contract_end_date;
    }

}