package com.opencheck.client.models;

/**
 * Created by vutha_000 on 3/7/2018.
 */

public class ProductCollectionModel extends LixiModel {
    private String banner = "";
    private int id = 0;
    private String name = "";
    private String priority = "";
    private String state = "";


    public ProductCollectionModel(String banner, int id, String name, String priority, String state) {
        this.banner = banner;
        this.id = id;
        this.name = name;
        this.priority = priority;
        this.state = state;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "LixiShopCategoryModel{" +
                "banner='" + banner + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", priority='" + priority + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
