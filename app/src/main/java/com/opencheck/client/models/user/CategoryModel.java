package com.opencheck.client.models.user;

import java.io.Serializable;

/**
 * Created by vutha_000 on 3/5/2018.
 */

public class CategoryModel implements Serializable {
    private String id ;
    private String count_merchant;
    private String description;
    private String image;
    private String imageSelected;
    private String name;
    private String parent_id;
    private String sequence;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount_merchant() {
        return count_merchant;
    }

    public void setCount_merchant(String count_merchant) {
        this.count_merchant = count_merchant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public void setImageSelected(String imageSelected) {
        this.imageSelected = imageSelected;
    }

    public String getImageSelected() {
        return imageSelected;
    }

    public static CategoryModel getCategoryDefault() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setId("-1");
        categoryModel.setName("Tất cả cửa hàng");
        categoryModel.setParent_id("-2");
        return categoryModel;
    }
}