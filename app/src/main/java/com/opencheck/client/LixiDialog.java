package com.opencheck.client;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.opencheck.client.models.socket.SocketUtmModel;
import com.opencheck.client.parent.LixiParentDialog;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ConstantValue;

/**
 * Created by vutha_000 on 2/28/2018.
 */

public abstract class LixiDialog extends LixiParentDialog implements View.OnClickListener {

    protected LixiActivity activity;
    private boolean fullScreen;
    private boolean dimBackgroud;
    private boolean titleBar;

    public LixiDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity);
        activity = _activity;
        fullScreen = _fullScreen;
        dimBackgroud = _dimBackgroud;
        titleBar = _titleBar;
        setCancelable(true);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleGoogleAnalytics();
        if (!titleBar)
            requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void show() {
        try {
            super.show();
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ViewGroup.LayoutParams params = getWindow().getAttributes();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            getWindow().setAttributes(
                    (WindowManager.LayoutParams) params);
            if (!dimBackgroud)
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            if (fullScreen)
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {

        }
    }

    @Override
    protected String getScreenName() {
        return null;
    }

    private void handleGoogleAnalytics() {
        LixiApplication application = (LixiApplication) activity.getApplication();
        Tracker mTracker = application.getDefaultTracker();

        String uri = "";
        if (ConstantValue.SOCKET_UTM_MODEL != null) {
            SocketUtmModel utmModel = ConstantValue.SOCKET_UTM_MODEL;
            StringBuilder builder = new StringBuilder();

            if (utmModel.getSource() != null)
                builder.append("utm_source=").append(utmModel.getSource());
            if (utmModel.getCampaign() != null)
                builder.append(builder.length() == 0 ? "utm_campaign=" : "&utm_campaign=").append(utmModel.getCampaign());
            if (utmModel.getContent() != null)
                builder.append(builder.length() == 0 ? "utm_content=" : "&utm_content=").append(utmModel.getContent());
            if (utmModel.getMedium() != null)
                builder.append(builder.length() == 0 ? "utm_medium=" : "&utm_medium=").append(utmModel.getMedium());
            if (utmModel.getTerm() != null)
                builder.append(builder.length() == 0 ? "utm_term=" : "&utm_term=").append(utmModel.getTerm());

            uri = builder.toString();

        }
        //String uri = (new AppPreferenceHelper(this)).getCampaignData();
        if (getScreenName() == null) {
            return;
        }
        mTracker.setScreenName(getScreenName() + "Screen");

        if (uri.length() == 0) {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    //.setCampaignParamsFromUrl(uri)
                    .build());
        } else {
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCampaignParamsFromUrl(uri)
                    .build());
            (new AppPreferenceHelper(activity)).setCampaignData("");
        }
    }
}
