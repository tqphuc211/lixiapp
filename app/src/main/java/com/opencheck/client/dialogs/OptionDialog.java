package com.opencheck.client.dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.OptionDialogBinding;

/**
 * Created by vutha_000 on 3/2/2018.
 */

public class OptionDialog extends LixiDialog {

    //region Layout Variable
    protected RelativeLayout rl_dismiss;

    private TextView tv_message;
    private TextView tv_title;
    private TextView tv_left;
    private TextView tv_right;
    //endregion
    private OptionDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = OptionDialogBinding.inflate(getLayoutInflater(), null, false);
        setContentView(mBinding.getRoot());

        findViews();
        rl_dismiss.setOnClickListener(this);
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_left = (TextView) findViewById(R.id.tv_left);
        tv_right = (TextView) findViewById(R.id.tv_right);
    }


    public OptionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    boolean dismissAble = true;

    public void setDismissAble(boolean dismissAble) {
        this.dismissAble = dismissAble;
        setCancelable(dismissAble);
    }

    public void setTitle(String title) {

    }

    public void setMessage(String message) {

    }

    public void setData(String title, String message, String left_label, String right_label, int leftColor, int rightColor, boolean boltLeft, boolean boltRight) {
        tv_title.setText(title);
        tv_message.setText(message);
        tv_left.setText(left_label);
        tv_right.setText(right_label);
        tv_left.setOnClickListener(this);
        tv_right.setOnClickListener(this);
        tv_left.setTextColor(leftColor);
        tv_right.setTextColor(rightColor);
        if (boltLeft)
            tv_left.setTypeface(null, Typeface.BOLD);
        if (boltRight)
            tv_right.setTypeface(null, Typeface.BOLD);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_dismiss:
                if (dismissAble)
                    dismiss();
                break;
            case R.id.tv_left:
                if (eventListener != null)
                    eventListener.onLeftClick();
                break;
            case R.id.tv_right:
                if (eventListener != null)
                    eventListener.onRightClick();
                break;
            default:
                break;
        }
    }

    public interface IDialogEvent {
        void onLeftClick();

        void onRightClick();
    }

    protected IDialogEvent eventListener;

    public void setIDidalogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
