package com.opencheck.client.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogChooseRegionBinding;
import com.opencheck.client.utils.AppPreferenceHelper;
import com.opencheck.client.utils.ItemClickListener;

public class ChooseRegionDialog extends LixiDialog {
    private LixiActivity activity;

    // View
    private TextView txtSaigon, txtHanoi;

    private ItemClickListener itemClickListener;
    private AppPreferenceHelper appPreferenceHelper;

    public ChooseRegionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
        this.activity = _activity;
    }

    private DialogChooseRegionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogChooseRegionBinding.inflate(
                LayoutInflater.from(getContext()), null, false);
        setContentView(mBinding.getRoot());
        txtSaigon = (TextView) findViewById(R.id.txtSaigon);
        txtHanoi = (TextView) findViewById(R.id.txtHanoi);

        appPreferenceHelper = new AppPreferenceHelper(activity);

        txtSaigon.setOnClickListener(this);
        txtHanoi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSaigon:
                appPreferenceHelper.setRegion(65);
                if (itemClickListener != null) {
                    itemClickListener.onItemCLick(0, txtSaigon.getText().toString(), null, 0L);
                }
                cancel();
                break;
            case R.id.txtHanoi:
                appPreferenceHelper.setRegion(66);
                if (itemClickListener != null) {
                    itemClickListener.onItemCLick(0, txtHanoi.getText().toString(), null, 0L);
                }
                cancel();
                break;
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_OUTSIDE) {
            cancel();
        }
        return false;
    }

    public void addOnItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
