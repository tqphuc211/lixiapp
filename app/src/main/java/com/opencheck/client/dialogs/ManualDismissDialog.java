package com.opencheck.client.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.ManualDismissNotifyDialogBinding;

/**
 * Created by vutha_000 on 3/2/2018.
 */

public class ManualDismissDialog extends LixiDialog {

    //region Layout Variable
    protected RelativeLayout rl_dismiss;

    private TextView tv_message;
    private TextView tv_title;
    private TextView bt_ok;
    //endregion

    private ManualDismissNotifyDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ManualDismissNotifyDialogBinding.inflate(getLayoutInflater(),
                null, false);
        setContentView(mBinding.getRoot());

        findViews();
        initDialogEvent();
    }

    private void initDialogEvent() {
        rl_dismiss.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
        rl_dismiss.setOnClickListener(this);
    }

    private void findViews() {
        rl_dismiss = (RelativeLayout) findViewById(R.id.rl_dismiss);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_title = (TextView) findViewById(R.id.tv_title);
        bt_ok = (TextView) findViewById(R.id.bt_ok);
    }


    public ManualDismissDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    boolean dismissAble = true;

    public void setDismissAble(boolean dismissAble) {
        this.dismissAble = dismissAble;
        setCancelable(dismissAble);
    }

    public void setTitle(String title) {
        tv_title.setText(title);
    }

    public void setMessage(String message) {
        tv_message.setText(message);
    }

    public void setTitleButton(String title) {
        bt_ok.setText(title);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_dismiss:
                if (dismissAble)
                    dismiss();
                break;
            case R.id.bt_ok:
                if (dismissAble)
                    dismiss();
                if (eventListener != null)
                    eventListener.onOk();
            default:
                break;
        }
    }

    public interface IDialogEvent {
        void onOk();
    }

    protected IDialogEvent eventListener;

    public void setIDidalogEventListener(
            IDialogEvent listener) {
        eventListener = listener;
    }
}
