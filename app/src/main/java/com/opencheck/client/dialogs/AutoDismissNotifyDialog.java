package com.opencheck.client.dialogs;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.AutoDismissNotifyDialogBinding;
import com.opencheck.client.utils.ConstantValue;

/**
 * Created by vutha_000 on 2/28/2018.
 */

public class AutoDismissNotifyDialog extends LixiDialog {


    private String message = "";

    public AutoDismissNotifyDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    //region Layout Variable
    private TextView tv_title;
    private TextView tv_message;
    private int time = 1000;
    //endregion
    private AutoDismissNotifyDialogBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = AutoDismissNotifyDialogBinding.inflate(LayoutInflater.from(
                getContext()), null, false);
        setContentView(mBinding.getRoot());
        initView();
        autoDimissDialog();
    }

    private void autoDimissDialog() {
        Handler dismissHandler = new Handler();
        dismissHandler.postDelayed(dismissDialog, ConstantValue.AUTO_DISMISS_INTERVAL);
    }

    private void initView() {
        tv_title = findViewById(R.id.tv_title);
        tv_message = findViewById(R.id.tv_message);
    }

    public void setMessage(String _message) {
        message = _message;
        tv_message.setText(message);
    }

    public void setTitle(String _title) {
        tv_title.setText(_title);
    }

    Runnable dismissDialog = new Runnable() {
        @Override
        public void run() {
            try {
                cancel();
            } catch (IllegalArgumentException e) {

            }
        }
    };

    @Override
    public void onClick(View view) {

    }
}
