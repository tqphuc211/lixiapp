package com.opencheck.client.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.R;
import com.opencheck.client.databinding.DialogUnableConfigRegionBinding;

public class UnableConfigRegionDialog extends LixiDialog {
    TextView txtDone;
    Activity activity;

    public UnableConfigRegionDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
        this.activity = _activity;
    }

    private DialogUnableConfigRegionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DialogUnableConfigRegionBinding.inflate(LayoutInflater.from(getContext()),
                null, false);
        setContentView(mBinding.getRoot());
        setCanceledOnTouchOutside(false);
        txtDone = (TextView) findViewById(R.id.txt_done);

        txtDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        UnableConfigRegionDialog.this.cancel();
    }
}
