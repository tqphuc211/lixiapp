package com.opencheck.client.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.opencheck.client.LixiActivity;
import com.opencheck.client.LixiDialog;
import com.opencheck.client.analytics.Analytics;

import io.socket.client.Socket;

public abstract class LixiTrackingDialog extends LixiDialog {
    public LixiTrackingDialog(@NonNull LixiActivity _activity, boolean _fullScreen, boolean _dimBackgroud, boolean _titleBar) {
        super(_activity, _fullScreen, _dimBackgroud, _titleBar);
    }

    protected Long id = null;
    protected Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
       // Toast.makeText(activity, "ccc", Toast.LENGTH_SHORT).show();
    }

    public void trackScreen() {
        Analytics.trackScreenView(getScreenName(), id);
    }

}
